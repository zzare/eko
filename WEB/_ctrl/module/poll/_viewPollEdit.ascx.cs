﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _ctrl_module_poll_cntPollEdit : BaseControlView
{

    public object Data;
    public object[] Parameters;

    protected POLL Poll;
    protected string  LangID;
    
    protected void Page_Load(object sender, EventArgs e)
    {

        LoadPoll();


        // show add only for new CWP
        if (Poll == null)
        {
            phAdd.Visible = false;

            // show only if least 1 poll
            if (Page.User.Identity.IsAuthenticated ){
                //EM_USER usr = EM_USER.GetUserByUserName(Page.User.Identity.Name );
                EM_USER usr = EM_USER.Current.GetCurrentPage();

                if (POLL.GetPollLangListByUser(usr.UserId, LangID).Count() > 0)
                    phAdd.Visible = true; ;
            }

        }
        else
            phAdd.Visible = false;

    }



    protected void LoadPoll() {

        List<POLL_CHOICE> choiceList = new List<POLL_CHOICE>();
        choiceList.Add(new POLL_CHOICE { Choice = "" });
        choiceList.Add(new POLL_CHOICE { Choice = "" });

        if (Parameters != null )
            LangID = Parameters[0].ToString();

        // default data
        tbQuestion.Value = "";
        cbActivePoll.Checked = true;

        if (Data != null)
        {
            // data
            Poll = (POLL)Data;

            POLL_LANG pollLang = POLL.GetPollLangByID(Poll.PollID, LangID);
            if (pollLang == null)
                pollLang = new POLL_LANG { PollQuestion = "" };

            // load poll data
            tbQuestion.Value = pollLang.PollQuestion;
            cbActivePoll.Checked = Poll.Active;

            // choice list
            choiceList = POLL.GetPollChoiceList(Poll.PollID, LangID).ToList();
        }


        // load poll choices
        lvList.DataSource = choiceList;
        lvList.DataBind();


    
    }
}
