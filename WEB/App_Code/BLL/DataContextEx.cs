﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Linq ;
using System.Data.Linq.Mapping;
using System.Reflection;
using System.Web;

/// <summary>
/// Summary description for DataContextEx
/// </summary>
public partial class eMenikDataContext
{


    [Function(Name = "dbo.em_GetEmenikSitemapLang_byMenuID_Hierarchy")]
    public ISingleResult<em_GetEmenikSitemapLang_HierarchyResult> em_GetEmenikSitemapLang_byMenuID_Hierarchy2([Parameter(Name = "MenuID", DbType = "Int")] System.Nullable<int> menuID, [Parameter(Name = "UICulture", DbType = "NVarChar(5)")] string uICulture)
    {
        IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), menuID, uICulture);
        return ((ISingleResult<em_GetEmenikSitemapLang_HierarchyResult>)(result.ReturnValue));
    }

    [Function(Name = "dbo.em_GetEmenikSitemapLangEx_byMenuID_Hierarchy")]
    public ISingleResult<em_GetEmenikSitemapLangEx_HierarchyResult> em_GetEmenikSitemapLangEx_byMenuID_Hierarchy2([Parameter(Name = "MenuID", DbType = "Int")] System.Nullable<int> menuID, [Parameter(Name = "UICulture", DbType = "NVarChar(5)")] string uICulture)
    {
        IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), menuID, uICulture);
        return ((ISingleResult<em_GetEmenikSitemapLangEx_HierarchyResult>)(result.ReturnValue));
    }

}
