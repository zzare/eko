﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Reflection;

public partial class _em_content_search : SM.EM.UI.BasePage_EMENIK
{
    public string  SearchString
    {
        get
        {
            //if (Request.QueryString["srch"] == null)
            //    ErrorRedirect(151, 404);
            //if (string.IsNullOrEmpty( Request.QueryString["srch"].Trim()))
            //    ErrorRedirect(151, 404);


            //return Microsoft.Security.Application.AntiXss.UrlDe(Request.QueryString["srch"]);
            return Server.UrlDecode(Request.QueryString["srch"]);
        }
    }





    protected override void OnInit(EventArgs e)
    {
        // set sitemap id
        CurrentSitemapID = -1;

        base.OnInit(e);
    }

    protected override void OnInitComplete(EventArgs e)
    {
        base.OnInitComplete(e);
        ProductRep prep = new ProductRep();
//        var list = objList.Prep.GetProductDescListSearch(SearchString, LANGUAGE.GetCurrentLang(), true, em_user.UserId);
        IQueryable<vw_Product> list = null;

        bool? active = true;
        if (IsModeCMS)
            active = null;

        if (!string.IsNullOrEmpty(SearchString))
            list = objList.Prep.GetProductDescListSearch(SearchString, LANGUAGE.GetCurrentLang(), active, em_user.UserId);

        objList.ProductList = list;
//        objList.PageSize = 40;
        objList.EnableSorting = false;
        objList.EnableFilters = false;
//        objList.StartingQueryString = "srch=" +  Request.QueryString["srch"];
    }
    protected void Page_Load(object sender, EventArgs e)
    {


    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        if (string.IsNullOrEmpty(SearchString ))
            objList.Visible = false;
    
    }



    protected override void LoadTitle()
    {
        base.LoadTitle();

        // append article to title
        if (!string.IsNullOrEmpty(SearchString ))
            Page.Title = SearchString  + " | " + Page.Title;
        else
            Page.Title = "Iskanje | " + Page.Title;


    }

    protected override void SetRelCanonical()
    {
        // base.SetRelCanonical();
        RelCanonical = Page.ResolveUrl( SM.EM.Rewrite.SearchProductEmenikUrl(this.em_user.UserId, SearchString));
    }


    protected void btSearch_Click(object o, EventArgs e)
    {
        if (!string.IsNullOrEmpty(tbSearch.Text.Trim()))
            Response.Redirect(Page.ResolveUrl(SM.EM.Rewrite.SearchProductEmenikUrl(em_user.UserId, tbSearch.Text)));


    }


}
