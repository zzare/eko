﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_ctrlSubMenu.ascx.cs" Inherits="_em_content__common_eko_ctrlSubMenu" %>



    <div class="navSubW">
        <div class="navSub">
            <div>
                <span class="navSub_choose"><%= GetGlobalResourceObject("Modules", "MenuChooseCategory")%>:</span>
            
                <div id="cMenuB" class="cMenuB">
                    <asp:SiteMapDataSource ID="smdsLeft" runat="server"  ShowStartingNode="false" StartingNodeOffset="1"  />
                    <asp:Menu ID="mLeft" DataSourceID="smdsLeft"   runat="server" CssSelectorClass="mEkoSub" ondatabound="mLeft_DataBound"  IncludeStyleBlock="false" Orientation="Horizontal" ></asp:Menu>
                </div>

                <div class="clearing"></div>
            </div>    
        </div>
    </div>
