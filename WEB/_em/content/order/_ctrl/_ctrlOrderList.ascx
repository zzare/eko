﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_ctrlOrderList.ascx.cs" Inherits="_em_content_order__ctrl__ctrlOrderList" %>
<%@ Register Namespace="SM.EM.UI.Controls" TagPrefix="EM"  %>


<asp:ListView ID="lvList" runat="server" >
    <LayoutTemplate>
        <div class="orderHistory">
            <asp:PlaceHolder ID="itemPlaceholder" runat="server"/>
        </div>
    </LayoutTemplate>
    
    <ItemTemplate>
        <span style="float:left;">
        Številka naročila: <a href='<%#  Page.ResolveUrl(SM.EM.Rewrite.OrderProductUrl(PageID, new Guid(Eval("OrderID").ToString()), 5, "v=1"))%>'><%# SHOPPING_ORDER.Common.RenderShoppingOrderNumber((SHOPPING_ORDER ) Container.DataItem )%></a>
        &nbsp;&nbsp;&nbsp;&nbsp;
        Status: <span class="orderHistory_darkS"><%# SHOPPING_ORDER.Common.RenderOrderStatus(Eval("OrderStatus"))%></span>
        &nbsp;&nbsp;&nbsp;&nbsp;
        Datum naročila: <span class="orderHistory_darkS"><%# SM.EM.Helpers.FormatDateMedium(Eval("DateStart"))%></span>
        &nbsp;&nbsp;&nbsp;&nbsp;
        Znesek: <span class="orderHistory_darkS"><%# SM.EM.Helpers.FormatPrice (Eval("TotalWithTax"))%></span>
        </span>
        <span style="float:right;"><a href='<%#  Page.ResolveUrl(SM.EM.Rewrite.OrderProductUrl(PageID, new Guid(Eval("OrderID").ToString()), 5, "v=1"))%>'>podrobnosti naročila &raquo;</a>    
        </span>
        <div class="clearing"></div>
        <span class="orderHistory_separator"></span>
            
    </ItemTemplate>
    
</asp:ListView>

<asp:PlaceHolder ID="phEmpty" Visible="false" runat="server">
    Ni naročil

</asp:PlaceHolder>

