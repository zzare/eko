﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_cmsNewsletterEmailList.ascx.cs" Inherits="SM.UI.Controls._cmsNewsletterEmailList" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Namespace="SM.UI.Controls" TagPrefix="SM" %>


<div class="qContList">

        <asp:LinqDataSource ID="ldsList" runat="server" OnSelecting="ldsList_OnSelecting" >
        </asp:LinqDataSource>
        
        <SM:smListView ID="lvList" runat="server" DataSourceID="ldsList" 
            InsertItemPosition="None" onitemcommand="lvList_ItemCommand"  >
            <LayoutTemplate>
                <table  class="tblData" cellpadding="0" cellspacing="0">
                        <thead>
                            <tr id="headerSort" runat="server">
                                <th ><asp:LinkButton ID="LinkButton2" CommandName="sort" CommandArgument="Email" runat="server">Email</asp:LinkButton></th>
                                <th ><asp:LinkButton ID="LinkButton3" CommandName="sort" CommandArgument="Status" runat="server">Status</asp:LinkButton></th>
                                <th ><asp:LinkButton ID="LinkButton1" CommandName="sort" CommandArgument="Opombe" runat="server">Opombe</asp:LinkButton></th>
                                <th width="100px" colspan="1" runat="server"  id="headManage" >Uredi</th>
                            </tr>
                        </thead>
                        <tbody id="itemPlaceholder" runat="server"></tbody>
                        <tfoot>
                            <tr>
                                <th style="text-align:right" colspan="4" >
                                    <asp:DataPager runat="server" ID="DataPager" PageSize="30" >
                                        <Fields>
                                            <asp:NextPreviousPagerField  />
                                            <asp:NumericPagerField ButtonCount="5" />
                                        </Fields>
                                    </asp:DataPager>
                                </th>
                            </tr>
                        </tfoot>
                    </table>
            </LayoutTemplate>
            
            <ItemTemplate>
                <tr  class='<%#  Container.DataItemIndex % 2 == 0 ? "  " : " odd " %>'  onmouseover ="this.className='<%#  Container.DataItemIndex % 2 == 0 ? "hovRow" : "hovRowodd" %>'" onmouseout ="this.className = GetCssClass(<%#  Container.DataItemIndex %>)">
                    <td>
                        <%# Eval("Email")%>
                    </td>
                    <td>
                        <%# Eval("Status")%>
                    </td>
                    <td>
                        <%# Eval("Notes")%>
                    </td>
                    <td width="50px" runat="server"  >
                        <asp:LinkButton runat="server" CommandArgument='<%# Eval("EmailID") %>' CommandName ="del">delete</asp:LinkButton>
                    </td>
                </tr>  
            
            </ItemTemplate>
            
            <EmptyDataTemplate>
                
                <div class="odd">
                    Seznam je prazen.
            
                </div>
                
            </EmptyDataTemplate>
            
        
        </SM:smListView>

</div>
