﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

/// <summary>
/// Summary description for  Module
/// </summary>
public partial class USER_MODULE
{

    public static IEnumerable<USER_MODULE> GetUserModules(Guid userID)
    {
        eMenikDataContext db = new eMenikDataContext();
        IEnumerable<USER_MODULE> list;

        list = from m in db.USER_MODULEs where m.UserId == userID  select m;
        return list;
    }

    public static USER_MODULE GetUserModuleByID(Guid userID, int modID)
    {
        eMenikDataContext db = new eMenikDataContext();
        USER_MODULE list;

        list = (from m in db.USER_MODULEs where m.UserId == userID && m.MOD_ID == modID  select m).FirstOrDefault();
        return list;
    }


    public static decimal GetUserModulePriceSum(Guid userID)
    {
        eMenikDataContext db = new eMenikDataContext();

        var list = (from u in db.USER_MODULEs join m in db.MODULEs on u.MOD_ID equals m.MOD_ID   where u.UserId == userID select m.EM_PRICE);
        if (list == null) return 0;
        if (list.Count() < 1) return 0;
        return list.Sum();
    }

    public static decimal GetUserModulePriceSumDiscount(EM_USER usr)
    {
        decimal total = GetUserModulePriceSum(usr.UserId ) ;

        return total * ((decimal)(100 - usr.GetUserDiscount()) / 100);
    }


}
