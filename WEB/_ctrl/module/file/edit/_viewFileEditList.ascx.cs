﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _ctrl_module_file_edit_viewFileEditList: BaseControlView
{

    //public object Data;
    //public object[] Parameters;
    public Guid ProductID;
    public string  LangID;
    public short FileType;



//    public IQueryable<FILE> Files;
    

    protected void Page_Load(object sender, EventArgs e)
    {


        LoadData();
    }



    public void LoadData()
    {



        if (Data  == null)
            return;
//        Files = (IQueryable<FILE>)Data;

        if (Parameters != null)
        {
            //if (Parameters[0] != null)
            //    UserCultures = (List<vw_UserCulture>)Parameters[0];

            if (Parameters.Length > 0 && Parameters[0] != null)
                ProductID = new Guid(Parameters[0].ToString());
            if (Parameters.Length > 1 && Parameters[1] != null)
                LangID = Parameters[1].ToString();
            if (Parameters.Length > 2 && Parameters[2] != null)
                FileType  = (short )Parameters[2];
        }

        // list
        lvFileList.DataSource = Data ;
        lvFileList.DataBind();

 
    }





}
