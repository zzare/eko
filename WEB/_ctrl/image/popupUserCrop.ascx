﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="popupUserCrop.ascx.cs" Inherits="SM.UI.Controls.popupUserCrop" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Namespace="SM.UI.Controls" TagPrefix="SM"  %>



<SM:InlineScript ID="InlineScript1"  runat="server">
<script type="text/javascript">


    var x,y,x2,y2;
    var cropRes;
    function int_jcrop() {
        var fW = <%= WidthForce  %>;
        var fH = <%= HeightForce  %>;
        var fR = fH/fW;
        var img = $('#<%= imgBig.ClientID%>');
        var iH, iW, iR, h,w;
        
        iH = img[0].height;
        iW = img[0].width;
        iR = iH/iW;    
        
        if (iR >= fR){
            w = iW;
            h= Math.floor(iW * fR);
            y = Math.floor(Math.abs((iH-h)/2));
            x=0;
        }
        else
        {   h = iH;
            w= Math.floor(iH / fR);    
            y = 0;
            x=Math.floor(Math.abs((iW-w)/2));
        }

        x2=x+w;
        y2=y+h;    
        
        img.Jcrop({
            onSelect: onCropEnd,
            setSelect:   [ x, y, x2, y2 ]
        });
    }

    function onCropEnd(c) {
        cropRes = c;
        x=c.x;
        y=c.y;
        x2=c.x2;
        y2 = c.y2;
        
    }
    function CustomCrop(){
        var img =  $get('<%= hfImg.ClientID%>').value;
        var type = $get('<%= hfType.ClientID%>').value;
        var save =  <%= AutoSaveImage.ToString().ToLower() %>;
        // crop image
        WebServiceEMENIK.CustomCropImage(img, type, x, y, x2, y2, '', save, OnCompleteImgCrop);
        return false;
    }        
    function OnCompleteImgCrop(res){
        // set image with new src
        if (res && res[0] && res[1]){
            $get('<%= hfImageUrlVirt.ClientID %>').value = res[0];
            $get('<%= ImgTargetClientID %>').src = res[1];
            var trgtUrl = $get('<%= TargetVirtualURLClientID %>');
            // set target url field
            if (trgtUrl )
                trgtUrl.value = res[0];
        }
        // close modal popup
        CloseCrop();
    }
    function CloseCrop(){
        $find("mpeUserCrop").hide();
    }    


    function pageLoad(){
//        if ($('#<%= imgBig.ClientID%>').height() == 0)
            $find("mpeUserCrop").add_shown(function (){
                if ($('#<%= imgBig.ClientID%>').height() == 0)
                    $('#<%= imgBig.ClientID%>').load(function (){int_jcrop();});
                 else
                    int_jcrop();                        
            });
  //      else
    //        $find("mpeUserCrop").add_shown(function (){int_jcrop();});
    }


</script>
</SM:InlineScript>



<asp:Panel ID="panCrop" CssClass="modPopupCrop" runat="server" style="display:none;">

    <div class="modPopup_header modPopup_headerSettings">
        <h4>UREDI/POPRAVI SLIKO</h4>
        <p>Uredi/popravi sliko. Z miško označi želeno vidnost slike. Če slika ni v okviru dovoljenih dimenzij, bo samodejno popravljena.</p>
    </div>
    <div class="modPopup_main">
    <div style="margin-top:5px; margin-bottom:5px; font-size:14px;">
        <asp:Label ID="labDesc" st runat="server" Text="Kliknite na sliko in označi želen kvadrat. Ko ste končali, kliknite OK. Če slika ni v okviru dovoljenih dimenzij, bo samodejno popravljena. "></asp:Label>
    </div>
    <div class="buttonwrapperModal">
            <asp:LinkButton ID="LinkButton1" CssClass="lbutton lbuttonConfirm" runat="server" OnClientClick="CustomCrop(); return false" ><span>Shrani</span></asp:LinkButton>
            <asp:LinkButton ID="LinkButton2" CssClass="lbutton lbuttonDelete" runat="server" OnClientClick="CloseCrop(); return false" ><span>Prekliči</span></asp:LinkButton>
    </div>
<img id="imgBig" runat="server" />

    <asp:HiddenField ID="hfImg" runat="server" />
    <asp:HiddenField ID="hfType" runat="server" />
    <asp:HiddenField ID="hfImageUrlVirt" runat="server" />    
        

    <br />
    
    
    </div>    
    <div class="modPopup_footer">
        <div class="buttonwrapperModal">
            <asp:LinkButton ID="btCrop" CssClass="lbutton lbuttonConfirm" runat="server" OnClientClick="CustomCrop(); return false" ><span>Shrani</span></asp:LinkButton>
            <asp:LinkButton ID="btCancel" CssClass="lbutton lbuttonDelete" runat="server" ><span>Prekliči</span></asp:LinkButton>
        </div>
     </div>

</asp:Panel>

           
<cc1:ModalPopupExtender RepositionMode="RepositionOnWindowResize" Y ="10"   BehaviorID="mpeUserCrop"  ID="mpeUserCrop" runat="server" TargetControlID="hfImg" PopupControlID="panCrop"
                        BackgroundCssClass="modBcg" CancelControlID="btCancel"  >
</cc1:ModalPopupExtender> 

