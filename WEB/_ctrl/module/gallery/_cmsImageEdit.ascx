﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_cmsImageEdit.ascx.cs" Inherits="SM.EM.UI.CMS.Controls._cmsImageEdit" %>
<%@ Register Namespace="SM.UI.Controls" TagPrefix="SM" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="SM" TagName="PopupUserCrop" Src="~/_ctrl/image/popupUserCrop.ascx"  %>

<SM:InlineScript runat="server">
    <script type="text/javascript" language="javascript">
        function SetMaxImageWidth(img, w) {
            if (img.width > w)
                img.width = w;
        }
        function OpenUserCrop() {
            $find("mpeUserCrop").show();
        }
    </script>    
</SM:InlineScript>

<span class="modPopup_mainTitle" >Uredi sliko</span>

<div >


    <img id="imgMain" runat="server" />
    <asp:HiddenField ID="hfImgVirtualUrl" runat="server" />
    <br />
    <div class="buttonwrappersim">
        <asp:LinkButton ID="btCrop" runat="server" CssClass="lbutton lbuttonEdit" OnClientClick="OpenUserCrop(); return false"><span>Popravi sliko</span></asp:LinkButton>
        <asp:LinkButton ID="btDelete" runat="server" CssClass="lbutton lbuttonDelete" OnClick="btDelete_Click" OnClientClick="return confirm('Ali res želite zbrisati sliko?')"><span>Zbriši sliko</span></asp:LinkButton>
    </div>    
</div>

<div>
    <span class="modPopup_mainTitle" >Prikazovanje</span>

    <asp:CheckBox ID="cbActive" runat="server" Text="&nbsp;&nbsp;Aktivna (prikazovanje slike na spletni strani)" />
            <cc1:ToggleButtonExtender ID="ToggleEx" runat="server"
            TargetControlID="cbActive" 
            ImageWidth="19" 
            ImageHeight="19"
            CheckedImageAlternateText="Odkljukaj"
            UncheckedImageAlternateText="Obkljukaj"
            UncheckedImageUrl="~/_inc/images/design/checkbox_off.gif" 
            CheckedImageUrl="~/_inc/images/design/checkbox_on.gif"
            DisabledCheckedImageUrl="~/_inc/images/design/checkbox_disabled.gif"
            DisabledUncheckedImageUrl="~/_inc/images/design/checkbox_disabled.gif" />

    <br />
    <br />
    <span class="modPopup_mainTitle" >Ostali podatki</span>

    Avtor slike:&nbsp;<asp:TextBox ID="tbAuthor" class="modPopup_TBXsettings" runat="server"></asp:TextBox>
    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1"  WatermarkText="Tukaj vpiši avtorja slike" TargetControlID ="tbAuthor" WatermarkCssClass="modPopup_mainWatermark" runat="server" ></cc1:TextBoxWatermarkExtender>
</div>
    
    <span class="modPopup_mainTitle" >Uredi naziv in opis slike za različne jezike</span>

<div >
    <SM:smListView ID="lvList" DataKeyNames="IMG_ID,LANG_ID" runat="server"  InsertItemPosition="None" >
        <LayoutTemplate>
            <asp:PlaceHolder id="itemPlaceholder" runat="server"></asp:PlaceHolder>
        </LayoutTemplate>
        
        <ItemTemplate>
            <div style="float:left; width:260px;">
                <asp:HiddenField ID="hfLang" Value='<%#  Eval("LANG_ID")%>' runat="server" />
                   
                <div style="color:#659700; font-size:15px; margin-bottom:4px;"> <%#  Eval("LANG_DESC")%></div>
                <span style="font-size:14px;">Naziv slike</span>
                <br />
                <asp:TextBox ID="tbTitle" CssClass="modPopup_TBXsettings modPopup_TBXsettingsGalLang" runat="server" MaxLength="256" Text='<%#  Eval("IMG_TITLE")%>'></asp:TextBox>
                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1"  WatermarkText="Tukaj vpiši naziv slike" TargetControlID ="tbTitle" WatermarkCssClass="modPopup_mainWatermark modPopup_mainWatermarkGalLang" runat="server" ></cc1:TextBoxWatermarkExtender>
                <br />
                <span style="font-size:14px;">Opis slike</span> 
                <br />
                <asp:TextBox ID="tbDesc" CssClass="modPopup_TBXsettings modPopup_TBXsettingsGalLang" runat="server" MaxLength="512" TextMode="MultiLine" Rows="3" Text='<%#  Eval("IMG_DESC")%>'></asp:TextBox>
                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2"  WatermarkText="Tukaj vpiši opis slike" TargetControlID ="tbDesc" WatermarkCssClass="modPopup_mainWatermark modPopup_mainWatermarkGalLang" runat="server" ></cc1:TextBoxWatermarkExtender>
                <br />
            </div>
        </ItemTemplate>
    </SM:smListView>
</div>

<br />

<SM:PopupUserCrop id="popupCrop" runat="server"></SM:PopupUserCrop>
