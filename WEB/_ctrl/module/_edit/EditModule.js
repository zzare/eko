﻿

function map_init() {
//    var map = new Object();
//    var mLat = null;
//    var mLong = null;
//    var iOpen = true;
//    var editID = null;

}

jQuery.fn.init_gal_sort = function() {
    return this.each(function() {
        $(this).sortable({
            cursor: "move",
            handle: 'img.imgMove',
            //items: "div",
            //helper:"clone",
            revert: true,
            opacity: 0.5,
            tolerance: "pointer",
            placeholder: "cwp_ph_img",
            //        forcePlaceholderSize: true,
            forcePlaceholderSize: true,
            zIndex: 1000,
            update: function(evn, ui) {
                var index = $(this).children('div.divGalImgThumb').index(ui.item);
                if (index >= 0) {
                    WebServiceCWP.MoveImage($(this).parents("div.cwp")[0].id, $(ui.item).children("img.imgMove")[0].id, index, OnMoveOk);
                }
            }
        });
    });
};


function cwp_init() {
    $("div.cwp_zone").sortable({
        cursor: "move",
        handle: 'div.cwpMove',
        connectWith: ["div.cwp_zone"],
        revert: true,
        opacity: 0.5,
        tolerance: "pointer",
        delay: 100,
        placeholder: "cwp_placeholder",
        dropOnEmpty: true,
        zIndex: 1000,
        forcePlaceholderSize: true,
        update: function(evn, ui) {
            var index = $(this).children("div.cwp").index(ui.item);
            if (index >= 0) {
                WebServiceCWP.MoveCWP(ui.item[0].id, em_g_cult, this.id, index, OnMoveOk);
            }
        }

    });
}

function gal_init() {
    $("div.galImgList").init_gal_sort();

}

function ttip_init() {
    x_Offset = 30;
    y_Offset = 10;
    posX = 0;
    dir = "left";

    $("a.btsettStatusHelp").hover(function(e) {
        if ($(window).width() / 2 < e.pageX) {
            dir = "right";
            posX = ($(window).width() - e.pageX) + y_Offset;
        }
        else {
            dir = "left";
            posX = e.pageX + y_Offset;
        }
        var h = $(this).next();
        $("div.settStatusWrapp").hide();
        h.show().css("top", (e.pageY - x_Offset) + "px").css(dir, posX + "px").fadeIn("fast");

    }, function() {
        $("div.settStatusWrapp").hide();
        //$(this).next().hide();
    });
}        

function OnMoveOk(res) {
    //window.location.reload();
    //if (refr)
//    __doPostBack('foo', '');
}
function Refresh(url) {
    if (url && url.length != "") {
        location.replace(url);
    }
    else
        __doPostBack('foo', '');
}

function highlight(el) {
    $(el).effect("pulsate", { times: 10 }, 600, function() { $(this).css("border", "0"); });
}
function publishSite() {
    $find("mpeSendPredracun").show();
    //WebServiceEMENIK.SetPageActive(onPublished);
}
function onPublished(data) {
    $("span.icon span").html(data);
}

function wiz_init() {

}



