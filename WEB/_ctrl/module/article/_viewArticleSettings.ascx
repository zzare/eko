﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewArticleSettings.ascx.cs" Inherits="_ctrl_module_article_cntArticleSettings" %>


<script language = "javascript" type="text/javascript">
    function settingsArticleSave(cwp, v) {
        var t = $("#" + v).find("input.tbTitle").val();
        var cb = $("#" + v).find("input.cbActive").attr('checked');
        var ps = $("#" + v).find("input.tbPageSize").val();
        var cbi = $("#" + v).find("input.cbShowImage").attr('checked');
        var cbab = $("#" + v).find("input.cbShowAbstract").attr('checked');
        var cbar = $("#" + v).find("input.cbShowArchive").attr('checked');
        var cbp = $("#" + v).find("input.cbShowPaging").attr('checked');

        var tt = $("#" + v).find("div.cSetThumbType :input:radio:checked ").val();
        if (tt == undefined || tt == null)
            tt = -1;

        var dt = $("#" + v).find("#ddlDisplayType").val();
        if (dt == undefined || dt == null)
            dt = -1;

            
        //alert(t + '<br/> ' + cb + '<br/> ' + ps + '<br/> ' + cbi + '<br/> ' + tt + '<br/> ');


        openModalLoading('Shranjujem...');
        WebServiceCWP.SaveSettingsArticle(cwp, t, cb, ps, cbab, cbar, cbi, tt, dt, '', cbp, onArticleSettingsSuccessSave, onCWPSettingsErrorSave, cwp);
    }



</script>

<div id='<%= ViewID %>'>

    <div class="modPopup_header modPopup_headerEdit">
        <h4>NASTAVITVE ELEMENTA DINAMIČNE VSEBINE</h4>
        <p>Uredi naziv elementa. Določi aktivnost naziva elementa na spletni strani. </p>
    </div>
    <div class="modPopup_main">
        
        
        <span class="modPopup_mainTitle" >Naziv elementa ČLANKI/NOVICE</span>
        <div style="float:left;">
            <input class="modPopup_TBXsettings tbTitle"  maxlength="256" type="text" value='<%= Cwp.TITLE %>' />
        </div>
        <div style="float:left; padding-top:3px;">
            <label  for="<%= cbActive.ClientID %>"> <input class="cbActive"  id="cbActive" runat="server" name ="cbActive" type="checkbox" />&nbsp;&nbsp;Prikaži naziv tega elementa na spletni strani</label>
        </div>

        <br />
        <div class="clearing"></div>        
        <span class="modPopup_mainTitle" >Nastavitve prikaza seznama (člankov/novic)</span>
         <input class="tbPageSize modPopup_TBXsettings"   style="width:20px;" maxlength="256" type="text" value='<%= Cwp.PAGE_SIZE.ToString() %>' />
         Število prikazanih zapisov (člankov/novic) v seznamu <i>* ostali so vidni v arhivu oz. na naslednjih straneh, če je vklopljeno straničenje.</i>
        
        <div style="margin-top:7px;">
            <label  for="<%= cbShowAbstract.ClientID %>"> <input class="cbShowAbstract modPopup_CBXsettings"  id="cbShowAbstract" runat="server" name ="cbShowAbstract" type="checkbox" />&nbsp;&nbsp;Prikaži kratek opis novice v seznamu</label>
        </div>               
                
        <div style="margin-top:7px;">
            <label  for="<%= cbShowArchive.ClientID %>"> <input class="cbShowArchive modPopup_CBXsettings"  id="cbShowArchive" runat="server" name ="cbShowArchive" type="checkbox" />&nbsp;&nbsp;Prikaži povezavo na VSE NOVICE pod seznamom</label>
        </div>               

        <div style="margin-top:7px;">
            <label  for="<%= cbShowPaging.ClientID %>"> <input class="cbShowPaging modPopup_CBXsettings"  id="cbShowPaging" runat="server" name ="cbShowPaging" type="checkbox" />&nbsp;&nbsp;Prikaži STRANIČENJE (paging)</label>
        </div>               
        

        <br /> <br />
        <div class="clearing"></div>        
        
        <span class="modPopup_mainTitle" >Prikaz slik v seznamu</span>
        <div style="margin-top:7px;">
            <label  for="<%= cbShowImage.ClientID %>"> <input class="cbShowImage modPopup_CBXsettings"  id="cbShowImage" runat="server" name ="cbShowImage" type="checkbox" />&nbsp;&nbsp;Prikaži prvo sliko ob novici v seznamu</label>
        </div>
        
        
        <span class="modPopup_mainTitle" >Velikost slike v seznamu </span>
        
        <div class="cSetThumbType">        
            <asp:ListView ID="lvType" runat="server">
                <LayoutTemplate>
                    <asp:PlaceHolder ID="itemPlaceholder" runat="server"/>
                </LayoutTemplate>

                <ItemTemplate>
                    <label>
                        <input <%# RenderChecked(Container.DataItem) %> type="radio" name="galsettings" value='<%# Eval("TYPE_ID") %>' />
                        <span ><%# Eval("DESC") %></span>
                        <div class="clearing"></div>
                    </label>
                    
                </ItemTemplate>
            </asp:ListView> 
        </div>
        
        
        <span class="modPopup_mainTitle" >Način prikaza</span>
        <select id="ddlDisplayType">
          <option <%= RenderCheckedType(CMS_WEB_PART.Common.DisplayType.ART_NORMAL) %>  value='<%= CMS_WEB_PART.Common.DisplayType.ART_NORMAL %>'>navadno</option>
          <option <%= RenderCheckedType(CMS_WEB_PART.Common.DisplayType.ART_WITH_SUBCATS) %> value='<%= CMS_WEB_PART.Common.DisplayType.ART_WITH_SUBCATS %>'>novice iz podkategorij</option>
        </select>
        

        
        

    </div>   
    <div class="clearing"></div>
    <div class="modPopup_footer"> 
        <div class="buttonwrapperModal">
            <a class="lbutton lbuttonConfirm" href="#" onclick="settingsArticleSave('<%= Cwp.CWP_ID.ToString() %>', '<%= ViewID %>'); return false"><span>Shrani</span></a>
            <a class="lbutton lbuttonDelete" href="#" onclick="closeModalLoader(); return false" ><span>Prekliči</span></a>
        </div>
    </div>
</div>






