﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

/// <summary>
/// Summary description for STATUS
/// </summary>
public partial class STATUS
{
    public static List<STATUS> GetStatusesByType(int type)
    {

        eMenikDataContext db = new eMenikDataContext();

        List<STATUS> rec = (from r in db.STATUS where r.TypeID == type  orderby r.Order  select r).ToList();
        return rec;
    }
    public static List<STATUS> GetActiveStatusesByType(int type)
    {

        eMenikDataContext db = new eMenikDataContext();

        List<STATUS> rec = (from r in db.STATUS where r.TypeID == type && r.Active == true orderby r.Order select r).ToList();
        return rec;
    }

    public static STATUS GetStatusByID(int id)
    {

        eMenikDataContext db = new eMenikDataContext();

        STATUS rec = (from r in db.STATUS  where r.StatusID == id select r).SingleOrDefault();
        return rec;

    }

    public static void DeleteStatus(int id) {

        eMenikDataContext db = new eMenikDataContext();

        STATUS rec = (from r in db.STATUS where r.StatusID == id select r).SingleOrDefault();
        db.STATUS.DeleteOnSubmit(rec);
        db.SubmitChanges();

    }

    public static bool AllowDelete(int tID)
    {
        return true;
    }

    public class Common {
        public class Type {
            public static int TicketOrder = 1;
        }
        public class Status {
            public static int TicketOrderStart = 2;
        
        }
    
        public static string  RenderCMSHref(int skID){

            return "~/cms/zoo/ManageSkupina.aspx?s=" + skID.ToString();
        }
    
    }
}
