﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="cwpArticleSettings.ascx.cs" Inherits="cwpArticleSettings" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="FredCK.FCKeditorV2" Namespace="FredCK.FCKeditorV2" TagPrefix="FCKeditorV2" %>



<span class="modPopup_mainTitle" >Naziv elementa članki/novice</span>
<div style="float:left;">
    <asp:TextBox ID="tbTitle" class="modPopup_TBXsettings" runat="server"></asp:TextBox>
</div>
<div style="float:left; padding-top:3px;">

    <asp:CheckBox ID="cbShowTitle" CssClass="modPopup_CBXsettings" runat="server" Text = "&nbsp;&nbsp;Prikaži naziv na spletni strani" />
    <cc1:ToggleButtonExtender ID="ToggleEx" runat="server"
        TargetControlID="cbShowTitle" 
        ImageWidth="19" 
        ImageHeight="19"
        CheckedImageAlternateText="Odkljukaj"
        UncheckedImageAlternateText="Obkljukaj"
        UncheckedImageUrl="~/_inc/images/design/checkbox_off.gif" 
        CheckedImageUrl="~/_inc/images/design/checkbox_on.gif"
        DisabledCheckedImageUrl="~/_inc/images/design/checkbox_disabled.gif"
        DisabledUncheckedImageUrl="~/_inc/images/design/checkbox_disabled.gif" />
</div>
<div class="clearing"></div>

<br />
<span class="modPopup_mainTitle" >Nastavitve prikaza seznama (člankov/novic) </span>

<asp:TextBox ID="tbPageSize" class="modPopup_TBXsettings" Width="20px" runat="server"></asp:TextBox>Število prikazanih zapisov (člankov/novic) v seznamu (ostali so vidni v arhivu)
<cc1:FilteredTextBoxExtender runat="server"  FilterType="Numbers" TargetControlID="tbPageSize"></cc1:FilteredTextBoxExtender>
<br />

<div style="margin-top:5px;">
<asp:CheckBox ID="cbShowAbstract" CssClass="modPopup_CBXsettings" runat="server" Text = "&nbsp;&nbsp;Prikaži kratek opis novice v seznamu" />
<cc1:ToggleButtonExtender ID="ToggleButtonExtender2" runat="server"
    TargetControlID="cbShowAbstract" 
    ImageWidth="19" 
    ImageHeight="19"
    CheckedImageAlternateText="Odkljukaj"
    UncheckedImageAlternateText="Obkljukaj"
    UncheckedImageUrl="~/_inc/images/design/checkbox_off.gif" 
    CheckedImageUrl="~/_inc/images/design/checkbox_on.gif"
    DisabledCheckedImageUrl="~/_inc/images/design/checkbox_disabled.gif"
    DisabledUncheckedImageUrl="~/_inc/images/design/checkbox_disabled.gif" />
</div>
<div style="margin-top:7px;">
<asp:CheckBox ID="cbShowArchive" CssClass="modPopup_CBXsettings" runat="server" Text = "&nbsp;&nbsp;Prikaži povezavo na vse novice pod seznamom" />
<cc1:ToggleButtonExtender ID="ToggleButtonExtender1" runat="server"
    TargetControlID="cbShowArchive" 
    ImageWidth="19" 
    ImageHeight="19"
    CheckedImageAlternateText="Odkljukaj"
    UncheckedImageAlternateText="Obkljukaj"
    UncheckedImageUrl="~/_inc/images/design/checkbox_off.gif" 
    CheckedImageUrl="~/_inc/images/design/checkbox_on.gif"
    DisabledCheckedImageUrl="~/_inc/images/design/checkbox_disabled.gif"
    DisabledUncheckedImageUrl="~/_inc/images/design/checkbox_disabled.gif" />
</div>
<br />
<span class="modPopup_mainTitle" >Slike</span>
<div style="margin-top:7px;">
    <asp:CheckBox ID="cbShowImage" CssClass="modPopup_CBXsettings" runat="server" Text = "&nbsp;&nbsp;Prikaži sliko v seznamu" />
</div>