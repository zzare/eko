﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

/// <summary>
/// Summary description for Content
/// </summary>
public partial class CONTENT
{
    public static void UpdateActive(int conID, bool active )
    {
        eMenikDataContext db = new eMenikDataContext();

        CONTENT cont = (from c in db.CONTENTs
                        where c.CON_ID == conID
                        select c).SingleOrDefault();
        cont.ACTIVE = active;
        db.SubmitChanges();
    }
    public static void UpdateActiveSecure(int conID, bool active, string username)
    {

        eMenikDataContext db = new eMenikDataContext();

        CONTENT cont = (from c in db.CONTENTs
                        join smp in db.SITEMAPs on c.SMAP_ID equals smp.SMAP_ID 
                        join m in db.MENUs on smp.MENU_ID equals m.MENU_ID 
                        where c.CON_ID == conID && m.USERNAME == username 
                        select c).SingleOrDefault();
        cont.ACTIVE = active;
        db.SubmitChanges();
    }

    public static void UpdateActiveToggle(int conID)
    {
        eMenikDataContext db = new eMenikDataContext();

        CONTENT cont = (from c in db.CONTENTs
                        where c.CON_ID == conID
                        select c).SingleOrDefault();
        cont.ACTIVE = !cont.ACTIVE;
        db.SubmitChanges();
    }

    public static void UpdateBody(int conID, string body)
    {
        eMenikDataContext db = new eMenikDataContext();

        CONTENT cont = (from c in db.CONTENTs
                        where c.CON_ID == conID
                        select c).SingleOrDefault();
        cont.CONT_BODY  = body;
        db.SubmitChanges();
    }
    public static void UpdateBody(Guid  cwp, string body)
    {
        eMenikDataContext db = new eMenikDataContext();

        CONTENT cont = (from c in db.CONTENTs where c.CWP_ID == cwp select c).SingleOrDefault();
        cont.CONT_BODY = body;
        db.SubmitChanges();
    }

    public static CONTENT UpdateBody(Guid cwpID, string body, Guid userID)
    {
        eMenikDataContext db = new eMenikDataContext();

        CONTENT cont = (from c in db.CONTENTs
                        from cwp in db.CMS_WEB_PARTs
                        where c.CWP_ID == cwpID && cwp.CWP_ID == c.CWP_ID && cwp.UserId == userID 
                        select c).SingleOrDefault();

        if (cont != null)
        {
            cont.CONT_BODY = body;
            db.SubmitChanges();
        }

        return cont;
    }


    public static CONTENT GetContent(int smpID)
    {
        eMenikDataContext db = new eMenikDataContext();

        CONTENT cont = (from c in db.CONTENTs join cul in db.CULTUREs on c.LANG_ID equals  cul.LANG_ID 
                       where c.SMAP_ID == smpID && c.ACTIVE == true && cul.UI_CULTURE == LANGUAGE.GetCurrentCulture()
                       orderby c.CON_ORDER ascending, c.DATE_ADDED descending, c.CON_ID
                       select c).FirstOrDefault();
        return cont;
    }

    public static CONTENT GetContentByID(int conID, bool hideInactive)
    {
        eMenikDataContext db = new eMenikDataContext();

        IEnumerable<CONTENT> cont = (from c in db.CONTENTs
                        join cul in db.CULTUREs on c.LANG_ID equals cul.LANG_ID
                        where c.CON_ID  == conID && cul.UI_CULTURE == LANGUAGE.GetCurrentCulture()
                        select c);
        if (hideInactive)
            cont = cont.Where(w=> w.ACTIVE == true );

        return cont.SingleOrDefault();
    }
    public static CONTENT GetContentByCwpID(Guid cwpID)
    {
        eMenikDataContext db = new eMenikDataContext();

        CONTENT cont = (from c in db.CONTENTs where c.CWP_ID == cwpID select c).SingleOrDefault();

        return cont;
    }

    public static CONTENT GetContentByCwpID(Guid cwpID, Guid  userID)
    {
        eMenikDataContext db = new eMenikDataContext();

        CONTENT cont = (from c in db.CONTENTs 
                        from cwp in db.CMS_WEB_PARTs 
                        where  c.CWP_ID == cwpID  && cwp.CWP_ID == c.CWP_ID && cwp.UserId == userID 
                        select c).SingleOrDefault();
        return cont;
    }

    public static CONTENT GetFirstContentByPageName(string pname)
    {
        eMenikDataContext db = new eMenikDataContext();

        CONTENT cont = (CONTENT)(from u in db.EM_USERs
                                 from uc in db.EM_USER_CULTUREs
                                 from c in db.CULTUREs
                                 from m in db.MENUs
                                 from sml in db.SITEMAP_LANGs
                                 from s in db.SITEMAPs
                                 from cwp in db.CMS_WEB_PARTs
                                 from con in db.CONTENTs
                                 where u.PAGE_NAME == pname && uc.UserId == u.UserId && uc.DEFAULT == true && uc.ACTIVE == true && c.LCID == uc.LCID
                                 && m.USERNAME == u.USERNAME & s.MENU_ID == m.MENU_ID && s.SMAP_ACTIVE == true && sml.SMAP_ID == s.SMAP_ID && sml.LANG_ID == c.LANG_ID && cwp.REF_ID == s.SMAP_GUID && sml.SMAP_ID == s.SMAP_ID && cwp.LANG_ID == sml.LANG_ID
                                 && con.CWP_ID == cwp.CWP_ID && cwp.ACTIVE == true && cwp.DELETED == false
                                 orderby c.ORDER, s.SMAP_ORDER, cwp.ORDER
                                 select con).FirstOrDefault();

        return cont;
    }
    public static CONTENT GetFirstContentBySitemap(int smap, string lang, bool random)
    {
        eMenikDataContext db = new eMenikDataContext();

        var cont = (from sml in db.SITEMAP_LANGs
                    from s in db.SITEMAPs
                                 from cwp in db.CMS_WEB_PARTs
                                 from con in db.CONTENTs
                                 where sml.SMAP_ID == smap  && sml.LANG_ID == lang && cwp.REF_ID == s.SMAP_GUID && s.SMAP_ID == sml.SMAP_ID && cwp.LANG_ID == sml.LANG_ID
                                 && con.CWP_ID == cwp.CWP_ID && cwp.ACTIVE == true && cwp.DELETED == false
                                 
                                 select con);

        if (random)
            cont = cont.OrderBy(w => Guid.NewGuid());
        else
            cont = cont.OrderBy(w => w.CMS_WEB_PART.ORDER);


        return (CONTENT)cont.FirstOrDefault();
    }




    public static void  DeleteContentBySitemapID(int smpID)
    {
        eMenikDataContext db = new eMenikDataContext();

        IQueryable <CONTENT> cont = (from c in db.CONTENTs where c.SMAP_ID == smpID select c);
        db.CONTENTs.DeleteAllOnSubmit(cont);


        IQueryable<CONTENT> contCwp = (from c in db.CONTENTs
                              from cwp in db.CMS_WEB_PARTs 
                              from smp in db.SITEMAPs 
                              where smp.SMAP_ID == smpID && cwp.REF_ID == smp.SMAP_GUID && c.CWP_ID == cwp.CWP_ID  select c);
        db.CONTENTs.DeleteAllOnSubmit(contCwp);

        db.SubmitChanges();
    }


    public static IEnumerable<LANGUAGE> GetSmapLangsNew(int id)
    {
        eMenikDataContext db = new eMenikDataContext();
        IEnumerable<LANGUAGE> smap;

        smap = from l in db.LANGUAGEs
               where !(from sml in db.SITEMAP_LANGs where sml.SMAP_ID == id select sml.LANG_ID).Contains(l.LANG_ID)
               select l;
        return smap;
    }

    public static IEnumerable<LANGUAGE> GetLanguagesAll()
    {
        eMenikDataContext db = new eMenikDataContext();
        IEnumerable<LANGUAGE> smap;

        smap = from l in db.LANGUAGEs orderby l.LANG_ORDER 
               select l ;
        return smap;
    }

    partial void OnValidate(System.Data.Linq.ChangeAction action)
    {
        if (this.LANG_ID.Length != 2)
            throw new ArgumentException("Length of LANG_ID must be 2 chars.");    
    }

}
