﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using SM.EM.BLL;


/// <summary>
/// Summary description for Page Module
/// </summary>
public partial class PAGE_MODULE
{

    public static IEnumerable<PAGE_MODULE> GetModules()
    {
        eMenikDataContext db = new eMenikDataContext();
        IEnumerable<PAGE_MODULE> mod;

        mod = from m in db.PAGE_MODULEs select m;
        return mod;
    }

    public static IEnumerable<PAGE_MODULE> GetModulesByType(int type)
    {
        eMenikDataContext db = new eMenikDataContext();
        IEnumerable<PAGE_MODULE> mod;

        mod = from m in db.PAGE_MODULEs where m.MOD_TYPE == type  select m;
        return mod;
    }
    public static IEnumerable<PAGE_MODULE> GetModulesCms()
    {
        eMenikDataContext db = new eMenikDataContext();
        IEnumerable<PAGE_MODULE> mod;

        mod = from m in db.PAGE_MODULEs where m.MOD_TYPE != 2 select m;
        return mod;
    }

    public static IEnumerable<PAGE_MODULE> GetModulesByTypeByMaster(int type, int master)
    {
        eMenikDataContext db = new eMenikDataContext();
        IEnumerable<PAGE_MODULE> mod;

        mod = from m in db.PAGE_MODULEs join mp in db.MASTER_PAGEMs on m.PMOD_ID equals mp.PMOD_ID where mp.MASTER_ID == master && m.MOD_TYPE == type orderby m.PMOD_ORDER , m.EM_COL_NO  select m;
        return mod;
    }


    public static IEnumerable<PAGEM_MOD> GetModulsForPageModuls(int pmodID)
    {
        eMenikDataContext db = new eMenikDataContext();

        return db.PAGEM_MODs.Where(pm => pm.PMOD_ID== pmodID );
    }

    public static void DeleteModulsForPageModule(int pmodID)
    {
        eMenikDataContext db = new eMenikDataContext();
        var pageMods = db.PAGEM_MODs.Where(pm => pm.PMOD_ID == pmodID);

        //db.PAGEM_MODs.AttachAll(pageMods);
        db.PAGEM_MODs.DeleteAllOnSubmit(pageMods);
        db.SubmitChanges();
    }

    public static PAGE_MODULE  GetPageModuleByMasterByID(int masterid, int id)
    {
        eMenikDataContext db = new eMenikDataContext();
        PAGE_MODULE mod;

        mod = (from mp in db.MASTER_PAGEMs join m in db.PAGE_MODULEs on mp.PMOD_ID equals m.PMOD_ID  where mp.PMOD_ID == id && mp.MASTER_ID == masterid  select m).SingleOrDefault();
        return mod;
    }
    public static PAGE_MODULE GetPageModuleByByID(int id)
    {
        // check cache
        if (SM.EM.Globals.Settings.ST.EnableCaching)
        {
            // if user in cache, return it
            if (BizObject.Cache[SM.EM.Caching.Key.PageModuleID(id)] != null)
                return (PAGE_MODULE)BizObject.Cache[SM.EM.Caching.Key.PageModuleID(id)];
        }
        

        eMenikDataContext db = new eMenikDataContext();
        PAGE_MODULE mod;
        mod = (from m in db.PAGE_MODULEs where m.PMOD_ID == id select m).SingleOrDefault();

        // cache
        BizObject.CacheData(SM.EM.Caching.Key.PageModuleID(id), mod);

        return mod;
    }

    public static PAGE_MODULE GetPageModuleBySitemap(int smap, Guid  userid)
    {
        // check cache
        if (SM.EM.Globals.Settings.ST.EnableCaching)
        {
            // if user in cache, return it
            if (BizObject.Cache[SM.EM.Caching.Key.PageModule(smap  )] != null)
                return (PAGE_MODULE )BizObject.Cache[SM.EM.Caching.Key.PageModule(smap)];
        }


        eMenikDataContext db = new eMenikDataContext();
        PAGE_MODULE  mod;


        //SM.HelperClasses.TraceTextWriter traceWriter = new SM.HelperClasses.TraceTextWriter();
        //db.Log = traceWriter; 
        mod = (from m in db.PAGE_MODULEs 
               from s in db.SITEMAPs 
               from mu in db.MENUs 
               from em in db.EM_USERs 
               where s.SMAP_ID == smap && m.PMOD_ID == s.PMOD_ID && mu.MENU_ID == s.MENU_ID && em.USERNAME == mu.USERNAME && em.UserId == userid 
               select m).SingleOrDefault();

        // cache
        BizObject.CacheData(SM.EM.Caching.Key.PageModule(smap), mod);


        return mod;
    }

    public class Common {

        public const int PRODUCT_ADDITIONAL = 51;
    
    
    }

}
