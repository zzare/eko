﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SM.EM.UI;
using SM.EM.UI.Controls;
using SM.EM;

public partial class ArticleList : SM.EM.UI.BasePage_CmsWebPart
{
    protected Guid CwpID;
    protected CustomPager Pager;

    public string RenderedList = "";

    public DateTime FiltDate { get {
        DateTime ret = DateTime.MinValue;
        if (Request.QueryString["dt"] != null)
            DateTime.TryParseExact(HttpUtility.UrlDecode(Request.QueryString["dt"]), "dd.MM.yyyy",System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None  , out ret);

        int year = DateTime.Now.Year;
        // get from year if set
        if (ret == DateTime.MinValue && Request.QueryString["y"] != null)
        {
            if (int.TryParse(Request.QueryString["y"], out year))
            {
                if (year < (DateTime.Now.Year - 15) || year > (DateTime.Now.Year + 15))
                    year = DateTime.Now.Year;
            }
        }


        // get from month if set
        if (ret == DateTime.MinValue && Request.QueryString["m"] != null)
        {
            int m = -1;
            if (int.TryParse(Request.QueryString["m"] , out m)){
                if (m > 0 && m <= 12)
                    ret = new DateTime(year, m, 1);
            }
        }



        return ret;
    } }

    public DateTime FiltDateEnd
    {
        get
        {
            DateTime ret = DateTime.MinValue;
            if (Request.QueryString["dte"] != null)
                DateTime.TryParseExact(HttpUtility.UrlDecode(Request.QueryString["dte"]), "dd.MM.yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out ret);

            // get from month if set
            if (ret == DateTime.MinValue && Request.QueryString["m"] != null)
            {
                int m = -1;
                if (int.TryParse(Request.QueryString["m"], out m))
                {
                    if (m > 0 && m <= 12)
                        ret = new DateTime(DateTime.Now.Year, m , 1).AddMonths(1).AddDays(-1);
                }
            }



            return ret;
        }
    }

    public int  FiltSmpID
    {
        get
        {
            int ret = CurrentSitemapID ;
            if (Request.QueryString["fsmp"] != null)
                int.TryParse(Request.QueryString["fsmp"], out ret);

            return ret;
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        Pager = new CustomPager();
        Pager.OrderColumns = "";
        Pager.PageSize = 2;
        Pager.SelectedClass = "pagesel";


        //objArticleList.AutoLoad = false;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        // get CWP id from QS
        if (Request.QueryString["cwp"] != null)
            if (SM.EM.Helpers.IsGUID(Request.QueryString["cwp"]))
                CwpID = new Guid(Request.QueryString["cwp"]);


        // init edit
        //EditArticle = objEditArticle;
        //if (IsModeCMS)
        //{
        //    objArticleList.OnEditArticleClick += new cmsArticleList.OnEditEventHandler(objArticleList_OnEditArticleClick);
        //    objEditArticle.OnArticleListChanged += new EventHandler(objEditArticle_OnArticleListChanged);
        //}

        //objArticleList.PageName = PageName;
        
        // load
        //if (!IsPostBack) {

        //}


    }

    protected override void OnLoadComplete(EventArgs e)
    {
        // load after complete (for proper IsModeCms initialization)
        //if (CwpID != Guid.Empty)
            LoadArticles();
        

        base.OnLoadComplete(e);
    }



    public string RenderDate() {
        string ret = "";

        if (FiltDate == DateTime.MinValue)
            return ret;

        // DAY filter
        if (FiltDate != DateTime.MinValue && FiltDateEnd == DateTime.MinValue)
        {
            ret = string.Format("<h1>Pregled dogodkov za dan: {0}</h1>", SM.EM.Helpers.FormatDate(FiltDate));
        } // from / to
        else if (FiltDate != DateTime.MinValue && FiltDateEnd != DateTime.MinValue)
        {
            ret = string.Format("<h1>Pregled dogodkov za mesec: {0}</h1>", FiltDate.ToString("MMMM"));
        }


        return ret;

    
    }




    void objEditArticle_OnArticleListChanged(object sender, EventArgs e)
    {
        LoadArticles();
    }

    protected void LoadArticles()
    {
        if (CwpID != Guid.Empty)
            LoadArticlesCwp();
        else if(FiltDate != DateTime.MinValue )
            LoadArticlesFiltered();

    }

    protected void LoadArticlesFiltered()
    {
        ArticleRep arep = new ArticleRep();

        // 

        var baseL = arep.db.ARTICLEs.Where(w => w.ACTIVE == true);

        // DAY filter
        if (FiltDate != DateTime.MinValue && FiltDateEnd == DateTime.MinValue)
        {
            DateTime dt = FiltDate.Date;
            baseL = baseL.Where(w => w.RELEASE_DATE != null && w.RELEASE_DATE.Value >= dt && w.RELEASE_DATE.Value < dt.AddDays(1));
        } // from / to
        else if (FiltDate != DateTime.MinValue && FiltDateEnd != DateTime.MinValue)
        {
            DateTime dt = FiltDate;
            DateTime dte = FiltDateEnd;

            baseL = baseL.Where(w => w.RELEASE_DATE != null && w.RELEASE_DATE.Value >= dt && w.RELEASE_DATE.Value < dte.AddDays(1));
        }

        // int displayType = CMS_WEB_PART.Common.DisplayType.ART_SPORED;
        int pageSize = 20;
        bool pageEnable = true;




        IQueryable<Data.ArticleData> list = null;

        // try cwp
        if (CwpID != Guid.Empty)
        {
            list = arep.GetArticlesByCwp(CwpID, IsModeCMS);
        }
        else if (FiltSmpID > 0)
        {
            list = arep.GetArticleListHierarchyWithImage(FiltSmpID, LANGUAGE.GetCurrentLang(), false, em_user.UserId, baseL, SM.BLL.Common.ImageType.ArticleThumbDefault);
        }

        objArticleList.Visible = false;

        //if (displayType == CMS_WEB_PART.Common.DisplayType.ART_SPORED) // ART_TEKMOVANJA
        //{



        //    RenderedList = ViewManager.RenderView("~/_ctrl/module/article/_viewArticleListSpored.ascx", list, new object[] { CwpID, em_user, LANGUAGE.GetCurrentLang(), CurrentSitemapID, false, pageEnable, pageSize, true });
        //}
        //else 
        {
            objArticleList.Visible = true;

            objArticleList.CwpID = CwpID;
            objArticleList.IsModeCMS = false;
            objArticleList.PageNum = 1;
            objArticleList.PageSize = pageSize;
            objArticleList.CurrentSitemapID = CurrentSitemapID;
            objArticleList.em_user = em_user;

            {
                objArticleList.ShowDesc = true;
                objArticleList.ShowImage = true;
                objArticleList.ImgTypeID = SM.BLL.Common.ImageType.ArticleThumbDefault;
            }

            objArticleList.Data = list;
            objArticleList.BindData();
        }


    }


    protected void LoadArticlesCwp() {
        objArticleList.CwpID = CwpID;
        objArticleList.IsModeCMS = IsModeCMS;
        objArticleList.PageNum = 1;
        objArticleList.PageSize = 100;
        objArticleList.CurrentSitemapID = CurrentSitemapID;
        objArticleList.em_user = em_user;

        CWP_ARTICLE cwpA = CMS_WEB_PART.GetCwpArticleByID(CwpID );
        if (cwpA != null)
        {
            objArticleList.ShowDesc = true;
            objArticleList.ShowImage = cwpA.SHOW_IMAGE;
            objArticleList.ImgTypeID = cwpA.IMG_TYPE_THUMB;
        }

        ArticleRep arep = new ArticleRep();

        var list = arep.GetArticlesByCwp(CwpID, IsModeCMS);

        // set pager TOTAL COUNT
        Pager.TotalCount = list.Count();

        objArticleList.Data = list.Skip(Pager.PageSize * (Pager.GetCurrentPage() - 1)).Take(Pager.PageSize);  
        objArticleList.BindData();

    
    }

    protected override void SetRelCanonical()
    {
        // base.SetRelCanonical();
        RelCanonical = Rewrite.EmenikUserUrlGetLeftPart(this.em_user) + Rewrite.EmenikUserArticleArchiveUrl (this.PageName, CurrentSitemapID, CwpID ).TrimStart("~".ToCharArray());
    }


    //void objArticleList_OnEditArticleClick(object sender, SM.BLL.Common.ClickIDEventArgs e)
    //{
    //    CwpEdit edit = EditArticle;
    //    if (edit == null)
    //        return;
    //    edit.IDref = e.ID;
    //    edit.CwpID = CwpID;
    //    //edit.cwp = cwp;
    //    edit.LoadData();


    ////    // find edit
    ////    CwpEditArticle edit = SM.HelperClasses.FindControl2<CwpEditArticle>(Page, "objEditArticle");
    ////    if (edit == null)
    ////        return;
    ////    edit.ArticleID = e.ID;
    ////    edit.CwpID = cwp.CWP_ID;
    ////    edit.cwp = cwp;
    ////    edit.LoadData();
    //}
}
