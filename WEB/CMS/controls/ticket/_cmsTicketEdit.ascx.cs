﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

namespace SM.UI.Controls
{
    public partial class _cmsTicketEdit : BaseControl 
    {
        public event EventHandler OnUserChanged;
        public event EventHandler OnCancel;

        public Guid TicketID {
            get
            {
                Guid res = Guid.Empty;
                if (ViewState["TicketID"] == null) return res;
                if (!SM.EM.Helpers.IsGUID(ViewState["TicketID"].ToString())) return res;

                return new Guid(ViewState["TicketID"].ToString());
            }
            set{
                ViewState["TicketID"] = value;
            }
        }


        protected short NewStatus;

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {

            }

        }





        // bind data
        public void BindData() {
            this.Visible = true;



            // load ddl
            if (ddlStatus.Items.Count <= 0)
            {
                ddlStatus.DataSource = STATUS.GetActiveStatusesByType(STATUS.Common.Type.TicketOrder);
                ddlStatus.DataValueField = "StatusID";
                ddlStatus.DataTextField = "Title";
                ddlStatus.DataBind();

            
            }


            if (TicketID == Guid.Empty)
            {
                //LoadDefaultValues();
            }
            else {
                LoadData();
            
            }


        }

        // LOAD Question
        protected void LoadData() {


            TICKET tic = TICKET.GetTicketByID (TicketID);

            litTitle.Text = tic.Title;
            litDesc.Text = tic.Desc;
            tbOrderRefID.Text = TICKET.Common.RenderTicketRef( tic.TicketRefID);

            lnkUser.HRef = Page.ResolveUrl(EM_USER.Common.RenderCMSHref(tic.UserId ));
            lnkUser.InnerHtml = EM_USER.GetUserByID(tic.UserId).USERNAME ;

            ddlStatus.SelectedValue = tic.StatusID.ToString();

            objFileList.RefID = tic.TicketID;
            objFileList.BindData();


            // status
            //litStatus.Text = tic.StatusID.ToString() ;


            panEdit.Visible = true;
        }

        public void Save() {
            
            // validation
            Page.Validate("tcktedit");
            if (!Page.IsValid) return;

            
            eMenikDataContext db = new eMenikDataContext();

            TICKET tic = db.TICKETs.SingleOrDefault(w => w.TicketID == TicketID);

            if (tic == null)
                return;

            tic.StatusID  = int.Parse( ddlStatus.SelectedValue);

            //usr.ACTIVE_USER = cbActive.Checked;
            //usr.ACTIVE_WWW = cbActiveWWW.Checked;
            //usr.IS_DEMO = cbDemo.Checked;
            //usr.IS_EXAMPLE_TEMPLATE = cbExampleTemplate.Checked;
            //usr.EMAIL = tbEmail.Text;

            //usr.PORTAL_ID =(short) int.Parse( ddlPortalID.SelectedValue) ; 

            //int discount = 0;
            //if (!int.TryParse(tbDiscountPercent.Text, out discount))
            //    discount = 0;
            //usr.DISCOUNT_PERCENT = discount;

            //if (string.IsNullOrEmpty(tbCustomCSS.Text.Trim()))
            //    usr.CUSTOM_CSS_RULES = null;
            //else
            //    usr.CUSTOM_CSS_RULES = tbCustomCSS.Text.Trim();



            // save
            db.SubmitChanges();

            


            // raise changed event
            OnUserChanged (this, new EventArgs());

        }

 
        protected void btSave_Click(object sender, EventArgs e)
        {
            Save();
        }
        protected void btCancel_Click(object sender, EventArgs e)
        {
            //this.Visible = false;
            OnCancel(this, new EventArgs());
            // remove status text
            ParentPage.mp.StatusText = "";

            
        }



    }
}