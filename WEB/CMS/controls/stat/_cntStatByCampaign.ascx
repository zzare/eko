﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_cntStatByCampaign.ascx.cs" Inherits="CMS_controls_stat_cntStatByCampaign" %>
<%@ Register Assembly="System.Web.DataVisualization, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>
    
    



<asp:Chart id="chUsersByCampaign" runat="server" Width="900px"  >
    <Titles><asp:Title Text="REGISTRIRANI UPORABNIKI po mesecih in po kampanjah "></asp:Title></Titles>
    <Legends><asp:Legend Docking="Top"  ></asp:Legend></Legends>
    <chartareas><asp:ChartArea Name="ChartArea1"></asp:ChartArea></chartareas>
</asp:Chart>


<asp:Chart id="chOrders" runat="server" Width="900px"  >
    <Titles><asp:Title Text="NAROČILA po mesecih in po kampanjah "></asp:Title></Titles>
    <Legends><asp:Legend Docking="Top"  ></asp:Legend></Legends>
    <chartareas><asp:ChartArea Name="ChartArea1"></asp:ChartArea></chartareas>
</asp:Chart>

<asp:Chart id="chPayments" runat="server" Width="900px"  >
    <Titles><asp:Title Text="PLAČILA po mesecih in po kampanjah "></asp:Title></Titles>
    <Legends><asp:Legend Docking="Top"  ></asp:Legend></Legends>
    <chartareas><asp:ChartArea Name="ChartArea1"></asp:ChartArea></chartareas>
</asp:Chart>




