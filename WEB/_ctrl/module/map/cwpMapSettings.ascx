﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="cwpMapSettings.ascx.cs" Inherits="cwpMapSettings" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="FredCK.FCKeditorV2" Namespace="FredCK.FCKeditorV2" TagPrefix="FCKeditorV2" %>




<span class="modPopup_mainTitle" >Naziv zemljevida</span>
<div style="float:left;">
    <asp:TextBox ID="tbTitle" class="modPopup_TBXsettings" runat="server"></asp:TextBox>
</div>
<div style="float:left; padding-top:3px;">

    <asp:CheckBox ID="cbShowTitle" CssClass="modPopup_CBXsettings" runat="server" Text = "&nbsp;&nbsp;Prikaži naziv na spletni strani" />
    <cc1:ToggleButtonExtender ID="ToggleEx" runat="server"
        TargetControlID="cbShowTitle" 
        ImageWidth="19" 
        ImageHeight="19"
        CheckedImageAlternateText="Odkljukaj"
        UncheckedImageAlternateText="Obkljukaj"
        UncheckedImageUrl="~/_inc/images/design/checkbox_off.gif" 
        CheckedImageUrl="~/_inc/images/design/checkbox_on.gif"
        DisabledCheckedImageUrl="~/_inc/images/design/checkbox_disabled.gif"
        DisabledUncheckedImageUrl="~/_inc/images/design/checkbox_disabled.gif" />
</div>
<div class="clearing"></div>
<br />


