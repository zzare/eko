﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewFileEditList.ascx.cs" Inherits="_ctrl_module_file_edit_viewFileEditList" %>


<script language = "javascript" type="text/javascript">

    function openAddFiles(id, typ, l) {
        setModalLoaderLoading();
        openModalLoading('Nalagam');
        var _items = new Array();
        Array.add(_items, id);
        Array.add(_items, l);
        Array.add(_items, typ);
        Array.add(_items, "saveAddFiles(_selItems, '" + id + "', '" + l + "');");
        WebServiceCWP.RenderViewAut(_items, 16, onPopupSuccessLoad, onError);
    }


    function deleteProdFile(id) {
        if (confirm('Ali res želite odstraniti to datoteko?') == false)
            return;

        WebServiceCWP.DeleteProductFile(id, onDeleteIDSuccess, onSaveError, id);
    }
    function onDeleteIDSuccess(ret, id) {
        refrFileList = true;
        
        if (ret.Code != 1) {
            setModalLoaderErr(ret.Message, true);
            return;
        }
        $("#" + id).slideUp("slow");
    }

    function saveAddFiles(selItems, pid, lang) {

        var _items = new Array();
        selItems.each(function (i, val) {
            Array.add(_items, $(this).find("input:checkbox").val());
        });

        openModalLoading('Shranjujem');
        WebServiceCWP.AddProductFiles(pid, lang, _items, onAddProductFilesSuccess, onSaveError, pid);

    }

    function onAddProductFilesSuccess(ret, pid) {
        refrFileList = true;

        closeModalLoading();
        closeModalLoader();

        closeModalLoader();
        editProductFile(pid);

        refrFileList = true;
    }
    

 
</script>


<div id='<%= ViewID %>' class="viewFileLangList">

    <input class="hfRefID" type="hidden" value='<%= ProductID %>' />
    <input class="hfFType" type="hidden" value='<%= FileType %>' />
    <input class="hfLangID" type="hidden" value='<%= LangID %>' />

    <div  class="itemFileList"  >
        <asp:ListView ID="lvFileList" runat="server"  InsertItemPosition="None" >
            <LayoutTemplate>
                <asp:PlaceHolder id="itemPlaceholder" runat="server"></asp:PlaceHolder>
            </LayoutTemplate>
                    
            <ItemTemplate>
                <div id='<%# Eval("ProdFileID") %>' class="fplListItem">
                    <%# Eval("Name") %> <%# SM.EM.Helpers.FormatDateLong( Eval("DateAdded") )%>
                    <a href="#" onclick="deleteProdFile('<%# Eval("ProdFileID") %>'); return false">zbriši</a>
                    <br />
                </div>
                
            </ItemTemplate>
        </asp:ListView>	 	        
    </div>

    <div class="buttonwrapperModal">
        <span class="modPopup_mainTitle" >Dodaj datoteko</span>
        <a id="fu_file_upload" href="javascript:void(0)" class="fu_file_upload lbutton lbuttonConfirm" ><span>Naloži NOVO datoteko iz diska</span></a>
    
        <span class="fuLoading" style="display:none; width: 64px">
            <img src='<%= SM.BLL.Common.ResolveUrl("~/_inc/images/animation/loading_animation_liferay.gif") %>' alt="loading..." />
        </span>

        <span style="display:none; color:#666;" class="upload-conf">Datoteka je dodana</span>

    </div> 



    <div class="buttonwrapperModal">
        <a class="lbutton lbuttonConfirm" href="#" onclick="openAddFiles('<%= ProductID %>', <%= FILE.Common.FileType.PRODUCT  %>, '<%= LangID %>'); return false"><span>Dodaj OBSTOJEČO datoteko</span></a>
            
    </div>

</div>