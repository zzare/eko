﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SM.EM.BLL;

namespace SM.EM.UI.Controls
{
    public partial class cwpGalleryImageList : BaseControl_CmsWebPart  
    {

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

        }
        protected void Page_Load(object sender, EventArgs e)
        {

                // edit mode
            if (ParentPage.IsModeCMS)
            {
                //objGalleryList.OnEditClick +=new cmsGalleryImageList.OnEditEventHandler(objGalleryList_OnEditClick); 

            }
        }


        public override void LoadData()
        {
            InitEditMode();
            LoadImages();
        }

        protected void InitEditMode()
        {


            // edit mode
            if (ParentPage.IsModeCMS)
            {
                divEdit.Visible = true;

                // button actions
                //btEdit.HRef = "javascript:onEdit('" + mpeEdit.BehaviorID + "');";
                btShow.HRef = "javascript:onToggleCwpActive('" + btShow.ClientID + "','" + btHide.ClientID + "', '" + cwp.CWP_ID.ToString() + "' ,true);";
                btHide.HRef = "javascript:onToggleCwpActive('" + btShow.ClientID + "','" + btHide.ClientID + "','" + cwp.CWP_ID.ToString() + "', false);";

                // initial hiding of buttons
                if (cwp.ACTIVE)
                {
                    btShow.Attributes["style"] = "display:none";
                    btHide.Attributes["style"] = "display:";
                }
                else
                {
                    btShow.Attributes["style"] = "display:";
                    btHide.Attributes["style"] = "display:none";
                }

            }
            else
            {
                divEdit.Visible = false;
            }

            // title
            //litTitle.Text = cwp.TITLE;
            //divTitleGallery.Visible = cwp.SHOW_TITLE;



        }

        protected void LoadImages()
        {

            objList.IsModeCMS = ParentPage.IsModeCMS;
            objList.Cwp = cwp;
            objList.BindData();


            // init articles
            //objGalleryList.IsEditMode = ParentPage.IsModeCMS;
            //objGalleryList.PageName = ParentPage.PageName;
            //objGalleryList.CwpID = cwp.CWP_ID;
            //objGalleryList.PageSize = cwp.PAGE_SIZE;

            //objGalleryList.BindData();
        
        
        }

        //void objGalleryList_OnEditClick(object sender, SM.BLL.Common.ClickIDEventArgs e)
        //{

        //    CwpEdit edit = ParentPage.EditImage ;
        //    if (edit == null) return;

        //    edit.IDref = e.ID;
        //    edit.CwpID = cwp.CWP_ID;
        //    edit.cwp = cwp;
        //    edit.LoadData();

        //}
        //public void OnNewImageClick(object sender, EventArgs e)
        //{
        //    CwpEdit edit = ParentPage.EditImageNew ;
        //    if (edit == null)
        //        return;
        //    edit.IDref = -1;
        //    edit.CwpID = cwp.CWP_ID;
        //    edit.cwp = cwp;
        //    edit.LoadData();

        //}

        //protected void btSettings_Click(object sender, EventArgs e)
        //{
        //    CwpEdit edit = ParentPage.GallerySettings;
        //    if (edit == null) return;

        //    edit.cwp = cwp;
        //    edit.CwpID = cwp.CWP_ID;
        //    edit.Zone = cwp.ZONE_ID;
        //    if (cwp.ID_INT != null)
        //        edit.ID = cwp.ID_INT.Value ;
        //    edit.LoadData();

        //}
    }
}