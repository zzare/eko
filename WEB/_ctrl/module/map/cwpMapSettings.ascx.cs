﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class cwpMapSettings : SM.EM.UI.Controls.CwpEdit  
{


    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public override void LoadData()
    {
        // load data
        tbTitle.Text  = cwp.TITLE;
        //tbPageSize.Text = cwp.PAGE_SIZE.ToString();
        cbShowTitle.Checked = cwp.SHOW_TITLE;


    }

    public bool SaveData() {

        if (!ParentPage.CanSave) return false;

        eMenikDataContext db = new eMenikDataContext();
        CMS_WEB_PART c = db.CMS_WEB_PARTs.SingleOrDefault(w => w.CWP_ID == CwpID);

//        c.TITLE = Server.HtmlEncode(tbTitle.Text);
        c.TITLE = tbTitle.Text;
        c.SHOW_TITLE = cbShowTitle.Checked;

        db.SubmitChanges();

        return true;
    }

    protected void btSaveEdit_Click(object sender, EventArgs e)
    {
        SaveData();
    }
}
