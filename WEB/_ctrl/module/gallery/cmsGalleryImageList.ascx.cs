﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SM.EM.BLL;
namespace SM.EM.UI.Controls
{
    public partial class cmsGalleryImageList : BaseControl
    {
#region Property


        private string containerCssClass = "contArticles";
        public string ContainerCssClass
        {
            get { return this.containerCssClass; }
            set { this.containerCssClass = value; }
        }

        private bool showImage = false;
        public bool ShowImage
        {
            get { return this.showImage; }
            set { this.showImage = value; }
        }
        private int displayCount = 10;
        public int DisplayCount
        {
            get { return this.displayCount; }
            set { this.displayCount = value; }
        }
        private bool isEditMode = false;
        public bool IsEditMode
        {
            get { return this.isEditMode; }
            set { this.isEditMode = value; }
        }

        public Guid  CwpID{get;set;}

        public string PageName {get;set;}

        public int PageNum
        {
            get { int ret =  (int)(ParentPage.GetViewStateValue(this, "PageNum") ?? 1);
            if (ret < 1) ret = 1;
            return ret;
            }
            set { ParentPage.SetViewStateValue (this, "PageNum", value); }
        }
        public int PageSize
        {
            get { int ret =  (int)(ParentPage.GetViewStateValue(this, "PageSize") ?? 10000 );
            if (ret < 1) ret = 10000;
            return ret;

            }
            set { ParentPage.SetViewStateValue (this, "PageSize", value); }
        }


#endregion

        public delegate void OnEditEventHandler(object sender,SM.BLL.Common.ClickIDEventArgs  e);
        public event OnEditEventHandler OnEditClick;


        protected void Page_Load(object sender, EventArgs e)
        {


        }
        // bind data
        public void BindData()
        {

            int pSize = 1000;
            if (!IsEditMode)
                pSize = PageSize;

            if (CwpID != Guid.Empty)
                lvList.DataSource = GALLERY.GetImageListByCwpPaged(CwpID, LANGUAGE.GetCurrentLang(), IsEditMode, PageNum, pSize); 

            lvList.DataBind();

            if (lvList.Items.Count < 1 && !isEditMode) { this.Visible = false; return; }
            else 
                this.Visible = true;


        }

        public string RenderOpenGalleryPopup()
        {
            if (lvList.Items.Count == 0) return "";
            return GALLERY.Common.RenderOpenGalleryPopup(CwpID, 0);
        }



        void objArticleEdit_OnArticleListChanged(object sender, EventArgs e)
        {
            this.BindData();
        }


        protected void lvList_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            if (e.CommandName == "editImage")
            {
                OnEditClick(this, new SM.BLL.Common.ClickIDEventArgs { ID = Int32.Parse(e.CommandArgument.ToString()) });
            }
        }




    }
}
