﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="step_advanced.ascx.cs" Inherits="_em_settings__ctrl_step_advanced" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%--<%@ Register TagPrefix="EM" TagName="AdvancedSettings" Src="~/_ctrl/emenik/_emAdvancedSettings.ascx"  %>--%>
<%@ Register Namespace="SM.UI.Controls" TagPrefix="SM"  %>

<SM:InlineScript ID="InlineScript1" runat="server">
    <script type="text/javascript" >


        function SelectRB(cont) {
            $(cont).find("input")[0].checked = true;
        
        }

        function IsDomainTaken() {
            var tbDomain = $('#<%= tbBuyDomain.ClientID %>');

            tbDomain.parent().find('span.fuLoading').css('visibility', 'visible').show();
            WebServiceP.IsDomainTaken(tbDomain.val(), DomainCheckSuccess, DomainCheckError);


        }
        function DomainCheckSuccess(a) {
            $('#spnDomainMsg').html(a).effect("highlight", { color: "#FF6600" }, 1000).parent().find('span.fuLoading').css('visibility', 'hidden').hide();
        }

        function DomainCheckError(a) {
            $('#spnDomainMsg').html('Napaka pri preverjanju domene. Prosim, poskusi ponovno. ').effect("highlight", { color: "#FF6600" }, 1000).parent().find('span.fuLoading').css('visibility', 'hidden').hide();
        }
        


    </script>

</SM:InlineScript>



<h1>Dodatne nastavitve
    <a href="javascript:void(0)" class="btsettStatusHelp"><img src='<%=Page.ResolveUrl("~/_inc/images/icon/help.png") %>' /></a>
    <div class="settStatusWrapp" >
        <div class="settStatus">
            Izberi ime tvoje spletne strani in vnesi domeno, če jo imaš. 
            <br />
            Ko boš končal pritisni gumb "Shrani spremembe" in začni z urejanjem vsebine tvoje spletne strani.
        </div>
    </div>
</h1>

<div style="line-height:150%;">
    <h2>
        Naziv strani
        <a href="javascript:void(0)" class="btsettStatusHelp"><img src='<%=Page.ResolveUrl("~/_inc/images/icon/help.png") %>' /></a>
        <div class="settStatusWrapp" >
            <div class="settStatus">
                <ul>
                <li>
                Naziv je viden v naslovu vsake strani. Lahko je viden tudi v glavi vsake strani
                </li><li>
                Naziv strani je zelo pomemben za spletne iskalnike.
                </li><li>
                Dovoljeno število znakov: 50. Dovoljeni so vsi znaki.
                </li>
                </ul>
            </div>
        </div>
    </h2>
    <asp:TextBox MaxLength="50" CssClass="wizzNav_TBX" ID="tbPageTitle" runat="server"></asp:TextBox>
    <asp:RequiredFieldValidator ValidationGroup="AdvSet" Display="Dynamic" ID="RequiredFieldValidator1" runat="server" ErrorMessage="* Vpiši NAZIV" ControlToValidate="tbPageTitle"></asp:RequiredFieldValidator>
    <i>Primer: Moje podjetje s.p.</i>
    <br />
    <br />


    <h1>Domena</h1>

    <div onclick="SelectRB(this)">

        <h2>
            <asp:RadioButton GroupName="gnDomain" ID="rbPageName" runat="server" />
            Želim imeti poddomeno v okviru sistema <%= SM.BLL.Common.Emenik.Data.PortalHostDomain ()   %>
            <a href="javascript:void(0)" class="btsettStatusHelp"><img src='<%=Page.ResolveUrl("~/_inc/images/icon/help.png") %>' /></a>
            <div class="settStatusWrapp" >
                <div class="settStatus">
                    <ul>
                    <li>
                    Stran je vidna na spletnem naslovu (http://<strong>kratko-ime-strani</strong>.<%= SM.BLL.Common.Emenik.Data.PortalHostDomain ()   %>/).
                    </li><li>
                    Kratko ime strani lahko vsebuje črke (brez šumnikov), številke in pomišljaje (presledki bodo zamenjani s pomišljaji).
                    </li><li>
                    Dovoljeno število znakov: 40
                    </li>
                    </ul>
                </div>
            </div>
        </h2>
        <asp:TextBox MaxLength="40" CssClass="wizzNav_TBX" ID="tbPageName" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator ValidationGroup="AdvSet" Display="Dynamic" ID="rfvPageName" runat="server" ErrorMessage="* Vpiši IME" ControlToValidate="tbPageName"></asp:RequiredFieldValidator>
        <asp:CustomValidator ValidationGroup="AdvSet" ID="cvPageName" runat="server" Display="Dynamic" ErrorMessage="* Stran s tem imenom že obstaja" OnServerValidate="ValidateDuplicate"></asp:CustomValidator>
        <asp:CustomValidator ValidationGroup="AdvSet" ID="cvPageNameValid" runat="server" Display="Dynamic" ErrorMessage="* Kratko ime strani ni pravilne oblike" OnServerValidate="ValidatePageName"></asp:CustomValidator>
        <i>Primer: moje-podjetje</i>
        <br />
        <br />
    </div>


    <div onclick="SelectRB(this)">        
        <h2>
            <asp:RadioButton GroupName="gnDomain" ID="rbCustomDomain" runat="server" />
            Imam lastno domeno, ki jo želim uporabiti
            <a href="javascript:void(0)" class="btsettStatusHelp"><img src='<%=Page.ResolveUrl("~/_inc/images/icon/help.png") %>' /></a>
            <div class="settStatusWrapp" >
                <div class="settStatus">
                Če imaš svojo domeno, jo vpiši. Obvezno moraš nastaviti domenske zapise DNS (record A)
                <br /><br />
                Podatki za preusmeritev:
                <br />
                IP: <%= SM.BLL.Common.Emenik.Data.PortalIP()  %>
                </div>
            </div>
        </h2>
        <asp:TextBox MaxLength="50" CssClass="wizzNav_TBX" ID="tbDomain" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator ValidationGroup="AdvSet" EnableClientScript="false" Display="Dynamic" ID="rfvCustomDomain" runat="server" ErrorMessage="* Vpiši domeno" ControlToValidate="tbDomain"></asp:RequiredFieldValidator>
        <asp:CustomValidator ValidationGroup="AdvSet" ID="cvDomain" runat="server" Display="Dynamic" ErrorMessage="* Domena je v sistemu 1dva3 že zasedena." OnServerValidate="ValidateDuplicateDomain"></asp:CustomValidator>
        <i>Če imaš svojo domeno, jo vpiši. Obvezno moraš popraviti domenske zapise DNS (record A)</i>
        <br />
        <asp:CheckBox ID="cbActivated" runat="server" Text =  "Domena je pravilno preusmerjena in aktivna" Enabled="false" />
        <br />
        * Podatki za preusmeritev:
        <br />
        IP: <%= SM.BLL.Common.Emenik.Data.PortalIP()  %>
        <br />
    </div>
        

    <asp:PlaceHolder ID="phBuyDomain" runat="server">
        <div onclick="SelectRB(this)">
            <h2>
                <asp:RadioButton GroupName="gnDomain" ID="rbBuyDomain" runat="server" />
                Želim naročiti lastno domeno (brezplačno ob plačilu letnega najema)
                <a href="javascript:void(0)" class="btsettStatusHelp"><img src='<%=Page.ResolveUrl("~/_inc/images/icon/help.png") %>' /></a>
                <div class="settStatusWrapp" >
                    <div class="settStatus">
                        <ul>
                        <li>
                            V okviru letnega najema ti pripada lastna domena.
                        </li>
                        <li>
                            Vnesi želeno domeno. Nato preveri, če še ni zasedena.
                        </li>
                        <li>
                            opomba: Preverjanje domene je le informativne narave. Lahko se zgodi, da je zasedena domena prikazana kot prosta.
                        </li>
                        </ul>
                    </div>
                </div>
            </h2>
            <asp:TextBox MaxLength="40" CssClass="wizzNav_TBX" ID="tbBuyDomain" runat="server"></asp:TextBox>
            <a href="#" onclick="IsDomainTaken(); return false"  >preveri razpoložljivost</a>
            <span class="fuLoading" style="display:none; width: 64px">
                <img src='<%= Page.ResolveUrl("~/_inc/images/animation/loading_animation_liferay.gif") %>' alt="preverjam..." />
            </span>
            <i>Primer: moje-podjetje.si</i>
            <br />
            <span id="spnDomainMsg"></span>
            
            <asp:RequiredFieldValidator ValidationGroup="AdvSet" Display="Dynamic" ID="rfvBuyDomain" runat="server" ErrorMessage="* Vpiši želeno domeno" ControlToValidate="tbBuyDomain"></asp:RequiredFieldValidator>
            <asp:CustomValidator ValidationGroup="AdvSet" ID="cvBuyDomainExists" runat="server" Display="Dynamic" ErrorMessage="* Domena je že zasedena" OnServerValidate="ValidateDomain"></asp:CustomValidator>
            
            
            <asp:PlaceHolder ID="phOrderDomain" runat="server">
                <asp:LinkButton ID="btOrderDomain" runat="server" CssClass="lbutton lbuttonEdit" onclick="btOrderDomain_Click"><span>Naroči domeno</span></asp:LinkButton>
            
            </asp:PlaceHolder>
            
            
            <br />
            <br />
        </div>
    
    </asp:PlaceHolder>


      
</div>


<%--<EM:AdvancedSettings id="objSettings" runat="server"></EM:AdvancedSettings>--%>