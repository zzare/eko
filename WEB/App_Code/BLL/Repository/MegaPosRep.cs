﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for MegaPosRep
/// </summary>
public class MegaPosRep
{

    private eMenikDataContext  db = new eMenikDataContext ();

   
    // Query Methods




    public MP_TRAN  GetTransByID(int id)
    {
        return db.MP_TRANs.SingleOrDefault(w => w.TransID  == id);
    }

    public MP_TRAN GetLastTransByOrderID(Guid oid)
    {
        return db.MP_TRANs.Where(w => w.RefID == oid).OrderByDescending(o=>o.TransID ).FirstOrDefault();
    }

    //public COMPANY GetCompanyByID(int id)
    //{
    //    return db.COMPANies.SingleOrDefault(w => w.COMPANY_ID == id);
    //}

    //public ADDRESS GetAddressByID(int id)
    //{
    //    return db.ADDRESSes.SingleOrDefault(w => w.ADDR_ID == id);
    //}


    //public IQueryable<PRODUCT_DESC_WRAP> GetProductDescListByPage(Guid productID, Guid pageID)
    //{
    //    var rec = from uc in db.EM_USER_CULTUREs
    //              from prod in db.PRODUCTs
    //              from c in db.CULTUREs
    //              join p in
    //                  (from p in db.PRODUCT_DESCs where p.PRODUCT_ID == productID select p) on c.LANG_ID equals p.LANG_ID into outer
    //              from pd in outer.DefaultIfEmpty()
    //              from l in db.LANGUAGEs
    //              where prod.PRODUCT_ID == productID && prod.PageId == pageID && prod.PageId == uc.UserId && c.LCID == uc.LCID && l.LANG_ID == c.LANG_ID
    //              orderby uc.DEFAULT descending, l.LANG_ORDER
    //              select new PRODUCT_DESC_WRAP { LANG_ID = l.LANG_ID, PRODUCT_ID = prod.PRODUCT_ID , NAME = pd.NAME, DESCRIPTION_LONG = pd.DESCRIPTION_LONG, DESCRIPTION_SHORT = pd.DESCRIPTION_SHORT };

    //    return (IQueryable<PRODUCT_DESC_WRAP>)rec;
    //}



    //public void DeleteAttr(ATTRIBUTE attr)
    //{

    //    db.ATTRIBUTEs.DeleteOnSubmit (attr );
    //}


    public void AddTrans(MP_TRAN  trans)
    {
        //if (customer.CustomerID == Guid.Empty)
        //    customer.CustomerID = Guid.NewGuid();

        //customer.DATE_REG = DateTime.Now;

        db.MP_TRANs.InsertOnSubmit(trans );
    }

    public void UpdateTrans(MP_TRAN trans, si.megapos.service.TxOperationResult res)
    {
        si.megapos.service.Tx tx = res.tx;
        si.megapos.service.TxInitData txInitData = tx.initData;
        si.megapos.service.TxStateData txStateData = tx.stateData;
        si.megapos.service.TxProcessData txProcessData = txStateData.processData;

        // date
        trans.DateUpdate = DateTime.Now;

        // status
        trans.Status = MegaPos.Common.GetStatusFromTrans(txStateData.type);


        // state data
        trans.StateTimestamp = txStateData.timestamp;
        trans.StateResultType  = txStateData.resultType;
        trans.StateResult  = txStateData.result ;


        //if (customer.CustomerID == Guid.Empty)
        //    customer.CustomerID = Guid.NewGuid();

        //customer.DATE_REG = DateTime.Now;

        //db.CUSTOMERs.InsertOnSubmit(customer );

    }


    //public COMPANY  AddCompany(COMPANY company, ADDRESS address)
    //{

    //    eMenikDataContext dbt = new eMenikDataContext();
    //    dbt.ADDRESSes.InsertOnSubmit(address);
        
    //    dbt.SubmitChanges();
        
    //    company.ADDR_ID = address.ADDR_ID;
    //    dbt.COMPANies.InsertOnSubmit(company);
        
    //    dbt.SubmitChanges();
    //    return company;

    //}


    //
    // Persistence

    public bool  Save()
    {
        try
        {
            db.SubmitChanges();
        }
        catch (Exception ex)
        {
            ERROR_LOG.LogError( ex, "Save MegaPos");

            return false;
        }
        return true;        
    }








}


