﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Net.Mail;
using System.Text;
using System.IO;

public partial class _ctrl_content_membership_ctrl_cntChangePassword : BaseControl_EMENIK 
{
    public bool AutoLogin = true ;
    public bool SendMail = true;



    public event  EventHandler OnRegistrationComplete;

    protected void Page_Load(object sender, EventArgs e)
    {





        if (!IsPostBack) { 

        
        
        }
        
    }

    protected void btChangePass_Click(object o, EventArgs e)
    {
        bool success = ChangePass();

    }


    protected bool ChangePass()
    {


        // validate input
        Page.Validate("chPass");
        if (!Page.IsValid)
            return false;

        // insert membership user

        MembershipUser usr = SM.EM.BLL.Membership.GetMembershipUser(Page.User.Identity.Name);
        if (usr == null)
            return false;

        bool success = usr.ChangePassword(tbOldPassword.Text, tbNewPassword.Text);

        phMsg.Visible = true;
        if (success)
        {
            litMsg.Text = "Geslo je uspešno spremenjeno";
            return true;
        }
        else
        {
            litMsg.Text = "Napaka pri spreminjanju gesla.<br/>";
            litMsg.Text += "Možni vzroki:<br/>";
            litMsg.Text += "- staro geslo ni pravilno vneseno<br/>";
            litMsg.Text += "- novo geslo ni dovolj dolgo<br/>";
        }

        return false;
    
    }



    protected void ValidateCompare(object o, ServerValidateEventArgs args)
    {
        args.IsValid = true;

        // check for duplicate page name
        if (tbNewPassword.Text != tbConfirmPassword .Text )
        {
            args.IsValid = false;
        
        }
        
    }

    protected void ValidateOld(object o, ServerValidateEventArgs args)
    {
        args.IsValid = true;

        // check for duplicate page name
        if (!Membership.ValidateUser(Page.User.Identity.Name , tbOldPassword.Text ))
        {
            args.IsValid = false;

        }

    }





}
