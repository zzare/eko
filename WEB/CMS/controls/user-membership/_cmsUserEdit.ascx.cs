﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

namespace SM.UI.Controls
{
    public partial class user_membership__cmsUserEdit : BaseControl 
    {
        public event EventHandler OnUserChanged;
        public event EventHandler OnCancel;

        public Guid UID {
            get
            {
                Guid res = Guid.Empty;
                if (ViewState["UID"] == null) return res;
                if (!SM.EM.Helpers.IsGUID(ViewState["UID"].ToString())) return res;

                return new Guid(ViewState["UID"].ToString());
            }
            set{
                ViewState["UID"] = value;
            }
        }
        public Guid ProjectID { get; set; }
        public Guid SiteID { get; set; }
        public Guid GroupID { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {

            objPermissionEdit.OnChanged += new EventHandler(objPermissionEdit_OnChanged);
            objPermissionList.OnDelete += new EventHandler(objPermissionList_OnDelete);

            if (!IsPostBack)
            {

            }

        }

        void objPermissionList_OnDelete(object sender, EventArgs e)
        {
            LoadPermissions();
        }

        void objPermissionEdit_OnChanged(object sender, EventArgs e)
        {
            LoadPermissions();
            
        }

        // default values
        protected void LoadDefaultValues() {
            phNew.Visible = true;
            phOld.Visible = false;

            panPermissions.Visible = false ;


            UID = Guid.Empty;

            tbUsernameNew.Text = "";
            tbEmail.Text = "";

            ParentPage.mp.StatusText = "Add NEW USER";

            btDeleteUser.Visible = false;
        }



        // bind data
        public void BindData() {
            this.Visible = true;




            if (UID == Guid.Empty)
            {
                LoadDefaultValues();
            }
            else {
                LoadData();
            
            }
            LoadRoles();

        }

        // LOAD Question
        protected void LoadData() {

            phNew.Visible = false ;
            phOld.Visible = true;

            panPermissions.Visible = true;




            MembershipUser usr = Membership.GetUser(UID);
            tbUserName.Text = usr.UserName;
            tbEmail.Text = usr.Email;
            
            // set status text
            string status = "";
            status = "Managing USER: " + usr.Email ;


            btDeleteUser.Visible = Roles.IsUserInRole("admin");

            // permissions
            LoadPermissions();




        }

        protected void LoadPermissions() {

            // permissions
            objPermissionEdit.UID = UID;
            objPermissionEdit.BindData();


            // list
            objPermissionList.UID = UID;
            objPermissionList.BindData();
        }

        protected void LoadRoles() {
            // load roles if admin
            //if (SM.BLL.BizObject.IsAdmin)
            //{
                List<string> list = Roles.GetAllRoles().ToList();
                if (list.Contains("admin"))
                    if (!Roles.IsUserInRole("admin"))
                        list.Remove("admin");
                
            cblRoles.DataSource = list;
                cblRoles.DataBind();


                foreach (ListItem item in cblRoles.Items)
                {

                    if (Roles.IsUserInRole(tbUserName.Text, item.Value))
                        item.Selected = true;
                }

        }

        public void Save() {
            
            // validation
            Page.Validate("usredit");
            if (!Page.IsValid) return;

            
            //eMenikDataContext db = new eMenikDataContext();

            // add new user

            string username = "";

            if (UID == Guid.Empty)
            {
                MembershipCreateStatus status;
                username = tbUsernameNew.Text;
                MembershipUser usr = Membership.CreateUser(tbUsernameNew.Text, tbPass.Text, tbEmail.Text, "q", "a", true, out status);


                if (status != MembershipCreateStatus.Success)
                {
                    string err = "";
                    switch (status)
                    {
                        case MembershipCreateStatus.DuplicateUserName:
                            err = "Duplicate";
                            break;
                        case MembershipCreateStatus.DuplicateEmail:
                            err = "Duplicate Email";
                            break;
                        case MembershipCreateStatus.InvalidUserName:
                            err = "Uporabniško ime je neveljavno";
                            break;
                    }

                    throw new Exception(err);
                }


            }
            else {

                username = tbUserName.Text;

                MembershipUser usr = Membership.GetUser(tbUserName.Text);
                usr.Email = tbEmail.Text;
                Membership.UpdateUser(usr);
            
            }



            //usr.ACTIVE_USER = cbActive.Checked;
            //usr.ACTIVE_WWW = cbActiveWWW.Checked;
            //usr.IS_DEMO = cbDemo.Checked;
            //usr.IS_EXAMPLE_TEMPLATE = cbExampleTemplate.Checked;
            //usr.EMAIL = tbEmail.Text;


            
            // save roles
            foreach(ListItem  item in cblRoles.Items  ){
                if (item.Selected) {
                    if (!Roles.IsUserInRole(username, item.Value))
                        Roles.AddUserToRole(username, item.Value);
                }
                else if (Roles.IsUserInRole(username, item.Value))
                    Roles.RemoveUserFromRole(username, item.Value);
            }


            // set status text
            ParentPage.mp.StatusText = "User has been SAVED ";

            // remove from cache
//            usr.RemoveFromCache();


            
            // raise changed event
            OnUserChanged (this, new EventArgs());

        }

        // GROUP
        protected void btSave_Click(object sender, EventArgs e)
        {
            Save();
        }
        protected void btCancel_Click(object sender, EventArgs e)
        {
            //this.Visible = false;
            OnCancel(this, new EventArgs());
            // remove status text
            ParentPage.mp.StatusText = "";

            
        }
        protected void btDelete_Click(object sender, EventArgs e)
        {
            if (!Roles.IsUserInRole("admin")) return;

            //USER_DATA.DeleteUserData(UID);


            //Membership.DeleteUser(tbUserName.Text, true);

            // raise changed event
            OnUserChanged(this, new EventArgs());
        }








}
}