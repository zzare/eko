﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="cmsContentEdit.ascx.cs" Inherits="_ctrl_module_content_cmsContentEdit" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="FredCK.FCKeditorV2" Namespace="FredCK.FCKeditorV2" TagPrefix="FCKeditorV2" %>
<%@ Register Namespace="SM.UI.Controls" TagPrefix="SM"  %>





<asp:HiddenField ID="hfEdit" runat="server" />
<asp:Panel CssClass="modPopup" ID="panEdit" runat="server" style="display:none;"  >

    <div class="modPopup_header modPopup_headerEdit">
        <h4>UREDI VSEBINO</h4>
        <p>Uredi vsebino trenutne spletne strani</p>
    </div>
    <div class="modPopup_mainFCK">
        <FCKeditorV2:FCKeditor  Width="100%" ID="fckBody" ToolbarSet="CMS_Content" runat="server" Height="400px"  ></FCKeditorV2:FCKeditor>
    </div>   
    <div class="modPopup_footer"> 
        <div class="buttonwrapperModal">
            <asp:LinkButton ID="btSaveEdit" runat="server" CssClass="lbutton lbuttonConfirm" OnClick="btSaveEdit_Click"><span>Shrani</span></asp:LinkButton>
            <asp:LinkButton ID="btCancelEdit" runat="server" CssClass="lbutton lbuttonDelete"><span>Prekliči</span></asp:LinkButton>
        </div>
    </div>
</asp:Panel>



<cc1:ModalPopupExtender  RepositionMode="RepositionOnWindowResizeAndScroll" Y ="50"  BehaviorID="mpeEditContent"  ID="mpeEditContent" runat="server" TargetControlID="hfEdit" PopupControlID="panEdit"
                        BackgroundCssClass="modBcg" CancelControlID="btCancelEdit" >
</cc1:ModalPopupExtender> 