﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SM.UI.Controls;
using SM.EM.UI.Controls;


public partial class _em_content_order__ctrl__stepPaymentType : BaseWizardStepOrder 
{
    public event EventHandler OnSaveOK;

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        ShortDesc = "Način plačila";
    }


    protected void Page_Load(object sender, EventArgs e)
    {

        rbDelivery.Text ="&nbsp;" +  Resources.Product.step1Rbl1;
        rbPredracun.Text = "&nbsp;" + Resources.Product.step1Rbl2;
        rbOnline.Text = "&nbsp;" + Resources.Product.step1Rbl3;

        cvPayment.ErrorMessage = "* " + Resources.Product.step1Title2;

    }


    public override void LoadData()
    {

        if (Order == null)
            return;
        // load order
        if (Order.PaymentType == SHOPPING_ORDER.Common.PaymentType.DELIVERY)
            rbDelivery.Checked = true;
        else if (Order.PaymentType == SHOPPING_ORDER.Common.PaymentType.ONLINE_e24)
            rbOnline.Checked = true;
        else if (Order.PaymentType == SHOPPING_ORDER.Common.PaymentType.PREDRACUN)
            rbPredracun.Checked = true;


    }

    protected void ValidatePayment(object o, ServerValidateEventArgs args)
    {
        args.IsValid = true;

        // check for duplicate page name
        if (!rbDelivery.Checked && !rbOnline.Checked && !rbPredracun.Checked)
        {
            args.IsValid = false;
            CustomValidator cv = (CustomValidator)o;
            cv.ErrorMessage = "* " + Resources.Product.step1Title2;
        }
    }

    protected void btNext_Click(object o, EventArgs e)
    {

        Page.Validate("valPayment");
        if (!Page.IsValid)
            return;

        // create new order if not exist
        if (Order == null)
            Order = Orep.EnsureOpenOrderByUser(SM.EM.BLL.Membership.GetCurrentUserID(), ParentPageEmenik.em_user.UserId);


        short  paymentType = SHOPPING_ORDER.Common.PaymentType.NOT_SET;
        if (rbDelivery.Checked)
            paymentType = SHOPPING_ORDER.Common.PaymentType.DELIVERY;
        else if (rbOnline.Checked)
            paymentType = SHOPPING_ORDER.Common.PaymentType.ONLINE_e24;
        else if (rbPredracun.Checked)
            paymentType = SHOPPING_ORDER.Common.PaymentType.PREDRACUN;

        Order.PaymentType = paymentType;
        // save
        Orep.Save();    

        // if all ok
        OnSaveOK(this, new EventArgs());
    
    }




}
