﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_cntCommentList.ascx.cs" Inherits="SM.EM.UI.Controls._cntCommentList" %>
    <a id='<%= RenderCommentStart()  %>'></a>
    <asp:ListView ID="listComments" runat="server" onitemcommand="listArticles_ItemCommand">
        <LayoutTemplate>            
                <asp:PlaceHolder ID="itemPlaceholder" runat="server"/>

        </LayoutTemplate>
        
        <ItemTemplate>
            
            <div class="comBox <%#  RenderAuthorClass(Container.DataItem) %>">
                <div class="comText ">
                    <div runat="server"  visible='<%# IsEditMode %>'>
                        
                        <asp:LinkButton Visible='<%# CanEditComment %>' ID="btEdit" CommandName="editComment" CommandArgument='<%# Eval("CommentRefID") %>'   runat="server">uredi</asp:LinkButton>
                        <asp:LinkButton ID="btDelete" OnClientClick="return confirm('Are you sure you want to delete this comment?');" CommandName="deleteComment" CommandArgument='<%# Eval("CommentRefID") %>'   runat="server">zbriši</asp:LinkButton>
                        <asp:LinkButton Visible='<%# CanEditComment &&  !((COMMENT)(Container.DataItem) ).Approved %>' ID="LinkButton1" CommandName="approveComment" CommandArgument='<%# Eval("CommentRefID") %>'   runat="server">potrdi</asp:LinkButton>
                    </div>
                    <%--<p style="color:#006699"><a id="comm<%# Eval("CommentID") %>" href="#comm<%# Eval("CommentID") %>">#</a>&nbsp;<%#  RenderCommentTitle(Container.DataItem) %></p>--%>
                    <div class="cCommDate"><%# RenderCommentAuthor(Container.DataItem)%> | <span class="comDate"><%# RenderDate(Container.DataItem)%></span></div>
                    <p><%#  RenderCommentDesc(Container.DataItem) %></p>
                </div>
                <div class="artImg">
                </div>
                <span class="com_sepa"></span>

            </div>
        </ItemTemplate>
    </asp:ListView>
    
