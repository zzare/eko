﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using SM.EM.BLL;

/// <summary>
/// Summary description for DESIGN
/// </summary>
public partial class TEMPLATES
{
    public static IEnumerable<THEME> GetThemesByMasterActivePublic(int masterID)
    {
        eMenikDataContext db = new eMenikDataContext();
        IEnumerable<THEME> list;

        list = from l in db.THEMEs
               where l.MASTER_ID == masterID && l.IS_PUBLIC == true && l.ACTIVE == true 
               orderby l.ORDER ascending
               select l;
        return list;
    }
    public static IEnumerable<THEME> GetThemesAll()
    {
        eMenikDataContext db = new eMenikDataContext();
        IEnumerable<THEME> list;

        list = from l in db.THEMEs
               from m in db.MASTERs 

               where m.MASTER_ID == l.MASTER_ID 
               orderby m.Master,  l.Theme  ascending
               select l;
        return list;
    }
    public static THEME GetThemeByID(int themeID)
    {
        // check cache
        if (SM.EM.Globals.Settings.ST.EnableCaching)
        {
            // if user in cache, return it
            if (BizObject.Cache[SM.EM.Caching.Key.Theme(themeID)] != null)
                return (THEME)BizObject.Cache[SM.EM.Caching.Key.Theme(themeID)];
        }


        eMenikDataContext db = new eMenikDataContext();
        THEME ret;

        ret = (from l in db.THEMEs where l.THEME_ID == themeID select l).SingleOrDefault();

        // cache
        BizObject.CacheData(SM.EM.Caching.Key.Theme(themeID ), ret); 
        
        return ret;
    }
    public static MASTER  GetMasterByID(int masterID)
    {
        // check cache
        if (SM.EM.Globals.Settings.ST.EnableCaching)
        {
            // if user in cache, return it
            if (BizObject.Cache[SM.EM.Caching.Key.Master(masterID )] != null)
                return (MASTER )BizObject.Cache[SM.EM.Caching.Key.Master(masterID)];
        }


        eMenikDataContext db = new eMenikDataContext();
        MASTER ret;

        ret = (from l in db.MASTERs 
               where l.MASTER_ID  == masterID 
               select l).SingleOrDefault();


        // cache
        BizObject.CacheData(SM.EM.Caching.Key.Master(masterID), ret); 

        return ret;
    }

    public class Common {

        public static int DEFAULT_THEME = 3;
        public static int ANIMAL_THEME = 4;
    
    }

}
