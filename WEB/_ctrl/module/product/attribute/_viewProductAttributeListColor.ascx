﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewProductAttributeListColor.ascx.cs" Inherits="_ctrl_module_product_attribute_viewProductAttributeListColor" %>

<div class="pd_attri_color">
<div class="pd_attri_colorI">
<span class="pd_attri_titlered">UREJANJE BARVNE KOMBINACIJE ARTIKLA</span>
<span class="pd_attri_text">Izberite posamezne barve vašega izdelka</span>

<div id='<%=ViewID %>' class="pd_attri_color_select">



<asp:ListView ID="lvList" runat="server" >
    <LayoutTemplate>
        
            <asp:PlaceHolder ID="itemPlaceholder" runat="server"/>

    </LayoutTemplate>
    
    <ItemTemplate>




        <%# RenderItem(Container.DataItem ) %>
        
        <% if (IsModeCMS){%>
            <span><a class="btEditContwthText" href="#" onclick="editProductAttribute('<%# ProductID %>', '<%# Eval("GroupAttID") %>'); return false" ><span>uredi</span></a></span>
        <%} %>
        <br />
    
    </ItemTemplate>
</asp:ListView>
</div>
</div>
</div>
