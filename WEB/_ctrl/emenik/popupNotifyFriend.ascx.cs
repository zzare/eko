using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Net.Mail;
using SM.EM.BLL;
using SM.UI.Controls;

namespace SM.EM.UI.Controls
{
    public partial class popupNotifyFriend : BaseControl
   {


      protected void Page_Load(object sender, EventArgs e)
      {

      }


        protected void btSaveEdit_Click(object sender, EventArgs e)
        {
            SendNotify();

        }

        public void SendNotify()
        {
            Page.Validate("notify");
            if (!Page.IsValid) {
                Show();
                return;
            }

            EM_USER usr = EM_USER.GetUserByUserName(Page.User.Identity.Name);
            if (usr == null) return;

            // send mail
            MailMessage message = SM.EM.Mail.Template.GetNotifyFriend (tbMessage.Text, usr );
            message.From = new MailAddress(SM.EM.Mail.Account.Narocila.Email, usr.EMAIL );
            message.To.Clear();
            message.To.Add(new MailAddress(tbEmail.Text ));
            message.IsBodyHtml = true;
            message.Subject = "naredi si svojo spletno stran";


            SmtpClient client = new SmtpClient();
            try
            {
                client.Credentials = new System.Net.NetworkCredential(SM.EM.Mail.Account.Narocila.Email, SM.EM.Mail.Account.Narocila.Pass);
                client.Port = 587;//or use 587            
                client.Host = "smtp.gmail.com";
                client.EnableSsl = true;

                client.Send(message);
            }

            catch (Exception ex)
            {
                litError.Text = "Napaka pri pošiljanju emaila";
                Show();
                return;
            }



            Hide();
        }


        public void Show() {
            mpeSendNotifyFriend.Show();
//            this.Visible = true;
  //          objSettings.LoadData();

        }
        public void Hide()
        {
            mpeSendNotifyFriend.Hide();
//            this.Visible = false ;
        }

  }
}