﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _ctrl_module_poll_viewPollCWP : BaseControlView
{

    public object Data;
    public object[] Parameters;
    public CMS_WEB_PART Cwp;
    public string PageName = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        LoadPoll();
    }



    public void LoadPoll() {
        divTitle.Visible = false;

        if (Cwp == null) 
            Cwp = (CMS_WEB_PART ) Data;

        if (Cwp  == null)
            return;

        if (Parameters != null)
        {
            if (Parameters[0] != null)
                IsModeCMS = bool.Parse(Parameters[0].ToString());

        }

        // load title
        litTitle.Text = Cwp .TITLE;
        divTitle.Visible = Cwp .SHOW_TITLE;

    }

    protected override void Render(HtmlTextWriter writer)
    {
        //if (Cwp == null)
        //    return;
        base.Render(writer);
    }



}
