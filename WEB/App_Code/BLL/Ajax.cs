﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using UrlRewritingNet.Web;

/// <summary>
/// Summary description for Ajax
/// </summary>
namespace SM.EM
{

    public class Ajax
    {
        public static class ReturnCode
        {
            public const int OK = 1;
            public const int OK_refresh = 2;
            public const int Failure = 0;
            public const int Error = -1;

        }


        public class ReturnData {
            public int Code { get; set; }
            public string Message { get; set; }
            public object Data { get; set; }
            public object ID { get; set; }        
        
        }







    }

}
