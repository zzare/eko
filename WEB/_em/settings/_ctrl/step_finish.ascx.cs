﻿using System;
using System.Collections;
using System.Collections.Generic ;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SM.EM.UI.Controls;

public partial class _em_settings__ctrl_step_finish : BaseWizardStep
{

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        Description = "Čestitamo.";
        ShortDesc = "Končano";
        StepID = 7;
        this.AllwaysHideSaveButton = true;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        

    }

    public override void LoadData()
    {
        
    }


    public override bool SaveData(int step)
    {
               
        return true;
        
    }

    public override bool Validate()
    {
        return true;
            
    }

}
