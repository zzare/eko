﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _mailTemplates_ctrlOrderCompletedUserMail : System.Web.UI.UserControl
{

    public object Data;
    public object[] Parameters;

    public SHOPPING_ORDER Order { get; set; }
    public EM_USER em_user { get; set; }

    
    protected void Page_Load(object sender, EventArgs e)
    {
        LoadData();
    }


    public void LoadData()
    {

        if (Data == null)
            return;
        Order = (SHOPPING_ORDER)Data;

        if (Parameters != null)
        {
            if (Parameters.Length > 0 &&  Parameters[0] != null)
                em_user = (EM_USER)Parameters[0];

        }

    }
}
