﻿<%@ Page Language="C#" MasterPageFile="~/CMS/CMS.master" AutoEventWireup="true" CodeFile="ManagePageModule.aspx.cs" Inherits="SM.EM.UI.CMS.ManagePageModule" Title="Untitled Page" %>
<%@ Register TagPrefix="EM" TagName="PopupModules" Src="~/CMS/controls/_ctlSelectModules.ascx"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="cph" Runat="Server">
    
    <asp:LinqDataSource ID="ldsModules" runat="server" 
        ContextTypeName="eMenikDataContext" EnableDelete="True" EnableInsert="True" 
        EnableUpdate="True" TableName="PAGE_MODULEs"  OrderBy="MOD_TYPE"
    ondeleted="ldsModules_Deleted" >
    </asp:LinqDataSource>
    
    <asp:LinqDataSource ID="ldsModule" runat="server" 
        ContextTypeName="eMenikDataContext" EnableDelete="True" EnableInsert="True" 
        EnableUpdate="True" TableName="PAGE_MODULEs" Where="PMOD_ID == @PMOD_ID"  oninserted="ldsModule_Inserted" >
        <WhereParameters>
            <asp:ControlParameter ControlID="gvModule" DefaultValue="-1" Name="PMOD_ID" 
                PropertyName="SelectedValue" Type="Int32" />
        </WhereParameters>
    </asp:LinqDataSource>
    
    <asp:LinqDataSource ID="ldsPModules" runat="server" 
        ContextTypeName="eMenikDataContext" EnableDelete="True" EnableInsert="True" 
        EnableUpdate="True" TableName="MODULEs"  >
    </asp:LinqDataSource> 
   

    
    <asp:DetailsView ID="dvModule" runat="server" AutoGenerateRows="False" 
        DataSourceID="ldsModule" DefaultMode="Insert" 
        oniteminserted="dvModule_ItemInserted" DataKeyNames="PMOD_ID" 
        onitemupdated="dvModule_ItemUpdated" 
        ondatabound="dvModule_DataBound"         >
        <Fields>
            <asp:BoundField DataField="PMOD_ID" HeaderText="PMOD_ID" InsertVisible="False" 
                ReadOnly="True" SortExpression="PMOD_ID" />
            <asp:BoundField DataField="PMOD_TITLE" HeaderText="PMOD_TITLE" SortExpression="PMOD_TITLE" />
            <asp:BoundField DataField="PMOD_DESC" HeaderText="PMOD_DESC" SortExpression="PMOD_DESC" />
            <asp:BoundField DataField="WWW_URL" HeaderText="WWW_URL" 
                SortExpression="WWW_URL" />
            <asp:BoundField DataField="CMS_URL" HeaderText="CMS_URL" 
                SortExpression="CMS_URL" />
            <asp:BoundField DataField="PMOD_ORDER" HeaderText="PMOD_ORDER" 
                SortExpression="PMOD_ORDER" />
            <asp:BoundField DataField="MOD_TYPE" HeaderText="TYPE" SortExpression="MOD_TYPE" />
            <asp:BoundField DataField="EM_COL_NO" HeaderText="EM_COL_NO" SortExpression="EM_COL_NO" />
            <asp:BoundField NullDisplayText="0" DataField="THUMB_URL" HeaderText="THUMB_URL" SortExpression="THUMB_URL" />
            <asp:TemplateField HeaderText="Modules">
                <EditItemTemplate>
                    <asp:CheckBoxList ID="cblPmod" runat="server" DataSourceID="ldsPModules" 
                        DataTextField="MOD_TITLE" DataValueField="MOD_ID">
                    </asp:CheckBoxList>
                </EditItemTemplate>
                <ItemTemplate>
                
                    <asp:CheckBoxList ID="cblPmod" runat="server" DataSourceID="ldsPModules" 
                        DataTextField="MOD_TITLE" DataValueField="MOD_ID">
                    </asp:CheckBoxList>
                </ItemTemplate>
            
            </asp:TemplateField>

            <asp:CommandField ShowEditButton="True" ShowInsertButton="True" />
        </Fields>
    </asp:DetailsView>
    
    <asp:GridView ID="gvModule" runat="server" AllowPaging="True" PageSize="20"
        AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="PMOD_ID" 
        DataSourceID="ldsModules" onrowupdated="gvModule_RowUpdated" 
        onselectedindexchanged="gvModule_SelectedIndexChanged" 
        onrowdeleting="gvModule_RowDeleting" >
        <Columns>
            <asp:CommandField ShowDeleteButton="True" ShowSelectButton="True" />
            <asp:BoundField DataField="PMOD_ID" HeaderText="PMOD_ID" ReadOnly="True" SortExpression="PMOD_ID" InsertVisible="False" />
            <asp:BoundField DataField="PMOD_TITLE" HeaderText="PMOD_TITLE"                 SortExpression="PMOD_TITLE" />
            <asp:BoundField DataField="WWW_URL" HeaderText="WWW_URL"                 SortExpression="WWW_URL" />
            <asp:BoundField DataField="CMS_URL" HeaderText="CMS_URL"                 SortExpression="CMS_URL" />
            <asp:BoundField DataField="PMOD_ORDER" HeaderText="PMOD_ORDER"                 SortExpression="PMOD_ORDER" />
            <asp:BoundField DataField="MOD_TYPE" HeaderText="TYPE" SortExpression="MOD_TYPE" />
            <asp:BoundField DataField="EM_COL_NO" HeaderText="EM_COL_NO" SortExpression="EM_COL_NO" />
            <asp:BoundField DataField="THUMB_URL" HeaderText="THUMB_URL" SortExpression="THUMB_URL" />            
        </Columns>
    </asp:GridView>
</asp:Content>



