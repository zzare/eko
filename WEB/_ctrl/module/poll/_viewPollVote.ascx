﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewPollVote.ascx.cs" Inherits="_ctrl_module_poll_viewPollVote" %>


<asp:PlaceHolder ID="phOK" runat="server">


    <asp:ListView ID="lvList" runat="server">
        <LayoutTemplate>
                <asp:PlaceHolder ID="itemPlaceholder" runat="server"/>
        
        </LayoutTemplate>

        <ItemTemplate>
            <p>
                <label class="pollChoice_label">
                    <input class="pollRbtn" type="radio" name="pollchoice<%# Eval("PollID") %>" value='<%# Eval("PollChoiceID") %>' />
                    <span class="pollChoice"><%# Eval("Choice") %></span>
                    <div class="clearing"></div>
                </label>
            </p>
        
        </ItemTemplate>

    </asp:ListView>

    <div class="cVote">
        <%= GetGlobalResourceObject("Modules", "Vote")%>:&nbsp;
        <a href="#" onclick ="votePoll('<%= PollID.ToString() %>', 'F'); return false" ><%= GetGlobalResourceObject("Modules", "women")%></a>
        &nbsp;
        <a href="#" onclick ="votePoll('<%= PollID.ToString() %>', 'M'); return false" ><%= GetGlobalResourceObject("Modules", "men")%></a>
    </div>

</asp:PlaceHolder>

<asp:PlaceHolder ID="phEmpty" runat="server">
    Dodane ni nobene možnosti.
</asp:PlaceHolder>


