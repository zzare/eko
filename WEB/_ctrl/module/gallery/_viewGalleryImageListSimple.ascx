﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewGalleryImageListSimple.ascx.cs" Inherits="SM.EM.UI.Controls._viewGalleryImageListSimple" %>
<%@ Register Namespace="SM.UI.Controls" TagPrefix="SM"  %>


<asp:ListView ID="lvList" runat="server" InsertItemPosition="None"  >
    <LayoutTemplate>
        <div id="itemPlaceholder" runat="server"></div>
            
    </LayoutTemplate>
    
    <ItemTemplate>
    
        <div >
        <% if (IsModeCMS){%>
            <a style="position:absolute; z-index:1000;" class="divGalImgThumb_btnEdit" href="#" onclick="editCustomCrop('<%# Eval("ImageID") %>', '<%# ImageTypeID.ToString() %>'); return false" ></a>
        <%} %>
        <a class='<%# ItemClass %>' rel='<%# ItemRel %>' href='<%# SM.BLL.Common.ResolveUrl ( Eval("UrlBig").ToString() )  %>' title='<%# RenderImageTitle(Container.DataItem)  %>' >
            <%--<span class="Spd_mask"></span>--%>
            <img class='<%# ItemClassImage %>' src='<%# SM.BLL.Common.ResolveUrl ( Eval("UrlThumb").ToString() )  %>' alt='<%# RenderImageTitle(Container.DataItem)  %>'  />
            <%--<div class="plImgTape_nw"></div>
            <div class="plImgTape_se"></div>--%>
        </a>
        </div>
    </ItemTemplate>
</asp:ListView>
