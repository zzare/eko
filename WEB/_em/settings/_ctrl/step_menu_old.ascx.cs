﻿using System;
using System.Collections;
using System.Collections.Generic ;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SM.EM.UI.Controls;

public partial class _em_settings__ctrl_step_menu_old : BaseWizardStep {

    protected int SitemapID
    {
        get
        {
            int smapID = -1;

            // get selected sitemap
            if (tvMenu.SelectedNode != null)
                int.TryParse(tvMenu.SelectedNode.Value, out smapID);

            // if not selected, try viewstate
            if (smapID < 0 && ViewState["smapID"] != null)
                smapID = (int)ViewState["smapID"];

            return smapID;
        }

        set { ViewState["smapID"] = value; }
    }
    public int ParentID
    {
        get
        {
            int res = -1;
            if (ViewState["PARENT"] == null) return res;
            if (!int.TryParse(ViewState["PARENT"].ToString(), out res)) return res;

            return res;
        }
        set
        {
            ViewState["PARENT"] = value;
        }
    }

    protected string GetError {
        get
        {
            if (Request.QueryString["e"] == null)
                return "";
            string ret = "";
            switch (Request.QueryString["e"])
            {
                case "1":
                    ret = "Napaka. Za izbran jezik ni nobene prikazane podstrani. Za prikazovanje mora biti določena vsaj ena podstran.";
                    break;
                
            }
            return ret;
        
        
        }
        
    
    }

    protected bool IsFirstLoad { get { return (tvMenu.SelectedNode != null); } }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        //Description = "Uredite navigacijo spletne strani. Urejanje navigacije spletne strani je razdeljeno na tri (3) dele.";
        //Description += "<br /> 1. <b>Navigacijska struktura</b> (Kliknite na želeno navigacijsko postavko za urejanje imena za vse izbrane jezike ter za določitev tipa strani) ";
        //Description += "<br /> &nbsp;&nbsp;&nbsp;- <b>Urejanje menuja: </b> Za vsak izbrani jezik določite \"naziv navigacijske postavke\" in ali naj se navigacijska postavka za dotični jezik prikazuje na spletni predstavitvi. ";
        //Description += "<br /> &nbsp;&nbsp;&nbsp;- <b>Tip postavitve na spletni strani: </b> Izberite tip postavitve (število stolpcev) izbrane navigacijske postavke ";
        //Description += "<br /> 2. <b>Dodajanje nove navigacijske postavke</b> (Za dodajanje nove navigacijske postavke kliknite gumb \"Dodaj postavko\") ";
        //Description += "<br /> 3. <b>Vrstni red postavk navigacije</b> (Vrstni red postavk navigacije uredite tako da kliknete na gumb \"Uredi vrstni red\") ";
        Description = "<ul><li>";
        Description += "V tem koraku boste uredili navigacijo na vaši spletni strani. To pomeni, da boste poljubno uredili in dodali podstrani kot so o nas, blog, galerija... ";
        Description += "</li><li>";
        Description += "UREJANJE PODSTRANI: Na levi strani lahko urejate navigacijsko strukturo. To pomeni, da lahko urejate že ustvarjene podstrani. Urediš lahko ime podstrani, njen prevod v tuj jezik* in postavitev strani. ";
        Description += "</li><li>";
        Description += "DODAJANJE NOVE PODSTRANI: V srednjem delu boste lahko dodali novo podstran. To storite da kliknete na \"dodaj postavko\" in vpišete ime nove podstrani ter kliknete \"dodaj\". ";
        Description += "</li><li>";
        Description += "UREJANJE VRSTNEGA REDA: Na desni strani pa je del namenjen za urejanje vrstnega reda vaših podstrani. Za urejanje kliknite na gumb \"Uredi vrstni red\" in potem s sistemom povleci & spusti urediš vrstni red strani (urejanje vrstnega reda spreminja vrstni red podstrani tudi na tvoji spletni strani). ";
        Description += "</li><li>";
        Description += "*če imaš omogočeno večjezičnost (drugi korak čarovnika) ";
        Description += "</li></ul>";
        ShortDesc = "Uredi meni"; 

        StepID = 3;
        AllwaysHideSaveButton = true; 
    }

    protected void Page_Load(object sender, EventArgs e)
    {


        if (!IsPostBack) {
            ErrorText = GetError;
        }
    }

    void objSort_OnSortChanged(object sender, EventArgs e)
    {
        // remove menu cache for current user
        SM.EM.Caching.RemoveEmenikUserMenuCacheKey(ParentPageEmenik.PageName);

        // redirect to same page
        Response.Redirect(SM.EM.Rewrite.EmenikUserSettingsUrlByStep(ParentPageEmenik.PageName, StepID));
    }

    public override void LoadData()
    {
        LoadDataTreeview(tvMenu, -1);
        //QUESTION.LoadDataTreeview(tvQuestions, QID == Guid.Empty ? "" : QID.ToString(), true, CategoryID, Active, false);

        LoadMenu();
    }

    protected void LoadMenu() {


        if (IsFirstLoad)
        {
            // load menu descriptions
            lvList.DataSource = SITEMAP.GetSitemapLangsBySitemap(SitemapID);
            lvList.DataBind();

            panEdit.Visible = true;


            // load page modules
            lvPageMod.DataSource = PAGE_MODULE.GetModulesByTypeByMaster(SM.BLL.Common.PageModuleTypes.Emenik_User, ParentPageEmenik.em_user.MASTER_ID.Value  );
            lvPageMod.DataBind();

            // load sitemap data
            SITEMAP smap = SITEMAP.GetSiteMapByID(SitemapID);
            tbCustomClass.Text = smap.CUSTOM_CLASS ?? "";
            phCustom.Visible = SM.EM.Security.Permissions.IsAdvancedCms();

            divPageMod.Visible = true;
        }
        else {
            panEdit.Visible = false;
            divPageMod.Visible = false;
        }

    }

    public override bool SaveData(int step)
    {
        // if is demo, can't save
        if (!CheckCanSave())
            return false;


        UpdateUserStep(step);

        eMenikDataContext db = new eMenikDataContext();

        SaveMenu();

        if (!ParentPageEmenik.em_user.ACTIVE_USER) step++;

        Response.Redirect(SM.EM.Rewrite.EmenikUserSettingsUrlByStep(ParentPageEmenik.PageName, step));

        return true;
        
    }

    protected void SaveMenu() {

        int i = 0;
        // save menu changes
        foreach (ListViewItem item in lvList.Items)
        {
            DataKey currentDataKey = lvList.DataKeys[i];

            TextBox tbTitle = item.FindControl("tbTitle") as TextBox;
            CheckBox cbActive = item.FindControl("cbActive") as CheckBox;

            // update sitemap lang
            TextBox tbDesc = item.FindControl("tbDesc") as TextBox;

            if ( tbDesc != null &&  SM.EM.Security.Permissions.IsAdvancedCms())
            {
                
                //MENU.ChangeSitemapLang(int.Parse(currentDataKey["SMAP_ID"].ToString()), tbTitle.Text, tbDesc.Text,  currentDataKey["LANG_ID"].ToString(), cbActive.Checked);
            }
            else
                MENU.ChangeSitemapLang(int.Parse(currentDataKey["SMAP_ID"].ToString()), tbTitle.Text, currentDataKey["LANG_ID"].ToString(), cbActive.Checked);

            i++;
        }
        //        db.SubmitChanges();

        // save page module
        i = 0;
        foreach (ListViewItem item in lvPageMod.Items)
        {
            DataKey dKey = lvPageMod.DataKeys[i];
            RadioButton rbPageMod = (RadioButton)item.FindControl("rbPageMod");
            if (rbPageMod.Checked)
            {
                SITEMAP.UpdatePageModule(SitemapID, Int32.Parse(dKey["PMOD_ID"].ToString()));
                break;
            }
            i++;

        }

        // save sitemap data
        SITEMAP.UpdateCustomClass(SitemapID, tbCustomClass.Text);  


        // remove menu cache for current user
        SM.EM.Caching.RemoveEmenikUserMenuCacheKey(ParentPageEmenik.PageName);


    }

    public override bool Validate()
    {
        return true;
            
    }

    protected void tvMenu_SelectedNodeChanged(object sender, EventArgs e)
    {
        // save button
        SitemapID = SitemapID;

        LoadMenu();
        AllwaysShowSaveButton = true;
    }


    // Fill treeview with data
    public  void LoadDataTreeview(TreeView tv, int sel)
    {
        // clear treeview
        tv.Nodes.Clear();

        // load treeview

        eMenikDataContext db = new eMenikDataContext();

        IEnumerable<em_GetSitemapLangHierarchyByUsernameResult> list = db.em_GetSitemapLangHierarchyByUsername(ParentPageEmenik.em_user.USERNAME );

        foreach (em_GetSitemapLangHierarchyByUsernameResult item in list)
        {
            // add roots
            if (item.HLevel == 0)
            {
                TreeNode cat = new TreeNode(item.SML_TITLE, item.SMAP_ID.ToString() );
                tv.Nodes.Add(cat);
                //cat.SelectAction = disableRoot ? TreeNodeSelectAction.None : TreeNodeSelectAction.Select;
                if (item.SMAP_ID == sel)
                {
                    // select current parent node and expand all from root
                    cat.Selected = true;
                }

                // remember parent node
                ParentID = item.PARENT.Value ;
            }
            //  add item
            else if (item.HLevel > 0)
            {
                // find parent node
                string parentValuePath = item.ValuePath.ToString().TrimStart(":".ToCharArray());
                parentValuePath = parentValuePath.Remove(parentValuePath.LastIndexOf(tv.PathSeparator)).ToLower();
                TreeNode parent = tv.FindNode(parentValuePath);
                if (parent != null)
                {
                    // add child node
                    TreeNode node = new TreeNode(SM.EM.Helpers.CutString(item.SML_TITLE, 20, " ..."), item.SMAP_ID.ToString());
                    parent.ChildNodes.Add(node);

                    // select current parent node and expand all from root
                    if (item.SMAP_ID == sel)
                    {
                        node.Selected = true;
                        TreeNode root = SM.EM.Helpers.Treeview.GetRootNodeForNode(node.Parent, tv);
                        root.ExpandAll();
                        node.Collapse();
                    }
                }
            }
        }

    }


    protected void btAdd_OnClick(object sender, EventArgs e)
    {
        // if is demo, can't save
        if (!CheckCanSave())
            return;


        // check if user has  menus left
        if (ParentPageEmenik.em_user.SITEMAP_COUNT  <= SITEMAP.GetSiteMapCountByUserName(ParentPageEmenik.em_user.USERNAME))
        {
            cvSitemapCount.IsValid = false;
            cvSitemapCount.ErrorMessage = "* Trenutno imate največje število dovoljenih menijev (" + ParentPageEmenik.em_user.SITEMAP_COUNT.ToString() + "). Če jih želite imeti več, nas prosim kontaktirajte.";
            return;
        }

        // 
        string title = tbTitle.Text.Trim();

        if (string.IsNullOrEmpty(title))
            title = "(nov meni)";



        // add new sitemap and all languages
        eMenikDataContext db = new eMenikDataContext();

        // insert new sitemap 

        SITEMAP s = new SITEMAP();
        SITEMAP r = SITEMAP.GetRootByPageName(ParentPageEmenik.em_user.PAGE_NAME);
        if (r == null) return;

        s.MENU_ID = r.MENU_ID;
        s.PARENT = r.SMAP_ID;
        s.SMAP_GUID = Guid.NewGuid();
        s.PMOD_ID = SM.BLL.Common.PageModule.TwoColumn;  // two column
        s.SMAP_TITLE = title;
        s.SMAP_ACTIVE = true;
        s.WWW_ROLES = "?,*";
        s.CMS_ROLES = "admin, em_cms";
        s.SMAP_ORDER = 100;
        s.WWW_VISIBLE = true;

        db.SITEMAPs.InsertOnSubmit(s);
        db.SubmitChanges();

        // get users languages
        List<LANGUAGE> langs = LANGUAGE.GetEmUsersLanguages(ParentPageEmenik.em_user.UserId).ToList();
        // add new item for each language 
        foreach (LANGUAGE lan in langs){
            MENU.ChangeSitemapLang(s.SMAP_ID, s.SMAP_TITLE, lan.LANG_ID, true);        
        }


        // hide add panel
        cpeNew.Collapsed = true;

        // refresh list
        LoadData();

        // clear text
        tbTitle.Text = "";

        // remove menu cache for current user
        SM.EM.Caching.RemoveEmenikUserMenuCacheKey(ParentPageEmenik.PageName);

        // refresh
        Response.Redirect(SM.EM.Rewrite.EmenikUserSettingsUrlByStep(ParentPageEmenik.PageName, StepID));

    }



    protected void lvPageMod_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType != ListViewItemType.DataItem )
            return;

        RadioButton rbPageMod = (RadioButton)e.Item.FindControl("rbPageMod");
        string script = "SetUniqueRadioButton('lvPageMod.*pageMod',this)";
        rbPageMod.Attributes.Add("onclick", script);

        ListViewDataItem di = (ListViewDataItem )e.Item;
        PAGE_MODULE pm  = (PAGE_MODULE )di.DataItem;
        
        if (pm.PMOD_ID == SITEMAP.GetSiteMapByID(SitemapID).PMOD_ID)
            rbPageMod.Checked = true;
        else
            rbPageMod.Checked = false;;
    }

    // SORT
    protected void btSort_Click(object sender, EventArgs e)
    {
        objSort.ParentID = ParentID ;
        objSort.Language = CULTURE.GetDefaultUserCulture(ParentPageEmenik.em_user.UserId).LANG_ID  ;
        objSort.BindData();
        mpeSort.Show();
    }
 

    protected void btDelete_Click(object sender, EventArgs e) {
        // if is demo, can't save
        if (!CheckCanSave())
            return;


        SITEMAP.DeleteSiteMapByID(SitemapID);
        // remove menu cache for current user
        SM.EM.Caching.RemoveEmenikUserMenuCacheKey(ParentPageEmenik.PageName);

        ParentPageEmenik.mp_em.LoadData();

        // refresh menu
        LoadData();

    }




    protected void btCloseSort_Click(object sender, EventArgs e)
    {

        // if is demo, can't save
        if (!CheckCanSave())
            return;

        // refresh
        Response.Redirect(SM.EM.Rewrite.EmenikUserSettingsUrlByStep(ParentPageEmenik.PageName, StepID));

    }



    protected void btSave_Click(object sender, EventArgs e)
        
    {
        // if is demo, can't save
        if (!CheckCanSave())
            return;


        SaveMenu();

        // refresh
        Response.Redirect(SM.EM.Rewrite.EmenikUserSettingsUrlByStep(ParentPageEmenik.PageName, StepID ));
    }


    protected string RenderEditHref(object o) {
        SITEMAP_LANG item = o as SITEMAP_LANG ;
        string ret = "";
        string cult = "";
        if (item == null)
            return ret;

        // get cultury by lang
        vw_UserCulture  culture = CULTURE.GetUserCultures(ParentPageEmenik.em_user.UserId).FirstOrDefault(w=>w.LANG_ID == item.LANG_ID );
        if (culture == null)
            return CULTURE.GetDefaultUserCulture(ParentPageEmenik.em_user.UserId).UI_CULTURE ;

        cult = culture.UI_CULTURE;

        ret = SM.EM.Rewrite.EmenikUserUrl(ParentPageEmenik.PageName, item.SMAP_ID, item.SML_TITLE, cult);
        return Page.ResolveUrl(ret);
    
    
    }




}
