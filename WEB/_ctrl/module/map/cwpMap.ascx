﻿<%@ Control Language="C#" AutoEventWireup="true"  CodeFile="cwpMap.ascx.cs" Inherits="SM.EM.UI.Controls.cwpMap" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="FredCK.FCKeditorV2" Namespace="FredCK.FCKeditorV2" TagPrefix="FCKeditorV2" %>



<div id='<%= cwp.CWP_ID.ToString() %>' class='<%= "cwp" + (ParentPage.IsModeCMS ? " cwpEdit " : " ")  + CMS_WEB_PART.Common.RenderHiddenClass(cwp) %>'>
    <div id="divEdit" runat="server" class="cwpHeader cwpMove" visible="false" >
                    
        <div class="cwpHeaderRight"> &nbsp;</div>

        <div style="float:right; ">
            <a id="btEdit" href="javascript:void(0);" title="uredi" class="btEdit" onclick="MapEditOpen('<%= cwp.CWP_ID.ToString() %>'); return false;"></a>
<% if (PERMISSION.User.Common.CanShowHideModule()){%>            
            <a id="btShow"  title="prikaži" runat="server" class="btShow"></a>
            <a id="btHide" title="skrij" runat="server" class="btHide"></a>
<%} %>
<% if (PERMISSION.User.Common.CanDeleteModule()){%>
            <a id="btDelete" href="#" title="izbriši" class="btDelete" onclick="deleteEL('<%= cwp.CWP_ID.ToString() %>'); return false" ></a>
<%} %>
<% if (PERMISSION.User.Common.CanEditModuleSettings ()){%>
            <a id="brSettings" title="nastavitve" runat="server" class="btSettings"  onserverclick="btSettings_Click"></a>
<%} %>
        </div>
        <div class="cwpHeaderLeft"> &nbsp;</div>
        <a id="btMove" class="btMove"></a>
        
        <div class="cwpHeaderTitle">
            Zemljevid
            <a href="javascript:void(0)" class="btsettStatusHelp btcwpHeadHelp"><img src='<%=Page.ResolveUrl("~/_inc/images/icon/help.png") %>' /></a>
            <div class="settStatusWrapp cwpHeadHelpWrapp" >
                <div class="settStatus cwpHeadHelp">
                    Zemljevid
                    <br />
                    Z elementom zemljevid lahko enostavno pokažeš na katerokoli lokacijo. 
                    <br /><br />
                    Nastaviš lahko:
                    <br />
                    - naslov<br />
                    - vrsto zemljevida <br />
                    - povečavo (zoom)<br />
                    - velikost<br />
                    - prikazno sporočilo
                </div>
            </div>
        </div>
    </div>
    <div class="clearing"></div>

    <div class='<%= "cMap" + ( ParentPage.IsModeCMS ? " cwpContainer" : "") %>'>

                                                    <div class="AnDe_mapW">
                                                        <div class="AnDe_mapT"></div>
                                                        <div class="AnDe_mapM">
                                                            <div class="AnDe_mapMI">




        <div id="divTitleContent"  class="cwpH1" runat="server">
            <h1><asp:Literal ID="litTitle" runat="server" ></asp:Literal></h1>
        </div>    

        <div class="contMap">
            <%--<div class="cwpMap">--%>
                <%--<div id="map"></div>--%>
            <%--</div>--%>
          <%--  <iframe id="Iframe1" scrolling="no"  marginheight="0" marginwidth="0"   style="border:0; margin:0; padding:0;" src='<%= Page.ResolveUrl(CWP_MAP.Common.RenderMapViewHref(cwp.CWP_ID)) %>'></iframe>--%>
            <iframe id="Iframe2" scrolling="no"  marginheight="0" marginwidth="0" frameborder="0" height="<%= ViewHeight %>"  style=" width:100%; margin:0; padding:0;" ></iframe>
            
        </div>
        
                                                                <div class="clearing"></div>
                                                            </div>
                                                        </div>
                                                        <div class="AnDe_mapB"></div>
                                                     </div>
        

    </div>


<asp:Panel CssClass="modPopup" ID="panSettings" runat="server" style="display:none;">
    <div class="modPopup_header modPopup_headerSettings">
        <h4>UREDI ZEMLJEVID</h4>
        <p>Uredi zemljevid. Določi naslov, lokacijo, velikost zemljevida, zoom in prikazovanje balončka.</p>
        <p>Spremembe bodo uveljavljene po kliku na gumb Shrani spremembe.</p>
    </div>
    <div class="modPopup_main">

<%--        <div class="cwpMap">
            <div id="mapEdit<%= cwp.CWP_ID.ToString() %>"></div>
        </div>--%>
        
        <iframe id="mapE<%= cwp.CWP_ID.ToString() %>" frameborder="0" marginheight="0" marginwidth="0" height="<%= EditHeight %>"  style=" width:100%;   margin:0; padding:0;" ></iframe>
        
        <%--naslov:<input id="tbSrch" type="text" value="Ljubljana" name="address" size="60"/>
        <a href="javascript:void(0)" onclick="showAddress($(this).parent().find('#tbSrch').val()); return false;">Prikaži na zemljevidu</a>
        <br />
        <br />
        Prikazni tekst
        <br />
        
        Naziv:
        <input id="tbTitle" type="text" name="Title" size="30"/>
        <br />
        Naslov:
        <input id="tbAddress" type="text" name="Address" size="60"/>--%>
        

        
    </div>    
    <div class="modPopup_footer">
        <div class="buttonwrapperModal">
            <%--<a href="javascript:void(0)" class="lbutton lbuttonConfirm" onclick="saveMap(); return false;"  ><span>Shrani spremembe</span></a>--%>
            <a href="javascript:void(0)" class="lbutton lbuttonDelete" onclick="MapEditClose(); return false;"  ><span>Prekliči</span></a>
        </div>
    </div>
</asp:Panel>

</div>


<asp:HiddenField ID="hfPopup" runat="server" />
<cc1:ModalPopupExtender RepositionMode="RepositionOnWindowResizeAndScroll" Y ="50"    ID="mpeMapEdit" runat="server" TargetControlID="hfPopup" PopupControlID="panSettings"
                        BackgroundCssClass="modBcg" >
</cc1:ModalPopupExtender> 
 