﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_cmsGalleryEdit.ascx.cs" Inherits="SM.EM.UI.CMS.Controls.CMS_cmsGalleryEdit" %>
<%@ Register Namespace="SM.UI.Controls" TagPrefix="SM" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="CMS" TagName="ImageSelect" Src="~/cms/controls/gallery/_cmsImageSelect.ascx"  %>

<h3>edit gallery</h3>
<div >
    Main image:
    <br />
    <img id="imgMain" runat="server" />
    <asp:HiddenField ID="hfImgID" runat="server" />
    
    <asp:LinkButton ID="btSelectImage" OnClick="btSelectImage_OnClick" runat="server">select image</asp:LinkButton>
    <CMS:ImageSelect ID="objImageSelect" runat="server" />
    
</div>

<div >
    <SM:smListView ID="lvList" DataKeyNames="GAL_ID,LANG_ID" runat="server"  InsertItemPosition="None" >
        <LayoutTemplate>
            <asp:PlaceHolder id="itemPlaceholder" runat="server"></asp:PlaceHolder>
        </LayoutTemplate>
        
        <ItemTemplate>
            <br />
            <asp:HiddenField ID="hfLang" Value='<%#  Eval("LANG_ID")%>' runat="server" />
               
            <strong> <%#  Eval("LANG_DESC")%></strong>
            <br />
            naziv galerije: <asp:TextBox ID="tbTitle" runat="server" MaxLength="256" Text='<%#  Eval("GAL_TITLE")%>'></asp:TextBox>
            <br />
            opis: <asp:TextBox ID="tbDesc" runat="server" MaxLength="512" TextMode="MultiLine" Rows="2" Text='<%#  Eval("GAL_DESC")%>'></asp:TextBox>
        
        </ItemTemplate>
    </SM:smListView>
</div>

<br />

<div>
    edit image types:
    <br />
    
    <SM:smListView ID="lvImageType" runat="server"  InsertItemPosition="None" >
        <LayoutTemplate>
            <asp:PlaceHolder id="itemPlaceholder" runat="server"></asp:PlaceHolder>
        </LayoutTemplate>
        
        <ItemTemplate>
            <div style="display:inline;">
                <%# Eval("TYPE_TITLE") %> [ <%# RenderDimensions(Eval("TYPE_ID"))%> ]
                <br />
            
            
            </div>
        
        </ItemTemplate>
    </SM:smListView>
    
    <br />
    dodaj nov tip:
    <asp:DropDownList ID="ddlType" runat="server" AppendDataBoundItems="true">
        
    </asp:DropDownList>
    <asp:LinkButton OnClick="btAddType_Click" ID="btAddType" runat="server">dodaj NOV TIP</asp:LinkButton>
    
    
    

</div>



<%--<asp:LinkButton ID="btSave" runat="server" onclick="btSave_Click">save</asp:LinkButton>
<asp:LinkButton ID="btCancel" runat="server" onclick="btCancel_Click">cancel</asp:LinkButton>--%>


