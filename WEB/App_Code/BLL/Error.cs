﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Security.Principal;
using System.Web.Caching;
using System.Net.Mail;
using System.Collections.Generic;
using System.Threading;
using System.Net.Mail;

/// <summary>
/// Summary description for Error
/// </summary>

public partial class ERROR_LOG
{
    public static void LogError(Exception ex, string notes) { 

                string user = "";
        string ip = "";

        if (HttpContext.Current != null){

        if (HttpContext.Current.User != null && HttpContext.Current.User.Identity.IsAuthenticated)
            user = HttpContext.Current.User.Identity.Name;
        ip = SM.EM.BLL.BizObject.CurrentUserIP;
        }

        string url = "";
        if (HttpContext.Current != null && HttpContext.Current.Request != null && HttpContext.Current.Request.Url != null)
            url = HttpContext.Current.Request.Url.AbsoluteUri; 
        string trace = ex.StackTrace + "<br/><br/>SOURCE::" + ex.Source + "<br/><br/>Environment.StackTrace::" + Environment.StackTrace ;
        trace += "<br/><br/>ToString::" + ex.ToString() + "<br/><br/>URL::" + url;
        // add browser version
        if (HttpContext.Current != null && HttpContext.Current.Request != null && HttpContext.Current.Request.Browser != null)
        {
            trace += "<br/><br/>BROWSER::" + HttpContext.Current.Request.Browser.Browser;
            trace += "<br/>version::" + HttpContext.Current.Request.Browser.Version ;
            trace += "<br/>platform::" + HttpContext.Current.Request.Browser.Platform;
            trace += "<br/>clr::" + HttpContext.Current.Request.Browser.ClrVersion.ToString();
            trace += "<br/>URL referrer::" + HttpContext.Current.Request.UrlReferrer;
            
        }


        LogError(notes + ex.Message + "<br/><br/> TargetSite::" + ex.TargetSite, trace, user, ip);
    }



    public static void LogError(string message, string trace, string user, string ip) {
        eMenikDataContext db = new eMenikDataContext();

        if (!SM.BLL.Common.Emenik.Data.EnableErrorLogging())
            return;

        try
        {
            ERROR_LOG err = new ERROR_LOG();
            err.DateError = DateTime.Now;
            err.Message = message;
            err.StackTrace = trace;
            if (!string.IsNullOrEmpty(user))
                err.Username = user;
            if (!string.IsNullOrEmpty(ip))
                err.IP = ip;

            db.ERROR_LOGs.InsertOnSubmit(err);
            db.SubmitChanges();
        }
        catch (Exception ex)
        {

        } 


        // send mail
        MailMessage mail = new MailMessage();
        mail.From = new MailAddress("mail@smardt.si");
        mail.To.Add(new MailAddress("zare@smardt.si"));


        mail.Subject = "NAPAKA - " +  SM.BLL.Common.Emenik.Data.PortalHostDomain()  + ":" + message ;
        mail.Body = "user:" + user + " || IP: " + ip + "<br/><br/><br/>" +  trace;
        mail.IsBodyHtml = true;

        SmtpClient client = new SmtpClient();


        try
        {
//            client.Credentials = new System.Net.NetworkCredential(SM.EM.Mail.Account.Send.Email, SM.EM.Mail.Account.Send.Pass);
            client.Port = 587;//or use 587            
            client.Host = "smtp.gmail.com";
            client.EnableSsl = true;
            client.Send(mail);
        }
        catch (Exception ex)
        {
        }


    
    }

    public static ERROR_LOG GetErrorByID(int id) {
        eMenikDataContext db = new eMenikDataContext();

        return db.ERROR_LOGs.SingleOrDefault(w => w.ErrorID == id);
    }

    public static string RenderError(int code) {
        switch (code)
        {
            case 5:
                return "Stran še ni aktivna";
            case 6:
                return "Stran je potekla";
            default:
                return "Stran ne obstaja";
        }
    
    

    }

}
