﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewMenuEdit.ascx.cs" Inherits="_ctrl_menu_edit_viewMenuEdit" %>




<div id="cEditMenu">    
    <asp:ListView ID="lvList" runat="server" InsertItemPosition="None" >
        <LayoutTemplate>
            <table id="tbEditMenu"  class="wizzNav_TBum">
                    <thead>
                        <tr id="headerSort" runat="server">
                            <th class="wizzNav_TBum_th1">Jezik</th>
                            <th >
                                Naziv
   	                            <a href="javascript:void(0)" class="btsettStatusHelp"><img src='<%=SM.BLL.Common.ResolveUrl ("~/_inc/images/icon/help.png") %>' /></a>
                                <div class="settStatusWrapp" >
                                <div class="settStatus">
                                Uredi tekst ki bo prikazan v meniju in v naslovu strani.
                                </div></div>
                            </th>
                            <th >
                                Prikazovanje
    	                        <a href="javascript:void(0)" class="btsettStatusHelp"><img src='<%=SM.BLL.Common.ResolveUrl ("~/_inc/images/icon/help.png") %>' /></a>
                                <div class="settStatusWrapp" >
                                <div class="settStatus">
                                Z obkljukanjem možnosti poveš v katerih jezikih želiš to podstran prikazovati v meniju.
                                </div></div>
                            </th>
                            <th ></th>
                            <th >
                                <%= (SM.EM.Security.Permissions.IsAdvancedCms()) ? "Opis" :"" %> 
                            </th>
                            
                        </tr>
                    </thead>
                    <tbody id="itemPlaceholder" runat="server"></tbody>
                </table>
        </LayoutTemplate>
        
        <ItemTemplate>
            <tr class="top" >
                <td>
                     <input type="hidden" value= '<%#  Eval("LANG_ID")%>' />
                    <%#  Eval("LANGUAGE.LANG_DESC")%>
                </td>
                <td class="wizzNav_TBum_td1">
                    <input style="width:160px" class="tbTitle wizzNav_TBX "  name="tbTitle" maxlength="40"  type="text" value='<%#  Eval("SML_TITLE")%>' />
                    <%--<asp:TextBox ID="tbTitle" class="wizzNav_TBX" runat="server" MaxLength="40" Width="160px" Text='<%#  Eval("SML_TITLE")%>'></asp:TextBox>--%>
                </td>
                <td>
                    <label  for="cbActiveM<%# Container.DataItemIndex %>"> <input class="wizzFootCbx" id="cbActiveM<%# Container.DataItemIndex %>"  name="cbActive"  type="checkbox" <%# ( ((bool) Eval("SML_ACTIVE")) ? "checked" : "") %> />&nbsp;&nbsp;&nbsp;Prikaži v meniju&nbsp;</label>
                    <%--<asp:CheckBox ID="cbActive" runat="server" Checked='<%# Eval("SML_ACTIVE") %>' Text="&nbsp;&nbsp;&nbsp;Prikaži v meniju&nbsp;"  CssClass="wizzFootCbx" />--%>
                </td>
                <td>&nbsp;&nbsp;
                    <a href='<%# RenderEditHref(Container.DataItem) %>' >uredi vsebino</a>
                </td>
            <% if (SM.EM.Security.Permissions.IsAdvancedCms()) {%>
                <td colspan="3" class="wizzNav_TBum_td1">
                    <input style="width:260px" class="wizzNav_TBX "  name="tbDesc" maxlength="100"  type="text" value='<%#  Eval("SML_DESC")%>' />
                    
                    <%--<asp:TextBox ID="tbDesc" class="wizzNav_TBX" runat="server" MaxLength="100" Width="260px" Text='<%#  Eval("SML_DESC")%>'></asp:TextBox>--%>
                </td>
            <%} %>
            </tr>  

            <% if (SM.EM.Security.Permissions.IsAdvancedCms()) {%>
            <tr>
                <td style="text-align:right;">
                    Desc:
                </td>
                <td colspan="7">
                    
                    <input style="width:95%" class="tbTitle wizzNav_TBX "  name="tbMetaDesc" maxlength="2000"  type="text" value='<%#  Eval("SML_META_DESC")%>' />
                </td>
            </tr>
            <%} %>
           <% if (SM.EM.Security.Permissions.IsPortalAdmin()) {%>
             <tr>
                <td style="text-align:right;">
                    KW:
                </td>
                <td colspan="7">
                    
                    <input style="width:95%" class="tbTitle wizzNav_TBX "  name="tbKeywords" maxlength="512"  type="text" value='<%#  Eval("SML_KEYWORDS")%>' />
                </td>
            </tr>
            <%} %>
        
        </ItemTemplate>
    </asp:ListView>
    
<% if (SM.EM.Security.Permissions.IsAdvancedCms()) {%>
        <br />
        Custom CLASS:
        
        <input  style="width:160px" class="wizzNav_TBX "  id="tbMenuCustomClass" maxlength="100"  type="text" value='<%= Smap.CUSTOM_CLASS %>' />
        
        <%--<asp:TextBox ID="tbCustomClass" class="wizzNav_TBX" runat="server" MaxLength="50" Width="160px" ></asp:TextBox>    --%>
<%} %>

</div>
