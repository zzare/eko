﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
namespace SM.UI.CMS
{
    public partial class CMS_Intranet_new_user : SM.EM.UI.CMS.BasePage_CMS 
    {




        protected bool IsAdmin { get {

            return true; // todo: remove
            if (User.Identity.IsAuthenticated)
                if (User.IsInRole("admin"))
                    return true;
            return false;
            }
        }

        protected override void OnInit(EventArgs e)
        {
            this.CurrentSitemapID = 1402;
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            // event handlers
            objUserEdit.OnRegistrationComplete +=new EventHandler(objUserEdit_OnRegistrationComplete);
            objUserEdit.AutoLogin = false;
            objUserEdit.SendMail = true;

            if (!IsPostBack)
            {
                    LoadData();

            }
        }

        void objUserEdit_OnRegistrationComplete(object sender, EventArgs e)
        {
            
        }


        protected void LoadData() {

            SetStatus("uporabnik je dodan");

        }





    }
}