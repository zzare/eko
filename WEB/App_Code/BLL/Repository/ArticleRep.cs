﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


/// <summary>
/// Summary description for ArticleRep
/// </summary>
public class ArticleRep
{

    public  eMenikDataContext  db = new eMenikDataContext ();

   
    // Query Methods
    public ARTICLE GetArticleByID(int artID, Guid userid)
    {
        
        ARTICLE art;

        //art = (from a in db.ARTICLEs
        //       from s in db.SITEMAPs
        //       from m in db.MENUs
        //       from u in db.EM_USERs
        //       where a.ART_ID == artID && s.SMAP_ID == a.SMAP_ID && m.MENU_ID == s.MENU_ID && u.USERNAME == m.USERNAME && u.UserId == userid
        //       select a).SingleOrDefault();

        art = (from a in db.ARTICLEs
               from cwp in db.CMS_WEB_PARTs 
               where a.ART_ID == artID && a.CWP_ID == cwp.CWP_ID && cwp.UserId == userid
               select a).SingleOrDefault();

        return art;
    }

    public ARTICLE GetArticleByID(Guid artGUID, Guid userid)
    {

        ARTICLE art;

        //art = (from a in db.ARTICLEs
        //       from s in db.SITEMAPs
        //       from m in db.MENUs
        //       from u in db.EM_USERs
        //       where a.ART_GUID == artGUID && s.SMAP_ID == a.SMAP_ID && m.MENU_ID == s.MENU_ID && u.USERNAME == m.USERNAME && u.UserId == userid
        //       select a).SingleOrDefault();


        art = (from a in db.ARTICLEs
               from cwp in db.CMS_WEB_PARTs
               where a.ART_GUID == artGUID && a.CWP_ID == cwp.CWP_ID && cwp.UserId == userid
               select a).SingleOrDefault();


        return art;
    }


    public Data.ArticleData GetArticleByID(int artID)
    {
        //eMenikDataContext db = new eMenikDataContext();

        var data = (from a in db.ARTICLEs
                    from cwp in db.CMS_WEB_PARTs

                    join cwpg in
                    //    (from cwpi in db.CMS_WEB_PARTs where cwpi.ZONE_ID == ARTICLE.Common.MAIN_GALLERY_ZONE select cwpi) 
                        (   from cwpi in db.CMS_WEB_PARTs
                            where //&& cwpi.ACTIVE == true && cwpi.DELETED == false
                            cwpi.ZONE_ID == ARTICLE.Common.MAIN_GALLERY_ZONE
                            orderby cwpi.ORDER
                            select cwpi
                            ) on a.ART_GUID equals cwpg.REF_ID into cwpo
                    from cwpg in cwpo.DefaultIfEmpty()
                    join cwpgg in db.CWP_GALLERies on cwpg.CWP_ID equals cwpgg.CWP_ID into cwpggo
                    from cwpgg in cwpggo.DefaultIfEmpty()


                    from s in db.SITEMAPs
                    from sml in db.SITEMAP_LANGs

                    where a.ART_ID == artID && a.CWP_ID == cwp.CWP_ID && cwp.REF_ID == s.SMAP_GUID
                    && sml.SMAP_ID == s.SMAP_ID && sml.LANG_ID == cwp.LANG_ID
                    select new Data.ArticleData(a, "", -1, cwpgg.THUMB_TYPE_ID, (cwpg == null ? Guid.Empty : cwpg.CWP_ID), sml.SML_TITLE, s.SMAP_ID));
                    
                    //select new Data.ArticleData(a, "", -1, cwpgg.THUMB_TYPE_ID, (cwpg == null ? Guid.Empty : cwpg.CWP_ID), "", s.SMAP_ID));

        Data.ArticleData art = data.FirstOrDefault();


        return art;
    }


    public Data.ArticleData GetArticleByIDSitemap(int artID, int smp)
    {
        //eMenikDataContext db = new eMenikDataContext();

        Data.ArticleData art = (from a in db.ARTICLEs
                                from cwp in db.CMS_WEB_PARTs
                                
                                join cwpg in
//                                    (from cwpi in db.CMS_WEB_PARTs where cwpi.ZONE_ID == ARTICLE.Common.MAIN_GALLERY_ZONE select cwpi) on a.ART_GUID equals cwpg.REF_ID into cwpo
                                    (
                                from ig in db.IMAGE_GALLERies
                                from cwpi in db.CMS_WEB_PARTs
                                where  cwpi.ID_INT == ig.GAL_ID 
                                //&& cwpi.ACTIVE == true && cwpi.DELETED == false
                                && cwpi.ZONE_ID == ARTICLE.Common.MAIN_GALLERY_ZONE
                                orderby cwpi.ORDER, ig.IMG_ORDER                                
                                
                                select cwpi
                                
                                
                                ) on a.ART_GUID equals cwpg.REF_ID into cwpo
                                from cwpg in cwpo.DefaultIfEmpty()
                                join cwpgg in db.CWP_GALLERies on cwpg.CWP_ID equals cwpgg.CWP_ID into cwpggo
                                from cwpgg in cwpggo.DefaultIfEmpty()
                                

                                //join cwpa in db.CWP_ARTICLEs on cwp.CWP_ID equals cwpa.CWP_ID 
                                from s in db.SITEMAPs
                                //let imgID = (from ig in db.IMAGE_GALLERies
                                //             from cwpg in db.CMS_WEB_PARTs
                                //             where cwpg.REF_ID == a.ART_GUID && cwpg.ID_INT == ig.GAL_ID
                                //             orderby cwp.ORDER, ig.IMG_ORDER
                                //             select ig.IMG_ID).FirstOrDefault()

                                //join img in db.IMAGEs on imgID equals img.IMG_ID into imgo

                                //from img in imgo.DefaultIfEmpty()
                                where a.ART_ID == artID && a.CWP_ID == cwp.CWP_ID && cwp.REF_ID == s.SMAP_GUID &&  s.SMAP_ID == smp
                                select new Data.ArticleData(a, "", -1, cwpgg.THUMB_TYPE_ID , (cwpg == null ? Guid.Empty : cwpg.CWP_ID))).SingleOrDefault();

        return art;
    }

    public IQueryable<Data.ArticleData> GetArticlesByCwp(Guid cwpID, bool isEditMode)
    {
        var b = db.ARTICLEs.OrderByDescending(o=>o.RELEASE_DATE).ThenByDescending(o=>o.DATE_MODIFY ).ThenByDescending(o=>o.DATE_ADDED );

        return GetArticlesByCwp(cwpID, isEditMode, b);

    }


    public IQueryable<Data.ArticleData> GetArticlesByCwp(Guid cwpID, bool isEditMode, IQueryable<ARTICLE> aBase)
    {
        var alist = from a in aBase where a.ACTIVE == true select a;
        if (!isEditMode)
            alist = alist.Where(w => (w.RELEASE_DATE == null || w.RELEASE_DATE < DateTime.Now.Date.AddDays(1)) && (w.EXPIRE_DATE == null || w.EXPIRE_DATE >= DateTime.Now.Date));

        var list = from cwp in db.CMS_WEB_PARTs
                   from a in alist

                   join cwpa in db.CWP_ARTICLEs on cwp.CWP_ID equals cwpa.CWP_ID
                   let imgID = (from ig in db.IMAGE_GALLERies
                                from cwpg in db.CMS_WEB_PARTs
                                where cwpg.REF_ID == a.ART_GUID && cwpg.ID_INT == ig.GAL_ID && cwpg.ACTIVE == true && cwpg.DELETED == false
                                orderby cwpg.ORDER, ig.IMG_ORDER
                                select ig.IMG_ID).FirstOrDefault()

                   join img in db.IMAGEs on imgID equals  img.IMG_ID into imgo                   
                   from img in imgo.Where(w => w.TYPE_ID == (cwpa.IMG_TYPE_THUMB ?? SM.BLL.Common.ImageType.ArticleThumbDefault)).DefaultIfEmpty()

                   from s in db.SITEMAPs
                   from sml in db.SITEMAP_LANGs

                   where cwp.CWP_ID == cwpID && a.CWP_ID == cwp.CWP_ID
                   && cwp.REF_ID == s.SMAP_GUID && sml.SMAP_ID == s.SMAP_ID && sml.LANG_ID == cwp.LANG_ID 

                   //orderby a.RELEASE_DATE descending, a.DATE_MODIFY descending, a.DATE_ADDED descending
                   select new Data.ArticleData(a, img.IMG_URL ?? "", img.IMG_ID, img.TYPE_ID, null, sml.SML_TITLE , s.SMAP_ID );

        return list;
    }

    public IQueryable<Data.ArticleData> GetArticlesByAdditionalSmap(bool isEditMode, int smp, int? imgType)
    {
        var list = from a in db.ARTICLEs
                from ars in db.ARTICLE_SMAPs

                //join cwpa in db.CWP_ARTICLEs on cwpid equals cwpa.CWP_ID 
                let imgID = (from ig in db.IMAGE_GALLERies
                             from cwpg in db.CMS_WEB_PARTs
                             where cwpg.REF_ID == a.ART_GUID && cwpg.ID_INT == ig.GAL_ID && cwpg.ACTIVE == true && cwpg.DELETED == false
                             orderby cwpg.ORDER, ig.IMG_ORDER
                             select ig.IMG_ID).FirstOrDefault()
                join img in db.IMAGEs on imgID equals img.IMG_ID into imgo
                from img in imgo.Where(w => w.TYPE_ID == (imgType ?? SM.BLL.Common.ImageType.ArticleThumbDefault)).DefaultIfEmpty()

                from s in db.SITEMAPs
                from sml in db.SITEMAP_LANGs

                where ars.SMAP_ID == smp 
                && ars.ART_ID == a.ART_ID && s.SMAP_ID == ars.SMAP_ID && sml.SMAP_ID == ars.SMAP_ID && sml.LANG_ID == ars.LANG_ID 
                orderby a.RELEASE_DATE descending, a.DATE_MODIFY descending, a.DATE_ADDED descending
                select new Data.ArticleData(a, img.IMG_URL ?? "", img.IMG_ID, img.TYPE_ID, null, sml.SML_TITLE, s.SMAP_ID);
            

        return list;

    }

    public IQueryable<Data.ArticleData> GetArticlesByCwpAndAdditionalSmap(Guid cwpID, bool isEditMode, int smp, int? imgType, string lang)
    {
        // base
        var alist = from a in db.ARTICLEs where a.ACTIVE == true select a;
        if (!isEditMode)
        {
            alist = alist.Where(w => (w.RELEASE_DATE == null || w.RELEASE_DATE < DateTime.Now.Date.AddDays(1)) && (w.EXPIRE_DATE == null || w.EXPIRE_DATE >= DateTime.Now.Date)
                && w.APPROVED == true
                );
        }

        // cmp
        var listSmp = from a in alist
                   from ars in db.ARTICLE_SMAPs

                   from s in db.SITEMAPs
                from sml in db.SITEMAP_LANGs

                      where ars.SMAP_ID == smp && a.ACTIVE == true
                && ars.ART_ID == a.ART_ID && s.SMAP_ID == ars.SMAP_ID && sml.SMAP_ID == ars.SMAP_ID && sml.LANG_ID == ars.LANG_ID 
               select new {a, sml};


        // cwp

        var listCwp = from cwp in db.CMS_WEB_PARTs
                   from a in alist

                   from s in db.SITEMAPs
                   from sml in db.SITEMAP_LANGs

                   where cwp.CWP_ID == cwpID && a.CWP_ID == cwp.CWP_ID
                   && cwp.REF_ID == s.SMAP_GUID && sml.SMAP_ID == s.SMAP_ID && sml.LANG_ID == cwp.LANG_ID 
                    select new {a, sml};

        // unon, sort, ArticleData
        var list = from a in listCwp.Union(listSmp)

                   let imgID = (from ig in db.IMAGE_GALLERies
                                from cwpg in db.CMS_WEB_PARTs
                                where cwpg.REF_ID == a.a.ART_GUID && cwpg.ID_INT == ig.GAL_ID && cwpg.ACTIVE == true && cwpg.DELETED == false
                                orderby cwpg.ORDER, ig.IMG_ORDER
                                select ig.IMG_ID).FirstOrDefault()
                   join img in db.IMAGEs on imgID equals img.IMG_ID into imgo
                   from img in imgo.Where(w => w.TYPE_ID == (imgType ?? SM.BLL.Common.ImageType.ArticleThumbDefault)).DefaultIfEmpty()
                   where a.sml.LANG_ID == lang
                   orderby a.a.RELEASE_DATE descending, a.a.DATE_MODIFY descending, a.a.DATE_ADDED descending
                   select new Data.ArticleData(a.a, img.IMG_URL ?? "", img.IMG_ID, img.TYPE_ID, null, a.sml.SML_TITLE, a.sml.SMAP_ID);


        return list;

    }



    public IQueryable<ARTICLE> GetArticlesByActive(bool active)
    {
        var alist = from a in db.ARTICLEs where a.ACTIVE == true select a;
        if (active)
            alist = alist.Where(w => (w.RELEASE_DATE == null || w.RELEASE_DATE < DateTime.Now.Date.AddDays(1)) && (w.EXPIRE_DATE == null || w.EXPIRE_DATE >= DateTime.Now.Date));

        return alist;
    }


    public IQueryable<Data.ArticleData> GetArticleListHierarchy(int parentSmap, string lang, bool? active, Guid pageID)
    {
        var rec = from a in GetArticlesByActive(active ?? true)
                  orderby a.RELEASE_DATE descending, a.DATE_MODIFY descending, a.DATE_ADDED descending
                  select a;
        return GetArticleListHierarchy(parentSmap, lang, active, pageID, rec);

    }

    public IQueryable<Data.ArticleData> GetArticleListHierarchy(int parentSmap, string lang, bool? active, Guid pageID, IQueryable<ARTICLE> aBase)
    {
        var list = from a in aBase
                   from sh in db.f_h_Sitemap(parentSmap)
                   from s in db.SITEMAPs
                   from sml in db.SITEMAP_LANGs
                   from cwp in db.CMS_WEB_PARTs 
                   where a.CWP_ID == cwp.CWP_ID && cwp.REF_ID == s.SMAP_GUID && sml.SMAP_ID == s.SMAP_ID && sml.LANG_ID == lang && s.SMAP_ID == sh.SMAP_ID && cwp.LANG_ID == lang
                   && !cwp.DELETED
                   select new Data.ArticleData(a, "", -1, -1, null, sml.SML_TITLE , s.SMAP_ID );

        //return rec.Where (w=>w.SMAP_ID == smap) ;
        return list;
    }

    public IQueryable<Data.ArticleData> GetArticleListHierarchyWithImage(int parentSmap, string lang, bool? active, Guid pageID, int imgType)
    {
        var rec = from a in GetArticlesByActive(active ?? true)
                  orderby a.RELEASE_DATE descending, a.DATE_MODIFY descending, a.DATE_ADDED descending
                  select a;
        return GetArticleListHierarchyWithImage(parentSmap, lang, active, pageID, rec, imgType);

    }
    public IQueryable<Data.ArticleData> GetArticleListHierarchyWithImage(int parentSmap, string lang, bool? active, Guid pageID, IQueryable<ARTICLE> aBase, int imgType)
    {
        var list = from a in aBase
                   from sh in db.f_h_Sitemap(parentSmap)
                   from s in db.SITEMAPs
                   from sml in db.SITEMAP_LANGs
                   from cwp in db.CMS_WEB_PARTs

                   join cwpa in db.CWP_ARTICLEs on cwp.CWP_ID equals cwpa.CWP_ID
                   let imgID = (from ig in db.IMAGE_GALLERies
                                from cwpg in db.CMS_WEB_PARTs
                                where cwpg.REF_ID == a.ART_GUID && cwpg.ID_INT == ig.GAL_ID && cwpg.ACTIVE == true && cwpg.DELETED == false
                                orderby cwpg.ORDER, ig.IMG_ORDER
                                select ig.IMG_ID).FirstOrDefault()
                   join img in db.IMAGEs on imgID equals img.IMG_ID into imgo
                   from img in imgo.Where(w => w.TYPE_ID == imgType).DefaultIfEmpty()                   

                   where a.CWP_ID == cwp.CWP_ID && cwp.REF_ID == s.SMAP_GUID && sml.SMAP_ID == s.SMAP_ID && sml.LANG_ID == lang && s.SMAP_ID == sh.SMAP_ID && cwp.LANG_ID == lang 
                   && !cwp.DELETED
                   select new Data.ArticleData(a, img.IMG_URL ?? "", img.IMG_ID, img.TYPE_ID, null, sml.SML_TITLE, s.SMAP_ID);

        //return rec.Where (w=>w.SMAP_ID == smap) ;
        return list;
    }

    public IQueryable<SITEMAP_LANG> GetArticleSitemapLangs(int artid, string lang)
    {
        IQueryable<SITEMAP_LANG> list = from pa in db.ARTICLE_SMAPs
                                        from sml in db.SITEMAP_LANGs
                                        where pa.ART_ID == artid && sml.SMAP_ID == pa.SMAP_ID && sml.LANG_ID == lang
                                        select sml;
        return list;
    }




    // add

    public void Add(ARTICLE  art)
    {
        if (HttpContext.Current != null)
            art.ADDED_BY = HttpContext.Current.User.Identity.Name;

        art.DATE_ADDED = DateTime.Now;
        art.ART_GUID = Guid.NewGuid();
        art.APPROVED = true;
        art.ACTIVE = true;

        db.ARTICLEs.InsertOnSubmit(art);

    }
    public void AddArticleSmap(ARTICLE_SMAP  psmp)
    {
        db.ARTICLE_SMAPs .InsertOnSubmit(psmp);
    }

    public void DeleteArticleSmaps(int id)
    {
        var list = from pga in db.ARTICLE_SMAPs
                   where pga.ART_ID == id
                   select pga;
        db.ARTICLE_SMAPs.DeleteAllOnSubmit(list);
    }

    
    // Persistence

    public bool  Save()
    {
        try
        {
            db.SubmitChanges();
        }
        catch (Exception ex)
        {
            ERROR_LOG.LogError( ex, "Save article");

            return false;
        }
        return true;        
    }








}