﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewProductList.ascx.cs" Inherits="_ctrl_module_product_viewProductList" %>
<script type="text/javascript" >
    $(document).ready(function() {

        if ($.browser.msie && $.browser.version.substr(0, 1) < 7) {
            $('.prod_render').hover(
                function() {
                    $(this).addClass("prod_renderHov");
                },
                function() {
                    $(this).removeClass("prod_renderHov");
                }
            );
        }

<% if(!IsModeCMS ) {%>
        $('div.prod_render').click(function(event) {
           if (!$(this).find("a.plToCart").is(":visible"))
                return;
            
            window.location = $(this).find("a").attr("href"); return false;
        });

<%} %>        


        //enacenje visine prod.list elementov
        var height = 0;
        jQuery.each($('.prod_render span.plNameS'), function() {
            var lHeight = $(this).height();
            if (lHeight > height) { height = lHeight; }
        });
        jQuery.each($('.prod_render span.plNameS'), function() {
            $(this).css({ height: height });
        });


        $("div.prod_render a.plToCart").click(function(e){
            agent = jQuery.browser;
	        if(agent.msie) {
		        e.cancelBubble = true;
	        } else {
		        e.stopPropagation();
	        }
        });

    });
    <% if(IsModeCMS ) {%>
function editCatDescLoad(s, l) {

    setModalLoaderLoading();
    openModalLoading('Odpiram...');
    var _items = new Array();
    Array.add(_items, s);
    Array.add(_items, l);
    WebServiceCWP.RenderViewAut(_items, 18, onContentLoadSuccess, onError);
}
<%} %>


function addToCartL(pid, t) {
            if (!$(t).is(":visible"))
                return;

            $(t).css('visibility', 'hidden');

            flytoBasketL(t);
             WebServiceP.AddToCart(pid, 1, new Array(), em_g_cult, em_g_lang, em_g_pid, onCartUpdateSuccess, onCartUpdateError, t);
    }

    function flytoBasketL(t) {
        var orig = $(t).parent().parent().parent().find('img');
        var basket = $('#cartPopup a.Acart span');
        var fly = orig.clone().css('top', orig.offset().top + 'px').css('left', orig.offset().left + 'px').css('position', 'absolute').appendTo('body');

        basket.css('backgroundPosition', 'bottom right');
        basket.parent().css('backgroundPosition', 'bottom left');

        fly.animate({
        left: basket.offset().left,
        top: basket.offset().top,
            height: '1',
            opacity: 0.0

        }, 900, function() { $(this).hide() });

        //orig.effect("pulsate", { times: 1 }, 500);
    }

</script> 

<div class="prod_renderW">

<%--MESO BEGIN--%>

            <%--<a class='prod_list_anchor'></a>
            <span class='plist_sepa'></span>
            <h2  class='prod_category font_bevan'>MESO</h2>


            <div class="prod_render prod_render_left">
                <div class="prod_renderM">
                    

                        <div class="plImg">

                            <div class="plImgMask">

                                <a class="plImgMaskA" href='#'  title='GOVEDO LIMUZINA KR NEKEJ' >

                                </a>
                            </div>
                            <img class="plImage" src='<%= SM.BLL.Common.ResolveUrl("userfiles/eko/11a53491-589c-49c6-8a96-235e378dc8b6.jpg")  %>' alt='' />


                        </div>


                        <div class="plText">
                            <a class="plName" href='#' >
                            <span class="plNameS font_bevan">GOVEDO LIMUZINA KR NEKEJ</span>

                            <span class="plPrice"><span class="plPriceNumber">95,00 €</span><%= GetGlobalResourceObject("Product", "priceRegularFrom")%>: </span>
    

                            </a>
                        </div>

                        <div class="plCartbtnW">
                            <div class="plCartbtn">
                                <a href="#" title="Podrobnosti" class="AcarthovFinish  font_bevan"><span><%= GetGlobalResourceObject("Product", "details")%></span></a>
                            </div>
                            <div class="clearing"></div>
                        </div>

        

                        <div class="clearing"></div>




                </div>
            </div>

            <div class="prod_render_right">
                <p>
                V neokrnjenem okolju pašnikov hriba Korada, kjer oko rado počije na odprtem razgledu Goriških brd, Jadranskega morja, Alp, Furlanije, Soške doline in predalpskega sveta, veselo muka govedo francoske pasme Limousine. Najprej štalca, potlej kravca? Ne, ne, pozabimo na štalco.
                <br /><br />
                Limuzinke, kot jim pravimo bližnji, skozi celo leto pasemo izključno na prostem, v naravi; pozimi jih pred mrazom varuje ljubezen in njihova gosta griva. Celoletna prosta paša na ekoklimi mediteransko-alpske idile in stalno gibanje goveda se brez pardona kažeta tudi v (že tako opevani) kakovosti mesa te pasme – meso poleg vrhunskega okusa odlikuje tudi izredno nizka vsebnost maščob.
                </p>
            </div>
            <div class="clearing"></div>--%>

<%--MESO END--%>


    <asp:ListView ID="lvList" runat="server" >
        <LayoutTemplate>
            
                <asp:PlaceHolder ID="itemPlaceholder" runat="server"/>

        </LayoutTemplate>
        
        <ItemTemplate>

        <asp:PlaceHolder ID="phFirst" runat="server" Visible='<%# (Container.DataItemIndex == 0 && ShowFirstCategoryDifferent ) %>'>

            <%# RenderCategories(Container.DataItem, (Container.DataItemIndex == 0 && ShowFirstCategoryDifferent), false)%>


            <div class="prod_render prod_render_left">
                <div class="prod_renderM">
                    <%# RenderListItem(Container.DataItem ) %>
                    <div class="clearing"></div>
                </div>
            </div>

            <div class="prod_render_right">
                <p>
                <%# CatDescRender  %>
                <%--V neokrnjenem okolju pašnikov hriba Korada, kjer oko rado počije na odprtem razgledu Goriških brd, Jadranskega morja, Alp, Furlanije, Soške doline in predalpskega sveta, veselo muka govedo francoske pasme Limousine. Najprej štalca, potlej kravca? Ne, ne, pozabimo na štalco.
                <br /><br />
                Limuzinke, kot jim pravimo bližnji, skozi celo leto pasemo izključno na prostem, v naravi; pozimi jih pred mrazom varuje ljubezen in njihova gosta griva. Celoletna prosta paša na ekoklimi mediteransko-alpske idile in stalno gibanje goveda se brez pardona kažeta tudi v (že tako opevani) kakovosti mesa te pasme – meso poleg vrhunskega okusa odlikuje tudi izredno nizka vsebnost maščob.--%>
                </p>
            </div>
            <div class="clearing"></div>

        
        </asp:PlaceHolder>

        <asp:PlaceHolder ID="phRest" runat="server" Visible='<%# (Container.DataItemIndex > 0 ||  !ShowFirstCategoryDifferent ) %>'>
        
            
            <%# RenderCategories(Container.DataItem, (Container.DataItemIndex > 0 || !ShowFirstCategoryDifferent))%>
            <div class="prod_render <%# RenderClass(Container.DataItem, Container.DataItemIndex) %>">
<%--                <div class="prod_renderT"></div>
--%>                <div class="prod_renderM">
                    <%# RenderListItem(Container.DataItem ) %>
                </div>
                <%--<div class="prod_renderB"></div>--%>
            </div>
           </asp:PlaceHolder>      
        </ItemTemplate>
    </asp:ListView>

    <asp:PlaceHolder ID="phEmpty" runat="server" Visible="false">
        <span class="prod_renderEmptyS"><%= GetGlobalResourceObject("Product", "msgEmptyProductList")%></span>

    </asp:PlaceHolder>
    <div class="clearing"></div>
</div>