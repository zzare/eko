﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using SM.EM;

namespace SM.EM.UI
{

    public partial class MasterPageCMS : BaseMasterPage 
    {
        public override string StatusText{
            get{
                return lStatusText.Text;
            }
            set{
                lStatusText.Text = value;
                if (value=="") lStatusText.Visible = false;
                else lStatusText.Visible = true;
            }
        }

        public override string ErrorText{
            get{
                return lErrorText.Text;
            }
            set{
                lErrorText.Text = value;
                if (value == "") lErrorText.Visible = false;
                else lErrorText.Visible = true;
            }
        }

        

        protected void Page_Load(object sender, EventArgs e)
        {
            // add CSS
            CSSMain.Href += "?" + Helpers.GetCssVersion();
            CSSMenu.Href += "?" + Helpers.GetCssVersion();
            CSSIEMenu6.Href += "?" + Helpers.GetCssVersion();
            CSSIECms.Href += "?" + Helpers.GetCssVersion();
            CSSIECms6.Href += "?" + Helpers.GetCssVersion();


            // Main title
           // Page.Title = "eMenik CMS";

            //SelectCurrentMenuItem(mTop);
            //SelectCurrentMenuItem(mTopSub);

        }

        // todo: move to ancestor
        protected void tvMenuTop_DataBound(object sender, EventArgs e)
        {
            TreeView tv = (TreeView)sender;
            SelectCurrentNodeTV( tv);
        }
        protected void mTop_DataBound(object sender, EventArgs e){
            Menu m = (Menu)sender;
            //SelectCurrentMenuItem(m);
        }
        protected void OnMenuPrerender(object sender, EventArgs e)
        {
            Menu m = (Menu)sender;
            SiteMapProvider smp = SiteMap.Providers[ MainSitemapProv];
            if (smp != null)
                SelectCurrentMenuItem(m,   smp);
        }

}
}