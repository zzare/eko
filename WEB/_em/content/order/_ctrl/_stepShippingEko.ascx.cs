﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SM.UI.Controls;
using SM.EM.UI.Controls;


public partial class _em_content_order__ctrl__stepShippingEko : BaseWizardStepOrder 
{
    public event EventHandler OnSaveOK;

    public CustomerRep Crep;
    public CUSTOMER Customer;

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        ShortDesc = Resources.Product.step2Title1;
    }
    public  DateTime GetNextWeekday(DateTime start, DayOfWeek day)
    {
        // The (... + 7) % 7 ensures we end up with a value in the range [0, 6]
        int daysToAdd = ((int)day - (int)start.DayOfWeek + 7) % 7;
        return start.AddDays(daysToAdd);
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Order != null)
        {
            Crep = new CustomerRep();
            Customer = Crep.GetCustomerByID(Order.UserId);
            // tmp
            if (Customer == null)
            {
                Customer = new CUSTOMER();
            }
        }


        cbShipping.Text = "&nbsp;" + Resources.Product.step2SurpriseFriend;
        TextBoxWatermarkExtender2.WatermarkText = Resources.Product.step2MsgEnterName;
        rfvObdarovanec.ErrorMessage = "* " +  Resources.Product.step2ValEnterName;
        rfvDate.ErrorMessage = "* " + Resources.Product.step2ValEnterDate;
        rfvHour.ErrorMessage = "* " + Resources.Product.step2ValEnterHour;
        rfvRegion.ErrorMessage = "* " + Resources.Product.step2ValEnterRegion;
        rfvCity.ErrorMessage = "* " + Resources.Product.step2ValEnterCity;
        rfvAddress.ErrorMessage = "* " + Resources.Product.step2ValEnterAddress;
        rfvPhone.ErrorMessage = "* " + Resources.Product.step2ValEnterPhone;
    }


    public override void LoadData()
    {
        if (Order == null)
            return;

        DateTime start = DateTime.Today;
        DateTime min = new DateTime (2011,12,8);

        if (start < min)
            start = min;


        // load dates
        DateTime day = GetNextWeekday(start, DayOfWeek.Wednesday);
        if (ddlShipping.Items.Count == 0) ddlShipping.Items.Add(new ListItem(Resources.Product.step2Select, ""));
        if (ddlShipping.Items.Count <= 1)
        {
            //for (int i = 0; i < 4; i++)
            //{
            //    // tmp
            //    if (day > new DateTime(2012, 1, 24))
            //        ddlShipping.Items.Add(new ListItem(day.ToShortDateString(), day.ToShortDateString()));
            //    day = GetNextWeekday(day.AddDays(1), DayOfWeek.Wednesday); // excluding current (+1)
            //}

            //day = new DateTime(2012, 2, 1);
            //if (day >= DateTime.Today)
            //    ddlShipping.Items.Add(new ListItem(day.ToShortDateString(), day.ToShortDateString()));
            //day = new DateTime(2012, 2, 7);
            //if (day >= DateTime.Today)
            //    ddlShipping.Items.Add(new ListItem(day.ToShortDateString(), day.ToShortDateString()));


            //day = new DateTime(2012, 6, 30);
            //if (day >= DateTime.Today)
            //    ddlShipping.Items.Add(new ListItem(day.ToShortDateString(), day.ToShortDateString()));
            //day = new DateTime(2012, 9, 19);
            //if (day >= DateTime.Today)
            //    ddlShipping.Items.Add(new ListItem(day.ToShortDateString(), day.ToShortDateString())); 
            //day = new DateTime(2012, 9, 26);
            //if (day >= DateTime.Today)
            //    ddlShipping.Items.Add(new ListItem(day.ToShortDateString(), day.ToShortDateString()));

            //day = new DateTime(2012, 11, 28);
            //if (day >= DateTime.Today)
            //    ddlShipping.Items.Add(new ListItem(day.ToShortDateString(), day.ToShortDateString()));
            //day = new DateTime(2012, 12, 5);
            //if (day >= DateTime.Today)
            //    ddlShipping.Items.Add(new ListItem(day.ToShortDateString(), day.ToShortDateString()));

            //day = new DateTime(2012, 12, 12);
            //if (day >= DateTime.Today)
            //    ddlShipping.Items.Add(new ListItem(day.ToShortDateString(), day.ToShortDateString()));
            //day = new DateTime(2012, 12, 19);
            //if (day >= DateTime.Today)
            //    ddlShipping.Items.Add(new ListItem(day.ToShortDateString(), day.ToShortDateString()));

            day = new DateTime(2013, 4, 11);
            if (day >= DateTime.Today)
                ddlShipping.Items.Add(new ListItem(day.ToShortDateString(), day.ToShortDateString()));
            day = new DateTime(2013, 4, 18);
            if (day >= DateTime.Today)
                ddlShipping.Items.Add(new ListItem(day.ToShortDateString(), day.ToShortDateString()));


        }

        // load shipping region
        if (ddlRegion.Items.Count == 0)
            ddlRegion.Items.Add(new ListItem(Resources.Product.step2Select, ""));
        if (ddlRegion.Items.Count <= 1)
        {
            if (LANGUAGE.GetCurrentLang() == "si")
            {
                ddlRegion.Items.Add(new ListItem(SHOPPING_ORDER.Common.RenderShoppingOrderShippingRegion(SHOPPING_ORDER.Common.ShippingRegion.LJUBLJANA), SHOPPING_ORDER.Common.ShippingRegion.LJUBLJANA.ToString()));
                ddlRegion.Items.Add(new ListItem(SHOPPING_ORDER.Common.RenderShoppingOrderShippingRegion(SHOPPING_ORDER.Common.ShippingRegion.NOVA_GORICA), SHOPPING_ORDER.Common.ShippingRegion.NOVA_GORICA.ToString()));
                ddlRegion.Items.Add(new ListItem(SHOPPING_ORDER.Common.RenderShoppingOrderShippingRegion(SHOPPING_ORDER.Common.ShippingRegion.OSTALO), SHOPPING_ORDER.Common.ShippingRegion.OSTALO.ToString()));
            }
            else if (LANGUAGE.GetCurrentLang() == "en")
            {
                ddlRegion.Items.Add(new ListItem(SHOPPING_ORDER.Common.RenderShoppingOrderShippingRegionEN(SHOPPING_ORDER.Common.ShippingRegion.LJUBLJANA), SHOPPING_ORDER.Common.ShippingRegion.LJUBLJANA.ToString()));
                ddlRegion.Items.Add(new ListItem(SHOPPING_ORDER.Common.RenderShoppingOrderShippingRegionEN(SHOPPING_ORDER.Common.ShippingRegion.NOVA_GORICA), SHOPPING_ORDER.Common.ShippingRegion.NOVA_GORICA.ToString()));
                ddlRegion.Items.Add(new ListItem(SHOPPING_ORDER.Common.RenderShoppingOrderShippingRegionEN(SHOPPING_ORDER.Common.ShippingRegion.OSTALO), SHOPPING_ORDER.Common.ShippingRegion.OSTALO.ToString()));
            }
            else if (LANGUAGE.GetCurrentLang() == "it")
            {
                ddlRegion.Items.Add(new ListItem(SHOPPING_ORDER.Common.RenderShoppingOrderShippingRegionIT(SHOPPING_ORDER.Common.ShippingRegion.LJUBLJANA), SHOPPING_ORDER.Common.ShippingRegion.LJUBLJANA.ToString()));
                ddlRegion.Items.Add(new ListItem(SHOPPING_ORDER.Common.RenderShoppingOrderShippingRegionIT(SHOPPING_ORDER.Common.ShippingRegion.NOVA_GORICA), SHOPPING_ORDER.Common.ShippingRegion.NOVA_GORICA.ToString()));
                ddlRegion.Items.Add(new ListItem(SHOPPING_ORDER.Common.RenderShoppingOrderShippingRegionIT(SHOPPING_ORDER.Common.ShippingRegion.OSTALO), SHOPPING_ORDER.Common.ShippingRegion.OSTALO.ToString()));
            }
        }


        // load hours
        if (ddlHour.Items.Count == 0)
            ddlHour.Items.Add(new ListItem(Resources.Product.step2Select, ""));
            
        if (ddlHour.Items.Count <= 1)
        {
            ddlHour.Items.Add(new ListItem("14:00 - 16:00", "14:00 - 16:00"));
            ddlHour.Items.Add(new ListItem("16:00 - 18:00", "16:00 - 18:00"));
            ddlHour.Items.Add(new ListItem("18:00 - 20:00", "18:00 - 20:00"));
            ddlHour.Items.Add(new ListItem("20:00 - 22:00", "20:00 - 22:00"));
        }




        var shippList = Orep.GetShippingAddressesByUserEko(Order.UserId);
        //lvListCopy.DataSource = shippList;
        //lvListCopy.DataBind();
        //if (lvListCopy.Items.Count <= 0)
        //    phCopyAddress.Visible = false;

        // preselect LAST
        var lastAddress = shippList.FirstOrDefault();
        if (lastAddress != null)
        {
            tbAddress.Text = lastAddress.Address;
            tbPhone.Text = lastAddress.Phone;
            tbCity.Text = lastAddress.City;

            if (Order.ShippingRegion == null)
            {
                ddlRegion.SelectedValue = lastAddress.Region.ToString();
            }
        }


        var compList = Orep.GetOrderUserAddressesByUser(Order.UserId);
        lvListCAddr.DataSource = compList;
        lvListCAddr.DataBind();
        if (lvListCAddr.Items.Count <= 0)
            phCopyC.Visible = false;

        //var compList = Orep.GetCompanyDetailsByUser(Order.UserId);
        //lvListCAddr.DataSource = compList;
        //lvListCAddr.DataBind();
        //if (lvListCAddr.Items.Count <= 0)
        //    phCopyC.Visible = false;














        
        // load order data
        tbCFirstname.Text = Order.OrderFirstName;
        tbCLastname.Text = Order.OrderLastName;
        tbCTititle.Text = Order.OrderCompany ;
        tbCTax.Text = Order.OrderTax;
        tbCAddress.Text = Order.OrderAddress;
        tbCCity.Text = Order.OrderCity;
        tbCPhone .Text = Order.OrderPhone;


        



        // shipping
        cbShipping.Checked = Order.UseDifferentShippingAddress;
        if (cbShipping.Checked) {
            tbObdarovanec.Text = Order.ShippingFirstName;
            
        }
        //tbName.Text = Order.ShippingFirstName;
        if (!string.IsNullOrWhiteSpace( Order.ShippingAddress))
            tbAddress.Text = Order.ShippingAddress;
        if (!string.IsNullOrWhiteSpace(Order.ShippingCity))
            tbCity.Text = Order.ShippingCity;
        if (!string.IsNullOrWhiteSpace(Order.ShippingPhone))
            tbPhone.Text = Order.ShippingPhone;
        //tbTime.Text = Order.OrderTime;
        tbNotes.Text = Order.OrderNotes;
        string selDate = "";

        if (Order.ShippedDate != null)
        {
            selDate = Order.ShippedDate.Value.ToShortDateString();
            ddlShipping.SelectedValue = selDate;
        }
        // time
        if (!string.IsNullOrWhiteSpace( Order.OrderTime))
        {
            ddlHour.SelectedValue = Order.OrderTime;
        }

        // region
        if (Order.ShippingRegion != null) {
            ddlRegion.SelectedValue = Order.ShippingRegion.ToString();
        }


        

        // load shipping data
        //if (Order.ShippingType == SHOPPING_ORDER.Common.ShippingType.SHIPPING)
        //{
        //    rbShipping.Checked = true;

        //}
        //else if (Order.ShippingType == SHOPPING_ORDER.Common.ShippingType.PERSONAL)
        //    rbPersonal.Checked = true;

        //cbCompany.Checked = Order.IsCompany;
        //if (Order.COMPANY_ID != null) {
        //    tbCTititle.Text = Order.COMPANY.NAME;
        //    tbCTax.Text = Order.COMPANY.TAX_NUMBER;
        //    tbCAddress.Text = Order.COMPANY.ADDRESS.STREET;
        //    tbCCity.Text = Order.COMPANY.ADDRESS.CITY;
        
        //}




    }

    //protected void ValidateShipping(object o, ServerValidateEventArgs args)
    //{
    //    args.IsValid = true;

    //    // check for duplicate page name
    //    if (!rbShipping.Checked && !rbPersonal.Checked)
    //    {
    //        args.IsValid = false;
    //        CustomValidator cv = (CustomValidator)o;
    //        cv.ErrorMessage = "* Izberite način dostave";
    //    }
    //}


    protected void btNext_Click(object o, EventArgs e)
    {
        bool isValid = true;
        Page.Validate("valShipping");
        if (!Page.IsValid)
            isValid = false;
        // validate company
        Page.Validate("vgCompany");
        if (!Page.IsValid)
            isValid = false;

        // eko
        rfvObdarovanec.Enabled = cbShipping.Checked;

        // validate shiupping address
        //if (rbShipping.Checked && cbShipping.Checked )
        {
            Page.Validate("vgShipping");
            if (!Page.IsValid)
                isValid = false;
        }

        // there was an error
        if (!isValid)
            return;


        // error
        if (Order == null)
            return;


        short shippingType = SHOPPING_ORDER.Common.ShippingType.NOT_SET;
        //if (rbShipping.Checked)
        {

            shippingType = SHOPPING_ORDER.Common.ShippingType.SHIPPING;
            Order.UseDifferentShippingAddress = false;



            DateTime dateShipping = DateTime.Parse(ddlShipping.SelectedValue);

            int region = -1;
            if (!string.IsNullOrWhiteSpace(ddlRegion.SelectedValue))
                region = int.Parse(ddlRegion.SelectedValue);
                
            Order.ShippingRegion = region ;
            Order.ShippingAddress = tbAddress.Text;
            Order.ShippingCity = tbCity.Text;

            //Order.ShippingPostal = tbTime.Text;
            //Order.ShippingFirstName = dateShipping.ToShortDateString();
            Order.ShippedDate = dateShipping;
            Order.OrderTime = ddlHour.SelectedValue;
            Order.OrderNotes = tbNotes.Text;
                Order.ShippingPhone = tbPhone.Text;
            //}


        }
        //else if (rbPersonal.Checked)
        //    shippingType = SHOPPING_ORDER.Common.ShippingType.PERSONAL;
        Order.ShippingType = shippingType;


        // save user order details
        Order.OrderFirstName = tbCFirstname.Text;
        Order.OrderLastName = tbCLastname.Text;
        Order.OrderAddress = tbCAddress.Text;
        Order.OrderCity = tbCCity.Text;
        Order.OrderCompany = tbCTititle.Text;
        Order.OrderTax = tbCTax.Text;
        Order.OrderPhone = tbCPhone.Text;


        // eko: use customer:
        Order.OrderFirstName = Customer.FIRST_NAME;
        Order.OrderLastName = Customer.LAST_NAME;
        if (cbShipping.Checked)
        {
            Order.UseDifferentShippingAddress = true;
            Order.ShippingFirstName = tbObdarovanec.Text;
        }



        // save
        Orep.Save();

        // if all ok
        OnSaveOK(this, new EventArgs());

    }


    public string RenderDisplayName(object o) {
        OrderRep.Data.ShippingAddress data = (OrderRep.Data.ShippingAddress)o;

        string ret = "";

        ret = data.FirstName + " " + data.LastName;

        if (!string.IsNullOrEmpty(data.Company ))
            ret += " " + data.Company;
        
        ret += ", " + data.Address;


        return ret;
    }


}
