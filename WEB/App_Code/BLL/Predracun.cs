﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Security.Principal;
using System.Web.Caching;
using System.Net.Mail;
using System.Collections.Generic;
using System.Threading;

/// <summary>
/// Summary description for PREDRACUN
/// </summary>

public partial class PREDRACUN
{
    public static PREDRACUN NewPredracun(Guid user, string notes)
    {
        eMenikDataContext db = new eMenikDataContext();

        PREDRACUN p = new PREDRACUN();

        // get PREDRACUN CODE
        p.UserId = user;
        p.Notes = notes;
        p.Price =  USER_MODULE.GetUserModulePriceSumDiscount(EM_USER.GetUserByID(user));
        p.PredracunCode = GetPredracunCode(user);

        p.DateAdded = DateTime.Now;

        db.PREDRACUNs.InsertOnSubmit(p);
        db.SubmitChanges();

        return p;
    }

    public static string GetPredracunCode(Guid userID) {
        EM_USER usr = EM_USER.GetUserByID(userID );

        string code = usr.CODE.ToString("000000");
        return code;
    }

    //public static bool SendPredracun(EM_USER usr, PREDRACUN p)
    //{


    //    MailMessage message = SM.EM.Mail.Template.GetPredracun (usr, p);
    //    message.From = new MailAddress(SM.EM.Mail.Account.Narocila.Email, SM.BLL.Common.Emenik.Data.PortalName() );
    //    message.IsBodyHtml = true;
    //    message.Subject = string.Format("Predračun (" + SM.BLL.Common.Emenik.Data.PortalName() + ") - ({0})", DateTime.Now);



    //    SmtpClient client = new SmtpClient();


    //    try
    //    {
    //        client.Credentials = new System.Net.NetworkCredential(SM.EM.Mail.Account.Narocila.Email, SM.EM.Mail.Account.Narocila.Pass);
    //        client.Port = 587;//or use 587            

    //        client.Host = "smtp.gmail.com";
    //        client.EnableSsl = true;

    //        client.Send(message);

    //    }

    //    catch (Exception ex)
    //    {
    //        return false;
    //    }
    //    return true;

    //}


}
