﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
namespace SM.UI.Controls
{
    public partial class _ctrlLanguage : BaseControl_EMENIK
    {
        private string _separator = " | ";
        public string Separator { get { return _separator; } set { _separator = value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) { 
                // load languages
                //LoadLanguages();
            
            }
        }

        public void LoadLanguages() { 
            // hide if not enabled
            if (!ParentPage.em_user.IS_MULTILINGUAL)
            {
                HideLangs();
                return;
            }

            lvList.DataSource = CULTURE.GetUserCultures(ParentPage.em_user.UserId);
            lvList.DataBind();
            ShowLangs();
        
        }

        protected void ShowLangs()
        {
            phVisible.Visible  = true;
            phHidden.Visible = false;

        }
        protected void HideLangs()
        {
            phVisible.Visible = false;
            phHidden.Visible = false;

        }

        protected void lvList_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            if (e.CommandName == "setLang") {
                string oldCult = LANGUAGE.GetCurrentCulture();
                string newCult = e.CommandArgument.ToString();

                // not change
                if (oldCult == newCult) return;

                LANGUAGE.SetCurrentCulture(newCult );
                Response.Redirect(SM.EM.Rewrite.EmenikUserUrl(ParentPage.PageName));
            }
        }

        protected string RenderSelClass(string cul) {
            if (LANGUAGE.GetCurrentCulture() == cul)
                return " selLang";
            return "";
        
        
        }
         
}
}
