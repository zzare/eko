﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_cmsImageNew.ascx.cs" Inherits="SM.EM.UI.CMS.Controls._cmsImageNew" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>





<span class="modPopup_mainTitle" >Izberi datoteko *</span>

        <asp:RegularExpressionValidator Display="Dynamic" EnableClientScript="true" ValidationGroup="file"  id="revFile" runat="server"  ErrorMessage="* Only .zip or image (.jpg, .gif, .png) files alowed)" 
                                        ValidationExpression="^.+(.jpg|.JPG|.gif|.GIF|.png|.PNG|.zip|.ZIP)$" 
                                        ControlToValidate="fuFile"></asp:RegularExpressionValidator>
        <asp:RequiredFieldValidator Display="Dynamic" ValidationGroup="file" EnableClientScript="true"  id="rfvUpload" runat="server"  ErrorMessage="* Please select file for upload"  ControlToValidate="fuFile"></asp:RequiredFieldValidator>
        <asp:CustomValidator ValidationGroup="fileC" Display="Dynamic" ID="cvFileSize" runat="server" ErrorMessage="" OnServerValidate="ValidateFileSize"></asp:CustomValidator>
        <asp:FileUpload ID="fuFile" runat="server" />
        <br />
     <span class="modPopup_mainTitle" >Prikazovanje</span>
       
        <asp:CheckBox ID="cbActive" CssClass="modPopup_CBXsettings" runat="server" Checked="true" Text="&nbsp;&nbsp;Aktivna (prikazovanje slike na spletni strani)" />
        <cc1:ToggleButtonExtender ID="ToggleEx" runat="server"
            TargetControlID="cbActive" 
            ImageWidth="19" 
            ImageHeight="19"
            CheckedImageAlternateText="Odkljukaj"
            UncheckedImageAlternateText="Obkljukaj"
            UncheckedImageUrl="~/_inc/images/design/checkbox_off.gif" 
            CheckedImageUrl="~/_inc/images/design/checkbox_on.gif"
            DisabledCheckedImageUrl="~/_inc/images/design/checkbox_disabled.gif"
            DisabledUncheckedImageUrl="~/_inc/images/design/checkbox_disabled.gif" />

        <br />
        <br />
    <span class="modPopup_mainTitle" >Ostali podatki</span>

        Avtor slike:&nbsp;<asp:TextBox ID="tbAuthor" class="modPopup_TBXsettings" runat="server"></asp:TextBox>
        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1"  WatermarkText="Tukaj vpiši avtorja slike" TargetControlID ="tbAuthor" WatermarkCssClass="modPopup_mainWatermark" runat="server" ></cc1:TextBoxWatermarkExtender>

        <br />
<%--        <asp:LinkButton ValidationGroup="file" ID="btUpload" runat="server" OnClick="btUpload_Click"  Text="Add image(s)" />

        <asp:LinkButton CausesValidation="false" ID="btCancel" runat="server" OnClick="btCancel_Click"  Text="Cancel" />--%>
        
        
        <br /><br />



