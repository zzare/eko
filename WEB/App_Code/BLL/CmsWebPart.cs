﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Security.Principal;
using System.Web.Caching;
using System.Net.Mail;
using System.Collections.Generic;
using System.Threading;

/// <summary>
/// Summary description for CMS_WEB_PART
/// </summary>

public partial class CMS_WEB_PART
{

    public void Delete() {
        DeleteCwp(this.CWP_ID, true);
    }


    /**********************       STATIC FUNCTIONS    *********************/
    public static CMS_WEB_PART GetWebpartByID(Guid id)
    {
        eMenikDataContext db = new eMenikDataContext();
        return db.CMS_WEB_PARTs.SingleOrDefault(w=>w.CWP_ID == id);
    }
    public static CMS_WEB_PART GetWebpartByID(Guid id, string user)
    {
        eMenikDataContext db = new eMenikDataContext();
        return (from c in db.CMS_WEB_PARTs 
                from u in db.EM_USERs 
                where c.CWP_ID == id && c.UserId == u.UserId && u.USERNAME == user select c).SingleOrDefault();
        
    }

    public static List<CMS_WEB_PART> GetWebpartsBySitemapLangUserModules(int sitemap, string lang, bool? active, bool? deleted, Guid usr)
    {
        eMenikDataContext db = new eMenikDataContext();
        // all webparts by sitemap and language or lang is null
        IQueryable <CMS_WEB_PART> list = (from w in db.CMS_WEB_PARTs 
                                          from um in db.USER_MODULEs 
                                          from sm in db.SITEMAPs 
                                   where um.MOD_ID == w.MOD_ID && um.UserId == usr && sm.SMAP_ID == sitemap && w.REF_ID == sm.SMAP_GUID   && (w.LANG_ID == lang || w.LANG_ID == null) select w);

        // filter
        if (active != null)
            list = list.Where(w => w.ACTIVE == active.Value);
        if (deleted != null)
            list = list.Where(w => w.DELETED == deleted.Value);

        return list.ToList();
    }
    public static IQueryable <CMS_WEB_PART> GetWebpartsByArticleUserModules(int artID, string lang, bool? active, bool? deleted, Guid usr)
    {
        eMenikDataContext db = new eMenikDataContext();
        // all webparts by sitemap and language or lang is null
        IQueryable<CMS_WEB_PART> list = (from w in db.CMS_WEB_PARTs
                                         from um in db.USER_MODULEs
                                         from a in db.ARTICLEs
                                         where a.ART_ID == artID && w.REF_ID == a.ART_GUID &&  um.MOD_ID == w.MOD_ID && um.UserId == usr && (w.LANG_ID == lang || w.LANG_ID == null)
                                         select w);

        // filter
        if (active != null)
            list = list.Where(w => w.ACTIVE == active.Value);
        if (deleted != null)
            list = list.Where(w => w.DELETED == deleted.Value);

        return list;
    }

    public static IQueryable<CMS_WEB_PART> GetWebpartsByRefIDUserModules(Guid  refID, string lang, bool? active, bool? deleted, Guid usr)
    {
        eMenikDataContext db = new eMenikDataContext();
        // all webparts by sitemap and language or lang is null
        IQueryable<CMS_WEB_PART> list = (from w in db.CMS_WEB_PARTs
                                         from um in db.USER_MODULEs
                                         where w.REF_ID == refID  && um.MOD_ID == w.MOD_ID && um.UserId == usr && (w.LANG_ID == lang || w.LANG_ID == null)
                                         select w);

        // filter
        if (active != null)
            list = list.Where(w => w.ACTIVE == active.Value);
        if (deleted != null)
            list = list.Where(w => w.DELETED == deleted.Value);

        return list;
    }

    public static List<CMS_WEB_PART> GetWebpartsByZoneSitemapLangUserModules(int sitemap, string lang, string zone, bool? active, bool? deleted, Guid usr)
    {
        eMenikDataContext db = new eMenikDataContext();
        // all webparts by sitemap and language or lang is null
        IQueryable<CMS_WEB_PART> list = (from w in db.CMS_WEB_PARTs
                                         from um in db.USER_MODULEs
                                         from sm in db.SITEMAPs
                                         where um.MOD_ID == w.MOD_ID && um.UserId == usr && sm.SMAP_ID == sitemap && w.REF_ID == sm.SMAP_GUID && w.ZONE_ID == zone && (w.LANG_ID == lang || w.LANG_ID == null)
                                   orderby w.ORDER ascending, w.DATE_ADDED descending
                                   select w);

        // filter
        if (active != null)
            list = list.Where(w => w.ACTIVE == active.Value);
        if (deleted != null)
            list = list.Where(w => w.DELETED == deleted.Value);

        return list.ToList();
    }
    public static List<CMS_WEB_PART> GetWebpartsByZoneSitemapLang(int sitemap, string lang, string zone, bool? active, bool? deleted)
    {
        eMenikDataContext db = new eMenikDataContext();
        // all webparts by sitemap and language or lang is null
        IQueryable<CMS_WEB_PART> list = (from w in db.CMS_WEB_PARTs
                                         from sm in db.SITEMAPs
                                   where  sm.SMAP_ID == sitemap && w.REF_ID == sm.SMAP_GUID && w.ZONE_ID == zone && (w.LANG_ID == lang || w.LANG_ID == null)
                                   orderby w.ORDER ascending, w.DATE_ADDED descending
                                   select w);

        // filter
        if (active != null)
            list = list.Where(w => w.ACTIVE == active.Value);
        if (deleted != null)
            list = list.Where(w => w.DELETED == deleted.Value);

        return list.ToList();
    }

    public static List<CMS_WEB_PART> GetWebpartsByZoneRefIDLang(Guid? refID, string lang, string zone, bool? active, bool? deleted)
    {
        eMenikDataContext db = new eMenikDataContext();
        // all webparts by sitemap and language or lang is null
        IQueryable<CMS_WEB_PART> list = (from w in db.CMS_WEB_PARTs
                                         where w.REF_ID == refID  && w.ZONE_ID == zone && (w.LANG_ID == lang || w.LANG_ID == null)
                                         orderby w.ORDER ascending, w.DATE_ADDED descending
                                         select w);

        // filter
        if (active != null)
            list = list.Where(w => w.ACTIVE == active.Value);
        if (deleted != null)
            list = list.Where(w => w.DELETED == deleted.Value);

        return list.ToList();
    }


    public static CMS_WEB_PART CreateNewWebPart(Guid?  refID, string lang, int mod, string zone, Guid user, string title, string desc, bool active, bool deleted, int order, bool showTitle, bool enablePaging, int pageSize, int? idInt)
    {

        eMenikDataContext db = new eMenikDataContext();

        CMS_WEB_PART wp = new CMS_WEB_PART();
        wp.CWP_ID = Guid.NewGuid();
        wp.REF_ID   = refID ;
        wp.LANG_ID = string.IsNullOrEmpty(lang ) ? null : lang;
        wp.MOD_ID = mod;
        wp.ZONE_ID = zone;
        wp.UserId = user;
        wp.TITLE = title;
        wp.DESC = desc;
        wp.ACTIVE = active;
        wp.DELETED = deleted;
        wp.ORDER = order;
        wp.SHOW_TITLE = showTitle;
        wp.PAGE_ENABLE = enablePaging;
        wp.PAGE_SIZE = pageSize;
        wp.DATE_ADDED = DateTime.Now;
        wp.ROLES_VIEW = "";
        wp.ROLES_EDIT = "";
        wp.ID_INT = idInt ;

        db.CMS_WEB_PARTs.InsertOnSubmit(wp);
        db.SubmitChanges();

        return wp;    
    }

    public static CMS_WEB_PART CreateNewCwpContent(string content, Guid?  smap_guid, string lang, int mod, string zone, Guid user, string title, string desc, bool active, bool deleted, int order, bool showTitle, bool enablePaging, int pageSize)
    {
        CMS_WEB_PART wp =  CreateNewWebPart(smap_guid, lang, mod, zone, user, title, desc, active, deleted, order, showTitle, enablePaging, pageSize, null);

        eMenikDataContext db = new eMenikDataContext();

        CONTENT con = new CONTENT();
        con.CWP_ID = wp.CWP_ID;
        con.CONT_BODY = content ?? "";
        con.SMAP_ID = null ;
        con.LANG_ID = wp.LANG_ID;
        con.DATE_ADDED = DateTime.Now;
        con.VIEW_COUNT = 0;
        con.ACTIVE = true;
        con.CON_ORDER = 0;

        db.CONTENTs.InsertOnSubmit(con);

        db.SubmitChanges();

        return wp;
    }

    public static CMS_WEB_PART CreateNewCwpArticle(int insertCount, string artTitle, string artShort, string artContent, Guid?  refID, string lang, int mod, string zone, Guid user, string title, string desc, bool active, bool deleted, int order, bool showTitle, bool enablePaging, int pageSize)
    {
        CMS_WEB_PART wp = CreateNewWebPart(refID, lang, mod, zone, user, title, desc, active, deleted, order, showTitle, enablePaging, pageSize, null);

        eMenikDataContext db = new eMenikDataContext();

        string usr = "";
        if (HttpContext.Current != null)
            if (HttpContext.Current.User.Identity.IsAuthenticated )
                usr = HttpContext.Current.User.Identity.Name;

        for (int i = 1; i <= insertCount; i++)
        {
            ARTICLE art = ARTICLE.CreateArticle(null, usr, wp.CWP_ID, artTitle + " " + i.ToString(), artShort + " " + i.ToString(), artContent + " " + i.ToString());

            // insert CWP for gallery
            CMS_WEB_PART wpg = CMS_WEB_PART.CreateNewCwpGallery(SM.BLL.Common.GalleryType.DefaultArticle, art.ART_GUID, null, MODULE.Common.GalleryModID, ARTICLE.Common.MAIN_GALLERY_ZONE, user, "Galerija", "", true, false, -1, false, false, 3, SM.BLL.Common.ImageType.ArticleDetailsThumbDefault, SM.BLL.Common.ImageType.EmenikBigDefault);
        
        }

        CWP_ARTICLE cwpA = new CWP_ARTICLE();
        cwpA.CWP_ID = wp.CWP_ID;
        cwpA.LIST_SHOW_ABSTRACT = true;
        cwpA.LIST_SHOW_ARCHIVE = true;
        db.CWP_ARTICLEs.InsertOnSubmit(cwpA);
        db.SubmitChanges();


        return wp;
    }

    public static CMS_WEB_PART CreateNewCwpGallery(int galType, Guid? refID, string lang, int mod, string zone, Guid user, string title, string desc, bool active, bool deleted, int order, bool showTitle, bool enablePaging, int pageSize)
    { 
        return CreateNewCwpGallery(galType, refID, lang, mod, zone, user, title, desc, active, deleted, order, showTitle, enablePaging, pageSize, SM.BLL.Common.ImageType.EmenikGalleryThumbDefault, SM.BLL.Common.ImageType.EmenikBigDefault);
    }
    public static CMS_WEB_PART CreateNewCwpGallery(int galType,  Guid? refID, string lang, int mod, string zone, Guid user, string title, string desc, bool active, bool deleted, int order, bool showTitle, bool enablePaging, int pageSize, int thumbType, int bigType)
    {

        eMenikDataContext db = new eMenikDataContext();

        GALLERY gal = new GALLERY();
        gal.GAL_TYPE = galType;
        db.GALLERies.InsertOnSubmit(gal);
        db.SubmitChanges();

        // copy image types for gallery type
        List<GALLERY_TYPE_IMAGE_TYPE> list = db.GALLERY_TYPE_IMAGE_TYPEs.Where(w => w.GAL_TYPE == galType).ToList();
        foreach (GALLERY_TYPE_IMAGE_TYPE item in list)
        {
            GALLERY_IMAGE_TYPE type = new GALLERY_IMAGE_TYPE { GAL_ID = gal.GAL_ID, TYPE_ID = item.TYPE_ID };
            db.GALLERY_IMAGE_TYPEs.InsertOnSubmit(type);
        }
        db.SubmitChanges();


        CMS_WEB_PART wp = CreateNewWebPart(refID, lang, mod, zone, user, title, desc, active, deleted, order, showTitle, enablePaging, pageSize, gal.GAL_ID);


        CWP_GALLERY cwpGal = new CWP_GALLERY();
        cwpGal.CWP_ID = wp.CWP_ID;
        cwpGal.THUMB_TYPE_ID = thumbType ;
        cwpGal.BIG_TYPE_ID = bigType ;
        db.CWP_GALLERies.InsertOnSubmit(cwpGal);

        db.SubmitChanges();

        return wp;
    }

    public static CMS_WEB_PART CreateNewCwpContactForm(Guid? refID, string lang, int mod, string zone, Guid user, string title, string desc, bool active, bool deleted, int order, bool showTitle, bool enablePaging, int pageSize)
    {
        CMS_WEB_PART wp = CreateNewWebPart(refID, lang, mod, zone, user, title, desc, active, deleted, order, showTitle, enablePaging, pageSize, null);

        eMenikDataContext db = new eMenikDataContext();

        CWP_CONTACT_FORM con = new CWP_CONTACT_FORM();
        con.CWP_ID = wp.CWP_ID;

        db.CWP_CONTACT_FORMs.InsertOnSubmit(con);

        db.SubmitChanges();

        return wp;
    }

    public static CMS_WEB_PART CreateNewCwpMap(Guid? refID, string lang, int mod, string zone, Guid user, string title, string desc, bool active, bool deleted, int order, bool showTitle, bool enablePaging, int pageSize)
    {
        CMS_WEB_PART wp = CreateNewWebPart(refID, lang, mod, zone, user, title, desc, active, deleted, order, showTitle, enablePaging, pageSize, null);

        eMenikDataContext db = new eMenikDataContext();

        EM_USER em_user = EM_USER.GetUserByID(user);

        CWP_MAP map = new CWP_MAP();
        map.CWP_ID = wp.CWP_ID;
        string address = "";

        if (em_user.SHOW_ADDRESS) {

            address += em_user.CONT_STREET;
            if (!String.IsNullOrEmpty(address))
                address += " ";
            address += em_user.CONT_POSTAL_CODE;
            if (!String.IsNullOrEmpty(address))
                address += " ";
            address += em_user.CONT_CITY;
        }
        map.ADDRESS = address;
        map.SRCH_ADDRESS = address;
        map.TITLE = em_user.PAGE_TITLE;
        map.ZOOM = 13;


        db.CWP_MAPs.InsertOnSubmit(map);

        db.SubmitChanges();

        return wp;
    }

    public static CMS_WEB_PART CreateNewCwpPoll(Guid? refID, string lang, int mod, string zone, Guid user, string title, string desc, bool active, bool deleted, int order, bool showTitle, bool enablePaging, int pageSize)
    {
        CMS_WEB_PART wp = CreateNewWebPart(refID, lang, mod, zone, user, title, desc, active, deleted, order, showTitle, enablePaging, pageSize, null);

        //POLL poll = POLL.InsertNewPoll(wp.CWP_ID, user, true, POLL.Common.BlockMode.IP, 10);
        //POLL.SavePollLang(poll.PollID, lang, "Primer ankete: Kakšno naj bo dobro anketno vprašanje?", true);
        //POLL.SavePollChoice(Guid.Empty, poll.PollID, lang, "Ne vem", 1);
        //POLL.SavePollChoice(Guid.Empty, poll.PollID, lang, "Vem", 2);

        return wp;
    }

    public static CMS_WEB_PART CreateNewCwp(Guid usr, Guid? refID, string lang, int modID, string zoneName, int pageModuleId, int pos)
    {
        //MODULE mod = MODULE.GetModuleByID(modID);
        ZONE_MODULE zmod = ZONE_MODULE.GetZoneModule(pageModuleId, modID, zoneName);

        CMS_WEB_PART wp = new CMS_WEB_PART();
        switch (modID)
        {
            case MODULE.Common.ContentModID :
                // create new CMS web part
                wp = CMS_WEB_PART.CreateNewCwpContent(zmod.DEFAULT_TEXT, refID, lang, modID, zoneName, usr, zmod.DEFAULT_TITLE, "", false, false, -1, false, false, 0);

                break;
            case MODULE.Common.ArticleModID:
                // create new CMS web part (2 articles)
                wp = CMS_WEB_PART.CreateNewCwpArticle(1, zmod.DEFAULT_TITLE, "Kratek opis novice", zmod.DEFAULT_TEXT, refID, lang, modID, zoneName, usr, zmod.DEFAULT_TITLE, "", false, false, -1, false, false, 5);

                break;
            case MODULE.Common.GalleryModID :
                // create new CMS gallery
                wp = CMS_WEB_PART.CreateNewCwpGallery(SM.BLL.Common.GalleryType.DefaultEmenikGallery, refID, lang, modID, zoneName, usr, zmod.DEFAULT_TITLE, "", false, false, -1, false, false, 3);

                break;
            case MODULE.Common.ContactFormModID:
                // create new CMS gallery
                wp = CMS_WEB_PART.CreateNewCwpContactForm(refID, lang, modID, zoneName, usr, zmod.DEFAULT_TITLE, "", false, false, -1, false, false, 3);

                break;
            case MODULE.Common.MapModID:
                // create new CMS gallery
                wp = CMS_WEB_PART.CreateNewCwpMap(refID, lang, modID, zoneName, usr, zmod.DEFAULT_TITLE, "", false, false, -1, false, false, 3);
                break;
            case MODULE.Common.PollModID:
                // create new CMS gallery
                wp = CMS_WEB_PART.CreateNewCwpPoll(refID, lang, modID, zoneName, usr, zmod.DEFAULT_TITLE, "", false, false, -1, false, false, 1);
                break;
            default:
                throw new Exception("module ne obstaja");
                break;
        }

        CMS_WEB_PART.MoveCWPtoPosition(wp.REF_ID , lang, wp.CWP_ID, zoneName, pos);

        return wp;

    }

    public static void DeleteCwp(Guid id, bool delete){
        eMenikDataContext db = new eMenikDataContext();
        CMS_WEB_PART wp = db.CMS_WEB_PARTs.Single(w => w.CWP_ID == id);
        if (wp == null) return;

        wp.DELETED = delete;
        db.SubmitChanges();
    }

    public static void DeleteCWPsBySitemap(int smap)
    {
        eMenikDataContext db = new eMenikDataContext();
        SITEMAP smp = SITEMAP.GetSiteMapByID(smap);
        if (smp == null)
            return;

        DeleteCWPsByRefID(smp.SMAP_GUID);
    }
    public static void DeleteCWPsByRefID(Guid refID)
    {
        eMenikDataContext db = new eMenikDataContext();

        // delete all articles
        var a = from l in db.CWP_ARTICLEs from c in db.CMS_WEB_PARTs where c.REF_ID  == refID  && l.CWP_ID == c.CWP_ID select l;
        db.CWP_ARTICLEs.DeleteAllOnSubmit(a);

        // delete all galleries
        var g = from l in db.CWP_GALLERies from c in db.CMS_WEB_PARTs where c.REF_ID == refID && l.CWP_ID == c.CWP_ID select l;
        db.CWP_GALLERies.DeleteAllOnSubmit(g);

        // delete all contact form
        var cf = from l in db.CWP_CONTACT_FORMs from c in db.CMS_WEB_PARTs where c.REF_ID == refID && l.CWP_ID == c.CWP_ID select l;
        db.CWP_CONTACT_FORMs.DeleteAllOnSubmit(cf);

        // delete all maps
        var m = from l in db.CWP_MAPs from c in db.CMS_WEB_PARTs where c.REF_ID == refID && l.CWP_ID == c.CWP_ID select l;
        db.CWP_MAPs.DeleteAllOnSubmit(m);

        // delete all CWP in polls
        var p = (from l in db.POLLs where l.CWP_ID == refID select l).ToList();
        p.ForEach(w => w.CWP_ID = null);


        List<CMS_WEB_PART> wp = db.CMS_WEB_PARTs.Where(w => w.REF_ID == refID).ToList();
        db.CMS_WEB_PARTs.DeleteAllOnSubmit(wp);
        db.SubmitChanges();
    }


    public static void UpdateActive(Guid  cwpID, bool active)
    {
        eMenikDataContext db = new eMenikDataContext();

        CMS_WEB_PART  cwp = (from c in db.CMS_WEB_PARTs 
                        where c.CWP_ID  == cwpID 
                        select c).SingleOrDefault();
        cwp.ACTIVE = active;
        db.SubmitChanges();
    }
    public static void UpdateActiveSecure(Guid  cwpID, bool active, string username)
    {

        eMenikDataContext db = new eMenikDataContext();

        CMS_WEB_PART cwp = (from c in db.CMS_WEB_PARTs
                        from u in db.EM_USERs 
                            where c.CWP_ID == cwpID && c.UserId == u.UserId && u.USERNAME == username
                        select c).SingleOrDefault();
        cwp.ACTIVE = active;
        db.SubmitChanges();
    }
    public static void UpdateOrder(Guid cwpID, int order, string zone)
    {
        eMenikDataContext db = new eMenikDataContext();

        CMS_WEB_PART cwp = (from c in db.CMS_WEB_PARTs
                            where c.CWP_ID == cwpID
                            select c).SingleOrDefault();
        cwp.ORDER = order < 0? 0 : order;
        cwp.ZONE_ID = zone;
        db.SubmitChanges();
    }
    public static void UpdateID(Guid cwpID, int id)
    {
        eMenikDataContext db = new eMenikDataContext();

        CMS_WEB_PART cwp = (from c in db.CMS_WEB_PARTs
                            where c.CWP_ID == cwpID
                            select c).SingleOrDefault();
        cwp.ID_INT = id;
        db.SubmitChanges();
    }
    public static void UpdateID(Guid cwpID, Guid  id)
    {
        eMenikDataContext db = new eMenikDataContext();

        CMS_WEB_PART cwp = (from c in db.CMS_WEB_PARTs
                            where c.CWP_ID == cwpID
                            select c).SingleOrDefault();
        cwp.ID_GUID = id;
        db.SubmitChanges();
    }

    public static bool MoveCWPtoPosition(Guid id, string zone, int pos, string user) {
        CMS_WEB_PART cwp =  CMS_WEB_PART.GetWebpartByID(id);

        // check if module can be dragged to new zone
        //if( !MODULE.IsCwpAllowedInZone(cwp.CWP_ID, zone, user))
        //    return false;

        MoveCWPtoPosition(cwp.REF_ID , LANGUAGE.GetCurrentLang(), id, zone, pos);

        return true;
    
    }

    public static void MoveCWPtoPosition(Guid? refID, string lang, Guid id, string zone, int pos)
    {

        int step = 5;
        int newOrder = step;// order for first item in list

        // get all webparts in zone
        List<CMS_WEB_PART> list = GetWebpartsByZoneRefIDLang(refID, lang, zone, null, false);
        // ...without current one
        list = list.Where(w => w.CWP_ID != id).ToList();

        // item is only one
        if (list.Count == 0)
        {
            UpdateOrder(id, newOrder, zone);
            return;
        }

        // item is last one
        if (list.Count <= pos)
        {
            newOrder = list[list.Count - 1].ORDER + step;
            UpdateOrder(id, newOrder, zone);
            return;
        }

        int ord = step;
        // reorder entire list
        for (int i = 0; i < list.Count(); i++)
        {

            if (i == pos || (pos < 0 && i==0))
            {
                newOrder = ord;
                ord += step;
            }
            UpdateOrder(list[i].CWP_ID, ord, zone);

            ord += step;
        }

        UpdateOrder(id, newOrder , zone);
    }

    public static CMS_WEB_PART SaveWebpartSettings(Guid id, string user, string title, bool active, int pageSize, int displayType)
    {
        return SaveWebpartSettings(id, user, title, active, pageSize , displayType , null);
    }

    public static CMS_WEB_PART SaveWebpartSettings(Guid id, string user, string title, bool active, int pageSize, int displayType, bool? enablePaging)
    {
        eMenikDataContext db = new eMenikDataContext();
        CMS_WEB_PART cwp =  (from c in db.CMS_WEB_PARTs 
                             from u in db.EM_USERs 
                             where c.CWP_ID == id && c.UserId == u.UserId && u.USERNAME == user select c).SingleOrDefault();
        if (cwp == null)
            return cwp;

        if (pageSize > -1)
            cwp.PAGE_SIZE = pageSize;
        if (enablePaging  != null)
            cwp.PAGE_ENABLE  = enablePaging.Value  ;

        cwp.TITLE = title;
        cwp.SHOW_TITLE = active;
        if (displayType > -1)
            cwp.DISPLAY_TYPE = displayType;


        db.SubmitChanges();
        return cwp;
    }


    public static CMS_WEB_PART UpdateCwpID(Guid cwpid, string user, Guid id)
    {
        eMenikDataContext db = new eMenikDataContext();
        CMS_WEB_PART cwp = (from c in db.CMS_WEB_PARTs
                            from u in db.EM_USERs
                            where c.CWP_ID == cwpid && c.UserId == u.UserId && u.USERNAME == user
                            select c).SingleOrDefault();
        if (cwp == null)
            return cwp;
        cwp.ID_GUID = id;
        db.SubmitChanges();
        return cwp;
    }




    public static CWP_GALLERY  GetCwpGalleryByID(Guid id)
    {
        eMenikDataContext db = new eMenikDataContext();
        return db.CWP_GALLERies.SingleOrDefault(w => w.CWP_ID == id);
    }
    public static CWP_ARTICLE GetCwpArticleByID(Guid id)
    {
        eMenikDataContext db = new eMenikDataContext();
        return db.CWP_ARTICLEs.SingleOrDefault(w => w.CWP_ID == id);
    }

    public static CWP_CONTACT_FORM GetCwpContactFormByID(Guid id)
    {
        eMenikDataContext db = new eMenikDataContext();
        return db.CWP_CONTACT_FORMs.SingleOrDefault(w => w.CWP_ID == id);
    }

    public static CWP_MAP GetCwpMapByID(Guid id)
    {
        eMenikDataContext db = new eMenikDataContext();
        return db.CWP_MAPs.SingleOrDefault(w => w.CWP_ID == id);
    }

    public static bool UpdateCwpMap(string uname, Guid id, double lat, double lng, int zoom, double mLat, double mLng, string title, string addr, string srch, int height, int width, bool showInfo, string mapType)
    {
        eMenikDataContext db = new eMenikDataContext();
        CWP_MAP map = (from m in db.CWP_MAPs from c in db.CMS_WEB_PARTs from em in db.EM_USERs where em.USERNAME == uname && c.UserId == em.UserId && m.CWP_ID == c.CWP_ID && m.CWP_ID == id select m ).SingleOrDefault();
        if (map == null) return false;

        map.LAT = lat;
        map.LNG = lng;
        map.ZOOM = zoom;
        map.MARKER_LAT = mLat;
        map.MARKER_LNG = mLng;
        map.TITLE = title;
        map.ADDRESS = addr;
        map.SRCH_ADDRESS = srch;
        map.HEIGHT = height;
        map.WIDTH = width;
        map.SHOW_INFO = showInfo;
        map.MAP_TYPE = mapType;

        db.SubmitChanges();
        return true;
    }

    public static bool UpdateCwpGallery(Guid id, int? galID, int ttype, int bigType)
    {
        eMenikDataContext db = new eMenikDataContext();
        CWP_GALLERY g = db.CWP_GALLERies.SingleOrDefault(w => w.CWP_ID == id);
        if (g == null) {
            g = new CWP_GALLERY { CWP_ID = id, THUMB_TYPE_ID = SM.BLL.Common.ImageType.EmenikThumbDefault };
            db.CWP_GALLERies.InsertOnSubmit(g);
        
        }
        int ttype_old = -1;
        if (g.THUMB_TYPE_ID != null)
            ttype_old = g.THUMB_TYPE_ID.Value;

        if (ttype > 0)
            g.THUMB_TYPE_ID = ttype;
        if (bigType > 0)
            g.BIG_TYPE_ID = bigType;

        db.SubmitChanges();

        // insert new images for new type where not exist (for existing images in gallery)
        if (galID != null && galID > 0 && ttype > 0 && ttype_old != ttype ){
            IMAGE.Func.AutoCreateImagesForGalleryForType(galID.Value, ttype);
        }

        return true;
    }

    public static bool UpdateCwpArticle(Guid id, int ttype, bool showImages, bool showAbstract, bool showArchive)
    {
        eMenikDataContext db = new eMenikDataContext();
        CWP_ARTICLE g = db.CWP_ARTICLEs.SingleOrDefault(w => w.CWP_ID == id);
        if (g == null)
        {
            g = new CWP_ARTICLE { CWP_ID = id, IMG_TYPE_THUMB = SM.BLL.Common.ImageType.ArticleThumbDefault  };
            db.CWP_ARTICLEs.InsertOnSubmit(g);

        }
        int ttype_old = -1;
        bool show_old = g.SHOW_IMAGE;
        if (g.IMG_TYPE_THUMB != null)
            ttype_old = g.IMG_TYPE_THUMB.Value;

        if (ttype > 0)
            g.IMG_TYPE_THUMB = ttype;

        // update
        g.SHOW_IMAGE = showImages;
        g.LIST_SHOW_ABSTRACT = showAbstract;
        g.LIST_SHOW_ARCHIVE = showArchive;

        db.SubmitChanges();

        // insert new images for new type where not exist (for existing images in all galleries)
        if (showImages && ttype > 0 && (ttype_old != ttype || show_old == false ))
        {
            IMAGE.Func.AutoCreateImagesForImageList(GALLERY.GetImageOrigsFromCWPArticleWithoutType(id, ttype), ttype);
        }

        return true;
    }



    public class Common {


        public class DisplayType {
            public const int ART_NORMAL = 1;
            public const int ART_2 = 2;
            public const int ART_WITH_SUBCATS = 3;

        
        
        }


        public static string RenderHiddenClass(CMS_WEB_PART cwp) {
            if (!cwp.ACTIVE) return "cwpHidden";
            return "";
        }
    
    }

}
