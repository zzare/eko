﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SM.EM.BLL;
namespace SM.EM.UI.Controls
{
    public partial class _cntCommentList : BaseControl
    {
#region Property


        private string containerCssClass = "contArticles";
        public string ContainerCssClass
        {
            get { return this.containerCssClass; }
            set { this.containerCssClass = value; }
        }
        private string appendToTitle = "&nbsp;&raquo;";
        public string AppendToTitle
        {
            get { return this.appendToTitle; }
            set { this.appendToTitle = value; }
        }

        private bool showImage = false;
        public bool ShowImage
        {
            get { return this.showImage; }
            set { this.showImage = value; }
        }
        private bool showDesc = true;
        public bool ShowDesc
        {
            get { return this.showDesc; }
            set { this.showDesc = value; }
        }
        private int titleLength = -1;
        public int TitleLength
        {
            get { return this.titleLength; }
            set { this.titleLength = value; }
        }
        private int descLength = -1;
        public int DescLength
        {
            get { return this.descLength; }
            set { this.descLength = value; }
        }
        private int displayCount = 10;
        public int DisplayCount
        {
            get { return this.displayCount; }
            set { this.displayCount = value; }
        }
        private string articleDetailsPath = "~/c/ArticleDetails.aspx";
        public string ArticleDetailsPath
        {
            get { return this.articleDetailsPath; }
            set { this.articleDetailsPath = value; }
        }
        private bool isEditMode = false;
        public bool IsEditMode
        {
            get { return this.isEditMode; }
            set { this.isEditMode = value; }
        }
        private bool autoLoad = true;
        public bool AutoLoad
        {
            get { return this.autoLoad; }
            set { this.autoLoad = value; }
        }
        public Guid  CwpID{get;set;}

        public string PageName {get;set;}

        public int PageNum
        {
            get { int ret =  (int)(ParentPage.GetViewStateValue(this, "PageNum") ?? 1);
            if (ret < 1) ret = 1;
            return ret;
            }
            set { ParentPage.SetViewStateValue (this, "PageNum", value); }
        }
        public int PageSize
        {
            get { int ret =  (int)(ParentPage.GetViewStateValue(this, "PageSize") ?? 10000 );
            if (ret < 1) ret = 10000;
            return ret;

            }
            set { ParentPage.SetViewStateValue (this, "PageSize", value); }
        }

        public int RefInt { get; set; }
        public bool CanEditComment { get; set; }


#endregion

        public delegate void OnEditEventHandler(object sender,SM.BLL.Common.ClickIDEventArgs  e);
        public event OnEditEventHandler OnEditCommentClick;
        public event OnEditEventHandler OnDeleteCommentClick;
        public string ForceTitle { get; set; }
        public int ComentsCount;


        protected void InitData() {

            //divArticles.Attributes["class"] = ContainerCssClass;
            //dpArt.PageSize = PageSize;
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
                if (AutoLoad) BindData();

        }
        // bind data
        public void BindData()
        {
            //if (!IsPostBack)
                InitData();
            
            // if group id is set, load articles for group only
            //if (CwpID   != Guid.Empty )
                //listArticles.DataSource = COMMENT.GetComments  (CwpID, IsEditMode, PageNum, PageSize );
            //else// load all articles for current sitemap
                if (RefInt < 1)
                    RefInt = CurrentSitemapID;
                listComments.DataSource = COMMENT.GetCommentsByApproved(RefInt, true, IsEditMode);

                listComments.DataBind();
                
            ComentsCount = listComments.Items.Count;


                if (listComments.Items.Count < 1) { this.Visible = false; return; }
            else this.Visible = true;

            // show/hide prev/next buttons



        }


        protected string RenderArtHref(object item)
        {
            ARTICLE art = (ARTICLE)item;
            if (!String.IsNullOrEmpty(PageName))
                return Page.ResolveUrl(SM.EM.Rewrite.EmenikUserArticleDetailsUrl(PageName,ParentPage.CurrentSitemapID , SM.BLL.Common.PageModule.ArticleDetails, art.ART_ID, art.ART_TITLE));
            else
                return Page.ResolveUrl(ArticleDetailsPath + "?art=" + art.ART_ID.ToString() + "&smp=" + art.SMAP_ID.ToString());

        }
        protected string RenderCommentTitle(object item)
        {
            COMMENT c = (COMMENT)item;

            if (!string.IsNullOrEmpty(ForceTitle))
                return ForceTitle;

            return Server.HtmlEncode( c.Title) ;

        }
        protected string RenderCommentDesc(object item)
        {
            if (!ShowDesc) return "";

            COMMENT c = (COMMENT)item;
            string desc = c.Comment ;

            return Helpers.ConvertToHtml( Server.HtmlEncode( Helpers.CutString(desc, DescLength ))) ;

        }

        protected string RenderDate(object item)
        {
            COMMENT c = (COMMENT)item;

            return string.Format( "{0:dddd, dd.MM.yyyy ob HH:mm} ", c.DateAdded);

        }

        protected string RenderCommentAuthor(object item)
        {
            COMMENT c = (COMMENT)item;
            if ((string.IsNullOrEmpty(c.WWW) || c.WWW == "http://") && c.UserId == Guid.Empty)
                return c.UserName;

            string url = Page.ResolveUrl(c.WWW);
            if (c.UserId != Guid.Empty) { 
                // get users page url
                //var  usr = CUSTOMER .GetCurrentCustomer ();

                return c.UserName;
            
            }

            return string.Format("<a href=\"{0}\">{1}</a> ", url, c.UserName );
        }

        protected string RenderAuthorClass(object item)
        {
            COMMENT c = (COMMENT)item;
            if (c.IsAuthor )
                return " commAuthor";

            return "";
        }

        protected string RenderCommentStart() {
            if (RefInt > 0)
                return "comments" + RefInt.ToString();
            return "";
        }

        void objArticleEdit_OnArticleListChanged(object sender, EventArgs e)
        {
            this.BindData();
        }


        protected void listArticles_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            if (e.CommandName == "editComment")
            {
                OnEditCommentClick(this, new SM.BLL.Common.ClickIDEventArgs { ID = Int32.Parse(e.CommandArgument.ToString()) });            
            }
            if (e.CommandName == "deleteComment")
            {
                int id = Int32.Parse(e.CommandArgument.ToString());
                // delete comment
                COMMENT.DeleteCommentByID(id);
                OnDeleteCommentClick (this, new SM.BLL.Common.ClickIDEventArgs { ID = id });
            }

            if (e.CommandName == "approveComment")
            {
                int id = Int32.Parse(e.CommandArgument.ToString());
                // delete comment
                COMMENT.ApproveCommentByID(id, true);
                OnDeleteCommentClick(this, new SM.BLL.Common.ClickIDEventArgs { ID = id });
            }
        }


    }
}
