﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SM.EM.UI.CMS;
using FredCK.FCKeditorV2;

namespace SM.EM.UI.CMS.Controls
{
    public partial class CMS_cmsArticle : BaseControl
    {
        public int _smapID = -1;
        public string _langID = "-1";

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void LoadTab() {
            listContent.DataBind();

        }


        protected void ldsContent_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {

            if (_smapID == -1) return;
            eMenikDataContext db = new eMenikDataContext();

            var list = from l in db.ARTICLEs  where l.SMAP_ID == _smapID   select l;
            if (_langID != "-1") list = list.Where(l => l.LANG_ID == _langID);

            e.Result = list ;
        }
        protected void listContent_ItemEditing(object sender, ListViewEditEventArgs e)
        {
            

            CloseInsert();
            ((Button)listContent.FindControl("btNew")).Visible = false;
//            ((TextBox )listContent.FindControl("tbExpireDate")).Text = e.
            //listContent.EditIndex = e.NewEditIndex;
        }

        private void CloseInsert()
        {
            listContent.InsertItemPosition = InsertItemPosition.None;
            ((Button)listContent.FindControl("btNew")).Visible = true;
        }
        protected void btNew_Click(object sender, EventArgs e)
        {
            listContent.EditIndex = -1;
            listContent.InsertItemPosition = InsertItemPosition.LastItem ;
            ((Button)sender).Visible = false;
            
        }
        protected void listContent_ItemCanceling(object sender, ListViewCancelEventArgs e)
        {
            if (e.CancelMode == ListViewCancelMode.CancelingInsert || e.CancelMode == ListViewCancelMode.CancelingEdit )
            {
                CloseInsert();
            }
        }

        protected void listContent_ItemInserted(object sender, ListViewInsertedEventArgs e)
        {
            CloseInsert();
        }
        protected void ldsContent_Inserting(object sender, LinqDataSourceInsertEventArgs e)
        {
            ARTICLE art = (ARTICLE)e.NewObject;
            art.LANG_ID = _langID;
            art.DATE_ADDED = DateTime.Now;
            art.DATE_MODIFY = DateTime.Now;
            art.SMAP_ID = _smapID;
            art.APPROVED = true;
            //art.COMMENTS_ENABLED = true;
            //art.MEMBERS_ONLY = false;
            art.ADDED_BY = HttpContext.Current.User.Identity.Name;
        }

        protected void listContent_ItemUpdated(object sender, ListViewUpdatedEventArgs e)
        {
            CloseInsert();
        }

        protected void listContent_ItemCreated(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType == ListViewItemType.InsertItem){
                // preselected checkboxes on INSERT
                CheckBox cb = (CheckBox)e.Item.FindControl("cbActive");
                if (cb != null) cb.Checked = true;
                cb = (CheckBox)e.Item.FindControl("cbApproved");
                if (cb != null) cb.Checked = true;
                cb = (CheckBox)e.Item.FindControl("cbComments");
                if (cb != null) cb.Checked = true;
                cb = (CheckBox)e.Item.FindControl("cbMembers");
                if (cb != null) cb.Checked = false;

                Button btNew = ((Button)listContent.FindControl("btNew"));
                if (btNew != null) btNew.Visible = false;

            }
        }


}
}