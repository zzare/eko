﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_cntScriptPublic.ascx.cs" Inherits="_ctrl_module_product_edit_cntScriptPublic" %>
<script type="text/javascript">

    // ** SHOPPING CART //
    jQuery.fn.fadeInShow = function(speed, easing, callback) {
if ($.browser.msie)
//    return this.show();
    return this.css('display', 'block').css('visibility', 'visible');
else
            //return this.css({ opacity: '0' }).css('visibility', 'visible').fadeIn(speed);
//    return this.css('display', 'block').css('visibility', 'visible').animate({ opacity: 1.0 }, speed, easing, callback);
    return this.css({ opacity: 0.0 }).css('display', 'block').css('visibility', 'visible').animate({ opacity: 1.0 }, speed, easing, callback);
};
jQuery.fn.fadeOutHide = function(speed) {
if ($.browser.msie)
    return this.hide();
else
    return this.fadeOut(speed, function() { $(this).css('visibility', 'hidden') });
//return this.animate({ opacity: 0.0 }, speed, function() { $(this).css('visibility', 'hidden') });
};       
    
    
    
    function refreshShoppingCart(data) {
        var pp = $('#cartPopup').html(data);
        $('#stepShoppingCartList').html(pp.find(".cartItemList").html()).find('.cartBottomL,.cartBottomB_btn').hide();
        pp.find('a.Acart span').css('backgroundPosition', 'top right').parent().css('backgroundPosition', 'top left');
        cufonCart();
        pp.find('.cartW').fadeInShow();        
    }

    function deleteCartItem(id) {
        $("div.ci" + id).slideUp("slow");
        WebServiceP.DeleteCartItem(id, em_g_cult, em_g_lang, em_g_pid, onCartUpdateSuccess, onCartUpdateError);
    }
    function editCartItem(id) {
        var _items = new Array();
        Array.add(_items, em_g_cult);
        Array.add(_items, id);
        Array.add(_items, em_g_lang);
        Array.add(_items, em_g_pid);
        openModalLoading(gmp_mess_loading);
        WebServiceP.RenderView(_items, 1, onEditCartSuccessLoad, onError);
    }
    function onEditCartSuccessLoad(ret) {
        if (ret.Code == 1) {
            setModalLoader(ret.Data);
            closeModalLoading();
        }
        else if (ret.Code == 0) {
            setModalLoaderErr(ret.Message, true);
            return;
        }

        $("#divModalLoader" + mpl).init_mpl().dialog('open').dialog('option', 'width', 610).dialog('option', 'position', 'center').parent().find("div.ui-dialog-titlebar").hide();
    }


    function onCartUpdateSuccess(ret, t) {
        if (ret.Code != 1) {
            setModalLoaderErr(ret.Message, true);
            return;
        }
        refreshShoppingCart(ret.Data);

        if (t)
            $(t).show().css('visibility', 'visible');
    }

    function onCartUpdateError(err, t) {
        logError(err);
        setModalLoaderErr(gmp_mess_error, true);
        if (t)
            $(t).show();        
    }    
    
    var gmp_mess_loading = '<%= Resources.Modules.MessageLoading   %>';
    var gmp_mess_error = '<%= Resources.Modules.MessageError   %>';
    var gmp_mess_submitting = '<%= Resources.Modules.MessageTitleSubmitting   %>';

</script>