﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

/// <summary>
/// Summary description for POLL
/// </summary>
public partial class POLL
{
    /* POLL IP */
    public bool CanVote(string ip)
    {
        if (this.BlockMode == Common.BlockMode.IP) {
            eMenikDataContext db = new eMenikDataContext();
            return !db.POLL_IPADDRESSes.Where(w => w.PollID == this.PollID && w.IPAddress == ip ).Any();  
        }
        else if (this.BlockMode == Common.BlockMode.NONE)
        { 
            return true;
        }
        return false;
    }



    /* static  */

    public static POLL GetPollByID(Guid id) {
        eMenikDataContext db = new eMenikDataContext();
        return db.POLLs.SingleOrDefault(w => w.PollID == id);
    }
    public static POLL GetPollByID(Guid id, Guid usr)
    {
        eMenikDataContext db = new eMenikDataContext();
        return db.POLLs.SingleOrDefault(w => w.PollID == id && w.UserId == usr  );
    }

    public static IEnumerable<POLL> GetPollListByCwpPaged(Guid cwp, bool isEditMode, int pageNum, int pageSize)
    {
        eMenikDataContext db = new eMenikDataContext();

        IEnumerable<POLL> arts;

        arts = from l in db.POLLs where l.CWP_ID == cwp select l;

        if (!isEditMode)
            arts = arts.Where(w => w.Active == true);

        return arts.OrderByDescending(w=>w.DateAdded ).Skip((pageNum-1)*pageSize).Take(pageSize);
    }

    public static IEnumerable<POLL> GetPollListByUser(Guid userid, bool isEditMode, int pageNum, int pageSize)
    {
        eMenikDataContext db = new eMenikDataContext();

        IEnumerable<POLL> arts;

        arts = from l in db.POLLs where l.UserId == userid  select l;

        if (!isEditMode)
            arts = arts.Where(w => w.Active == true);

        return arts.OrderByDescending(w => w.DateAdded).Skip((pageNum - 1) * pageSize).Take(pageSize);
    }
    public static IEnumerable<POLL> GetPollListByUserLang(Guid userid, string lang, bool isEditMode)
    {
        eMenikDataContext db = new eMenikDataContext();

        IEnumerable<POLL> arts;

        arts = from l in db.POLLs from pl in db.POLL_LANGs  where l.UserId == userid && pl.PollID == l.PollID && pl.LANG_ID == lang select l;

        if (!isEditMode)
            arts = arts.Where(w => w.Active == true);

        return arts.OrderByDescending(w => w.DateAdded);
    }


    public static POLL SavePoll(Guid? pollID, Guid? cwpID, Guid? userID, bool active, short blockMode, short order) {
        if (pollID != null && pollID != Guid.Empty)
        {
            // update poll
            return UpdatePoll(pollID, cwpID, userID, active, blockMode, order);
        }
        else { 
        // insert new poll
            return InsertNewPoll(cwpID, userID, active, blockMode, order);
        }
    }

    public static POLL UpdatePoll(Guid? pollID, Guid? cwpID, Guid? userID, bool active, short blockMode, short order) {
        eMenikDataContext db = new eMenikDataContext();

        if (pollID == null)
            return InsertNewPoll(cwpID, userID, active, blockMode, order);

        POLL p = db.POLLs.SingleOrDefault(w => w.PollID == pollID && w.UserId == userID);
        if (p == null)
            return p;
        if (cwpID != null)
            p.CWP_ID = cwpID;
        p.UserId = userID;
        p.Active = active;
        p.BlockMode = blockMode;
        p.Order = order;

        db.SubmitChanges();

        return p;
    
    }
    public static POLL UpdatePollCWP(Guid pollID, Guid cwpID)
    {
        eMenikDataContext db = new eMenikDataContext();

        POLL p = db.POLLs.SingleOrDefault(w => w.PollID == pollID);
        if (p == null)
            return p;
        if (cwpID != null)
        {
            p.CWP_ID = cwpID;
            db.SubmitChanges();
        }
        return p;
    }


    public static POLL InsertNewPoll(Guid? cwpID, Guid? userID, bool active, short blockMode, short order) {
        eMenikDataContext db = new eMenikDataContext();

        POLL p = new POLL();
        p.PollID = Guid.NewGuid();
        //p.CWP_ID = cwpID;
        p.UserId = userID;
        p.Active = active;
        p.BlockMode = blockMode;
        p.Order = order;
        p.DateAdded = DateTime.Now;

        db.POLLs.InsertOnSubmit(p);
        db.SubmitChanges();

        // update cwp id
        if (cwpID != null && cwpID != Guid.Empty )
            CMS_WEB_PART.UpdateID(cwpID.Value , p.PollID);

        return p;
    }

    /*  POLL LANG  */

    public static POLL_LANG GetPollLangByID(Guid id, string langID)
    {
        eMenikDataContext db = new eMenikDataContext();
        return db.POLL_LANGs.SingleOrDefault(w => w.PollID == id && w.LANG_ID == langID);
    }
    public static IEnumerable <POLL_LANG> GetPollLangListByUser(Guid userid, string langID)
    {
        eMenikDataContext db = new eMenikDataContext();
        var list = from pl in db.POLL_LANGs
               from p in db.POLLs
               where p.UserId == userid && pl.PollID == p.PollID && pl.LANG_ID == langID
               select pl;

        //var list = from p in db.POLLs
        //           join t_pl in ( from pls in db.POLL_LANGs orderby (pls.LANG_ID == langID   )  select pls ) on p.PollID equals t_pl.PollID into outer
        //           where p.UserId == userid 
        //           select outer.FirstOrDefault();

        return list;

    }

    public static void SavePollLang(Guid pollID, string lang, string question, bool active)
    {
        eMenikDataContext db = new eMenikDataContext();

        POLL_LANG r = db.POLL_LANGs.SingleOrDefault(w => w.PollID == pollID && w.LANG_ID == lang);

        if (r == null)
        {
            r = new POLL_LANG { PollID = pollID, LANG_ID = lang };
            db.POLL_LANGs.InsertOnSubmit(r);
        }
        r.PollQuestion = question;
        r.Active = active;

        db.SubmitChanges();
    }



    /*  POLL CHOICEs   */
    public static IEnumerable <POLL_CHOICE> GetPollChoiceList(Guid pollId, string langID)
    {
        eMenikDataContext db = new eMenikDataContext();
        var list =  db.POLL_CHOICEs.Where(w => w.PollID == pollId && w.LANG_ID == langID).OrderBy (o=>o.Order );

        return list;
    }

    public static POLL_CHOICE  SavePollChoice(Guid pollChoiceID, Guid pollID, string lang, string choice, int order)
    {
        eMenikDataContext db = new eMenikDataContext();

        POLL_CHOICE r = null ;

        if (pollChoiceID != Guid.Empty )
            r =  db.POLL_CHOICEs.SingleOrDefault(w => w.PollChoiceID == pollChoiceID && w.PollID == pollID && w.LANG_ID == lang);
        
        if (r == null)
        {
            r = new POLL_CHOICE {PollChoiceID = Guid.NewGuid(), PollID = pollID, LANG_ID = lang, Order = (short )order};
            db.POLL_CHOICEs.InsertOnSubmit(r);
        }
        
        r.Choice = choice;
        if (order > -1)
            r.Order = (short) order;

        db.SubmitChanges();

        return r;
    }

    public static POLL_CHOICE VotePollChoice(Guid pollChoiceID, string type, string ip)
    {
        eMenikDataContext db = new eMenikDataContext();

        POLL poll = (from p in db.POLLs from pc in db.POLL_CHOICEs where p.PollID == pc.PollID && pc.PollChoiceID == pollChoiceID select p).SingleOrDefault();
        if (poll == null)
            return null;

        POLL_CHOICE r = db.POLL_CHOICEs.SingleOrDefault(w => w.PollChoiceID == pollChoiceID );
        if (r == null)
            return r;

        if (poll.CanVote(ip))
        {
            // cast vote
            if (type == "F")
                r.VoteCountFemale++;
            else
                r.VoteCountMale++;
            db.SubmitChanges();

            SavePollIP(poll.PollID, ip);
        }
        
        return r;
    }
    


    


    public static void SavePollIP(Guid pollID, string ipaddress)
    {
        eMenikDataContext db = new eMenikDataContext();
        POLL_IPADDRESS ip = db.POLL_IPADDRESSes.SingleOrDefault(w=>w.PollID == pollID && w.IPAddress == ipaddress  );
        if (ip != null)
            return;

        ip = new POLL_IPADDRESS { PollID = pollID, IPAddress = ipaddress };
        db.POLL_IPADDRESSes.InsertOnSubmit(ip);
        db.SubmitChanges();
    }




    public static string RenderPollUpdateCommand(HttpContext context)
    {
        string ret = "";
        // check if it is first time
        if (context.Items["RenderPollUpdateCommand"] != null)
            if ((int)context.Items["RenderPollUpdateCommand"] == 1)
                return ret;
        
        // remember for possible next poll on same page
        context.Items["RenderPollUpdateCommand"] = 1;

        // get current page
        ret = "getPollResultsIfVoted();";
        ret = " $(document).ready(function(){ " + ret + " });";
        ret = "<script type=\"text/javascript\">" + ret + "</script>";

        //ret = DateTime.Now.ToString() + ret;
        return ret ;
    
    }


    public class Common {
//        public const string MAP_DOMAIN = "~";

        public class BlockMode {
            public const short NONE = 0;
            public const short IP = 1;
            public const short COOKIE = 2;

        
        }

        
    
    }

}
