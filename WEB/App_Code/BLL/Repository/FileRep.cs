﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ProductRep
/// </summary>
public class FileRep
{

    private eMenikDataContext  db = new eMenikDataContext ();

   
    // Query Methods




    //public CUSTOMER GetCustomerByID(Guid id)
    //{
    //    return db.CUSTOMERs.SingleOrDefault(w => w.CustomerID  == id);
    //}

    //public COMPANY GetCompanyByID(int id)
    //{
    //    return db.COMPANies.SingleOrDefault(w => w.COMPANY_ID == id);
    //}

    //public ADDRESS GetAddressByID(int id)
    //{
    //    return db.ADDRESSes.SingleOrDefault(w => w.ADDR_ID == id);
    //}


    public IQueryable<Data.PRODUCT_FILE> GetFileListByProductLang(Guid prodID, string lang)
    {
        var rec = from f in db.FILEs
                  from pf in db.PRODUCT_FILE_LANGs 
                  where pf.PRODUCT_ID == prodID  && pf.LANG_ID == lang && pf.FileID == f.FileID 
                  orderby f.Name ascending
                  select new Data.PRODUCT_FILE{ FileID = f.FileID, DateAdded = f.DateAdded, Desc = f.Desc , FileType = f.FileType, Folder = f.Folder, Name = f.Name, RefID = f.RefID , Type = f.Type, UserId = f.UserId,
                     ProdFileID = pf.ProdFileID, PRODUCT_ID = pf.PRODUCT_ID, LANG_ID = pf.LANG_ID };
        return rec;
    }

    public IQueryable<FILE> GetFileListByPage(Guid pageID, short fileType)
    {
        var rec = from f in db.FILEs
                  where f.UserId == pageID && f.FileType == fileType 
                  orderby f.Name ascending
                  select f;
        return rec;
    }






    public void AddProductFileLang(PRODUCT_FILE_LANG pfile)
    {
        // if file exists, don't add new
        if (db.PRODUCT_FILE_LANGs.Where(w => w.LANG_ID == pfile.LANG_ID && w.PRODUCT_ID == pfile.PRODUCT_ID && w.FileID == pfile.FileID).Any())
            return;

        db.PRODUCT_FILE_LANGs.InsertOnSubmit(pfile );

    }




    //
    // Persistence


    public bool  Save()
    {
        try
        {
            db.SubmitChanges();
        }
        catch (Exception ex)
        {
            ERROR_LOG.LogError( ex, "Save FILE");

            return false;
        }
        return true;        
    }


    public class Data{
    
    
        public class PRODUCT_FILE : FILE {

        public Guid ProdFileID { get; set; }
        public Guid PRODUCT_ID { get; set; }
        public Guid FileID { get; set; }
        public string LANG_ID { get; set; }
    
    
    }
    
    }





}