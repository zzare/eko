﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.Caching;
/*using SM.ST.BLL.ST;*/
using System.Collections.Generic;
using System.Xml.Linq;

/// <summary>
/// Summary description for CMSSiteMapProvider
/// </summary>
public class CMSSiteMapProvider : StaticSiteMapProvider 
{

    private readonly object siteMapLock = new object(); 
    private SiteMapNode root = null;
    private  int menuID = 1;
    public const string CacheDependencyKey = "CMSSiteMapProviderCacheDependency";
     
    protected void AddAllChildNodes(int sitemapID, SiteMapNode parent) {
        string smapKey;
        string smapUrl = string.Empty;
        string[] roleList;
        string smapTitle = "";

        // add all child nodes
        IEnumerable<em_GetSitemapNavByParentByLangResult> childs = SITEMAP.GetSiteMapNavByParentByLang(sitemapID, "si");
        foreach (em_GetSitemapNavByParentByLangResult sm in childs)
        {

            smapUrl = string.Empty;
            roleList = new string[] { "admin" }; 

            // URL
            smapUrl = sm.CMS_URL.Replace("{}", sm.SMAP_ID.ToString());
            
            // ROLES
            roleList = (sm.CMS_ROLES + ",admin").Split(new char[] { ',', ';' }, 512); // allways allow admin


            smapKey = sm.SMAP_ID.ToString();
            SiteMapNode smapNode = FindSiteMapNodeFromKey(smapKey);

            // add node
            if (smapNode == null)
            {
                try {

                    smapNode = new SiteMapNode(this, smapKey, smapUrl, sm.SML_TITLE, sm.SML_DESC );
                    smapNode.Roles = roleList;
                    AddNode(smapNode, parent); 

                } 
                catch (InvalidOperationException)
                {                    
                }            
            }
            // add sitemap subnodes
            AddAllChildNodes(sm.SMAP_ID, smapNode);
            
        }
    }
    public void OnSitemapChanged() {
        // Refresh the site map
        Clear();
        //_nodes.Clear();
        root = null;
    }

   public override SiteMapNode BuildSiteMap() {

        // Use a lock to make this method thread-safe 
        lock (siteMapLock) { 

            // First, see if we already have constructed the 
            // rootNode. If so, return it... 
            if (root != null) return root; 

            // We need to build the site map! 
            // Clear out the current site map structure 
            base.Clear(); 
             
              
            // Datacontext
            //eMenikDataContext db = new eMenikDataContext();
            // Create the root SiteMapNode 
            SITEMAP rootSmap = (SITEMAP) SITEMAP.GetRootByMenu(menuID);
            root = new SiteMapNode( this, "root:"+ menuID , "~/cms/Default.aspx", "Domov"); 
            root.Roles = ("?,*").Split(new char[] { ',', ';' }, 512);
            AddNode(root); 


            // Add all child nodes for ROOT
            AddAllChildNodes(rootSmap.SMAP_ID, root);

            
            // Finally, return the root node 
            return root; 
        } 
    }
 
    protected override SiteMapNode GetRootNodeCore() {
        return BuildSiteMap(); 
    }

    public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config)
    {
        // Verify parameters
        if (config == null) throw new ArgumentNullException("config");
        if (String.IsNullOrEmpty(name)) name = "CMSSiteMapProvider";

        // Add a default "description" attribute to config if the
        // attribute doesn't exist or is empty
        if (string.IsNullOrEmpty(config["description"]))
        {
            config.Remove("description");
            config.Add("description", "CMS SQL site map provider");
        }

        // Call the base class's Initialize method
        base.Initialize(name, config);

        menuID = Int32.Parse( config["menuID"]);

    }
   

    public override bool IsAccessibleToUser(HttpContext context, SiteMapNode node)
    {

        if (!base.SecurityTrimmingEnabled) return true;

        if (node.Roles.Contains("?") && !context.User.Identity.IsAuthenticated) return true;

        if (context.User.Identity.IsAuthenticated && node.Roles.Contains("*")) return true;

        foreach (string role in node.Roles)
        {

            if (context.User.IsInRole(role)) return true;

        }

        return false;
    }

    public override SiteMapNode CurrentNode
    {
        get
        {
            if (HttpContext.Current != null)
            {
                SiteMapNode ret = ResolveSiteMapNode(HttpContext.Current);
                if (ret != null) return ret;

                string key = "";
                if (HttpContext.Current.Items["smp"] != null) key = HttpContext.Current.Items["smp"].ToString();

                if (string.IsNullOrEmpty(key))
                    if (HttpContext.Current.Request.QueryString["smp"] != null)
                        key = HttpContext.Current.Request.QueryString["smp"];

                ret = this.FindSiteMapNodeFromKey(key);

                if (ret == null)
                    ret = base.FindSiteMapNode(HttpContext.Current);

                return ret;
            }
            else
                return null;
        }
    }
   
}
