﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _ctrl_module_article_viewBlogEdit : BaseControlView
{

    public object Data;
    public object[] Parameters;
    public ARTICLE  Article;
    //protected int ArtID = -1;
    public SITEMAP_LANG SitemapLang;
    protected List<SITEMAP_LANG> SmapList;

    //protected List<PRODUCT_GROUP_ATTRIBUTE> ProductGroupAttList;
    //protected List<SITEMAP_LANG > ProductSmapList;


    protected void Page_Load(object sender, EventArgs e)
    {
        LoadData();
    }



    public void LoadData()
    {



        if (Data  == null)
            Article = new ARTICLE { ART_TITLE="", ART_BODY = "", ACTIVE = true  };
        else
            Article = (ARTICLE)Data;



        if (Parameters != null)
        {
            if (Parameters[0] != null)
                SitemapLang = (SITEMAP_LANG)Parameters[0];

            //if (Parameters.Length > 1 && Parameters[1] != null)
            //    LangID = Parameters[1].ToString();
        }

        // force recepti
        int smp = 1483;
        if (Article != null)
        {
            eMenikDataContext db = new eMenikDataContext();
            var smpArt = (from a in db.ARTICLEs 
                            from cwp in db.CMS_WEB_PARTs 
                            from s in db.SITEMAPs 
                            where a.ART_ID == Article.ART_ID &&  a.CWP_ID == cwp.CWP_ID && s.SMAP_GUID == cwp.REF_ID 
                            select s).FirstOrDefault() ;
            if (smpArt != null )
                smp = smpArt.SMAP_ID;

        }

        SitemapLang = SITEMAP.GetSitemapLangByID(smp, LANGUAGE.GetCurrentLang());
        

        fckBody.Config["FloatingPanelsZIndex"] = "200000";
        fckBody.Value = Server.HtmlDecode(Article.ART_BODY);

        cbComments.Checked = Article.COMMENTS_ENABLED;
        cbApproved.Checked = Article.APPROVED;
        cbExposed.Checked = Article.EXPOSED ;
        cbSortWeight.Checked = Article.SortWeight > 0;

        // add image
        objAddNewImage.ForceDummyGallery = true;



        // load additional categories
        ArticleRep arep = new ArticleRep();
        SmapList = arep.GetArticleSitemapLangs(Article.ART_ID, SitemapLang.LANG_ID).ToList();
        lvProductSmap.DataSource = SITEMAP.GetSiteMapChildsByIDByLang(SM.BLL.Custom.Settings.SmpKlepetalnica(), LANGUAGE.GetCurrentLang()).ToList(); ;
        lvProductSmap.DataBind();




        //// load attributes
        //ProductRep prep = new ProductRep();
        //ProductGroupAttList = prep.GetProductGroupAttributesByProduct(Prod.PRODUCT_ID).ToList();
        
        //lvListAttr.DataSource = prep.GetGroupAttributesByPage(Prod.PageId);
        //lvListAttr.DataBind();

        //// load additional categories
        //ProductSmapList = prep.GetProductSitemapLangs(Prod.PRODUCT_ID, SitemapLang.LANG_ID).ToList();
        //lvProductSmap.DataSource = prep.GetSitemapLangsAdditionalByPM(Prod.PageId, PAGE_MODULE.Common.PRODUCT_ADDITIONAL , SitemapLang.LANG_ID);
        //lvProductSmap.DataBind();



    }
    protected string RenderCheckedProdSmap(object o)
    {
        string ret = "";
        if (SmapList == null)
            return ret;

        SITEMAP_LANG ga = (SITEMAP_LANG)o;

        if (SmapList.Exists(w => w.SMAP_ID == ga.SMAP_ID))
            ret = " checked ";

        return ret;
    }

    //protected string RenderChecked( object o) {
    //    string ret = "";

    //    if (ProductGroupAttList == null)
    //        return ret;

    //    GROUP_ATTRIBUTE ga = (GROUP_ATTRIBUTE )o;

    //    if (ProductGroupAttList.Exists(w => w.GroupAttID == ga.GroupAttID))
    //        ret = " checked ";

    //    return ret;    
    
    //}


    //protected string RenderCheckedProdSmap(object o)
    //{
    //    string ret = "";
    //    if (ProductSmapList == null)
    //        return ret;

    //    SITEMAP_LANG ga = (SITEMAP_LANG)o;

    //    if (ProductSmapList.Exists(w => w.SMAP_ID  == ga.SMAP_ID ))
    //        ret = " checked ";

    //    return ret;
    //}


}
