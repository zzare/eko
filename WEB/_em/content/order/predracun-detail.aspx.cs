﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SM.EM.UI;
using System.Text;
using System.IO;
using System.Net.Mail;


public partial class c_user_PredracunDetail : SM.EM.UI.BasePage_EMENIK
{

    protected Guid OrderID
    {
        get
        {

            // check QS
            if (Request.QueryString["o"] != null)
                if (SM.EM.Helpers.IsGUID(Request.QueryString["o"]))
                    return new Guid(Request.QueryString["o"]);

            // check VS
            if (ViewState["OrderID"] != null)
                return new Guid(ViewState["OrderID"].ToString());


            // order doesn't exist
            this.ErrorRedirect(71);
            return Guid.Empty;
        }
        set { ViewState["OrderID"] = value; }
    }

    protected override void OnPreInit(EventArgs e)
    {
        base.OnPreInit(e);
        //CurrentSitemapID = 800;
    }

    protected override void OnInit(EventArgs e)
    {
        // set sitemap id
        CurrentSitemapID = -1;

        base.OnInit(e);

        //orep = new OrderRep();

        // init caching
        this.OutputCacheDuration = -1;

        // init
        objPredracunDetail.UserId = SM.EM.BLL.Membership.GetCurrentUserID();
        objPredracunDetail.OrderID = OrderID;
        //objPredracunDetail.em_user = em_user;
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            LoadData();

    }
    protected void LoadData()
    {
        objPredracunDetail.BindData();
    }


}
