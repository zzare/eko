﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _ctrl_module_product_attribute_viewProductAttribute : BaseControlView
{

    public object Data;
    public object[] Parameters;
    public float AvgRating;
    public int Number = 5;
    public string OnSrc = "_em/templates/_custom/mozestore/_inc/images/design/star_productlist_off.png";
    public string OffSrc = "_em/templates/_custom/mozestore/_inc/images/design/star_productlist_on.png";


    protected void Page_Load(object sender, EventArgs e)
    {
        LoadData();
    }



    public void LoadData()
    {
        this.Visible = true;


        if (Data != null)
            AvgRating = (float)Data;

        if (Data == null && AvgRating == null)
        {
            return;
        }

        if (Parameters != null)
        {
            if (Parameters.Length > 0 && Parameters[0] != null)
                Number = (int)Parameters[0];

        }

        // bind data
        lvList.DataSource = new string[Number];
        lvList.DataBind();


    }

    protected string RenderRatingSrc(int num)
    {
        string ret = "";
        int avg = (int)Math.Round(AvgRating );

        if (avg <= num)
            ret = OnSrc;
        else
            ret = OffSrc ;

        return SM.BLL.Common.ResolveUrl( ret);

    }

    protected string RenderAvg()
    {
        string ret = "";
         ret = SM.EM.Helpers.FormatPriceEdit(AvgRating ) ;

        return ret;

    }    



}
