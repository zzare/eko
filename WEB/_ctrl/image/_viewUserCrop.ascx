﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewUserCrop.ascx.cs" Inherits="SM.UI.Controls.popupUserCrop" %>

<script type="text/javascript">

    function ready_customCrop(){
         if (!$('#<%= imgBig.ClientID%>')[0].complete)
            $('#<%= imgBig.ClientID%>').load(function (){int_jcrop();});
        else
            int_jcrop(); 
    }


    var x,y,x2,y2;
    var cropRes;
    function int_jcrop() {
        var fW = <%= WidthForce  %>;
        var fH = <%= HeightForce  %>;
        var fR = fH/fW;
        var img = $('#<%= imgBig.ClientID%>');
        var iH, iW, iR, h,w;
        
        iH = img[0].height;
        iW = img[0].width;
        iR = iH/iW;    
        
        if (iR >= fR){
            w = iW;
            h= Math.floor(iW * fR);
            y = Math.floor(Math.abs((iH-h)/2));
            x=0;
        }
        else
        {   h = iH;
            w= Math.floor(iH / fR);    
            y = 0;
            x=Math.floor(Math.abs((iW-w)/2));
        }

        x2=x+w;
        y2=y+h;    
        
        img.Jcrop({
            onSelect: onCropEnd,
            setSelect:   [ x, y, x2, y2 ]
        });
    }

    function onCropEnd(c) {
        cropRes = c;
        x=c.x;
        y=c.y;
        x2=c.x2;
        y2 = c.y2;
        
    }
    function CustomCrop(){
        var img =  $('#<%= hfImg.ClientID%>').val();
        var type = $('#<%= hfType.ClientID%>').val();
        var save =  <%= AutoSaveImage.ToString().ToLower() %>;
        // crop image
        openModalLoading('Shranjujem...');
        WebServiceEMENIK.CustomCropImage(img, type, x, y, x2, y2, '', save, OnCompleteImgCrop);
    }        
    function OnCompleteImgCrop(res){
        
        // set image with new src
        if (res && res[0] && res[1]){

        }
        //refersh old images
        var src = '<%= ImgTargetSrc %>';
        closeModalLoading();
        $('img[src$='+ src +']').attr('src',res[1]).sm_animate_change();
        
        // close modal popup
        closeModalLoader();
    }

      

</script>

    <div class="modPopup_header modPopup_headerSettings">
        <h4>UREDI/POPRAVI SLIKO</h4>
        <p>Uredi/popravi sliko. Z miško označi želeno vidnost slike. Če slika ni v okviru dovoljenih dimenzij, bo samodejno popravljena.</p>
    </div>
    <div class="modPopup_main">
    <div style="margin-top:5px; margin-bottom:5px; font-size:14px;">
        Kliknite na sliko in označi želen kvadrat. Ko ste končali, kliknite OK. Če slika ni v okviru dovoljenih dimenzij, bo samodejno popravljena. 
    </div>
    <div class="buttonwrapperModal">
        <a class="lbutton lbuttonConfirm" onclick="CustomCrop(); return false" href="#"><span>Shrani</span></a>
        <a class="lbutton lbuttonDelete" onclick="closeModalLoader(); return false" href="#"><span>Prekliči</span></a>
    </div>
    
    <img id="imgBig" runat="server" />
    
    <input type="hidden" ID="hfImg" runat="server" />
    <input type="hidden" ID="hfType" runat="server" />
    <input type="hidden" ID="hfImageUrlVirt" runat="server" />

    <br />
    
    
    </div>    
    <div class="modPopup_footer">
        <div class="buttonwrapperModal">
            <a class="lbutton lbuttonConfirm" onclick="CustomCrop(); return false" href="#"><span>Shrani</span></a>
            <a class="lbutton lbuttonDelete" onclick="closeModalLoader(); return false" href="#"><span>Prekliči</span></a>
        </div>
     </div>



