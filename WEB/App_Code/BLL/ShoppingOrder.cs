﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

/// <summary>
/// Summary description for SHOPPING_ORDER
/// </summary>
public partial class SHOPPING_ORDER
{

    public decimal GetShippingPrice()
    {
        return CalculateShippingPrice(this.TotalWithTax ?? 0 );
    }
    public decimal CalculateShippingPrice(decimal total) {
        decimal ret = 0;
        if (this.ShippingType == Common.ShippingType.SHIPPING)
        {
            // free
            if (this.ShippingRegion == SHOPPING_ORDER.Common.ShippingRegion.LJUBLJANA)
                return ret;
            if (this.ShippingRegion == SHOPPING_ORDER.Common.ShippingRegion.NOVA_GORICA)
                return ret;

            return GetShippingPriceByTotal(total);

        }
        return 0;
    }
    public DateTime GetDatumZapadlosti() {
        DateTime date = DateTime.Today;

        if (this.ShippedDate != null)
            date = this.ShippedDate.Value;


        return date.AddDays(7);
    }


    public string GetShippingName() {
        if (this.UseDifferentShippingAddress)
            return this.ShippingFirstName;
        string ret = OrderFirstName + " " + OrderLastName;

        if (!string.IsNullOrEmpty(this.OrderCompany))
            ret += " (" + this.OrderCompany  +")";

        return ret;    
    }

    public string GetShippingAddress()
    {
        if (this.UseDifferentShippingAddress)
            return this.ShippingAddress;
        string ret = this.OrderAddress;

        // eko
        ret = this.ShippingAddress;

        return ret;
    }

    public string GetShippingPostalAndCity()
    {
        if (this.UseDifferentShippingAddress)
            return this.ShippingCity ;
        string ret = this.OrderPostal + " " + this.OrderCity  ;

        // eko
        ret = this.ShippingCity;

        return ret;
    }

    public string GetShippingDateEko()
    {
       
        string ret = this.ShippedDate.Value .ToShortDateString ();

        return ret;
    }
    public string GetShippingTimeEko()
    {

        string ret = this.OrderTime ;

        return ret;
    }

    public string GetShippingPhone()
    {
        if (this.UseDifferentShippingAddress)
            return this.ShippingPhone;
        string ret = this.OrderPhone;

        return ret;
    }
    public bool IsValid
    {
        get { return (GetRuleViolations().Count() == 0); }
    }

    public IEnumerable<RuleViolation> GetRuleViolations()
    {
        if (this.OrderStatus  == SHOPPING_ORDER.Common.Status.CONFIRMED  )
        {
            // confirmed
            if (PaymentType == null || PaymentType == SHOPPING_ORDER.Common.PaymentType.NOT_SET)
                yield return new RuleViolation("Način plačila ni določen", "PaymentType");
            if (ShippingType == null || ShippingType == SHOPPING_ORDER.Common.ShippingType.NOT_SET )
                yield return new RuleViolation("Način dostave ni določen", "ShippingType");
            if (Total  <= 0)
                yield return new RuleViolation("Cena ni pravilna", "Total");

        }

        // common
        if (UserId  == Guid.Empty)
            yield return new RuleViolation("Uporabnik ni določen", "UserId");
        if (PageId == Guid.Empty)
            yield return new RuleViolation("Stran ni določena", "PageId");


        yield break;
    }

    partial void OnValidate(System.Data.Linq.ChangeAction action)
    {
        if (!IsValid)
            throw new ApplicationException("Podatki niso pravilno vneseni.");
    }





    /**********************       STATIC FUNCTIONS    *********************/
    public static SHOPPING_ORDER GetSHOPPING_ORDERByID(Guid id)
    {
        eMenikDataContext db = new eMenikDataContext();

        return  db.SHOPPING_ORDERs .Where(w => w.OrderID  == id).SingleOrDefault();
    }
    public static SHOPPING_ORDER GetShoppingOrderByIDByUser(Guid id, Guid user)
    {
        eMenikDataContext db = new eMenikDataContext();

        return db.SHOPPING_ORDERs.Where(w => w.OrderID == id && w.UserId == user ).SingleOrDefault();
    }
    public static SHOPPING_ORDER GetShoppingOrderByIDByPage(Guid id, Guid user)
    {
        eMenikDataContext db = new eMenikDataContext();

        return db.SHOPPING_ORDERs.Where(w => w.OrderID == id && w.PageId == user).SingleOrDefault();
    }


    public static SHOPPING_ORDER GetLastOpenSHOPPING_ORDERByUser( Guid user)
    {
        eMenikDataContext db = new eMenikDataContext();

        return db.SHOPPING_ORDERs.Where(w => w.UserId == user && w.OrderStatus <= Common.Status.STEP_4  ).SingleOrDefault();
    }
    public static IQueryable<SHOPPING_ORDER> GetShoppingOrdersByCustomer(Guid userID)
    {

        eMenikDataContext db = new eMenikDataContext();
        return from o in db.SHOPPING_ORDERs where o.UserId == userID && o.OrderStatus >= SHOPPING_ORDER.Common.Status.CONFIRMED select o;
    }

    //public static IQueryable<SHOPPING_ORDER> GetShoppingOrdersByCustomer(Guid userID)
    //{

    //    eMenikDataContext db = new eMenikDataContext();
    //    return from o in db.SHOPPING_ORDERs where o.UserId == userID select o;
    //}


    public static int GetMaxOrderNum()
    {
        eMenikDataContext db = new eMenikDataContext();

        return db.SHOPPING_ORDERs.Where(w => w.OrderStatus >= SHOPPING_ORDER.Common.Status.CONFIRMED ).Max(m=>m.OrderNumber ) ?? 0;
    }
    public static int GetNexOrderNum()
    {
        return GetMaxOrderNum() + 1;
    }



    public static SHOPPING_ORDER CreateNewSHOPPING_ORDERBlank(Guid user)
    {
        eMenikDataContext db = new eMenikDataContext();
        SHOPPING_ORDER o = new SHOPPING_ORDER();

        o.UserId = user;
        o.DateStart = DateTime.Now;
        o.OrderID = Guid.NewGuid();
        o.DatePayed = null;
        o.Payed = false;
        o.OrderStatus = Common.Status.NEW;
        o.Total = 0;
        o.PaymentType = Common.PaymentType.NOT_SET;
        o.PredracunID = null;

        db.SHOPPING_ORDERs.InsertOnSubmit(o);
        db.SubmitChanges();
        return o;
    }



    public static decimal GetShippingPriceByTotal(decimal total)
    {

        decimal ret = Common.SHIPPING_PRICE ;

        if (total >= Common.SHIPPING_FREE_TRESHOLD)
        {
            ret = 0;
        }
        return ret;
    }

    public static SHOPPING_ORDER CalculateSHOPPING_ORDERTotal(Guid SHOPPING_ORDER, Guid user)
    {
        eMenikDataContext db = new eMenikDataContext();

        SHOPPING_ORDER ord = (from o in db.SHOPPING_ORDERs where o.OrderID == SHOPPING_ORDER && o.UserId == user select o).SingleOrDefault();

                // calculate total price
                decimal tot = (from l in db.CART_ITEMs  where l.OrderID == SHOPPING_ORDER select l.Price).Sum();
                decimal tax = tot *(Common.TAX_FACTOR );
                ord.Total = tot;
                ord.Tax = tax;
                ord.DateStart = DateTime.Now;



        db.SubmitChanges();
        return ord;
    }



    public static void  UpdateSHOPPING_ORDERTicketsActive(Guid SHOPPING_ORDER, bool active)
    {

        using (eMenikDataContext db = new eMenikDataContext())
        {
            db.ExecuteCommand("UPDATE t SET t.Active = {0} FROM TICKET t, SHOPPING_ORDER_ITEM oi WHERE oi.OrderID = {1} AND t.TicketID = oi.TicketID   ", active, SHOPPING_ORDER);

            TICKET tic = (from t in db.TICKETs from o in db.SHOPPING_ORDERs where o.OrderID == SHOPPING_ORDER && t.TicketID == o.TicketID select t).SingleOrDefault();
            if (tic != null)
            {
                tic.Active = active;
                db.SubmitChanges();
                
            }
        }
    }



    public class Common {
        public const decimal TAX_FACTOR = 0.2m;
        public const decimal SHIPPING_FREE_TRESHOLD = 200m;
        public const decimal SHIPPING_PRICE = 15m;

        public class Status
        {


            public const short CANCELED = -1;
            public const short NEW = 0;
            public const short STEP_1 = 1;
            public const short STEP_2 = 2;
            public const short STEP_3 = 3;
            public const short STEP_4 = 4;
            public const short STEP_CONFIRMED = 5;

            public const short ONLINE_PAYMENT_IN_PROGRESS = 7;
            //public const short ONLINE_PAYMENT_FAILED = 8;

            public const short CONFIRMED = 10;

            public const short SENT_DELIVERY = 12;

            public const short ONLINE_PAYMENT_RESERVED = 13;
            
            public const short PAYED = 20;
            public const short FINISHED = 30;
            //public const short CONFIRMED = 5;
            //public const short PAYED = 6;
            //public const short FINISHED = 7;

        }

        public class Tax
        {


            public const decimal  DEFAULT = 0.2m;

        }
        //public class DomainOrderStatus
        //{
        //    public const short NOT_SHOPPING_ORDERED = 0;
        //    public const short SHOPPING_ORDERED = 1;
        //    public const short BOUGHT = 2;
        //    public const short SHOPPING_ORDER_NOT_POSSIBLE = 3;

        //}

        public class PaymentType
        {
            public const short NOT_SET = 0;
            public const short PREDRACUN = 1;
            public const short DELIVERY = 2;
            public const short ONLINE_e24 = 3;

        }

        public class ShippingType
        {
            public const short NOT_SET = 0;
            public const short SHIPPING = 1;
            public const short PERSONAL = 2;

        }

        public class ModuleItems {
            public const int NAJEM = 5;
            public const int VNOS_SIMPLE = 11;
            public const int CUSTOM_CSS = 10;
            public const int CUSTOM_DESIGN = 12;
        
        
        }

        public class ShippingRegion
        {
            public const short LJUBLJANA = 1;
            public const short NOVA_GORICA = 2;
            public const short OSTALO = 3;

        }

        public static string RenderSHOPPING_ORDERRef(object refID){
            int code = int.Parse(refID.ToString());

            return "N" + code.ToString("00000");
    }

        public static string RenderShoppingOrderNumber(SHOPPING_ORDER order)
        {
            // try order number
            if (order.OrderStatus >= SHOPPING_ORDER.Common.Status.CONFIRMED && order.OrderNumber != null) {
//                return "100" + order.OrderNumber.Value.ToString("0000");
                return  order.OrderNumber.Value.ToString("0000");
            }
            
            // return order ref id
            return "N" + order.OrderRefID.ToString("00000");
        }


        public static string RenderOrderStatus(object stat)
        {
            short status = short.Parse(stat.ToString());
            string ret = "";
            if (status  ==  SHOPPING_ORDER.Common.Status.NEW)
                ret  = "novo";
            else if (status  >=   SHOPPING_ORDER.Common.Status.STEP_1 && status <=  SHOPPING_ORDER.Common.Status.STEP_4 )
                ret = "nepotrjeno";
            else if (status == SHOPPING_ORDER.Common.Status.CONFIRMED)
                ret = "oddano";
            else if (status == SHOPPING_ORDER.Common.Status.SENT_DELIVERY)
                ret = "poslano (plač. po povzetju)";
            else if (status == SHOPPING_ORDER.Common.Status.PAYED)
                ret = "plačano";
            else if (status == SHOPPING_ORDER.Common.Status.FINISHED)
                ret = "odpremljeno";
            else if (status == SHOPPING_ORDER.Common.Status.ONLINE_PAYMENT_IN_PROGRESS)
                ret = "online plačilo v poteku";
            else if (status == SHOPPING_ORDER.Common.Status.ONLINE_PAYMENT_RESERVED)
                ret = "online plačilo uspešno";
            else if (status == SHOPPING_ORDER.Common.Status.CANCELED)
                ret = "preklicano";

            return ret;
        }

        public static  string RenderSHOPPING_ORDERPayment(SHOPPING_ORDER Order)
        {
            string ret = "";
            if (Order == null)
                return ret;

            switch (Order.PaymentType )
            {
                case SHOPPING_ORDER.Common.PaymentType.PREDRACUN:
                    ret = "Plačilo po predračunu";

                    if (Order.OrderStatus >= SHOPPING_ORDER.Common.Status.CONFIRMED)
                        ret += "<br/><a href=\"" + SM.BLL.Common.ResolveUrl(SHOPPING_ORDER.Common.Predracun.GetPredracunHref(Order.OrderID, Order.PageId.Value  )) + "\">Ogled predračuna &raquo;</a>";
                    break;
                case SHOPPING_ORDER.Common.PaymentType.ONLINE_e24 :
                    ret = "On-line plačilo";
                    break;
                case SHOPPING_ORDER.Common.PaymentType.DELIVERY :
                    ret = "Plačilo po povzetju";
                    break;

            }
            ret += "<br/><br/>";
            return ret;
        }

        public static string RenderShoppingOrderShipping(short? type)
        {
            string ret = "";

            switch (type)
            {
                case SHOPPING_ORDER.Common.ShippingType.SHIPPING:
                    ret = "Dostava";
                    break;
                case SHOPPING_ORDER.Common.ShippingType.PERSONAL :
                    ret = "Osebni prevzem";
                    break;
            }
            ret += "<br/><br/>";
            return ret;
        }


        public static string RenderShoppingOrderShippingRegion(short? type)
        {
            string ret = "";

            switch (type)
            {
                case SHOPPING_ORDER.Common.ShippingRegion .LJUBLJANA:
                    ret = "Ljubljana in okolica (brezplačna dostava)";
                    break;
                case SHOPPING_ORDER.Common.ShippingRegion.NOVA_GORICA:
                    ret = "Nova Gorica in okolica (brezplačna dostava)";
                    break;
                case SHOPPING_ORDER.Common.ShippingRegion.OSTALO:
                    ret = "* Ostalo (doplačilo za dostavo 15€)";
                    break;
            }
            return ret;
        }

        public static string RenderShoppingOrderShippingRegionEN(short? type)
        {
            string ret = "";

            switch (type)
            {
                case SHOPPING_ORDER.Common.ShippingRegion.LJUBLJANA:
                    ret = "Ljubljana";
                    break;
                case SHOPPING_ORDER.Common.ShippingRegion.NOVA_GORICA:
                    ret = "Nova Gorica";
                    break;
                case SHOPPING_ORDER.Common.ShippingRegion.OSTALO:
                    ret = "* Rest (additional 15€)";
                    break;
            }
            return ret;
        }

        public static string RenderShoppingOrderShippingRegionIT(short? type)
        {
            string ret = "";

            switch (type)
            {
                case SHOPPING_ORDER.Common.ShippingRegion.LJUBLJANA:
                    ret = "Ljubljana";
                    break;
                case SHOPPING_ORDER.Common.ShippingRegion.NOVA_GORICA:
                    ret = "Nova Gorica";
                    break;
                case SHOPPING_ORDER.Common.ShippingRegion.OSTALO:
                    ret = "* resto (addizionale 15€)";
                    break;
            }
            return ret;
        }


        public static string RenderShoppingOrderShippingDetail(SHOPPING_ORDER order)
        {
            string ret = "";
            if (order == null)
                return ret;

            switch (order.ShippingType)
            {
                case SHOPPING_ORDER.Common.ShippingType.SHIPPING:
                    //ret = "<strong>Dostava</strong>";

                        //ret += "<br/>";
                        ret += "Datum: " + order .GetShippingDateEko () + "<br/>";
                        ret += "Regija: " + SHOPPING_ORDER.Common.RenderShoppingOrderShippingRegion((short) (order .ShippingRegion??0))  + "<br/>";
                        ret += "Kraj: " + order .GetShippingPostalAndCity () + "<br/>";
                        ret += "Naslov: " + order .GetShippingAddress() + "<br/>";
                        ret += "Ura: " + order .GetShippingTimeEko() + "<br/>";
                    if (!string.IsNullOrWhiteSpace(order.ShippingPhone  ))
                        ret += "Telefon: " + order.ShippingPhone + "<br/>";
                    if (!string.IsNullOrWhiteSpace(order.OrderNotes ))
                        ret += "Opombe: " + order.OrderNotes + "<br/>";

                        //ret += "Tel: " + order.GetShippingPhone() + "<br/>";
                    //ret += "<br/>";

                    break;
                case SHOPPING_ORDER.Common.ShippingType.PERSONAL:
                    ret = "Osebni prevzem";
                    break;
            }
            //ret += "<br/><br/>";
            return ret;
        }
        //public static string RenderShoppingOrderShippingDetail(SHOPPING_ORDER order)
        //{
        //    string ret = "";

        //    switch (order.ShippingType)
        //    {
        //        case SHOPPING_ORDER.Common.ShippingType.SHIPPING:
        //            ret = "<strong>Dostava na naslov</strong>";

        //            ret += "<br/>";
        //            ret += "Ime in priimek: " + order.GetShippingName() + "<br/>";
        //            ret += "Naslov: " + order.GetShippingAddress() + "<br/>";
        //            ret += "Poštna številka in kraj: " + order.GetShippingPostalAndCity() + "<br/>";
        //            ret += "Tel: " + order.GetShippingPhone() + "<br/>";
        //            //ret += "<br/>";

        //            break;
        //        case SHOPPING_ORDER.Common.ShippingType.PERSONAL:
        //            ret = "Osebni prevzem";
        //            break;
        //    }
        //    ret += "<br/><br/>";
        //    return ret;
        //}
        public static string RenderShoppingOrderCompanyDetail(SHOPPING_ORDER order)
        {
            string ret = "";

            if (order.IsCompany)
            {
                ret += "Naziv: " + order.COMPANY.NAME + "<br/>";
                ret += "Davčna št.: " + order.COMPANY.TAX_NUMBER + "<br/>";
                ret += "Zavezanec za DDV: " + ( order.COMPANY.ZAVEZANEC ? "DA" : "NE")  + "<br/>";
                ret += "Naslov: " + order.COMPANY.ADDRESS.STREET  + "<br/>";
                ret += "Poštna številka in kraj: " + order.COMPANY.ADDRESS.CITY;
                    //ret += "<br/>";

            }
            ret += "<br/>";
            return ret;
        }

        public static string RenderShoppingOrderUserData(SHOPPING_ORDER order)
        {
            string ret = "";

            if (order == null)
                return ret;

            string name = order.OrderFirstName + " " + order.OrderLastName;
            string tax = "";
            string address = order.OrderAddress;
            string city = order.OrderPostal + " " + order.OrderCity;

            // title
            if (!string.IsNullOrEmpty(order.OrderCompany))
                name = order.OrderCompany;

            string forUser = "";
            // for friend - eko
            if (order.UseDifferentShippingAddress)
            {
                forUser = "" + order.ShippingFirstName;
                ret += " <strong>" + forUser + "</strong> (" + name + ")<br/>";

            }
            else
            {
                ret += " <strong>" + name + "</strong><br/>";
            }

            
            // customer email
            var c = CUSTOMER.GetCustomerByID(order.UserId);
            if (c != null) {
                ret += "" + c.EMAIL + "<br/>";            
            }





            if (!string.IsNullOrWhiteSpace(address))
                ret += "Naslov: " + address + "<br/>";
            if (!string.IsNullOrWhiteSpace(city))
                ret += "Poštna številka in kraj: " + city + "<br/>";

            // tax
            if (!string.IsNullOrWhiteSpace(order.OrderTax))
                ret += "ID št. za DDV: " + order.OrderTax + "<br/>";


            if (!string.IsNullOrWhiteSpace(order.OrderPhone))
                ret += "Telefon: " + order.OrderPhone;
            //ret += "<br/>";

            //ret += "<br/>";
            return ret;
        }





        public static string GetSHOPPING_ORDERHref(string items , string  ru){
            string ret = "~/c/user/SHOPPING_ORDER.aspx?i=" + items + "&" + ru;

            return ret;        
        }
        public static string GetSHOPPING_ORDERDetailsHref(Guid o)
        {
            string ret = "~/c/user/SHOPPING_ORDER-detail.aspx?o=" + o.ToString();

            return ret;
        }
        public static string GetSHOPPING_ORDEREditHref(Guid o)
        {
            string ret = "~/c/user/SHOPPING_ORDER.aspx?o=" + o.ToString();

            return ret;
        }

        public static string GetShoppingOrderDetailsCMSHref(Guid o)
        {
            string ret = "~/c/user/shopping-order/orders.aspx?o=" + o.ToString();

            return ret;
        }

        public class Predracun {


            public static string RenderPredracunID(object refID)
            {
                if (refID == null) return "";
                int code = int.Parse(refID.ToString());

                return code.ToString("000000") + "P";
            }

            public static string GetPredracunHref(Guid o, Guid pageid)
            {
                string ret = "~/_em/content/order/predracun-detail.aspx?o=" + o.ToString();

                ret = SM.EM.Rewrite.PredracunDetailUrl(pageid, o);


                return ret;
            }
        
        }

        public class Export {

            public static string GetExportOrderXmlHref(Guid o)
            {
                string ret = "~/c/NarociloXml.ashx?o=" + o.ToString();
                return ret;
            }
        }
    
    }

    //partial void OnValidate(System.Data.Linq.ChangeAction action)
    //{
    //}

}
