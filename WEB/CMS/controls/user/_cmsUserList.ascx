﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_cmsUserList.ascx.cs" Inherits="SM.UI.Controls._cmsUserList" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Namespace="SM.UI.Controls" TagPrefix="SM" %>


<SM:InlineScript runat="server">
    <script type="text/javascript" language="javascript">
    function SetUniqueCheckbox(nameregex, current)
    {
       re = new RegExp(nameregex);
       for(i = 0; i < document.forms[0].elements.length; i++){
          elm = document.forms[0].elements[i];
          
          if (elm.type == 'checkbox')
          {
             if (re.test(elm.name))
             {
                elm.checked = false;
             }
          }
       }
       current.checked = true;
       
    Sys.Application.notifyScriptLoaded();
}
function ToggleAllUsers(all) {
    $("input:checkbox").each(function() {
        this.checked = all.checked;
    });        
}
    </script>
</SM:InlineScript>


<div class="qContList">

        <asp:LinqDataSource ID="ldsList" runat="server" OnSelecting="ldsList_OnSelecting" >
        </asp:LinqDataSource>
        
        <SM:smListView ID="lvList" runat="server" DataSourceID="ldsList" 
            DataKeyNames="UserId" InsertItemPosition="None" 
            onitemcommand="lvList_ItemCommand" onitemdatabound="lvList_ItemDataBound" 
            ondatabound="lvList_DataBound" >
            <LayoutTemplate>
                <table  class="tblData" cellpadding="0" cellspacing="0">
                        <thead>
                            <tr id="headerSort" runat="server">
                                <th width="50px" runat="server" id="headCB" ><input type="checkbox" onclick="ToggleAllUsers(this);" /></th>
                                <th ><asp:LinkButton ID="LinkButton2" CommandName="sort" CommandArgument="LASTNAME" runat="server">Priimek</asp:LinkButton></th>
                                <th ><asp:LinkButton ID="LinkButton1" CommandName="sort" CommandArgument="FIRSTNAME" runat="server">Ime</asp:LinkButton></th>
                                <th ><asp:LinkButton ID="LinkButton3" CommandName="sort" CommandArgument="PAGE_NAME" runat="server">Page name</asp:LinkButton></th>
                                <th ><asp:LinkButton ID="LinkButton4" CommandName="sort" CommandArgument="USERNAME" runat="server">Username</asp:LinkButton></th>
                                <th ><asp:LinkButton ID="LinkButton5" CommandName="sort" CommandArgument="DATE_REG" runat="server">REGISTRACIJA</asp:LinkButton></th>
                                <th ><asp:LinkButton ID="LinkButton6" CommandName="sort" CommandArgument="DATE_VALID" runat="server">VELJAVNO DO</asp:LinkButton></th>
                                <th ><asp:LinkButton ID="LinkButton7" CommandName="sort" CommandArgument="DATE_PAYED" runat="server">Zadnje PLAČANO</asp:LinkButton></th>
                                <th ><asp:LinkButton ID="LinkButton9" CommandName="sort" CommandArgument="PAYED_PRICE" runat="server">Zadnji ZNESEK</asp:LinkButton></th>
                                <th ><asp:LinkButton ID="LinkButton8" CommandName="sort" CommandArgument="DATE_ORDER_CHANGED" runat="server">Zadnja SPREM. pak.</asp:LinkButton></th>
                                <th ><asp:LinkButton ID="LinkButton10" CommandName="sort" CommandArgument="PAYED" runat="server">PLAČ.</asp:LinkButton></th>
                                <th ><asp:LinkButton ID="LinkButton11" CommandName="sort" CommandArgument="ACTIVE_WWW" runat="server">AKT.</asp:LinkButton></th>
                                <th width="100px" colspan="2" runat="server"  id="headManage" >Uredi</th>
                            </tr>
                        </thead>
                        <tbody id="itemPlaceholder" runat="server"></tbody>
                        <tfoot>
                            <tr>
                                <th style="text-align:right" id="footTH" runat="server"  >
                                    <asp:DataPager runat="server" ID="DataPager" >
                                        <Fields>
                                            <asp:NextPreviousPagerField  />
                                            <asp:NumericPagerField ButtonCount="5" />
                                        </Fields>
                                    </asp:DataPager>
                                </th>
                            </tr>
                        </tfoot>
                    </table>
            </LayoutTemplate>
            
            <ItemTemplate>
                <tr  class='<%#  Container.DataItemIndex % 2 == 0 ? "  " : " odd " %>'  onmouseover ="this.className='<%#  Container.DataItemIndex % 2 == 0 ? "hovRow" : "hovRowodd" %>'" onmouseout ="this.className = GetCssClass(<%#  Container.DataItemIndex %>)">
                    <td runat="server"  visible='<%# ShowCheckboxColumn %>'>
                        <asp:CheckBox ID="cbSelectUser"  runat="server" />
                        <asp:HiddenField ID="hfUser" runat="server" Value='<%# Eval("UserId") %>' />
                    </td>
                    <td>
                        <%# Eval("LASTNAME")%>
                    </td>
                    <td>
                        <%# Eval("FIRSTNAME")%>
                    </td>
                    <td>
                        <%# Eval("PAGE_NAME")%>
                    </td>
                    <td>
                        <%# Eval("USERNAME")%>
                    </td>
                    <td>
                        <%# SM.EM.Helpers.FormatDateLong (Eval("DATE_REG"))%>
                    </td>
                    <td>
                        <%# SM.EM.Helpers.FormatDate(Eval("DATE_VALID"))%>
                    </td>
                    <td>
                        <%# SM.EM.Helpers.FormatDate(Eval("DATE_PAYED"))%>
                    </td>
                    <td>
                        <%# SM.EM.Helpers.FormatPrice(Eval("PAYED_PRICE"))%>
                    </td>
                    <td>
                        <%# SM.EM.Helpers.FormatDate(Eval("DATE_ORDER_CHANGED"))%>
                    </td>
                    <td>
                        <asp:CheckBox ID="CheckBox1" Checked='<%# Eval("PAYED")%>' Enabled = "false" runat="server" />
                    </td>
                    <td>
                        <asp:CheckBox ID="CheckBox2" Checked='<%# Eval("ACTIVE_WWW")%>' Enabled = "false" runat="server" />
                    </td>
                    <td width="50px" runat="server"  visible='<%# ShowManageColumn && ShowEditColumn %>'>
                        <a target='<%# OnEditTarget %>' href='<%# Page.ResolveUrl( EM_USER.Common.RenderCMSHref(new Guid(Eval("UserId").ToString()))) %>' title="edit user" >uredi</a>
                    </td>
                    <td width="50px" runat="server"  visible='<%# ShowManageColumn && IsAdmin %>'>
                        <asp:LinkButton ID="lbLogin" CommandArgument='<%# Eval("USERNAME") %>' CommandName="login" runat="server">login</asp:LinkButton>
                    </td>                
                </tr>  
            
            </ItemTemplate>
            
            <EmptyDataTemplate>
                
                <div class="odd">
                    Seznam je prazen.
            
                </div>
                
            </EmptyDataTemplate>
            
        
        </SM:smListView>

</div>
