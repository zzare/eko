﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_ctrlLanguageDdl.ascx.cs" Inherits="_ctrl_language_ctrlLanguageDdl" %>

<script type="text/javascript">

    $(document).ready(function () {

        $('#cLang div.lang_droppanel').hide();

        $('#cLang ').hover(function () {
            $('#cLang div.lang_droppanel').stop(true, true).slideDown('fast');
        }, function () {
            $('#cLang div.lang_droppanel').stop(true, true).slideUp('slow');
        });
    
    });


</script>


<div id="cLang" class="cLang">

    <a class="lang_sel" ><span><%=  RenderCurrentLanguage() %></span></a>
<asp:PlaceHolder ID="phVisible" runat="server">
    <div class="lang_droppanel">

        <asp:ListView ID="lvList" runat="server" >
            <LayoutTemplate>
                 <div id="itemPlaceholder" runat="server"></div>
            </LayoutTemplate>
            
            <ItemTemplate>
                <%# RenderDllLang(Container.DataItem ) %>
            </ItemTemplate>

        </asp:ListView>




    <%--    <a href="#"><span>ENGLISH</span></a>
        <a href="#"><span>DEUTSCH</span></a>
        <a href="#"><span>ENGLISH</span></a>
        <a href="#"><span>DEUTSCH</span></a>
        <a href="#"><span>ENGLISH</span></a>
        <a href="#"><span>DEUTSCH</span></a>--%>
    </div>


 </asp:PlaceHolder>

<asp:PlaceHolder ID="phHidden" Visible="false" runat="server">
&nbsp;    
</asp:PlaceHolder>


<%--    <asp:PlaceHolder ID="phVisible" runat="server">

        <%= ParentPage.RenderEditLinkStep(2) %>
        <asp:ListView ID="lvList" runat="server" onitemcommand="lvList_ItemCommand">
            <LayoutTemplate>
                
                
                <div id="itemPlaceholder" runat="server"></div>
            </LayoutTemplate>
            
            <ItemTemplate>
                <a  class="lang-<%# Eval("UI_CULTURE").ToString().ToLower() +  " " +  RenderSelClass(Eval("UI_CULTURE").ToString()) %>"  href='<%# Page.ResolveUrl( SM.EM.Rewrite.EmenikUserUrl(ParentPage.PageName, Eval("UI_CULTURE").ToString() ))  %>' title="<%#Eval("DESC") %>"><%# Eval("DESC").ToString().ToLower() %></a>
            </ItemTemplate>
            
            <ItemSeparatorTemplate>
            <%= Separator%> 
            </ItemSeparatorTemplate>

        </asp:ListView>

    </asp:PlaceHolder>
    

    <asp:PlaceHolder ID="phHidden" Visible="false" runat="server">
    &nbsp;    
    </asp:PlaceHolder>
--%>    
</div>