﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_ctrlOrderSummaryFormatted.ascx.cs" Inherits="_em_content_order__ctrl__ctrlOrderSummaryFormatted" %>
<%@ Register Namespace="SM.EM.UI.Controls" TagPrefix="EM"  %>
<%@ Register TagPrefix="SM" TagName="OrderDetail" Src="~/_em/content/order/_ctrl/_ctrlOrderDetail.ascx"    %>

<%--<asp:PlaceHolder ID="phStatus" runat="server">
    <h3>Status naročila</h3>            

    Naročilo je <%= SHOPPING_ORDER.Common.RenderOrderStatus(Order.OrderStatus) %>
    <br />
</asp:PlaceHolder>--%>


<style>
    
    
			/* Client-specific Styles */
			#outlook a{padding:0;} /* Force Outlook to provide a "view in browser" button. */
			body{width:100% !important;} .ReadMsgBody{width:100%;} .ExternalClass{width:100%;} /* Force Hotmail to display emails at full width */
			body{-webkit-text-size-adjust:none;} /* Prevent Webkit platforms from changing default text sizes. */
			
			/* Reset Styles */
			body{margin:0; padding:0;}
			img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}
			table td{border-collapse:collapse;}
			#backgroundTable{height:100% !important; margin:0; padding:0; width:100% !important;}

    
    
/* 
     table.cPostavke, .artDetailW .artText table {line-height:130%; border-width: 0px; border-style: solid; border-color: #666; border-collapse: collapse; width:600px; color:#000; margin-top:20px; font-size:11px; font-family:Arial;}
     table.cPostavke th, .artDetailW .artText table th {padding:4px 5px 4px 5px; border-width: 1px; border-style: solid; border-color: #000; border-left-style:none; border-right-style:none;}
     table.cPostavke td, .artDetailW .artText table td {padding:4px 5px 4px 5px; border-width: 1px; border-style: solid; border-color: #000; border-collapse: collapse; border-left-style:none; border-right-style:none; vertical-align:top;}
*/

</style>
<center>
<table border="0" cellpadding="0" cellspacing="0" width="650" style=" line-height:100%; font-family:Arial; text-align:left;">
<tbody>
    <tr>
        <td style="font-size:0px; line-height:0px; vertical-align:top;">
            <img height="44" width="294" alt="Terra-gourmet" src='http://terra-gourmet.com/_em/templates/_custom/eko/_inc/images/design/ekorepublika.png' />
        </td>
        <td style="font-family:Arial; font-size:10px; color:#666; line-height:120%; padding-top:10px; width:220px;">
            <%=  SM.BLL.Common.Emenik.Data.SMARDT_TITLE  %><br />
            <%=  SM.BLL.Common.Emenik.Data.SMARDT_ADDRESS  %>, <%=  SM.BLL.Common.Emenik.Data.SMARDT_CITY  %><br />
            tel.:  <%=  SM.BLL.Common.Emenik.Data.SMARDT_PHONE  %><br />
            TRR: <%= SM.BLL.Common.Emenik.Data.TRR() %><br />
            E-naslov: info@terra-gourmet.com<br />
            www naslov: <%=  SM.BLL.Common.Emenik.Data.SMARDT_URL  %><br />
            Ekološke proizvode kontrolira: SI-EKO-001 

        </td>
    </tr>
    <tr>
        <td style="font-family:Arial; font-size:10px; color:#000; line-height:120%; padding-top:40px; vertical-align:top;">
            <%= SHOPPING_ORDER.Common.RenderShoppingOrderUserData(Order)  %>
            <%--<br />--%>
            <%= SHOPPING_ORDER.Common.RenderShoppingOrderShippingDetail (Order)  %>
            <% if (!IsPredracun){%>
            <%= SHOPPING_ORDER.Common.RenderSHOPPING_ORDERPayment(Order)%>
            <%} %>
            <%--<strong>Naziv podjetja</strong><br />
            Naslov<br />
            P.št.	Kraj<br />

            <br /><br /><br /><br />

            ID št. za DDV: --%>
        </td>
        <td style="font-family:Arial; font-size:10px; color:#666; line-height:120%; padding-top:40px; vertical-align:top;">

            <table border="0" cellpadding="0" cellspacing="0" style=" line-height:100%;">
                <tbody>
                    <tr>
                        <td style="font-size:10px; line-height:120%; vertical-align:top; text-align:right;">
                            ID št. za DDV:
                        </td>
                        <td style="font-size:10px; line-height:120%; vertical-align:top; padding-left:6px;">
                            <%= SM.BLL.Common.Emenik.Data.TaxNo() %>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size:10px; line-height:120%; vertical-align:top; text-align:right;">
                            Referenca:
                        </td>
                        <td style="font-size:10px; line-height:120%; vertical-align:top; padding-left:6px;">
                            <%= SHOPPING_ORDER.Common.RenderShoppingOrderNumber(Order)%>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size:10px; line-height:120%; vertical-align:top; text-align:right;">
                            Datum storitve:
                        </td>
                        <td style="font-size:10px; line-height:120%; vertical-align:top; padding-left:6px;">
                            <%= SM.EM.Helpers.FormatDate(Order.GetShippingDateEko())%>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size:10px; line-height:120%; vertical-align:top; text-align:right;">
                            Datum računa:
                        </td>
                        <td style="font-size:10px; line-height:120%; vertical-align:top; padding-left:6px;">
                            <%= SM.EM.Helpers.FormatDate(Order.DateStart)%>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size:10px; line-height:120%; vertical-align:top; text-align:right;">
                            Rok plačila:
                        </td>
                        <td style="font-size:10px; line-height:120%; vertical-align:top; padding-left:6px;">
                            <%--<%= SM.EM.Helpers.FormatDate(Order.GetShippingDateEko())%>--%>
                            <%= SM.EM.Helpers.FormatDate( Order.GetDatumZapadlosti() ) %>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size:10px; line-height:120%; vertical-align:top; text-align:right;">
                            Kraj: 
                        </td>
                        <td style="font-size:10px; line-height:120%; vertical-align:top; padding-left:6px;">
                            Ljubljana 
                        </td>
                    </tr>

                </tbody>
            </table>
        </td>
    </tr>
</tbody>
</table>

 	
            <table border="0" cellpadding="0" cellspacing="0"  width="650" style=" line-height:100%; background-color:#DDD9C3; color:#000; margin-top:20px; font-family:Arial; text-align:left;">
                <tbody>
                    <% if (!IsPredracun) {%>
                    <tr>
                        <td style="font-size:10px; line-height:130%; vertical-align:top; padding-right:0px; padding-top:4px; padding-bottom:4px; padding-left:5px; width:100px;">
<%--                            <strong>RAČUN ŠT.: </strong>--%>
                            <strong>PREDRAČUN ŠT.: </strong>
                        </td>
                        <td style="font-size:10px; line-height:130%; padding-right:0px; padding-top:4px; padding-bottom:4px; padding-left:0px;">
                            <%--<strong><%= SHOPPING_ORDER.Common.Predracun.RenderPredracunID(Order.OrderRefID)  %> </strong>--%>
                            <strong><%= SHOPPING_ORDER.Common.RenderShoppingOrderNumber(Order)%> </strong>
                        </td>
                    </tr>
                    <%} else {%>
                    <tr>
                        <td style="font-size:10px; line-height:130%; vertical-align:top; padding-right:0px; padding-top:4px; padding-bottom:4px; padding-left:5px; width:100px;">
                            <strong>PREDRAČUN ŠT.: </strong>
                        </td>
                        <td style="font-size:10px; line-height:130%; padding-right:0px; padding-top:4px; padding-bottom:4px; padding-left:0px;">
<%--                            <strong><%= SHOPPING_ORDER.Common.Predracun.RenderPredracunID(Order.OrderRefID)  %> </strong>--%>                                                    
                            <strong><%= SHOPPING_ORDER.Common.RenderShoppingOrderNumber(Order)%> </strong>
                        </td>
                    </tr>

                    <%} %>

                </tbody>
            </table>




            <table border="0" cellpadding="0" cellspacing="0" style="line-height:130%; border-width: 0px; border-style: solid; border-color: #808080; border-collapse: collapse; width:650px; color:#000; margin-top:20px; font-size:10px; font-family:Arial; border-left-style:none; border-right-style:none; text-align:left;">
                <thead>
                    <tr>
                        <th style="padding:4px 5px 4px 5px; border-width: 1px; border-style: solid; border-color: #808080; border-left-style:none; border-right-style:none; text-align:left;">
                            Opis
                        </th>
                        <th style="padding:4px 5px 4px 5px; border-width: 1px; border-style: solid; border-color: #808080; border-left-style:none; border-right-style:none; text-align:right;">
                            Znesek
                        </th>
                    </tr>
                </thead>
                <tbody>

                    <asp:ListView ID="lvList" runat="server" >
                        <LayoutTemplate>
                                <asp:PlaceHolder ID="itemPlaceholder" runat="server"/>
                        </LayoutTemplate>
    
                        <ItemTemplate>


                            <tr>
                                <td style="padding:4px 5px 4px 5px; border-width: 1px; border-style: solid; border-color: #808080; border-collapse: collapse; border-left-style:none; border-right-style:none; vertical-align:top;">
                                   <%--<a target="_blank" href='<%# SM.BLL.Common.ResolveUrl(SM.EM.Rewrite.EmenikUserProductDetailsUrl(   new Guid(Eval("PageId").ToString()), new Guid(Eval("ProductID").ToString()), Eval("ProductTitle").ToString() ))  %>'  title='<%# Eval("ProductTitle") %>' >--%>
                                    
                                   <%# SM.EM.Helpers.StripHTML(  Eval("ProductTitle").ToString() )  %>
                                   <%# RenderProductAttributesPrice(Container.DataItem) %> 
                                   <%# RenderProductAttributes(Container.DataItem) %>
                                   , <%# Eval("Quantity") %>x
                                    <%--</a> --%>
                                </td>
                                <td style="padding:4px 5px 4px 5px; border-width: 1px; border-style: solid; border-color: #808080; border-collapse: collapse; border-left-style:none; border-right-style:none; vertical-align:top; text-align:right;">
                                    <%# SM.EM.Helpers.FormatPrice (Eval("PriceTotal"))%>
                                </td>
                            </tr>
    
    
    
    
                        </ItemTemplate>
    
                    </asp:ListView>






                    <%--<tr>
                        <td style="padding:4px 5px 4px 5px; border-width: 1px; border-style: solid; border-color: #000; border-collapse: collapse; border-left-style:none; border-right-style:none; vertical-align:top;">
                            Artikel naziv, kos ipd
                        </td>
                        <td style="padding:4px 5px 4px 5px; border-width: 1px; border-style: solid; border-color: #000; border-collapse: collapse; border-left-style:none; border-right-style:none; vertical-align:top; text-align:right;">
                            0,00 €
                        </td>
                    </tr>
                    <tr>
                        <td style="padding:4px 5px 4px 5px; border-width: 1px; border-style: solid; border-color: #000; border-collapse: collapse; border-left-style:none; border-right-style:none; vertical-align:top;">
                            Artikel naziv, kos ipd
                        </td>
                        <td style="padding:4px 5px 4px 5px; border-width: 1px; border-style: solid; border-color: #000; border-collapse: collapse; border-left-style:none; border-right-style:none; vertical-align:top; text-align:right;">
                            0,00 €
                        </td>
                    </tr>
                    <tr>
                        <td style="padding:4px 5px 4px 5px; border-width: 1px; border-style: solid; border-color: #000; border-collapse: collapse; border-left-style:none; border-right-style:none; vertical-align:top;">
                            Artikel naziv, kos ipd
                        </td>
                        <td style="padding:4px 5px 4px 5px; border-width: 1px; border-style: solid; border-color: #000; border-collapse: collapse; border-left-style:none; border-right-style:none; vertical-align:top; text-align:right;">
                            0,00 €
                        </td>
                    </tr>
                    <tr>
                        <td style="padding:4px 5px 4px 5px; border-width: 1px; border-style: solid; border-color: #000; border-collapse: collapse; border-left-style:none; border-right-style:none; vertical-align:top;">
                            Artikel naziv, kos ipd
                        </td>
                        <td style="padding:4px 5px 4px 5px; border-width: 1px; border-style: solid; border-color: #000; border-collapse: collapse; border-left-style:none; border-right-style:none; vertical-align:top; text-align:right;">
                            0,00 €
                        </td>
                    </tr>
                    <tr>
                        <td style="padding:4px 5px 4px 5px; border-width: 1px; border-style: solid; border-color: #000; border-collapse: collapse; border-left-style:none; border-right-style:none; vertical-align:top;">
                            Artikel naziv, kos ipd
                        </td>
                        <td style="padding:4px 5px 4px 5px; border-width: 1px; border-style: solid; border-color: #000; border-collapse: collapse; border-left-style:none; border-right-style:none; vertical-align:top; text-align:right;">
                            0,00 €
                        </td>
                    </tr>
                    <tr>
                        <td style="padding:4px 5px 4px 5px; border-width: 1px; border-style: solid; border-color: #000; border-collapse: collapse; border-left-style:none; border-right-style:none; vertical-align:top;">
                            Artikel naziv, kos ipd
                        </td>
                        <td style="padding:4px 5px 4px 5px; border-width: 1px; border-style: solid; border-color: #000; border-collapse: collapse; border-left-style:none; border-right-style:none; vertical-align:top; text-align:right;">
                            0,00 €
                        </td>
                    </tr>
                    <tr>
                        <td style="padding:4px 5px 4px 5px; border-width: 1px; border-style: solid; border-color: #000; border-collapse: collapse; border-left-style:none; border-right-style:none; vertical-align:top;">
                            Artikel naziv, kos ipd
                        </td>
                        <td style="padding:4px 5px 4px 5px; border-width: 1px; border-style: solid; border-color: #000; border-collapse: collapse; border-left-style:none; border-right-style:none; vertical-align:top; text-align:right;">
                            0,00 €
                        </td>
                    </tr>
                    <tr>
                        <td style="padding:4px 5px 4px 5px; border-width: 1px; border-style: solid; border-color: #000; border-collapse: collapse; border-left-style:none; border-right-style:none; vertical-align:top;">
                            Artikel naziv, kos ipd
                        </td>
                        <td style="padding:4px 5px 4px 5px; border-width: 1px; border-style: solid; border-color: #000; border-collapse: collapse; border-left-style:none; border-right-style:none; vertical-align:top; text-align:right;">
                            0,00 €
                        </td>
                    </tr>--%>
                </tbody>
            </table>




            <table border="0" cellpadding="0" cellspacing="0"  width="650" style=" text-align:left;">
                <tbody>
                    <tr>
                        <td style="width:50%; padding-right:0px; padding-top:0px; padding-bottom:0px; padding-left:0px;">
                        </td>
                        <td style="padding-right:0px; padding-top:0px; padding-bottom:0px; padding-left:0px;">
                            <table border="0" cellpadding="0" cellspacing="0" style="line-height:130%; border-width: 0px; border-style: solid; border-color: #808080; border-collapse: collapse; width:100%; color:#000; margin-top:20px; font-size:10px; background-color:#DDD9C3; font-family:Arial; border-left-style:none; border-right-style:none;">
                                <tbody>



                                    <tr>
                                        <td style="padding:4px 5px 4px 5px; border-width: 1px; border-style: solid; border-color: #808080; border-collapse: collapse; border-left-style:none; border-right-style:none;">
                                            Dostava
                                        </td>
                                        <td style="padding:4px 5px 4px 5px; border-width: 1px; border-style: solid; border-color: #808080; border-collapse: collapse; border-left-style:none; border-right-style:none; text-align:right;">
                                            <%= SM.EM.Helpers.FormatPrice(GetShipping)%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding:4px 5px 4px 5px; border-width: 1px; border-style: solid; border-color: #808080; border-collapse: collapse; border-left-style:none; border-right-style:none;">
                                            <strong>Skupaj brez DDV</strong>
                                        </td>
                                        <td style="padding:4px 5px 4px 5px; border-width: 1px; border-style: solid; border-color: #808080; border-collapse: collapse; border-left-style:none; border-right-style:none; text-align:right;">
                                            <strong><%= SM.EM.Helpers.FormatPrice(GetTotalSumWithShipping - GetTotalTax)%></strong>
                                        </td>
                                    </tr>
<%--                                    <tr>
                                        <td style="padding:4px 5px 4px 5px; border-width: 1px; border-style: solid; border-color: #808080; border-collapse: collapse; border-left-style:none; border-right-style:none;">
                                            DDV - SKUPAJ
                                        </td>
                                        <td style="padding:4px 5px 4px 5px; border-width: 1px; border-style: solid; border-color: #808080; border-collapse: collapse; border-left-style:none; border-right-style:none; text-align:right;">
                                            <%= SM.EM.Helpers.FormatPrice(GetTotalTax)%>
                                        </td>
                                    </tr>--%>
                                    <tr>
                                        <td style="padding:4px 5px 4px 5px; border-width: 1px; border-style: solid; border-color: #808080; border-collapse: collapse; border-left-style:none; border-right-style:none;">
                                            DDV -  20 %
                                            <%--DDV -  20 %--%>
                                        </td>
                                        <td style="padding:4px 5px 4px 5px; border-width: 1px; border-style: solid; border-color: #808080; border-collapse: collapse; border-left-style:none; border-right-style:none; text-align:right;">
                                            <%= SM.EM.Helpers.FormatPrice(GetTotalTax_20)%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding:4px 5px 4px 5px; border-width: 1px; border-style: solid; border-color: #808080; border-collapse: collapse; border-left-style:none; border-right-style:none;">
                                            DDV -  8 %
                                            <%--DDV -  20 %--%>
                                        </td>
                                        <td style="padding:4px 5px 4px 5px; border-width: 1px; border-style: solid; border-color: #808080; border-collapse: collapse; border-left-style:none; border-right-style:none; text-align:right;">
                                            <%= SM.EM.Helpers.FormatPrice(GetTotalTax_8)%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding:4px 5px 4px 5px; border-width: 1px; border-style: solid; border-color: #808080; border-collapse: collapse; border-left-style:none; border-right-style:none;">
                                            <strong>Skupaj z DDV</strong>
                                        </td>
                                        <td style="padding:4px 5px 4px 5px; border-width: 1px; border-style: solid; border-color: #808080; border-collapse: collapse; border-left-style:none; border-right-style:none; text-align:right;">
                                            <strong><%= SM.EM.Helpers.FormatPrice(GetTotalSumWithShipping)%></strong>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>

                </tbody>
            </table>






            <table border="0" cellpadding="0" cellspacing="0" width="650" style="line-height:130%; font-family:Arial; font-size:10px; color:#000; margin-top:50px; text-align:center;" >
                <tbody>
                    <tr>
                        <td>
                            <strong>Pogoji naročila in plačila: <br /><br />

                            * Minimalen nakup <%= SM.BLL.Custom.MinimumOrderSum.ToString() %> EUR.<br />
                            * Pri naročilih nad 200 EUR je dostava brezplačna kjerkoli v Sloveniji.<br /><br />

                            Dostava v Ljubljani in njeni okolici je brezplačna <br />
                            Dostava v Novi Gorici in njeni okolici je brezplačna <br />
                            V ostalih delih Slovenije je za dostavo potrebno doplačati 15 EUR.</strong> 

                        </td>
                    </tr>
                </tbody>
            </table>


            <table border="0" cellpadding="0" cellspacing="0" width="650" style="line-height:130%; font-family:Arial; font-size:10px; color:#666; margin-top:50px; text-align:center; border-top-width: 1px; border-top-style: solid; border-top-color: #808080;" >
                <tbody>
                    <tr>
                        <td style="padding-top:2px;">
                            Družba je vpisana pri Okrožnem sodišču Nova Gorica, Srg št. 10489800, 10.11.2006, ID št. za DDV:  SI18006825,  m. št.  2239825, <br />
                            TRR:  SI56 1010 0003 9714 802, Banka Koper, osnovni kapital 7.500 EUR
                        </td>
                    </tr>
                </tbody>
            </table>


</center>

<%--<SM:OrderDetail ID="objOrderDetail" runat="server" />--%>

