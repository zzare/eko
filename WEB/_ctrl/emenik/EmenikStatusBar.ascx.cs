using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SM.EM.BLL;
using SM.UI.Controls;

namespace SM.EM.UI.Controls
{
   public partial class EmenikStatusBar : BaseControl_EMENIK
   {


      protected void Page_Load(object sender, EventArgs e)
      {


          if (!Page.User.Identity.IsAuthenticated && !ParentPage.em_user.IS_DEMO )
          {
              this.Visible = false;
              return;
          }
          if (ParentPage.IsEditMode)
          {
              //              if (bp.IsEditMode && bp.IsContentPage)
              this.Visible = true;
              LoadData();
          }
          else
          {
              this.Visible = false;
              return;
          }

      }

      protected void LoadData() {
            //litStatus.Text = EM_USER.GetPageStatus(ParentPage.em_user);
      
      }

      protected void btSettings_Click(object sender, EventArgs e)
      {
          ParentPage.SelectedStatusMenu = SM.BLL.Common.EditModeStatus.Settings;
          Trace.Warn("3 - SelectedStatusMenu = " + SM.BLL.Common.EditModeStatus.Settings.ToString());

          string page = ParentPage.PageName;
          if (page == null) page = "";
          Response.Redirect(Rewrite.EmenikUserSettingsUrl(page));
      }
      protected void btView_Click(object sender, EventArgs e)
      {
          int old = ParentPage.SelectedStatusMenu;

          ParentPage.SelectedStatusMenu = SM.BLL.Common.EditModeStatus.View;
          Trace.Warn("4 - SelectedStatusMenu = " + SM.BLL.Common.EditModeStatus.View.ToString());

          // if before was on settings... redirect to default page
          if ((old != SM.BLL.Common.EditModeStatus.View) && (old != SM.BLL.Common.EditModeStatus.CMS))
              Response.Redirect(Rewrite.EmenikUserUrl(ParentPage.PageName));
          else
          {
              if (ParentPage is BasePage_CmsWebPart )
                  (ParentPage as BasePage_CmsWebPart ).LoadWebParts();
          }
      }

      protected void btEdit_Click(object sender, EventArgs e)
      {
          int old = ParentPage.SelectedStatusMenu;

          ParentPage.SelectedStatusMenu = SM.BLL.Common.EditModeStatus.CMS;
          Trace.Warn("5 - SelectedStatusMenu = " + SM.BLL.Common.EditModeStatus.CMS.ToString());

          // if before was on settings... redirect to defautl page
          if ((old != SM.BLL.Common.EditModeStatus.View) && (old != SM.BLL.Common.EditModeStatus.CMS))
              Response.Redirect(Rewrite.EmenikUserUrl(ParentPage.PageName));
          else
          {
              if (ParentPage is BasePage_CmsWebPart)
                  (ParentPage as BasePage_CmsWebPart).LoadWebParts();
          }

      }
      protected void btHelp_Click(object sender, EventArgs e)
      {
          ParentPage.SelectedStatusMenu = SM.BLL.Common.EditModeStatus.Help;
          Trace.Warn("6 - SelectedStatusMenu = " + SM.BLL.Common.EditModeStatus.Help.ToString());
      }

      protected void btLogout_Logout(object sender, EventArgs e)
      {
          // logout from this domain
          FormsAuthentication.SignOut();
//          SM.EM.Security.Login.LogOut();

          // clear session
          SM.EM.Security.Login.LogOut();

          // if user has custom domain... logout from other domain
          if (ParentPage.em_user.CUSTOM_DOMAIN != null && !String.IsNullOrEmpty(ParentPage.em_user.CUSTOM_DOMAIN) && ParentPage.em_user.CUSTOM_DOMAIN_ACTIVATED)
          {
              // logout from other domain also
              string domain = SM.BLL.Common.Emenik.Data.PortalHostDomain();
              string returnUrl = ParentPage.em_user.CUSTOM_DOMAIN;


//              returnUrl = "http://" + returnUrl;
              // redirect to main domain
              returnUrl = "http://" + SM.BLL.Common.Emenik.Data.PortalHostDomain();

              // logout from other domain
              Response.Redirect("http://" + SM.BLL.Common.Emenik.Data.PortalHostDomain() + "/c/a/Logout.aspx?r=" + Server.UrlEncode( returnUrl));

              //              SM.EM.Rewrite.EmenikUserUrlLogout(domain, returnUrl );

          }
          else if(SM.BLL.Common.Emenik.Data.EnableSubDomainUsers () ){
              // logout from main domain also
              string domain = SM.BLL.Common.Emenik.Data.PortalHostDomain();
              string returnUrl = "";


//              returnUrl = Rewrite.ResolveUserDomain(ParentPage.PageName );
              returnUrl = "http://" + SM.BLL.Common.Emenik.Data.PortalHostDomain();

              // logout from other domain
              Response.Redirect("http://" + SM.BLL.Common.Emenik.Data.PortalHostDomain() + "/c/a/Logout.aspx?r=" + returnUrl);
          
          }
          else  { 
                // redirect to root
              Response.Redirect("~/");

          
          }


           }


      protected override void OnPreRender(EventArgs e)
      {
          base.OnPreRender(e);

          // preselect selected menu

          //btHelp.CssClass = btHelp.CssClass.Replace("SelStat","");
          //btSettings.CssClass = btSettings.CssClass.Replace("SelStat","");
          btView.CssClass = btView.CssClass.Replace("SelStat", "");
          btEdit.CssClass = btEdit.CssClass.Replace("SelStat", "");

          if (ParentPage.SelectedStatusMenu <= 0) return;


          phTooltip.Visible = false;
          phCMS.Visible = false;
          phView.Visible = false;

          switch (ParentPage.SelectedStatusMenu)
          {
              //case SM.BLL.Common.EditModeStatus.Settings:
              //    btSettings.CssClass += " SelStat";
              //    break;
              //case SM.BLL.Common.EditModeStatus.Help:
              //    btHelp.CssClass += " SelStat";
              //    break;
              case SM.BLL.Common.EditModeStatus.View:
                  btView.CssClass += " SelStat";
                  phTooltip.Visible = true;
                  phView.Visible = true;


                  break;
              case SM.BLL.Common.EditModeStatus.CMS:
                  btEdit.CssClass += " SelStat";
                  phTooltip.Visible = true;
                  phCMS.Visible = true;
                  break;
          }
      }

      protected string RenderSelClassEdit()
      {
          string ret = "";
          if (ParentPage.SelectedStatusMenu <= 0)
              return ret;

          if (ParentPage.SelectedStatusMenu == SM.BLL.Common.EditModeStatus.Settings)
              ret = " SelStat";

          return ret;
      }



  }
}