﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="step_header.ascx.cs" Inherits="_em_settings__ctrl_step_header" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="SM" TagName="PopupUserCrop" Src="~/_ctrl/image/popupUserCrop.ascx"  %>
<%@ Register Namespace="SM.UI.Controls" TagPrefix="SM"  %>


<SM:InlineScript ID="InlineScript1"  runat="server">
<script type="text/javascript">

    function SetMaxImageWidth(img, w){
        if(img.width > w)
            img.width = w;
    }
    
    function simulateCB() {
        var n = $("#<%= cbNothing.ClientID %>")[0];

        n.checked = false;
        if ($("#cCB :checkbox:checked").size() == 0)
            n.checked = true;          
    }

    function simulateRB(cb) {
        var s = cb.checked;
        $("#cCB :checkbox").attr('checked', false);
        //$("#cCB :checkbox").each(function() { $get(this.id).set_checked(true); });
        cb.checked = true;
        
        $('#cEditLogo').hide();
    }
    function toggleLogo(cb) {
        if (cb.checked)
            $('#cEditLogo').show();
        else
            $('#cEditLogo').hide();

    }


    function setLoading(valG) {
        // only if is page valid
        if (typeof (Page_ClientValidate) == 'function') {
            Page_ClientValidate(valG);
        }

        if (!Page_IsValid)
            return;

        $("#divLoading").dialog('open').show();
    }

    $(document).ready(function() {
        if ($('#<%= cbShowLogo.ClientID %>')[0].checked) $('#cEditLogo').show(); else $('#cEditLogo').hide();
        $("#divLoading").dialog({
            autoOpen: false,
            bgiframe: true,
            height: 140,
            zIndex: 200001,
            closeOnEscape: false,
            draggable: true,
            resizable: false,
            modal: true
        }).parent().find("a.ui-dialog-titlebar-close").hide();
    });
   
    
</script>     
</SM:InlineScript>


<div id="divLoading" style="display:none;vertical-align: middle;" title="Nalagam...">
	<img style="position:relative;width:66px;height:66px;left:50%;margin-left:-33px; margin-top:20px" title="nalagam..." src='<%= Page.ResolveUrl("~/cms/_res/images/icon/ajax-loader.gif") %>' />
</div>




<h1 style="padding-bottom:10px; ">
<asp:Literal ID="litheadh1" runat="server" Text = "Kaj želite imeti v glavi svoje strani?"></asp:Literal>
    <a href="javascript:void(0)" class="btsettStatusHelp"><img src='<%=Page.ResolveUrl("~/_inc/images/icon/help.png") %>' /></a>
    <div class="settStatusWrapp" >
    <div class="settStatus">
        Določi kaj se bo pojavilo v glavi spletne strani. Na izbiro imaš, da ta prostor pustiš prazen, da napišeš tekst ali pa da naložiš logotip podjetja. V primeru da logotipa nimaš, se lahko obrneš na nas in poslali ti bomo ponudbo za izdelavo logotipa.
        <br />
        Če trenutna grafična predloga to omogoča, lahko spremeniš motivno sliko.
<%--        <ul><li>
        Če se odločite, da ne boste imeli ničesar enostavno obkljukajte "Ne želim ne loga ne teksta"
        </li><li>
        Če želite imeti samo tekst potem odkljukajte "V glavi strani želim imeti tekst - moj naziv", le tega boste uredili v sedmem koraku tega čarovnika. Za predogled pa bo uporabljen privzeti naslov Moja spletna stran.";
        </li><li>
        Če želite na strani imeti objavljen svoj logotip ali logotip vašega podjetja odkljukajte "V glavi strani želim imeti prikazan logo". To vam bo odprlo možnost nalaganja in obdelave vašega logotipa.
        Za potrditev izbire morate pritisniti še na gumb "Shrani spremembe".
        </li></ul>
        <br />
--%>        Za potrditev izbire moraš pritisniti še na gumb "Shrani spremembe".
        </div></div>

</h1>
<asp:Literal ID="litNote" runat="server"></asp:Literal>

<div id="cCB">

    <asp:CheckBox ID="cbNothing" onclick="simulateRB(this)"  runat="server" Text="&nbsp;&nbsp;&nbsp;<strong>Ne želim ne logotipa ne teksta</strong>" Checked="false" />
    <%--<cc1:ToggleButtonExtender ID="ToggleButtonExtender2" runat="server"
        TargetControlID="cbNothing" 
        ImageWidth="19" 
        ImageHeight="19"
        CheckedImageAlternateText="Odkljukaj"
        UncheckedImageAlternateText="Obkljukaj"
        UncheckedImageUrl="~/_inc/images/design/checkbox_off.gif" 
        CheckedImageUrl="~/_inc/images/design/checkbox_on.gif"
        DisabledCheckedImageUrl="~/_inc/images/design/checkbox_disabled.gif"
        DisabledUncheckedImageUrl="~/_inc/images/design/checkbox_disabled.gif" />  --%>  
        <a href="javascript:void(0)" class="btsettStatusHelp"><img src='<%=Page.ResolveUrl("~/_inc/images/icon/help.png") %>' /></a>
        <div class="settStatusWrapp" >
        <div class="settStatus">
            V glavi spletne strani ne bo prikazan ne logotip ne tekst.
        </div></div>

        <br />

<asp:CheckBox ID="cbShowText"   runat="server" Text="&nbsp;&nbsp;&nbsp;<strong>V glavi strani želim imeti tekst - moj naziv</strong>" Enabled="true" Checked="false"/>
    <%--<cc1:ToggleButtonExtender ID="ToggleButtonExtender1" runat="server"
        TargetControlID="cbShowText" 
        ImageWidth="19" 
        ImageHeight="19"
        CheckedImageAlternateText="Odkljukaj"
        UncheckedImageAlternateText="Obkljukaj"
        UncheckedImageUrl="~/_inc/images/design/checkbox_off.gif" 
        CheckedImageUrl="~/_inc/images/design/checkbox_on.gif"
        DisabledCheckedImageUrl="~/_inc/images/design/checkbox_disabled.gif"
        DisabledUncheckedImageUrl="~/_inc/images/design/checkbox_disabled.gif" />  --%>  
        <a href="javascript:void(0)" class="btsettStatusHelp"><img src='<%=Page.ResolveUrl("~/_inc/images/icon/help.png") %>' /></a>
        <div class="settStatusWrapp" >
        <div class="settStatus">
            V glavi spletne bo prikazan naziv spletne strani. Le-tega se uredi v sedmem koraku (dodatno) tega čarovnika.
        </div></div>

        <br />
        

    <asp:CheckBox ID="cbShowLogo"   runat="server" Text="&nbsp;&nbsp;&nbsp;<strong>V glavi strani želim imeti logotip</strong>" Enabled="true" Checked="true" />
<%--    <cc1:ToggleButtonExtender ID="ToggleLogo" runat="server"
        TargetControlID="cbShowLogo" 
        ImageWidth="19" 
        ImageHeight="19"
        CheckedImageAlternateText="Odkljukaj"
        UncheckedImageAlternateText="Obkljukaj"
        UncheckedImageUrl="~/_inc/images/design/checkbox_off.gif" 
        CheckedImageUrl="~/_inc/images/design/checkbox_on.gif"
        DisabledCheckedImageUrl="~/_inc/images/design/checkbox_disabled.gif"
        DisabledUncheckedImageUrl="~/_inc/images/design/checkbox_disabled.gif" />--%>
        <a href="javascript:void(0)" class="btsettStatusHelp"><img src='<%=Page.ResolveUrl("~/_inc/images/icon/help.png") %>' /></a>
        <div class="settStatusWrapp" >
        <div class="settStatus">
            V glavi spletne strani bo prikazan logotip.
            <br />
            Odprla se bo možnost nalaganja in obdelave logotipa.
        </div></div>



    <div id="cEditLogo">
        <h2>Določi sliko LOGOTIPA:
            <a href="javascript:void(0)" class="btsettStatusHelp"><img src='<%=Page.ResolveUrl("~/_inc/images/icon/help.png") %>' /></a>
            <div class="settStatusWrapp" >
            <div class="settStatus">
            Klikni gumb "Browse/Prebrskaj", poišči logotip (format logotipa mora biti .jpg, .png ali .gif) ki ga želiš prenesti iz svojega računalnika in klikni "ok/vredu". Za prenos klikni gumb "Prenesi" in sistem bo samodejno prenesel sliko iz tvojega računalnika in jo po potrebi pomanjšal.          
            </div></div>
        </h2>
        <p>
            V primeru da slika ni v dovoljenih dimenzijah, bo samodejno obdelana.<br />
            <strong>Dovoljene dimenzije:</strong> &nbsp;<strong><asp:Literal ID="litWidth" runat="server" Text = ""></asp:Literal></strong> &nbsp;| &nbsp;
            <strong><asp:Literal ID="litHight" runat="server" Text = ""></asp:Literal></strong>
        </p>
        <br />
        <asp:RequiredFieldValidator ID="rfvFile" Display="Dynamic" ValidationGroup="header" runat="server" ControlToValidate="fuImage" ErrorMessage="* Izberite sliko"></asp:RequiredFieldValidator>
        <%--<asp:RegularExpressionValidator ValidationGroup="header"  id="RegularExpressionValidator1" Display="Dynamic" runat="server"  ErrorMessage="* Slika mora biti tipa .jpg, .png" 
            ValidationExpression="^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))(.jpg|.JPG|.gif|.GIF|.jpeg|.JPEG|.bmp|.BMP|.png|.PNG)$"  ControlToValidate="fuImage"></asp:RegularExpressionValidator>--%>
        <asp:CustomValidator ValidationGroup="headerC" Display="Dynamic" ID="cvFileType" runat="server" ErrorMessage="* Slika mora biti tipa .jpg, .png, .gif" OnServerValidate="ValidateFileType"></asp:CustomValidator>
        <asp:CustomValidator ValidationGroup="headerC" Display="Dynamic" ID="cvFileSize" runat="server" ErrorMessage="" OnServerValidate="ValidateFileSize"></asp:CustomValidator>

        <p><strong>1.</strong> <strong>Izberi</strong> sliko iz svojega računalnika.</p>

        <asp:FileUpload ToolTip="Izberi sliko" ID="fuImage" runat="server"  />
        
        <p><strong>2.</strong> <strong>Prenesi</strong> sliko na svojo spletno stran. </p>
        <div class="buttonwrappersim">
            <asp:LinkButton ID="btUpload" runat="server" OnClientClick="setLoading('header')" CssClass="lbutton lbuttonUpload" onclick="btUpload_OnClick" ValidationGroup="header" ><span>Prenesi</span></asp:LinkButton>
        </div>
        <div class="clearing"></div>

        <asp:Panel ID="panImageLogo" runat="server">

            <img id="imgMain" runat="server" />

            <p><strong>3.</strong> Če ti samodejno preoblikovanje ni všeč, klikni <strong>Popravi sliko</strong>.</p>

            <div class="buttonwrappersim">
                <asp:LinkButton ID="btCrop" runat="server" CssClass="lbutton lbuttonEdit" onclick="btUserCrop_OnClick"><span>Popravi sliko</span></asp:LinkButton>
                <a href="javascript:void(0)" class="btsettStatusHelp"><img src='<%=Page.ResolveUrl("~/_inc/images/icon/help.png") %>' /></a>
                <div class="settStatusWrapp" >
                <div class="settStatus">
                Sliko lahko urediš tudi sam s klikom na "Popravi sliko". Sistem bo samodejno ponudil višino in širino, ki ustreza obliki tvoje spletne strani, polje le premakneš tako, da zajameš želeni del slike. Potem klikni gumb "Shrani".
                </div></div>
            </div>
            <br />

            <p><strong>4.</strong> Če si zadovoljen s sliko, <strong>SHRANI SPREMEMBE</strong>.</p>
        </asp:Panel>

        <asp:HiddenField ID="hfImg" runat="server" />
        <asp:HiddenField ID="hfType" runat="server" />
        <asp:HiddenField ID="hfLogoVirtualUrl" runat="server" />

            
        <div class="clearing"></div>
        
        
        
    </div>

        
    

</div>          





<div class="wizzNav_Separator"></div>
<asp:Panel ID="panHeader" runat="server">
    <h2>Določi sliko MOTIVA
        <a href="javascript:void(0)" class="btsettStatusHelp"><img src='<%=Page.ResolveUrl("~/_inc/images/icon/help.png") %>' /></a>
        <div class="settStatusWrapp" >
        <div class="settStatus">
            Klikni gumb "Browse/Prebrskaj", poišči logotip (format logotipa mora biti .jpg, .png ali .gif) ki ga želiš prenesti iz svojega računalnika in klikni "ok/vredu". Za prenos klikni gumb "Prenesi" in sistem bo samodejno prenesel sliko iz tvojega računalnika in jo po potrebi pomanjšal.          
        </div></div>
    </h2>
    <p>
        V primeru da slika ni v dovoljenih dimenzijah, bo samodejno obdelana.
        <br />
        <strong>Dovoljene dimenzije:</strong> &nbsp;<strong><asp:Literal ID="litWidthH" runat="server" Text = ""></asp:Literal></strong> &nbsp;| &nbsp;
        <strong><asp:Literal ID="litHeightH" runat="server" Text = ""></asp:Literal></strong>        
    </p>
    <br />
    <asp:RequiredFieldValidator ID="rfvFileH" Display="Dynamic" ValidationGroup="headerH" runat="server" ControlToValidate="fuImageH" ErrorMessage="* Izberite sliko"></asp:RequiredFieldValidator>
    <asp:CustomValidator ValidationGroup="headerC" Display="Dynamic" ID="cvFileTypeH" runat="server" ErrorMessage="* Slika mora biti tipa .jpg, .png, .gif" OnServerValidate="ValidateFileTypeH"></asp:CustomValidator>
    <asp:CustomValidator ValidationGroup="headerC" Display="Dynamic" ID="cvFileSizeH" runat="server" ErrorMessage="" OnServerValidate="ValidateFileSizeH"></asp:CustomValidator>

    <p><strong>1.</strong> <strong>Izberi</strong> sliko iz svojega računalnika.</p>
    <asp:FileUpload ToolTip="Izberi sliko" ID="fuImageH" runat="server"   />
    
    <p><strong>2.</strong> <strong>Prenesi</strong> sliko na svojo spletno stran. </p>    
    <div class="buttonwrappersim">
        <asp:LinkButton ID="btUploadH" runat="server" OnClientClick="setLoading('headerH')" CssClass="lbutton lbuttonUpload" onclick="btUploadH_OnClick" ValidationGroup="headerH" ><span>Prenesi</span></asp:LinkButton>
    </div>
    <div class="clearing"></div>

    <asp:Panel ID="panImageH" runat="server">

        <img id="imgMainH" runat="server"  />
        
        <p><strong>3.</strong> Če ti samodejno preoblikovanje ni všeč, klikni <strong>Popravi sliko</strong>.</p>
        <div class="buttonwrappersim">
            <asp:LinkButton ID="btUserCropH" runat="server" CssClass="lbutton lbuttonEdit" onclick="btUserCropH_OnClick"><span>Popravi sliko</span></asp:LinkButton>
            <a href="javascript:void(0)" class="btsettStatusHelp"><img src='<%=Page.ResolveUrl("~/_inc/images/icon/help.png") %>' /></a>
            <div class="settStatusWrapp" >
            <div class="settStatus">
                Sliko lahko urediš tudi sam s klikom na "Popravi sliko". Sistem bo samodejno ponudil višino in širino, ki ustreza obliki tvoje spletne strani, polje le premakneš tako, da zajameš želeni del slike. Potem klikni gumb "Shrani".
            </div></div>
        </div>

        <p><strong>4.</strong> Če si zadovoljen s sliko, <strong>SHRANI SPREMEMBE</strong>.</p>
    </asp:Panel>

    <asp:HiddenField ID="hfImgH" runat="server" />
    <asp:HiddenField ID="hfTypeH" runat="server" />  
    <asp:HiddenField ID="hfHeaderVirtualURL" runat="server" />
  



</asp:Panel>
    
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <SM:PopupUserCrop id="popupCrop" runat="server"></SM:PopupUserCrop>        
        </ContentTemplate>
        
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btCrop" EventName="Click"  />
            <asp:AsyncPostBackTrigger ControlID="btUserCropH" EventName="Click"  />
        
        </Triggers>
    
    </asp:UpdatePanel>


