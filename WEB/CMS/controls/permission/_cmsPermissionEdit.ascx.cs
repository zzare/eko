﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

namespace SM.UI.Controls
{
    public partial class permission__cmsPermissionEdit : BaseControl 
    {
        public event EventHandler OnChanged;
        public event EventHandler OnCancel;

        public Guid UID {
            get
            {
                Guid res = Guid.Empty;
                if (ViewState["UID"] == null) return res;
                if (!SM.EM.Helpers.IsGUID(ViewState["UID"].ToString())) return res;

                return new Guid(ViewState["UID"].ToString());
            }
            set{
                ViewState["UID"] = value;
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {



        }

        // default values
        protected void LoadDefaultValues() {
            UID = Guid.Empty;

        }



        // bind data
        public void BindData() {
            this.Visible = true;




            if (UID == Guid.Empty)
            {
                return;
            }
            else {
                LoadData();
            
            }
          

        }

        // LOAD permission
        protected void LoadData() {

            var scopeList = EM_USER.GetUsers().Select(w=>new { w.PAGE_NAME, w.UserId  });


            if (ddlScope.Items.Count == 0)
            {
                ddlScope.Items.Add(new ListItem(" ALL PAGES ", PERMISSION.Common.Scope.ALL.ToString()));

                ddlScope.DataTextField = "PAGE_NAME";
                ddlScope.DataValueField = "UserId";
                ddlScope.DataSource = scopeList;
                ddlScope.DataBind();
            }


            if (ddlObject.Items.Count == 0)
            {
                ddlObject.DataTextField = "Key";
                ddlObject.DataValueField = "Value";
                ddlObject.DataSource = PERMISSION.Common.Obj.Items;
                ddlObject.DataBind();
            }



            Array itemValues = System.Enum.GetValues(typeof(PERMISSION.Common.Action));
            Array itemNames = System.Enum.GetNames(typeof(PERMISSION.Common.Action));

            if (ddlAction.Items.Count == 0)
            {

                foreach (short r in Enum.GetValues(typeof(PERMISSION.Common.Action)))
                {
                    ListItem item = new ListItem(Enum.GetName(typeof(PERMISSION.Common.Action), r), r .ToString());
                    ddlAction.Items.Add(item);
                }
            }


            if (ddlType.Items.Count == 0)
            {

                ddlType.Items.Add(new ListItem("- select type -", ""));
                ddlType.Items.Add(new ListItem(Enum.GetName(typeof(PERMISSION.Common.Type), PERMISSION.Common.Type.Allow), ((short)PERMISSION.Common.Type.Allow).ToString()));
                ddlType.Items.Add(new ListItem(Enum.GetName(typeof(PERMISSION.Common.Type), PERMISSION.Common.Type.Deny), ((short)PERMISSION.Common.Type.Deny).ToString()));
                ddlType.DataBind();
            }






        }



        public void Save() {
            
            // validation
            Page.Validate("permiss");
            if (!Page.IsValid) return;


            PermissionRep perr = new PermissionRep();

            PERMISSION p = new PERMISSION();
            p.UserId = UID;
            p.Scope = new Guid(ddlScope.SelectedValue);
            p.Object = new Guid(ddlObject.SelectedValue);

            p.Action = short.Parse(ddlAction.SelectedValue);
            p.Type  = short.Parse(ddlType.SelectedValue);

            p.ScopeTitle = ddlScope.SelectedItem.Text;
            p.ObjectTitle = ddlObject.SelectedItem.Text;

            perr.Add(p);

            if (!perr.Save())
            {
                ParentPage.mp.ErrorText = "Napaka pri dodeljevanju pravic ";
                return;
            }
            
            
 
            // remove from cache
//            usr.RemoveFromCache();


            
            // raise changed event
            OnChanged(this, new EventArgs());

        }

        
        protected void btAdd_Click(object sender, EventArgs e)
        {
            Save();
        }
       








}
}