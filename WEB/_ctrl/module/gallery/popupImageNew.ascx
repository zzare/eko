﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="popupImageNew.ascx.cs" Inherits="SM.EM.UI.Controls.popupImageNew" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="CMS" TagName="ImageNew" Src="~/_ctrl/module/gallery/_cmsImageNew.ascx"  %>
<%@ Register TagPrefix="CMS" TagName="GalleryList" Src="~/_ctrl/module/gallery/_cmsGalleryList.ascx"  %>


<asp:HiddenField ID="hfEditImageNew" runat="server" />

        
<asp:Panel CssClass="modPopup" ID="panEditImageNew" runat="server" style="display:none;">
    <div class="modPopup_header modPopup_headerAdd">
        <h4>DODAJ SLIKO</h4>
        <p>V galerijo lahko dodaš posamezno sliko, ki jo preneseš iz svojega računalnika ali pa izbereš že obstoječo galerijo</p>
    </div>

    <div class="modPopup_main">

        <cc1:TabContainer ID="tabModule" runat="server" ActiveTabIndex="0"  >
            <cc1:TabPanel ID="tpNew" HeaderText="Dodaj NOVO sliko v galerijo" runat="server"  >            
                <ContentTemplate>
                    
                    <CMS:ImageNew ID="objImageNew" runat="server" />

                    <div class="buttonwrapperModal">
                        <asp:LinkButton ID="btSaveEdit" runat="server" OnClientClick="openModalLoading('Nalagam sliko')" CssClass="lbutton lbuttonConfirm" OnClick="btSaveEdit_Click" ><span>Dodaj sliko</span></asp:LinkButton>

                    </div>
                </ContentTemplate>
            </cc1:TabPanel>
            
            <cc1:TabPanel ID="tpGallery" HeaderText="Izberi obstoječo galerijo" runat="server" >            
                <ContentTemplate>
                    <CMS:GalleryList ID="objGalleryList" runat="server" />
                    <div class="clearing"></div>
                </ContentTemplate>
            </cc1:TabPanel>

        </cc1:TabContainer>
    </div>
    <div class="modPopup_footer"> 
        <div class="buttonwrapperModal">
            <asp:LinkButton ID="btCancelEdit" runat="server" CssClass="lbutton lbuttonDelete"><span>Prekliči</span></asp:LinkButton>
        </div>
    </div>
</asp:Panel>

           
<cc1:ModalPopupExtender RepositionMode="RepositionOnWindowResizeAndScroll" Y ="50"  BehaviorID="mpeEditImageNew"  ID="mpeEditImageNew" runat="server" TargetControlID="hfEditImageNew" PopupControlID="panEditImageNew"
                        BackgroundCssClass="modBcg" CancelControlID="btCancelEdit" >
</cc1:ModalPopupExtender> 

<%-- dummy file upload for having another one in sub user control for first time postback file upload--%>
<asp:FileUpload ID="dummyFU" runat="server" style="display:none;" />             



