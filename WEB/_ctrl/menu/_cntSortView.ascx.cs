﻿using System;
using System.Collections;
using System.Collections.Generic ;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


namespace SM.UI.Controls
{
    public partial class _cntSortView : System.Web.UI.UserControl 
    {
        public object Data;
        public object[] Parameters;

        public int ParentID;



        public string  Language { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {


                BindData();

        }

        public void BindData() 
        {

            if (Parameters != null)
            {
                if (Parameters[0] != null)
                    ParentID = int.Parse(Parameters[0].ToString());
            }



            // data binding
            lvSort.DataSource = Data;
            
            //lvSort.DataSource = SITEMAP.GetSiteMapNavByParentByLangWWWByCulture(ParentID, Culture);
            lvSort.DataBind();

        }

        //protected void lvSort_ItemDataBound(object sender, ListViewItemEventArgs e)
        //{
        //    if (e.Item.ItemType != ListViewItemType.DataItem)
        //        return;

        //    SITEMAP_LANG item = ((ListViewDataItem)e.Item).DataItem as SITEMAP_LANG  ;
            
        //}

        protected string RenderChilds(object dataItem) {

            SITEMAP_LANG item = dataItem as SITEMAP_LANG;
            if (item == null)
                return "";
            string ret = "";

            var data = SITEMAP.GetSiteMapChildsByIDByLang(item.SMAP_ID , item.LANG_ID );
            if (data == null)
                return ret;

            ret =  ViewManager.RenderView("~/_ctrl/menu/_cntSortView.ascx", data, new object[]{ item.SMAP_ID });

            return ret;

        
        }






    }
}
