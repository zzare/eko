﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewProductDetailRating.ascx.cs" Inherits="_ctrl_module_product_viewProductDetailRating" %>

<script type="text/javascript">
    $(document).ready(function() {
        
        $("#prod-rating").stars({
            inputType: "select",
            oneVoteOnly: true,
            //captionEl: $("#stars-cap"),
            callback: function(ui, type, value) {
                WebServiceP.CastVote('<%= Prod.PRODUCT_ID.ToString()  %>', em_g_cult, value, onVoteSuccess);
                var conf = $("#prod-rating").parent().find("span.vote-conf");
                conf.show();
                setTimeout(function(){ conf.fadeOut(1000) }, 2000);
            }
        });


    });


    function onVoteSuccess(ret) {
        if (ret.Code == 1) {
            //$("#prod-rating").stars("select", 2);
            $("#prod-rating").find("#rating-avg").html('(' + ret.Data + ')');
        
        }
    }


</script>


<div id="prod-rating">
    <span id="rating-avg" style="display:none;">(<%= SM.EM.Helpers.FormatPriceEdit(VOTE.GetAverage(Prod.TOTAL_RATING, Prod.VOTES))%>)</span>
    <select name="selrate" <%= RenderDisabled() %> >
        <option value="1" <%= RenderSelectedRating(1) %>>Zelo slabo</option>
        <option value="2" <%= RenderSelectedRating(2) %>>Slabo</option>
        <option value="3" <%= RenderSelectedRating(3) %> >Povprečno</option>
        <option value="4" <%= RenderSelectedRating(4) %>>Dobro</option>
        <option value="5" <%= RenderSelectedRating(5) %>>Odlično</option>
    </select>
</div>
<span style="display:none; color:#666;" class="vote-conf">Hvala za vašo oceno!</span>
