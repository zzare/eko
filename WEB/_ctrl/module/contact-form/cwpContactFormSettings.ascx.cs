﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class cwpContentSettings : SM.EM.UI.Controls.CwpEdit  
{


    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public override void LoadData()
    {
        // load data
        tbTitle.Text = cwp.TITLE;
        //tbPageSize.Text = cwp.PAGE_SIZE.ToString();
        cbShowTitle.Checked = cwp.SHOW_TITLE;

        CWP_CONTACT_FORM cf = CMS_WEB_PART.GetCwpContactFormByID(CwpID);
        if (cf != null)
        {
            tbEmail.Text = cf.MAIL_TO;
            tbSubject.Text = cf.SUBJECT;

        }
        if (String.IsNullOrEmpty(tbEmail.Text))
            tbEmail.Text = ParentPage.em_user.EMAIL;

        if (String.IsNullOrEmpty(tbSubject.Text))
            tbSubject.Text = "Spletno povpraševanje " + SM.BLL.Common.Emenik.Data.PortalName();




    }

    public bool SaveData() {

        if (!ParentPage.CanSave) return false;

        Page.Validate("settCF");
        if (!Page.IsValid) return false;

        eMenikDataContext db = new eMenikDataContext();
        CMS_WEB_PART c = db.CMS_WEB_PARTs.SingleOrDefault(w => w.CWP_ID == CwpID);

//        c.TITLE = Server.HtmlEncode(tbTitle.Text);
        c.TITLE = tbTitle.Text;
        c.SHOW_TITLE = cbShowTitle.Checked;

        CWP_CONTACT_FORM  cf = db.CWP_CONTACT_FORMs.SingleOrDefault(w => w.CWP_ID == CwpID);
        if (cf == null)
        {
            cf = new CWP_CONTACT_FORM { CWP_ID = CwpID };
            db.CWP_CONTACT_FORMs.InsertOnSubmit(cf);
        }
        cf.MAIL_TO = tbEmail.Text ;
        cf.SUBJECT = tbSubject.Text;


        db.SubmitChanges();

        return true;
    }

    protected void btSaveEdit_Click(object sender, EventArgs e)
    {
        SaveData();
    }
}
