﻿<%@ Control Language="C#" AutoEventWireup="true"  CodeFile="cwpContent.ascx.cs" Inherits="SM.EM.UI.Controls._ctrl_module_content_cwpContent" %>
<%@ Register TagPrefix="SM" TagName="Content" Src="~/_ctrl/module/content/cmsContent.ascx"   %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="FredCK.FCKeditorV2" Namespace="FredCK.FCKeditorV2" TagPrefix="FCKeditorV2" %>

<div id='<%= cwp.CWP_ID.ToString() %>' class='<%= "cwp" + (ParentPage.IsModeCMS ? " cwpEdit " : " ")  + CMS_WEB_PART.Common.RenderHiddenClass(cwp) %>'>
    <div id="divEdit" runat="server" class="cwpHeader cwpMove" visible="false" >
<%--                    <img class="cwpMove" src='<%= Page.ResolveUrl("~/_inc/images/icon/move.png") %>'  alt="move" title="move"  />
--%>                    
                    
        <div class="cwpHeaderRight"> &nbsp;</div>

        <div style="float:right; ">
            <a id="btEdit" href="#" title="uredi" onclick="editContentLoad('<%= cwp.CWP_ID.ToString()%>'); return false" class="btEdit" ></a>
<%--            <a id="btEdit" title="uredi" runat="server" class="btEdit" onserverclick="OnEdit"></a>--%>
<% if (PERMISSION.User.Common.CanShowHideModule()){%>            
            <a id="btShow"  title="prikaži" runat="server" class="btShow"></a>
            <a id="btHide" title="skrij" runat="server" class="btHide"></a>
<%} %>
<% if (PERMISSION.User.Common.CanDeleteModule()){%>
            <a id="btDelete" href="#" title="izbriši" class="btDelete" onclick="deleteEL('<%= cwp.CWP_ID.ToString() %>'); return false" ></a>
<%} %>
<% if (PERMISSION.User.Common.CanEditModuleSettings ()){%>
            <a id="brSettings" title="nastavitve" runat="server" class="btSettings"  onserverclick="btSettings_Click"></a>
<%} %>
        </div>
        <div class="cwpHeaderLeft"> &nbsp;</div>
        <a id="btMove" class="btMove"></a>
        
        <div class="cwpHeaderTitle">
            Vsebina
            <a href="javascript:void(0)" class="btsettStatusHelp btcwpHeadHelp"><img src='<%=Page.ResolveUrl("~/_inc/images/icon/help.png") %>' /></a>
            <div class="settStatusWrapp cwpHeadHelpWrapp" >
                <div class="settStatus cwpHeadHelp">
                    Urejevalnik statičnih spletnih vsebin
                    <br />
                    Element omogoča urejanje statičnih vsebin spletne predstavitve. Je osnovni modul, s katerim se vnaša vsebine na spletne strani. Urejanje je izredno enostavno, s pomočjo urejevalnika statičnih vsebin. Vsebino spreminjaš poljubno kadar želiš.
                    <br /><br />
                    Z elementom za urejanje statičnih vsebin lahko urejaš:
                    <br />
                    - dodajanje, odstranjevanje spletne vsebine <br />
                    - dodajanje, odstranjevanje slikovnega gradiva      
                </div>
            </div>
        </div>
    </div>
    <div class="clearing"></div>

    <div class='<%= "cCont " + (ParentPage.IsModeCMS ? "cwpContainer" : "") %>'>

        <div id="divTitleContent" class="cwpH1" runat="server">
            <h1><asp:Literal ID="litTitle" runat="server" ></asp:Literal></h1>
        </div>    

        <div>

            <SM:Content ID="objContent" runat="server" Visible ="true" />
            <div class="clearing"></div>
        </div>
    </div>

</div>