﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewMenuLevelHierarchy.ascx.cs" Inherits="SM.UI.Controls._menu_lookup_cntMenuLevelHierarchy" %>

<ul  id="p<%= ParentID %>" >
    <asp:ListView ID="lvSort" runat="server" >
        <LayoutTemplate>
            <asp:PlaceHolder ID="itemPlaceHolder" runat="server"></asp:PlaceHolder>
        </LayoutTemplate>
                             
        <ItemTemplate >
           <li id='<%# Eval("SMAP_ID") %>' >
                <span><%# Eval("SML_TITLE") %></span>
                <%# RenderChilds(Container.DataItem ) %>
            </li>            
        </ItemTemplate>
    
    </asp:ListView>

</ul>