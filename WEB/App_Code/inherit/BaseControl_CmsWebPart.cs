﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for BaseControl
/// </summary>
public abstract class BaseControl_CmsWebPart : BaseControl
{
    public  SM.EM.UI.BasePage_CmsWebPart ParentPage;
    public CMS_WEB_PART cwp;


    protected override void OnInit(System.EventArgs e)
    {
        base.OnInit(e);

        if (Page is SM.EM.UI.BasePage_CmsWebPart)
            ParentPage = (SM.EM.UI.BasePage_CmsWebPart)this.Page;


    }

    public virtual void  LoadData(){
    }
}
