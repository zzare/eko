﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_cmsPaymentList.ascx.cs" Inherits="SM.UI.Controls._cmsPaymentList" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Namespace="SM.UI.Controls" TagPrefix="SM" %>

<div class="qContList">

        <asp:LinqDataSource ID="ldsList" runat="server" OnSelecting="ldsList_OnSelecting" >
        </asp:LinqDataSource>
        
        <SM:smListView ID="lvList" runat="server" DataSourceID="ldsList" 
             InsertItemPosition="None"  onitemcommand="lvList_ItemCommand" >
            <LayoutTemplate>
                <table  class="tblData" cellpadding="0" cellspacing="0">
                        <thead>
                            <tr id="headerSort" runat="server">
                                <th ><asp:LinkButton ID="LinkButton2" CommandName="sort" CommandArgument="DatePayed" runat="server">Datum</asp:LinkButton></th>
                                <th ><asp:LinkButton ID="LinkButton1" CommandName="sort" CommandArgument="DateAdded" runat="server">Evidentirano</asp:LinkButton></th>
                                <th ><asp:LinkButton ID="LinkButton3" CommandName="sort" CommandArgument="Price" runat="server">Znesek</asp:LinkButton></th>
                                <th ><asp:LinkButton ID="LinkButton4" CommandName="sort" CommandArgument="Type" runat="server">TIP</asp:LinkButton></th>
                                <th ><asp:LinkButton ID="LinkButton5" CommandName="sort" CommandArgument="Notes" runat="server">Opombe</asp:LinkButton></th>
                                <th ><asp:LinkButton ID="LinkButton6" CommandName="sort" CommandArgument="AddedBy" runat="server">Evidentiral</asp:LinkButton></th>
                                <th width="100px" colspan="2" runat="server"  id="headManage" >Uredi</th>
                            </tr>
                        </thead>
                        <tbody id="itemPlaceholder" runat="server"></tbody>
                        <tfoot>
                            <tr>
                                <th style="text-align:right" colspan="8"  >
                                    <asp:DataPager runat="server" ID="DataPager" >
                                        <Fields>
                                            <asp:NextPreviousPagerField  />
                                            <asp:NumericPagerField ButtonCount="5" />
                                        </Fields>
                                    </asp:DataPager>
                                </th>
                            </tr>
                        </tfoot>
                    </table>
            </LayoutTemplate>
            
            <ItemTemplate>
                <tr  class='<%#  Container.DataItemIndex % 2 == 0 ? "  " : " odd " %>'  onmouseover ="this.className='<%#  Container.DataItemIndex % 2 == 0 ? "hovRow" : "hovRowodd" %>'" onmouseout ="this.className = GetCssClass(<%#  Container.DataItemIndex %>)">
                    <td runat="server"  visible='<%# ShowCheckboxColumn %>'>
                        <asp:CheckBox ID="cbSelectUser"  runat="server" />
                        <asp:HiddenField ID="hfUser" runat="server" Value='<%# Eval("UserId") %>' />
                    </td>
                    <td>
                        <%# SM.EM.Helpers.FormatDate(Eval("DatePayed"))%>
                    </td>
                    <td>
                        <%# SM.EM.Helpers.FormatDate(Eval("DateAdded"))%>
                    </td>
                    <td>
                        <%# SM.EM.Helpers.FormatPrice(Eval("Price"))%>
                    </td>
                    <td>
                        <%# PAYMENT.Common.RenderPaymentType( int.Parse( Eval("Type").ToString()))%>
                    </td>
                    <td>
                        <%# Eval("Notes") ?? ""%>
                    </td>
                    <td>
                        <%# Eval("AddedBy")%>
                    </td>
                    <td width="50px" runat="server"  visible='<%# ShowManageColumn && ShowEditColumn %>'>
                        <asp:LinkButton ID="LinkButton7" CommandArgument='<%# Eval("PaymentID") %>' CommandName="edit" runat="server">Edit</asp:LinkButton>
                    </td>
                </tr>  
            
            </ItemTemplate>
            
            <EmptyDataTemplate>
                
                <div class="odd">
                    ni plačil
            
                </div>
                
            </EmptyDataTemplate>
        </SM:smListView>

</div>
