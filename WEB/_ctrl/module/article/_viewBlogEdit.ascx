﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewBlogEdit.ascx.cs" Inherits="_ctrl_module_article_viewBlogEdit" %>
<%@ Register Assembly="FredCK.FCKeditorV2" Namespace="FredCK.FCKeditorV2" TagPrefix="FCKeditorV2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="SM" TagName="ImageAddNew" Src="~/_ctrl/module/gallery/_viewImageAddNew.ascx"   %>

<style type="text/css">
    .ui-datepicker
    {
        z-index: 150005;
    }
</style>

<script language = "javascript" type="text/javascript">
    function editBlogSave(id, cwp, v, act) {
        var t = $("#" + v).find("#tbTitle").val();

        var ds = $("#" + v).find("#tbReleaseDate").val();
        var de = $("#" + v).find("#tbExpireDate").val();
        var a = $("#" + v).find("#tbAbstract").val();
        var b = FCKeditorAPI.GetInstance('<%= fckBody.ClientID %>').GetXHTML(true);
        var c = $("#" + v).find(".cbComments").attr('checked');
        var ap = $("#" + v).find(".cbApproved").attr('checked');
        var exp = $("#" + v).find(".cbExposed").attr('checked');
        var sw = $("#" + v).find(".cbSortWeight").attr('checked') ? 1 : 0;
        var emb = $("#" + v).find(".tbBlogVideo").val();
        
        if (ap == null || ap == undefined)
            ap = false;

        // blog
        c = true;

        var smp = $("#hfBlogSmap").val();

        var _itemsAdd = new Array();
        $("#artSmapList input:checkbox:checked").each(function (i, val) {
            Array.add(_itemsAdd, $(this).val());
        });

        //alert(t + '\n' + ds + '\n' + de + '\n' + a + '\n' + b + '\n');

        
        openModalLoading('Shranjujem...');
        WebServiceP.SaveBlogEdit(blogNewRefID, id, cwp, t, ds, de, a, b, c, ap, exp, sw, em_g_lang, smp, emb, _itemsAdd, onBlogSuccessSave, onSaveError, cwp);

    }

    function onBlogSuccessSave(ret, cwp) {
        if (ret.Code == 0) {
            setModalLoaderErrValidation('', ret.Message);
            return;
        }
        else if (ret.Code == 2) {
            // refresh
            closeModalLoader();
            openModalLoading('Osvežujem...');
            //document.location.reload(false);
            window.location.href = window.location.href;
            return;
        }
        else if (ret.Code != 1) {
            setModalLoaderErr(ret.Message, true);
            return;
        }

        $("#" + cwp).find('div.cArtList').html(ret.Data).parent().sm_animate_change();
    }


    function deleteArticle(a, del) {
        var ok = confirm('Ali res želite zbrisati ta zapis?');
        if (!ok)
            return;

        openModalLoading('Brišem...');
        WebServiceCWP.DeleteArticle(a, del, em_g_lang, onArticleSettingsSuccessSave, onSaveError);
    }


    $(document).ready(function () {
        $.datepicker.setDefaults($.extend({ showMonthAfterYear: false }, $.datepicker.regional['']));
        //        $(".datepicker").datepicker({
        //            beforeShow: function(i, e) {
        //                var z = jQuery(i).closest(".ui-dialog").css("z-index") + 4;
        //                e.dpDiv.css('z-index', z);
        //            }
        //        });
        $.datepicker.setDefaults($.datepicker.regional['sl']);
        $(".datepicker").datepicker({
            buttonText: 'izberi datum',
            numberOfMonths: 1,
            showButtonPanel: true,
            changeMonth: true,
            changeYear: true,
            showOn: 'both',
            buttonImage: '<%= SM.BLL.Common. ResolveUrl("~/CMS/_res/images/button/calendar.png") %>',
            buttonImageOnly: true,
            showAnim: 'fadeIn'
        });

    });

</script>


<div id='<%= ViewID %>' >
    
        <div class="modPopup_mainArticle">
            <table cellpadding="0" cellspacing="0" width="98%" class="tblData">
                <tr>
                    <td colspan="7">
                        <div class="artEdit">
                            <span class="modPopup_mainTitle" >Naslov vsebine</span>
                            <input class="modPopup_TBXsettings" style="width:99%;" id="tbTitle" maxlength="256" type="text"  value='<%=  Article.ART_TITLE %>' />
                        </div>
                        
                        <div style="<%= (SM.EM.Security.Permissions.IsPortalAdmin() ? "" : "display:none;" ) %>">
                        <span class="modPopup_mainTitle"  >Vidnost vsebine</span>
                        <div class="floatL">
                            <span class="artEdit">OD:</span>
                            <input class="modPopup_TBXsettings modPopup_TBXdate datepicker" id="tbReleaseDate" value='<%= SM.EM.Helpers.FormatDate( Article.RELEASE_DATE ) %>' type="text"   />
                            

                            <span class="artEdit">DO:</span>
                            <input class="modPopup_TBXsettings modPopup_TBXdate datepicker" id="tbExpireDate" value='<%= SM.EM.Helpers.FormatDate(  Article.EXPIRE_DATE ) %>' type="text"   />
 

                        </div>
                        <div class="clearing"></div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th class="artEdit">
                        <span class="modPopup_mainTitle artEdit" >Kratek uvod:</span>
                    </th>
                </tr>
                <tr>
                    <td colspan="7">
                        <textarea id="tbAbstract" style="width:99%" class="modPopup_TBXsettings modPopup_TBXsettingsGalLang" rows="4" cols="50"   ><%= Article.ART_ABSTRACT %></textarea>
                        
                    </td>
                </tr>
                <tr>
                    <th class="artEdit">

                        <span class="modPopup_mainTitle artEdit" >Vsebina</span>
                        <%--<asp:RequiredFieldValidator EnableClientScript="false" ControlToValidate="fckBody" ID="reqBody" runat="server" ErrorMessage="* Vpiši vsebino"  Display="Dynamic" ValidationGroup="editArt"></asp:RequiredFieldValidator>--%>

                    </th>
                </tr>
                <tr>
                    <td colspan="7">
                    
                        <FCKeditorV2:FCKeditor  ID="fckBody" ToolbarSet="CMS_Content" runat="server" Height="300px" Width="99%" ></FCKeditorV2:FCKeditor>
                    </td>
                </tr>

                <tr>
                    <th class="artEdit">
                        <span class="modPopup_mainTitle artEdit" >Slike</span>
                    </th>
                </tr>
                <tr>
                    <td colspan="7">
                         <SM:ImageAddNew ID="objAddNewImage" runat="server" />
                    </td>
                </tr>
                <tr>
                    <th class="artEdit">
                        <span class="modPopup_mainTitle artEdit" >Video URL (Youtube)</span>
                    </th>
                </tr>
                <tr>
                    <td colspan="7">
                        <input class="modPopup_TBXsettings tbBlogVideo" style="width:99%;"  maxlength="256" type="text"  value='<%=  Article.EMBEDD %>' /> 
                    </td>
                </tr>

                <tr>
                    <th class="artEdit">
                        <span class="modPopup_mainTitle artEdit" >Glavna kategorija</span>
                    </th>
                </tr>
                <tr>
                    <td colspan="7">
                        <%--<span class="modPopup_mainTitle">Oddelek:</span> --%>
                        <span id="blogSmapTitle" ><%= SitemapLang.SML_TITLE %></span> 
                        <input id="hfBlogSmap" type="hidden" value='<%= ( SitemapLang != null) ? SitemapLang.SMAP_ID : -1  %>' /> 
                        <img onclick="openLookup('blogSmapTitle', 'hfBlogSmap', 2, <%= SM.BLL.Custom.Settings.SmpKlepetalnica() %>); return false" alt="prebrskaj" src='<%= SM.BLL.Common.ResolveUrl("~/_inc/images/button/lookup.png")%>' />

                    </td>
                </tr>

                <tr>
                    <th class="artEdit">
                        <span class="modPopup_mainTitle artEdit" >Dodatno izpostavljeno v kategorijah</span>
                    </th>
                </tr>
                <tr>
                    <td colspan="7">
                        <asp:ListView ID="lvProductSmap" runat="server">
                            <LayoutTemplate>
                                <div id="artSmapList">
                                    <asp:PlaceHolder ID="itemPlaceholder" runat="server"/>
                                </div>
                
                            </LayoutTemplate>

                            <ItemTemplate>
                                <label>
                                    <input <%# RenderCheckedProdSmap(Container.DataItem) %> type="checkbox" value='<%# Eval("SMAP_ID") %>' />
                                    &nbsp;<%# Eval("SML_TITLE")%>
                                </label>
            
                            </ItemTemplate>
                        </asp:ListView>        

                    </td>
                </tr>


                <tr style="display:none;">
                    <th colspan="7">

                        <span class="modPopup_mainTitle artEdit" >Dodatno</span>
                    </th>
                </tr>
                <tr style="display:none;">
                    <td>
                            <div style="float:left; padding-top:3px;">
                                <label  for="<%= cbComments.ClientID %>"> <input class="cbComments"  id="cbComments" runat="server" name ="cbComments" type="checkbox" />&nbsp;&nbsp;Omogoči komentiranje</label>
                            </div>
<%--                            <div style="float:left; padding-top:3px; padding-left:20px;">
                                <label  for="<%= cbApproved.ClientID %>"> <input class="cbApproved"  id="cbApproved" runat="server" name ="cbApproved" type="checkbox" />&nbsp;&nbsp;Dodatno izpostavi</label>
                            </div>--%>
                            <div style="float:left; padding-top:3px; padding-left:20px;">
                                <label  for="<%= cbExposed.ClientID %>"> <input class="cbExposed"  id="cbExposed" runat="server" name ="cbExposed" type="checkbox" />&nbsp;&nbsp;Izpostavljena oblika (poudarjeno)</label>
                            </div>
                    </td>
                </tr>   

                <tr style="<%= (SM.EM.Security.Permissions.IsPortalAdmin  () == false) ? "display:none;" : "" %>" >
                    <td>
                            <div style="float:left; padding-top:3px;">
                                <label  for="<%= cbSortWeight.ClientID %>"> <input class="cbSortWeight"  id="cbSortWeight" runat="server" name ="cbSortWeight" type="checkbox" />&nbsp;&nbsp;Večja prioriteta na vstopni</label>
                            </div>
                            <div style="float:left; padding-top:3px; padding-left:20px;">
                                <label  for="<%= cbApproved.ClientID %>"> <input class="cbApproved"  id="cbApproved" runat="server" name ="cbApproved" type="checkbox" />&nbsp;&nbsp;Potrjen</label>
                            </div>
                    </td>
                </tr>  
                             
            </table>
        </div>    
  

    <div class="modPopup_footer"> 
        <div class="buttonwrapperModal">
            <% if (Article.ART_ID > 0){%>
            <div class="floatR">
                <a class="lbutton lbuttonDelete" href="#" onclick="deleteArticle('<%= Article.ART_GUID.ToString() %>', <%= (!Article.ACTIVE).ToString().ToLower() %>); return false" ><span><%= (Article.ACTIVE ? "ZBRIŠI" : "PREKLIČI BRISANJE" ) %></span></a>            
            </div>            
            <%} %>
            <a class="lbutton lbuttonConfirm" href="#" onclick="editBlogSave('<%= Article.ART_ID %>', '<%= Article.CWP_ID.ToString() %>', '<%= ViewID.ToString() %>'); return false"><span>Shrani</span></a>
            <a class="lbutton lbuttonEdit" href="#" onclick="closeModalLoader(); return false" ><span>Prekliči</span></a>
            
        </div>
    </div>
</div>