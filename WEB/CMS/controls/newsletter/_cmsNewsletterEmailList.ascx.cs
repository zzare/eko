﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;


namespace SM.UI.Controls
{
    public partial class _cmsNewsletterEmailList : BaseControl 
    {
        public delegate void OnEditEventHandler(object sender, SM.BLL.Common.Args .IDguidEventArgs e);
        public event OnEditEventHandler OnEdit;
        public event EventHandler  OnDelete;
        public string OnEditTarget = "_self";
        public int PageSize = 20;

        public string SearchText { get; set; }
        public bool? Active { get; set; }
        public short? Status {get; set;}
        public bool IsAdmin { get; set; }
        public Guid NewsletterID{get; set;}

        
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        protected void ldsList_OnSelecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            eMenikDataContext db = new eMenikDataContext();

            var  list = from l in db.NEWSLETTER_EMAILs where l.NewsletterID == NewsletterID    select l;

            // result
            e.Result = list.OrderByDescending(o => o.Email  );
        }


        public void BindData()
        {
            lvList.DataBind();


        }




        protected void lvList_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            if (e.CommandName == "del") { 
                // delete item
                NEWSLETTER.DeleteNewsletterEmail(int.Parse(e.CommandArgument.ToString() ));

                OnDelete(this, new EventArgs());
            }
        }
}


}