﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SM.EM.UI;
using SM.EM.UI.Controls;
using SM.EM;

public partial class _em_content_ArticleDetails : SM.EM.UI.BasePage_CmsWebPart
{

    public int ArtID
    {
        get
        {
            int _artID = -1;
            if (Request.QueryString["art"] == null) return _artID;
            if (!Int32.TryParse(SM.EM.Helpers.GetQueryString ("art") , out _artID)) return _artID;
            return _artID;
        }

    }

    Data.ArticleData  _art;
    public Data.ArticleData art
    {
        get
        {
            if (_art != null) return _art;

            ArticleRep arep = new ArticleRep();


            _art =  arep.GetArticleByID(ArtID);

            // tmp
            //eMenikDataContext db = new eMenikDataContext();
            //var list = from c in db.CMS_WEB_PARTs
            //           from a in db.ARTICLEs 
            //           from s in db.SITEMAPs 
            //    where c.MOD_ID == 2 && a.CWP_ID == c.CWP_ID && s.SMAP_GUID == c.REF_ID   select new {s.SMAP_ID, a.ART_ID, a.ART_GUID , c.UserId  };
            //foreach (var item in list)
            //{
            //    _art = arep.GetArticleByIDSitemap(item.ART_ID, item.SMAP_ID);
            //    if (  _art == null   )
            //        CMS_WEB_PART.CreateNewCwpGallery(SM.BLL.Common.GalleryType.DefaultArticle, item.ART_GUID, null, MODULE.Common.GalleryModID, ARTICLE.Common.MAIN_GALLERY_ZONE, item.UserId.Value , "Galerija", "", true, false, -1, false, false, 3, SM.BLL.Common.ImageType.ArticleDetailsThumbDefault, SM.BLL.Common.ImageType.EmenikBigDefault);
        
            
            
            //}



            // error
            if (_art == null)
                ErrorRedirect(41, 404);

            return _art;
        }
        set { _art = value; }
    }

    public override Guid? CwpRefID
    {
        get
        {
            if (art != null)
                _CwpRefID = art.ART_GUID;

            return _CwpRefID;
        }
        set{
            base.CwpRefID = value;
        }
    }

    protected override void OnInit(EventArgs e)
    {
        objComment.EnableNonAuthenticatedComments = true;
        PageModule = SM.BLL.Common.PageModule.ArticleDetails;

        // set sitemap id
        CurrentSitemapID = art .CAT_SMAP_ID ;

        base.OnInit(e);

    }

    public override void LoadCwpList()
    {
        bool? active = true;
        if (IsModeCMS) active = null; // if edit mode, show all

        // get all webparts on current page by language ( not deleted ones)
        CwpList = CMS_WEB_PART.GetWebpartsByArticleUserModules(ArtID , LANGUAGE.GetCurrentLang(), active, false, em_user.UserId).ToList();
            
      //  base.LoadCwpList();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //objEditArticle.OnArticleListChanged += new EventHandler(objEditArticle_OnArticleListChanged);
        objEditModule.OnReload += new EventHandler(objEditModule_OnReload);

        objCommentList.OnDeleteCommentClick += new SM.EM.UI.Controls._cntCommentList.OnEditEventHandler(objCommentList_OnDeleteCommentClick);
        objCommentList.OnEditCommentClick += new SM.EM.UI.Controls._cntCommentList.OnEditEventHandler(objCommentList_OnEditCommentClick);

        objComment.OnListChanged += new _cntCommentEdit.OnEditEventHandler(objComment_OnListChanged);



        // wpz
        mnMain.CurrentWebPartZone = wpz_art_1.ID;
        //mnRight.CurrentWebPartZone = wpz_right.ID;

        cmsArticleDetails.CurrentSitemapID = CurrentSitemapID;
        cmsArticleDetails.art = art;

        // init comments
        objComment.RefInt = cmsArticleDetails.ArtID;
        objComment.ForceTitle = cmsArticleDetails.art.ART_TITLE;
        objComment.ShowWWW = false;
        objComment.IsAuthor = IsEditMode;

        objCommentList.ForceTitle = cmsArticleDetails.art.ART_TITLE;
        objCommentList.RefInt = cmsArticleDetails.ArtID;
        objCommentList.AutoLoad = false;
        objCommentList.CanEditComment = SM.EM.Security.Permissions.IsPortalAdmin();

        // set ref_id for base
        CwpRefID = CwpRefID;


    }

    protected override void LoadTitle()
    {
        base.LoadTitle();

        // append article to title
        Page.Title = cmsArticleDetails.art.ART_TITLE +  " | " +Page.Title;

    }

    protected override void SetRelCanonical()
    {
        // base.SetRelCanonical();
        RelCanonical =  Rewrite.EmenikUserUrlGetLeftPart(this.em_user ) +  Rewrite.EmenikUserArticleDetailsUrl(this.PageName, CurrentSitemapID, PageModule, cmsArticleDetails.ArtID, cmsArticleDetails.art.ART_TITLE).TrimStart("~".ToCharArray());

    }

    protected override void OnLoadComplete(EventArgs e)
    {
        base.OnLoadComplete(e);

        LoadComments();
    }

    protected void LoadComments() {

        if (cmsArticleDetails.art.COMMENTS_ENABLED)
        {
            phComment.Visible = true;

            objComment.InitData();
            //init edit mode
            objCommentList.IsEditMode = IsEditMode && IsModeCMS;
            objCommentList.BindData();

        }
        else
        {
            phComment.Visible = false;

        }
    
    }

    void objComment_OnListChanged(object sender, SM.BLL.Common.Args.IDguidEventArgs e)
    {
        // reload data
        this.RemovePageFromCache();

        Response.Redirect(Request.Url.AbsolutePath  + "#comm" + e.ID );
     
    }


    void objEditModule_OnReload(object sender, EventArgs e)
    {
        // reload data
            cmsArticleDetails.LoadData();

            LoadComments();
    }

    void objCommentList_OnEditCommentClick(object sender, SM.BLL.Common.ClickIDEventArgs e)
    {
        // edit comment
        objComment.CommentRefID = e.ID;
        objComment.LoadData();
    }

    void objCommentList_OnDeleteCommentClick(object sender, SM.BLL.Common.ClickIDEventArgs e)
    {
        objCommentList.BindData();
    }


    protected override void OnPreRender(EventArgs e)
    {
        if (cmsArticleDetails.art == null)
            ErrorRedirect(8);

        // init
//        divEdit.Visible = IsModeCMS;

        base.OnPreRender(e);
    }

    protected void btEdit_Click(object sender, EventArgs e)
    {

//        CwpEdit edit = objEditArticle;
        CwpEdit edit = EditArticle;
        if (edit == null)
            return;
        edit.IDref = cmsArticleDetails.ArtID;
        edit.CwpID = cmsArticleDetails.art.CWP_ID.Value ;
//        edit.cwp = CMS_WEB_PART.GetWebpartByID(edit.CwpID);

        edit.LoadData();


    }




    protected string RenderBackToPageHref() {

        string smapTitle = "default";
        smapTitle = this.CurrentPageTitle;
        
        return Page.ResolveUrl(SM.EM.Rewrite.EmenikUserUrl(PageName, CurrentSitemapID, smapTitle ));
    
    }

    public override string RenderBreadCrumbs(string separator)
    {
        string _curclass = "class=\"sel\"";
        return base.RenderBreadCrumbs(CurrentSmpEmenik, separator, false) + separator + "<a " + _curclass + " href=\"" + RelCanonical + "\">" + art.ART_TITLE + "</a>";
    }


}
