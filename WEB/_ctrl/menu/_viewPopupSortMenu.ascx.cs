﻿using System;
using System.Collections;
using System.Collections.Generic ;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


namespace SM.UI.Controls
{
    public partial class _viewPopupSortMenu : BaseControlView
    {
        public object Data;
        public object[] Parameters;



        public int ParentID
        {
            get;
            set;
        }
        public string  Language { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {


                LoadData();

        }

        protected void LoadData()
        {

            if (Data == null)
                return ;

            if (Parameters != null) {
                if (Parameters.Length > 0 && Parameters[0] != null)
                    ParentID  = (int)Parameters[0];
                if (Parameters.Length > 1 && Parameters[1] != null)
                    Language  = Parameters[1].ToString();
            }



            if (ParentID < 1)
                return ;

            objSort.Data = Data;
            objSort.ParentID = ParentID;
        
        
        }

    }
}
