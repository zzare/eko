﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewMenuPageModuleEdit.ascx.cs" Inherits="_ctrl_menu_edit_viewMenuPageModuleEdit" %>


<h1>Tip postavitve na spletni predstavitvi:
    <a href="javascript:void(0)" class="btsettStatusHelp"><img src='<%=SM.BLL.Common.ResolveUrl ("~/_inc/images/icon/help.png") %>' /></a>
    <div class="settStatusWrapp" >
    <div class="settStatus">
    Izberi koliko stolpcev za vsebine želiš na podstrani.
    </div></div>
</h1>

<div id="cEditMenuPmod">
    <asp:ListView ID="lvPageMod" runat="server" InsertItemPosition="None" >
        <LayoutTemplate>
            <div class="wizzNav_lvPageMod">
                <ul id="itemPlaceholder" runat="server">
                </ul>
            </div>                
        </LayoutTemplate>
        
        <ItemTemplate>
            <li>
                <label >
                    <input  type="radio" name="pageMod" <%# RenderChecked(Container.DataItem) %> value='<%# Eval("PMOD_ID") %>' />
                    <span ><%# Eval("PMOD_TITLE")%></span>
                </label>
                
                <%--<asp:RadioButton ID="rbPageMod" runat="server" GroupName="pageMod" Text= '<%#  Eval("PMOD_TITLE")%>'/>--%>
                <br />
                <img src='<%#  SM.BLL.Common.ResolveUrl ((Eval("THUMB_URL") ?? "").ToString())%>' alt='<%#  Eval("PMOD_TITLE")%>' />
                <br />
                
                <%#  Eval("PMOD_DESC")%>
                
            </li>
        
        </ItemTemplate>
    </asp:ListView>

    <div class="clearing"></div>
</div>