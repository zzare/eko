﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Net.Mail;

namespace SM.CMS.UI.Controls
{

    public partial class _cntContactForm : BaseControl 
    {

        //public Guid CwpID = Guid.Empty;
        public string Subject { get; set; }
        public string MailTo { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void btSend_Click(object sender, EventArgs e)
        {
            Page.Validate("contForm");
            if (!Page.IsValid) return;

            // subject
            if (String.IsNullOrEmpty(Subject))
                Subject = "Spletno povpraševanje";

            // mail to
            if (String.IsNullOrEmpty(MailTo ))
                MailTo  = SM.EM.Mail.Account.Info.Email  ;


            //CWP_CONTACT_FORM cf = CMS_WEB_PART.GetCwpContactFormByID(CwpID);
//            string mailTo = ParentPage.em_user.EMAIL ;
            string subject = Subject ;
//            string mailFromName = ParentPage.em_user.PAGE_NAME;

            //if (cf != null) {
            //    if (cf.MAIL_TO != null && ! string.IsNullOrEmpty(cf.MAIL_TO ))
            //        mailTo = cf.MAIL_TO;
            //    if (cf.SUBJECT != null && !string.IsNullOrEmpty(cf.SUBJECT))
            //        subject   = cf.SUBJECT;
            
            //}

            // send mail
            MailMessage message = new MailMessage();
            message.From = new MailAddress(tbEmail.Text);
            message.ReplyTo = new MailAddress(tbEmail.Text);
            message.To.Add(new MailAddress(MailTo ));


            message.Subject = subject  + ":" + tbSubject.Text;
            message.Body = "Zadeva: " + tbSubject.Text + "<br/>" + "Ime: " + tbName.Text + "<br/>" + tbBody.Text;
            message.IsBodyHtml = true;

            SmtpClient client = new SmtpClient();


            try
            {

                // if local, use gmail, else use server SMTP
                if (HttpContext.Current != null && HttpContext.Current.Request.IsLocal)
                {
                    client.Credentials = new System.Net.NetworkCredential(SM.EM.Mail.Account.Send.Email, SM.EM.Mail.Account.Send.Pass);
                    client.Port = 587;//or use 587            
                    client.Host = "smtp.gmail.com";
                    client.EnableSsl = true;
                }
                else
                {
                    //smtpClient.DeliveryMethod = SmtpDeliveryMethod.PickupDirectoryFromIis;

                    // network delivery for propely setting SENDER header in mail
                    client.DeliveryMethod = SmtpDeliveryMethod.Network;
                    client.Host = "localhost";
                }




                client.Send(message);
                mvContact.SetActiveView(vSent);
            }
            catch (Exception ex)
            {
            }

        }

    }
}