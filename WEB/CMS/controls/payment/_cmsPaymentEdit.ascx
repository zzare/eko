﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_cmsPaymentEdit.ascx.cs" Inherits="SM.UI.Controls._cmsPaymentEdit" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<div class = "qContEdit clearfix">
        
    <asp:Button ValidationGroup="payment" ID="btPay" runat="server" Text="ADD NEW PAYMENT" onclick="btPay_Click" CssClass="btnOrange " />

    <br />

   
   DATUM PLAČILA:
   <br />
    <asp:TextBox ID="tbDatePayed"  runat="server" ></asp:TextBox>
    <asp:ImageButton ID="btDatePayed" ImageUrl="~/CMS/_res/images/button/calendar.png" runat="server" />&nbsp
    <cc1:CalendarExtender ID="ceDatePayed" runat="server" TargetControlID="tbDatePayed" PopupPosition="BottomLeft" PopupButtonID ="btDatePayed">
    </cc1:CalendarExtender>                            
    <cc1:MaskedEditExtender ID="meeDatePayed" runat="server" TargetControlID="tbDatePayed" Mask="99/99/9999" MessageValidatorTip="true" 
            MaskType="Date" DisplayMoney="Left" AcceptNegative="Left" ErrorTooltipEnabled="True"  CultureName="sl-SI" />                            
    <cc1:MaskedEditValidator ID="mevDatePayed" runat="server" ControlExtender="meeDatePayed" ControlToValidate="tbDatePayed"  InvalidValueMessage="* Datum je neveljaven"
        Display="Dynamic" TooltipMessage="" EmptyValueBlurredText="" InvalidValueBlurredMessage=""  ValidationGroup="payment" />   

    <br />
    ZNESEK:
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="payment"  ErrorMessage="* Vpiši znesek" ControlToValidate="tbPrice"></asp:RequiredFieldValidator>
    <br />
    <asp:TextBox ID="tbPrice"  runat="server"></asp:TextBox>
    
    <br />
    OPOMBE: 
    <br />
    <asp:TextBox ID="tbNotes"  runat="server" TextMode="MultiLine" Rows="3"></asp:TextBox>
    
    <br />    
    TIP:
    
    <br />
    <asp:RequiredFieldValidator ID="rfvType" runat="server" ValidationGroup="payment"  ErrorMessage="* Izberi tip plačila" ControlToValidate="rblType"></asp:RequiredFieldValidator>
    <asp:RadioButtonList  ID="rblType" runat="server">
    </asp:RadioButtonList>
    

    
</div>

