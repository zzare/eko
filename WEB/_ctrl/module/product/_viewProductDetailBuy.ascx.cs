﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _ctrl_module_product_viewProductDetailBuy : BaseControlView
{

    public object Data;
    public object[] Parameters;
    public vw_Product Prod;



    protected void Page_Load(object sender, EventArgs e)
    {
        

        //LoadList();
    }

    protected override void OnPreRender(EventArgs e)
    {
        LoadList();
        base.OnPreRender(e);
    }

    public void LoadList() {

        if (Data == null)
            return;
        Prod = Data as vw_Product;

        if (Parameters != null)
        {
            if (Parameters[0] != null)
                IsModeCMS = bool.Parse(Parameters[0].ToString());
            //if (Parameters.Length > 1 && Parameters[1] != null)
            //    LangID = Parameters[1].ToString();
        }


        // attribute
        ProductRep prep = new ProductRep();

        objProductAttribute.Data = Prod.PRODUCT_ID;
        objProductAttribute.IsModeCMS = IsModeCMS  ;

        objProductAttribute.ProductGroupList = prep.GetGroupAttributesByProduct(Prod.PRODUCT_ID, Prod.LANG_ID ).ToList();
//        objProductAttribute.ProductAttributeList = prep.GetAttributesByProduct(Prod.PRODUCT_ID).ToList();
//        objProductAttribute.ProductAttributeList = prep.GetwProductAttributesByProduct(Prod.PRODUCT_ID).ToList();
        objProductAttribute.ProductAttributeList = prep.GetwProductAttributeDescsByProduct(Prod.PRODUCT_ID, Prod.LANG_ID ).ToList();
        objProductAttribute.LoadData();


    }



}
