﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_cntOrderEdit.ascx.cs" Inherits="SM.UI.Controls.shopping_order_cntOrderEdit" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%--<%@ Register TagPrefix="SM" TagName="OrderItemList" Src="~/c/user/_ctrl/_cntOrderItemListTicket.ascx"  %>--%>
<%--<%@ Register TagPrefix="SM" TagName="ShoppingCart" Src="~/_ctrl/module/product/cart/_viewShoppingCart.ascx"   %>  --%>
<%@ Register TagPrefix="SM" TagName="OrderDetail" Src="~/_em/content/order/_ctrl/_ctrlOrderDetail.ascx"    %>

<div class = "qContEdit clearfix">
        
    <%--<asp:Button ValidationGroup="ordredit" ID="btSave" runat="server" Text="Save user" onclick="btSave_Click" CssClass="btnOrange " />
--%>
    <asp:Button ID="btCancel" runat="server" Text="Zapri" onclick="btCancel_Click" CssClass="btnRed"/>


    <br />

    
    <asp:Panel ID="panEdit" runat="server" Visible="false" CssClass="floatR">
        <div>
            Št. naročila: <asp:TextBox ID="tbOrderRefID" ReadOnly="true" runat="server"></asp:TextBox>
            <br />
            Datum oddaje: <asp:Literal ID="litDateStart" runat="server"></asp:Literal>
            <br />
            Uporabnik: <a runat="server" id="lnkUser" target="_blank" title="edit user" >user</a>
            <br />
            Znesek: <asp:Literal ID="litTotal" runat="server"></asp:Literal>
            <br />

             Izvoz:<a target="_blank" href="<%= SM.BLL.Common.ResolveUrlFull(SHOPPING_ORDER.Common.Export.GetExportOrderXmlHref(Order.OrderID )) %>">XML</a>   
             <br />
             Račun:<a target='_blank' href='<%= Page.ResolveUrl( SM.EM.Rewrite.OrderProductUrl( SM.BLL.Custom.MainUserID,  Order.OrderID, 5, "v=1")) %>' title="račun" >račun</a>                        
        
        </div>
    </asp:Panel>
    


    <h2>Status: <asp:Literal ID="litStatus" runat="server"></asp:Literal></h2>
    
    <br />   
    <h3>Spremeni status</h3>
    
    <asp:PlaceHolder ID="phSetPayed" runat="server">
    
        <asp:Button ValidationGroup="ordredit" OnClick="btSetPayed_Click" ID="btSetPayed" OnClientClick="return confirm('Ste prepričani, da želite nadaljevati? Sprememba statusa nazaj ni možna');" runat="server" Text="Naročilo je plačano >>" />
        datum plačila:
        <asp:TextBox  ID="tbDatePayed" runat="server"></asp:TextBox>
        <asp:ImageButton ID="btDatePayed" ImageUrl="~/CMS/_res/images/button/calendar.png" runat="server" />&nbsp
        <cc1:CalendarExtender  ID="ceDatePayed" runat="server" TargetControlID="tbDatePayed" PopupPosition="BottomLeft" PopupButtonID ="btDatePayed">
        </cc1:CalendarExtender>                            
        <cc1:MaskedEditExtender ID="meeDatePayed" runat="server" TargetControlID="tbDatePayed" Mask="99/99/9999" MessageValidatorTip="true" 
                MaskType="Date" DisplayMoney="Left" AcceptNegative="Left" ErrorTooltipEnabled="True"  CultureName="sl-SI" />                            
        <cc1:MaskedEditValidator ID="mevDatePayed" runat="server" ControlExtender="meeDatePayed" ControlToValidate="tbDatePayed"  InvalidValueMessage="* Datum je neveljaven"
            Display="Dynamic" TooltipMessage="" EmptyValueBlurredText="" InvalidValueBlurredMessage=""  ValidationGroup="payment" />   
        <asp:RequiredFieldValidator ValidationGroup="ordredit" ControlToValidate="tbDatePayed"  ID="rfvDatePayed" runat="server" ErrorMessage="* Izberi datum plačila"></asp:RequiredFieldValidator>
        <br />
    </asp:PlaceHolder>

    <asp:PlaceHolder ID="phSetPayedOnline" runat="server">
    
        <asp:Button ValidationGroup="ordredit" OnClick="btSetPayedOnline_Click" ID="btSetPayedOnline" OnClientClick="return confirm('Ste prepričani, da želite nadaljevati? Sprememba statusa nazaj ni možna');" runat="server" Text="Prenesi denar (naročilo bo plačano) >>" />
        <%--datum plačila:
        <asp:TextBox  ID="tbDatePayedOnline" runat="server"></asp:TextBox>
        <asp:ImageButton ID="btDatePayedOnline" ImageUrl="~/CMS/_res/images/button/calendar.png" runat="server" />&nbsp
        <cc1:CalendarExtender  ID="ceDatePayedOnline" runat="server" TargetControlID="tbDatePayedOnline" PopupPosition="BottomLeft" PopupButtonID ="btDatePayedOnline">
        </cc1:CalendarExtender>                            
       <cc1:MaskedEditExtender ID="meeDatePayedOnline" runat="server" TargetControlID="tbDatePayedOnline" Mask="99/99/9999" MessageValidatorTip="true" 
                MaskType="Date" DisplayMoney="Left" AcceptNegative="Left" ErrorTooltipEnabled="True"  CultureName="sl-SI" />                            
         <cc1:MaskedEditValidator ID="MaskedEditValidator1" runat="server" ControlExtender="meeDatePayedOnline" ControlToValidate="tbDatePayedOnline"  InvalidValueMessage="* Datum je neveljaven"
            Display="Dynamic" TooltipMessage="" EmptyValueBlurredText="" InvalidValueBlurredMessage=""  ValidationGroup="payment" />   
        <asp:RequiredFieldValidator ValidationGroup="ordredit" ControlToValidate="tbDatePayedOnline"  ID="rfvDatePayedOnline" runat="server" ErrorMessage="* Izberi datum plačila"></asp:RequiredFieldValidator>
        --%><br />
    </asp:PlaceHolder>

    <asp:PlaceHolder ID="phDeliverySend" runat="server">
    
        <asp:Button ValidationGroup="ordredit" OnClick="btDeliverySend_Click" ID="btDeliverySend" OnClientClick="return confirm('Ste prepričani, da želite nadaljevati? Stranka bo obveščena po emailu');" runat="server" Text="Naročilo je poslano >>" />
        <br />
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="phDelivery" runat="server">
    
        <asp:Button ValidationGroup="ordredit" OnClick="btDeliveryFinished_Click" ID="btDeliveryFinished" OnClientClick="return confirm('Ste prepričani, da želite nadaljevati? Naročilo bo zaključeno');" runat="server" Text="Naročilo je plačano >>" />
        datum plačila:
        <asp:TextBox  ID="tbDateDeliveryFinished" runat="server"></asp:TextBox>
        <asp:ImageButton ID="btDateDeliveryFinished" ImageUrl="~/CMS/_res/images/button/calendar.png" runat="server" />&nbsp
        <cc1:CalendarExtender  ID="CalendarExtender1" runat="server" TargetControlID="tbDateDeliveryFinished" PopupPosition="BottomLeft" PopupButtonID ="btDateDeliveryFinished">
        </cc1:CalendarExtender>                            
        <cc1:MaskedEditExtender ID="meeDateDeliveryFinished" runat="server" TargetControlID="tbDateDeliveryFinished" Mask="99/99/9999" MessageValidatorTip="true" 
                MaskType="Date" DisplayMoney="Left" AcceptNegative="Left" ErrorTooltipEnabled="True"  CultureName="sl-SI" />                            
        <cc1:MaskedEditValidator ID="mevDateDeliveryFinished" runat="server" ControlExtender="meeDateDeliveryFinished" ControlToValidate="tbDateDeliveryFinished"  InvalidValueMessage="* Datum je neveljaven"
            Display="Dynamic" TooltipMessage="" EmptyValueBlurredText="" InvalidValueBlurredMessage=""  ValidationGroup="ordreditDEl_FIN" />   
        <asp:RequiredFieldValidator ValidationGroup="ordreditDEl_FIN" ControlToValidate="tbDateDeliveryFinished"  ID="RequiredFieldValidator1" runat="server" ErrorMessage="* Izberi datum plačila"></asp:RequiredFieldValidator>
        <br />
    </asp:PlaceHolder>
    
    
    <asp:PlaceHolder ID="phSetFinished" runat="server">
        <asp:Button OnClick = "btSetFinished_Click" ID="btSetFinished" runat="server" Text="Naročilo je odpremljeno >>" />
        datum :
        <asp:TextBox  ID="tbDateFinished" runat="server"></asp:TextBox>
        <asp:ImageButton ID="btDateFinished" ImageUrl="~/CMS/_res/images/button/calendar.png" runat="server" />&nbsp
        <cc1:CalendarExtender  ID="ceDateFinished" runat="server" TargetControlID="tbDateFinished" PopupPosition="BottomLeft" PopupButtonID ="btDateFinished">
        </cc1:CalendarExtender>                            
        <cc1:MaskedEditExtender ID="meeDateFinished" runat="server" TargetControlID="tbDateFinished" Mask="99/99/9999" MessageValidatorTip="true" 
                MaskType="Date" DisplayMoney="Left" AcceptNegative="Left" ErrorTooltipEnabled="True"  CultureName="sl-SI" />                            
        <cc1:MaskedEditValidator ID="mevDateFinished" runat="server" ControlExtender="meeDateFinished" ControlToValidate="tbDateFinished"  InvalidValueMessage="* Datum je neveljaven"
            Display="Dynamic" TooltipMessage="" EmptyValueBlurredText="" InvalidValueBlurredMessage=""  ValidationGroup="payment" />   
        <asp:RequiredFieldValidator ValidationGroup="ordreditFIN" ControlToValidate="tbDateFinished"  ID="rfvDateFinished" runat="server" ErrorMessage="* Izberi datum "></asp:RequiredFieldValidator>
        <br />
    </asp:PlaceHolder>

    <asp:PlaceHolder ID="phSetPayedBack" runat="server">
    
        <asp:Button OnClick="btSetPayedBack_Click" ID="btSetPayedBack" runat="server" Text="<< Nazaj - ni še odpremljeno" />

    </asp:PlaceHolder>
    
    <asp:PlaceHolder ID="phCancel" runat="server" >
    
        <asp:Button ValidationGroup="none" OnClick="btCancelOrder_Click" OnClientClick="return confirm('Ste res prepričani, da želite PREKLICATI naročilo??? Sprememba nazaj ni možna.');" ID="btCancelOrder" runat="server" Text="<< PREKLIČI NAROČILO " />

    </asp:PlaceHolder>    
    

    <br /><br />

    <h3>Naročnik</h3>
    <%= SHOPPING_ORDER.Common.RenderShoppingOrderUserData(Order)  %>
    <br />
     
    <h3>Način plačila</h3>
    <%= SHOPPING_ORDER.Common.RenderSHOPPING_ORDERPayment(Order)  %>

    <h3>Način dostave</h3>
    <%= SHOPPING_ORDER.Common.RenderShoppingOrderShippingDetail (Order)  %>

    
    
<%--    <% if (Order.IsCompany) {%>
        <h3>Podatki za račun (podjetje)</h3>
        <%= SHOPPING_ORDER.Common.RenderShoppingOrderCompanyDetail(Order)%>
        <br />
    <%} %>--%>


<h3>Vsebina naročila</h3>
<SM:OrderDetail ID="objOrderDetail" runat="server" />


    
</div>

