﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_cmsUserList.ascx.cs" Inherits="SM.UI.Controls._ctrl_membership_cmsUserList" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Namespace="SM.UI.Controls" TagPrefix="SM" %>


<div class="qContList">

         <SM:smListView ID="lvList" runat="server"   InsertItemPosition="None" 
            onitemcommand="lvList_ItemCommand"  >
            <LayoutTemplate>
                <table  class="tblData" cellpadding="0" cellspacing="0">
                        <thead>
                            <tr id="headerSort" runat="server">
                                <th ><asp:LinkButton ID="LinkButton4" CommandName="sort" CommandArgument="Username" runat="server">Username</asp:LinkButton></th>
                                <th ><asp:LinkButton ID="LinkButton10" CommandName="sort" CommandArgument="Email" runat="server">Email</asp:LinkButton></th>
                                <th ><asp:LinkButton ID="LinkButton11" CommandName="sort" CommandArgument="ACTIVE_WWW" runat="server">AKT.</asp:LinkButton></th>
                                <th width="100px" colspan="2" runat="server"  id="headManage" >Uredi</th>
                            </tr>
                        </thead>
                        <tbody id="itemPlaceholder" runat="server"></tbody>
                        <tfoot>
                            <tr>
                                <th style="text-align:right" colspan="4" id="footTH" runat="server"  >
                                    <asp:DataPager runat="server" ID="DataPager" PageSize="230"  >
                                        <Fields>
                                            <asp:NextPreviousPagerField  />
                                            <asp:NumericPagerField ButtonCount="5" />
                                        </Fields>
                                    </asp:DataPager>
                                </th>
                            </tr>
                        </tfoot>
                    </table>
            </LayoutTemplate>
            
            <ItemTemplate>
                <tr  class='<%#  Container.DataItemIndex % 2 == 0 ? "  " : " odd " %>'  onmouseover ="this.className='<%#  Container.DataItemIndex % 2 == 0 ? "hovRow" : "hovRowodd" %>'" onmouseout ="this.className = GetCssClass(<%#  Container.DataItemIndex %>)">
                    <td>
                        <%# Eval("Username")%>
                    </td>
                    <td>
                        <%# Eval("Email")%>
                    </td>

<%--                    <td>
                        <asp:CheckBox ID="CheckBox1" Checked='<%# Eval("PAYED")%>' Enabled = "false" runat="server" />
                    </td>
                    <td>
                        <asp:CheckBox ID="CheckBox2" Checked='<%# Eval("ACTIVE_WWW")%>' Enabled = "false" runat="server" />
                    </td>--%>
                    <td width="50px" runat="server"  visible='<%# ShowManageColumn && ShowEditColumn %>'>
                        <a target='<%# OnEditTarget %>' href='<%# Page.ResolveUrl( SM.EM.BLL.Membership.Common.RenderCMSHref(new Guid(Eval("ProviderUserKey").ToString()))) %>' title="edit user" >uredi</a>
                    </td>
                    <td width="50px" runat="server"  visible='<%# ShowManageColumn && IsAdmin %>'>
                        <asp:LinkButton ID="lbLogin" CommandArgument='<%# Eval("USERNAME") %>' CommandName="login" runat="server">login</asp:LinkButton>
                    </td>                
                </tr>  
            
            </ItemTemplate>
            
            <EmptyDataTemplate>
                
                <div class="odd">
                    Seznam je prazen.
            
                </div>
                
            </EmptyDataTemplate>
            
        
        </SM:smListView>

</div>
