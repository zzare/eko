﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;


namespace SM.UI.Controls
{
    public partial class _cmsTicketList : BaseControl 
    {
        public delegate void OnEditEventHandler(object sender, SM.BLL.Common.Args .IDguidEventArgs e);
        public event OnEditEventHandler OnEdit;
        public event OnEditEventHandler OnDelete;
        public string OnEditTarget = "_self";
        public int PageSize = 20;

        public string SearchText { get; set; }
        public bool? Active { get; set; }
        public short? Status {get; set;}
        public bool IsAdmin { get; set; }

        
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        protected void ldsList_OnSelecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            eMenikDataContext db = new eMenikDataContext();

            
            var  list = from l in db.TICKETs where l.Active == true  select l;

                // apply filters
                //if (Active == true)
                //    list = list.Where(w => w.OrderStatus >= ORDER.Common.Status.CONFIRMED );
                //else if (Active == false)
                //    list = list.Where(w => w.OrderStatus < ORDER.Common.Status.CONFIRMED );

            if (Status != null)
                    list = list.Where(w => w.StatusID == Status );


            if (!String.IsNullOrEmpty(SearchText))
                list = list.Where(w => w.Title.Contains(SearchText ) || w.Desc.Contains(SearchText ) ||  (w.TicketRefID .ToString().Contains(SearchText) || (w.EM_USER.USERNAME ).Contains(SearchText) ));


            // if search is not set, show only root tickets
            if (String.IsNullOrEmpty(SearchText))
                list = list.Where(w => w.ParentTicketID == null );




            // result
            e.Result = list.OrderByDescending(o => o.ParentTicketID ).OrderByDescending (o=>o.DateAdded);
        }


        public void BindData()
        {
            lvList.DataBind();


        }


        protected string RenderStatus(object item) {
            TICKET tic = (TICKET )item;
            if (tic == null)
                return "";

            string ret = "";
            ret = TICKET.Common.RenderTicketStatus(tic.StatusID);
            return ret;
        }

        protected void lvList_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                // load sub tickets
                ListView lvListSub = (ListView)e.Item.FindControl("lvListSub");
                if (lvListSub == null) return;
                ListViewDataItem item = (ListViewDataItem)e.Item;

                TICKET t = (TICKET)item.DataItem;

                lvListSub.DataSource = TICKET.GetTicketsByParent(t.TicketID);
                lvListSub.DataBind();
            }
        }
}


}