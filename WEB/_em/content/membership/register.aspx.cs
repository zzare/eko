﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class _em_content_membership_register : SM.EM.UI.BasePage_EMENIK
{

    protected override void OnPreInit(EventArgs e)
    {
        if (PageName == "")
            PageName = "eko";


        base.OnPreInit(e);
        //this.PageModule = -11;
    }

    protected override void OnInit(EventArgs e)
    {
        CurrentSitemapID = -1;
        base.OnInit(e);

    }

    protected void Page_Load(object sender, EventArgs e)
    {

        // if confirmation
        if (Request.QueryString["ac"] != null)
        {
            if (Request.QueryString["ac"] == "confirm")
            {
                SetViewConfirmation();
                return;
            }
        }
        
        
        // if logged in
        if (User.Identity.IsAuthenticated) {
            SetViewLogin();
            return;
        }




    }

    protected void SetViewLogin()
    {
        phRegister.Visible = false;
        phConfirmation.Visible = false;
        phLogin.Visible = true;
    }
    protected void SetViewRegister()
    {
        phRegister.Visible = true;
        phConfirmation.Visible = false;
        phLogin.Visible = false;
    }
    protected void SetViewConfirmation()
    {
        phRegister.Visible = false;
        phConfirmation.Visible = true;
        phLogin.Visible = false;
    }



    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        this.Title = Resources.Modules.RegisterTitle  + " | " + TitleRoot;
    }


}
