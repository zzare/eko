﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Net.Mail;
using System.Text;
using System.IO;

public partial class _ctrl_main_page_cntRegisterShort : BaseControl 
{
    public bool AutoLogin = true ;
    public bool SendMail = true;

    public event  EventHandler OnRegistrationComplete;

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack) { 
            // disable clientside validation if autenticated
            if (Page.User.Identity.IsAuthenticated)
            {
                valEmailPattern.EnableClientScript = false;
                valRequireEmail.EnableClientScript = false;
                valRequirePassword.EnableClientScript = false;
                valRequireUserName.EnableClientScript = false;
                revUsername.EnableClientScript = false;
                valPasswordLength.EnableClientScript = false;
            
            
            }
        
        
        }
        
    }



    protected bool RegisterUser() {


        // validate input
        Page.Validate("regShort");
        if (!Page.IsValid)
            return false;

        // insert membership user
        MembershipCreateStatus status;
        MembershipUser usr = Membership.CreateUser(UserName.Text, Password.Text, Email.Text, "q", "a", true, out status);

        // user is successfully created
        if (status == MembershipCreateStatus.Success)
        {
            // check for bonus
            Guid bonus = Guid.Empty;
            //if (this.Profile.Emenik.BonusID != null && this.Profile.Emenik.BonusID != Guid.Empty)
            //{
            //    bonus = this.Profile.Emenik.BonusID;
            //    this.Profile.Save();
            //}


            // insert emenik user
            EM_USER emu = EM_USER.CreateNewUser(new Guid(usr.ProviderUserKey.ToString()), usr.UserName, usr.Email, bonus);

            // send confirmation mail
            SendConfirmationMail(Password.Text, emu);

            return true;

        }
        else
        {
            switch (status)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    cvUsername.IsValid = false;
                    break;
                case MembershipCreateStatus.DuplicateEmail:
                    Response.Write("Duplicate Email");
                    break;
                case MembershipCreateStatus.InvalidUserName:
                    Response.Write("Uporabniško ime je neveljavno");
                    break;
            }


        }

        return false;
    
    
    }

    protected void btRegister_Click(object sender, EventArgs e)
    {

        if (RegisterUser() && AutoLogin )
            // login new user
            SM.EM.Security.Login.LogIn(UserName.Text, false, "");


        OnRegistrationComplete(this, new EventArgs());
    }


    protected void btOrder_Click(object sender, EventArgs e)
    {
        //if (RegisterUser())
        //{
        //    //create default values
        //    EM_USER em_user = EM_USER.GetUserByUserName(UserName.Text.Trim());
        //    if (em_user == null)
        //        return;
        //    EM_USER.InsertDefaultUserData(em_user);

        //    string url = "";
        //    url = ORDER.Common.GetOrderHref(ORDER.Common.ModuleItems.NAJEM.ToString() + "," + ORDER.Common.ModuleItems.VNOS_SIMPLE.ToString(), "");


        //    // login and redirect to orders
        //    if (AutoLogin)
        //        SM.EM.Security.Login.LogIn(UserName.Text, false, SM.BLL.Common.Emenik.ResolveMainDomainUrl(url));
        //}
        //else
        //{ // if username was not entered and user is autenticated, redirect to orders
        //    if (Page.User.Identity.IsAuthenticated && !Page.IsValid && string.IsNullOrEmpty(UserName.Text.Trim()))
        //    {
        //        string url = "";
        //        url = ORDER.Common.GetOrderHref(ORDER.Common.ModuleItems.NAJEM.ToString() + "," + ORDER.Common.ModuleItems.VNOS_SIMPLE.ToString(), "");

        //        Response.Redirect(SM.BLL.Common.Emenik.ResolveMainDomainUrl(url));         
            
        //    }
                
        
        
        //}

    }


    protected void ValidateDuplicate(object o, ServerValidateEventArgs args)
    {
        args.IsValid = true;

        // check for duplicate page name
        if (EM_USER.ExistPageName(UserName.Text.Trim() ))
        {
            args.IsValid = false;
        
        }
        
    }

    protected void ValidateUserName(object o, ServerValidateEventArgs args)
    {
        args.IsValid = true;

        // check for duplicate page name
        if (UserName.Text.Contains(SM.BLL.Common.Emenik.Data.PortalHostDomain() ))
        {

            args.IsValid = false;
            CustomValidator val = o as CustomValidator;
            val.ErrorMessage  = "Uporabniško ime ne sme vsebovati domene " + SM.BLL.Common.Emenik.Data.PortalHostDomain();
        }
    }

    protected void ValidateEmail(object o, ServerValidateEventArgs args)
    {
        args.IsValid = true;

        // check for duplicate page name
        if ( !SM.EM.Helpers.IsValidEmail(Email.Text, true ))
        {
            args.IsValid = false;
        
        }
    }

    protected void ValidateAccept(object o, ServerValidateEventArgs args)
    {
        args.IsValid = true;

        // check for accept
        if (!cbAccept.Checked )
        {
            args.IsValid = false;

        }
    }

    protected void SendConfirmationMail(string password, EM_USER usr)
    {
        if (!SendMail) return;

        if (usr == null) return;


        StringBuilder sb = new StringBuilder();
        HtmlTextWriter writer = new HtmlTextWriter(new StringWriter(sb));

        objMailConfirmation.UserName = usr.USERNAME ;
        objMailConfirmation.Password = password;
        objMailConfirmation.PageHref = "http://www." + SM.BLL.Common.Emenik.Data.PortalHostDomain();
        objMailConfirmation.Visible = true;
        objMailConfirmation.RenderControl(writer);
        objMailConfirmation.Visible = false;

        // send mail
//        MailMessage message = SM.EM.Mail.Template.GetNewUserConfirmation(password, usr);
        MailMessage message = new MailMessage();
        message.From = new MailAddress(SM.EM.Mail.Account.Info.Email, SM.BLL.Common.Emenik.Data.PortalName());
        message.To.Clear();
        message.To.Add(new MailAddress(usr.EMAIL ));
        message.IsBodyHtml = true;
        message.Body = sb.ToString();
        message.Subject = SM.BLL.Common.Emenik.Data.PortalName() + " - uporabniški podatki";


        SmtpClient client = new SmtpClient();
        try
        {
            client.Credentials = new System.Net.NetworkCredential(SM.EM.Mail.Account.Info.Email, SM.EM.Mail.Account.Info.Pass);
            client.Port = 587;//or use 587            
            client.Host = "smtp.gmail.com";
            client.EnableSsl = true;

            client.Send(message);
        }

        catch (Exception ex)
        {
        }


    }

}
