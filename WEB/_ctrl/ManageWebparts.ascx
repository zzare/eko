<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ManageWebparts.ascx.cs" Inherits="SM.EM.UI.Controls.ManageWebparts" %>
<asp:EditorZone ID="EditorZone1" runat="server">
<ZoneTemplate>

    <asp:AppearanceEditorPart ID="aep" runat="server">
    </asp:AppearanceEditorPart>
    
    <asp:BehaviorEditorPart ID="bep" runat="server">
    </asp:BehaviorEditorPart>
    
    <asp:LayoutEditorPart ID="lep" runat="server">
    </asp:LayoutEditorPart>
    
    <asp:PropertyGridEditorPart id="pgep" runat="server">
    </asp:PropertyGridEditorPart>
</ZoneTemplate>
</asp:EditorZone>
