﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_menuSort.ascx.cs" Inherits="SM.UI.Controls._menuSort" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<style type="text/css">

	
	
	.sortCont li{
/*height:40px;*/
/*width:200px;*/
list-style-type:none;
}

#masterList li {list-style-type:none;}
.sortCont ul {padding-left:1em;margin-left:0em;}



.success_text {color:green;font-weight:bold;}
.error_text {color:red;font-weight:bold;}

.containerTitle {font-weight:bold;font-size:105%;}
.unmovable {background-color:Red; font-size:2px !important;padding:2px !important;margin:0px;}
/*.unmovable {list-style-type:none !important;font-size:1px !important;padding:0px !important;margin:0px !important;}*/


	
</style>
<script type="text/javascript">

    $(document).ready(function() {
        $("ul.sortCont").sortable({
            cursor: "move",
            //        appendTo:"body",
            //        handle: 'li.sortItem',
            cancel: "li.unmovable",
            revert: true,
            items: "li",
            opacity: 0.7,
            axis: "y",
            //tolerance: "intersect",
            //connectWith: [".sortCont ul"],
            placeholder: "cwp_placeholder",
            forcePlaceholderSize: true,
            //        dropOnEmpty: true,
            zIndex: 1000000,
            update: function(evn, ui) {
                var index = $(ui.item).parent().children(".sortItem").index(ui.item);
                var prnt = $(ui.item).parent()[0].id;
                //$(evn.target).parent().children().index(evn.target);

                if (index >= 0) {

                    //alert('id:: ' + ui.item[0].id + ' ; pos :: ' + index + ' ; parent:: ' + prnt);
                    //                WebServiceCWP.MoveMenu(ui.item[0].id, index, prnt, Refresh);
                    WebServiceCWP.MoveMenuParent(ui.item[0].id, index, prnt);
                     
                }
            }
        });

    });
</script>




<%--<div class="sortCont" id="divSortM" runat="server" >

</div>--%>

<ul class="sortCont" id='p<%= ParentID.ToString()  %>' >
    <li class="unmovable"/>
    <%= RenderItems()%>

</ul>







