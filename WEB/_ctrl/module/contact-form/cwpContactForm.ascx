﻿<%@ Control Language="C#" AutoEventWireup="true"  CodeFile="cwpContactForm.ascx.cs" Inherits="SM.EM.UI.Controls._ctrl_module_content_cwpContent" %>
<%@ Register TagPrefix="SM" TagName="ContactForm" Src="~/_ctrl/module/contact-form/ContactForm.ascx"   %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="FredCK.FCKeditorV2" Namespace="FredCK.FCKeditorV2" TagPrefix="FCKeditorV2" %>

<div id='<%= cwp.CWP_ID.ToString() %>' class='<%= "cwp" + (ParentPage.IsModeCMS ? " cwpEdit " : " ")  + CMS_WEB_PART.Common.RenderHiddenClass(cwp) %>'>
    <div id="divEdit" runat="server" class="cwpHeader cwpMove" visible="false" >                   
                    
        <div class="cwpHeaderRight"> &nbsp;</div>

        <div style="float:right; ">
<% if (PERMISSION.User.Common.CanShowHideModule()){%>            
            <a id="btShow"  title="prikaži" runat="server" class="btShow"></a>
            <a id="btHide" title="skrij" runat="server" class="btHide"></a>
<%} %>
<% if (PERMISSION.User.Common.CanDeleteModule()){%>
            <a id="btDelete" href="#" title="izbriši" class="btDelete" onclick="deleteEL('<%= cwp.CWP_ID.ToString() %>'); return false" ></a>
<%} %>
<% if (PERMISSION.User.Common.CanEditModuleSettings ()){%>
            <a id="brSettings" title="nastavitve" runat="server" class="btSettings"  onserverclick="btSettings_Click"></a>
<%} %>
        </div>
        <div class="cwpHeaderLeft"> &nbsp;</div>
        <a id="btMove" class="btMove"></a>
        
        <div class="cwpHeaderTitle">
            Kontakt
            <a href="javascript:void(0)" class="btsettStatusHelp btcwpHeadHelp"><img src='<%=Page.ResolveUrl("~/_inc/images/icon/help.png") %>' /></a>
            <div class="settStatusWrapp cwpHeadHelpWrapp" >
                <div class="settStatus cwpHeadHelp">
                    Kontaktni obrazec
                    <br />
                    Element omogoča obiskovalcem tvoje spletne strani da oddajo različna povpraševanja glede tvoje ponudbe. Povpraševanje se samodejno pošlje na podan e-poštni naslov.                
                </div>
            </div>
        </div>
    </div>
    <div class="clearing"></div>

    <div class='<%= "cContF" + (  ParentPage.IsModeCMS ? "cwpContainer" : "" ) %>'>

        <div id="divTitleContent"  class="cwpH1" runat="server">
            <h1><asp:Literal ID="litTitle" runat="server" ></asp:Literal></h1>
        </div>    
        <div class="contContact">
            <SM:ContactForm ID="objContactForm" runat="server" Visible ="true" />
        </div>
    </div>

</div>