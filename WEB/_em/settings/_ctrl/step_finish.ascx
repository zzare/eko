﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="step_finish.ascx.cs" Inherits="_em_settings__ctrl_step_finish" %>

<h1>Čestitamo</h1>

<h2>Uspešno ste prišli čez nastavitve svoje spletne strani. Vse te nastavitve lahko kadarkoli urejate v meniju NASTAVITVE na enak način.</h2>

<h2>Kaj sedaj?</h2>
<p>Sedaj lahko pričnete z vnašanjem vsebine.</p>

<br />


    <div class="firststepLeftInner">
        <h2>Kako do urejanja vsebine?</h2>
        <img style="float:left;" src='<%=Page.ResolveUrl("~/_inc/images/design/1dva3/1dva3_startcontent.png") %>' />
        <p>
            <br />
            V statusni vrstici na vrhu kliknete UREDI VSEBINO.
            <br />
            ali
            <br />
            Kliknete na katerega od MENIJEV.
            <br />
            ali
            <br />
            Kliknete na spodnji gumb Začni z urejanjem vsebin.
        </p>
        <div class="clearing"></div>
        <br />
        <a class="BTN_firststepregister" style="float:left;" href='<%= Page.ResolveUrl(SM.EM.Rewrite.EmenikUserUrl(ParentPageEmenik.PageName)) %>' >Začni z urejanjem vsebin</a>
        <%--<asp:LinkButton CssClass="BTN_firststepregister" style="float:left;" ID="btSkipWizard" runat="server" >Začni z urejanjem vsebin</asp:LinkButton>--%>
        <div class="clearing"></div>
    </div>

    