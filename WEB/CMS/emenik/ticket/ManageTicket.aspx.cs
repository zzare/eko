﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
namespace SM.UI.CMS
{
    public partial class CMS_Intranet_ManageTicket: SM.EM.UI.CMS.BasePage_CMS 
    {

        protected Guid TicketID
        {
            get
            {
                if (Request.QueryString["t"] == null)
                    return Guid.Empty;
                if (!SM.EM.Helpers.IsGUID(Request.QueryString["t"]))
                    return Guid.Empty;

                return new Guid(Request.QueryString["t"]);
            }
        }

        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);
            CurrentSitemapID = 919;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            // event handlers
            objEdit.OnUserChanged += new EventHandler(objEdit_OnChanged);
            objEdit.OnCancel += new EventHandler(objEdit_OnCancel);
            objFilter.OnFilterChanged += new EventHandler(objFilter_OnFilterChanged);
            objList.OnDelete += new SM.UI.Controls._cmsTicketList.OnEditEventHandler(objList_OnDelete);
            objList.OnEdit += new SM.UI.Controls._cmsTicketList.OnEditEventHandler(objList_OnEdit);
            
            // init

            ApplyFilters();

            if (!IsPostBack)
            {
                // if Order is set as QS, show edit Order
                if (TicketID != Guid.Empty)
                {
                    LoadData();

                }

                    else // normal load
                {
                    HideTabs();
                    cpeQList.Collapsed = false;
                    objList .BindData();
                }

            }
        }


        protected void LoadData() {

            objEdit.TicketID = TicketID ;
            objEdit.BindData();
            cpeQList.Collapsed = true;
            cpeQList.ClientState = "true";

            objList.BindData();



            ShowTabs();
        }

        void objEdit_OnCancel(object sender, EventArgs e)
        {
            HideTabs();

            cpeQList.Collapsed = false;
            cpeQList.ClientState = "false";
            
        }


        protected void ApplyFilters() {
            // list
            objList.SearchText = objFilter.SearchText;
//            objList.Active = objFilter.Active;
            objList.Status = objFilter.Status;
            //objOrderList.RoleID = objFilter.Role;

            //objOrderEdit.ProjectID = ProjectID;
            //objOrderEdit.SiteID = SiteID;


        
        }
        // filter
        void objFilter_OnFilterChanged(object sender, EventArgs e)
        {
            // filters
            ApplyFilters();

            // refresh question lists
            objList.BindData();



        }
        //edit
        void objEdit_OnChanged(object sender, EventArgs e)
        {
            // refresh  lists
            ApplyFilters();

            // refresh  lists
            objList.BindData();

            // refresh edit
            objEdit.BindData();


        }

        //list
        void objList_OnEdit(object sender, SM.BLL.Common.Args.IDguidEventArgs e)
        {
            // show tabs
            ShowTabs();


            // apply filters
            ApplyFilters();



        }
        void objList_OnDelete(object sender, SM.BLL.Common.Args.IDguidEventArgs e)
        {
            ApplyFilters();

            // refresh group lists
            objList.BindData();

        }




        protected void ShowTabs()
        {
            tabTicket.Visible = true;
            tabTicket.ActiveTabIndex = 0;

        }
        protected void HideTabs()
        {
            tabTicket.Visible = false;

        }


    }
}