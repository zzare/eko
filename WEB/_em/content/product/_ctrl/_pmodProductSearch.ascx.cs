﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SM.UI.Controls;
using System.Linq.Dynamic;
using LinqKit;
using SM.EM.UI.Controls;

public partial class _em_content_product__ctrl__pmodProductSearch : BaseControl_CmsWebPart
{


    public string SearchString
    {
        get
        {
            //if (Request.QueryString["srch"] == null)
            //    ErrorRedirect(151, 404);
            //if (string.IsNullOrEmpty( Request.QueryString["srch"].Trim()))
            //    ErrorRedirect(151, 404);


            //return Microsoft.Security.Application.AntiXss.UrlDe(Request.QueryString["srch"]);
            return Server.UrlDecode(Request.QueryString["srch"]);
        }
    }



    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);


    }

    protected void Page_Load(object sender, EventArgs e)
    {

        
        ProductRep prep = new ProductRep();
        //        var list = objList.Prep.GetProductDescListSearch(SearchString, LANGUAGE.GetCurrentLang(), true, em_user.UserId);
        IQueryable<vw_Product> list = null;

        bool? active = true;
        if (ParentPage . IsModeCMS)
            active = null;

        if (!string.IsNullOrEmpty(SearchString))
            list = objList.Prep.GetProductDescListSearch(SearchString, LANGUAGE.GetCurrentLang(), active, ParentPage. em_user.UserId);

        objList.ProductList = list;
        //        objList.PageSize = 40;
        objList.EnableSorting = false;
        objList.EnableFilters = false;
        objList.EnableNew = false;
        //        objList.StartingQueryString = "srch=" +  Request.QueryString["srch"];


    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        if (string.IsNullOrEmpty(SearchString))
            objList.Visible = false;

    }

    protected void btSearch_Click(object o, EventArgs e)
    {
        string srch = tbSearch.Text.Trim();




        if (!string.IsNullOrEmpty(srch))
        {
            //Response.Redirect(Page.ResolveUrl(SM.EM.Rewrite.SearchProductEmenikUrl(em_user.UserId, tbSearch.Text)));

            srch = Microsoft.Security.Application.AntiXss.UrlEncode(srch);
            Response.Redirect(Page.ResolveUrl(Request.Url.AbsolutePath) + "?srch=" + srch);
        }


    }


}
