﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
namespace SM.UI.CMS
{
    public partial class ManageCampaign : SM.EM.UI.CMS.BasePage_CMS 
    {
        protected Guid UserID
        {
            get
            {
                if (Request.QueryString["u"] == null)
                    return Guid.Empty;
                if (!SM.EM.Helpers.IsGUID(Request.QueryString["u"]))
                    return Guid.Empty;

                return new Guid(Request.QueryString["u"]);
            }
        }
        protected Guid CampaignID
        {
            get
            {
                if (Request.QueryString["c"] == null)
                    return Guid.Empty;
                if (!SM.EM.Helpers.IsGUID(Request.QueryString["c"]))
                    return Guid.Empty;

                return new Guid(Request.QueryString["c"]);
            }
        }

        protected bool IsAdmin { get {

            return true; // todo: remove
            if (User.Identity.IsAuthenticated)
                if (User.IsInRole("admin"))
                    return true;
            return false;
            }
        }

        protected override void OnInit(EventArgs e)
        {
            CurrentSitemapID = 775;

            base.OnInit(e);

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            // event handlers
            objEdit.OnChanged += new EventHandler(objUserEdit_OnUserChanged);
            objEdit.OnCancel += new EventHandler(objUserEdit_OnCancel);
            objEdit.OnRefresh += new EventHandler(objEdit_OnRefresh);
            objFilter.OnFilterChanged += new EventHandler(objFilter_OnFilterChanged);
            objList.OnDelete += new SM.UI.Controls._cmsCampaigntList.OnEditEventHandler(objUserList_OnDelete);
            objList.OnEdit += new SM.UI.Controls._cmsCampaigntList.OnEditEventHandler(objUserList_OnEdit);
            
            // init
//            objUserList.IsAdmin = IsAdmin;

            ApplyFilters();

            if (!IsPostBack)
            {
                // if user is set as QS, show edit user
                if (CampaignID  != Guid.Empty)
                {
                    LoadData();

                }
                    //// if project is set, insert new site
                    //else if (ProjectID != Guid.Empty)
                    //{

                    //    objUserEdit.UID = Guid.Empty;
                    //    objUserEdit.BindData();
                    //    cpeQList.Collapsed = true;
                    //    objUserList .BindData();

                    //    ShowTabs();

                    //    // hide some tabs

                    //}
                    else // normal load
                {
                    HideTabs();
                    cpeQList.Collapsed = false;
                    objList .BindData();
                }

            }
        }

        void objEdit_OnRefresh(object sender, EventArgs e)
        {
            
        }


        protected void LoadData() {

            objEdit.CampaignID = CampaignID;
            objEdit.BindData();
            cpeQList.Collapsed = true;
            cpeQList.ClientState = "true";
            objList.BindData();


            ShowTabs();
        }

        void objUserEdit_OnCancel(object sender, EventArgs e)
        {
            HideTabs();

            cpeQList.Collapsed = false;
            cpeQList.ClientState = "false";
            
        }


        protected void ApplyFilters() {
            // list
            objList.SearchText = objFilter.SearchText;
            //objList.Active = objFilter.Active;
            //objUserList.RoleID = objFilter.Role;


            // item
            //objProjectEdit.CategoryID = objProjectFilter.CategoryID;
        
        }
        // filter
        void objFilter_OnFilterChanged(object sender, EventArgs e)
        {
            // filters
            ApplyFilters();

            // refresh question lists
            objList.BindData();

            
        }
        //edit
        void objUserEdit_OnUserChanged(object sender, EventArgs e)
        {
            // refresh user lists
            ApplyFilters();

            // refresh user lists
            objList.BindData();

            // refresh edit
            objEdit.BindData();


        }

        //list
        void objUserList_OnEdit(object sender, SM.BLL.Common.Args.IDguidEventArgs e)
        {
            // show tabs
            ShowTabs();


            // apply filters
            ApplyFilters();


            // load selected question
            objEdit.CampaignID = e.ID;
            objEdit.BindData();

            //objSiteEdit.SID = e.ClickedID;
            //objSiteEdit.BindData();

        }
        void objUserList_OnDelete(object sender, SM.BLL.Common.Args.IDguidEventArgs e)
        {
            ApplyFilters();

            // refresh group lists
            objList.BindData();

        }

        protected void btNew_Click(object o, EventArgs e) {
            objEdit.CampaignID = Guid.Empty;
            objEdit.BindData();
            ShowTabs();
        }



        protected void ShowTabs()
        {
            //tabCampaign.Visible = true;
            //tabCampaign.ActiveTabIndex = 0;
            objEdit.Visible = true;
            

        }
        protected void HideTabs()
        {
            objEdit.Visible = false;

            //tabCampaign.Visible = false;

        }


    }
}