﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

/// <summary>
/// Summary description for COMMENT
/// </summary>
public partial class COMMENT
{
    public static COMMENT GetCommentByID(int id)
    {
        eMenikDataContext db = new eMenikDataContext();

        return  db.COMMENTs.Where(w => w.CommentRefID  == id).SingleOrDefault();
    }

    public static IEnumerable<COMMENT> GetComments(int refInt, bool? active)
    {
        eMenikDataContext db = new eMenikDataContext();

        IEnumerable<COMMENT> list =  db.COMMENTs.Where(w => w.RefInt == refInt ).OrderBy (o=>o.DateAdded );

        if (active != null)
            list = list.Where(w=>w.Active == active);

        return list;
    }
    public static IEnumerable<COMMENT> GetCommentsByApproved(int refInt, bool? active, bool isEditMode)
    {
        eMenikDataContext db = new eMenikDataContext();

        IEnumerable<COMMENT> list = db.COMMENTs.Where(w => w.RefInt == refInt).OrderBy(o => o.DateAdded);

        if (!isEditMode )
            list = list.Where(w => w.Approved == true);

        if (active != null)
            list = list.Where(w => w.Active == active);

        return list;
    }
    public static int GetCommentsCount(int refInt)
    {
        eMenikDataContext db = new eMenikDataContext();

        var list = db.COMMENTs.Where(w => w.RefInt == refInt);
        list = list.Where(w => w.Active == true);

        return list.Count();
    }



    public static COMMENT AddComment(int refInt, Guid userid, string name, string www, string email, string title, string comment, bool isAuthor)
    {
        return     AddComment(Guid.Empty, refInt, userid, name, www, email, title, comment, isAuthor);
    }


    public static COMMENT AddComment(Guid  refGuid, int refInt, Guid userid, string name, string www, string email, string title, string comment, bool isAuthor) {
        eMenikDataContext db = new eMenikDataContext();

        COMMENT c = new COMMENT();
        c.CommentID = Guid.NewGuid();
        c.RefGuid = refGuid;
        c.RefInt = refInt;
        c.UserId = userid;
        c.Title = title;
        c.Comment = comment;
        c.UserName = name;
        c.WWW = www;
        c.Email = email;
        c.IsAuthor = isAuthor;

        c.DateAdded = DateTime.Now;
        c.Active = true;
        c.Approved = false;

        db.COMMENTs.InsertOnSubmit(c);
        db.SubmitChanges();

        return c;
    }



    public static void ApproveCommentByID(int id, bool approved)
    {
        eMenikDataContext db = new eMenikDataContext();

        COMMENT c = db.COMMENTs.Where(w => w.CommentRefID == id).SingleOrDefault();
        c.Approved = approved;
        db.SubmitChanges();
    } 


    public static void  DeleteCommentByID(int id)
    {
        eMenikDataContext db = new eMenikDataContext();

        COMMENT c =  db.COMMENTs.Where(w => w.CommentRefID == id).SingleOrDefault();
        db.COMMENTs.DeleteOnSubmit(c);
        db.SubmitChanges();
    }    



    public class Common { 
    
    
    
    }

    //partial void OnValidate(System.Data.Linq.ChangeAction action)
    //{
    //}

}
