﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="cmsArticleEdit.ascx.cs" Inherits="SM.EM.UI.Controls.cmsArticleEdit" %>
<%@ Register Assembly="FredCK.FCKeditorV2" Namespace="FredCK.FCKeditorV2" TagPrefix="FCKeditorV2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:HiddenField ID="hfEdit" runat="server" />

        
<asp:Panel CssClass="modPopup" ID="panEdit" runat="server" style="display:none;">
    <div class="modPopup_header modPopup_headerAdd">
        <h4>DODAJANJE/UREJANJE DINAMIČNIH VSEBIN</h4>
        <p>Uredi novico/članek</p>
    </div>
        <div class="modPopup_mainArticle">
            <table cellpadding="0" cellspacing="0" width="100%" class="tblData">
                <tr>
                    <td colspan="7">
                        <div class="artEdit">
                            <span class="modPopup_mainTitle" ><asp:Label ID="lbTitle" runat="server" Text="Naslov vsebine" ></asp:Label></span>
                            <asp:TextBox ID="tbTitle" class="modPopup_TBXsettings" runat="server" Text = '<%#Bind("ART_TITLE") %>' Width="99%"></asp:TextBox>
                            <cc1:TextBoxWatermarkExtender ID="tbwexUserName" WatermarkCssClass="modPopup_mainWatermark" runat="server"  WatermarkText="Tukaj vpiši naslov." TargetControlID ="tbTitle"></cc1:TextBoxWatermarkExtender>
                            
                            <asp:RequiredFieldValidator ControlToValidate="tbTitle" ID="reqTitle" runat="server" ErrorMessage="* Vpiši naslov"  Display="Dynamic"  ValidationGroup="editArt"></asp:RequiredFieldValidator>
                            
<%--                            <cc1:ValidatorCalloutExtender ID="vceTitle" runat="server" TargetControlID="reqTitle">
                            </cc1:ValidatorCalloutExtender>--%>
                        </div>
                        <span class="modPopup_mainTitle" ><asp:Label ID="lbTimeline" runat="server" Text="Vidnost vsebine" ></asp:Label></span>
                        <div class="floatL">
                            <asp:Label ID="lbRelDate" runat="server" Text="OD:" CssClass="artEdit" ></asp:Label>
                            <asp:TextBox ID="tbReleaseDate" class="modPopup_TBXsettings modPopup_TBXdate" runat="server" Text = '<%#Bind("RELEASE_DATE", "{0:dd.MM.yyyy}") %>'></asp:TextBox>
                            <asp:ImageButton ID="btReleaseDate" ImageUrl="~/CMS/_res/images/button/calendar.png" runat="server" />&nbsp
                            <cc1:CalendarExtender ID="ceReleaseDate" runat="server" TargetControlID="tbReleaseDate" PopupPosition="BottomLeft" PopupButtonID ="btReleaseDate">
                            </cc1:CalendarExtender>                            
                            <cc1:MaskedEditExtender ID="meeReleaseDate" runat="server" TargetControlID="tbReleaseDate" Mask="99/99/9999" MessageValidatorTip="true" 
                                    MaskType="Date" DisplayMoney="Left" AcceptNegative="Left" ErrorTooltipEnabled="True"  CultureName="sl-SI" />                            
                            <cc1:MaskedEditValidator ID="mevReleaseDate" runat="server" ControlExtender="meeReleaseDate" ControlToValidate="tbReleaseDate"  InvalidValueMessage="* Datum je neveljaven"
                                Display="Dynamic" TooltipMessage="" EmptyValueBlurredText="" InvalidValueBlurredMessage=""  ValidationGroup="editArt" />
                            &nbsp;&nbsp;
                            <asp:Label ID="lbExpDate" runat="server" Text="DO:"  CssClass="artEdit"></asp:Label>
                            <asp:TextBox ID="tbExpireDate" class="modPopup_TBXsettings modPopup_TBXdate" runat="server" Text = '<%#Bind("EXPIRE_DATE", "{0:dd.MM.yyyy}") %>'></asp:TextBox>
                            <asp:ImageButton ID="btExpireDate" ImageUrl="~/CMS/_res/images/button/calendar.png" runat="server" />
                            <cc1:CalendarExtender ID="ceExpireDate" runat="server" TargetControlID="tbExpireDate" PopupPosition="BottomLeft" PopupButtonID="btExpireDate"  >
                            </cc1:CalendarExtender>
                            <cc1:MaskedEditExtender ID="meeExpireDate" runat="server" TargetControlID="tbExpireDate" Mask="99/99/9999" MessageValidatorTip="true" 
                                    MaskType="Date" DisplayMoney="Left" AcceptNegative="Left" ErrorTooltipEnabled="True"  CultureName="sl-SI" />                            
                            <cc1:MaskedEditValidator ID="mevExpireDate" runat="server" ControlExtender="meeExpireDate" ControlToValidate="tbExpireDate"  InvalidValueMessage="* Datum je neveljaven"
                                Display="Dynamic" TooltipMessage="" EmptyValueBlurredText="" InvalidValueBlurredMessage=""  ValidationGroup="editArt" />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </div>
                        <div class="floatL artEdit">

                            <asp:CheckBox ID="cbActive" CssClass="modPopup_CBXsettings" runat="server" Checked='<%#Bind("ACTIVE") %>' Text="&nbsp;&nbsp;Prikaži novico" />&nbsp&nbsp
                                <cc1:ToggleButtonExtender ID="ToggleEx" runat="server"
                                    TargetControlID="cbActive" 
                                    ImageWidth="19" 
                                    ImageHeight="19"
                                    CheckedImageAlternateText="Odkljukaj"
                                    UncheckedImageAlternateText="Obkljukaj"
                                    UncheckedImageUrl="~/_inc/images/design/checkbox_off.gif" 
                                    CheckedImageUrl="~/_inc/images/design/checkbox_on.gif"
                                    DisabledCheckedImageUrl="~/_inc/images/design/checkbox_disabled.gif"
                                    DisabledUncheckedImageUrl="~/_inc/images/design/checkbox_disabled.gif" />

        <%--                    <asp:CheckBox ID="cbApproved" runat="server" Checked='<%#Bind("APPROVED") %>' Text=" Approved" />&nbsp&nbsp
                            <asp:CheckBox ID="cbComments" runat="server" Checked='<%#Bind("COMMENTS_ENABLED") %>' Text=" Comments enabled" />&nbsp&nbsp
                            <asp:CheckBox ID="cbMembers" runat="server" Checked='<%#Bind("MEMBERS_ONLY") %>' Text=" Members only" />--%>
                        </div>
                        <div class="clearing"></div>
                    </td>
                </tr>
                <tr>
                    <th class="artEdit">
                        <span class="modPopup_mainTitle" ><asp:Label ID="lbAbstract" runat="server" Text="Kratek povzetek vsebine:"  CssClass="artEdit"></asp:Label></span>
                        <%--<asp:RequiredFieldValidator  ControlToValidate="tbAbstract" ID="reqAbstract" runat="server" ErrorMessage="* Vpiši kratko vsebino" Display="Dynamic" ValidationGroup="editArt"></asp:RequiredFieldValidator>--%>
                    </th>
                </tr>
                <tr>
                    <td colspan="7">
                        <asp:TextBox ID="tbAbstract" class="modPopup_TBXsettings"  Width="99%" TextMode="MultiLine" Rows="4" runat="server" Text='<%#Bind("ART_ABSTRACT") %>'></asp:TextBox>
                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" WatermarkCssClass="modPopup_mainWatermark"  WatermarkText="Tukaj vpiši kratek povzetek novice." TargetControlID ="tbAbstract"></cc1:TextBoxWatermarkExtender>
                    </td>
                </tr>
                <tr>
                    <th class="artEdit">

                        <span class="modPopup_mainTitle" ><asp:Label ID="lbBody" runat="server" Text="Vsebina"  CssClass="artEdit"></asp:Label></span>
                        <asp:RequiredFieldValidator EnableClientScript="false" ControlToValidate="fckBody" ID="reqBody" runat="server" ErrorMessage="* Vpiši vsebino"  Display="Dynamic" ValidationGroup="editArt"></asp:RequiredFieldValidator>

                    </th>
                </tr>
                <tr>
                    <td colspan="7">
                    
                        <FCKeditorV2:FCKeditor Visible="false" ID="fckBody" ToolbarSet="CMS_Content" runat="server" Height="300px" Width="100%" Value='<%#Bind("ART_BODY") %>'></FCKeditorV2:FCKeditor>
                        <%--<asp:TextBox ID="fckBody" Width="100%" TextMode="MultiLine" Rows="4" runat="server" Text='<%#Bind("ART_ABSTRACT") %>'></asp:TextBox>--%>
                    </td>
                </tr>
                <tr>
                    <th colspan="7">

                        <span class="modPopup_mainTitle artEdit" >Dodatno</span>
                    </th>
                </tr>
                <tr>
                    <td>
                            <asp:CheckBox ID="cbComments" CssClass="modPopup_CBXsettings" runat="server"  Text="&nbsp;&nbsp;Omogoči komentiranje" />&nbsp&nbsp
                            <cc1:ToggleButtonExtender ID="ToggleButtonExtender1" runat="server"
                                    TargetControlID="cbComments" 
                                    ImageWidth="19" 
                                    ImageHeight="19"
                                    CheckedImageAlternateText="Odkljukaj"
                                    UncheckedImageAlternateText="Obkljukaj"
                                    UncheckedImageUrl="~/_inc/images/design/checkbox_off.gif" 
                                    CheckedImageUrl="~/_inc/images/design/checkbox_on.gif"
                                    DisabledCheckedImageUrl="~/_inc/images/design/checkbox_disabled.gif"
                                    DisabledUncheckedImageUrl="~/_inc/images/design/checkbox_disabled.gif" />
                    </td>
                </tr>                
            </table>
        </div>
        <div class="modPopup_footer">
            <div class="buttonwrapperModal">
                <div class="floatR">
                    <asp:LinkButton ID="btDelete" OnClick="btDelete_Click" OnClientClick="return confirm('Ali res želite zbrisati to vsebino?')" runat="server" CssClass="lbutton lbuttonDelete"><span>Zbriši vsebino</span></asp:LinkButton>
                </div>
                <asp:LinkButton ID="btSaveEdit" runat="server" CssClass="lbutton lbuttonConfirm" OnClick="btSaveEdit_Click" ValidationGroup="editArt"><span>Shrani</span></asp:LinkButton>
                <asp:LinkButton ID="btCancelEdit" runat="server" CssClass="lbutton lbuttonDelete"><span>Prekliči</span></asp:LinkButton>
                
            </div>

            
        </div>



</asp:Panel>

           
<cc1:ModalPopupExtender RepositionMode="RepositionOnWindowResizeAndScroll"  Y ="50"  BehaviorID="mpeEditArticle"  ID="mpeEditArticle" runat="server" TargetControlID="hfEdit" PopupControlID="panEdit"
                        BackgroundCssClass="modBcg" CancelControlID="btCancelEdit" >
</cc1:ModalPopupExtender>              



