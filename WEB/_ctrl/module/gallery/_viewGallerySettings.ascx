﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewGallerySettings.ascx.cs" Inherits="_ctrl_module_poll_cntGallerySettings" %>


<script language = "javascript" type="text/javascript">
    function settingsGallerySave() {
        var t = $("#<%= tbTitle.ClientID %>").val();
        var cb = $("#<%= cbActive.ClientID %>").attr('checked');
        var ps = $("#<%= tbPageSize.ClientID %>").val();
        var tt = $("#galSetThumbType :input:radio:checked ").val();
        var _items = new Array();
        $("#galSetLangDesc div.item").each(function(i, val) {
            var _lan = '' + $(this).find('input:hidden').val();
            var _title = '' + $(this).find('input:text').val();
            var _desc = '' + $(this).find('textarea').val();
            var _item = new Object();
            _item.Lang = _lan;
            _item.Title = _title;
            _item.Desc = _desc;
            Array.add(_items, _item);
        });

        openModalLoading('Shranjujem...');
        WebServiceCWP.SaveSettingsGallery(cwpID, t, cb, ps, tt, _items, onGallerySettingsSuccessSave, onCWPSettingsErrorSave);
    }

</script>

<div class="">

    <div class="modPopup_header modPopup_headerEdit">
        <h4>NASTAVITVE ELEMENTA GALERIJA</h4>
         <p>Uredi naziv elementa. Določi aktivnost naziva elementa na spletni strani.</p>
    </div>
    <div class="modPopup_main">
        
        
        <span class="modPopup_mainTitle" >Naziv elementa anketa</span>
        <div style="float:left;">
            <input class="modPopup_TBXsettings"  id="tbTitle" maxlength="256" runat="server" type="text" />
        </div>
        <div style="float:left; padding-top:3px;">
            <label  for="<%= cbActive.ClientID %>"> <input  id="cbActive" runat="server" name ="cbActive" type="checkbox" />&nbsp;&nbsp;Prikaži naziv tega elementa na spletni strani</label>
        </div>
        

        <br />
        <div class="clearing"></div>        
        <span class="modPopup_mainTitle" >Velikost majhne slike v seznamu (thumbnail)</span>
        
        <div id="galSetThumbType">        
            <asp:ListView ID="lvType" runat="server">
                <LayoutTemplate>
                    <asp:PlaceHolder ID="itemPlaceholder" runat="server"/>
                </LayoutTemplate>

                <ItemTemplate>
                    <label>
                        <input <%# RenderChecked(Container.DataItem) %> type="radio" name="galsettings" value='<%# Eval("TYPE_ID") %>' />
                        <span ><%# Eval("DESC") %></span>
                        <div class="clearing"></div>
                    </label>
                    
                </ItemTemplate>
            </asp:ListView> 
        </div>
        
        <br />
        <div class="clearing"></div>        
        <span class="modPopup_mainTitle" >Število prikazanih slik obiskovalcem (ali v PREDOGLEDU) na strani (v načinu UREDI vedno vidiš vse slike)</span>
         <input class="modPopup_TBXsettings"  id="tbPageSize" style="width:20px;" maxlength="256" runat="server" type="text" />
         <i>* Ostale so obiskovalcem vidne v galeriji (ko jo odprejo s klikom na eno od slik).</i>
               
        
         <br />       
        <span class="modPopup_mainTitle" >Uredi naziv in opis galerije za različne jezike</span>        
                
        <div id="galSetLangDesc" >
            <asp:ListView ID="lvList" runat="server"   InsertItemPosition="None" >
                <LayoutTemplate>
                    <asp:PlaceHolder id="itemPlaceholder" runat="server"></asp:PlaceHolder>
                </LayoutTemplate>
                
                <ItemTemplate>
                    <div class="item" style="float:left; width:260px;">
                        <input type="hidden" value= '<%#  Eval("LANG_ID")%>' />
                           
                        <div style="color:#659700; font-size:15px; margin-bottom:4px;"> <%#  Eval("LANG_DESC")%></div>
                        <span style="font-size:14px;">Naziv galerije</span>
                        <br />
                        <input class="modPopup_TBXsettings modPopup_TBXsettingsGalLang" maxlength="256" type="text" value="<%# Eval("GAL_TITLE") %>" />
                        <br />
                        <span style="font-size:14px;">Opis galerije</span> 
                        <br />
                        <textarea class="modPopup_TBXsettings modPopup_TBXsettingsGalLang" rows="3" cols="50"   ><%# Eval("GAL_DESC") %></textarea>
                        <br />
                    </div>
                </ItemTemplate>
            </asp:ListView>
        </div>        
        

    </div>   
    <div class="clearing"></div>
    <div class="modPopup_footer"> 
        <div class="buttonwrapperModal">
            <a class="lbutton lbuttonConfirm" href="#" onclick="settingsGallerySave(); return false"><span>Shrani</span></a>
            <a class="lbutton lbuttonDelete" href="#" onclick="closeModalLoader(); return false" ><span>Prekliči</span></a>
        </div>
    </div>
</div>






