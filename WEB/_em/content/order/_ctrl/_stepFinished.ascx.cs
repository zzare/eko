﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using SM.UI.Controls;
using SM.EM.UI.Controls;
using System.Text;
using System.Globalization;

public partial class _em_content_order__ctrl__stepFinished : BaseWizardStepOrder 
{
    public event EventHandler OnSaveOK;

    List<ProductRep.Data.ShoppingCartItem> CartItemList;

    public bool IsViewing { get {
        bool ret = false;
        if (Request.QueryString["v"] != null)
            return (Request.QueryString["v"] == "1");

        return ret;
    } }


    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        ShortDesc = "Zaključek nakupa";
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        // order summary (do not remove!)
        objOrderSummaryFormatted.PageId = ParentPageEmenik.em_user.UserId;
        objOrderSummaryFormatted.Order = Order;

    }


    public override void LoadData()
    {
        ProductRep prep = new ProductRep();

        CartItemList = prep.GetCartItemList(Order.UserId, LANGUAGE.GetCurrentLang(), ParentPageEmenik.em_user.UserId, Order.OrderID);
        objShoppingCart.Data = CartItemList;
        objShoppingCart.ShowPriceWithShipping = true;
        objShoppingCart.Order = Order;
        objShoppingCart.Parameters = new object[] { ParentPageEmenik.em_user.UserId, false, false };
        objShoppingCart.LoadData();


        
        // order summary
        objOrderSummaryFormatted.PageId = ParentPageEmenik.em_user.UserId;
        objOrderSummaryFormatted.Order = Order;
        objOrderSummaryFormatted.LoadData();


        // show analytics ONLY if not viewing
        if (!IsViewing)
        {
            // ... and is order confirmed
            if (Order != null && Order.OrderStatus >= SHOPPING_ORDER.Common.Status.CONFIRMED )
                ParentPage.AnalyticsAppendScript = GetOrderSubmittedAnalytics();
        }

    }

    protected string GetOrderSubmittedAnalytics() {
        StringBuilder sb = new StringBuilder();

        CultureInfo ci = new CultureInfo("en-US", false);
        //Decimal val = Decimal.Parse(str, ci.NumberFormat);
        

        // Create a transaction object.
        sb.Append("_gaq.push(['_addTrans',");
        sb.Append(string.Format("'{0}', ", SHOPPING_ORDER.Common.RenderShoppingOrderNumber(Order)));           // order ID - required
        sb.Append(string.Format("'{0}', ", SM.BLL.Common.Emenik.Data.PortalName ())); // affiliation or store name
        sb.Append(string.Format("'{0}', ", Order.TotalWithTax.Value.ToString("N", CultureInfo.CreateSpecificCulture("en-US"))));          // total - required
        sb.Append(string.Format("'{0}', ", Order.Tax.ToString("N", CultureInfo.CreateSpecificCulture("en-US"))));           // tax
        sb.Append(string.Format("'{0}', ", Order.GetShippingPrice().ToString("N", CultureInfo.CreateSpecificCulture("en-US"))));              // shipping
        sb.Append(string.Format("'{0}', ", Order .GetShippingPostalAndCity()));       // city
        sb.Append(string.Format("'{0}', ", SHOPPING_ORDER.Common.RenderShoppingOrderShippingRegion((short)(Order .ShippingRegion ?? 0))));     // state or province
        sb.Append(string.Format("'SLO'"));             // country
        sb.Append(string.Format("]);"));
    
        // Add items to the transaction.
        foreach (var item in CartItemList) {

            sb.Append("<br/>");
            sb.Append("_gaq.push(['_addItem',");
            sb.Append(string.Format("'{0}', ", SHOPPING_ORDER.Common.RenderShoppingOrderNumber(Order)));           // order ID - required
            sb.Append(string.Format("'{0}', ", item.ProductID )); // SKU/code - required
            sb.Append(string.Format("'{0}', ", SM.EM.Helpers.StripHTML( item.ProductTitle ) ));          // product name
            sb.Append(string.Format("'{0}', ", "") );           // category or variation
            sb.Append(string.Format("'{0}', ", item.PriceTotal.ToString("N", CultureInfo.CreateSpecificCulture("en-US"))));              // unit price - required
            sb.Append(string.Format("'{0}', ", item.Quantity  ));       // quantity - required
            sb.Append(string.Format("]);"));
        
        }

        // Submit the transaction to the Analytics servers.
        sb.Append("<br/>");
        sb.Append("_gaq.push(['_trackTrans']);");

        return sb.ToString();
    }



    protected void btNext_Click(object o, EventArgs e)
    { 
    
    

        // if all ok
        OnSaveOK(this, new EventArgs());
    
    }



    protected string RenderFinishedText() { 
        string ret ="";
        string h = "";
        string desc = "";
        bool refresh = false;
        if (Order.PaymentType != SHOPPING_ORDER.Common.PaymentType.ONLINE_e24)
        { 
            h = "Hvala za naročilo.";
        }
            // e24 payment
        else if (Order.PaymentType == SHOPPING_ORDER.Common.PaymentType.ONLINE_e24)
        {

            h = "Napaka! Procesiranje plačila ni uspelo.";
            OnlineE24PipeRep olr = new OnlineE24PipeRep();

            // get trans
            if (Order.TransIDe24 != null)
            {
                E24_TRAN  trans = olr.GetTransByID(Order.TransIDe24.Value);
                if (trans != null){
                
                    //if (trans.Result == OnlineE24Payment .Common.Status.INITIALIZED ){
                    //    //bool realTime = true;
                    //    //if (realTime)
                    //    {
                    //        //h = "Transakcija se procesira in bo končana v nekaj sekundah. Prosimo počakajte trenutek.";
                    //        h = "Online plačilo je v poteku. Prosimo dokončajte postopek nakupa";
                    //        refresh = true;
                    //    }
                    //    //else {
                    //    //    h = "Hvala za oddan nakup. Podatki so bili uspešno zajeti. O izidu plačila boste obveščeni po elektronski pošti in na tej strani.";
                    //    //}
                    
                    
                    
                    //} // failed
                    //else
                    if (trans.Result == OnlineE24Payment.Common.Status.FAILED || trans.Result == OnlineE24Payment.Common.Status.NOT_APPROVED || trans.Result == OnlineE24Payment.Common.Status.INITIALIZED)
                    {
                        // move items back to basket
                        ProductRep prep = new ProductRep();
                        prep.UpdateCartItemOrder(Order.UserId, Order.PageId, Order.OrderID, null);
                        Order.OrderStatus = SHOPPING_ORDER.Common.Status.STEP_4;
                        Orep.Save();

                        h = "Plačilo ni uspelo. "; //Šifra napake: " + trans.RetCode  + ", " + trans.RetMsg ;
                        desc = "Prosimo poskusite ponovno čez nekaj trenutkov.";
                        desc += "<div class=\"stepnextbuttonW\">";
                        desc += "<a class=\"Astepnext\" href=\"" + Page.ResolveUrl(SM.EM.Rewrite.OrderProductUrl(SM.BLL.Custom.MainUserID , Order.OrderID  , 4)) + "\" ><span>POSKUSI PONOVNO &raquo;</span></a>";
                        desc += "</div><div class=\"clearing\"></div>";

                    } //ok
                    else if (trans.Result == OnlineE24Payment.Common.Status.APPROVED || trans.Result == OnlineE24Payment.Common.Status.CAPTURED)
                    {
                        h = "Hvala za naročilo.";
                    } // canceled
                    else if (trans.Status == MegaPos.Common.Status.CANCELLED )
                    {
                        h = "Naročilo je bilo preklicano.";

                    }
                }
            }
        }


        else {

            h = "Napaka! Prišlo je do napake pri procesiranju plačila";

            // get trans
            MegaPosRep mpr = new MegaPosRep();
            if (Order.TransID != null){
                MP_TRAN trans = mpr.GetTransByID(Order.TransID.Value );
                if (trans != null){
                
                    if (trans.Status == MegaPos.Common.Status.INITIALIZING ){
                        bool realTime = true;
                        if (realTime)
                        {
                            h = "Transakcija se procesira in bo končana v nekaj sekundah. Prosimo počakajte trenutek.";
                            refresh = true;
                        }
                        else {
                            h = "Hvala za oddan nakup. Podatki so bili uspešno zajeti. O izidu plačila boste obveščeni po elektronski pošti in na tej strani.";
                        }
                    
                    
                    
                    } // failed
                    else if (trans.Status == MegaPos.Common.Status.FAILED)
                    {
                        h = "Napaka! Plačilo ni uspelo. Šifra napake: " + trans.RetCode  + ", " + trans.RetMsg ;
                        desc = "Prosimo poskusite ponovno čez nekaj trenutkov.";
                        desc += "<div class=\"stepnextbuttonW\">";
                        desc += "<a class=\"Astepnext\" href=\"" + Page.ResolveUrl(SM.EM.Rewrite.OrderProductUrl(SM.BLL.Custom.MainUserID , Order.OrderID  , 4)) + "\" ><span>POSKUSI PONOVNO &raquo;</span></a>";
                        desc += "</div><div class=\"clearing\"></div>";

                    } //ok
                    else if (trans.Status == MegaPos.Common.Status.INITIALIZED || trans.Status == MegaPos.Common.Status.PROCESSED)
                    {
                        h = "Hvala za naročilo.";
                    } // canceled
                    else if (trans.Status == MegaPos.Common.Status.CANCELLED )
                    {
                        h = "Naročilo je bilo preklicano.";

                    }
                }
            }
        }


        // auto refresh
        if (refresh) {

            Response.AppendHeader("Refresh", MegaPos.StatusRefreshInterval().ToString());
            
        }



        return "<h3>" + h + "</h3>" + desc;
    
    
    
    }


}
