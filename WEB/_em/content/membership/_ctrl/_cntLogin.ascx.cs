﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Net.Mail;
using System.Text;
using System.IO;
using AjaxControlToolkit;
using SM.UI.Controls;

public partial class _ctrl_content_membership_ctrl_cntLogin : BaseControl_EMENIK 
{


    protected void Page_Load(object sender, EventArgs e)
    {

        //tmp 
        //Membership.CreateUser("sm_admin", "3archie!1moze");

        Login login = (Login)LoginView1.FindControl("Login1");


        if (login != null)
        {
            TextBoxWatermarkExtender weUsername = (TextBoxWatermarkExtender)login.FindControl("weUsername");
            if (weUsername != null)
                weUsername.WatermarkText = Resources.Modules.LoginEmail;

            TextBoxWatermarkExtender wePass = (TextBoxWatermarkExtender)login.FindControl("wePass");
            if (wePass != null)
                wePass.WatermarkText = Resources.Modules.LoginPassword;

            LinkButtonDefault LoginButton = (LinkButtonDefault)login.FindControl("LoginButton");
            if (LoginButton != null)
                LoginButton.Text = "<span>" + Resources.Modules.LoginButton + "</span>";

            RequiredFieldValidator UserNameRequired = (RequiredFieldValidator)login.FindControl("UserNameRequired");
            if (UserNameRequired != null)
            {
                UserNameRequired.ErrorMessage = Resources.Modules.LoginEmail;
                UserNameRequired.ToolTip = Resources.Modules.LoginEmail;
            }

            RequiredFieldValidator PasswordRequired = (RequiredFieldValidator)login.FindControl("PasswordRequired");
            if (PasswordRequired != null)
            {
                PasswordRequired.ErrorMessage = Resources.Modules.LoginPassword;
                PasswordRequired.ToolTip = Resources.Modules.LoginPassword;
            }

            TextBox Password = (TextBox)login.FindControl("Password");
            if (Password != null)
            {
                Password.Attributes["title"]  = Resources.Modules.LoginPassword;
            }

        }




    }


    protected void LoginError(object sender, EventArgs e)
    {

        //There was a problem logging in the user

        //See if this user exists in the database
        Login login = (Login)LoginView1.FindControl("Login1");
        MembershipUser userInfo = Membership.GetUser(login.UserName);
        Literal litLoginError = (Literal)login.FindControl("litLoginError");
        Control cLoginErr = login.FindControl("cLoginErr");
        PlaceHolder phResend = (PlaceHolder ) login.FindControl("phResend");

        if (userInfo == null)
            litLoginError.Text = Resources.Modules.LoginErrorUserNotExistsL + " " + login.UserName + " " + Resources.Modules.LoginErrorUserNotExistsR;
        else if (!userInfo.IsApproved)
        { //See if the user is locked out or not approved
            litLoginError.Text = Resources.Modules.LoginErrorUserNotApproved + ".";
            phResend.Visible = true;
        }
        else if (userInfo.IsLockedOut)
            litLoginError.Text = Resources.Modules.LoginErrorUserLocked + ".";
        else
            //The password was incorrect (don't show anything, the Login control already describes the problem)
            litLoginError.Text = Resources.Modules.LoginErrorPasswordIncorrect  + ".";

        cLoginErr.Visible = true;

    }

    protected void OnLogin(object sender, EventArgs e)
    {

        Login login = (Login)LoginView1.FindControl("Login1");

        // get users profile (the cookie is not set yet)
        //EM_USER usr = EM_USER.GetUserByUserName(login.UserName);

        //if (usr == null)
        //    return;

        // remember me
        //CheckBox cbRememberMe = (CheckBox)login.FindControl("loginCbx");

        string ru = "";

        //if (Roles.IsUserInRole(login.UserName, "admin") || Roles.IsUserInRole(login.UserName, "em_admin"))
        //{
        //    ru = SM.BLL.Common.Emenik.ResolveMainDomainUrl("~/cms/Default.aspx?smp=2");

        //}

        ParentPage.RedirectBackIfSpecified();

        // EMENIK LOGIN
        //if (Roles.IsUserInRole(login.UserName, "em_user"))
        //    SM.EM.Security.Login.LogIn(login.UserName, cbRememberMe.Checked, ru);

        if (!string.IsNullOrEmpty(ru))
            Response.Redirect(ru);

        // redirect to default page
//        Response.Redirect(SM.BLL.Common.ResolveUrl(SM.EM.Rewrite.EmenikUserUrl("eko", 1473, "Trgovina", LANGUAGE.GetCurrentCulture())));
        Response.Redirect(SM.BLL.Common.ResolveUrl(SM.EM.Rewrite.EmenikUserUrl("eko", 1473, "Trgovina")));
            


    }



    protected void btResend_Click(object sender, EventArgs e)
    {
         Login login = (Login)LoginView1.FindControl("Login1");
        MembershipUser usr = Membership.GetUser(login.UserName);
        

        // send confirmation mail        
        SendConfirmationMail(usr, "");
    }

    protected void btLogout_Logout(object sender, EventArgs e)
    {

        // if user has custom domain... logout from other domain
        EM_USER usr = EM_USER.GetUserByUserName(Page.User.Identity.Name);

        if (usr == null)
            return;

        // clear session
        SM.EM.Security.Login.LogOut();
        //Session["is_admin"] = null;

        if (usr.CUSTOM_DOMAIN != null && !String.IsNullOrEmpty(usr.CUSTOM_DOMAIN) && usr.CUSTOM_DOMAIN_ACTIVATED)
        {
            // logout from other domain also
            string domain = usr.CUSTOM_DOMAIN;
            string returnUrl = SM.BLL.Common.Emenik.Data.PortalHostDomain();
            //if (SM.EM.Rewrite.Helpers.IsDefaultDomain()){
            //    domain = ParentPage.em_user.CUSTOM_DOMAIN;
            //    returnUrl = SM.BLL.Common.Emenik.Data.PortalHostDomain();              
            //}

            returnUrl = "http://" + returnUrl;

            // logout from this domain
            FormsAuthentication.SignOut();
            //                SM.EM.Security.Login.LogOut();

            Response.Redirect(SM.EM.Rewrite.EmenikUserUrlLogout(domain, returnUrl));
        }
        else if (SM.BLL.Common.Emenik.Data.EnableSubDomainUsers())
        {
            // logout from subdomain also
            string domain = usr.CUSTOM_DOMAIN;
            string returnUrl = SM.BLL.Common.Emenik.Data.PortalHostDomain();

            returnUrl = "http://" + returnUrl;

            // logout from this domain
            FormsAuthentication.SignOut();

            Response.Redirect(SM.EM.Rewrite.EmenikUserUrlLogoutSubdomain(usr.PAGE_NAME, returnUrl));
        }
        else
        {
            // logout from this domain
            FormsAuthentication.SignOut();
            //                SM.EM.Security.Login.LogOut();


        }
    }


    protected void SendConfirmationMail(MembershipUser usr, string pass)
    {
        //if (!SendMail) return;

        if (usr == null) return;


        // construct the verification URL
        string key = Server.UrlEncode(SM.EM.Security.Encryption.Hex(usr.ProviderUserKey.ToString()));
        string verifyUrl = Request.Url.GetLeftPart(UriPartial.Authority) + Page.ResolveUrl(SM.EM.Rewrite.EmenikCustomerEmailVerify(ParentPage.em_user.UserId, key));



        StringBuilder sb = new StringBuilder();
        HtmlTextWriter writer = new HtmlTextWriter(new StringWriter(sb));

        objMailConfirmation.UserName = usr.UserName;
        objMailConfirmation.Password = pass;
        objMailConfirmation.VerifyUrl = verifyUrl;
        objMailConfirmation.PageName = SM.BLL.Common.Emenik.Data.PortalName();

        objMailConfirmation.PageHref = SM.EM.Rewrite.EmenikUserUrlGetLeftPart(ParentPage.em_user);
        objMailConfirmation.Visible = true;
        objMailConfirmation.RenderControl(writer);
        objMailConfirmation.Visible = false;

        // send mail
        //        MailMessage message = SM.EM.Mail.Template.GetNewUserConfirmation(password, usr);
        MailMessage message = new MailMessage();
        message.Sender = new MailAddress(SM.EM.Mail.Account.Info.Email, SM.BLL.Common.Emenik.Data.PortalName());
        message.From = new MailAddress(SM.EM.Mail.Account.Info.Email, SM.BLL.Common.Emenik.Data.PortalName());
        message.To.Clear();
        message.To.Add(new MailAddress(usr.Email));
        message.IsBodyHtml = true;
        message.Body = sb.ToString();
        message.Subject = ParentPage.em_user.PAGE_TITLE + " - uporabniški podatki";


        SmtpClient client = new SmtpClient();
        try
        {
            // if local, use gmail, else use server SMTP
            if (HttpContext.Current != null && HttpContext.Current.Request.IsLocal)
            {
                client.Credentials = new System.Net.NetworkCredential(SM.EM.Mail.Account.Info.Email, SM.EM.Mail.Account.Info.Pass);
                client.Port = 587;//or use 587            
                client.Host = "smtp.gmail.com";
                client.EnableSsl = true;
            }
            else
            {
                // network delivery for propely setting SENDER header in mail
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.Host = "localhost";
            }


            client.Send(message);
        }

        catch (Exception ex)
        {
            ERROR_LOG.LogError(ex, "SendConfirmationMail");
        }


    }


}
