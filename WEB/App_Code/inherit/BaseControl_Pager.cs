﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Linq.Expressions;
using System.Data.Linq ;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;


/// <summary>
/// Summary description for BaseControl
/// </summary>
public abstract class BaseControl_Pager : BaseControl_EMENIK
{

    public string QSKeySort = "ord";
    public string QSKeyDirection = "dir";
    public string QSKeyPage = "page";

    public string StartingQueryString = "";

    public string SelectedClass = "sel";
    public string PageLinkTemplate = "<a {0} class=\"pagenum {2}\">{1}</a>";


    public abstract string GetOrderColumnByID(int id);

    public abstract bool GetDefaultDirectionByID(int id);

    public virtual string RenderOrderDirectionByID(bool dir)
    {
        string ret = "1";
        if (dir == true)
            ret = "1";
        else if (dir == false)
            ret = "0";

        return ret;
    }

    public virtual string RenderSqlOrderDirectionByID(bool dir)
    {
        string ret = "asc";
        if (dir == true)
            ret = "asc";
        else
            ret = "desc";

        return ret;
    }

    /* CURRENT */
    public virtual  int GetCurrentOrder()
    {
        int ret = 1;
        if (HttpContext.Current == null)
            return ret;
        if (HttpContext.Current.Request.QueryString[QSKeySort] == null)
            return ret;

        int.TryParse(HttpContext.Current.Request.QueryString[QSKeySort], out ret);

        return ret;
    }
    public virtual bool GetCurrentDirection()
    {
        bool ret = GetDefaultDirectionByID(GetCurrentOrder());
        if (HttpContext.Current == null)
            return ret;
        if (HttpContext.Current.Request.QueryString[QSKeyDirection] == null)
            return ret;

        int dir = 0;
        int.TryParse(HttpContext.Current.Request.QueryString[QSKeyDirection], out dir);
        if (dir == 1)
            ret = true;
        else
            ret = false;

        return ret;
    }
    public virtual  int GetCurrentPage()
    {
        int ret = 1;
        if (HttpContext.Current == null)
            return ret;
        if (HttpContext.Current.Request.QueryString[QSKeyPage] == null)
            return ret;

        int.TryParse(HttpContext.Current.Request.QueryString[QSKeyPage], out ret);

        if (ret > TotalPages)
            ret = TotalPages;

        if (ret < 1)
            ret = 1;

        return ret;
    }

    public virtual string RenderCurrentSortSQL()
    {
        string ret = "";

        // render QS
        ret = GetOrderColumnByID(GetCurrentOrder());
        ret += " " + RenderSqlOrderDirectionByID(GetCurrentDirection());

        return ret;
    }


    /* RENDER */

    public virtual  string RenderSortQS(int id, bool changeDir)
    {
        string ret = "";
        bool dir = GetDefaultDirectionByID(id);

        // if current is selected, change direction
        if (changeDir && id == GetCurrentOrder() && GetDefaultDirectionByID(id) == GetCurrentDirection())
        {
            dir = !GetCurrentDirection();
        }
        else if (!changeDir && id == GetCurrentOrder()) {
            dir = GetCurrentDirection();        
        }

        // render QS
        ret = QSKeySort +"=" + id.ToString();
        ret += "&" + QSKeyDirection + "=" + RenderOrderDirectionByID(dir);

        return ret;
    }

    public virtual string RenderPageQS(int id)
    {
        string ret = "";
        if (id <= 1)
            return ret;

        // render QS
        ret = QSKeyPage+  "=" + id.ToString();
        return ret;
    }

    public virtual string RenderCurrentFilterQSWithout(string fid)
    {
        string ret = "";
        // render QS
        //ret = "page=" + id.ToString();
        return ret;
    }



    public virtual string RenderSortFullUrl(int sortId)
    {
        return RenderSortPageFullUrl(sortId, 1, true, "");
    }
    public virtual string RenderPageFullUrlHREF(int pageId)
    {

        if (pageId < 1 || pageId == GetCurrentPage() || pageId > TotalPages)
            return "";

        return "href=\"" + RenderSortPageFullUrl(GetCurrentOrder(), pageId, false, "") + "\"";
    }

    public virtual string  GetStartingAbsolutePath(){
        return Request.Url.AbsolutePath;
    }

    public virtual string RenderSortPageFullUrl(int sortId, int pageId, bool changeSortDir, string withoutFilt)
    {
        string ret = "";

        //ret = Utils.Web.QueryString.Current.Remove(QSKeySort).Remove(QSKeyPage).Remove(QSKeyDirection).ToString();

        ret = GetStartingAbsolutePath();
        string qs = StartingQueryString;
        
        // sort
        if (!string.IsNullOrEmpty(RenderSortQS(sortId, changeSortDir)))
        {
            if (!string.IsNullOrEmpty(qs))
                qs += "&";
            qs += RenderSortQS(sortId, changeSortDir);
        }
        
        // page
        if (!string.IsNullOrEmpty(RenderPageQS(pageId)))
        {
            if (!string.IsNullOrEmpty(qs))
                qs += "&";
            qs += RenderPageQS(pageId);
        }
        
        //filter
        if (!string.IsNullOrEmpty(RenderCurrentFilterQSWithout (withoutFilt)))
        {
            if (!string.IsNullOrEmpty(qs))
                qs += "&";
            qs += RenderCurrentFilterQSWithout(withoutFilt);
        }

        // append to url
        if (!string.IsNullOrEmpty(qs))
            qs = "?" + qs;

        ret += qs;
        
        return Page.ResolveUrl(ret);
    }

    public virtual string RenderSelClass(int sortId)
    {
        string ret = "";

        if (sortId != GetCurrentOrder())
            return ret;
        ret = " sel";
        ret += " " + RenderSqlOrderDirectionByID(GetCurrentDirection());

        return ret;
    }


    /* PAGING */

    //protected IQueryable<T> _productList;
    //public virtual  IQueryable<T> ProductList
    //{
    //    get
    //    {
    //        return _productList;
    //    }

    //    set { _productList = value; }
    //}
    protected int _pageSize = 10;
    public  virtual int PageSize
    {
        get
        {
            return _pageSize;
        }
        set {
            _pageSize = value;
        }
    }
    protected  int _TotalCount = -1;
    protected virtual int TotalCount
    {
        get
        {
            //if (_TotalCount >= 0)
            //    return _TotalCount;
            //if (ProductList == null)
            //    return 0;

            //_TotalCount = ProductList.Count() ;
            return _TotalCount;
        }
        set { _TotalCount = value; }
    }

    protected virtual int TotalPages
    {
        get
        {
            if ((TotalCount % PageSize) == 0)
            {
                return (TotalCount / PageSize);
            }
            else
            {
                double result = (TotalCount / PageSize);
                result = Math.Ceiling(result);
                return (Convert.ToInt32(result) + 1);
            }
        }
    }

    public virtual string RenderSelPageClass(int page)
    {
        string ret = "";

        if (page != GetCurrentPage())
            return ret;
        ret = " " + SelectedClass;

        return ret;
    }


    public string RenderPageLinks(int limit) {
        string ret = "";

        int count = TotalPages;

        int min = 1;
        int max = TotalPages;
        int cur = GetCurrentPage();
        int half_limit = (int)Math.Round((decimal)(limit-1)/2);

        if (limit > 0 && count > limit) {
            // limit


            // min
            if (max < limit || (cur <= half_limit + 1))
                max = min + limit - 1;
            // max
            else if (cur >= max - half_limit - 1)
                min = max - limit + 1;
            else
            {
                min = cur - half_limit;
                max = min + limit -1;
            }
            
            count = limit;

        
        
        }
        


        for (int i = min; i <= max; i++)
        {
            ret += string.Format(PageLinkTemplate, RenderPageFullUrlHREF(i), i, RenderSelPageClass(i));
        }


        return ret;
    }


}
