﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewAttributeEditRange.ascx.cs" Inherits="_ctrl_module_product_attribute_viewAttributeEditRange" %>

<script language = "javascript" type="text/javascript">

    function editProductAttributeSaveRange(id, grid) {
        var _items = new Array();
        $("#prodAttributesRange").find("tbody tr").each(function (i, val) {
            var _item = new Object();
            _item.ValMin = $(this).find("#tbaMin").val();
            _item.ValMax = $(this).find("#tbaMax").val();
            _item.ValDefault = $(this).find("#tbaDefault").val();
            _item.Value = $(this).find("input.hfAtrID").val();

            Array.add(_items, _item);
        });

        openModalLoading('Shranjujem...');
        WebServiceCWP.SaveProductAttributesRange(id, grid, _items, em_g_lang, onProductEditSuccessSave, onProductSaveError, id);

    }



    $(document).ready(function() {
        $('input.valid-number').bind('keypress', function (e) {
            return (e.which != 45 &&  e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) ? false : true; //allow '-'(45)
        })


    });
       
</script>



<div class="modPopup_header modPopup_headerEdit">
    <h4>UREDI DODATNE ATRIBUTE</h4>
    <p>Uredi dodatne atribute za ta artikel</p>
</div>
<div class="modPopup_main">
    
    <span class="modPopup_mainTitle"><%= GroupAttribute.Title  %></span> 
    
    
    <asp:ListView ID="lvList" runat="server" >
        <LayoutTemplate>
            <div id="prodAttributesRange">
<%--                <label>
                    <input id="cbAttrSelAll" type="checkbox"  />
                    &nbsp;Označi vse/nobenega
                </label>
                <br />--%>

                <table>
                    <thead>
                        <tr>
                            <th></th>
                            <th>Minimum</th>
                            <th>Maximum</th>
                            <th>Privzeto</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        <asp:PlaceHolder ID="itemPlaceholder" runat="server"/>
                    
                    </tbody>
                
                </table>
            </div>
        </LayoutTemplate>
        
        <ItemTemplate>
            

            <tr>
                <td>
                    <label>
                        <%--<input <%# RenderChecked(Container.DataItem) %> type="checkbox" value='<%# Eval("AttributeID") %>' />--%>
                        &nbsp;<%# Eval("Title")  %>
                        
                        <input type="hidden" class="hfAtrID" value='<%# Eval("AttributeID")  %>' />
                    </label>
                </td>
                <td>
                    <input class="modPopup_TBXsettings valid-number" style="width:80px; margin-right:3px; text-align:right;" id="tbaMin" maxlength="20"  type="text" value='<%# GetAttribute(Container.DataItem).ValMin  %>' />
                </td>
                <td>
                    <input class="modPopup_TBXsettings valid-number" style="width:80px; margin-right:3px; text-align:right;" id="tbaMax" maxlength="20"  type="text" value='<%# GetAttribute(Container.DataItem).ValMax %>' />
                </td>
                <td>
                    <input class="modPopup_TBXsettings valid-number" style="width:80px; margin-right:3px; text-align:right;" id="tbaDefault" maxlength="20"  type="text" value='<%# GetAttribute(Container.DataItem).ValDefault %>' />
                </td>

            </tr>            

        </ItemTemplate>
    </asp:ListView>    
    
    

</div>   

<div class="modPopup_footer"> 
    <div class="buttonwrapperModal">
        <a class="lbutton lbuttonConfirm" href="#" onclick="editProductAttributeSaveRange('<%= ProductID %>', '<%= GroupAttribute.GroupAttID %>'); return false"><span>Shrani</span></a>
        <a class="lbutton lbuttonDelete" href="#" onclick="closeModalLoader(); return false" ><span>Prekliči</span></a>
    </div>
</div>
