﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;


/// <summary>
/// Summary description for BasePage
/// </summary>
namespace SM.EM.UI
{

    public class BasePage : System.Web.UI.Page
    {
        public string TitleRoot = SM.BLL.Common.Emenik.Data.PortalName();
        public BaseMasterPage mp ;
        protected bool CheckPermissions = true;
        public const string LanguageDropDownID = "ctl00$selLanguage$ddlLanguage";
        public const string PostBackEventTarget = "__EVENTTARGET";
        public string RegisterDocumentReadyScriptStart = "";
        public string RegisterDocumentReadyScript = "";
        public string RegisterDocumentReadyScriptEnd = "";
        public string RegisterClientScriptBlockStart = "";
        protected bool EnableBonusID = true;
        protected bool AutoLoadMetaFromSitemap = true;
        protected bool  ForceMainPageDomain = false;
        public string RelCanonical = "";

        public string AnalyticsAppendScript = "";



        public int OutputCacheDuration = -1;
        public bool OutputCacheSliding = false;

        public  int CurrentSitemapID
        {
            get
            {
                int _smpID = -1;

                if (Context.Items["smp"] != null)
                {
                    int.TryParse(Context.Items["smp"].ToString(), out _smpID);
                    if (_smpID > 0) return _smpID;
                }

                if (Request.QueryString["smp"] == null) return _smpID;
                // remove duplicate entries
                string _smp = Request.QueryString["smp"].Split(",".ToCharArray())[0];

                if (!Int32.TryParse(_smp , out _smpID)) return _smpID;
                return _smpID;
            }
            set { Context.Items["smp"] = value; }
        }
        public int CurrentSitemapIDMenu
        {
            set { Context.Items["smp_menu"] = value; }
            get
            {
                if (Context.Items["smp_menu"] != null)
                    return (int)Context.Items["smp_menu"];
                return CurrentSitemapID;
            
            }
        }

        public SiteMapNode GetCurrentSitemapNode {

            get {
                if (!string.IsNullOrEmpty(DefaultProvider))
                {
                    string[] pList = DefaultProvider.Split(",".ToCharArray());

                    foreach (string p in pList)
                    {
                        SiteMapProvider prov = SiteMap.Providers[p.Trim()];

                        if (prov != null && prov.CurrentNode != null)
                        {
                            return prov.CurrentNode;
                        }

                    }

                }
                return null;
            
            }
        
        }

        protected string DefaultProvider = "main, mainFooter";
        
        
        protected SITEMAP_LANG _currentSitemapLang;
        public SITEMAP_LANG CurrentSitemapLang
        {
            get
            {
                if (_currentSitemapLang != null)
                    return _currentSitemapLang;
                // retireve sitemap 
                _currentSitemapLang = SITEMAP.GetSitemapLangByID(CurrentSitemapID, LANGUAGE.GetCurrentLang());
                return _currentSitemapLang;
            }
        }



        protected override void InitializeCulture()
        {
            //string culture = (HttpContext.Current.Profile as ProfileCommon).Preferences.Culture;
            ///<remarks><REMARKS>
          ///Check if PostBack occured. Cannot use IsPostBack in this method
          ///as this property is not set yet.
          ///</remarks>
            if (Request[PostBackEventTarget] != null)
            {
                string controlID = Request[PostBackEventTarget];

                if (controlID.Equals(LanguageDropDownID))
                {
                    string selectedValue = Request.Form[Request[PostBackEventTarget]].ToString();

                    //Thread.CurrentThread.CurrentUICulture = new CultureInfo(selectedValue);
                    //Thread.CurrentThread.CurrentCulture = new CultureInfo(selectedValue);
                    this.Culture = selectedValue;
                    this.UICulture = selectedValue;
                    Session["MyCulture"] = selectedValue;
                }
            }
            ///<remarks>
            ///Get the culture from the session if the control is tranferred to a
            ///new page in the same application.
            ///</remarks>
            if (Session["MyUICulture"] != null && Session["MyCulture"] != null)
            {
                //Thread.CurrentThread.CurrentUICulture = (CultureInfo)Session["MyUICulture"];
                //Thread.CurrentThread.CurrentCulture = (CultureInfo)Session["MyCulture"];
                this.Culture = Session["MyCulture"].ToString();
                this.UICulture = Session["MyCulture"].ToString();
            }

            // set CULTURE based on current language/culture
            string  culture =  LANGUAGE.GetCurrentCulture();
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
            Thread.CurrentThread.CurrentCulture = new CultureInfo(culture);
            




            base.InitializeCulture();


            //this.Culture = culture;
            //this.UICulture = culture;
        }



        //public int SelectedSitemapID = -1;

        protected override void OnPreInit(EventArgs e)
        {
            if (ForceMainPageDomain)
                Redirect301ToMainDomain();


            //Theme = "Basic";
            //SelectedSitemapID = CurrentSitemapID;
            base.OnPreInit(e);

            mp = (BaseMasterPage)Page.Master;

            // init cache
            OutputCacheDuration = SM.BLL.Common.Emenik.Data.OutputCacheDuration();
            OutputCacheSliding = SM.BLL.Common.Emenik.Data.OutputCacheSliding();

     
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            CurrentSitemapID = CurrentSitemapID; // add current sitemap to context too
            //this.Context.Items["smp"] = SelectedSitemapID;
            this.Title = TitleRoot;

            // check for bonus
            if (EnableBonusID)
            {
                if (!User.Identity.IsAuthenticated) // only for NON AUTHENTICATED USERs
                {
                    string bonId = "";
                    if (Request.QueryString["bid"] != null)// bonus ID from USER
                    {
                        bonId = Server.UrlDecode(Request.QueryString["bid"]);
                        bonId = SM.EM.Security.Encryption.DeHex(bonId);
                        if (SM.EM.Helpers.IsGUID(bonId))
                        {
                            //(HttpContext.Current.Profile as ProfileCommon).Emenik.BonusID = new Guid(bonId);
                            //(HttpContext.Current.Profile as ProfileCommon).Save();
                        }
                    }
                    if (Request.QueryString["cid"] != null) // bonus ID from CAMPAIGN (AFFILLIATE)
                    {
                        bonId = Server.UrlDecode(Request.QueryString["cid"]);
                        if (SM.EM.Helpers.IsGUID(bonId))
                        {
                            //(HttpContext.Current.Profile as ProfileCommon).Emenik.BonusID = new Guid(bonId);
                            //(HttpContext.Current.Profile as ProfileCommon).Save();
                        }
                    }
                }
            }

            // check for CentrSource ID
            if (Request.QueryString["csepr"] != null) // bonus ID from CAMPAIGN (AFFILLIATE)
            {
                string csepr = Server.UrlDecode(Request.QueryString["csepr"]);
                // add value to cookie
                HttpCookie csCookie = new HttpCookie("csepr");
                csCookie.Value = Request.QueryString["csepr"];
                Response.Cookies.Add(csCookie);

                // save campaign reference
                //(HttpContext.Current.Profile as ProfileCommon).Emenik.BonusID = new Guid(Centrsource.Common.CampaignID);
                //(HttpContext.Current.Profile as ProfileCommon).Save();

                //if (Page.User.Identity.Name == "zare.88")
                //    Response.Write("csepr:" + Request.QueryString["csepr"]);
                
            }


        }

        protected bool HasPermission() {

            // check for permissions
            if (!CheckPermissions) return true;
             
            
            // get all providers
            string[] pList = DefaultProvider.Split(",".ToCharArray());

            foreach (string p in pList) {
                SiteMapProvider prov = SiteMap.Providers[p.Trim()];
                if (prov != null)
                {
                    SiteMapNode node = prov.FindSiteMapNodeFromKey(CurrentSitemapID.ToString());
                    if (node != null) return true;
                }
            
            }



            
            return false;
        }
        
        protected override void OnLoad(EventArgs e)
        {

            base.OnLoad(e);
            
            // athorization
            if (!HasPermission()) RequestLogin();
        

            // force menu selection
            //if (SelectedSitemapID > -1) mp.SelectedSitemapID = SelectedSitemapID; 


            // auto load META TAGS from CMS
            if (AutoLoadMetaFromSitemap) {
                LoadMETA();
            }



            LoadTitle();
        }


        protected override void OnPreRender(EventArgs e)
        {


            base.OnPreRender(e);

            // register script block (at the beginnig FORM)
            if (!String.IsNullOrEmpty(RegisterClientScriptBlockStart))
                this.Page.ClientScript.RegisterClientScriptBlock(this.Page.GetType(), "blockScript", "<script type=\"text/javascript\">$(document).ready(function(){" + RegisterClientScriptBlockStart + " });</script>");

            
            // register document ready script (at the end, right before end FORM)
            if (!String.IsNullOrEmpty(RegisterDocumentReadyScriptStart + RegisterDocumentReadyScript + RegisterDocumentReadyScriptEnd))
                this.Page.ClientScript.RegisterStartupScript(this.Page.GetType(), "readyScript", "<script type=\"text/javascript\">$(document).ready(function(){" + RegisterDocumentReadyScriptStart + RegisterDocumentReadyScript + RegisterDocumentReadyScriptEnd + " });</script>");


            // register some base scripts
            // IE6 only
            if (Request.Browser.Browser == "IE" && Request.Browser.MajorVersion <= 6)
            {
                Helpers.RegisterScriptIncludeAJAX(ResolveUrl("~/JavaScript/AdapterUtils.js"), this, "AdapterUtils");
                Helpers.RegisterScriptIncludeAJAX(ResolveUrl("~/JavaScript/MenuAdapter.js"), this, "Menuadapter");
                Helpers.RegisterScriptIncludeAJAX(ResolveUrl("~/JavaScript/TransparentPng.js"), this, "IePngFix");
            }

            // set output caching if enabled
            OutputCaching();

            // set rel = canonical
            SetRelCanonical();
            if (!string.IsNullOrEmpty(RelCanonical))
            {
                HtmlLink link = new HtmlLink();
                link.Attributes["rel"] = "canonical";
                link.Href = Page.ResolveUrl(RelCanonical);
                Header.Controls.Add(link);
            }

        }


        protected virtual void LoadMETA()
        {
            SITEMAP_LANG sml = CurrentSitemapLang;
            if (sml == null)
                return;

            // add description meta tag
            if (!string.IsNullOrEmpty(sml.SML_META_DESC))
            {
                HtmlMeta description = new HtmlMeta();
                description.Name = "DESCRIPTION";
                description.Content = sml.SML_META_DESC;
                Header.Controls.Add(description);
            }

            // add keywords
            if (!string.IsNullOrEmpty(sml.SML_KEYWORDS))
            {
                HtmlMeta keys = new HtmlMeta();
                keys.Name = "KEYWORDS";
                keys.Content = sml.SML_KEYWORDS;
                Header.Controls.Add(keys);
            }

        }

        protected virtual void SetRelCanonical() { }

        public void SetTitle(string title)
        {
            this.Title = title + " | " + TitleRoot;
        }

        protected virtual void LoadTitle(){

            // set title
            // foreach (SiteMapProvider prov in SiteMap.Providers){

            if (!string.IsNullOrEmpty(DefaultProvider))
            {
                 string[] pList = DefaultProvider.Split(",".ToCharArray());

                 foreach (string p in pList) {
                     SiteMapProvider prov = SiteMap.Providers[p.Trim()];

                     if (prov != null && prov.CurrentNode != null)
                     {
                         Page.Title = GetTitleFromSiteMap(prov, prov.CurrentNode);
                     }

                 }




                //SiteMapProvider prov = SiteMap.Providers[DefaultProvider];
                //if (prov.CurrentNode != null)
                //{
                //    Page.Title = GetTitleFromSiteMap(prov, prov.CurrentNode);
                //}
            }
            //}
        }

        protected string GetTitleFromSiteMap(SiteMapNode aNode)
        {
            return GetTitleFromSiteMap(aNode.Provider, aNode, " | ", true, true);
        }


        protected string GetTitleFromSiteMap(SiteMapProvider aProvider, SiteMapNode aNode)
        {
            if (aNode == null || aNode.ParentNode == null)
            {
                return TitleRoot;
            }
            string tit = aNode.Description;
            if (string.IsNullOrEmpty(aNode.Description))
                tit = aNode.Title;
            return tit  + " | " + GetTitleFromSiteMap(aProvider, aNode.ParentNode);
        }

        protected string GetTitleFromSiteMap(SiteMapProvider aProvider, SiteMapNode aNode, string separ, bool addRoot, bool reverse)
        {
            if (aNode == null || aNode.ParentNode == null)
            {
                if (addRoot)
                    return  TitleRoot;
                else
                    return "";
            }
            string ret = GetTitleFromSiteMap(aProvider, aNode.ParentNode, separ, addRoot, reverse);
            if (ret != "")
            {
                if (reverse)
                    ret = separ + ret;
                else
                    ret = ret + separ;
            }

            // get title
            string tit = aNode.Description;
            if (string.IsNullOrEmpty(aNode.Description))
                tit = aNode.Title;

            if (reverse)
                ret = tit + ret;
            else
                ret = ret + tit;

            return ret;
        }


        public string BaseUrl
        {
            get
            {
                string url = this.Request.ApplicationPath;
                if (url.EndsWith("/"))
                    return url;
                else
                    return url + "/";
            }
        }

        public string FullBaseUrl
        {
            get
            {
                return this.Request.Url.AbsoluteUri.Replace(
                   this.Request.Url.PathAndQuery, "") + this.BaseUrl;
            }
        }

        public void RequestLogin()
        {
            this.Response.Redirect(FormsAuthentication.LoginUrl + "?ReturnUrl=" + this.Request.RawUrl);
        }

        public string GetReturnUrl()
        {
            string ru = "";
            ru = Request.Url.Scheme + "://"  + Request.Url.Authority + Request.Url.AbsolutePath ;

            return "ru=" +  Server.UrlEncode( ru);
        }

        public string GetReturnUrlWithQS()
        {
            string ru = "";
            ru = Request.Url.Scheme + "://" + Request.Url.Authority + Request.Url.PathAndQuery;

            return "ru=" + Server.UrlEncode(ru);
        }


        public string PreserveReturnUrl(string url)
        {
            string ru = "";
            if (Request.QueryString["ru"] != null)
                ru = Request.QueryString["ru"];
            string u = url;
            if (!u.Contains("?") && !string.IsNullOrEmpty(ru))
                u = u + "?ru=" + HttpUtility.UrlEncode( ru);
            return u;
        }
        public void RedirectBackIfSpecified() {
            string ru = "";
            if (Request.QueryString["ru"] != null)
                ru = Server.UrlDecode( Request.QueryString["ru"]);

            if (!String.IsNullOrEmpty(ru))
                Response.Redirect(Page.ResolveUrl(ru));
        
        }

        protected string  RenderReturnUrlBackLink(string title)
        {
            string ru = "";
            if (Request.QueryString["ru"] != null)
                ru = Server.UrlDecode( Request.QueryString["ru"]);

            if (String.IsNullOrEmpty(ru))
                return "";

            if (string.IsNullOrEmpty(title))
                title = "nazaj";

            return string.Format("<a href=\"{0}\">{1}</a>", Page.ResolveUrl( ru), title );

        }



        public void SetViewStateValue(Control ctrl, string key, object value) {
            this.ViewState[ctrl.ClientID + key] = value;        
        }
        public object GetViewStateValue(Control ctrl, string key)
        {
            return this.ViewState[ctrl.ClientID + key];
        }

        public void ErrorRedirect(int errID )
        {
                ErrorRedirect(errID , 404, false );

        }
        public void ErrorRedirect(int errID, int code)
        {
            ErrorRedirect(errID, code , false);
        }


        public void ErrorRedirect(int errID, int code, bool mainDomain)
        {
            string redirectUrl =  "~/_err/Error.aspx?e=" + errID.ToString();

            if (mainDomain)
                redirectUrl = SM.BLL.Common.Emenik.ResolveMainDomainUrl(redirectUrl);
            else
                redirectUrl = Page.ResolveUrl(redirectUrl) ;

            // redirect with code 404
            if (code == 404)
            {
                Response.StatusCode = 404;
                Response.StatusDescription = "Page not found";
                Response.RedirectLocation = redirectUrl;
                Response.End();
            }
            else if (code == 301)
            {
                Response.StatusCode = 301;
                Response.StatusDescription = "Moved Permanently";
                Response.RedirectLocation = redirectUrl;
                Response.End();
            }
            else
            {
                Response.Redirect(redirectUrl);
            }

        }

        public  string RenderSettingsHref()
        {

            string ru = GetReturnUrl();
            return Page.ResolveUrl(SM.EM.Rewrite.Main.UserSettingsURL() + "?" + ru);
        }

        public virtual void OutputCaching(){
        }
        
        public virtual void RemovePageFromCache()
        {
        }



        protected void Redirect301(string url)
        {

                Response.Status = "301 Moved Permanently";
                Response.RedirectLocation = url.Replace("~/", "http://" + SM.BLL.Common.Emenik.Data.PortalHostDomain()+ "/");
                Response.End();
        }
        protected void Redirect301ToMainDomain()
        {

            if (!Rewrite.Helpers.IsPortalHostDomain())
            {
                Response.Status = "301 Moved Permanently";
                Response.RedirectLocation = "http://" + SM.BLL.Common.Emenik.Data.PortalHostDomain() +  Request.Url.PathAndQuery;
                Response.End();
            }
        }

        protected void Redirect404(string url)
        {

            Response.StatusCode = 404;
            Response.StatusDescription = "Moved Permanently";
            Response.RedirectLocation = url.Replace("~/", "http://" + SM.BLL.Common.Emenik.Data.PortalHostDomain() + "/");
            Response.End();
        }


    }
}