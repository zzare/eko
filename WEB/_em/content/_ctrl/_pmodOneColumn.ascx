﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_pmodOneColumn.ascx.cs" Inherits="_em_content__ctrl_pmodOneColumn" %>
<%@ Register Namespace="SM.EM.UI.Controls" TagPrefix="EM"  %>
<%@ Register TagPrefix="EM" TagName="ModuleNew" Src="~/_ctrl/emenik/emModuleNew.ascx"  %>
<%@ Register TagPrefix="EM" TagName="SubMenu" Src="~/_em/templates/_custom/eko/_ctrl/_ctrlSubMenu.ascx"  %>


<%--NEW BEGIN--%>
    <h1 class="ph_h2" style="font-size:40px;"><span class="ph_h2_text"><span class="ph_h2_picL"></span><span class="ph_h2_picR"></span><%= ParentPage.CurrentSitemapLang.SML_DESC  %></span></h1>
    <%= RenderDescription() %>

    <% if (LANGUAGE.GetCurrentLang() == "si"){%>
    <div class="catdecs_btnW">
        <div class="catdecs_btn">
            <% if (SM.EM.Security .Permissions.CanAddBlog()) {%>
                <a class="btn_send btAddBlog" onclick ="newBlogLoad('<%= Guid.Empty %>', -1, <%= CurrentSitemapID %>); return false" href="#"><span class="font_bevan"><%= GetGlobalResourceObject("Modules", "BlogAddNew")%></span></a>
            <%} else {%>
                <a class="btn_send btAddBlog"  href="<%= SM.BLL.Common.ResolveUrl(SM.EM.Rewrite.EmenikCustomerLogin(SM.BLL.Custom.MainUserID, ParentPage.GetReturnUrl()))%>"><span class="font_bevan"><%= GetGlobalResourceObject("Modules", "LoginTitle").ToString().ToUpper()%></span></a>
            <%} %>
            <div class="clearing"></div>
        </div>
    </div>
<%}else if (LANGUAGE.GetCurrentLang() == "en"){ %>  
<%}else if (LANGUAGE.GetCurrentLang() == "it"){ %>  
<%} %>



    <EM:SubMenu ID="objSubMenu" runat="server" />

    <span class="art_sepa"></span>
<%--NEW END--%>



    <div class="cont_wideW">
        <div class="cont_wideT"></div>
        <div class="cont_wideM">
            <div class="cont_wideMI">

                <EM:ModuleNew ID="mnMain" runat="server" />

                <EM:CwpZone ID="wpz_1" runat="server">
                </EM:CwpZone>
    
            </div>    
        </div>
        <div class="cont_wideB"></div>
    </div>
