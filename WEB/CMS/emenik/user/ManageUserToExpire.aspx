﻿<%@ Page Language="C#" MasterPageFile="~/CMS/CMS.master" AutoEventWireup="true" CodeFile="ManageUserToExpire.aspx.cs" Inherits="SM.UI.CMS.CMS_Intranet_ManageUserToExpire" Title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register TagPrefix="SB" TagName="UserEdit" Src="~/CMS/controls/user/_cmsUserEdit.ascx"   %>
<%@ Register TagPrefix="SB" TagName="UserFilter" Src="~/CMS/controls/user/_cmsUserFilter.ascx"   %>
<%@ Register TagPrefix="SB" TagName="UserList" Src="~/CMS/controls/user/_cmsUserList.ascx"   %>
<%@ Register TagPrefix="EM" TagName="PaymentList" Src="~/CMS/controls/payment/_cmsPaymentList.ascx"   %>
<%@ Register TagPrefix="EM" TagName="PaymentEdit" Src="~/CMS/controls/payment/_cmsPaymentEdit.ascx"   %>

<asp:Content ID="c1" ContentPlaceHolderID="cphNavL" runat="server">
    
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cph" Runat="Server">

    <asp:UpdatePanel ID="upList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>

            <asp:Panel ID="panSListHD" runat="server" CssClass="cpeListHD" >
                <asp:Image ID="imgCollapse" runat="server" />
               USER list
            </asp:Panel>

            <asp:Panel ID="panSList" runat="server">

                <div class="qFilter">
                    <asp:UpdatePanel ID="upFilter" runat="server" UpdateMode="Always">
                        <ContentTemplate>
                            <div class="floatR">
                                <SB:UserFilter ID="objFilter" runat="server" />
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>

                <SB:UserList ID="objUserList" runat="server" ShowCheckboxColumn="false" ShowManageColumn="true" ShowEditColumn="true" />
            </asp:Panel>    

            <cc1:CollapsiblePanelExtender ID="cpeQList" runat="server" Collapsed="false" CollapseControlID="panSListHD" CollapsedImage="~/_res/images/icon/plus.gif" ExpandedImage="~/_res/images/icon/minus.gif" ImageControlID="imgCollapse" ExpandControlID="panSListHD" TargetControlID="panSList" ExpandDirection="Vertical" CollapsedText="Expand" ExpandedText="Collapse" >
            </cc1:CollapsiblePanelExtender>    
                
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="objFilter" EventName="OnFilterChanged"   />
            <asp:AsyncPostBackTrigger ControlID="tabUser$tpUser$objUserEdit" EventName="OnUserChanged"   />
            <asp:AsyncPostBackTrigger ControlID="tabUser$tpUser$objUserEdit" EventName="OnCancel"   />
        </Triggers>
    </asp:UpdatePanel>

        
    <br />

    <asp:UpdatePanel ID="upEdit" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
        <ContentTemplate>
            <cc1:TabContainer ID="tabUser" runat="server" ActiveTabIndex="0"  >
                <cc1:TabPanel ID="tpUser" HeaderText="User DATA" runat="server"  >
                    <ContentTemplate>
                        
                         <SB:UserEdit   id="objUserEdit" runat="server" />
                    
                    </ContentTemplate>
                </cc1:TabPanel>    
                
                <cc1:TabPanel ID="tpPayment" HeaderText="PAYMENTS" runat="server" Visible="true"  >
                    <ContentTemplate>
                    
                        <EM:PaymentList id="objPayment" runat="server"></EM:PaymentList>
                        <EM:PaymentEdit id="objPaymentEdit" runat="server"></EM:PaymentEdit>
                            
                    </ContentTemplate>
                </cc1:TabPanel>                   
            </cc1:TabContainer>

        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger EventName="OnEdit" ControlID="objUserList" />
        </Triggers>        
    </asp:UpdatePanel>              





</asp:Content>