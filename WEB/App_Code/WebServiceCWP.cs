﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Linq.Dynamic;

/// <summary>
/// Summary description for WebServiceCWP
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]
public class WebServiceCWP : System.Web.Services.WebService {

    public class LangTitleDesc
    {
        public string Lang { get; set; }
        public string Title { get; set; }
        public string Desc { get; set; }
    }
    public class LangTitleAbstrDesc:LangTitleDesc 
    {
        public string Abstract { get; set; }
    }



    public WebServiceCWP () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

#region CWP

    [WebMethod]
    public bool UpdateCwpActive(Guid  cwpID, bool active)
    {
        if (Context.User.Identity.IsAuthenticated)
            if (Context.User.IsInRole("admin"))
                CMS_WEB_PART.UpdateActive(cwpID, active);
            else
            {

                // get current page
                EM_USER usr = EM_USER.Current.GetCurrentPage();
                if (usr == null)
                {
                    return active;
                }

                // check permissions
                if (!PERMISSION.User.HasPermissionTypeMODULE(usr.UserId, null, cwpID, PERMISSION.Common.Obj.MOD_ALL, PERMISSION.Common.Action.Hide, PERMISSION.Common.Type.Allow))
                {
                    return active;
                }


                CMS_WEB_PART.UpdateActiveSecure(cwpID, active, usr.USERNAME );
            }

        return active;
    }

    [WebMethod]
    public string MoveCWP(Guid cwpID, string cult, string zone, int pos)
    {
        if (Context == null)
            return "no go ;)";

 
        if (Context.User.Identity.IsAuthenticated)
        {
            // get current page
            EM_USER usr = EM_USER.Current.GetCurrentPage();
            if (usr == null)
            {
                return "Nimaš pravic. Kontaktiraj administratorja.";
            }

            // check permissions
            if (!PERMISSION.User.HasPermissionTypeMODULE(usr.UserId, null, cwpID, PERMISSION.Common.Obj.MOD_ALL, PERMISSION.Common.Action.Move, PERMISSION.Common.Type.Allow))
            {
                return "Nimaš pravic. Kontaktiraj administratorja.";
            }

            // set culture
            LANGUAGE.SetCurrentCulture(cult);

//            bool res = CMS_WEB_PART.MoveCWPtoPosition(cwpID, zone, pos, Context.User.Identity.Name);
            bool res = CMS_WEB_PART.MoveCWPtoPosition(cwpID, zone, pos, usr.USERNAME );
            if (!res)
                return "Element ne more biti premaknjen v ta prostor.";
        }

        return "";
    }

    [WebMethod]
    public string MoveImage(Guid cwpID, int img, int pos)
    {
        if (Context == null)
            return "no go ;)";

        if (Context.User.Identity.IsAuthenticated)
        {

            // get current page
            EM_USER usr = EM_USER.Current.GetCurrentPage();
            if (usr == null){
                return "Nimaš pravic. Kontaktiraj administratorja.";
            }
            // check permissions
            if (!PERMISSION.User.HasPermissionTypeMODULE(usr.UserId, null, cwpID, PERMISSION.Common.Obj.MOD_GALLERY, PERMISSION.Common.Action.Edit, PERMISSION.Common.Type.Allow)){
                return "Nimaš pravic. Kontaktiraj administratorja.";
            }


//            bool res = IMAGE.Move.MoveImageToPosition(cwpID, img, pos, Context.User.Identity.Name);
            bool res = IMAGE.Move.MoveImageToPosition(cwpID, img, pos, usr.USERNAME );
            if (!res)
                return "Nimaš pravic";
        }

        return "";
    }

    [WebMethod(EnableSession = true)]
    public SM.EM.Ajax.ReturnData DeleteElement(Guid id)
    {
        SM.EM.Ajax.ReturnData ret = new SM.EM.Ajax.ReturnData { Code = SM.EM.Ajax.ReturnCode.Failure, Message = " no go! " };

        if (Context == null)
        {
            ret.Message = "no go ;)";
            return ret;
        }
        if (!Context.User.Identity.IsAuthenticated)
        {
            ret.Message = "Tvoja seja je potekla. Ponovno se moraš prijaviti v sistem.";
            return ret;
        }

        if (Context.User.Identity.IsAuthenticated)
        {
            // get current page
            EM_USER usr = EM_USER.Current.GetCurrentPage();
            if (usr == null){
                ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
                return ret;
            }
            // check permissions
            if (!PERMISSION.User.HasPermissionTypeMODULE(usr.UserId, null, id, PERMISSION.Common.Obj.MOD_ALL, PERMISSION.Common.Action.Delete, PERMISSION.Common.Type.Allow))
            {
                ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
                return ret;
            }


            // get CWP
//            CMS_WEB_PART cwp = CMS_WEB_PART.GetWebpartByID(id, Context.User.Identity.Name);
            CMS_WEB_PART cwp = CMS_WEB_PART.GetWebpartByID(id, usr.USERNAME );
            if (cwp == null)
            {
                ret.Message = "Nimaš pravic ";
                return ret;
            }

            // delete
            cwp.Delete();

            // return
            ret.Message = "success";
            ret.Data = id;
            ret.Code = SM.EM.Ajax.ReturnCode.OK;
        }
        return ret;
    }
#endregion

    [WebMethod(EnableSession=true)]
    public string MoveMenuParent(int id, int pos, string parent)
    {
        int parentID = -1;

        int.TryParse(parent.TrimStart("p".ToCharArray()), out parentID);

        if (id == parentID)
            return "";
        if (parentID < 1)
            return "";

        if (Context == null)
            return "no go ;)";
        string res = "";
        if (Context.User.Identity.IsAuthenticated)
        {
            // get current page
            EM_USER usr = EM_USER.Current.GetCurrentPage();
            if (usr == null)
            {
                return "Nimaš pravic. Kontaktiraj administratorja.";
            }
            // check permissions
            if (!PERMISSION.User.HasPermissionTypeMODULE(usr.UserId, PERMISSION.Common.Obj.MENU, PERMISSION.Common.Action.Move, PERMISSION.Common.Type.Allow))
            {
                return "Nimaš pravic. Kontaktiraj administratorja.";
            }

            if (SM.EM.Security.Permissions.IsAdvancedCms())
                res = MENU.Move.MoveSmapToPositionParent(id, pos, parentID, usr.USERNAME );
            else
                res = MENU.Move.MoveSmapToPosition(id, pos, usr.USERNAME);

            if (res == "")
                return "Nimaš pravic";
        }

        // return refresh url
        // redirect to same page
        return res.Replace("~/", VirtualPathUtility.ToAbsolute("~/"));
    }


    [WebMethod]
    public string MoveMenu(int id, int pos)
    {
        if (Context == null)
            return "no go ;)";
        string res = "";
        if (Context.User.Identity.IsAuthenticated)
        {
            // get current page
            EM_USER usr = EM_USER.Current.GetCurrentPage();
            if (usr == null)
            {
                return "Nimaš pravic. Kontaktiraj administratorja.";
            }
            // check permissions
            if (!PERMISSION.User.HasPermissionTypeMODULE(usr.UserId, PERMISSION.Common.Obj.MENU, PERMISSION.Common.Action.Move, PERMISSION.Common.Type.Allow))
            {
                return "Nimaš pravic. Kontaktiraj administratorja.";
            }


             res = MENU.Move.MoveSmapToPosition(id, pos, usr.USERNAME );
            if (res == "")
                return "Nimaš pravic";
        }

        // return refresh url
        // redirect to same page
        return res.Replace("~/", VirtualPathUtility.ToAbsolute("~/"));
        //return HttpRuntime.AppDomainAppVirtualPath; VirtualPathUtility.ToAbsolute("~");
    }

    [WebMethod(EnableSession = true)]
    public SM.EM.Ajax.ReturnData SaveMenuDesc(int smp, string lang, string title, string content)
    {
        SM.EM.Ajax.ReturnData ret = new SM.EM.Ajax.ReturnData { Code = SM.EM.Ajax.ReturnCode.Failure, Message = " no go! " };

        if (Context == null)
        {
            ret.Message = "no go ;)";
            return ret;
        }
        if (!Context.User.Identity.IsAuthenticated)
        {
            ret.Message = "Tvoja seja je potekla. Ponovno se moraš prijaviti v sistem.";
            return ret;
        }


        if (Context.User.Identity.IsAuthenticated)
        {
            // get current page
            EM_USER usr = EM_USER.Current.GetCurrentPage();
            if (usr == null)
            {
                ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
                return ret;
            }
            // check permissions
            if (!PERMISSION.User.HasPermissionTypeMODULE(usr.UserId, PERMISSION.Common.Obj.MENU, PERMISSION.Common.Action.Edit , PERMISSION.Common.Type.Allow))
            {
                ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
                return ret;
            }


            var sl = MENU.UpdateSitemapLangDesc(smp, lang, title, content);

//            CONTENT con = CONTENT.UpdateBody(cwpID, content, usr.UserId);
            if (sl == null)
            {
                ret.Message = "Nimaš pravic";
                return ret;
            }

            ret.Message = "success";
            ret.Code = SM.EM.Ajax.ReturnCode.OK;
            ret.Data = content;

        }
        return ret;
    }

#region Content
    [WebMethod(EnableSession = true)]
    public SM.EM.Ajax.ReturnData  GetContent(Guid cwpID)
    {

        SM.EM.Ajax.ReturnData ret = new SM.EM.Ajax.ReturnData { Code= SM.EM.Ajax.ReturnCode.Failure , Message= " no go! " };


        if (Context == null)
        {
            ret.Message = "no go ;)";
            return ret;
        }
        if (!Context.User.Identity.IsAuthenticated)
        {
            ret.Message = "Tvoja seja je potekla. Ponovno se moraš prijaviti v sistem.";
            return ret;
        }


        if (Context.User.Identity.IsAuthenticated)
        {

            // get current page
            EM_USER usr = EM_USER.Current.GetCurrentPage();
            if (usr == null)
            {
                ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
//                ret.Message =HttpContext.Current .Items["pname"]  + ";" + HttpContext.Current .Request.QueryString["pname"]; 
                return ret;
            }
            // check permissions
            if (!PERMISSION.User.HasPermissionTypeMODULE(usr.UserId, null, cwpID, PERMISSION.Common.Obj.MOD_CONTENT,  PERMISSION.Common.Action.Edit , PERMISSION.Common.Type.Allow ))
            {
                ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
                return ret;
            }




            CONTENT con = CONTENT.GetContentByCwpID(cwpID, usr.UserId);

            if (con == null)
            {
                ret.Message = "Nimaš pravic";
                return ret;
            }


            ret.Message = "success";
            ret.Code = SM.EM.Ajax.ReturnCode.OK;

            ret.Data = ViewManager.RenderView("~/_ctrl/module/content/_cntContentEditAjax.ascx", Context.Server.HtmlDecode( con.CONT_BODY) );

        }
        return ret;
    }

    [WebMethod(EnableSession = true)]
    public SM.EM.Ajax.ReturnData SaveContent(Guid cwpID, string content)
    {
        SM.EM.Ajax.ReturnData ret = new SM.EM.Ajax.ReturnData { Code = SM.EM.Ajax.ReturnCode.Failure, Message = " no go! " };

        if (Context == null)
        {
            ret.Message = "no go ;)";
            return ret;
        }
        if (!Context.User.Identity.IsAuthenticated)
        {
            ret.Message = "Tvoja seja je potekla. Ponovno se moraš prijaviti v sistem.";
            return ret;
        }


        if (Context.User.Identity.IsAuthenticated)
        {
            // get current page
            EM_USER usr = EM_USER.Current.GetCurrentPage();
            if (usr == null)
            {
                ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
                return ret;
            }
            // check permissions
            if (!PERMISSION.User.HasPermissionTypeMODULE(usr.UserId, null, cwpID, PERMISSION.Common.Obj.MOD_CONTENT, PERMISSION.Common.Action.Edit, PERMISSION.Common.Type.Allow))
            {
                ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
                return ret;
            }




            //EM_USER usr = EM_USER.GetUserByUserName(Context.User.Identity.Name);
            //if (usr == null)
            //{
            //    ret.Message = "no go ";
            //    return ret;
            //}

            CONTENT con = CONTENT.UpdateBody(cwpID, content,  usr.UserId);
            if (con == null)
            {
                ret.Message = "Nimaš pravic";
                return ret;
            }

            ret.Message = "success";
            ret.Code = SM.EM.Ajax.ReturnCode.OK;
            ret.Data = null;

        }
        return ret;
    }
    #endregion


#region Poll

    [WebMethod(EnableSession = true)]
    public SM.EM.Ajax.ReturnData GetPoll(Guid pollID, string lang)
    {

        SM.EM.Ajax.ReturnData ret = new SM.EM.Ajax.ReturnData { Code = SM.EM.Ajax.ReturnCode.Failure, Message = " no go! " };

        if (Context == null)
        {
            ret.Message = "no go ;)";
            return ret;
        }
        if (!Context.User.Identity.IsAuthenticated)
        {
            ret.Message = "Tvoja seja je potekla. Ponovno se moraš prijaviti v sistem.";
            return ret;
        }


        if (Context.User.Identity.IsAuthenticated)
        {
            // get current page
            EM_USER usr = EM_USER.Current.GetCurrentPage();
            if (usr == null)
            {
                ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
                return ret;
            }
            // check permissions
            if (!PERMISSION.User.HasPermissionTypeMODULE(usr.UserId, PERMISSION.Common.Obj.MOD_POLL, PERMISSION.Common.Action.Edit, PERMISSION.Common.Type.Allow))
            {
                ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
                return ret;
            }




            POLL data = null;
            object[] param = new object[]{lang};


            // get poll
            if (pollID != Guid.Empty)
            {
                //EM_USER usr = EM_USER.GetUserByUserName(Context.User.Identity.Name);
                //if (usr == null)
                //{
                //    ret.Message = "no go ";
                //    return ret;
                //}

                data = POLL.GetPollByID(pollID, usr.UserId);
                if (data == null)
                {
                    ret.Message = "Nimaš pravic " ;
                    return ret;
                }

            }

            ret.Message = "success";
            ret.Code = SM.EM.Ajax.ReturnCode.OK;

            ret.Data = ViewManager.RenderView("~/_ctrl/module/poll/_viewPollEdit.ascx", data, param);

        }
        return ret;
    }

    [WebMethod(EnableSession = true)]
    public SM.EM.Ajax.ReturnData SavePoll(Guid pollID, Guid? cwpID, string lang, bool active, string question, Dictionary<string, string> items)
    {
        SM.EM.Ajax.ReturnData ret = new SM.EM.Ajax.ReturnData { Code = SM.EM.Ajax.ReturnCode.Failure, Message = " no go! " };

        if (Context == null)
        {
            ret.Message = "no go ;)";
            return ret;
        }

        if (!Context.User.Identity.IsAuthenticated)
        {
            ret.Message = "Tvoja seja je potekla. Ponovno se moraš prijaviti v sistem.";
            return ret;
        }


        if (Context.User.Identity.IsAuthenticated)
        {
            // get current page
            EM_USER usr = EM_USER.Current.GetCurrentPage();
            if (usr == null)
            {
                ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
                return ret;
            }
            // check permissions
            if (!PERMISSION.User.HasPermissionTypeMODULE(usr.UserId, PERMISSION.Common.Obj.MOD_POLL, PERMISSION.Common.Action.Edit, PERMISSION.Common.Type.Allow))
            {
                ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
                return ret;
            }


            //EM_USER usr = EM_USER.GetUserByUserName(Context.User.Identity.Name);
            //if (usr == null)
            //{
            //    ret.Message = "no go ";
            //    return ret;
            //}


            // save poll
            POLL data = POLL.SavePoll(pollID, cwpID, usr.UserId, active, POLL.Common.BlockMode.IP, 10);
            if (data == null)
            {
                ret.Message = "Nimaš pravic";
                return ret;
            }
            // save lang
            POLL.SavePollLang(data.PollID , lang, question, active);

            if (pollID != Guid.Empty)
            {
                // remove not present choices
                eMenikDataContext db = new eMenikDataContext();
                List<POLL_CHOICE> ch_list = db.POLL_CHOICEs.Where(w => w.PollID == pollID && w.LANG_ID == lang).ToList();
                foreach (POLL_CHOICE item in ch_list)
                {
                    if (!items.ContainsKey(item.PollChoiceID.ToString()))
                        db.POLL_CHOICEs.DeleteOnSubmit(item);
                }
                db.SubmitChanges();
            }
            
            // save choices
            int order = 1;
            foreach (KeyValuePair<string, string> item in items) { 
                Guid choiceID = Guid.Empty;
                // save choice
                if (SM.EM.Helpers.IsGUID(item.Key))
                {
                    choiceID = new Guid(item.Key);
                    
                }
                if (!string.IsNullOrEmpty(item.Value.Trim()) )
                    POLL.SavePollChoice(choiceID, data.PollID, lang, item.Value, order);
                order ++;
            }



            // get poll list to refresh
            if (data.PollID != Guid.Empty)
            {

                ret.Message = "success";
                ret.Code = SM.EM.Ajax.ReturnCode.OK;
                ret.Data = ViewManager.RenderView("~/_ctrl/module/poll/_viewPoll.ascx", data, new object[] { true, lang, cwpID  });
                ret.ID = cwpID.ToString();

                //                ret.Data = ViewManager.RenderView("~/_ctrl/module/poll/_viewPollVote.ascx", POLL.GetPollChoiceList(data.PollID, lang), new object[] { pollID, lang });
                //ret.Data = ViewManager.RenderView("~/_ctrl/module/poll/_viewPollList.ascx", _data, param);
            }
            
        }
        return ret;
    }

    [WebMethod(EnableSession = true)]
    public SM.EM.Ajax.ReturnData SaveSettingsPoll(Guid cwpID, string title, bool active)
    {
        SM.EM.Ajax.ReturnData ret = new SM.EM.Ajax.ReturnData { Code = SM.EM.Ajax.ReturnCode.Failure, Message = " no go! " };

        if (Context == null)
        {
            ret.Message = "no go ;)";
            return ret;
        }

        if (!Context.User.Identity.IsAuthenticated)
        {
            ret.Message = "Tvoja seja je potekla. Ponovno se moraš prijaviti v sistem.";
            return ret;
        }
        // get current page
        EM_USER usr = EM_USER.Current.GetCurrentPage();
        if (usr == null)
        {
            ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
            return ret;
        }
        // check permissions
        if (!PERMISSION.User.HasPermissionTypeMODULE(usr.UserId, null, cwpID, PERMISSION.Common.Obj.MOD_POLL, PERMISSION.Common.Action.Settings, PERMISSION.Common.Type.Allow))
        {
            ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
            return ret;
        }




        CMS_WEB_PART data = CMS_WEB_PART.SaveWebpartSettings(cwpID, usr.USERNAME , title, active, -1, -1);
        if (data == null)
        {
            ret.Message = "Nimaš pravic ";
            return ret;
        }

        ret.Message = "success";
        ret.Code = SM.EM.Ajax.ReturnCode.OK;
        ret.Data = ViewManager.RenderView("~/_ctrl/module/poll/_viewPollCWP.ascx", data, new object[] { true });
        ret.ID = cwpID.ToString();

        return ret;
    }

    [WebMethod(EnableSession = true)]
    public SM.EM.Ajax.ReturnData GetPollAdd(Guid cwpID)
    {
        SM.EM.Ajax.ReturnData ret = new SM.EM.Ajax.ReturnData { Code = SM.EM.Ajax.ReturnCode.Failure, Message = " no go! " };
        if (Context == null)
        {
            ret.Message = "no go ;)";
            return ret;
        }
        if (!Context.User.Identity.IsAuthenticated)
        {
            ret.Message = "Tvoja seja je potekla. Ponovno se moraš prijaviti v sistem.";
            return ret;
        }

        // get current page
        EM_USER usr = EM_USER.Current.GetCurrentPage();
        if (usr == null)
        {
            ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
            return ret;
        }
        // check permissions
        if (!PERMISSION.User.HasPermissionTypeMODULE(usr.UserId, null, cwpID, PERMISSION.Common.Obj.MOD_POLL, PERMISSION.Common.Action.Edit, PERMISSION.Common.Type.Allow))
        {
            ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
            return ret;
        }

        string view = "";
        CMS_WEB_PART data = CMS_WEB_PART.GetWebpartByID(cwpID, usr.USERNAME );
        if (data == null)
        {
            ret.Message = "Nimaš pravic ";
            return ret;
        }

        ret.Message = "success";
        ret.Code = SM.EM.Ajax.ReturnCode.OK;
        ret.Data = ViewManager.RenderView("~/_ctrl/module/poll/_viewPollAdd.ascx", data);
        return ret;
    }

#endregion



    [WebMethod(EnableSession = true)]
    public SM.EM.Ajax.ReturnData GetSettings(Guid cwpID){
        SM.EM.Ajax.ReturnData ret = new SM.EM.Ajax.ReturnData { Code = SM.EM.Ajax.ReturnCode.Failure, Message = " no go! " };
        if (Context == null )
        {
            ret.Message = "no go ;)";
            return ret;
        }
        if (!Context.User.Identity.IsAuthenticated)
        {
            ret.Message = "Tvoja seja je potekla. Ponovno se moraš prijaviti v sistem.";
            return ret;
        }

        // get current page
        EM_USER usr = EM_USER.Current.GetCurrentPage();
        if (usr == null)
        {
            ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
            return ret;
        }
        // check permissions
        if (!PERMISSION.User.HasPermissionTypeMODULE(usr.UserId, null, cwpID, PERMISSION.Common.Obj.MOD_ALL, PERMISSION.Common.Action.Settings, PERMISSION.Common.Type.Allow))
        {
            ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
            return ret;
        }

        string view = "";
        CMS_WEB_PART data = CMS_WEB_PART.GetWebpartByID(cwpID, usr.USERNAME );
        if (data == null)
        {
            ret.Message = "Nimaš pravic ";
            return ret;
        }
        object[] parameters = new object[] { };

        switch (data.MOD_ID )
	    {
            case MODULE.Common.PollModID :
                view = "~/_ctrl/module/poll/_viewPollSettings.ascx";
                break;

            case MODULE.Common.GalleryModID:
                view = "~/_ctrl/module/gallery/_viewGallerySettings.ascx";

                IEnumerable<vw_GalleryDesc> list = null ;
                if (data.ID_INT != null)
                    list = GALLERY.GetGalleryDescsByIDEmenik(data.ID_INT.Value, usr.USERNAME );
                parameters = new object[] {true, null, list};
                break;

            case MODULE.Common.ArticleModID:
                view = "~/_ctrl/module/article/_viewArticleSettings.ascx";

                parameters = new object[] { true, null };
                break;
	    }

        if (string.IsNullOrEmpty(view ))
        {
            ret.Message = "View ne obstaja";
            return ret;       
        }

        ret.Message = "success";
        ret.Code = SM.EM.Ajax.ReturnCode.OK;
        ret.Data = ViewManager.RenderView(view, data, parameters);

        
        return ret;
    }



#region Image
    [WebMethod(EnableSession = true)]
    public SM.EM.Ajax.ReturnData GetImageEdit(int imgID, int typeID)
    {
        SM.EM.Ajax.ReturnData ret = new SM.EM.Ajax.ReturnData { Code = SM.EM.Ajax.ReturnCode.Failure, Message = " no go! " };
        if (Context == null){
            ret.Message = "no go ;)";
            return ret;
        }
        if (!Context.User.Identity.IsAuthenticated){
            ret.Message = "Tvoja seja je potekla. Ponovno se moraš prijaviti v sistem.";
            return ret;
        }

        // get current page
        EM_USER usr = EM_USER.Current.GetCurrentPage();
        if (usr == null)
        {
            ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
            return ret;
        }
        // check permissions
        if (!PERMISSION.User.HasPermissionTypeMODULE(usr.UserId, PERMISSION.Common.Obj.MOD_GALLERY, PERMISSION.Common.Action.Edit, PERMISSION.Common.Type.Allow))
        {
            ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
            return ret;
        }

        // todo: possible security risk
//        vw_Image data = IMAGE.GetvwImageByID(imgID, typeID, usr.USERNAME);
        vw_Image data = IMAGE.GetvwImageByID(imgID, typeID);
        if (data == null)
        {
            ret.Message = "Nimaš pravic ";
            return ret;
        }

        ret.Message = "success";
        ret.Code = SM.EM.Ajax.ReturnCode.OK;
        ret.Data = ViewManager.RenderView("~/_ctrl/module/gallery/_viewImageEdit.ascx", data, new object[]{usr });
        return ret;
    }

    [WebMethod(EnableSession = true)]
    public SM.EM.Ajax.ReturnData GetImageCustomCrop(int imgID, int typeID)
    {
        SM.EM.Ajax.ReturnData ret = new SM.EM.Ajax.ReturnData { Code = SM.EM.Ajax.ReturnCode.Failure, Message = " no go! " };
        if (Context == null)
        {
            ret.Message = "no go ;)";
            return ret;
        }
        if (!Context.User.Identity.IsAuthenticated)
        {
            ret.Message = "Tvoja seja je potekla. Ponovno se moraš prijaviti v sistem.";
            return ret;
        }

        // get current page
        EM_USER usr = EM_USER.Current.GetCurrentPage();
        if (usr == null)
        {
            ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
            return ret;
        }
        // check permissions
        if (!PERMISSION.User.HasPermissionTypeMODULE(usr.UserId, PERMISSION.Common.Obj.MOD_GALLERY, PERMISSION.Common.Action.Edit, PERMISSION.Common.Type.Allow))
        {
            ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
            return ret;
        }

        // todo: improve security: now all editors have global image edit permissions
//        IMAGE_ORIG data = IMAGE.GetImageByID(imgID, usr.USERNAME);
        IMAGE_ORIG data = IMAGE.GetImageByID(imgID);
        if (data == null)
        {
            ret.Message = "Nimaš pravic ";
            return ret;
        }
        IMAGE_TYPE t = IMAGE_TYPE.GetImageTypeByID(typeID);
        if (t == null)
        if (data == null)
        {
            ret.Message = "Napaka.";
            return ret;
        }
        IMAGE img = IMAGE.GetImageByID(imgID, typeID);

        ret.Message = "success";
        ret.Code = SM.EM.Ajax.ReturnCode.OK;
        ret.Data = ViewManager.RenderView("~/_ctrl/image/_viewUserCrop.ascx", data, new object[] { t, img.IMG_URL.TrimStart("~".ToCharArray())   });
        return ret;
    }



    [WebMethod(EnableSession = true)]
    public SM.EM.Ajax.ReturnData SaveImage(int imgID, Guid? cwpID, string author, bool active, List<LangTitleDesc> listLangDesc)
    {
        SM.EM.Ajax.ReturnData ret = new SM.EM.Ajax.ReturnData { Code = SM.EM.Ajax.ReturnCode.Failure, Message = " no go! " };

        if (Context == null)
        {
            ret.Message = "no go ;)";
            return ret;
        }

        if (!Context.User.Identity.IsAuthenticated)
        {
            ret.Message = "Tvoja seja je potekla. Ponovno se moraš prijaviti v sistem.";
            return ret;
        }

        // get current page
        EM_USER usr = EM_USER.Current.GetCurrentPage();
        if (usr == null)
        {
            ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
            return ret;
        }
        // check permissions
        if (!PERMISSION.User.HasPermissionTypeMODULE(usr.UserId, PERMISSION.Common.Obj.MOD_GALLERY, PERMISSION.Common.Action.Edit, PERMISSION.Common.Type.Allow))
        {
            ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
            return ret;
        }

        // save IMAGE
        IMAGE_ORIG  img = IMAGE.UpdateImage(imgID,author, active, usr.USERNAME );
        if (img == null)
        {
            ret.Message = "Nimaš pravic ";
            return ret;
        }

        // save IMAGE desc
        if (listLangDesc != null && listLangDesc.Count > 0 )
        {

            foreach (LangTitleDesc item in listLangDesc)
            {
                if (string.IsNullOrEmpty(item.Lang.Trim()))
                    continue;
                // save desc
                IMAGE.SaveImageDesc(img.IMG_ID, item.Lang, item.Title, item.Desc);
            }

        }

        ret.Message = "success";
        ret.Code = SM.EM.Ajax.ReturnCode.OK;

        // return new image list
        if (cwpID != null && cwpID != Guid.Empty)
        {
            var cwp_gal = CMS_WEB_PART.GetCwpGalleryByID(cwpID.Value);
            var img_list = GALLERY.GetImageListByCwpPaged(cwpID.Value, LANGUAGE.GetCurrentLang(), true, 1, 100);
            var data = CMS_WEB_PART.GetWebpartByID(cwpID.Value );
            ret.Data = ViewManager.RenderView("~/_ctrl/module/gallery/_viewGalleryImageList.ascx", data, new object[] { true, cwp_gal, img_list });
            ret.ID = cwpID.ToString();
        }
        else
        {
            ret.Code = SM.EM.Ajax.ReturnCode.OK_refresh;
        }

        return ret;
    }

    [WebMethod(EnableSession = true)]
    public SM.EM.Ajax.ReturnData SaveSettingsGallery(Guid cwpID, string title, bool active, int pageSize, int tType, List<LangTitleDesc> listLangDesc)
    {
        SM.EM.Ajax.ReturnData ret = new SM.EM.Ajax.ReturnData { Code = SM.EM.Ajax.ReturnCode.Failure, Message = " no go! " };

        if (Context == null)
        {
            ret.Message = "no go ;)";
            return ret;
        }

        if (!Context.User.Identity.IsAuthenticated)
        {
            ret.Message = "Tvoja seja je potekla. Ponovno se moraš prijaviti v sistem.";
            return ret;
        }
        // get current page
        EM_USER usr = EM_USER.Current.GetCurrentPage();
        if (usr == null)
        {
            ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
            return ret;
        }
        // check permissions
        if (!PERMISSION.User.HasPermissionTypeMODULE(usr.UserId, null, cwpID , PERMISSION.Common.Obj.MOD_GALLERY, PERMISSION.Common.Action.Settings, PERMISSION.Common.Type.Allow))
        {
            ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
            return ret;
        }



        int pSize = pageSize;
        if (pSize < 0)
            pSize = 0;

        // save CWP
        CMS_WEB_PART data = CMS_WEB_PART.SaveWebpartSettings(cwpID, usr.USERNAME , title, active, pSize, -1);
        if (data == null)
        {
            ret.Message = "Nimaš pravic ";
            return ret;
        }

        // save CWP gallery
        CMS_WEB_PART.UpdateCwpGallery(data.CWP_ID, data.ID_INT, tType, -1);

        // save GALLERY desc
        if (listLangDesc != null && listLangDesc.Count > 0 && data.ID_INT != null)
        {

            foreach (LangTitleDesc item in listLangDesc)
            {
                if (string.IsNullOrEmpty(item.Lang.Trim()))
                    continue;
                // save desc
                GALLERY.SaveGalleryDesc(data.ID_INT.Value, item.Lang, item.Title, item.Desc);
            }

        }

        // return new image list
        var cwp_gal = CMS_WEB_PART.GetCwpGalleryByID(data.CWP_ID);
        var img_list = GALLERY.GetImageListByCwpPaged(data.CWP_ID, LANGUAGE.GetCurrentLang(), true, 1, pSize);
        ret.Message = "success";
        ret.Code = SM.EM.Ajax.ReturnCode.OK;
        ret.Data = ViewManager.RenderView("~/_ctrl/module/gallery/_viewGalleryImageList.ascx", data, new object[] { true, cwp_gal, img_list });
        ret.ID = cwpID.ToString();

        return ret;
    }


    [WebMethod(EnableSession = true)]
    public SM.EM.Ajax.ReturnData DeleteImage(int imgID, Guid? cwpID)
    {
        SM.EM.Ajax.ReturnData ret = new SM.EM.Ajax.ReturnData { Code = SM.EM.Ajax.ReturnCode.Failure, Message = " no go! " };

        if (Context == null){
            ret.Message = "no go ;)";
            return ret;
        }

        if (!Context.User.Identity.IsAuthenticated){
            ret.Message = "Tvoja seja je potekla. Ponovno se moraš prijaviti v sistem.";
            return ret;
        }

        // get current page
        EM_USER usr = EM_USER.Current.GetCurrentPage();
        if (usr == null)
        {
            ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
            return ret;
        }
        // check permissions
        if (!PERMISSION.User.HasPermissionTypeMODULE(usr.UserId, PERMISSION.Common.Obj.MOD_GALLERY, PERMISSION.Common.Action.Delete, PERMISSION.Common.Type.Allow))
        {
            ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
            return ret;
        }

        // todo: possible security risk
//        IMAGE_ORIG img = IMAGE.GetImageByID(imgID, usr.USERNAME);
        IMAGE_ORIG img = IMAGE.GetImageByID(imgID);
        if (img == null)
        {
            ret.Message = "Nimaš pravic ";
            return ret;
        }

        // delete IMAGE
        if (!IMAGE.DeleteImage(imgID))
        {
            ret.Message = "Napaka pri brisanju slike";
            return ret;        
        }

        ret.Message = "success";
        ret.Code = SM.EM.Ajax.ReturnCode.OK;

        // return new image list
        if (cwpID != null && cwpID != Guid.Empty)
        {
            var cwp_gal = CMS_WEB_PART.GetCwpGalleryByID(cwpID.Value);
            var img_list = GALLERY.GetImageListByCwpPaged(cwpID.Value, LANGUAGE.GetCurrentLang(), true, 1, 100);
            var data = CMS_WEB_PART.GetWebpartByID(cwpID.Value);
            ret.Data = ViewManager.RenderView("~/_ctrl/module/gallery/_viewGalleryImageList.ascx", data, new object[] { true, cwp_gal, img_list });
            ret.ID = cwpID.ToString();
        }
        else
        {
            ret.Code = SM.EM.Ajax.ReturnCode.OK_refresh;
        }

        return ret;
    }

#endregion


#region Product
    [WebMethod(EnableSession = true)]
    public SM.EM.Ajax.ReturnData SaveProductNew(string name, string code, int smp, string lang, string action)
    {
        SM.EM.Ajax.ReturnData ret = new SM.EM.Ajax.ReturnData { Code = SM.EM.Ajax.ReturnCode.Failure, Message = " no go! " };

        if (Context == null)
        {
            ret.Message = "no go ;)";
            return ret;
        }

        if (!Context.User.Identity.IsAuthenticated)
        {
            ret.Message = "Tvoja seja je potekla. Ponovno se moraš prijaviti v sistem.";
            return ret;
        }

        // get current page
        EM_USER usr = EM_USER.Current.GetCurrentPage();
        if (usr == null)
        {
            ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
            return ret;
        }
        // check permissions
        if (!PERMISSION.User.HasPermissionTypeMODULE(usr.UserId, null, null,   PERMISSION.Common.Obj.PRODUCT, PERMISSION.Common.Action.New, PERMISSION.Common.Type.Allow))
        {
            ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
            return ret;
        }


        // get user
        //EM_USER usr = EM_USER.GetUserByUserName(Context.User.Identity.Name);
        //if (usr == null)
        //{
        //    ret.Message = "no go ";
        //    return ret;
        //}


        // save PRODUCT
        ProductRep productRep = new ProductRep();

        PRODUCT prod = new PRODUCT();
        prod.CODE = code;
        prod.PRODUCT_ID = Guid.NewGuid();
        prod.PageId = usr.UserId;
        prod.SMAP_ID = smp;

        PRODUCT_DESC pdesc = new PRODUCT_DESC();
        pdesc.PRODUCT_ID = prod.PRODUCT_ID;
        pdesc.LANG_ID = lang;
        pdesc.NAME = name;

        // validate
        List<RuleViolation> errlist = new List<RuleViolation>();
        bool valid = true;
        if (!prod.IsValid){ errlist = prod.GetRuleViolations().ToList();
        valid = false;

        }if (!pdesc.IsValid)
        {
            errlist.AddRange(pdesc.GetRuleViolations().ToList());
            valid = false;
        }
        if (!valid) {
            //ret.Message = RuleViolation.RenderError(errlist, " <br/>");
            ret.Message = ViewManager.RenderView("~/_ctrl/module/_err/_viewErrValidationListPopup.ascx", errlist, new object[] { });
            return ret;
        }

        // all ok, save
        productRep.Add(prod);
        productRep.AddDesc(pdesc);
        productRep.Save();


        ret.Message = "success";
        ret.Code = SM.EM.Ajax.ReturnCode.OK;

        if (action == "edit") { 
            //return redirect url
            ret.Data = SM.BLL.Common.ResolveUrl( SM.EM.Rewrite.EmenikUserProductDetailsUrl(prod, pdesc));
        }
        //else// refresh list
        //{   
        //    var data = productRep.GetProductDescList(smp, lang, null, prod.PageId );
        //    ret.Data = ViewManager.RenderView("~/_ctrl/module/product/_viewProductList.ascx", data, new object[]{} );

        //}


        return ret;
    }

    [WebMethod(EnableSession = true)]
    public SM.EM.Ajax.ReturnData SaveProductEdit(Guid prodID, string name, string code, decimal price, float discount, int stock, bool active, List<Guid> groupAtt, Guid manu, int smp, List<int> smapAdd, bool action, bool sale, string lang, string imp, int prodType, double tax, int? mainImgID)
    {
        SM.EM.Ajax.ReturnData ret = new SM.EM.Ajax.ReturnData { Code = SM.EM.Ajax.ReturnCode.Failure, Message = " no go! " };

        if (Context == null)
        {
            ret.Message = "no go ;)";
            return ret;
        }

        if (!Context.User.Identity.IsAuthenticated)
        {
            ret.Message = "Tvoja seja je potekla. Ponovno se moraš prijaviti v sistem.";
            return ret;
        }

        // get current page
        EM_USER usr = EM_USER.Current.GetCurrentPage();
        if (usr == null)
        {
            ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
            return ret;
        }
        // check permissions
        if (!PERMISSION.User.HasPermissionTypePAGE(usr.UserId, PERMISSION.Common.Obj.PRODUCT, PERMISSION.Common.Action.Edit, PERMISSION.Common.Type.Allow))
        {
            ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
            return ret;
        }


        //// get user
        //EM_USER usr = EM_USER.GetUserByUserName(Context.User.Identity.Name);
        //if (usr == null)
        //{
        //    ret.Message = "no go ";
        //    return ret;
        //}


        // save PRODUCT
        ProductRep productRep = new ProductRep();

        PRODUCT prod = productRep.GetProductByID(prodID);
        if (prod == null || prod.PageId != usr.UserId)

        {
            ret.Message = "Izdelek ne obstaja";
            return ret;
        }
        prod.CODE = code;
        if (PRODUCT.Common.ProductPriceHasTax)
            prod.PRICE = price;
        else
            throw new NotImplementedException();

        prod.DISCOUNT_PERCENT = discount;
        prod.UNITS_IN_STOCK = stock;
        prod.SMAP_ID = smp;
        prod.ACTIVE = active;
        prod.CAT_ACTION = action;
        prod.CAT_SALE = sale ;
        prod.DATE_MODIFIED = DateTime.Now;

        prod.TAX_RATE = tax;


        if (manu == Guid.Empty)
            prod.ManufacturerID = null;
        else
            prod.ManufacturerID = manu;

        int importance = 0;
        int.TryParse(imp, out importance);
        prod.IMPORTANCE = importance;

        //if (mainImgID != null)
            prod.MainImgID = mainImgID;

        // eko
            if (sale) {
                prod.ADDED_DATE = DateTime.Now;
            }


        // desc
        PRODUCT_DESC pdesc = productRep.GetProductDescByID(prodID, lang);
        if (pdesc == null)
        {
            pdesc = new PRODUCT_DESC { PRODUCT_ID = prod.PRODUCT_ID, LANG_ID = lang };
            productRep.AddDesc(pdesc);
        }
        pdesc.NAME = name;

        // attributes
        productRep.DeleteProductGroupAttr(prod.PRODUCT_ID);
        var pType = prodType;

        foreach (Guid item in groupAtt) { 
            PRODUCT_GROUP_ATTRIBUTE  pga = new PRODUCT_GROUP_ATTRIBUTE{ PRODUCT_ID= prod.PRODUCT_ID, GroupAttID = item  };
            productRep.AddProductGroupAttribute(pga);        

            // if group attribute is range, insert also PRODUCT_ATTRIBUTE for GROUP with default values
            GROUP_ATTRIBUTE gra = productRep.GetGroupAttributeByID(item);
            if (gra != null) {
                if (gra.GrType == ATTRIBUTE.Common.GrType.RANGE) {
                    // get attributes which are missing
                    var attrList = productRep.GetAttributesByproductInvert(item, prod.PRODUCT_ID );
                    foreach (var itm in attrList ){
                        productRep.AddProductAttribute (new PRODUCT_ATTRIBUTE { AttributeID = itm.AttributeID, PRODUCT_ID = prod.PRODUCT_ID, ValMin = itm.ValMin, ValMax = itm.ValMax, ValDefault = itm.ValDefault   }); 
                    }
                }
                else if (gra.GrType == ATTRIBUTE.Common.GrType.RBL_PRICE )
                {
                    pType  = PRODUCT.ProductType.PRICE_DEPENDS_ON_ATRIBUTE;
                }
            }

        }
        // product type (check if price attribute is set)
        prod.PRODUCT_TYPE = pType ;


        // additional smaps
        productRep.DeleteProductSmaps(prod.PRODUCT_ID);
        foreach (int  item in smapAdd)
        {
            PRODUCT_SMAP pga = new PRODUCT_SMAP { PRODUCT_ID = prod.PRODUCT_ID, SMAP_ID = item };
            productRep.AddProductSmap(pga);
        }
        

        // validate
        List<RuleViolation> errlist = new List<RuleViolation>();
        bool valid = true;
        if (!prod.IsValid)
        {
            errlist = prod.GetRuleViolations().ToList();
            valid = false;

        } if (!pdesc.IsValid)
        {
            errlist.AddRange(pdesc.GetRuleViolations().ToList());
            valid = false;
        }
        if (!valid)
        {
            //ret.Message = RuleViolation.RenderError(errlist, " <br/>");
            ret.Message = ViewManager.RenderView("~/_ctrl/module/_err/_viewErrValidationListPopup.ascx", errlist, new object[] { });
            return ret;
        }


        // all ok, save
        if (!productRep.Save()) {
            ret.Message = "Napaka pri shranjevanju";
            return ret;
        }

        ret.Data = ViewManager.RenderView("~/_ctrl/module/product/_viewProductDetail.ascx", productRep.GetProductVWByID(prodID, lang, usr.UserId), new object[] { true });
            


        ret.Message = "success";
        ret.Code = SM.EM.Ajax.ReturnCode.OK;

        return ret;
    }

    [WebMethod(EnableSession = true)]
    public SM.EM.Ajax.ReturnData SaveProductDesc(Guid prodID, string lang, List<LangTitleAbstrDesc> listLangDesc, int scope)
    {
        SM.EM.Ajax.ReturnData ret = new SM.EM.Ajax.ReturnData { Code = SM.EM.Ajax.ReturnCode.Failure, Message = " no go! " };

        if (Context == null)
        {
            ret.Message = "no go ;)";
            return ret;
        }

        if (!Context.User.Identity.IsAuthenticated)
        {
            ret.Message = "Tvoja seja je potekla. Ponovno se moraš prijaviti v sistem.";
            return ret;
        }
        // get current page
        EM_USER usr = EM_USER.Current.GetCurrentPage();
        if (usr == null)
        {
            ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
            return ret;
        }
        // check permissions
        if (!PERMISSION.User.HasPermissionTypePAGE(usr.UserId, PERMISSION.Common.Obj.PRODUCT, PERMISSION.Common.Action.Edit, PERMISSION.Common.Type.Allow))
        {
            ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
            return ret;
        }



        //// get user
        //EM_USER usr = EM_USER.GetUserByUserName(Context.User.Identity.Name);
        //if (usr == null)
        //{
        //    ret.Message = "no go ";
        //    return ret;
        //}
        // check if owner of product
        ProductRep productRep = new ProductRep();
        PRODUCT prod = productRep.GetProductByID(prodID);
        if (prod == null || prod.PageId != usr.UserId)
        {
            ret.Message = "Izdelek ne obstaja";
            return ret;
        }

        // save  desc
        ProductRep prep = new ProductRep();
        // and validate
        List<RuleViolation> errlist = new List<RuleViolation>();
        bool valid = true;
        if (listLangDesc != null && listLangDesc.Count > 0)
        {

            foreach (LangTitleAbstrDesc item in listLangDesc)
            {
                if (string.IsNullOrEmpty(item.Lang.Trim()))
                    continue;
                // save desc

                PRODUCT_DESC pdesc = null;
                // update all
                string tit = item.Title;
                if (string.IsNullOrWhiteSpace(item.Title))
                    tit = prod.CODE + " - " + item.Lang ;

                if (scope == 1)
                {


                    pdesc = prep.UpdateProductDesc(prodID, item.Lang, tit, item.Desc, item.Abstract);
                }
                //update only long desc
                else if (scope == 2)
                {
                    pdesc = prep.UpdateProductDescLong(prodID, item.Lang, item.Desc);
                }
                //update long desc and title
                else if (scope == 3)
                {
                    pdesc = prep.UpdateProductDesc(prodID, item.Lang, tit, item.Desc, item.Abstract ?? "");
                }
                if (!pdesc.IsValid)
                {
                    errlist.AddRange(pdesc.GetRuleViolations().ToList());
                    valid = false;
                }
            }
        }
        if (!valid)
        {
            ret.Message = ViewManager.RenderView("~/_ctrl/module/_err/_viewErrValidationListPopup.ascx", errlist, new object[] { });
            return ret;
        }
        // save
        if (!prep.Save())
        {
            ret.Message = "Napaka pri shranjevanju";
            return ret;
        }      

        ret.Message = "success";
        ret.Code = SM.EM.Ajax.ReturnCode.OK;

        // return product desc
        ret.Data = ViewManager.RenderView("~/_ctrl/module/product/_viewProductDetail.ascx", prep.GetProductVWByID(prodID, lang, usr.UserId ), new object[] { true });
       

        return ret;
    }

    [WebMethod(EnableSession = true)]
    public SM.EM.Ajax.ReturnData SaveProductAttributes(Guid prodID, Guid grID, List<Guid> groupAtt, string lang)
    {
        SM.EM.Ajax.ReturnData ret = new SM.EM.Ajax.ReturnData { Code = SM.EM.Ajax.ReturnCode.Failure, Message = " no go! " };

        if (Context == null)
        {
            ret.Message = "no go ;)";
            return ret;
        }

        if (!Context.User.Identity.IsAuthenticated)
        {
            ret.Message = "Tvoja seja je potekla. Ponovno se moraš prijaviti v sistem.";
            return ret;
        }

        // get current page
        EM_USER usr = EM_USER.Current.GetCurrentPage();
        if (usr == null)
        {
            ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
            return ret;
        }
        // check permissions
        if (!PERMISSION.User.HasPermissionTypePAGE(usr.UserId, PERMISSION.Common.Obj.PRODUCT, PERMISSION.Common.Action.Edit, PERMISSION.Common.Type.Allow))
        {
            ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
            return ret;
        }

        //// get user
        //EM_USER usr = EM_USER.GetUserByUserName(Context.User.Identity.Name);
        //if (usr == null)
        //{
        //    ret.Message = "no go ";
        //    return ret;
        //}


        // save PRODUCT
        ProductRep productRep = new ProductRep();

        vw_Product  prod = productRep.GetProductVWByID(prodID , lang, usr.UserId);
        if (prod == null || prod.PageId != usr.UserId)
        {
            ret.Message = "Izdelek ne obstaja";
            return ret;
        }





        // attributes
        productRep.DeleteProductAttributesByGroup(prodID , grID );
        foreach (Guid item in groupAtt)
        {
            PRODUCT_ATTRIBUTE pga = new PRODUCT_ATTRIBUTE { PRODUCT_ID = prod.PRODUCT_ID, AttributeID  = item };
            productRep.AddProductAttribute(pga);
        }

        // all ok, save
        if (!productRep.Save())
        {
            ret.Message = "Napaka pri shranjevanju";
            return ret;
        }

        ret.Data = ViewManager.RenderView("~/_ctrl/module/product/_viewProductDetail.ascx", prod, new object[] { true });


        ret.Message = "success";
        ret.Code = SM.EM.Ajax.ReturnCode.OK;

        return ret;
    }

    [WebMethod(EnableSession = true)]
    public SM.EM.Ajax.ReturnData SaveProductAttributesRange(Guid prodID, Guid grID, List<ProductRep.Data .AttributeItem> attrList, string lang)
    {
        SM.EM.Ajax.ReturnData ret = new SM.EM.Ajax.ReturnData { Code = SM.EM.Ajax.ReturnCode.Failure, Message = " no go! " };

        if (Context == null)
        {
            ret.Message = "no go ;)";
            return ret;
        }

        if (!Context.User.Identity.IsAuthenticated)
        {
            ret.Message = "Tvoja seja je potekla. Ponovno se moraš prijaviti v sistem.";
            return ret;
        }

        // get current page
        EM_USER usr = EM_USER.Current.GetCurrentPage();
        if (usr == null)
        {
            ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
            return ret;
        }
        // check permissions
        if (!PERMISSION.User.HasPermissionTypePAGE(usr.UserId, PERMISSION.Common.Obj.PRODUCT, PERMISSION.Common.Action.Edit, PERMISSION.Common.Type.Allow))
        {
            ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
            return ret;
        }

        // save PRODUCT
        ProductRep productRep = new ProductRep();

        vw_Product prod = productRep.GetProductVWByID(prodID, lang, usr.UserId);
        if (prod == null || prod.PageId != usr.UserId)
        {
            ret.Message = "Izdelek ne obstaja";
            return ret;
        }

        // attributes
        productRep.DeleteProductAttributesByGroup(prodID, grID);
        foreach (ProductRep.Data.AttributeItem attr in attrList)
        {
            PRODUCT_ATTRIBUTE pga = new PRODUCT_ATTRIBUTE { PRODUCT_ID = prod.PRODUCT_ID, AttributeID = attr.Value   };
            productRep.AddProductAttribute(pga);
            pga.ValMin = attr.ValMin;
            pga.ValMax = attr.ValMax;
            pga.ValDefault = attr.ValDefault;
        }

        // all ok, save
        if (!productRep.Save())
        {
            ret.Message = "Napaka pri shranjevanju";
            return ret;
        }

        ret.Data = ViewManager.RenderView("~/_ctrl/module/product/_viewProductDetail.ascx", prod, new object[] { true });


        ret.Message = "success";
        ret.Code = SM.EM.Ajax.ReturnCode.OK;

        return ret;
    }

    [WebMethod(EnableSession = true)]
    public SM.EM.Ajax.ReturnData SaveProductAttributesPrice(Guid prodID, Guid grID, List<ProductRep.Data.AttributeItem> attrList, string lang)
    {
        SM.EM.Ajax.ReturnData ret = new SM.EM.Ajax.ReturnData { Code = SM.EM.Ajax.ReturnCode.Failure, Message = " no go! " };

        if (Context == null)
        {
            ret.Message = "no go ;)";
            return ret;
        }

        if (!Context.User.Identity.IsAuthenticated)
        {
            ret.Message = "Tvoja seja je potekla. Ponovno se moraš prijaviti v sistem.";
            return ret;
        }

        // get current page
        EM_USER usr = EM_USER.Current.GetCurrentPage();
        if (usr == null)
        {
            ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
            return ret;
        }
        // check permissions
        if (!PERMISSION.User.HasPermissionTypePAGE(usr.UserId, PERMISSION.Common.Obj.PRODUCT, PERMISSION.Common.Action.Edit, PERMISSION.Common.Type.Allow))
        {
            ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
            return ret;
        }

        // save PRODUCT
        ProductRep productRep = new ProductRep();

        vw_Product prod = productRep.GetProductVWByID(prodID, lang, usr.UserId);
        if (prod == null || prod.PageId != usr.UserId)
        {
            ret.Message = "Izdelek ne obstaja";
            return ret;
        }

        // attributes
        var count =  productRep.DeleteProductAttributesByGroup(prodID, grID);
        foreach (ProductRep.Data.AttributeItem attr in attrList)
        {
            PRODUCT_ATTRIBUTE pga = new PRODUCT_ATTRIBUTE { PRODUCT_ID = prod.PRODUCT_ID, AttributeID = attr.Value };
            productRep.AddProductAttribute(pga);
            //pga.ValMin = attr.ValMin;
            //pga.ValMax = attr.ValMax;
            //pga.ValDefault = attr.ValDefault;
            pga.Price = attr.Price;
            pga.Code = attr.Code;
            pga.UnitsInStock  = attr.Stock;

            // desc
            PRODUCT_ATTRIBUTE_DESC pad = productRep.GetProductAttributeDescByID(prodID, attr.Value, lang);
            if (pad == null) {
                pad = new PRODUCT_ATTRIBUTE_DESC { PRODUCT_ID = prod.PRODUCT_ID, AttributeID = attr.Value , LANG_ID = lang };
                productRep.AddProductAttributeDesc(pad);
            }
            pad.Title = attr.Title;
            pad.Desc = attr.Desc;
            
        }



        // all ok, save
        if (!productRep.Save())
        {
            ret.Message = "Napaka pri shranjevanju";
            return ret;
        }

        // update product main price
        PRODUCT p = productRep.GetProductByID(prodID);
        p.PRICE = attrList.Min(w => w.Price) ?? 0 ;

        // product range MAX price
        p.PriceRangeMax = attrList.Max(w => w.Price) ?? 0;

        if (attrList.Count > count)
        { // update date added (new item was added)
            p.ADDED_DATE = DateTime.Now;
        }


        productRep.Save();


        ret.Data = ViewManager.RenderView("~/_ctrl/module/product/_viewProductDetail.ascx", prod, new object[] { true });


        ret.Message = "success";
        ret.Code = SM.EM.Ajax.ReturnCode.OK;

        return ret;
    }


    [WebMethod(EnableSession = true)]
    public SM.EM.Ajax.ReturnData SaveProductAttributeDesc(Guid pid, Guid attr, string lang, string title, string desc)
    {
        SM.EM.Ajax.ReturnData ret = new SM.EM.Ajax.ReturnData { Code = SM.EM.Ajax.ReturnCode.Failure, Message = " no go! " };

        if (Context == null)
        {
            ret.Message = "no go ;)";
            return ret;
        }
        if (!Context.User.Identity.IsAuthenticated)
        {
            ret.Message = "Tvoja seja je potekla. Ponovno se moraš prijaviti v sistem.";
            return ret;
        }


        if (Context.User.Identity.IsAuthenticated)
        {
            // get current page
            EM_USER usr = EM_USER.Current.GetCurrentPage();
            if (usr == null)
            {
                ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
                return ret;
            }
            // check permissions
            if (!PERMISSION.User.HasPermissionTypeMODULE(usr.UserId, PERMISSION.Common.Obj.MENU, PERMISSION.Common.Action.Edit, PERMISSION.Common.Type.Allow))
            {
                ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
                return ret;
            }

            // update
            // desc
            ProductRep productRep = new ProductRep();
            PRODUCT_ATTRIBUTE_DESC pad = productRep.GetProductAttributeDescByID(pid, attr, lang);
            if (pad == null)
            {
                pad = new PRODUCT_ATTRIBUTE_DESC { PRODUCT_ID = pid, AttributeID = attr, LANG_ID = lang };
                productRep.AddProductAttributeDesc(pad);
            }
            if (!string.IsNullOrWhiteSpace(title))
                pad.Title = title;
            pad.Desc = desc;

            // all ok, save
            if (!productRep.Save())
            {
                ret.Message = "Napaka pri shranjevanju";
                return ret;
            }
            //var wAttr = productRep.GetProductAttributeDescByID(pid, attr, lang);

            ret.Message = "success";
            ret.Code = SM.EM.Ajax.ReturnCode.OK;
            ret.Data = new { Title = pad.Title, Desc = pad.Desc, AttributeID = pad.AttributeID };

        }
        return ret;
    }


    [WebMethod(EnableSession = true)]
    public SM.EM.Ajax.ReturnData DeleteProduct(Guid prodID, bool del, string lang)
    {
        SM.EM.Ajax.ReturnData ret = new SM.EM.Ajax.ReturnData { Code = SM.EM.Ajax.ReturnCode.Failure, Message = " no go! " };

        if (Context == null)
        {
            ret.Message = "no go ;)";
            return ret;
        }

        if (!Context.User.Identity.IsAuthenticated)
        {
            ret.Message = "Tvoja seja je potekla. Ponovno se moraš prijaviti v sistem.";
            return ret;
        }

        // get current page
        EM_USER usr = EM_USER.Current.GetCurrentPage();
        if (usr == null)
        {
            ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
            return ret;
        }
        // check permissions
        if (!PERMISSION.User.HasPermissionTypePAGE(usr.UserId, PERMISSION.Common.Obj.PRODUCT, PERMISSION.Common.Action.Delete, PERMISSION.Common.Type.Allow))
        {
            ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
            return ret;
        }


        //// get user
        //EM_USER usr = EM_USER.GetUserByUserName(Context.User.Identity.Name);
        //if (usr == null)
        //{
        //    ret.Message = "no go ";
        //    return ret;
        //}


        // save PRODUCT
        ProductRep productRep = new ProductRep();

        PRODUCT prod = productRep.GetProductByID(prodID);
        if (prod == null || prod.PageId != usr.UserId)
        {
            ret.Message = "Izdelek ne obstaja";
            return ret;
        }
        prod.DELETED = del;

        // validate
        List<RuleViolation> errlist = new List<RuleViolation>();
        bool valid = true;
        if (!prod.IsValid)
        {
            errlist = prod.GetRuleViolations().ToList();
            valid = false;

        }
        if (!valid)
        {
            ret.Message = ViewManager.RenderView("~/_ctrl/module/_err/_viewErrValidationListPopup.ascx", errlist, new object[] { });
            return ret;
        }

        // all ok, save
        if (!productRep.Save())
        {
            ret.Message = "Napaka pri shranjevanju";
            return ret;
        }

        ret.Data = ViewManager.RenderView("~/_ctrl/module/product/_viewProductDetail.ascx", productRep.GetProductVWByID(prodID, lang, usr.UserId), new object[] { true });

        ret.Message = "success";
        ret.Code = SM.EM.Ajax.ReturnCode.OK;

        return ret;
    }

    [WebMethod(EnableSession = true)]
    public SM.EM.Ajax.ReturnData DeleteProductFile(Guid id)
    {
        SM.EM.Ajax.ReturnData ret = new SM.EM.Ajax.ReturnData { Code = SM.EM.Ajax.ReturnCode.Failure, Message = " no go! " };

        if (Context == null)
        {
            ret.Message = "no go ;)";
            return ret;
        }
        if (!Context.User.Identity.IsAuthenticated)
        {
            ret.Message = "Tvoja seja je potekla. Ponovno se moraš prijaviti v sistem.";
            return ret;
        }

        if (Context.User.Identity.IsAuthenticated)
        {
            // get current page
            EM_USER usr = EM_USER.Current.GetCurrentPage();
            if (usr == null)
            {
                ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
                return ret;
            }
            // check permissions
            if (!PERMISSION.User.HasPermissionTypePAGE(usr.UserId, PERMISSION.Common.Obj.PRODUCT, PERMISSION.Common.Action.Delete, PERMISSION.Common.Type.Allow))
            {
                ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
                return ret;
            }


            // get CWP
            //            CMS_WEB_PART cwp = CMS_WEB_PART.GetWebpartByID(id, Context.User.Identity.Name);
           FILE.DeleteProductFile(id, usr.UserId);

            // return
            ret.Message = "success";
            ret.Data = id;
            ret.Code = SM.EM.Ajax.ReturnCode.OK;
        }
        return ret;
    }

    [WebMethod(EnableSession = true)]
    public SM.EM.Ajax.ReturnData AddProductFiles(Guid id, string lang, List<Guid> items)
    {
        SM.EM.Ajax.ReturnData ret = new SM.EM.Ajax.ReturnData { Code = SM.EM.Ajax.ReturnCode.Failure, Message = " no go! " };

        if (Context == null)
        {
            ret.Message = "no go ;)";
            return ret;
        }
        if (!Context.User.Identity.IsAuthenticated)
        {
            ret.Message = "Tvoja seja je potekla. Ponovno se moraš prijaviti v sistem.";
            return ret;
        }

        if (Context.User.Identity.IsAuthenticated)
        {
            // get current page
            EM_USER usr = EM_USER.Current.GetCurrentPage();
            if (usr == null)
            {
                ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
                return ret;
            }
            // check permissions
            if (!PERMISSION.User.HasPermissionTypePAGE(usr.UserId, PERMISSION.Common.Obj.PRODUCT, PERMISSION.Common.Action.Edit, PERMISSION.Common.Type.Allow))
            {
                ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
                return ret;
            }


            FileRep frep = new FileRep();
            foreach (Guid item in items) {

                frep.AddProductFileLang(new PRODUCT_FILE_LANG { FileID = item, LANG_ID = lang, PRODUCT_ID = id, ProdFileID = Guid.NewGuid() });
            }
            // save
            if (!frep.Save()) {
                ret.Message = "Napaka pri shranjevanju";
                return ret;
            }

            // return
            ret.Message = "success";
            ret.Data = id;
            ret.Code = SM.EM.Ajax.ReturnCode.OK;
        }
        return ret;
    }


#endregion


#region Article
    [WebMethod(EnableSession = true)]
    public SM.EM.Ajax.ReturnData SaveArticleEdit(int artID, Guid cwpID, string title, string dateStart, string dateExpire, string abstr, string body, bool comments, bool approved, bool exposed, short sw, string lang, int smp)
    {
        SM.EM.Ajax.ReturnData ret = new SM.EM.Ajax.ReturnData { Code = SM.EM.Ajax.ReturnCode.Failure, Message = " no go! " };

        if (Context == null)
        {
            ret.Message = "no go ;)";
            return ret;
        }

        if (!Context.User.Identity.IsAuthenticated)
        {
            ret.Message = "Tvoja seja je potekla. Ponovno se moraš prijaviti v sistem.";
            return ret;
        }

        // get current page
        EM_USER usr = EM_USER.Current.GetCurrentPage();
        if (usr == null)
        {
            ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
            return ret;
        }
        // check permissions
        if (!PERMISSION.User.HasPermissionTypeMODULE(usr.UserId, null, cwpID ,  PERMISSION.Common.Obj.MOD_ARTICLE, PERMISSION.Common.Action.Edit, PERMISSION.Common.Type.Allow))
        {
            ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
            return ret;
        }

        // get CWP
        CMS_WEB_PART cwp = CMS_WEB_PART.GetWebpartByID(cwpID, usr.USERNAME );
        if (cwp == null)
        {
            ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
            return ret;
        }


        // save ARTICLE
        ArticleRep arep = new ArticleRep();

        ARTICLE art;
        if (artID <= 0)
        {// new article
            art = new ARTICLE();
            art.LANG_ID = lang;
            art.SMAP_ID = smp;
            art.CWP_ID = cwp.CWP_ID;

            arep.Add(art);

            // insert CWP for gallery
            CMS_WEB_PART wp = CMS_WEB_PART.CreateNewCwpGallery(SM.BLL.Common.GalleryType.DefaultArticle, art.ART_GUID, null, MODULE.Common.GalleryModID, ARTICLE.Common.MAIN_GALLERY_ZONE, usr.UserId, "Galerija", "", true, false, -1, false, false, 1, SM.BLL.Common.ImageType.ArticleDetailsThumbDefault, SM.BLL.Common.ImageType.EmenikBigDefault);
        }
        else
        {
//            art = arep.GetArticleByID(artID, cwp.UserId.Value);
            art = arep.GetArticleByID(artID, cwp.UserId.Value);
            if (art == null)
            {
                ret.Message = "Članek ne obstaja";
                return ret;
            }
        }



        // validate date
        DateTime releaseDate = DateTime.MinValue ;
        if (!string.IsNullOrEmpty(dateStart.Trim()))
            DateTime.TryParse(dateStart.Trim(), out releaseDate);
        
        DateTime expireDate = DateTime.MinValue;
        if (!string.IsNullOrEmpty(dateExpire.Trim()))
            DateTime.TryParse(dateExpire.Trim(), out expireDate);

        releaseDate = SM.EM.Helpers.Date.ReValidateSqlDateTime(releaseDate);
        expireDate  = SM.EM.Helpers.Date.ReValidateSqlDateTime(expireDate );

        // data
        art.ART_TITLE = title;
        art.RELEASE_DATE = releaseDate == System.Data.SqlTypes.SqlDateTime.MinValue ? (DateTime?)null : releaseDate;
        art.EXPIRE_DATE = expireDate == System.Data.SqlTypes.SqlDateTime.MinValue ? (DateTime?)null : expireDate;
        art.DATE_MODIFY = DateTime.Now;
        art.ART_ABSTRACT = abstr ;
        art.ART_BODY =  Server.HtmlEncode( body);
        art.COMMENTS_ENABLED = comments;

        art.APPROVED = approved;
        art.EXPOSED = exposed;
        art.SortWeight = sw;


        // validate
        List<RuleViolation> errlist = new List<RuleViolation>();
        bool valid = true;
        if (!art.IsValid)
        {
            errlist = art.GetRuleViolations().ToList();
            valid = false;

        } 
        if (!valid)
        {
            ret.Message = ViewManager.RenderView("~/_ctrl/module/_err/_viewErrValidationListPopup.ascx", errlist, new object[] { });
            return ret;
        }


        // all ok, save
        if (!arep.Save())
        {
            ret.Message = "Napaka pri shranjevanju";
            return ret;
        }


        //if (action == "ajax-list-cwp")
        //{
        //    // return new image list
        //    var cwp_gal = CMS_WEB_PART.GetCwpGalleryByID(data.CWP_ID);
        //    var img_list = GALLERY.GetImageListByCwpPaged(data.CWP_ID, LANGUAGE.GetCurrentLang(), true, 1, pSize);

        //    ret.Code = SM.EM.Ajax.ReturnCode.OK;
        //    ret.Data = ViewManager.RenderView("~/_ctrl/module/article/_viewArticleList.ascx", data, new object[] { true, cwp_gal, img_list });
        //    ret.ID = cwpID.ToString();
        //}
        //else {
            ret.Code = SM.EM.Ajax.ReturnCode.OK_refresh;
        //}

        ret.Message = "success";

        return ret;
    }


    [WebMethod(EnableSession = true)]
    public SM.EM.Ajax.ReturnData SaveSettingsArticle(Guid cwpID, string title, bool active, int pageSize, bool showAbstract, bool showArchive, bool showImages, int tType, int dType, string action, bool showPaging)
    {
        SM.EM.Ajax.ReturnData ret = new SM.EM.Ajax.ReturnData { Code = SM.EM.Ajax.ReturnCode.Failure, Message = " no go! " };

        if (Context == null)
        {
            ret.Message = "no go ;)";
            return ret;
        }

        if (!Context.User.Identity.IsAuthenticated)
        {
            ret.Message = "Tvoja seja je potekla. Ponovno se moraš prijaviti v sistem.";
            return ret;
        }

        // get current page
        EM_USER usr = EM_USER.Current.GetCurrentPage();
        if (usr == null)
        {
            ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
            return ret;
        }
        // check permissions
        if (!PERMISSION.User.HasPermissionTypeMODULE(usr.UserId, null, cwpID, PERMISSION.Common.Obj.MOD_ARTICLE , PERMISSION.Common.Action.Settings, PERMISSION.Common.Type.Allow))
        {
            ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
            return ret;
        }


        int pSize = pageSize;
        if (pSize < 0)
            pSize = 0;

        // save CWP
        CMS_WEB_PART data = CMS_WEB_PART.SaveWebpartSettings(cwpID, usr.USERNAME , title, active, pSize, dType, showPaging  );
        if (data == null)
        {
            ret.Message = "Nimaš pravic ";
            return ret;
        }

        // save CWP article
        CMS_WEB_PART.UpdateCwpArticle(data.CWP_ID, tType, showImages, showAbstract, showArchive);



        //if (action == "ajax-list-cwp")
        //{
        //    // return new image list
        //    var cwp_gal = CMS_WEB_PART.GetCwpGalleryByID(data.CWP_ID);
        //    var img_list = GALLERY.GetImageListByCwpPaged(data.CWP_ID, LANGUAGE.GetCurrentLang(), true, 1, pSize);

        //    ret.Code = SM.EM.Ajax.ReturnCode.OK;
        //    ret.Data = ViewManager.RenderView("~/_ctrl/module/article/_viewArticleList.ascx", data, new object[] { true, cwp_gal, img_list });
        //    ret.ID = cwpID.ToString();
        //}
        //else {
        ret.Code = SM.EM.Ajax.ReturnCode.OK_refresh;
        //}

        ret.Message = "success";

        return ret;
    }

    [WebMethod(EnableSession = true)]
    public SM.EM.Ajax.ReturnData DeleteArticle(Guid artGUID, bool del, string lang)
    {
        SM.EM.Ajax.ReturnData ret = new SM.EM.Ajax.ReturnData { Code = SM.EM.Ajax.ReturnCode.Failure, Message = " no go! " };

        if (Context == null)
        {
            ret.Message = "no go ;)";
            return ret;
        }

        if (!Context.User.Identity.IsAuthenticated)
        {
            ret.Message = "Tvoja seja je potekla. Ponovno se moraš prijaviti v sistem.";
            return ret;
        }

        // get current page
        EM_USER usr = EM_USER.Current.GetCurrentPage();
        if (usr == null)
        {
            ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
            return ret;
        }
        // check permissions
        if (!PERMISSION.User.HasPermissionTypeMODULE(usr.UserId, null, null, PERMISSION.Common.Obj.MOD_ARTICLE, PERMISSION.Common.Action.Delete, PERMISSION.Common.Type.Allow))
        {
            ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
            return ret;
        }

        //// get user
        //EM_USER usr = EM_USER.GetUserByUserName(Context.User.Identity.Name);
        //if (usr == null)
        //{
        //    ret.Message = "no go ";
        //    return ret;
        //}


        // delete ARTICLE
        ArticleRep arep = new ArticleRep(); ;

        ARTICLE art = arep.GetArticleByID  (artGUID, usr.UserId );
        if (art == null )
        {
            ret.Message = "Članek ne obstaja";
            return ret;
        }
        art.ACTIVE = del;

        // validate
        List<RuleViolation> errlist = new List<RuleViolation>();
        bool valid = true;
        if (!art.IsValid)
        {
            errlist = art.GetRuleViolations().ToList();
            valid = false;

        }
        if (!valid)
        {
            ret.Message = ViewManager.RenderView("~/_ctrl/module/_err/_viewErrValidationListPopup.ascx", errlist, new object[] { });
            return ret;
        }

        // all ok, save
        if (!arep.Save())
        {
            ret.Message = "Napaka pri shranjevanju";
            return ret;
        }


        ret.Code = SM.EM.Ajax.ReturnCode.OK_refresh;


        ret.Message = "success";

        return ret;
    }


#endregion

    [WebMethod(EnableSession = true)]
    public SM.EM.Ajax.ReturnData UpdateID(Guid cwpID, Guid id)
    {
        SM.EM.Ajax.ReturnData ret = new SM.EM.Ajax.ReturnData { Code = SM.EM.Ajax.ReturnCode.Failure, Message = " no go! " };
        if (Context == null)
        {
            ret.Message = "no go ;)";
            return ret;
        }
        if (!Context.User.Identity.IsAuthenticated)
        {
            ret.Message = "Tvoja seja je potekla. Ponovno se moraš prijaviti v sistem.";
            return ret;
        }

        // get current page
        EM_USER usr = EM_USER.Current.GetCurrentPage();
        if (usr == null)
        {
            ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
            return ret;
        }
        // check permissions
        if (!PERMISSION.User.HasPermissionTypeMODULE(usr.UserId, null, cwpID, PERMISSION.Common.Obj.MOD_ARTICLE, PERMISSION.Common.Action.Settings, PERMISSION.Common.Type.Allow))
        {
            ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
            return ret;
        }

        string view = "";
        CMS_WEB_PART cwp = CMS_WEB_PART.UpdateCwpID(cwpID, usr.USERNAME , id );
        if (cwp == null)
        {
            ret.Message = "Nimaš pravic ";
            return ret;
        }

        object data = new object() ;
        object[] parameters = new object[]{};
        switch (cwp.MOD_ID)
        {
            case MODULE.Common.PollModID:
                view = "~/_ctrl/module/poll/_viewPoll.ascx";
                data = POLL.UpdatePollCWP(id, cwpID);

                parameters = new object[] { true, cwp.LANG_ID, cwpID  };
                ret.ID = cwpID ;                
                break;
        }

        if (string.IsNullOrEmpty(view))
        {
            ret.Message = "View ne obstaja";
            return ret;
        }

        ret.Message = "success";
        ret.Code = SM.EM.Ajax.ReturnCode.OK;
        ret.Data = ViewManager.RenderView(view, data, parameters );
        ret.ID = cwpID;
        return ret;
    }



    [WebMethod(EnableSession = true)]
    public SM.EM.Ajax.ReturnData RenderViewAut(object[] args, int viewID)
    {
        SM.EM.Ajax.ReturnData ret = new SM.EM.Ajax.ReturnData { Code = SM.EM.Ajax.ReturnCode.Failure, Message = " no go! " };
        if (Context == null)
        {
            ret.Message = "no go ;)";
            return ret;
        }
        if (!Context.User.Identity.IsAuthenticated)
        {
            ret.Message = "Tvoja seja je potekla. Ponovno se moraš prijaviti v sistem.";
            return ret;
        }

        // get current page
        EM_USER usr = EM_USER.Current.GetCurrentPage();
        if (usr == null)
        {
            ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
            return ret;
        }
        // check permissions
        //if (!PERMISSION.User.HasPermissionTypeMODULE(usr.UserId, null, cwpID, PERMISSION.Common.Obj.MOD_ARTICLE, PERMISSION.Common.Action.Settings, PERMISSION.Common.Type.Allow))
        //{
        //    ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
        //    return ret;
        //}


        //// get user
        //EM_USER usr = EM_USER.GetUserByUserName(Context.User.Identity.Name);
        //if (usr == null)
        //{
        //    ret.Message = "no go ";
        //    return ret;
        //}

        string view = "";
        object data = new object();
        object[] parameters = new object[] {};

        // get view data
        int sml;
        string lang = "";
        Guid id;
        if(viewID == 1)
        {
                sml = (int)args[0];
                lang = args[1].ToString();
                data = SITEMAP.GetSitemapLangByID(sml, lang);
                view = "~/_ctrl/module/product/edit/_viewProductNew.ascx";
        }
        else if (viewID == 2)
        { // openLookup - sitemap
                string tit = args[0].ToString();
                string hf = args[1].ToString();
                int smp = -1;
                if (args.Length > 2 && args[2] != null)
                    smp = (int)args[2];
                else
                    smp = SITEMAP.GetRootByPageName(usr.PAGE_NAME).SMAP_ID;
                lang = CULTURE.GetDefaultUserCulture(usr.UserId).LANG_ID;
                //sml = SITEMAP.GetRootByPageName(usr.PAGE_NAME).SMAP_ID;
                data = SITEMAP.GetSiteMapChildsByIDByLang(smp, lang);
                parameters = new object[] { smp, tit, hf };
                view = "~/_ctrl/module/product/_viewProductSmapLookup.ascx";
        }
        else if (viewID == 3)
        {
                id = new Guid(args[0].ToString());
                lang = args[1].ToString();

                ProductRep prep = new ProductRep();
                vw_Product p =  prep.GetProductVWByID (id, lang, usr.UserId  );
                if (data == null)
                {
                    ret.Message = "Napaka";
                    return ret;
                }        
                data = p;
                parameters = new object[] { SITEMAP.GetSitemapLangByID(p.SMAP_ID , lang)};
                view = "~/_ctrl/module/product/edit/_viewProductEdit.ascx";
        }
        else if (viewID == 4)
        { // product desc ALL
            id = new Guid(args[0].ToString());
            ProductRep prep = new ProductRep();
            data = prep.GetProductDescListByPage(id, usr.UserId);
            if (data == null)
            {
                ret.Message = "Napaka";
                return ret;
            }
            
            parameters = new object[] { CULTURE.GetUserCultures(usr.UserId), id };
            view = "~/_ctrl/module/product/edit/_viewProductDescEdit.ascx";
        }
        else if (viewID == 5)
        { // editProductAttribute
            id = new Guid(args[0].ToString());
            Guid grid = new Guid(args[1].ToString());
            ProductRep prep = new ProductRep();
            data = id; 
            if (data == null)
            {
                ret.Message = "Napaka";
                return ret;
            }
            GROUP_ATTRIBUTE gr = prep.GetGroupAttributeByID (grid);
            var attrList = prep.GetAttributes(grid);
            if (!string.IsNullOrEmpty(gr.OrderByField))
                attrList = attrList.OrderBy(gr.OrderByField);
            else
                attrList = attrList.OrderBy(w => w.Ordr);

            parameters = new object[] { true, gr , attrList.ToList() };
            if (gr.GrType == ATTRIBUTE.Common.GrType.RANGE )
                view = "~/_ctrl/module/product/attribute/_viewAttributeEditRange.ascx";
            else if (gr.GrType == ATTRIBUTE.Common.GrType.RBL_PRICE )
                    view = "~/_ctrl/module/product/attribute/_viewAttributeEditPrice.ascx";
                else
                    view = "~/_ctrl/module/product/attribute/_viewAttributeEdit.ascx";
        }
        else if (viewID == 6)
        {
            string tit = args[0].ToString();
            string hf = args[1].ToString();
            ProductRep prep = new ProductRep();
            data = prep.GetManufacturers(usr.UserId);
            parameters = new object[] { tit, hf };
            view = "~/_ctrl/module/product/lookup/_viewProductManufacturerLookup.ascx";
        }
        else if (viewID == 7)
        {// openSortMenu
            int parent = int.Parse(args[0].ToString());
            lang = args[1].ToString();

            data = SITEMAP.GetSiteMapChildsByIDByLang(parent, lang );
            if (data == null)
            {
                ret.Message = "Napaka";
                return ret;
            }
            parameters = new object[] { parent };
            view = "~/_ctrl/menu/_viewPopupSortMenu.ascx";
        }
        else if (viewID == 8)
        {// newMenuLoad
            sml = (int)args[0];
            lang = args[1].ToString();
            string title = args[2].ToString();

            if (string.IsNullOrEmpty(title))
            {
                data = SITEMAP.GetSitemapLangByID(sml, lang);
            }
            else {
                data = new SITEMAP_LANG { SMAP_ID = sml, LANG_ID = lang, SML_TITLE = title  };
            }

            view = "~/_ctrl/menu/edit/_viewMenuNew.ascx";
        }
        else if (viewID == 9)
        {//editMenuLoad
            sml = (int)args[0];
            lang = args[1].ToString();
                data = SITEMAP.GetSiteMapByID(sml, usr.UserId );
                var list = SITEMAP.GetSitemapLangsBySitemap(sml);
                var pmodl = PAGE_MODULE.GetModulesByTypeByMaster(SM.BLL.Common.PageModuleTypes.Emenik_User, usr.MASTER_ID.Value);
                parameters = new object[] { usr, list, pmodl  };
            //else
            //{
            //    data = new SITEMAP_LANG { SMAP_ID = sml, LANG_ID = lang, SML_TITLE = title };
            //}

            view = "~/_ctrl/menu/edit/_viewMenuEditPopup.ascx";
        }
        else if (viewID == 10)
        {// addImageLoad
            //int gal = (int)args[0];
            Guid cwp = new Guid(args[0].ToString());
            //lang = args[1].ToString();
            data = cwp;
            parameters = new object[] {  };

            view = "~/_ctrl/module/gallery/_viewImageAddPopup.ascx";
        }
        else if (viewID == 11)
        {// reloadCWPImgList
            Guid cwpID = new Guid(args[0].ToString());
            lang = args[1].ToString();

            var cwp_gal = CMS_WEB_PART.GetCwpGalleryByID(cwpID);
            if (cwp_gal == null )
            {
                ret.Message = "Napaka";
                return ret;
            }

            var img_list = GALLERY.GetImageListByCwpPaged(cwpID, lang, true, 1, 100);
            data = CMS_WEB_PART.GetWebpartByID(cwpID, usr.USERNAME );
            parameters = new object[] { true, cwp_gal, img_list };

            view = "~/_ctrl/module/gallery/_viewGalleryImageList.ascx";
            ret.ID = cwpID.ToString();
        }
        else if (viewID == 12)
        {// reloadProductDetails
            Guid pid = new Guid(args[0].ToString());
            lang = args[1].ToString();

            view = "~/_ctrl/module/product/_viewProductDetail.ascx";
            ProductRep prep = new ProductRep();
            data = prep .GetProductVWByID(pid , lang, usr.UserId);
            parameters = new object[] { true };
        }
        else if (viewID == 13)
        {// editArticleLoad
            Guid cwp = new Guid(args[0].ToString());
            //lang = args[1].ToString();
            int artID = int.Parse(args[1].ToString());

            view = "~/_ctrl/module/article/_viewArticleEdit.ascx";
            //ProductRep prep = new ProductRep();
            //data = ARTICLE.GetArticleByID(artID, usr.UserId);
            if (artID > 0)
                data = ARTICLE.GetArticleByID(artID);
            else
            {
                data = new ARTICLE {ART_ID = -1, ART_TITLE = "", ART_BODY = "", ACTIVE = true, CWP_ID = cwp , RELEASE_DATE = DateTime.Now.Date  };
            }
            parameters = new object[] {  };
        }
        else if (viewID == 14)
        { // product desc LONG
            id = new Guid(args[0].ToString());
            ProductRep prep = new ProductRep();
            data = prep.GetProductDescListByPage(id, usr.UserId);
            if (data == null)
            {
                ret.Message = "Napaka";
                return ret;
            }

            parameters = new object[] { CULTURE.GetUserCultures(usr.UserId), id };
            view = "~/_ctrl/module/product/edit/_viewProductDescEditFCK.ascx";
        }
        else if (viewID == 15)
        { // PRODUCT FILE EDIT 
            id = new Guid(args[0].ToString());
            //ProductRep prep = new ProductRep();
            data = CULTURE.GetUserCultures(usr.UserId);
            if (data == null)
            {
                ret.Message = "Napaka";
                return ret;
            }

            parameters = new object[] { id };
            view = "~/_ctrl/module/file/edit/_viewFileEditProduct.ascx";
        }
        else if (viewID == 16)
        { // FILE ADD
            id = new Guid(args[0].ToString());
            short fType = short.Parse(args[2].ToString());
            string callback = args[3].ToString();
            FileRep frep = new FileRep();
            data = frep.GetFileListByPage(usr.UserId, fType); 
            if (data == null)
            {
                ret.Message = "Napaka";
                return ret;
            }

            parameters = new object[] { usr.UserId, id, lang, callback };
            view = "~/_ctrl/module/file/edit/_viewFileAddLookup.ascx";
        }
        else if (viewID == 17)
        {// attribute color lookup
            string tit = args[0].ToString();
            Guid ga = new Guid(args[1].ToString());
            Guid pid = new Guid(args[2].ToString());

            //string hf = args[1].ToString();
            ProductRep prep = new ProductRep();
            data = prep.GetAttributesByProduct(pid).Where(w => w.GroupAttID == ga );
            parameters = new object[] { tit };
            view = "~/_ctrl/module/product/attribute/lookup/_viewColorLookup.ascx";
        }
        else if (viewID == 18)
        {// editCatDescLoad
            sml = int.Parse(args[0].ToString());
            lang  = args[1].ToString();


            data = SITEMAP.GetSitemapLangByID(sml, lang);
            parameters = new object[] {  };
            view = "~/_ctrl/menu/edit/_viewMenuEditDesc.ascx";
        }
        else if (viewID == 19)
        {// editProductAttributeDesc
            id = new Guid(args[0].ToString());
            var att = new Guid(args[1].ToString());
            lang = args[2].ToString();
            ProductRep prep = new ProductRep();


            data = prep.GetwProductAttributeDescByID(id, att, lang);
            parameters = new object[] { };
            view = "~/_ctrl/module/product/attribute/_viewAttributeDescEdit.ascx";
        }
        else if (viewID == 20)
        { // product desc SHORT
            id = new Guid(args[0].ToString());
            ProductRep prep = new ProductRep();
            data = prep.GetProductDescListByPage(id, usr.UserId);
            if (data == null)
            {
                ret.Message = "Napaka";
                return ret;
            }

            parameters = new object[] { CULTURE.GetUserCultures(usr.UserId), id, false, true };
            view = "~/_ctrl/module/product/edit/_viewProductDescEditFCK.ascx";
        }



        if (data == null){
            ret.Message = "Napaka pri nalaganju";
            return ret;
        }        

        ret.Message = "success";
        ret.Code = SM.EM.Ajax.ReturnCode.OK;
        ret.Data = ViewManager.RenderView(view , data, parameters );
        return ret;
    }

}

