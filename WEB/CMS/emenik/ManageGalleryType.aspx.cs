﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace SM.UI.CMS.EM
{
    public partial class CMS_ManageGalleryType : SM.EM.UI.CMS.BasePage_CMS 
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //lvList.DataBind();

            
            }

        }
        protected void ldsList_OnSelecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            eMenikDataContext db = new eMenikDataContext();

            var list = from l in db.GALLERY_TYPEs   select l;

            //if (_langID != "-1") contents = contents.Where(c => c.LANG_ID == _langID);

            e.Result = list;
        }


        protected void ldsList_Inserting(object sender, LinqDataSourceInsertEventArgs e)
        {
            // set GUID
            GALLERY_TYPE c = (GALLERY_TYPE)e.NewObject;
        }
        protected void lvList_ItemEditing(object sender, ListViewEditEventArgs e)
        {
            // hide insert item
            lvList.InsertItemPosition = InsertItemPosition.None;
        }
 
        protected void lvList_ItemUpdated(object sender, ListViewUpdatedEventArgs e)
        {
            // show insert item
            lvList.InsertItemPosition = InsertItemPosition.LastItem;

            // update all site type categories
            CheckBoxList cblTypes = (CheckBoxList)lvList.Items[lvList.EditIndex].FindControl("cblTypes");
            if (cblTypes == null) return;

            int galType = int.Parse (  lvList.DataKeys[lvList.EditIndex].Value.ToString());
            // delete all previous record
            //GALLERY .DeleteimageTypesForGalleryType(galType );

            // insert all records
            foreach (ListItem item in cblTypes.Items)
            {
                if (item.Selected)
                {
                    // if not exists, insert it
                    GALLERY.InsertImageTypeForGalleryType(int.Parse(item.Value), galType);
                }
                else{ 
                    // delete it
                    GALLERY.DeleteImageTypeForGalleryType(galType, int.Parse(item.Value));
                
                }
            }

        }
        protected void lvList_ItemCanceling(object sender, ListViewCancelEventArgs e)
        {
            // show insert item
            lvList.InsertItemPosition = InsertItemPosition.LastItem;

        }

#region Validation
        protected void ValidateDuplicates(object sender, ServerValidateEventArgs e)
        {

            int id = -1;

            if (lvList.EditIndex >= 0)
                id =  (Int32 )lvList.DataKeys[lvList.EditIndex].Value ;
 
            string name = e.Value;
            eMenikDataContext db = new eMenikDataContext();
            var recs = (from r in db.GALLERY_TYPEs   where r.GAL_TITLE   == name   select r );

            // if editing change SQL
            if (id != null)
                recs = recs.Where(w => w.GAL_TYPE  != id);

            var rec = recs.FirstOrDefault();

            if (rec==null){
                e.IsValid = true;
                return;
            }
            e.IsValid = false;

            CustomValidator val = (CustomValidator)sender;
            val.ErrorMessage = "* Gallery type ";
            if (rec.GAL_TITLE == e.Value)
                val.ErrorMessage += " '" + rec.GAL_TITLE + "' already exists.";
        }


#endregion

        protected void ldsList_Inserted(object sender, LinqDataSourceStatusEventArgs e)
        {
            GALLERY_TYPE c = (GALLERY_TYPE)e.Result;

            CheckBoxList cblTypes = (CheckBoxList)lvList.InsertItem.FindControl("cblTypes");

            // delete previous items
            foreach (ListItem item in cblTypes.Items)
            {
                // insert checked items
                if (item.Selected)
                    GALLERY.InsertImageTypeForGalleryType(int.Parse(item.Value), c.GAL_TYPE);
            }
        }

        protected void lvList_ItemDataBound(object sender, ListViewItemEventArgs e)
        {

            ListViewDataItem dataItem = (ListViewDataItem)e.Item;

            if (lvList.EditIndex < 0) return;

            if (dataItem.DisplayIndex != lvList.EditIndex) return;


            // preselect all categories
            CheckBoxList cblTypes = (CheckBoxList)e.Item.FindControl("cblTypes");
            if (cblTypes == null) return;


            foreach (ListItem item in cblTypes.Items)
            {
                if (GALLERY.ExistImageTypeForGalleryType( int.Parse(item.Value), int.Parse(lvList.DataKeys[lvList.EditIndex].Value.ToString())) )
                {
                    item.Selected = true;
                }
            }


        }
    }
}
