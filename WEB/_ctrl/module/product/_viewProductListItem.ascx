﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewProductListItem.ascx.cs" Inherits="_ctrl_module_product_viewProductListItem" %>
<%--<%@ Register TagPrefix="SM" TagName="Rating" Src="~/_ctrl/module/vote/_viewRating.ascx"   %>--%>

<div class="plImg">
    <%--<img src="../../../_em/templates/_custom/MozeStore/_inc/images/_temp/prodlistimgtest1.jpg" />--%>
    <div class="plImgMask">

        <a class="plImgMaskA" href='<%= SM.BLL.Common.ResolveUrl(SM.EM.Rewrite.EmenikUserProductDetailsUrl(   Prod.PageId, Prod.ID_INT, Prod.NAME ))  %>'  title='<%= Prod.NAME %>' >
<%--            <span class="plRatingS">
                <SM:Rating id="objRating" runat="server"></SM:Rating>
            </span>--%>
            <%= RenderItemTitle() %>
<%--            <span class="actionS"><%= GetGlobalResourceObject("Product", "actionOffer")%></span>
            <span class="inactiveS">NEAKTIVEN ARTIKEL</span>--%>
        </a>
    </div>
    <% if (IsModeCMS) {%>
    <a style="position:absolute;z-index:15;" class="divGalImgThumb_btnEdit" href="#" onclick="editCustomCrop('<%= Prod.IMG_ID %>', '<%= Prod.TYPE_ID %>'); return false" ></a>
    <%} %>
    <img class="plImage" src='<%= SM.BLL.Common.ResolveUrl(Prod.IMG_URL)  %>' alt='<%= SM.EM.Helpers.StripHTML(Prod.NAME) %>' />
        <%--<div class="plImgTape_nw"></div>
        <div class="plImgTape_se"></div>--%>

</div>


<div class="plText">
    <a class="plName" href='<%= SM.BLL.Common.ResolveUrl(SM.EM.Rewrite.EmenikUserProductDetailsUrl(   Prod.PageId, Prod.ID_INT, Prod.NAME ))  %>' >
    <span class="plNameS font_bevan"><%= Prod.NAME %></span>
<% if (false && Prod.PRODUCT_TYPE == PRODUCT.ProductType.NORMAL    ) {%>    
    <span class="plPrice"><span class="plPriceNumber"><%= SM.EM.Helpers.FormatPrice(Prod.PRICE)%></span><%= GetGlobalResourceObject("Product", "priceRegular")%>: </span>
<%}
   else if (false && Prod.PRODUCT_TYPE == PRODUCT.ProductType.PRICE_DEPENDS_ON_ATRIBUTE)
   {%>
   <span class="plPrice"><span class="plPriceNumber"> <%= SM.EM.Helpers.FormatPrice(Prod.PRICE)%></span>
   <% if (Prod.PriceRangeMax == null || Prod.PriceRangeMax.Value <= 0 || Prod.PriceRangeMax.Value > Prod.PRICE){%> 
   <span class="plPriceFrom"><%= GetGlobalResourceObject("Product", "priceFrom")%></span><%--show price FROM only if all prices are not the same--%>
   <%} %>
   <%= GetGlobalResourceObject("Product", "priceRegular")%>: </span>
    
<%} %>

    <%--<span class="plDiscount"><%= GetGlobalResourceObject("Product", "discount")%>: <%= SM.EM.Helpers.FormatDiscount(Prod.DISCOUNT_PERCENT)%></span>--%>
    <%--<span class="plPriceSave"><%= GetGlobalResourceObject("Product", "priceYour")%>: <%= SM.EM.Helpers.FormatPriceFinal(Prod.PRICE, Prod.DISCOUNT_PERCENT)%></span>--%>
    </a>
</div>
<%--<% if (Prod.PRODUCT_TYPE == PRODUCT.ProductType.NORMAL)
   {%>   
<div class="plCartbtnW">
    <div class="plCartbtn">
        <a href="#" title="V košarico" onclick="addToCartL('<%= Prod.PRODUCT_ID %>', this); return false"  class="plAcartAdd  font_bevan"><span><%= GetGlobalResourceObject("Product", "addToBasket")%></span></a>
    </div>
    <div class="clearing"></div>
</div>
<%}
   else if (Prod.PRODUCT_TYPE == PRODUCT.ProductType.PRICE_DEPENDS_ON_ATRIBUTE || Prod.PRODUCT_TYPE == PRODUCT.ProductType.NO_PRICE )
   {%>--%>   
<div class="plCartbtnW">
    <div class="plCartbtn">
        <a href="<%= SM.BLL.Common.ResolveUrl(SM.EM.Rewrite.EmenikUserProductDetailsUrl(   Prod.PageId, Prod.ID_INT, Prod.NAME ))  %>" title="Podrobnosti" class="AcarthovFinish  font_bevan"><span><%= GetGlobalResourceObject("Product", "details")%></span></a>
    </div>
    <div class="clearing"></div>
</div>

        
<%--<%} %>--%>
<div class="clearing"></div>
