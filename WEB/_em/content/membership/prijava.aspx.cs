﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class _em_content_membership_prijava : SM.EM.UI.BasePage_EMENIK
{

    protected override void OnPreInit(EventArgs e)
    {
        if (PageName == "")
            PageName = "eko";

        
        base.OnPreInit(e);
        this.PageModule = -11;
    }

    protected override void OnInit(EventArgs e)
    {
        CurrentSitemapID = -1;
        base.OnInit(e);

    }

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack) {

            if (Page.Request.QueryString["fp"] == "1")
            {
                string script = " setModalMsg('Pozabljeno geslo', 'Geslo je bilo poslano na sporočeni naslov.', true);";
                this.RegisterDocumentReadyScript  += script;
                //string script = "$(function(){ setModalMsg('Pozabljeno geslo', 'Geslo je bilo poslano na sporočeni naslov.', true);   });";
                
                //ClientScript.RegisterStartupScript(Page.GetType(), "val-error", script, true);
            }
        }

        // set meta desc
        this.MetaDesc  = "Predstavljamo vam Terra-Gourmet, deželo gurmanov. Skupaj smo ustvarili spletni klub za sladokusce, kjer si lahko izmenjujete mnenja, recepte in misli, predvsem pa naročite izbrane vrhunske izdelke. Med njimi boste našli okusno EKO meso goveda Limousine, ki je prvovrstna EKO govedina brez konkurence. Skrbno smo izbirali in ji dodali serijo dopolnjujočih izdelkov, kot so razni siri, jabolčni sok, vino, olivno olje in vinski kis, da boste lahko svojo pojedino zaokrožili v popolno harmonijo okusov.";

    }


    //protected void btEnter_Click(object sender, EventArgs e)
    //{
    //    Response.Redirect(SM.BLL.Common.ResolveUrl(SM.EM.Rewrite.EmenikUserUrl("eko", 1471, "V besedi")));
    
    
    //}


    protected override void OnPreRender(EventArgs e)
    {
        this.RelCanonical = Page.ResolveUrl("~/");
        Page.Title = TitleRoot;
        base.OnPreRender(e);
    }

}
