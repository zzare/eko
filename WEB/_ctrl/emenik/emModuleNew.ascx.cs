using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic ;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SM.EM.BLL;
using SM.UI.Controls;

namespace SM.EM.UI.Controls
{
    public partial class emModuleNew : BaseControl_CmsWebPart
    {
        public string CurrentWebPartZone { get; set; }
        public Guid? CwpRefID { get; set; }
        public int? RefSitemapID { get; set; }

        

        protected void Page_Load(object sender, EventArgs e)
        {
            this.Visible = true;
            rfvModNew.ValidationGroup = "modNew" + CurrentWebPartZone;
            btAdd.ValidationGroup = "modNew" + CurrentWebPartZone;

        }
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            LoadData();
            
        }

        public override  void LoadData()
        {
            if (String.IsNullOrEmpty(CurrentWebPartZone) || (ParentPage != null && ParentPage.PageModule < 1))
            {
                this.Visible = false;
                return;
            }


            // if not edit mode, hide control
            if (ParentPage == null)
            {
                this.Visible = false;
                return;
            }
            if (!ParentPage. IsModeCMS )
            {
                this.Visible = false;
                return;
            }

            // if can not add, hide
            if (!PERMISSION.User.Common.CanAddNewModule(ParentPage.em_user.UserId))
            {
                this.Visible = false;
                return;
            }

            // edit mode
            this.Visible = true;
            if (ddlUserModule.Items.Count < 1)
            {

                List<MODULE> list = MODULE.GetModulesByUserByZone(ParentPage.em_user.UserId, CurrentWebPartZone, ParentPage.PageModule);
                if (list.Count < 1) { this.Visible = false; return; }
                ddlUserModule.Items.Add(new ListItem("- NOV ELEMENT -", ""));
                ddlUserModule.DataSource = list;
                ddlUserModule.DataTextField = "EM_NAME";
                ddlUserModule.DataValueField = "MOD_ID";
                ddlUserModule.DataBind();
            }
            ddlUserModule.SelectedIndex = 0;
        }

        protected void btAdd_Click(object o, EventArgs e)
        {

            AddNewModule();


        }

        protected void AddNewModule() {

            //if (!ParentPage.CanSave) return;

            if (ddlUserModule.SelectedValue == "") return;

            if (String.IsNullOrEmpty(CurrentWebPartZone) || ( ParentPage != null && ParentPage.PageModule < 1))
            {
                this.Visible = false;
                return;
            }

            // tmp: todo odstrani
            if (!PERMISSION.User.Common.CanAddNewModule(ParentPage.em_user.UserId )) return;


            // get refID
            CwpRefID = ParentPage.CwpRefID;
            if (CwpRefID == Guid.Empty)
                return;

            // create webpart and add to zone
            ParentPage.AddEmptyCwpToZone(CwpRefID, int.Parse(ddlUserModule.SelectedValue), CurrentWebPartZone, ParentPage.PageModule, 0, true);

        
        }

    }
}