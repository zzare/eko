﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _em_content__common_eko_ctrlSubMenu : BaseControl_EMENIK  
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (ParentPage.SmpEmenik.CurrentNode != null)
            smdsLeft.Provider = ParentPage.SmpEmenik;
        else if (ParentPage.SmpEmenikList[7].CurrentNode != null)
            smdsLeft.Provider = ParentPage.SmpEmenikList[7];

    }


    protected void mLeft_DataBound(object sender, EventArgs e)
    {
        Menu m = (Menu)sender;
        ParentPage.mp_em.SelectCurrentMenuItem(m, smdsLeft.Provider);

        if (mLeft.Items.Count == 0)
          this.Visible = false;

    }

    protected override void OnPreRender(EventArgs e)
    {


        base.OnPreRender(e);
    }



}
