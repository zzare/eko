﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_pmodProductSearch.ascx.cs" Inherits="_em_content_product__ctrl__pmodProductSearch" %>
<%@ Register Namespace="SM.EM.UI.Controls" TagPrefix="EM"  %>
<%@ Register Namespace="SM.UI.Controls" TagPrefix="SM"  %>
<%@ Register TagPrefix="EM" TagName="ModuleNew" Src="~/_ctrl/emenik/emModuleNew.ascx"  %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="SM" TagName="ProductList" Src="~/_em/content/product/_ctrl/_pmodProductList.ascx"   %>




       
<div class="searchContHeadW">
        <h1>Iskanje</h1>
        
        <asp:Panel ID="panSearch" runat="server" DefaultButton="btSearch">
            
            <table class="Tsearch" cellpadding="0" cellspacing="0" border="0">
                <tr>
                <td>
                    <asp:TextBox ID="tbSearch" runat="server" CssClass="TBXsearch"></asp:TextBox>
                    <cc1:TextBoxWatermarkExtender ID="tbwexheaderSearchInput" runat="server" WatermarkText="- vpišite iskalni pojem -" TargetControlID ="tbSearch" WatermarkCssClass="headerSearchInputWater"></cc1:TextBoxWatermarkExtender>
                </td>
                <td style="padding-left:10px;">
                    <SM:LinkButtonDefault ID="btSearch" OnClick="btSearch_Click" CssClass="Astepnext"  runat="server" ><span>POIŠČI</span></SM:LinkButtonDefault>
                </td>
                </tr>
            </table>            

            <div class="clearing"></div>
        </asp:Panel>

<% if (!string.IsNullOrEmpty(SearchString)){%>
    
    <br />

        <h3>Iskalni niz: <strong><%= SearchString%></strong></h3>
<%} %>
</div>

        <SM:ProductList ID="objList" runat="server" />
    
