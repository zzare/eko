using System;
using System.Data;
using System.Configuration;
using System.Web.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace SM.EM
{
   public class SMSection : ConfigurationSection
   {
      [ConfigurationProperty("defaultConnectionStringName", DefaultValue = "sm_EMENIKConnectionString")]
      public string DefaultConnectionStringName
      {
         get { return (string)base["defaultConnectionStringName"]; }
         set { base["connectionStdefaultConnectionStringNameringName"] = value; }
      }

      [ConfigurationProperty("defaultCacheDuration", DefaultValue = "600")]
      public int DefaultCacheDuration
      {
         get { return (int)base["defaultCacheDuration"]; }
         set { base["defaultCacheDuration"] = value; }
      }

      [ConfigurationProperty("contactForm", IsRequired=true)]
      public ContactFormElement ContactForm
      {
         get { return (ContactFormElement) base["contactForm"]; }
      }


      [ConfigurationProperty("ST", IsRequired = true)]
      public STElement ST
      {
         get { return (STElement)base["ST"]; }
      }
   }

   public class ContactFormElement : ConfigurationElement
   {
      [ConfigurationProperty("mailSubject", DefaultValue="Mail from TheBeerHouse: {0}")]
      public string MailSubject
      {
         get { return (string)base["mailSubject"]; }
         set { base["mailSubject"] = value; }
      }

      [ConfigurationProperty("mailTo", IsRequired=true)]
      public string MailTo
      {
         get { return (string)base["mailTo"];  }
         set { base["mailTo"] = value;  }
      }

      [ConfigurationProperty("mailCC")]
      public string MailCC
      {
         get { return (string)base["mailCC"]; }
         set { base["mailCC"] = value; }
      }
   }



   public class STElement : ConfigurationElement
   {
      [ConfigurationProperty("connectionStringName")]
      public string ConnectionStringName
      {
         get { return (string)base["connectionStringName"]; }
         set { base["connectionStringName"] = value; }
      }

      public string ConnectionString
      {
         get
         {
            string connStringName = (string.IsNullOrEmpty(this.ConnectionStringName) ?
               Globals.Settings.DefaultConnectionStringName : this.ConnectionStringName);
            return WebConfigurationManager.ConnectionStrings[connStringName].ConnectionString;
         }
      }

      [ConfigurationProperty("providerType", DefaultValue = "SM.ST.DAL.SqlClient.SqlSTProvider")]
      public string ProviderType
      {
         get { return (string)base["providerType"]; }
         set { base["providerType"] = value; }
      }

      [ConfigurationProperty("ratingLockInterval", DefaultValue = "15")]
      public int RatingLockInterval
      {
         get { return (int)base["ratingLockInterval"]; }
         set { base["ratingLockInterval"] = value; }
      }

      [ConfigurationProperty("pageSize", DefaultValue = "10")]
      public int PageSize
      {
         get { return (int)base["pageSize"]; }
         set { base["pageSize"] = value; }
      }

      [ConfigurationProperty("rssItems", DefaultValue = "5")]
      public int RssItems
      {
         get { return (int)base["rssItems"]; }
         set { base["rssItems"] = value; }
      }

      [ConfigurationProperty("defaultOrderListInterval", DefaultValue = "7")]
      public int DefaultOrderListInterval
      {
         get { return (int)base["defaultOrderListInterval"]; }
         set { base["defaultOrderListInterval"] = value; }
      }

      [ConfigurationProperty("sandboxMode", DefaultValue = "false")]
      public bool SandboxMode
      {
         get { return (bool)base["sandboxMode"]; }
         set { base["sandboxMode"] = value; }
      }

      [ConfigurationProperty("businessEmail", IsRequired=true)]
      public string BusinessEmail
      {
         get { return (string)base["businessEmail"]; }
         set { base["businessEmail"] = value; }
      }

      [ConfigurationProperty("currencyCode", DefaultValue = "�")]
      public string CurrencyCode
      {
         get { return (string)base["currencyCode"]; }
         set { base["currencyCode"] = value; }
      }

      [ConfigurationProperty("lowAvailability", DefaultValue = "10")]
      public int LowAvailability
      {
         get { return (int)base["lowAvailability"]; }
         set { base["lowAvailability"] = value; }
      }

        [ConfigurationProperty("commonDiscountPercent", DefaultValue = "0")]
       public int CommonDiscountPercent
       {
           get { return (int)base["commonDiscountPercent"]; }
           set { base["commonDiscountPercent"] = value; }
       }

      [ConfigurationProperty("enableCaching", DefaultValue = "true")]
      public bool EnableCaching
      {
         get { return (bool)base["enableCaching"]; }
         set { base["enableCaching"] = value; }
      }

      [ConfigurationProperty("cacheDuration")]
      public int CacheDuration
      {
         get
         {
            int duration = (int)base["cacheDuration"];
            return (duration > 0 ? duration : Globals.Settings.DefaultCacheDuration);
         }
         set { base["cacheDuration"] = value; }
      }
   }
}
