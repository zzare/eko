﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewArticleListBlogHome.ascx.cs" Inherits="_ctrl_module_product_viewArticleListBlogHome" %>

<script type="text/javascript">



    $(document).ready(function () {

        $('div.cCycleBlog').cycle({
            timeout: 0,
            speed: 2000,
            cleartypeNoBg: true,
            fx: 'fade',
            speedIn: 600,
            speedOut: 500,
            delay: 0,
            pager: '#cycleNav',
            prev: '.cycleArrowL',
            next:'.cycleArrowR'

            //, pagerAnchorBuilder: pagerFactory
        });
    });

//    function pagerFactory(idx, slide) { 
//        var s = idx > 2 ? ' style="display:none"' : '';
//        return '<a' + s + ' href="#">' + (idx + 1) + '</a>'; 
//    }; 


</script>








<div id="divArticles" runat="server"  class="cCycleBlog" >
    <asp:ListView ID="lvList" runat="server" >
        <LayoutTemplate>
            
                <asp:PlaceHolder ID="itemPlaceholder" runat="server"/>

        </LayoutTemplate>
        <ItemTemplate>

        <div class="cCycleBlogItem">
        


            <div class="artBox <%# RenderArtExposedClass(Container.DataItem) %>">
                <div class="artBoxI">
                    <h2 class="articleH2"><a href='<%#  RenderArtHref(Container.DataItem) %>'><%#  RenderArtTitle(Container.DataItem) %></a></h2>
                    <span class="artBox_date"><%#  RenderAdditionalInfo(Container.DataItem)%></span>
                    <div runat="server"  visible='<%# (bool)( ShowImage && HasImage(  Container.DataItem)) %>' >
<% if (IsModeCMS) {%>
                        <a style="position:absolute;" class="divGalImgThumb_btnEdit" href="#" onclick="editCustomCrop('<%# Eval("IMG_ID") %>', '<%# Eval("TYPE_ID") %>'); return false" ></a>
<%} %>
                        <a class="AartBox_img" href='<%#  RenderArtHref(Container.DataItem) %>' title='<%#  RenderArtTitle(Container.DataItem) %>'>
                            <img width="352" height="215" src='<%# SM.BLL.Common.ResolveUrl ( Eval("ART_IMAGE").ToString() )  %>' alt='<%#  RenderArtTitle(Container.DataItem) %>' />
                        </a>
                    </div>     
                    
                    <div id="Div1" class="artBox_video" runat="server"  visible='<%# (bool)( HasVideo(  Container.DataItem)) %>' >
                        <%# RenderVideo(Container.DataItem)%>
                    </div>                      
                                   
<%--<% if (IsModeCMS){%>
                    <a  class="btEditArticle" href="#" onclick="editArticleLoad('<%= CwpID.ToString() %>', <%# Eval("ART_ID") %>); return false" ></a>

<%} %>--%>
                                    
                    <div class="artBoxR">
                    <p><%#  RenderArtDesc(Container.DataItem) %></p>
                    <a class="art_more" href='<%#  RenderArtHref(Container.DataItem) %>'><%=GetGlobalResourceObject("Modules", "ReadMore")%>&nbsp;&rsaquo;</a>
                    </div>
                    <div class="clearing"></div>
                </div>
                
                
                
                <div class="clearing"></div>

               
            </div>
            <%--<span class="art_sepa"></span>--%>

        </div>

        </ItemTemplate>
    </asp:ListView>
    

</div>
<div id="cycleNav" class="cycleNav" ></div>
<a href="#" class="cycleArrowL"></a>
<a href="#" class="cycleArrowR"></a>

<asp:PlaceHolder ID="phEmpty" runat="server" Visible="false">
    <%= GetGlobalResourceObject("Modules", "MessageEmptyList") %>
</asp:PlaceHolder>

