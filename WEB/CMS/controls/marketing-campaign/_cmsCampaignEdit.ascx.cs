﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

namespace SM.UI.Controls
{
    public partial class _cmsCampaignEdit : BaseControl 
    {
        public event EventHandler OnChanged;
        public event EventHandler OnCancel;
        public event EventHandler OnRefresh;

        public Guid  CampaignID {
            get
            {
                if (ViewState["CampaignID"] == null) return Guid.Empty;

                return new Guid(ViewState["CampaignID"].ToString());
            }
            set{
                ViewState["CampaignID"] = value;
            }
        }
        public int PopupID
        {
            get
            {
                return (int)(ViewState["PopupID"] ?? -1);
            }
            set
            {
                ViewState["PopupID"] = value;
            }
        }

        public Guid UserID { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            objSelect.OnUserSelect += new EventHandler(objSelect_OnUserSelect);

            if (!IsPostBack)
            {

            }

        }

        void objSelect_OnUserSelect(object sender, EventArgs e)
        {
            if (objSelect.GetSelectedItems[0] == null)
                return;

            Guid selected = objSelect.GetSelectedItems[0];

            if (PopupID == 1)
                hfMarketing.Value  = selected.ToString();
            else if (PopupID == 2)
                hfAffiliate.Value  = selected.ToString();

            this.OnRefresh(this, new EventArgs());
            
        }

        // default values
        protected void LoadDefaultValues() {
            //CampaignID = Guid.Empty ;

            tbName.Text = "";
            tbDesc.Text = "";

            tbMarketingPercent.Text = "";
            tbMarketingUser.Text = "";
            hfMarketing.Value = "";

            tbAffiliatePercent .Text = "";
            tbAffiliateUser .Text = "";
            hfAffiliate.Value = "";

            ddlUserDiscount.SelectedValue = "";
            tbDateFrom.Text = "";
            tbDateTo.Text = "";
            tbUserDiscount.Text = "";
            tbPromoCode.Text = "";



        }



        // bind data
        public void BindData() {
            this.Visible = true;


            // ddl popust
            if (ddlUserDiscount.Items.Count <=1 )
            {
                ddlUserDiscount.Items.Add(new ListItem("NAJEM", SHOPPING_ORDER.Common.ModuleItems.NAJEM.ToString()));
            }

            if (CampaignID  == Guid.Empty )
            {
                LoadDefaultValues();
            }
            else {
                LoadData();
            
            }
 

        }

        // LOAD Question
        protected void LoadData() {
            LoadDefaultValues();

            MARKETING_CAMPAIGN  c = MARKETING_CAMPAIGN.GetCampaignByID (CampaignID);
            tbName.Text = c.Name ;
            tbMarketingPercent.Text = c.MarketingPercent.ToString() ;
            hfMarketing.Value  = (c.MarketingUserId ?? Guid.Empty).ToString() ;

            //if (c.MarketingUserId != null) {
            //    tbMarketingUser.Text = c.EM_USER1.USERNAME ;
            
            //}

            tbAffiliatePercent .Text = c.AffilliatePercent .ToString();
            hfAffiliate.Value  = (c.AffilliateUserId  ?? Guid.Empty).ToString();
            //if (c.AffilliateUserId  != null)
            //{
            //    tbAffiliateUser .Text = c.EM_USER.USERNAME;

            //}
            tbDesc.Text = c.Desc;

            ddlType.SelectedValue = c.AffilliateType.ToString();


            string userDiscountType = "";
            if (c.DiscountType != null && c.DiscountType.Value != 0 )
                userDiscountType = c.DiscountType.Value.ToString() ;
            ddlUserDiscount.SelectedValue = userDiscountType ;

            if (c.DateValidFrom != null)
                tbDateFrom.Text = c.DateValidFrom.Value.ToShortDateString();
            if (c.DateValidTo != null)
                tbDateTo.Text = c.DateValidTo.Value.ToShortDateString();

            if (c.UserDiscount != null)
                tbUserDiscount.Text = c.UserDiscount.Value.ToString() ;
            tbPromoCode.Text = c.CampaignPromoCode ;
            tbPromoLink.Text = MARKETING_CAMPAIGN.Common.GetPromoLink(c.CampaignID); 

        }



        public void SaveData()
        {
            
            // validation
            Page.Validate("marketing");
            if (!Page.IsValid) return;

            
            eMenikDataContext db = new eMenikDataContext();

            MARKETING_CAMPAIGN c;
            // add new payment
            if (CampaignID  == Guid.Empty )
            {
                c = new MARKETING_CAMPAIGN();
                c.CampaignID = Guid.NewGuid();
                CampaignID = c.CampaignID;
                c.DateCreated = DateTime.Now;
            
                db.MARKETING_CAMPAIGNs.InsertOnSubmit(c);


            }
            else
            {
                c = db.MARKETING_CAMPAIGNs.SingleOrDefault(w => w.CampaignID == CampaignID);
            }

            c.Name = tbName.Text;
            c.Desc = tbDesc.Text;
            if (!string.IsNullOrEmpty(hfMarketing.Value))
            {
                Guid usr = new Guid(hfMarketing.Value);
                if (usr != Guid.Empty)
                    c.MarketingUserId = usr;
                double percent = 0;
                double.TryParse(tbMarketingPercent.Text, out percent);
                c.MarketingPercent = percent;
            }
            else
            {
                c.MarketingUserId = null;
                c.MarketingPercent = 0;
            }
            if (!string.IsNullOrEmpty(hfAffiliate.Value))
            {

                Guid usr = new Guid(hfAffiliate.Value);
                if (usr != Guid.Empty)
                    c.AffilliateUserId = usr;
                double percent = 0;
                double.TryParse(tbAffiliatePercent.Text, out percent);

                c.AffilliatePercent = percent;
            }
            else
            {
                c.AffilliateUserId = null;
                c.AffilliatePercent = 0;
            }

            // discount
            if (!string.IsNullOrEmpty(tbUserDiscount.Text))
            {
                double percent = 0;
                double.TryParse(tbUserDiscount.Text, out percent);
                c.UserDiscount = percent;
            }
            else
            {
                c.UserDiscount = 0;
            }
            // date from
            if (!string.IsNullOrEmpty(tbDateFrom .Text))
            {
                DateTime  date = DateTime.MinValue ;
                DateTime .TryParse (tbDateFrom .Text, out date );
                if (date != DateTime.MinValue)
                    c.DateValidFrom  = date;
                else
                    c.DateValidFrom = null;
            }
            else
            {
                c.DateValidFrom = null;
            }
            // date to
            if (!string.IsNullOrEmpty(tbDateTo.Text))
            {
                DateTime date = DateTime.MinValue;
                DateTime.TryParse(tbDateTo.Text, out date);
                if (date != DateTime.MinValue)
                    c.DateValidTo = date;
                else
                    c.DateValidTo = null;
            }
            else
            {
                c.DateValidTo = null;
            }

            // disocunt type
            if (!string.IsNullOrEmpty(ddlUserDiscount.SelectedValue ))
            {
                short type = 0;
                short.TryParse(ddlUserDiscount.SelectedValue , out type);
                c.DiscountType= type;
            }
            else
            {
                c.DiscountType = null;
            }


            c.AffilliateType = short.Parse( ddlType.SelectedValue);
            c.CreatedBy = Page.User.Identity.Name;
            c.CampaignPromoCode = tbPromoCode.Text;


            db.SubmitChanges();


            objSelect.IsOpen = false;




            // raise changed event
            OnChanged (this, new EventArgs());

        }

        // save
        protected void btSave_Click(object sender, EventArgs e)
        {
            SaveData();
        }

        // cancel
        protected void btCancel_Click(object sender, EventArgs e)
        {
            OnCancel(this, new EventArgs());
        }

        protected void btSetMarketing_Click(object sender, EventArgs e)
        {
            PopupID = 1;
            objSelect.OpenPopup();
        }

        protected void btSetAffiliate_Click(object sender, EventArgs e)
        {
            PopupID = 2;
            objSelect.OpenPopup();
        }
        

    }
}