﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _ctrl_module_poll_viewPollAdd : BaseControlView
{

    public object Data;
    public object[] Parameters;
    public CMS_WEB_PART Cwp;
    protected string LangID;

    protected void Page_Load(object sender, EventArgs e)
    {
        LoadPoll();
    }



    public void LoadPoll() {

        if (Cwp == null) 
            Cwp = (CMS_WEB_PART ) Data;

        if (Cwp  == null)
            return;

        if (Parameters != null)
        {
            if (Parameters[0] != null)
                IsModeCMS = bool.Parse(Parameters[0].ToString());

            if (Parameters.Length > 1 && Parameters[1] != null)
                LangID = Parameters[1].ToString();
        }


        if (Cwp.UserId != null)
        {
            lvList.DataSource = POLL.GetPollLangListByUser(Cwp.UserId.Value, Cwp.LANG_ID).Distinct();
            lvList.DataBind();
        }


    }

    protected string RenderChecked(object d ) {
        string ret = "";
        if (Cwp == null)
            return ret;
        if (Cwp.ID_GUID == null)
            return ret;

        POLL_LANG item = (POLL_LANG)d;

        if (item.PollID == Cwp.ID_GUID.Value )
            return "checked";

        return ret;
    
    }



}
