
IF EXISTS (SELECT * FROM sysobjects WHERE name='f_fts_ProductDesc' and type='FN')
DROP Function f_fts_ProductDesc
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].f_fts_ProductDesc (@keywords nvarchar(4000))
RETURNS TABLE
AS
RETURN (
   SELECT *
   FROM FREETEXTTABLE (PRODUCT_DESC, *, @keywords) 
   )

GO
