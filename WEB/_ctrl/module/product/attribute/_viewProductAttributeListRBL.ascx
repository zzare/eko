﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewProductAttributeListRBL.ascx.cs" Inherits="_ctrl_module_product_attribute_viewProductAttributeListRBL" %>

<script type="text/javascript">

    $(function () {


        $('.itmProdAttrPrice').find(" input[type='radio']").fancyopts({
            width: 16,
            height: 16,
            hasActive: false,
            image: g_resUrl + '_em/templates/_custom/eko/_inc/images/design/button_radio.png'

            , onCheck: function (value, checked) {


                var at = value;
                var gr = $(this).parents('.itmAttrGroup').eq(0).attr('id');
                if (checked) {
                    selProdAttrPrice(at, gr);
                    $(this).parents('.cAttrPrice').find('input.hfAttr').val(value);

                }
            }
        });




        //        $('.itmProdAttrPrice input').click(function () {
        //            //    $('.itmProdAttrPrice ').click(function () {

        //            alert($(this).find('input:radio').is(':checked'));
        //            //            if (!$(this).is(':checked'))
        //            //                return;

        //            var gr = $(this).parents('.itmAttrGroup').eq(0).attr('id');
        //            var at = $(this).attr('id');
        //            //            var gr = $(this).parents('.itmAttrGroup').eq(0).attr('id');
        //            //            var at = $(this).parents('.itmProdAttrPrice').attr('id');
        //            //  alert(at + ', ' + gr);
        //            selProdAttrPrice(at, gr);
        //        });



    });

    function selFirstRB(indx) {
        //sel first RBL price
        var _at = $('.prod_detailB .itmProdAttrPrice ').eq(indx).attr('id');
        if (_at) {
            $('#pap' + _at).fancyopts('check', true);
        }
    
    }


    function selProdAttrPrice(attr, attrGroup) {

        var $r = $('#' + attrGroup).find('#' + attr);
        
        $('span.pd_pricefull_number').html($r.attr('data-price'));
        $r.parents('.cAttrPrice').find('input.hfAttr').val(attr);
        var pr = parseInt($r.attr('data-price'));
        if (pr >= 0)
            prodAttrShowPriceNormal();
        else
            prodAttrShowPriceContact();
        

        // sel img
        var indx = $r.index('.itmProdAttrPrice');
        $('div.prod_detailLI').cycle(indx);

    }
    function prodAttrShowPriceNormal() {
        $('.pd_pricefull, .pd_cartbtnW, .prod_detailBot').show();
        $('.cProductPriceContact').hide();
    }

    function prodAttrShowPriceContact() {
        $('.pd_pricefull, .pd_cartbtnW, .prod_detailBot').hide();
        $('.cProductPriceContact').show();

    }



</script>


<div >

<asp:ListView ID="lvList" runat="server" >
    <LayoutTemplate>
        
            <asp:PlaceHolder ID="itemPlaceholder" runat="server"/>

    </LayoutTemplate>
    
    <ItemTemplate>
        <% if (IsModeCMS){%>
            <span><a class="btEditContwthText" href="#" onclick="editProductAttribute('<%# ProductID %>', '<%# Eval("GroupAttID") %>'); return false" ><span>dodaj/odstrani/uredi pakete</span></a></span>
        <%} %>

        <div id="ag<%# Eval("GroupAttID") %>" class="itmAttrGroup  ">
            <%# RenderItem(Container.DataItem ) %>
        </div>

    </ItemTemplate>

    <ItemSeparatorTemplate>
        <%--<br />    --%>
    
    </ItemSeparatorTemplate>

</asp:ListView>

</div>