﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Security.Principal;
using System.Web.Caching;
using System.Net.Mail;
using System.Collections.Generic;
using System.Threading;

/// <summary>
/// Summary description for ScheduleTask
/// </summary>

namespace SM.BLL
{
    public class ScheduleTask
    {
        public const string DummyCacheItemKey = "dmmySchdTask";

        public static  bool IsCacheScheduleInCache() {
            if (HttpRuntime.Cache[DummyCacheItemKey] !=  null) return true;

            return false;
        }

        public static bool RegisterCacheEntry()
        {
//            HttpRuntime.Cache.Remove(DummyCacheItemKey);
            if (!SM.BLL.Common.AppSettings.EnableSchedule ) return false;
            if (IsCacheScheduleInCache()) return false;

            HttpRuntime.Cache.Insert(DummyCacheItemKey, "", null, DateTime.MaxValue, TimeSpan.FromMinutes(10), CacheItemPriority.Normal, new CacheItemRemovedCallback(CacheItemRemovedCallback));

            return true;
        }

        public static void CacheItemRemovedCallback(string key, object value, CacheItemRemovedReason reason)
        {
            if (!SM.BLL.Common.AppSettings.EnableSchedule) return;

            RegisterCacheEntry();

            // Do the service works
            //Task.IntranetSendReminders();
            RunSchedule();

        }

        public  static void RunSchedule() {
            
            // get all jobs for execution
            List<JOB_LOG> jobs = JOB.GetJobsToExecute(DateTime.Now);

            foreach (JOB_LOG job in jobs) {

                // if job is due, start it
                if (job.JobID == new Guid("96b178d4-ea3d-45f3-81cf-a024b1516cc1"))
                {
                    Emenik.EmenikSendRemindersNotifyToExpire(DateTime.Now, job.JobLogID, job.JobID);


                }

                // update job executed
                JOB.UpdateJobExecuted(job.JobLogID );
            
            }
        
        }


        public class Emenik {
            public const int DAYS_BEFORE_EXPIRE = 30;
            public const int DAYS_LAST_NOTIFY = 2; // na koliko dni se pošilja



            public static void EmenikSendRemindersNotifyToExpire(object data)
            {

                object[] parameters = (object[])data;
                DateTime  date = DateTime.Parse( parameters[0].ToString());
                int id = int.Parse(parameters[1].ToString());
                Guid  jobId = new Guid (parameters[2].ToString());
                EmenikSendRemindersNotifyToExpire(date, id, jobId );
            

            }

            public static void EmenikSendRemindersNotifyToExpire(DateTime date, int jobLogID, Guid jobID)
            {


                // get all usersd to send emails
                List<EM_USER> list = EM_USER.GetUsersToNotifyToExpire(DAYS_BEFORE_EXPIRE, jobID, DAYS_LAST_NOTIFY);

                // send mails for these users
                foreach (EM_USER usr in list)
                {

                    // compose mail message
                    MailMessage msg = new MailMessage();
                    msg.Body = SM.EM.Mail.Template.GetNotifyToExpireBodyNoContext(usr);
                    msg.From = new MailAddress(SM.EM.Mail.Account.Info.Email, SM.BLL.Common.Emenik.Data.PortalName());
                    msg.To.Add(new MailAddress(usr.EMAIL ));
                    msg.IsBodyHtml = true;
                    msg.Subject = string.Format("Vaša stran bo potekla (" + usr.PAGE_TITLE  + ") ");

                    // smtp
                    SmtpClient client = new SmtpClient();
                    client.Credentials = new System.Net.NetworkCredential(SM.EM.Mail.Account.Info.Email, SM.EM.Mail.Account.Info.Pass);
                    client.Port = 587;
                    client.Host = "smtp.gmail.com";
                    client.EnableSsl = true;




                    Common.SendScheduleEmail(usr.UserId, usr.EMAIL, msg, client, jobLogID);

                    // send the mails asynchronously
                    //object[] parameters = new object[] { usr.UserId, usr.EMAIL, msg, client,  jobLogID };
                    //ParameterizedThreadStart pts = new ParameterizedThreadStart(Common.SendScheduleEmail );
                    //Thread thread = new Thread(pts);
                    //thread.Name = "SendScheduleEmail";
                    //thread.Priority = ThreadPriority.Lowest;
                    //thread.Start(parameters);


                    //SendScheduleEmail(usr.EMAIL , jobLogID);
                        //}

                }

            }

        }
        public class Common{

            public static void SendScheduleEmail(object data)
            {

                object[] parameters = (object[])data;
                Guid  refID = new Guid (parameters[0].ToString());
                string email = parameters[1].ToString();
                MailMessage  msg = parameters[2] as MailMessage ;
                SmtpClient client = parameters[3] as SmtpClient;
                int id = int.Parse(parameters[4].ToString());

                SendScheduleEmail(refID,email, msg,  client, id);


            }
            public static void SendScheduleEmail(Guid refID, string email, MailMessage msg,  SmtpClient client, int jobLogID)
            {

                // check if email was sent

                eMenikDataContext db = new eMenikDataContext();

                JOB_EMAIL jobE = (from e in db.JOB_EMAILs where e.ReferenceID == refID && e.Email == email && e.JobLogID == jobLogID select e).SingleOrDefault();

                if (jobE != null) return; // mail was already sent


                // send email
                try
                {
                    client.SendCompleted += new SendCompletedEventHandler(SendScheduleEmail_SendCompleted);
                    object[] param = new object[] { refID, email, jobLogID  };
                    
                    client.SendAsync(msg, param );                    
                    
                }
                catch (Exception x)
                { 

                    ERROR_LOG.LogError("error sending mail: SendScheduleEmail", x.StackTrace, email, "");
                    db.JOB_EMAILs.InsertOnSubmit(jobE);

                    jobE.Notes = "Error: " + x.Message;
                    db.SubmitChanges();
                }
            }

            static void SendScheduleEmail_SendCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
            {
                // update DB that mail was sent
                object[] parameters = (object[])e.UserState ;
                eMenikDataContext db = new eMenikDataContext();

                JOB_EMAIL jobE = new JOB_EMAIL { ReferenceID = new Guid(parameters[0].ToString()), Email = parameters[1].ToString(), JobLogID = int.Parse(parameters[2].ToString()), DateSent = DateTime.Now };
                db.JOB_EMAILs.InsertOnSubmit(jobE);

                db.SubmitChanges();
            
            }


        }
    }
}