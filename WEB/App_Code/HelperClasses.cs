﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

/// <summary>
/// Summary description for HelperClasses
/// </summary>
/// 
namespace SM
{
    public class HelperClasses
    {

        public class TraceTextWriter : System.IO.TextWriter
        {
            public override System.Text.Encoding Encoding
            {
                get { throw new NotImplementedException(); }
            }

            public override void WriteLine(string value)
            {
                HttpContext.Current.Trace.Write("LINQ", value);
                base.WriteLine(value);
            }
        }


        public static T FindControl<T>(System.Web.UI.ControlCollection Controls) where T : class
        {

            T found = default(T);

            if (Controls != null && Controls.Count > 0)
            {

                for (int i = 0; i < Controls.Count; i++)
                {

                    found = Controls[i] as T;

                    if (found != null)
                    {

                        break;

                    }

                    else

                        found = FindControl<T>(Controls[i].Controls);

                }

            }

            return found;

        }

        public T FindControl<T>(string id, Page p) where T : Control
        {
            return FindControl<T>(p, id);
        }

        public static T FindControl<T>(Control startingControl, string id) where T : Control
        {
            // this is null by default
            T found = default(T);

            int controlCount = startingControl.Controls.Count;

            if (controlCount > 0)
            {
                for (int i = 0; i < controlCount; i++)
                {
                    Control activeControl = startingControl.Controls[i];
                    if (activeControl is T)
                    {
                        found = startingControl.Controls[i] as T;
                        if (string.Compare(id, found.ID, true) == 0) break;
                        else found = null;
                    }
                    else
                    {
                        found = FindControl<T>(activeControl, id);
                        if (found != null) break;
                    }
                }
            }
            return found;
        }

        /// <summary>

        /// Similar to Control.FindControl, but recurses through child controls.

        /// </summary>

        public static T FindControl2<T>(Control startingControl, string id) where T : Control
        {

            T found = startingControl.FindControl(id) as T;

            if (found == null)
            {
                found = FindChildControl<T>(startingControl, id);
            }

            return found;

        }
    /// <summary>     

    /// Similar to Control.FindControl, but recurses through child controls.

    /// Assumes that startingControl is NOT the control you are searching for.

    /// </summary>

    public static T FindChildControl<T>(Control startingControl, string id) where T : Control

    {
        T found = null;
        foreach (Control activeControl in startingControl.Controls)
        {
            found = activeControl as T;

            if (found == null || (string.Compare(id, found.ID, true) != 0))
            {
                found = FindChildControl<T>(activeControl, id);
            }

            if (found != null)
            {
                break;
            }
        }
        return found;
    }


    }
}