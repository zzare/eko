﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

/// <summary>
/// Summary description for MAp
/// </summary>
public partial class CWP_MAP
{

    public class Common {
//        public const string MAP_DOMAIN = "~";
        public static string MAP_DOMAIN
        {
            get
            {
                // todo umakni
                return SM.BLL.Common.ResolveUrl("~");


                return "http://" + SM.BLL.Common.Emenik.Data.PortalHostDomain();
            }
        }



        public static string RenderMapViewHref()
        {


            return MAP_DOMAIN +  "/_ctrl/module/map/page/ShowMap.aspx";
        }
        public static string RenderMapViewHref(Guid id)
        {
            return RenderMapViewHref() + "?id=" + id.ToString();
        }
        public static string RenderMapViewHref(Guid id, string theme)
        {
            return RenderMapViewHref(id) + "&t=" + theme;
        }

        public static string RenderMapEditHref()
        {

            string retUrl = "";
            if (HttpContext.Current == null) return "";
            retUrl = SM.BLL.Common.ResolveUrl( HttpContext.Current.Request.RawUrl);
            string username = HttpContext.Current.Server.UrlEncode( SM.EM.Security.Encryption.Hex( HttpContext.Current.User.Identity.Name   ));

            return MAP_DOMAIN + "/_ctrl/module/map/page/EditMap.aspx?ru=" + HttpContext.Current.Server.UrlEncode( retUrl) + "&k=" + username ;
        }


        public static string RenderMapEditHref(Guid id)
        {
            return RenderMapEditHref() + "&id=" + id.ToString();
        }
        public static string RenderMapEditHref( string theme)
        {
            return RenderMapEditHref() + "&t=" + theme ;
        }
    
    }

}
