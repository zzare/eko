﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewProductAttributeListDDL.ascx.cs" Inherits="_ctrl_module_product_attribute_viewProductAttributeListDDL" %>



<asp:ListView ID="lvList" runat="server" >
    <LayoutTemplate>
        
            <asp:PlaceHolder ID="itemPlaceholder" runat="server"/>

    </LayoutTemplate>
    
    <ItemTemplate>

        <%# RenderItem(Container.DataItem ) %>
        
        <% if (IsModeCMS){%>
            <span><a class="btEditContwthText" href="#" onclick="editProductAttribute('<%# ProductID %>', '<%# Eval("GroupAttID") %>'); return false" ><span>uredi</span></a></span>
        <%} %>
        <br />
    
    </ItemTemplate>
</asp:ListView>