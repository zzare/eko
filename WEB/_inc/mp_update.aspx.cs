﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Security.Cryptography.X509Certificates;

public partial class _inc_mp_update : System.Web.UI.Page
{
    class MyPolicy : ICertificatePolicy
    {
        public bool CheckValidationResult(
            ServicePoint srvPoint
            , X509Certificate certificate
            , WebRequest request
            , int certificateProblem)
        {

            //Return True to force the certificate to be accepted.
            return true;

        } // end CheckValidationResult
    } // class MyPolicy




    // uvoz certifikata
    //winhttpcertcfg.exe -i "D:\_smardt\01 - kolpa-san\Smardt\megapos\produkcija\kolpa.p12" -a zare -c LOCAL_MACHINE\My -p 133073146
    //winhttpcertcfg.exe -i "D:\inetpub\kolpa-san\online\cert\megapos\produkcija\kolpa.p12" -a "NETWORK SERVICE" -c LOCAL_MACHINE\My -p 133073146



    protected void Page_Load(object sender, EventArgs e)
    {
        String txId = "";
        //FileInfo file;
        //StreamWriter fileSW;

        int i = 0;
        foreach (String entry in Request.QueryString)
        {
            if (entry.Equals("txId"))
            {
                txId = Request.QueryString.Get(i);
                break;
            }
            i++;
        }

        int TxId = -1;
        // try get txID
        if (!int.TryParse(txId.TrimStart("sm".ToCharArray()).TrimEnd("ks".ToCharArray()), out TxId)) // fix
            return;

        si.megapos.service.MegaposProcessorService service = new si.megapos.service.MegaposProcessorService();
        System.Net.ServicePointManager.ServerCertificateValidationCallback += delegate
        {   // to se uporabi pri .NET2.0
            return true;
        };

        //System.Net.ServicePointManager.CertificatePolicy = new MyPolicy(); // to se uporabi pri .NET1.1
        //string Certificate = @"D:\_test\eon\certifikati_za_trgovino_test\test\test.cer"; // nujno je treba dodat se .p12 certifikat accountu ASPNET
        //string store = "test";
        X509Certificate cert = X509Certificate.CreateFromCertFile(MegaPos.CertificatePath());
        service.ClientCertificates.Add(cert);


        si.megapos.service.TxOperationResult loadTxResult = service.loadTx(MegaPos.Store(), txId);
        String opResult = "<br>";
        opResult += "<br><b>RESPONSE data:</b><br> code: " + loadTxResult.code + "<br> msg: " + loadTxResult.msg + "<br> success: " + loadTxResult.success + "<br> txId: " + loadTxResult.txId;

        if (loadTxResult.success == true)
        {

            // check for IP
            if (HttpContext.Current.Request.UserHostAddress != "213.143.80.150" || HttpContext.Current.Request.UserHostAddress != "213.143.80.151")
            {
                //return;
            }

            




            MegaPosRep mpr = new MegaPosRep();

            // get trans
            MP_TRAN trans = mpr.GetTransByID(TxId);

            if (trans == null)
                return;


            // update trans
            mpr.UpdateTrans(trans, loadTxResult);
            mpr.Save();

            // update order
            OrderRep orep = new OrderRep();
            SHOPPING_ORDER Order = orep.GetOrderByID(trans.RefID);
            if (Order == null)
                return;

            if (trans.Status == MegaPos.Common.Status.INITIALIZED)
            {
                Order.OrderStatus = SHOPPING_ORDER.Common.Status.ONLINE_PAYMENT_RESERVED;
                Order.DateStart = DateTime.Now;
                orep.SubmitOrderOnlinePaymentEnd (Order);
            }
            else if (trans.Status == MegaPos.Common.Status.PROCESSED)
            {
                Order.DatePayed  = DateTime.Now;
                Order.OrderStatus = SHOPPING_ORDER.Common.Status.PAYED;
            }
            else if (trans.Status == MegaPos.Common.Status.FAILED )
            {
                Order.OrderStatus = SHOPPING_ORDER.Common.Status.STEP_4;

                // move articels back to basket
                ProductRep prep = new ProductRep();
                prep.UpdateCartItemOrder(Order.UserId , Order .PageId, Order .OrderID, null);
            }
            else if (trans.Status == MegaPos.Common.Status.CANCELLED )
            {
                Order.OrderStatus = SHOPPING_ORDER.Common.Status.CANCELED;
            }


            // save
            orep.Save();
            

            //opResult += "<br><br>Zapisujem initData in stateData v datoteko: ./transakcije/" + txId + " ...";
            //si.megapos.service.Tx tx = loadTxResult.tx;
            //si.megapos.service.TxInitData txInitData = tx.initData;
            //si.megapos.service.TxStateData txStateData = tx.stateData;
            //si.megapos.service.TxProcessData txProcessData = txStateData.processData;

            //file = new FileInfo("c:/tmp/" + txId);
            //fileSW = file.CreateText();
            //fileSW.Write(txStateData.type + ";" + txStateData.result + ";" + txInitData.txType);
            //fileSW.Write(";" + txInitData.addInfo + ";" + txInitData.custName + ";" + txInitData.custSurname + ";" + txInitData.paymentGateway);
            //fileSW.Write(";" + txProcessData.amount.ToString() + ";" + txProcessData.currency + ";" + txStateData.resultType + ";" + txStateData.timestamp.ToString() + ";" + tx.store + ";" + tx.txId);
            //fileSW.Close();
            opResult += "uspelo!";
        }
        else
        {
            opResult += "Ni uspelo!";
        }

        //Label1.Text = opResult;

    }

}