﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewEditCartItem.ascx.cs" Inherits="_ctrl_module_product_cart_viewEditCartItem" %>
<%@ Register TagPrefix="SM" TagName="ProductAttribute" Src="~/_ctrl/module/product/attribute/_viewProductAttributeList.ascx"   %>

 <script type="text/javascript" >
     function editCartItemSave(id) {
         var isValid = $("#editCart").valid();
         if (isValid) {

             var q = $("#vgUpdateCartItem").find("#ceditQuantity").val();
             var _items = new Array();
             $("#vgUpdateCartItem select").each(function() {
                 var _item = new Object();
                 _item.ID = $(this).attr('name');
                 _item.Value = $(this).val();
                 _item.ValSet = null;
                 Array.add(_items, _item);
             });
             $("#vgUpdateCartItem .cAttrSlider").each(function () {
                 var _item = new Object();

                 var sl = $(this).find("div.attrSlider");

                 _item.ID = $(this).attr('name');
                 _item.Value = sl.attr('name');
                 _item.ValSet = sl.slider("option", "value");
                 Array.add(_items, _item);
             });

             $("#vgUpdateCartItem .cAttrColor").each(function () {
                 var _item = new Object();

                 _item.ID = $(this).find("input.hfGrAttr").val();
                 _item.Value = $(this).find("input.hfAttr").val();
                 _item.ValSet = null;
                 Array.add(_items, _item);
             });

             $("#vgUpdateCartItem .cAttrPrice").each(function () {
                 var _item = new Object();
                 _item.ID = $(this).find("input.hfGrAttr").val();
                 _item.Value = $(this).find("input.hfAttr").val();

                 _item.ValSet = null;
                 Array.add(_items, _item);
             });

             openModalLoading('<%= GetGlobalResourceObject("Modules", "progressSaveBasket")%>');
             WebServiceP.UpdateCartItem(id, q, _items, em_g_cult, em_g_lang, em_g_pid, onCartUpdateItemSuccess, onCartUpdateError);
         }
     }

     function onCartUpdateItemSuccess(ret) {
         closeModalLoading();
         closeModalLoader();
         onCartUpdateSuccess(ret);
     }


     $(document).ready(function() {

         $("#editCart").validate({
             onsubmit: false,
             rules: {
             ceditQuantity: { required: true, number: true }
             },
             messages: {
             ceditQuantity: {
                 required: '<%= GetGlobalResourceObject("Modules", "valEnterQuantity")%>',
                     number: '<%= GetGlobalResourceObject("Modules", "valEnterNumber")%>'
                 }
             }
         });

         $('#vgUpdateCartItem select').each(function() {
             $(this).rules('add', {
                 required: true,
                 messages: {
                     required: '<%= GetGlobalResourceObject("Modules", "valChoose")%>'
                 }
             });
         });

     });
                
</script>                

<div class="">
<%--    <div class="modPopup_header modPopup_headerEdit">
        <h4><%= GetGlobalResourceObject("Product", "titleEditCart")%></h4>
        <p><%= GetGlobalResourceObject("Product", "descEditCart")%></p>
    </div>--%>
    <div class="modPopup_main" >
        <form id="editCart" action="" >
            <span class="modPopup_mainTitle"><%= GetGlobalResourceObject("Product", "editProduct")%> <%= SM.EM.Helpers.StripHTML(CartItem.ProductTitle)  %></span> 

            <div id="vgUpdateCartItem">

                <SM:ProductAttribute ID="objProductAttribute" runat="server" />
                                                
                <span class="pd_RML_quantityL"><%= GetGlobalResourceObject("Product", "quantity")%> <input name="ceditQuantity" id="ceditQuantity" value='<%= CartItem.Quantity  %>' class="quantityInput" /></span>

            </div>            
        </form>
        
    </div>   

    <div class="modPopup_footer"> 
        <div class="buttonwrapperModal">
            <a class="lbutton lbuttonConfirm AcarthovFinish" href="#" onclick="editCartItemSave('<%= CartItem.CartItemID %>'); return false"><span><%= GetGlobalResourceObject("Modules", "save")%></span></a>
            <a class="lbutton lbuttonDelete AcarthovFinish" href="#" onclick="closeModalLoader(); return false" ><span><%= GetGlobalResourceObject("Modules", "cancel")%></span></a>
        </div>
    </div>                                
</div>