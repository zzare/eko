﻿using System;
using System.Collections;
using System.Collections.Generic ;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SM.EM.UI.Controls;

public partial class _em_settings__ctrl_step_design : BaseWizardStep {

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        //Description = "Izberite celosten izgled spletne strani in barvno kombinacijo, ki vam najbolj ustreza. To storite tako, kliknete gumb \"izberi obliko\" pod želeno obliko in barvno kombinacijo.";
        //Description += "<br /> Obliko in barvno kombinacijo lahko kasneje poljubno spreminjate.";

        Description = "Izberi obliko in barvno kombinacijo spletne strani. ";
        Description += "To narediš tako, da klikneš na barvno kombinacijo želene predloge. ";
        Description += "Barvo lahko zamenjaš kadarkoli s klikom na gumb \"<u>Nastavitve spletne strani</u>\" in nato na gumb \"<u>Oblika strani</u>\". ";
        Description += "<i>Namig: Če se z miško zapelješ nad sliko oblikovne kombinacije in se tam zaustaviš, se izbrana slika poveča.</i>";
        
        //Description = "<ul><li>";
        //Description += "Pozdravljeni! Jaz sem " + "<a href=\"" + Page.ResolveUrl("~/default.aspx") + "\">1dva3.si</a> " + "čarovnik, ki vas bom popeljal skozi 7 preprostih korakov do vaše nove spletne strani. ";
        //Description += "Naj vas opomnim, da se lahko v vsak korak kadarkoli vrnete in spremenite vaše nastavitve.";
        //Description += "</li><li>";
        //Description += "IZBIRA TEME: V prvem koraku si izberite obliko in barvno kombinacijo spletne strani.To naredite tako, da kliknete na barvno kombinacijo želene oblikovne predloge. Ne skrbite, temo ali barvo lahko zamenjate kadarkoli s klikom na gumb \"Nastavitve\".";
        //Description += "</li><li>";
        //Description += "Namig: Če se z miško zapeljete nad sliko oblikovno/barvne kombinacije in se tam zaustavite, se vam izbrana slika poveča.";
        //Description += "</li></ul>";

        ShortDesc = "Izberi celosten izgled spletne strani";
        StepID = 1;
        AllwaysHideSaveButton = true;
        ShowTopNavigation = true;
        Page.MaintainScrollPositionOnPostBack = false;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        

    }

    public override void LoadData()
    {
        //lvList.DataBind();
    }

    public override bool SaveData(int step)
    {
        //UpdateUserStep(step);


        return true;
        
    }

    public override bool Validate()
    {
        if (ParentPageEmenik.em_user.STEPS_DONE < 1)
        {
            ErrorText = " Izberite predlogo.";
            return false;
        }
        
        return true;
            
    }

    protected void ldsList_Selecting(object sender, LinqDataSourceSelectEventArgs e)
    {
        eMenikDataContext db = new eMenikDataContext();

        var list = from m in db.MASTERs where m.ACTIVE == true //join g in db.MASTER_GROUPs on m.MGR_ID equals g.MGR_ID 
                   orderby m.ORDER 
                   select m;
        //if (_langID != "-1") list = list.Where(l => l.LANG_ID == _langID);

        e.Result = list;
    }
    protected void lvList_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem) { 
            // load themes listview for master page
            ListView lvTheme = (ListView)e.Item.FindControl("lvTheme");
            if (lvTheme == null) return;
            ListViewDataItem item = (ListViewDataItem)e.Item;
            MASTER m = (MASTER)item.DataItem ;

            IEnumerable <THEME> list = TEMPLATES.GetThemesByMasterActivePublic(m.MASTER_ID);

            lvTheme.DataSource = list;
            lvTheme.DataBind();
        }
    }

    protected void btSelect_OnClick(object sender, EventArgs e) {


        // if is demo, can't save
        if (!CheckCanSave()) 
            return;

        LinkButton btSelect = (LinkButton ) sender;
        int ThemeID = Int32.Parse(btSelect.CommandArgument);



        ChangeTheme(1, ThemeID);
    }

    protected void ChangeTheme(int step, int themeID) {
        UpdateUserStep(step);


        EM_USER.ChangeDesign(ParentPageEmenik.em_user.UserId, themeID);

        //ParentPageEmenik.em_user.RemoveFromCache();

        // redirect to same step, to change page theme
        Response.Redirect(SM.EM.Rewrite.EmenikUserSettingsUrlByStep( ParentPageEmenik.em_user.PAGE_NAME , 1));
    }

    protected string  IsSelectedTheme(object themeID, string cssclass) {
        if (Int32.Parse(themeID.ToString()) == ParentPageEmenik.em_user.THEME_ID ) return cssclass;

        return "";
    }

}
