﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewImageEdit.ascx.cs" Inherits="SM.EM.UI.Controls._viewImageEdit" %>
<%@ Register Namespace="SM.UI.Controls" TagPrefix="SM" %>
<%@ Register TagPrefix="SM" TagName="PopupUserCrop" Src="~/_ctrl/image/popupUserCrop.ascx"  %>

<script type="text/javascript" language="javascript">
    function editImageSave(img) {
        var a = $("#<%= tbAuthor.ClientID %>").val();
        var cb = $("#<%= cbActive.ClientID %>").attr('checked');
        var _items = new Array();
        $("#galImgLangDesc div.item").each(function(i, val) {
            var _lan = '' + $(this).find('input:hidden').val();
            var _title = '' + $(this).find('input:text').val();
            var _desc = '' + $(this).find('textarea').val();
            var _item = new Object();
            _item.Lang = _lan;
            _item.Title = _title;
            _item.Desc = _desc;
            Array.add(_items, _item);
        });

        openModalLoading('Shranjujem...');
        WebServiceCWP.SaveImage(img, cwpID, a, cb, _items, onImageEditSuccessSave, onError);
    }
    var _cropSrc = null;


    function SetMaxImageWidth(img, w) {
        if (img.width > w)
            img.width = w;
    }
    $(document).ready(function() {
    SetMaxImageWidth($('#<%= imgMain.ClientID %>')[0], <%= MAX_IMAGE_WIDTH %>) ;

    });

</script>  


<div>
<div class="modPopup_header modPopup_headerSettings">
        <h4>NASTAVITVE SLIKE</h4>
        <p>Uredi (popravi) sliko. Uredi naziv in opis posamezne slike ter prikazovanje slike znotraj galerije.</p>
    </div>
    <div class="modPopup_main">


        <span class="modPopup_mainTitle" >Uredi sliko
        <%if (SM.EM.Security.Permissions.IsPortalAdmin()){ %>
            &nbsp;[št. slike: <%= Image.IMG_ID%>]
        <%} %>
        </span>
        <div >


            <img id="imgMain" runat="server" />
            <input type="hidden" ID="hfImgVirtualUrl" runat="server" />
            <br />
            <div class="buttonwrappersim">
                <%--<asp:LinkButton ID="btCrop" runat="server" CssClass="lbutton lbuttonEdit" OnClientClick="OpenUserCrop(); return false"><span>Popravi sliko</span></asp:LinkButton>
                <asp:LinkButton ID="btDelete" runat="server" CssClass="lbutton lbuttonDelete" OnClick="btDelete_Click" OnClientClick="return confirm('Ali res želite zbrisati sliko?')"><span>Zbriši sliko</span></asp:LinkButton>--%>
                <a href="#" onclick="editCustomCrop('<%= Image.IMG_ID %>', '<%= Image.TYPE_ID %>', '<%= Image.IMG_NAME %>'); return false" class="lbutton lbuttonEdit"><span>Popravi sliko</span></a>
                <a href="#" onclick="deleteImage('<%= Image.IMG_ID %>'); return false" class="lbutton lbuttonDelete"><span>Zbriši sliko</span></a>
            </div>    
        </div>

        <div>
            <span class="modPopup_mainTitle" >Prikazovanje</span>

            <label  for="<%= cbActive.ClientID %>"> <input  id="cbActive" runat="server" name ="cbActive" type="checkbox" />&nbsp;&nbsp;Aktivna (prikazovanje slike na spletni strani)</label>

            <br />
            <br />
            <span class="modPopup_mainTitle" >Ostali podatki</span>

            Avtor slike:&nbsp;
            <input class="modPopup_TBXsettings"  id="tbAuthor" maxlength="256" runat="server" type="text" />
        </div>
            
            <span class="modPopup_mainTitle" >Uredi naziv in opis slike za različne jezike</span>

        <div id="galImgLangDesc">
            <asp:ListView ID="lvList" DataKeyNames="IMG_ID,LANG_ID" runat="server"  InsertItemPosition="None" >
                <LayoutTemplate>
                    <asp:PlaceHolder id="itemPlaceholder" runat="server"></asp:PlaceHolder>
                </LayoutTemplate>
                
                <ItemTemplate>
                    <div class="item" style="float:left; width:260px;">
                        <input type="hidden" value= '<%#  Eval("LANG_ID")%>' />
                           
                        <div style="color:#659700; font-size:15px; margin-bottom:4px;"> <%#  Eval("LANG_DESC")%></div>
                        <span style="font-size:14px;">Naziv slike</span>
                        <br />
                        <input class="modPopup_TBXsettings modPopup_TBXsettingsGalLang"  id="tbTitle" maxlength="256" type="text" value='<%#  Eval("IMG_TITLE")%>' />
                        <br />
                        <span style="font-size:14px;">Opis slike</span> 
                        <br />
                        <textarea class="modPopup_TBXsettings modPopup_TBXsettingsGalLang" rows="3" cols="50"   ><%# Eval("IMG_DESC")%></textarea>
                        <br />
                    </div>
                </ItemTemplate>
            </asp:ListView>
        </div>

        <br />

        <%--<SM:PopupUserCrop id="popupCrop" runat="server"></SM:PopupUserCrop>--%>

    <div class="clearing"></div>
</div>
<div class="modPopup_footer"> 
    <div class="buttonwrapperModal">
        <a class="lbutton lbuttonConfirm" href="#" onclick="editImageSave('<%= Image.IMG_ID %>'); return false"><span>Shrani</span></a>
        <a class="lbutton lbuttonDelete" href="#" onclick="closeModalLoader(); return false" ><span>Prekliči</span></a>
    </div>
</div>
</div>