﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

/// <summary>
/// Summary description for FILE
/// </summary>
public partial class FILE
{
    public string FilePath() {

        string ret = "";
        ret = "~/" + this.FileID.ToString() + Common.SECURE_FILE_ENDING;
        return ret;
    }

    

    public static FILE GetFileByID(Guid  id)
    {
        eMenikDataContext db = new eMenikDataContext();

        return  (from r in db.FILEs where r.FileID == id select r).SingleOrDefault();
    }
    public static IEnumerable< FILE> GetFilesByRefID(Guid id)
    {
        eMenikDataContext db = new eMenikDataContext();

        return (from r in db.FILEs where r.RefID  == id select r);
    }



    public static void DeleteProductFile(Guid id, Guid pageID)
    {
        eMenikDataContext db = new eMenikDataContext();

        PRODUCT_FILE_LANG  file = (from s in db.PRODUCT_FILE_LANGs  
                                from f in db.FILEs                                 
                                where f.FileID == s.FileID && f.UserId == pageID 
                                && s.ProdFileID  == id 
                                   select s).SingleOrDefault();
        if (file == null) return;
        db.PRODUCT_FILE_LANGs .DeleteOnSubmit(file);
        db.SubmitChanges();

    }
    public static void DeleteFile(Guid  id)
    {

        eMenikDataContext db = new eMenikDataContext();

        FILE f = (from s in db.FILEs where s.FileID == id select s).SingleOrDefault();
        if (f == null) return;
        db.FILEs.DeleteOnSubmit(f);
        db.SubmitChanges();

    }

    public static int TotalSize() {
        eMenikDataContext db = new eMenikDataContext();

        return (int) (from f in db.FILEs select f.Size).Sum();
    }




    public class Common {

        public const string SECURE_FILE_ENDING = ".secFile";

        public const string SECURE_FOLDER = "~/UserFiles/_secure/";
        public const string TICKET_FOLDER = SECURE_FOLDER + "ticket/";
        public const string PRODUCT_FILES_FOLDER = "~/UserFiles/product/";


        public class FileType {
            public const short TICKET_ORDER = 1;
            public const short PRODUCT = 2;
        
        }

        public class Render {
            public static string  FileLinkOrder(string href, string title){
                string ret = "";

                ret = "<div><a target=\"_blank\" href=\"" + href + "\" >" + title + "</a></div>";

                return ret;
            }

            public static string ImageHtml(string src, string alt)
            {
                string ret = "";

                ret = "<img src=\"" + SM.BLL.Common.ResolveUrl(src) + "\" alt=\"" + alt +"\" />";
                    
                    //"<div><a target=\"_blank\" href=\"" + href + "\" >" + title + "</a></div>";

                return ret;
            }
            
        
        
        }

        //public static string  RenderCMSHref(int skID){

        //    return "~/cms/zoo/ManageSkupina.aspx?s=" + skID.ToString();
        //}
    
    }
}
