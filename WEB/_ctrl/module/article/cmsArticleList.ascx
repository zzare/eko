﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="cmsArticleList.ascx.cs" Inherits="SM.EM.UI.Controls.cmsArticleList" %>
<%@ Register Assembly="FredCK.FCKeditorV2" Namespace="FredCK.FCKeditorV2" TagPrefix="FCKeditorV2" %>
<%@ Register TagPrefix="SM" TagName="ArticleEdit" Src="~/_ctrl/module/article/cmsArticleEdit.ascx"   %>

<div id="divArticles" runat="server" >
    <asp:ListView ID="listArticles" runat="server" onitemcommand="listArticles_ItemCommand">
        <LayoutTemplate>
            
                <asp:PlaceHolder ID="itemPlaceholder" runat="server"/>

        </LayoutTemplate>
        <ItemTemplate>
            <div class="artBox">
                <div class="artBoxI">
                    <% if (IsEditMode) {%>
                        <a style="position:absolute;" class="divGalImgThumb_btnEdit" href="#" onclick="editCustomCrop('<%# Eval("IMG_ID") %>', '<%# Eval("TYPE_ID") %>'); return false" ></a>
                    <%} %>
                    <a class="AartBox_img" href='<%#  RenderArtHref(Container.DataItem) %>' title='<%#  RenderArtTitle(Container.DataItem) %>'>
                        <img src='<%# SM.BLL.Common.ResolveUrl ( Eval("ART_IMAGE").ToString() )  %>' alt='<%#  RenderArtTitle(Container.DataItem) %>' />
                    </a>
                    <span runat="server"  visible='<%# IsEditMode %>'>

                    <asp:LinkButton ID="btEdit" CommandName="editArticle" CommandArgument='<%# Eval("ART_ID") %>' class="btEditArticle"  runat="server"></asp:LinkButton>
                    <%--<a id="btEdit" href=<%# "javascript:onEdit('mpeArtEdit" + Eval("ART_ID") + "-" + Container.DataItemIndex + "');" %> title="uredi" runat="server" class="btEditArticle">edit</a>--%>
                       </span>          
                                    
                    <h2><a href='<%#  RenderArtHref(Container.DataItem) %>'><%#  RenderArtTitle(Container.DataItem) %></a></h2>
                    <p><%#  RenderArtDesc(Container.DataItem) %></p>

                
                
                
                
                </div>
                
                
                
                
<%--                
                <div runat="server" class="artImg" visible='<%# (bool)( ShowImage && HasImage(  Container.DataItem)) %>' >
                    <% if (IsEditMode) {%>
                        <a style="position:absolute;" class="divGalImgThumb_btnEdit" href="#" onclick="editCustomCrop('<%# Eval("IMG_ID") %>', '<%# Eval("TYPE_ID") %>'); return false" ></a>
                    <%} %>
                    <a href='<%#  RenderArtHref(Container.DataItem) %>' title='<%#  RenderArtTitle(Container.DataItem) %>'>
                        <img src='<%# SM.BLL.Common.ResolveUrl ( Eval("ART_IMAGE").ToString() )  %>' alt='<%#  RenderArtTitle(Container.DataItem) %>' />
                    </a>
                </div>
                <div class="artText">
                <div style="width:90%;">
                    <div runat="server"  visible='<%# IsEditMode %>'>
                        <asp:LinkButton ID="btEdit" CommandName="editArticle" CommandArgument='<%# Eval("ART_ID") %>' class="btEditArticle"  runat="server"></asp:LinkButton>
                        <%--<a id="btEdit" href=<%# "javascript:onEdit('mpeArtEdit" + Eval("ART_ID") + "-" + Container.DataItemIndex + "');" %> title="uredi" runat="server" class="btEditArticle">edit</a>--%>
 
    <%--                 
                    </div>
                    <h2><a href='<%#  RenderArtHref(Container.DataItem) %>'><%#  RenderArtTitle(Container.DataItem) %></a></h2>
                    <p><%#  RenderArtDesc(Container.DataItem) %></p>
                </div>
                </div>
--%>                
                <div class="clearing"></div>
                <div class="artFoot" runat="server"  visible='<%# ShowFooter %>' >
                    <div id="artFootWrapper" class="clearfix" > 
                        <div id="artFootTwoCols" class="clearfix"> 
                            <div id="artFootMain" >            
                                <%# RenderComments(Container.DataItem ) %>
                            </div>
                            <div id="artFootRight" >
                                <%# RenderDatePublished(Container.DataItem ) %>
                            </div>
                        </div> 
                        <div id="artFootLeft" >
                            <%# RenderAuthor(Container.DataItem ) %>
                        </div>
                    </div>
                </div>
            </div>
            
        </ItemTemplate>
    </asp:ListView>
    
    <asp:DataPager ID="dpArt" runat="server" PagedControlID="listArticles" PageSize="2" Visible="false">
        <Fields >
            <asp:TemplatePagerField OnPagerCommand="PagerCommand" >
            <PagerTemplate>
                <asp:Button ID="btPrev" runat="server"  CommandName="Previous" Text="Nazaj" />
                <asp:Button ID="btNext" runat="server" CommandName="Next" Text="Naprej" />
            </PagerTemplate>
            </asp:TemplatePagerField>
        </Fields>
    </asp:DataPager> 

</div>

