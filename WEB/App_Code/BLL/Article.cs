﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

/// <summary>
/// Summary description for Content
/// </summary>
public partial class ARTICLE
{

    public bool IsValid
    {
        get { return (GetRuleViolations().Count() == 0); }
    }

    public IEnumerable<RuleViolation> GetRuleViolations()
    {
        if (this.ART_ID == null || this.ART_ID < 1)
        {
            // new article

        }
        else
        {

        }

        // common
        if (ART_GUID  == null || ART_GUID == Guid.Empty  )
            yield return new RuleViolation("Identifikator ni določen", "ART_GUID");
        if (string.IsNullOrEmpty( ART_TITLE ))
            yield return new RuleViolation("Naslov ni določen", "ART_TITLE");


        yield break;
    }

    partial void OnValidate(System.Data.Linq.ChangeAction action)
    {
        if (!IsValid)
            throw new ApplicationException("Podatki niso pravilno vneseni.");
    }


    public  bool HasImage
    {
        get
        {
            return HasVideo(this.EMBEDD);

        }
    }
    public static bool HasVideo(string src) {
        if (string.IsNullOrWhiteSpace(src))
            return false;

        if (string.IsNullOrWhiteSpace(VIDEO.GetVideoIDFromUrl(src)))
            return false;

        return true;
    
    }


    //public static IEnumerable<ARTICLE> GetArticles(int smpID, bool isEditMode)
    //{
    //    eMenikDataContext db = new eMenikDataContext();

    //    IEnumerable<ARTICLE> arts ;
        
    //    if (isEditMode )
    //        arts = db.em_GetSitemapArticlesByCultureAll(smpID, LANGUAGE.GetCurrentCulture()).ToList();
    //    else
    //        arts = db.em_GetSitemapArticlesByCulture(smpID, LANGUAGE.GetCurrentCulture()).ToList();

    //    return arts ;
    //}
    public static IEnumerable<ARTICLE> GetArticlesPages(int smpID, int pageSize)
    {
        eMenikDataContext db = new eMenikDataContext();

        return db.ARTICLEs.Where(w => w.SMAP_ID == smpID).OrderByDescending(o => o.DATE_ADDED).Take(pageSize);

    }
    //public static IEnumerable<ARTICLE> GetArticlesByCWP(Guid cwp , bool isEditMode)
    //{
    //    eMenikDataContext db = new eMenikDataContext();

    //    IEnumerable<ARTICLE> arts;

    //    if (isEditMode)
    //        arts = db.em_GetSitemapArticlesByCwpByCultureAll (cwp, LANGUAGE.GetCurrentCulture()).ToList();
    //    else
    //        arts = db.em_GetSitemapArticlesByCwpByCulture (cwp, LANGUAGE.GetCurrentCulture()).ToList();

    //    return arts;
    //}
    public static IEnumerable<ARTICLE> GetArticlesByCwpPaged(Guid cwp, bool isEditMode, int pageNum, int pageSize)
    {
        eMenikDataContext db = new eMenikDataContext();

        var list = from c in db.CMS_WEB_PARTs
                   from a in db.ARTICLEs
                   where c.CWP_ID == cwp && a.CWP_ID == c.CWP_ID
                   orderby a.RELEASE_DATE descending , a.DATE_MODIFY descending, a.DATE_ADDED descending 
                   select a;
        if (!isEditMode)
            list = list.Where(w => w.ACTIVE == true);



        //if (isEditMode)
        //    arts = db.em_GetSitemapArticlesByCwpByCultureAllPaged(cwp, LANGUAGE.GetCurrentCulture(), pageNum, pageSize ).ToList();
        //else
        //    arts = db.em_GetSitemapArticlesByCwpByCulturePaged(cwp, LANGUAGE.GetCurrentCulture(), pageNum, pageSize).ToList();

        return list.Skip(pageNum -1).Take(pageSize );
    }

    public static ARTICLE GetArticleByID(int artID)
    {
        eMenikDataContext db = new eMenikDataContext();

        ARTICLE art;

        //art = db.em_GetSitemapArticleByID(artID).FirstOrDefault();  
        art = db.ARTICLEs.SingleOrDefault(w=>w.ART_ID == artID );

        return art;
    }
    public static ARTICLE GetArticleByIDSitemap(int artID, int smp)
    {
        eMenikDataContext db = new eMenikDataContext();

        ARTICLE art;

        //art = db.em_GetSitemapArticleByID(artID).FirstOrDefault();

        art = (from a in db.ARTICLEs
              from cwp in db.CMS_WEB_PARTs
              from s in db.SITEMAPs
              where a.ART_ID == artID && a.CWP_ID == cwp.CWP_ID && cwp.REF_ID == s.SMAP_GUID
              select a).SingleOrDefault();
            
            //db.ARTICLEs.SingleOrDefault(w => w.ART_ID == artID && w.SMAP_ID == smp);

        return art;
    }
    public static ARTICLE GetArticleByID(int artID, Guid userid)
    {
        eMenikDataContext db = new eMenikDataContext();

        ARTICLE art;

        //art = db.em_GetSitemapArticleByID(artID).FirstOrDefault();
        art = (from a in db.ARTICLEs
              from s in db.SITEMAPs
              from m in db.MENUs
              from u in db.EM_USERs
              where a.ART_ID == artID && s.SMAP_ID == a.SMAP_ID && m.MENU_ID == s.MENU_ID && u.USERNAME == m.USERNAME && u.UserId == userid
              select a).SingleOrDefault();
            
        return art;
    }

    public static ARTICLE CreateArticle(int? smap, string user, Guid cwp, string title, string abstr, string body)
    {

        eMenikDataContext db = new eMenikDataContext();

        ARTICLE art = new ARTICLE();

        art.SMAP_ID = smap ;
        art.LANG_ID = LANGUAGE.GetCurrentLang();
        art.ART_GUID = Guid.NewGuid();
        art.CWP_ID= cwp;
        art.ADDED_BY = user;
        art.DATE_ADDED = DateTime.Now; 
        art.DATE_MODIFY = DateTime.Now;
        art. ART_TITLE = title;
        art. ART_ABSTRACT = abstr ;
        art. ART_BODY = body;
        art. RELEASE_DATE = DateTime.Now;
        art.APPROVED = true;
        art.ACTIVE = true;

        db.ARTICLEs.InsertOnSubmit(art);

        db.SubmitChanges();
    
        return art;
    
    }

    public static void DeleteContentBySitemapID(int smpID)
    {
        eMenikDataContext db = new eMenikDataContext();

        IQueryable<ARTICLE> cont = (from c in db.ARTICLEs where c.SMAP_ID == smpID select c);
        db.ARTICLEs.DeleteAllOnSubmit(cont);


        IQueryable<ARTICLE > artCwp = (from c in db.ARTICLEs
                                 from cwp in db.CMS_WEB_PARTs
                                 from smp in db.SITEMAPs
                                 where smp.SMAP_ID == smpID && cwp.REF_ID == smp.SMAP_GUID && c.CWP_ID == cwp.CWP_ID
                                 select c);
        db.ARTICLEs.DeleteAllOnSubmit(artCwp);

        db.SubmitChanges();
    }

    public static void DeleteArticleByID(int artID)
    {
        eMenikDataContext db = new eMenikDataContext();

        ARTICLE art = (from c in db.ARTICLEs where c.ART_ID == artID  select c).SingleOrDefault();
        if (art == null)
            return;
        db.ARTICLEs.DeleteOnSubmit(art);
        db.SubmitChanges();
    }



    public class Common {
        public const string MAIN_GALLERY_ZONE = "art_main_gal";
    
    
    }

    public class Render
    {
        public static string RenderDatePublished(object item)
        {
            Data.ArticleData a = (Data.ArticleData)item;

            DateTime date = a.DATE_ADDED;
            if (a.RELEASE_DATE != null && a.RELEASE_DATE.Value.Date > System.Data.SqlTypes.SqlDateTime.MinValue)
                date = a.RELEASE_DATE.Value;

            //if (date == date.Date) // if time is not set, show only date part
            return string.Format("{0:dd.MM.yyyy} ", date);
            //else
            //  return string.Format("{0:dddd, dd.MM.yyyy ob HH:mm} ", date);
        }

        public static string RenderCategoryHref(object item, string cssClass, string pname)
        {
            string ret = "";


            Data.ArticleData a = (Data.ArticleData)item;

            if (string.IsNullOrEmpty(a.CAT_SML_TITLE) || a.CAT_SMAP_ID < 1)
                return ret;
            string css = "";
            if (!string.IsNullOrEmpty(cssClass))
                css = "class=\""+ cssClass  +"\"";

            return string.Format("<a href=\"{0}\" {2} >{1}</a>", SM.BLL.Common.ResolveUrl( SM.EM.Rewrite.EmenikUserUrl(pname, a.CAT_SMAP_ID, a.CAT_SML_TITLE )), a.CAT_SML_TITLE, css );
            //else
            //  return string.Format("{0:dddd, dd.MM.yyyy ob HH:mm} ", date);
        }

        public static string RenderVideoEmbed(object item, int width=440, int height=258, string style="")
        {
            string ret = "";
            Data.ArticleData art = (Data.ArticleData)item;
            if (string.IsNullOrWhiteSpace(art.EMBEDD))
                return ret;

            string vid = VIDEO.GetVideoIDFromUrl(art.EMBEDD);

            string src = "http://www.youtube.com/embed/" + vid;
            // add wmode
            if (!src.Contains("?"))
                src += "?";
            else
                src += "&";
            src += "wmode=transparent";


            string rendStyle = "";
            if (!string.IsNullOrWhiteSpace(style))
                rendStyle = "style='" + style + "'";

            ret = string.Format("<iframe width='{1}' height='{2}' src='{0}' {3} frameborder='0' allowfullscreen></iframe>", src, width, height, rendStyle  );

            return ret;
        }
    }

    //partial void OnValidate(System.Data.Linq.ChangeAction action)
    //{
    //}

}
