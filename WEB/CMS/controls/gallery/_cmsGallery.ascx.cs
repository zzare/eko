﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SM.EM.UI.CMS;

namespace SM.EM.UI.CMS.Controls
{
    public partial class CMS_cmsGallery : BaseControl
    {
        public int _smapID = -1;
        public string _langID = "-1";
        public string UserName { get; set; }

        public int GalleryID {
            get {
                if (ViewState["GalleryID"] == null) return -1;
                return int.Parse(ViewState["GalleryID"].ToString());
            }

            set {ViewState["GalleryID"] = value;}
        }
        public int GalleryTypeID
        {
            get
            {
                if (ViewState["GalleryTypeID"] == null) return SM.BLL.Common.GalleryType.DefaultGallery ;
                return int.Parse(ViewState["GalleryTypeID"].ToString());
            }

            set { ViewState["GalleryTypeID"] = value; }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            // event handlers
            objImageNew.OnImageAdded += new EventHandler(objImageNew_OnImageAdded);
            objImageNew.OnImageAddCanceled += new EventHandler(objImageNew_OnImageAddCanceled);
            objImageEdit.OnImageEdited += new EventHandler(objImageEdit_OnImageEdited);
            objImageEdit.OnImageEditCanceled += new EventHandler(objImageEdit_OnImageEditCanceled);
            objImageList.OnEdit += new SM.UI.Sandbox.Controls._cmsImageList.OnEditEventHandler(objImageList_OnEdit);
            objGalleryEdit.OnGalleryEdited += new EventHandler(objGalleryEdit_OnGalleryEdited);
            objGalleryEdit.OnGalleryEditCanceled += new EventHandler(objGalleryEdit_OnGalleryEditCanceled);

            // init
            objImageNew.GalleryID  = GalleryID ;
            objImageNew.UploadFolder = "~/userfiles/testzip/";
            objImageList.GalleryID = GalleryID;
            objImageList.LangID = _langID;
            objImageList.ImageTypeID = SM.BLL.Common.ImageType.CMSthumb;
            objImageList.SelectedImageID = objImageEdit.ImageID;
            objGalleryEdit.GalleryID = GalleryID;

            

            if (!IsPostBack)
            {
                //objImageList.BindData();

            }

        }

        void objGalleryEdit_OnGalleryEditCanceled(object sender, EventArgs e)
        {
            HideEditGallery();
        }

        void objGalleryEdit_OnGalleryEdited(object sender, EventArgs e)
        {
            HideEditGallery();
        }

        void objImageNew_OnImageAddCanceled(object sender, EventArgs e)
        {
            HideNew();
                
        }

        void objImageEdit_OnImageEditCanceled(object sender, EventArgs e)
        {
            HideEditImage();
            objImageEdit.ImageID = 0;
            objImageList.SelectedImageID = objImageEdit.ImageID;

            // bind to deselect class
            objImageList.BindData();

        }

        void objImageEdit_OnImageEdited(object sender, EventArgs e)
        {
            HideEditImage();
            objImageEdit.ImageID = 0;
            objImageList.SelectedImageID = objImageEdit.ImageID;
            // bind to deselect class
            objImageList.BindData();
        }

        void objImageList_OnEdit(object sender, SM.BLL.Common.ClickIDEventArgs e)
        {
            ShowEditImage();
            objImageEdit.ImageID = e.ID;
            objImageEdit.BindData();
            objImageList.SelectedImageID = objImageEdit.ImageID;

        }

        void objImageNew_OnImageAdded(object sender, EventArgs e)
        {
            litStatus.Text = "Image(s) has been added.";

            // hide add image
            HideNew();


            // refresh image list
            objImageList.BindData();
        }

        public void LoadTab() {

            // load gallery data
            if (_smapID > 0)
            {
                GALLERY gal = GALLERY.GetGalleryBySitemap(_smapID);
                if (gal != null)
                {

                    GalleryID = gal.GAL_ID;

                    // load images
                    objImageList.ImageTypeID = SM.BLL.Common.ImageType.CMSthumb;
                    objImageList.GalleryID = GalleryID;
                    objImageList.BindData();
                }
            }

        }

        // NEW IMAGE
        protected void btNewImage_OnClick(object sender, EventArgs e) {
            // show add image
            ShowNew();

            // create gallery if not set
            if (GalleryID < 1)
            {
                GALLERY gal =  GALLERY.CreateGalleryForType(GalleryTypeID, UserName, _smapID );
                GalleryID = gal.GAL_ID;
            }
        }
        protected void btCancelNew_Click(object o, EventArgs sender)
        {
            objImageNew.Cancel();
            HideNew();
        }
        protected void btUpload_Click(object o, EventArgs sender)
        {
            objImageNew.Save();
        }

        protected void HideNew()
        {
            objImageNew.Visible = false;
            btCancelNew.Visible = false;
            btUpload.Visible = false;

            // init data
            objImageNew.BindData();
        }
        protected void ShowNew()
        {
            objImageNew.Visible = true;
            btCancelNew.Visible = true;
            btUpload.Visible = true;

            // hide others
            HideEditImage();
            HideEditGallery();

        }

        // EDIT IMAGE
        protected void btSaveImage_Click(object sender, EventArgs e)
        {
            objImageEdit.Save();
            

        }
        protected void btCancelImage_Click(object sender, EventArgs e)
        {
            objImageEdit.Cancel();
            

        }
        protected void ShowEditImage()
        {
            objImageEdit.Visible = true;
            btSaveImage .Visible = true;
            btCancelImage.Visible = true;

            // hide others
            HideEditGallery();
            HideNew();
        }
        protected void HideEditImage()
        {
            objImageEdit.Visible = false ;
            btSaveImage.Visible = false;
            btCancelImage.Visible = false;
        }

        // EDIT GALLERY
        protected void btEditGallery_OnClick(object sender, EventArgs e)
        {
            // show edit gallery
            ShowEditGallery();
            objGalleryEdit.BindData();

        }
        protected void ShowEditGallery()
        {
            objGalleryEdit.Visible = true;
            btSaveGallery.Visible = true;
            btCancelGallery.Visible = true;

            // hide others
            HideEditImage();
            HideNew();

        }
        protected void HideEditGallery()
        {
            objGalleryEdit.Visible = false;
            btSaveGallery.Visible = false;
            btCancelGallery.Visible = false;
        }
        protected void btSaveGallery_Click(object sender, EventArgs e)
        {
            objGalleryEdit.Save();
        }
        protected void btCancelGallery_Click(object sender, EventArgs e)
        {
            objGalleryEdit.Cancel();
        }

        // LIST
        protected void btDelete_OnClick(object sender, EventArgs e)
        {
            foreach (int imgID in objImageList.GetSelectedItems) { 
                // delete image
                IMAGE.DeleteImageFromGallery(imgID, GalleryID); 

            }
            if (objImageList.GetSelectedItems.Count > 0)
            {
                // refresh list
                objImageList.BindData();
            }
        }
    }
}