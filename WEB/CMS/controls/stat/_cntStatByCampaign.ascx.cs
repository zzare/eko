﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.DataVisualization.Charting;


public partial class CMS_controls_stat_cntStatByCampaign : BaseControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        InitChart();

    }

    protected void InitChart() {
        DateTime dateStart = DateTime.Now.AddMonths  (-3);



    
        // get chart data
        eMenikDataContext db  = new eMenikDataContext ();
        // registered
        var dReg = from u in db.EM_USERs
                   where u.DATE_REG > dateStart
                   group u by new {BonusID =  u.BONUS_USER_ID, u.DATE_REG.Value.Month, u.DATE_REG.Value.Year  } into gu
                   join mc in db.MARKETING_CAMPAIGNs on gu.Key.BonusID equals mc.CampaignID into mcd
                   from mcdo in mcd.DefaultIfEmpty()
                   select new { UserCount = gu.Count(), DateReg = new DateTime(gu.Key.Year, gu.Key.Month, 1), BonusID = (gu.Key.BonusID ?? Guid.Empty), Campaign = (mcdo.Name ?? "1dva3.si") };
        // orders
        var dOrd = from o in db.SHOPPING_ORDERs
                   from u in db.EM_USERs
                   where o.UserId == u.UserId && o.DateStart > dateStart && o.OrderStatus >= SHOPPING_ORDER.Common.Status.CONFIRMED
                   group o by new { BonusID = u.BONUS_USER_ID ,  o.DateStart.Month, o.DateStart.Year } into gu
                   join mc in db.MARKETING_CAMPAIGNs on gu.Key.BonusID equals mc.CampaignID into mcd
                   from mcdo in mcd.DefaultIfEmpty()
                   select new { OrderCount = gu.Count(), OrderSum = gu.Sum(w => w.Total), BonusID = (gu.Key.BonusID ?? Guid.Empty), DateOrder = new DateTime(gu.Key.Year, gu.Key.Month, 1), Campaign = (mcdo.Name ?? "1dva3.si") };
//                   select new { OrderSum = gu.Sum(w => w.Total), BonusID = (gu.Key.BonusID ?? Guid.Empty), DateOrder = new DateTime(gu.Key.Year, gu.Key.Month, 1),  };
        // payments
        var dPay = from o in db.SHOPPING_ORDERs
                   from u in db.EM_USERs
                   where o.UserId == u.UserId && o.DatePayed > dateStart && o.OrderStatus >= SHOPPING_ORDER.Common.Status.PAYED && o.Payed == true
                   group o by new { BonusID = u.BONUS_USER_ID, o.DatePayed.Value.Month, o.DatePayed.Value.Year } into gu
                   join mc in db.MARKETING_CAMPAIGNs on gu.Key.BonusID equals mc.CampaignID into mcd
                   from mcdo in mcd.DefaultIfEmpty()
                   select new { OrderCount = gu.Count(), OrderSum = gu.Sum(w => w.Total), DateOrder = new DateTime(gu.Key.Year, gu.Key.Month, 1), Campaign = (mcdo.Name ?? "1dva3.si") };




    

        // USERS BY CAMPAIGN
        chUsersByCampaign.ChartAreas[0].AxisX.Interval = 1;
        chUsersByCampaign.ChartAreas[0].AxisX.IntervalType = DateTimeIntervalType.Months ;
        chUsersByCampaign.ChartAreas[0].AxisX.MajorGrid.LineColor = System.Drawing.Color.FromArgb(200, 200, 200);
        chUsersByCampaign.ChartAreas[0].AxisY.MajorGrid.LineColor = System.Drawing.Color.FromArgb(200, 200, 200);
        //chUsersByMonth.ChartAreas[0].AxisX.IntervalOffset = 0.5;

        // Show data points labels
        //chUsersByMonth.Series["Series1"].Points.DataBind(data, "DateReg", "UserCount", "Tooltip=UserCount");
        chUsersByCampaign.DataBindCrossTable(dReg, "Campaign", "DateReg", "UserCount", "Tooltip=Campaign, Label=UserCount, LegendText=Campaign");
        chUsersByCampaign.Series[0].AxisLabel = "#VALX{MMMM}";
        //chUsersByCampaign.ChartAreas[0].Area3DStyle.Enable3D = true;


        // ORDERS
        chOrders.ChartAreas[0].AxisX.Interval = 1;
        chOrders.ChartAreas[0].AxisX.IntervalType = DateTimeIntervalType.Months;
        chOrders.ChartAreas[0].AxisX.MajorGrid.LineColor = System.Drawing.Color.FromArgb(200, 200, 200);
        chOrders.ChartAreas[0].AxisY.MajorGrid.LineColor = System.Drawing.Color.FromArgb(200, 200, 200);
        //chUsersByMonth.ChartAreas[0].AxisX.IntervalOffset = 0.5;

        // Show data points labels
        chOrders.DataBindCrossTable(dOrd, "Campaign", "DateOrder", "OrderSum", "Tooltip=Campaign, Label=OrderSum{C2}, LegendText=Campaign");

         chOrders.Series[0].AxisLabel = "#VALX{MMMM}";

         // PAYMENTS
         chPayments.ChartAreas[0].AxisX.Interval = 1;
         chPayments.ChartAreas[0].AxisX.IntervalType = DateTimeIntervalType.Months;
         chPayments.ChartAreas[0].AxisX.MajorGrid.LineColor = System.Drawing.Color.FromArgb(200, 200, 200);
         chPayments.ChartAreas[0].AxisY.MajorGrid.LineColor = System.Drawing.Color.FromArgb(200, 200, 200);
         //chUsersByMonth.ChartAreas[0].AxisX.IntervalOffset = 0.5;

         // Show data points labels
         chPayments.DataBindCrossTable(dPay, "Campaign", "DateOrder", "OrderSum", "Tooltip=Campaign, Label=OrderSum{C2}, LegendText=Campaign");

         //        chOrders.Series.Add(new Series());
         //      chOrders.Series[0].Points.DataBind(dOrd, "DateOrder", "OrderSum", "Tooltip=OrderSum");
         if (chPayments.Series.Count > 0)
             chPayments.Series[0].AxisLabel = "#VALX{MMMM}";
        
        

    }
}
