﻿<%@ Page EnableViewState="false" Language="C#" AutoEventWireup="true" CodeFile="EditMap.aspx.cs" Inherits="_ctrl_module_map_page_EditMap" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css"  >
        body {
            background:transparent !important;
        }        
        h3 {font-size:15px;padding:5px 0;}
        h1, h2, h3 {font-family:Tahoma;font-weight:normal;}
        * {
            margin:0; 
            padding:0; 
        }
        p{color:#666666;font-family:Tahoma;font-size:12px;}
        input.map_TBXsettings {
            color:#333333;
            margin:0 10px 0 0;
            padding:4px 4px 0px 4px;
            vertical-align:middle;
            min-height:20px;
            height:auto !important; 
            height:20px;
            }
        table.mapTABLE{font-size:13px; font-family:Tahoma; color:#666;}
        table.mapTABLE td{padding:2px;}
        a.map_AHREF{color:#0066CC;}
        a.map_AHREF:hover{text-decoration:none;}
    </style>
    
</head>
<body>
    <script type="text/javascript">
        var map = null;
        var mLat = null;
        var mLong = null;
        var iOpen = true;
        var editID = null;
        var marker = null;

        var k = '<%= Key %>';
        var ru = '<%= ReturnUrl %>';


        function getMapTypeByUrlArg(u) {
            if (!u)
                return G_NORMAL_MAP;

            if (u == 'm')
                return G_NORMAL_MAP;
            if (u == 'k')
                return G_SATELLITE_MAP;
            if (u == 'h')
                return G_HYBRID_MAP;
            if (u == 'p')
                return G_PHYSICAL_MAP;

            return G_NORMAL_MAP;
        }

        function map_edit(id, c, lat, lng, z, mx, my, title, addr, h, w, srch, i, t) {
            var hMin = 200;
            if (h < hMin) h = hMin;

            editID = id;
            var mhtml = GetMarkerHtml(title, addr );
            map_load(id, c, lat, lng, z, mx, my, mhtml, h, w, i, t);

            $(c).width($(c).width());
            $(c).resizable({
                handles: 's, se',
//                alsoResize: $(c).parent(),
//                alsoResize: $("body"),
                minWidth: w,
                maxWidth: w,
                minHeight: hMin,
                stop: function(event, ui) { map.checkResize(); }
            });


            $("#tbTitle").val(title);
            $("#tbAddress").val(addr );

            if (!lat || !lng) {
                showAddress(srch, GetMarkerHtml(title, addr));
                $("#tbSrch").val(srch);
            }            


        }

        function map_load(id, c, lat, lng, z, mx, my, mhtml, h, w, i, t) {
            if (GBrowserIsCompatible()) {

                var hMin = 200;
                if (h < hMin) h = hMin;

                if (w > 1)
                    $(c).width(w);
                $(c).height(h);


                map = new GMap2(c);

                var loc = new GLatLng(lat, lng);
                map_loadMarker(map, mx, my, mhtml, i);
                map.setCenter(loc, z);
                map.setUIToDefault();
                map.setMapType(getMapTypeByUrlArg(t));
            }

        }
        function map_loadMarker(mp, lat, lng, mhtml, openI) {

            //var marker = new GMarker(new GLatLng(lat, lng), { draggable: true });

            if (marker != null)
                mp.removeOverlay(marker);

            marker = new GMarker(new GLatLng(lat, lng), { draggable: true });                
                
            mLat = lat;
            mLong = lng;
            GEvent.addListener(marker, "dragstart", function() { mp.closeInfoWindow(); });
            GEvent.addListener(marker, "dragend", function() {
                marker.openInfoWindowHtml(mhtml, { maxWidth: 150 });
                mLat = marker.getPoint().lat();
                mLong = marker.getPoint().lng();
            });
            GEvent.addListener(marker, "click", function() {
                marker.openInfoWindowHtml(mhtml, { maxWidth: 150 });
            });
            GEvent.addListener(mp, "infowindowclose", function() { iOpen = false; });
            GEvent.addListener(mp, "infowindowopen", function() { iOpen = true; });

            mp.addOverlay(marker);
            if (openI == true)
                marker.openInfoWindowHtml(mhtml, { maxWidth: 150 });


        }


        function showAddress(address, mhtml) {
            if (mhtml == null) mhtml = address;
            var geocoder = new GClientGeocoder();

            if (geocoder) {
                geocoder.getLatLng(address, function(point) {
                    if (!point) {
                        alert(" Naslov: " + address + " ni najden");
                    } else {
                        map.setCenter(point, 13);
                        map_loadMarker(map, point.lat(), point.lng(), mhtml, true);
                    }
                });
            }
        }

        function saveMap() {
            
            var lat = map.getCenter().lat();
            var lng = map.getCenter().lng();
            var z = map.getZoom();
            var c = $('#map');

            var tit = $("#tbTitle").val();
            var addr = $("#tbAddress").val();
            var srch = $("#tbSrch").val();
            var msg = GetMarkerHtml(tit, addr);

            WebServiceEMENIK.UpdateCwpMapEnc(k, editID, lat, lng, map.getZoom(), mLat, mLong, tit, addr, srch, c.height(), -1, iOpen, map.getCurrentMapType().getUrlArg(), OnMapUpdated);
        }
        function OnMapUpdated() {
            parent.window.location.href = ru;
        }

        function GetMarkerHtml(title, addr) {
            if (addr && addr.length != 1)
                addr = "<p>" + addr + "</p>";
            return "<h3>" + title + "</h3>" + addr;
        }

        
         
    </script>
    <form id="form1" runat="server">
    
            <asp:ScriptManager ID="sm" runat="server" ScriptMode="Release" EnableScriptGlobalization="true" EnableScriptLocalization="true">
            <Services>
                <asp:ServiceReference Path="~/_inc/webservice/WebServiceEMENIK.asmx" />
                <asp:ServiceReference Path="~/_inc/webservice/WebServiceCWP.asmx" />
            </Services>
        </asp:ScriptManager>  
   


    
    <div style="width:700px; font-size:13px; font-family:Tahoma; color:#666;" class="modPopup_mainMap">
        <span class="modPopup_mainTitle" >Uredi zemljevid</span>

        <table class="mapTABLE" >
            <tr>
                <td>
                    Skoči na naslov:
                </td>
                
                <td>
                        <input id="tbSrch" class="map_TBXsettings" type="text" value="Ljubljana" name="address" size="60"/>        
                        <a href="javascript:void(0)" onclick="showAddress($('#tbSrch').val()); return false;" class="map_AHREF">Prikaži na zemljevidu</a>


                </td>
            </tr>
        
        </table>
        <div id="map" ></div>
        <div style="margin-top:4px;">* Če želiš spremeniti velikost (višino) zemljevida, z miško premakni spodnji desni rob zemljevida.</div>
        
        <div class="buttonwrapperModal">
            <a href="javascript:void(0)" class="lbutton lbuttonConfirm" onclick="saveMap(); return false;" ><span>Shrani spremembe</span></a>
         </div>

        
        <span class="modPopup_mainTitle" >Prikazni tekst (v balončku)</span>

        <table class="mapTABLE">
            <tr>
                <td>
                        Naziv:
                </td>
                <td>
                        <input id="tbTitle" class="map_TBXsettings" type="text" name="Title" size="30"/>
                </td>
            </tr>
             <tr>
                <td>
                        Naslov:
                </td>
                <td>
                        <input id="tbAddress" class="map_TBXsettings" type="text" name="Address" size="60"/>
                </td>
            </tr>
       
        
        </table>
        <br />

        <br />        
        <br />
    </div>
   
    </form>
</body>
</html>
