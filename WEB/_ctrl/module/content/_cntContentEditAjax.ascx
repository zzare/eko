﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_cntContentEditAjax.ascx.cs" Inherits="_ctrl_module_content_cntContentEditAjax" %>
<%@ Register Assembly="FredCK.FCKeditorV2" Namespace="FredCK.FCKeditorV2" TagPrefix="FCKeditorV2" %>
<%@ Register Namespace="SM.UI.Controls" TagPrefix="SM"  %>

<script type="text/javascript">

    function editContentSave() {
        conBody = null;

        var oEditor = FCKeditorAPI.GetInstance('<%= fckBody.ClientID %>');
        if (oEditor) { conBody = oEditor.GetXHTML(true); }

        openModalLoading('Shranjujem...');
        WebServiceCWP.SaveContent(conID, conBody, onContentSaveSuccess, onContentSaveError);
    }

</script>

<div class="">

    <div class="modPopup_header modPopup_headerEdit">
        <h4>UREDI VSEBINO</h4>
        <p>Uredi vsebino trenutne spletne strani</p>
    </div>
    <div class="modPopup_mainFCK">
        <FCKeditorV2:FCKeditor  Width="100%" ID="fckBody" ToolbarSet="CMS_Content" runat="server" Height="400px"  ></FCKeditorV2:FCKeditor>
    </div>   
    <div class="modPopup_footer"> 
        <div class="buttonwrapperModal">
            <a class="lbutton lbuttonConfirm" href="#" onclick="editContentSave(); return false"><span>Shrani</span></a>
            <a class="lbutton lbuttonDelete" href="#" onclick="closeModalLoader(); return false" ><span>Prekliči</span></a>
        </div>
    </div>
</div>
