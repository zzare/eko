﻿<%@ Page Language="C#" MasterPageFile="~/CMS/CMS.master" AutoEventWireup="true" CodeFile="ManageError.aspx.cs" Inherits="SM.UI.CMS.CMS_Intranet_ManageError" Title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register TagPrefix="EM" TagName="ErrorList" Src="~/CMS/controls/error/_cmsErrorList.ascx"   %>


<asp:Content ID="Content3" ContentPlaceHolderID="cph" Runat="Server">

     <asp:PlaceHolder ID="phList" runat="server">

        <asp:Panel ID="panSListHD" runat="server" CssClass="cpeListHD" >
            <asp:Image ID="imgCollapse" runat="server" />
           Error 
        </asp:Panel>

        <asp:Panel ID="panSList" runat="server">

            <EM:ErrorList ID="objErrorList" runat="server" ShowCheckboxColumn="false" ShowManageColumn="true" ShowEditColumn="true" />
        </asp:Panel>    

        <cc1:CollapsiblePanelExtender ID="cpeQList" runat="server" Collapsed="false" CollapseControlID="panSListHD" CollapsedImage="~/_res/images/icon/plus.gif" ExpandedImage="~/_res/images/icon/minus.gif" ImageControlID="imgCollapse" ExpandControlID="panSListHD" TargetControlID="panSList" ExpandDirection="Vertical" CollapsedText="Expand" ExpandedText="Collapse" >
        </cc1:CollapsiblePanelExtender>    
                
    </asp:PlaceHolder>                


    <asp:PlaceHolder ID="phDetails" runat="server">
        <asp:Literal ID="litDetail" runat="server"></asp:Literal>
    
    </asp:PlaceHolder>

           





</asp:Content>