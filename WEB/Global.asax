﻿<%@ Application Language="C#" %>
<%@ Import Namespace="System.Web.Routing" %>
<script runat="server">

    void Application_Start(object sender, EventArgs e) 
    {
        // Code that runs on application startup

        // start scheduled service
        SM.BLL.ScheduleTask.RegisterCacheEntry();
        
        // create all domain rewtrite rules
        //SM.EM.Rewrite.Domain.AddAllDomainRewrites();
        UrlRewritingNet.Web.UrlRewriting.BeforeAddingRules += CustomRules;

        // routing
        RegisterRoutes(RouteTable.Routes);
    }

    public static void RegisterRoutes(RouteCollection routes)
    {
        //routes.MapPageRoute("home", "", "~/c/default.aspx");
        routes.MapPageRoute("prijava", "prijava", "~/_em/content/membership/prijava.aspx");
        routes.MapPageRoute("registriraj", "registriraj", "~/_em/content/membership/register-customer.aspx");
        //routes.MapPageRoute("vstopi", "", "~/_em/content/membership/prijava.aspx");
        
        

        //routes.MapPageRoute("si/svetlo-pivo", "si/svetlo-pivo", "~/_em/content/default.aspx?c=sl-SI&smp=1434&pname=union");
        //routes.MapPageRoute("ustvari", "ustvari", "~/c/default.aspx");


        //routes.MapPageRoute("galerija-paging", "galerija/{sorting}/{paging}", "~/c/galerija.aspx");
        //routes.MapPageRoute("galerija-sorting", "galerija/{sorting}", "~/c/galerija.aspx");
        //routes.MapPageRoute("galerija", "galerija", "~/c/galerija.aspx");

        ////        routes.MapPageRoute("junak", "superjunak/{junakID}", "~/c/superjunak.aspx");
        //routes.MapPageRoute("junak", "superjunak/{junakID}", "~/c/galerija.aspx");



        //routes.MapPageRoute("prijava", "prijava", "~/c/prijava.aspx");

        //routes.MapPageRoute("admin", "admin", "~/sm/admin/admin.aspx");


    }


    void CustomRules(object sender, EventArgs e)
    {
        //SM.EM.Rewrite.Domain.AddAllDomainRewrites();
        SM.EM.Rewrite.Domain.AddAllRewrites();
    }     
    
    void Application_End(object sender, EventArgs e) 
    {
        //  Code that runs on application shutdown

    }
        
    void Application_Error(object sender, EventArgs e) 
    {
        // Code that runs when an unhandled error occurs


        Exception ex = Server.GetLastError().GetBaseException();
        HttpException serverError = Server.GetLastError() as HttpException;


        string user = "";
        string ip = "";
        if (User != null && User.Identity.IsAuthenticated)
            user = User.Identity.Name;
        ip = SM.EM.BLL.BizObject.CurrentUserIP;


        if (!SM.BLL.Common.Emenik.Data.EnableErrorLogging())
            return;

        string url = "";
        if (HttpContext.Current != null && HttpContext.Current.Request != null && HttpContext.Current.Request.Url != null)
            url = HttpContext.Current.Request.Url.AbsoluteUri; 
        string trace = ex.StackTrace + "<br/><br/>SOURCE::" + ex.Source + "<br/><br/>Environment.StackTrace::" + Environment.StackTrace ;
        trace += "<br/><br/>ToString::" + ex.ToString() + "<br/><br/>URL::" + url;
        // add browser version
        if (HttpContext.Current != null && HttpContext.Current.Request != null && HttpContext.Current.Request.Browser != null)
        {
            trace += "<br/><br/>BROWSER::" + HttpContext.Current.Request.Browser.Browser;
            trace += "<br/>version::" + HttpContext.Current.Request.Browser.Version ;
            trace += "<br/>platform::" + HttpContext.Current.Request.Browser.Platform;
            trace += "<br/>clr::" + HttpContext.Current.Request.Browser.ClrVersion.ToString();
            trace += "<br/>URL referrer::" + HttpContext.Current.Request.UrlReferrer;
            
        }

        trace += "<br/>HTTP CODE::" + serverError.GetHttpCode().ToString();
        
        // log error
        ERROR_LOG.LogError(ex.Message + "<br/><br/> TargetSite::" + ex.TargetSite, trace , user, ip);


        int errorCode = serverError.GetHttpCode();
        
        // clear exception
        Server.ClearError();

        Server.Transfer("~/_err/Error.aspx?e=" + errorCode.ToString());        

    }

    void Session_Start(object sender, EventArgs e) 
    {
        // Code that runs when a new session is started

    }

    void Session_End(object sender, EventArgs e) 
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.

    }

    protected void Application_BeginRequest(object sender, EventArgs e)
    {
        //if (HttpContext.Current.Request.Url.ToString().Contains("tskStart.aspx"))
        //{
        //    // Add the item in cache and when succesful, do the work.
        //    SM.BLL.ScheduleTask.RegisterCacheEntry();
        //}

    }

    public void Profile_OnMigrateAnonymous(object sender, ProfileMigrateEventArgs args)
    {
//        ProfileCommon anonymousProfile = Profile.GetProfile(args.AnonymousID);


        ////////


        // update shopping cart
        ProductRep prep = new ProductRep();
        Guid annID = new Guid(args.AnonymousID);
        Guid newID = SM.EM.BLL.Membership.GetCurrentUserID();
        
        prep.UpdateCartItemUser(annID, newID, null, null );
        prep.Save();

        // Delete the anonymous profile. If the anonymous ID is not 
        // needed in the rest of the site, remove the anonymous cookie.
        ProfileManager.DeleteProfile(args.AnonymousID);
        AnonymousIdentificationModule.ClearAnonymousIdentifier();

        // Delete the user row that was created for the anonymous user.
        Membership.DeleteUser(args.AnonymousID, true);

    }

    public override string GetVaryByCustomString(HttpContext context, string arg)
    {
        string[] args = arg.Split(";".ToCharArray());        
        
        string ret = "";
        foreach (string item in args)
        {
            if (item == "cms_user")
            {
                if (context.User.Identity.IsAuthenticated)
                {
                    // new cache for each user, and then disable caching programmatically
                    //if (context.User.IsInRole("em_user"))
                    //    ret += "em_user=" + context.User.Identity.Name;
                    if (SM.EM.Security.Permissions.IsCMSeditor())
                        ret += "cms_user=" + context.User.Identity.Name;

                }
            }
            if (item == "ie6")
            {
                if (context.Request.Browser.Browser == "IE" && context.Request.Browser.MajorVersion <= 6)
                {
                    ret += ";ie6;";
                }
            }
            if (item == "req_login")
            {
                //if (context.User.Identity.IsAuthenticated)
                {
                    // new cache for each user, and then disable caching programmatically
                    //if (context.User.IsInRole("em_user"))
                    //    ret += "em_user=" + context.User.Identity.Name;
                    // if (SM.EM.Security.Permissions.IsCMSeditor())
                    ret += ";req_login=" + context.User.Identity.IsAuthenticated;

                }
            }            
        }
        return ret + base.GetVaryByCustomString(context, arg);
    }    
    
       
</script>
