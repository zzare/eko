﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class _err_Error : SM.EM.UI.BasePage 
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        CheckPermissions = false;

    }
    protected void Page_Load(object sender, EventArgs e)
    {
        SetTitle("Error");

        if (!IsPostBack)
        {
            int code = -1;
            code = int.Parse(Request.QueryString["e"]);
            
            if (code != -1)
                lbText.Text = ERROR_LOG.RenderError(code);
            else
                lbText.Text = "Stran ne obstaja.";

            // page not found
            if (code == 404)
            {
                Response.StatusCode = 404;
                Response.StatusDescription = "Page not found";
            }


        }
    }
}
