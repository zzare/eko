﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _ctrl_module_poll_viewPollVote : BaseControlView
{

    public object Data;
    public object[] Parameters;
    protected List<POLL_CHOICE> PollList;
    protected string LangID;
    protected Guid PollID;

    protected void Page_Load(object sender, EventArgs e)
    {
        LoadPoll();
    }



    protected void LoadPoll() {

        if (Data == null)
            return;

        // data
        PollList = ((IEnumerable<POLL_CHOICE>)Data).ToList();

        if (Parameters != null)
        {
            if (Parameters[0] != null)
                PollID = new Guid (Parameters[0].ToString());
            if (Parameters.Length > 1 && Parameters[1] != null)
                LangID = Parameters[1].ToString();
        }


        // load poll choices
        lvList.DataSource = PollList;
        lvList.DataBind();

        // show/hide
        if (lvList.Items.Count > 0)
        {
            phEmpty.Visible = false;
            phOK.Visible = true;
        }
        else {
            phEmpty.Visible = true;
            phOK.Visible = false;

        
        }
    }



}
