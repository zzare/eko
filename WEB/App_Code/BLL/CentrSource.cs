﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using UrlRewritingNet.Web;
using System.Net;
using System.IO;
using System.Xml;
using System.Text;
using System.Globalization;

/// <summary>
/// Summary description for CentrSource
/// </summary>

public class Centrsource
{



    public class ExternalPurchase
    {

        public string purchaseid;
        public string description;
        public decimal totalprice;
        public string SecretKey;

        public int lastStatus;
        public int lastError;
        public string lastXmlResponse;


        protected string m_csepr;
        public string csepr
        {
            get
            {
                if (m_csepr == null || string.IsNullOrEmpty(m_csepr.Trim()))
                    m_csepr = HttpContext.Current.Request.Cookies["csepr"].Value;

                return m_csepr;
            }
            set { m_csepr = value; }
        }

        
        public ExternalPurchase()
        {
        }

        public string Process()
        {
            string result = "";

            try
            {

                string csUrl = "http://si.centrsource.com/services/external-purchase/external-purchase.asmx";
                string body = getBody();

                HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(csUrl);

                //System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
                Encoding enc = Encoding.GetEncoding("utf-8");

                byte[] bytesToWrite = enc.GetBytes(body);

                objRequest.Headers.Add("SOAPAction", "\"http://centrsource.com/service/external-purchase/RegisterExternalPurchase\"");
                objRequest.ContentType = "text/xml;charset=\"utf-8\"";
                //objRequest.Accept = "text/xml";
                objRequest.Method = "POST";
                objRequest.ContentLength = bytesToWrite.Length;

                //send
                using (Stream newStream = objRequest.GetRequestStream())
                {
                    newStream.Write(bytesToWrite, 0, bytesToWrite.Length);
                    newStream.Close();
                }


                // get response
                WebResponse response = objRequest.GetResponse();
                Stream responseStream = response.GetResponseStream();

                // cerate XML from response
                XmlDocument myXMLDocument = new XmlDocument();
                XmlTextReader myXMLReader = null;
                myXMLReader = new XmlTextReader(responseStream);
                myXMLDocument.Load(myXMLReader);


                // get result
                result = processResponse(myXMLDocument, "ExternalPurchaseID");

            }

            catch (WebException e)
            {
                string err = "";
                if (e.Status == WebExceptionStatus.ProtocolError)
                {
                    err += string.Format("Status Code : {0}", ((HttpWebResponse)e.Response).StatusCode);
                    err += string.Format("Status Description : {0}", ((HttpWebResponse)e.Response).StatusDescription);
                }

                err += "<br/>response::";
                if (e.Response != null)
                {
                    using (WebResponse response = e.Response)
                    {
                        Stream data = response.GetResponseStream();
                        using (StreamReader reader = new StreamReader(data))
                        {
                            err += reader.ReadToEnd();
                        }
                    }
                }
                err += "<br/>";
                result = err +  e.Message ; //processResponse(myXMLDocument, "faultstring");
                result += "<br/>Xml body:" + HttpUtility.HtmlEncode( getBody());

                string user = "";
                string ip = "";
                if (HttpContext.Current != null){
                if (HttpContext.Current.User != null && HttpContext.Current.User.Identity.IsAuthenticated)
                    user = HttpContext.Current.User.Identity.Name;
                }
                ip = SM.EM.BLL.BizObject.CurrentUserIP;

                ERROR_LOG.LogError("CentrSource SOAP error: " + e.Message, result, user, ip);
            }
            //catch(Exception e) {
            //    Console.WriteLine(e.Message);
            //}


            return result;


        }



        public string getBody()
        {
            string original = "";
            string body = "";
            CultureInfo usc = new CultureInfo("en-US");

            
            body = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
            body = (body + ("\r\n" + "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/200" +
            "1/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"));
            body = (body + ("\r\n" + "<soap:Body>"));
            body = (body + ("\r\n" + " <RegisterExternalPurchase xmlns=\"http://centrsource.com/service/external-purchase\">"));
            body = (body + ("\r\n" + ("  <SecretKey>" + (HttpUtility.HtmlEncode(SecretKey.ToString()) + "</SecretKey>"))));
            body = (body + ("\r\n" + "  <extPurchaseData>"));
            body = (body + ("\r\n" + ("   <PurchaseId>" + (HttpUtility.HtmlEncode(purchaseid.ToString()) + "</PurchaseId>"))));
            body = (body + ("\r\n" + ("   <Description>" + (HttpUtility.HtmlEncode(description) + "</Description>"))));
            body = (body + ("\r\n" + ("   <TotalPrice>" + (string.Format(usc, "{0}", totalprice) + "</TotalPrice>"))));
            body = (body + ("\r\n" + ("   <CSReference>" + (HttpUtility.HtmlEncode(csepr) + "</CSReference>"))));
            body = (body + ("\r\n" + "   <CSReferenceType>OfferAction</CSReferenceType>"));
            body = (body + ("\r\n" + "  </extPurchaseData>"));
            body = (body + ("\r\n" + " </RegisterExternalPurchase>"));
            body = (body + ("\r\n" + "</soap:Body>"));
            body = (body + ("\r\n" + "</soap:Envelope>"));
            //SetLocale;
            //original;
            return body;
        }

        private string processResponse(XmlDocument responseXML, string elementName)
        {

        string  result = "";
        XmlNodeList objTags =  responseXML.GetElementsByTagName(elementName);

        if ((objTags == null)) {
            result = "napaka";
        }
        else {
            result = objTags.Item(0).Value ;
        }
        return result;
    }




    }


    public class Common {


        public const string SecretKey = "bc13a2218acf4ec391971e9b6257871b";
        public const string CampaignID = "a43946dd-7201-4ab2-9336-53d05c21606c";
    
    }

}

