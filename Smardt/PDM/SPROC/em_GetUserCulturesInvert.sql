IF EXISTS (SELECT * FROM sysobjects WHERE name='em_GetUserCulturesInvert' and type='P')
DROP Procedure em_GetUserCulturesInvert

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		zare
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].em_GetUserCulturesInvert
	-- Add the parameters for the stored procedure here

	@UserId         uniqueidentifier

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON
	
	
SELECT *

  FROM CULTURE c
 WHERE NOT EXISTS(SELECT 1 FROM vw_UserCulture u WHERE u.UserId = @UserId AND c.LCID = u.LCID )
ORDER BY [ORDER] ASC


END
GO
