﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_cmsUserEdit.ascx.cs" Inherits="SM.UI.Controls.user_membership__cmsUserEdit " %>
<%@ Register TagPrefix="SB" TagName="PermissionEdit" Src="~/CMS/controls/permission/_cmsPermissionEdit.ascx"   %>
<%@ Register TagPrefix="SB" TagName="PermissionList" Src="~/CMS/controls/permission/_cmsPermissionList.ascx"   %>

<div class = "qContEdit clearfix">
        
    <asp:Button ValidationGroup="usredit" ID="btSave" runat="server" Text="Save user" onclick="btSave_Click" CssClass="btnOrange " />

    <asp:Button ID="btCancel" runat="server" Text="Cancel" onclick="btCancel_Click" CssClass="btnRed"/>

    <asp:Button Visible="false" ID="btDeleteUser" runat="server" Text="Delete user" OnClientClick="javascript: return confirm('Ali res želiš zbrisati uporabnika????')" onclick="btDelete_Click" CssClass="btnRed"/>

    <br />

    
        <div style="line-height:150%;" class="floatR">
            <h2>Role</h2>
            <asp:CheckBoxList ID="cblRoles" runat="server">
            </asp:CheckBoxList>
            
            
            <asp:Panel ID="panPermissions" runat="server" Visible="false" >
                    <h2>Pravice</h2>
                    <SB:PermissionList id="objPermissionList" runat="server" />
                    <SB:PermissionEdit id="objPermissionEdit" runat="server" />
            </asp:Panel>
            
    
            
        </div>
        
    


    
    
    <asp:PlaceHolder ID="phOld" runat="server">
    <h2>Podatki</h2>
        
        UserName: <h1><asp:Literal ID="tbUserName" runat="server"></asp:Literal></h1>
        <br />
    </asp:PlaceHolder>

    <asp:PlaceHolder ID="phNew" runat="server">
    <h2>Dodaj novega</h2>
        
        UserName: <asp:TextBox ID="tbUsernameNew" runat="server"></asp:TextBox>
        <br />
        PASS: <asp:TextBox ID="tbPass" runat="server"></asp:TextBox>
        <br />
    </asp:PlaceHolder>


        Email: <asp:TextBox ID="tbEmail" Enabled="true"  runat="server"></asp:TextBox>
        <br />    
    
        
    
    
</div>

