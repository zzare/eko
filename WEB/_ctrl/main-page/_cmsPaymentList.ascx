﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_cmsPaymentList.ascx.cs" Inherits="SM.UI.Controls._cmsPaymentList" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Namespace="SM.UI.Controls" TagPrefix="SM" %>

<div class="qContList">

        
        <SM:smListView ID="lvList" runat="server" InsertItemPosition="None"   >
            <LayoutTemplate>
                <table  class="tblData Table_payment" cellpadding="0" cellspacing="0">
                        <thead>
                            <tr id="headerSort" runat="server">
                                <th >Datum plačila</th>
                                <th >Evidentirano</th>
                                <th >Znesek</th>
                                <th >Opomba</th>
                            </tr>
                        </thead>
                        <tbody id="itemPlaceholder" runat="server"></tbody>
                    </table>
            </LayoutTemplate>
            
            <ItemTemplate>
                <tr  class='<%#  Container.DataItemIndex % 2 == 0 ? "  " : " odd " %>'  onmouseover ="this.className='<%#  Container.DataItemIndex % 2 == 0 ? "hovRow" : "hovRowodd" %>'" onmouseout ="this.className = GetCssClass(<%#  Container.DataItemIndex %>)">
                    <td>
                        <%# SM.EM.Helpers.FormatDate(Eval("DatePayed"))%>
                    </td>
                    <td>
                        <%# SM.EM.Helpers.FormatDate(Eval("DateAdded"))%>
                    </td>
                    <td class="tdR">
                        <%# SM.EM.Helpers.FormatPrice(Eval("Price"))%>
                    </td>
                    <td>
                        <%# RenderType(Container.DataItem ) %>
                    </td>
                </tr>  
            
            </ItemTemplate>
            
            <EmptyDataTemplate>
                
                <div class="odd">
                    Ni še zabeleženih plačil
            
                </div>
                
            </EmptyDataTemplate>
        </SM:smListView>

</div>
