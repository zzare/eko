﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.Caching;
/*using SM.ST.BLL.ST;*/
using System.Collections.Generic;
using System.Xml.Linq;

/// <summary>
/// Summary description for CMSSiteMapProvider
/// </summary>
public class CMSEditSiteMapProvider : StaticSiteMapProvider 
{

    private readonly object siteMapLock = new object(); 
    private SiteMapNode root = null;
    private  int menuID = 3;
    public const string CacheDependencyKey = "CMSEditSiteMapProviderCacheDependency";

    protected void AddAllChildNodes(int sitemapID, SiteMapNode parent) {
        string smapKey;
        string smapUrl = string.Empty;
        string[] roleList;
        string smapTitle = "";

        // add all child nodes

        IEnumerable<em_GetSitemapNavByParentByLangEditCMSResult> childs = SITEMAP.GetSiteMapNavByParentByLangEditCMS(sitemapID, "si");
        foreach (em_GetSitemapNavByParentByLangEditCMSResult sm in childs)
        {

            smapUrl = string.Empty;
            roleList = new string[] { "admin" }; 

            // URL
            if (sm.CMS_URL != null) 
                if (sm.CMS_URL.ToLower().Contains("~/cms/ManageCMS".ToLower()))
                    smapUrl = SM.EM.Helpers.GetUrlCMSsmardt(sm.SMAP_ID, sm.SML_TITLE);
            //            smapUrl = sm.CMS_URL.Replace("{}", sm.SMAP_ID.ToString());
            
            // ROLES
            roleList = (sm.CMS_ROLES + ",admin").Split(new char[] { ',', ';' }, 512); // allways allow admin


            smapKey = sm.SMAP_ID.ToString();
            SiteMapNode smapNode = FindSiteMapNodeFromKey(smapKey);

            // add node
            if (smapNode == null)
            {
                try {
                    smapNode = new SiteMapNode(this, smapKey, smapUrl, sm.SML_TITLE, sm.SML_DESC );
                    smapNode.Roles = roleList;
                    AddNode(smapNode, parent); 

                } 
                catch (InvalidOperationException)
                {                    
                }            
            }
            // add sitemap subnodes
            AddAllChildNodes(sm.SMAP_ID, smapNode);
            
        }
    }
    public void OnSitemapChanged() {
        // Refresh the site map
        Clear();
        //_nodes.Clear();
        root = null;
    }

   public override SiteMapNode BuildSiteMap() {

        // Use a lock to make this method thread-safe 
        lock (siteMapLock) { 

            // First, see if we already have constructed the 
            // rootNode. If so, return it... 
            if (root != null) return root; 

            // We need to build the site map! 
            // Clear out the current site map structure 
            base.Clear(); 

            // Create the root SiteMapNode 
            root = new SiteMapNode(this, "root:-1", "~/cms/Default.aspx", "ROOT");
            root.Roles = ("?").Split(new char[] { ',', ';' }, 512);
            AddNode(root); 

            //  create all the menus with childs
            IEnumerable<MENU> menus = MENU.GetMenusBackend();
            foreach (MENU m in menus)
            {

                SITEMAP rootSmap = (SITEMAP)SITEMAP.GetRootByMenu(m.MENU_ID);
                if (rootSmap == null) continue;
                SiteMapNode rootMenu = new SiteMapNode(this, rootSmap.SMAP_ID.ToString(), "./?m=" + rootSmap.SMAP_ID.ToString(), m.MENU_TITLE);
                //                SiteMapNode rootMenu = new SiteMapNode(this, rootSmap.SMAP_ID.ToString(), "", m.MENU_TITLE);
                rootMenu.Roles = ("?").Split(new char[] { ',', ';' }, 512);
                AddNode(rootMenu, root);

                // Add all child nodes for ROOT MENU
                AddAllChildNodes(rootSmap.SMAP_ID, rootMenu);
            }
            
            // render childs only for main menu
            //MENU m = MENU.GetMenuByID(menuID); 

            //    SITEMAP rootSmap = (SITEMAP)SITEMAP.GetRootByMenu(m.MENU_ID);
            //    if (rootSmap == null) return root;
            //    SiteMapNode rootMenu = new SiteMapNode(this, rootSmap.SMAP_ID.ToString(), "./?m=" + rootSmap.SMAP_ID.ToString(), m.MENU_TITLE);
            //    //                SiteMapNode rootMenu = new SiteMapNode(this, rootSmap.SMAP_ID.ToString(), "", m.MENU_TITLE);
            //    rootMenu.Roles = ("?").Split(new char[] { ',', ';' }, 512);
            //    AddNode(rootMenu, root);

            //    // Add all child nodes for ROOT MENU
            //    AddAllChildNodes(rootSmap.SMAP_ID, rootMenu);
            
            

            // Finally, return the root node 
            return root; 
        } 
    }
 
    protected override SiteMapNode GetRootNodeCore() {
        return BuildSiteMap(); 
    }

    public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config)
    {
        // Verify parameters
        if (String.IsNullOrEmpty(name)) name = "CMSEditSiteMapProvider";

        // Add a default "description" attribute to config if the
        // attribute doesn't exist or is empty
        if (string.IsNullOrEmpty(config["description"])){
            config.Remove("description");
            config.Add("description", "CMS SQL site map provider");
        }

        // Call the base class's Initialize method
        base.Initialize(name, config);

    }

    public override bool IsAccessibleToUser(HttpContext context, SiteMapNode node)
    {

        if (!base.SecurityTrimmingEnabled) return true;

        if (node.Roles.Contains("?")) return true;

        if (context.User.Identity.IsAuthenticated && node.Roles.Contains("*")) return true;

        foreach (string role in node.Roles)
        {

            if (context.User.IsInRole(role)) return true;

        }

        return false;



    }
   
}
