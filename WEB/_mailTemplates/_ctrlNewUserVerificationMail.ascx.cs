﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _mailTemplates_ctrlNewUserVerificationMail : System.Web.UI.UserControl
{
    public string UserName { get; set; }
    public string Password { get; set; }
    public string VerifyUrl { get; set; }
    public string PageHref { get; set; }
    public string PageName { get; set; }

    
    protected void Page_Load(object sender, EventArgs e)
    {

    }
}
