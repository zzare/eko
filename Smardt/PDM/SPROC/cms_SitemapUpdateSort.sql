IF EXISTS (SELECT * FROM sysobjects WHERE name='cms_SitemapUpdateSort' and type='P')
DROP Procedure cms_SitemapUpdateSort

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		zare
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].cms_SitemapUpdateSort
	-- Add the parameters for the stored procedure here

	@SMAP_ID         int,
    @ORDER           int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON
	
	
UPDATE [dbo].[SITEMAP]
   SET [SMAP_ORDER] = @ORDER
 WHERE SMAP_ID = @SMAP_ID


END
GO
