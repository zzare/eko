﻿<%@ WebHandler Language="C#" Class="NarociloXml" %>

using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web.Routing;
using System.Drawing;
using System.Drawing.Imaging ;


public class NarociloXml : IHttpHandler
{

    public Graphics gr;


    
    public void ProcessRequest (HttpContext context) {
        context.Response.ContentType = "text/xml";
        
        
        // get order id
        Guid OrderID = Guid.Empty ;
        
        if (context.Request.QueryString ["o"] != null)
            if (SM.EM.Helpers.IsGUID(context.Request.QueryString ["o"]) )
                OrderID = new Guid(context.Request.QueryString ["o"]);

        if (OrderID == Guid.Empty)
            return;
        
        // get order
        OrderRep orep = new OrderRep();
        var Order = orep.GetOrderByID(OrderID);

        if (Order == null)
            return;





        XElement xml = new XElement(
            "data",
            new XAttribute("artnarst", SHOPPING_ORDER.Common.RenderShoppingOrderNumber(Order)),
            new XAttribute("artnardt", Order.DateStart )
        );

        XDocument doc = new XDocument(
        new XDeclaration("1.0", "utf-8", "yes"),
        new XComment("Terra-Gourmet"),
        xml
        );
        
        // list
        ProductRep prep = new ProductRep();
        var baseList = prep.GetCartItemList(Order.UserId, LANGUAGE.GetCurrentLang(), Order.PageId , Order.OrderID).ToList();



        foreach (var item in baseList) {
            var p = prep.GetProductVWByID(item.ProductID, LANGUAGE.GetDefaultLang(), Order.PageId.Value  );

            var smp = SITEMAP.GetSitemapLangByID(p.SMAP_ID, LANGUAGE.GetDefaultLang());
            XElement orderItem = new XElement("post",
                                new XElement("artnarst", Order.OrderRefID),
                                new XElement("artnardt", Order.DateStart.ToShortDateString() ),
                                new XElement("artctrl", p.CODE),
                                new XElement("artnazi", SM.EM.Helpers.StripHTML( p.NAME )),
                                new XElement("artgrupa",smp.SML_TITLE ),
                                new XElement("artcena", item.PriceTotal),
                                new XElement("artzaloga", item.Quantity)
                                );
            XElement orderWrap = new XElement("bestof",
                                orderItem );

            xml.Add(orderWrap);
         //   doc.Add(orderWrap);
        
        }
        
        
        
        
        

        //var list = from i in baseList
        //           from p in prep.db.PRODUCTs 
        //           where p.PRODUCT_ID == i.ProductID 
                   
        //           select new XElement("bestof",
        //                        new XElement("artctrl", p.CODE),
        //                        new XElement("artnazi", "/"),
        //                        new XElement("artgrupa", "kategorija"),
        //                        new XElement("artcena", i.PriceTotal),
        //                        new XElement("artzaloga", i.Quantity)
        //                        );

        //var list = from s in (from s in db.SEND_TO_USERs
        //                      where s.Notes != ""
        //           orderby s.DateSend descending
        //           select s).Take(20).ToList()
        //           select new XElement("item",
        //                        new XAttribute("from", (string.IsNullOrWhiteSpace(s.GetFirstNameFrom) ? "Janez" : s.GetFirstNameFrom)),
        //                        new XAttribute("desc", s.Notes),
        //                        new XAttribute("date", s.DateSend)
        //               //new XAttribute("date", DateTime.Now )
        //                        );





        string filename = "narocilo-" + SHOPPING_ORDER.Common.RenderShoppingOrderNumber(Order) + ".xml";
        context.Response.AddHeader("content-disposition", "attachment;filename=" + filename );

    
        doc.Save(context.Response.Output);
        
        
        // set cache
        TimeSpan refresh = new TimeSpan(0, 0, 10);
        context.Response.Cache.SetExpires(DateTime.Now.Add(refresh));
        context.Response.Cache.SetMaxAge(refresh);
        context.Response.Cache.SetCacheability(HttpCacheability.Server);
        //context.Response.CacheControl = HttpCacheability.Public.ToString();
        context.Response.Cache.SetValidUntilExpires(true);
        
    }


    
    public bool IsReusable {
        get {
            return false;
        }
    }

}