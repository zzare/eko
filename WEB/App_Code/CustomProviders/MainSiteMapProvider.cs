﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.Caching;
using SM.EM.BLL;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Collections;


/// <summary>
/// Summary description for MainSiteMapProvider
/// </summary>
public class MainSiteMapProvider : SmardtStaticSitemapProvider 
{

    
    private readonly object siteMapLock = new object(); 
    private SiteMapNode root = null;
    private  int menuID = 1;
    public const string CacheDependencyKey = "MainSiteMapProvider";
    public bool cacheData = true;

    protected void AddAllChildNodes(int sitemapID, SiteMapNode parent) {
        string smapKey;
        string smapUrl = string.Empty;
        string[] roleList;

        // add all child nodes
         
        IEnumerable<em_GetSitemapNavByParentByLangWWWResult> childs = SITEMAP.GetSiteMapNavByParentByLangWWW(sitemapID);
        foreach (em_GetSitemapNavByParentByLangWWWResult sm in childs)
        {

            smapUrl = string.Empty;
            roleList = new string[] { "admin" }; 

            // URL
            if (sm.SM_WWW_URL != null && !string.IsNullOrEmpty(sm.SM_WWW_URL))
                smapUrl = sm.SM_WWW_URL.Replace("{}", sm.SMAP_ID.ToString());
            else if (sm.WWW_URL != null)
                smapUrl = sm.WWW_URL.Replace("{}", sm.SMAP_ID.ToString());
            
            // ROLES
            roleList = (sm.WWW_ROLES + ",admin").Split(new char[] { ',', ';' }, 512); // allways allow admin


            smapKey = sm.SMAP_ID.ToString();
            SiteMapNode smapNode=null; //= FindSiteMapNodeFromKey(smapKey);

            // add node
            if (smapNode == null)
            {
                try {
                     string desc = sm.SML_DESC;
                    if (string.IsNullOrEmpty(desc))
                        desc = sm.SML_TITLE;
                    smapNode = new SiteMapNode(this, smapKey, smapUrl, sm.SML_TITLE, desc );
                    smapNode.Roles = roleList;

                    // add custom attributes
                    smapNode["class"] = sm.CUSTOM_CLASS ?? "";
                    AddNode(smapNode, parent); 

                } 
                catch (InvalidOperationException)
                {                    
                }            
            }
            // add sitemap subnodes
            AddAllChildNodes(sm.SMAP_ID, smapNode);
            
        }
    }
    public void OnSitemapChanged() {
        // Refresh the site map
        Clear();
        //_nodes.Clear();
        root = null;
    }

    protected string GetCacheKey() {
        //string key = "m-" + menuID.ToString() + "_" + "l-" + LANGUAGE.GetCurrentCulture();
        return SM.EM.Caching.GetMenuCacheKey(menuID );
    }
    protected SiteMapNode GetRootNodeCachedLocalized() {

        string key = GetCacheKey();
        SiteMapNode node=null;

        if (!cacheData)
            return null;

        if (BizObject.Cache[key + "root"] != null)
            node = (SiteMapNode)BizObject.Cache[key + "root"];

        if (node == null)
            return null;

     //    if (_childNodeCollectionTable == null)


        if (BizObject.Cache[key + "_childNodeCollectionTable"] != null)
            _childNodeCollectionTable = (Hashtable)((Hashtable)BizObject.Cache[key + "_childNodeCollectionTable"]).Clone();

         //if (_keyTable == null)
             if (BizObject.Cache[key + "_keyTable"] != null)
                 _keyTable = (Hashtable)((Hashtable)BizObject.Cache[key + "_keyTable"]).Clone();

         //if (_parentNodeTable == null)
             if (BizObject.Cache[key + "_parentNodeTable"] != null)
                 _parentNodeTable = (Hashtable)((Hashtable)BizObject.Cache[key + "_parentNodeTable"]).Clone();

         //if (_urlTable == null)
             if (BizObject.Cache[key + "_urlTable"] != null)
                 _urlTable = (Hashtable)((Hashtable)BizObject.Cache[key + "_urlTable"]).Clone();

         if (_childNodeCollectionTable == null || _keyTable == null || _parentNodeTable == null || _urlTable == null)
            return null;

         if (ChildNodeCollectionTable[node] == null)
             return node;

         SiteMapNodeCollection childs = new SiteMapNodeCollection(_childNodeCollectionTable.Count);
         foreach (SiteMapNode child in (SiteMapNodeCollection)this.ChildNodeCollectionTable[node])
         {
             if (SecurityTrimmingEnabled && !child.IsAccessibleToUser(HttpContext.Current))
                 continue;
             else
                 childs.Add(child);
             
         }

         node.ChildNodes = childs; //(SiteMapNodeCollection)this.ChildNodeCollectionTable[node];

        return node;

    }

   public override SiteMapNode BuildSiteMap() {

        // Use a lock to make this method thread-safe 
        lock (siteMapLock) { 

            // First, see if we already have constructed the 
            // rootNode. If so, return it...
            this.Clear(); 
            root = GetRootNodeCachedLocalized();
            if (root != null) return root; 

            // We need to build the site map! 
            // Clear out the current site map structure 
            base.Clear(); 

            
            // Datacontext
            //eMenikDataContext db = new eMenikDataContext();
            // Create the root SiteMapNode 
            SITEMAP rootSmap = (SITEMAP) SITEMAP.GetRootByMenu(menuID);
            root = new SiteMapNode( this, "root:"+ menuID , "", "ROOT");
            root.Roles = ( "?").Split(new char[] { ',', ';' }, 512);
            AddNode(root);




            // Add all child nodes for ROOT
            AddAllChildNodes(rootSmap.SMAP_ID, root);


            // cache all sitemap data
            string key = GetCacheKey();
            BizObject.CacheData(key + "root", root);

            
            Hashtable cacheChild = new Hashtable(ChildNodeCollectionTable );
            Hashtable cacheKey = new Hashtable(KeyTable );
            Hashtable cacheParent = new Hashtable(ParentNodeTable );
            Hashtable cacheUrl = new Hashtable(UrlTable );

            
            BizObject.CacheData(key + "_childNodeCollectionTable", cacheChild);
            BizObject.CacheData(key + "_keyTable", cacheKey);
            BizObject.CacheData(key + "_parentNodeTable", cacheParent);
            BizObject.CacheData(key + "_urlTable", cacheUrl);

          
            // Finally, return the root node 
            return root; 
        } 
    }
 
    protected override SiteMapNode GetRootNodeCore() {
        return BuildSiteMap(); 
    }

    public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config)
    {
        // Verify parameters
        if (config == null) throw new ArgumentNullException("config");
        if (String.IsNullOrEmpty(name)) name = "MainSiteMapProvider";

        // Add a default "description" attribute to config if the
        // attribute doesn't exist or is empty
        if (string.IsNullOrEmpty(config["description"]))
        {
            config.Remove("description");
            config.Add("description", "CMS SQL site map provider");
        }

        // Call the base class's Initialize method
        base.Initialize(name, config);

        menuID = Int32.Parse( config["menuID"]);

    }


    public override SiteMapNode CurrentNode
    {
        get
        {
            if (HttpContext.Current != null)
            {
                SiteMapNode ret = ResolveSiteMapNode(HttpContext.Current);
                if (ret != null) return ret;

                string key = "";
                if (HttpContext.Current.Items["smp"] != null) key = HttpContext.Current.Items["smp"].ToString();

                if (string.IsNullOrEmpty(key))
                    if (HttpContext.Current.Request.QueryString["smp"] != null)
                        key = HttpContext.Current.Request.QueryString["smp"];

                ret = this.FindSiteMapNodeFromKey(key);

                if (ret == null)
                    ret = base.FindSiteMapNode(HttpContext.Current);

                return ret;
            }
            else
                return null;
        }
    }


    
    public override bool IsAccessibleToUser(HttpContext context, SiteMapNode node)
    {

        if (!base.SecurityTrimmingEnabled) return true;

        if (node.Roles.Contains("?")) return true;

        if (context.User.Identity.IsAuthenticated && node.Roles.Contains("*")) return true;

        foreach (string role in node.Roles ){

            if (context.User.IsInRole(role))   return true;
               
        }

        return false;


        
    }
   
}
