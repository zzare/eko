var em_g_cult = '';
var pollID = null;

function em_init() {
    $("embed").attr("wmode", "transparent").each(function() { var cl = $(this).clone(); $(this).replaceWith(cl) });
    $("object").append('<param name="wmode" value="transparent">').each(function() { var cl = $(this).clone(); $(this).replaceWith(cl) });

}
function formatObjectEmbed(obj) {
    $(obj).find("embed").attr("wmode", "transparent");
    $(obj).find("object").append('<param name="wmode" value="transparent">').each(function() { var cl = $(this).clone(); $(this).replaceWith(cl) });
}


function RequestLogin() {
    if (Sys.Services.AuthenticationService.get_isLoggedIn())
        return;
    this.window.location = "/?returnurl=" + this.window.location;
}

function reloadPage() {
    window.location.replace(window.location.href);
}

/* map  **/
var editID = null;
var mapEditSrc = null;
function loadMapView(id, s) {
    var c = $('#' + id);
    c.find("iframe").eq(0).each(function() {
        var w = $(this).parent().width();

        this.src = s + '&w=' + w;
        $(this).width(w);
    });
}

function loadMapEdit(id) {

    var c = $('#' + id);
    $('#mapE' + id).eq(0).each(function() {

        var w = c.width();
        this.src = getMapEditSrc(id) + '&w=' + w;
    });

}
function getMapEditSrc(id) {
    return mapEditSrc + '&id=' + id;
}



function MapEditOpen(id) {
//    RequestLogin();

    editID = id;
    var pop = $find("mpeMapEdit" + id);
    pop.show();
    loadMapEdit(id);
}


function MapEditClose() {
    $find('mpeMapEdit' + editID).hide();
    $('#mapE' + editID).attr('src', '');

}
/* MPL */
function mpl_init() {
    $("#divModalMsg").dialog({
        autoOpen: false,
        bgiframe: true,
        height: 'auto',
        zIndex: 150002,
        closeOnEscape: true,
        draggable: true,
        resizable: true,
        modal: true
    });

    $("#divModalLoading").dialog({
        autoOpen: false,
        bgiframe: true,
        height: 'auto',
        zIndex: 150002,
        closeOnEscape: false,
        draggable: true,
        resizable: false,
        modal: true
    });

}

/* MODAL loader */
function openModalLoading(title) {
    $("#divModalLoading").dialog('open').show().dialog('option', 'title', title).parent().find('a.ui-dialog-titlebar-close').hide();
}

function setModalLoader(data) {
    closeModalLoading();
    mpl++;
    var _mpl = $("#divModalLoader").clone().attr('id', 'divModalLoader' + mpl).attr('class', 'divModalLoader').appendTo("body");
    _mpl.html(data).show('fast');

    //    $("#divModalLoader").html(data).show('fast');

}
function setModalLoaderLoading() {
    $("#divModalLoading").dialog('open').show();

}

/* ERROR */
function logError(err) {
    var log = "Stack Trace: " + err.get_stackTrace() + "/r/n" +
          "Error: " + err.get_message() + "/r/n" +
          "Status Code: " + err.get_statusCode() + "/r/n" +
          "Exception Type: " + err.get_exceptionType() + "/r/n" +
          "Timed Out: " + err.get_timedOut();

    WebServiceP.LogError(log);
}
function onError(err) {
    logError(err);
    setModalLoaderErr("Pri�lo je do napake. Prosim poskusite ponovno.", true);
}
function setModalMsg(title, text, single) {
    var data = '<h2>' + text + '</h2>';
    var r = $("#divModalMsg").dialog('open').show().html(data).dialog('option', 'title', title);
    closeModalLoading();
    if (single) {
        closeModalLoader();
    }
    return r;
}
function setModalLoaderErr(text, single) {
    setModalMsg('Napaka', text, single);
}

function closeModalLoading() {
    $("#divModalLoading").dialog('close');
}
function closeModalLoader() {
    $("#divModalLoader" + mpl).dialog('close').remove();
    mpl--;
    
    $("#divModalLoader").dialog('close');
}
function closeModalLoaderAll() {
    $(".divModalLoader").not('#divModalLoader').dialog('close').remove();
    mpl = 0;
}


function setModalLoaderErrValidation(title, text) {
    var data = '';
    if (title.length > 0)
        data += '<h2>' + title + '</h2>';
    data += text;
    setModalLoaderErr(data, false);
}

function closeModalMsg() {
    $("#divModalMsg").dialog('close');
}

function onPopupSuccessLoad(ret) {
    if (ret.Code == 1) {
        setModalLoader(ret.Data);
        closeModalLoading();
    }
    else if (ret.Code == 0) {
        setModalLoaderErr(ret.Message, true);
        return;
    }
    $("#divModalLoader" + mpl).init_mpl().dialog('open').dialog('option', 'width', 810).dialog('option', 'position', 'center').parent().find("div.ui-dialog-titlebar").hide();
}

function openLookup(tit, hf, id, params) {

    var _items = new Array();
    Array.add(_items, tit);
    Array.add(_items, hf);
    if (params)
        Array.add(_items, params);
    openModalLoading('Odpiram...');
    WebServiceCWP.RenderViewAut(_items, id, onLookupSuccessLoad, onSaveError);
}
function onSaveError(err) {
    logError(err);
    setModalLoaderErr("Pri�lo je do napake. Prosim poskusite ponovno.", false);
}

function onLookupSuccessLoad(ret) {
    if (ret.Code == 1) {
        setModalLoader(ret.Data);
        closeModalLoading();
    }
    else if (ret.Code == 0) {
        setModalLoaderErr(ret.Message, true);
        return;
    }
    $("#divModalLoader" + mpl).init_mpl().dialog('open').dialog('option', 'width', 600).dialog('option', 'position', 'center').parent().find("div.ui-dialog-titlebar").hide();
}


/* POLL */
function votePoll(id, typ) {
    var v = $('#cPoll' + id + ' input:checked');
    pollID = id;

    if (v.val() == null) {
        $('#cPoll' + id).find("span.err").css('color', 'red').show();
        return;
    }

    $('#cPoll' + id).html($('#divModalLoading').clone().attr('id', '').attr('class', ''));
    WebServiceP.VotePoll(v.val(), em_g_cult, typ, onPollVoteSuccess, onError);
}

function onPollVoteSuccess(ret) {
    if (ret.Code != 1) {
        setModalLoaderErr(ret.Message, true);
        return;
    }

    var _c = $("#cPoll" + pollID.toLowerCase()).html(ret.Data).hide();
    animateChart(pollID.toLowerCase(), 'val', _c.find('div.cPollRes a')[0]);
}
function animateChart(id, typ, a) {
    var _c = $("#cPoll" + id).hide();
    _c.find('div.cPollRes a').removeClass('sel');
    var _p = _c.find(".pollChart").css({ width: "0%" });
    _c.show();
    _p.each(function() {
        var _perc = $(this).attr(typ) + '%';
        $(this).stop().animate({ width: _perc }, "slow", "swing");
    });
    $(a).addClass('sel');
}

function getPollResultsIfVoted() {
    $('div.cPollItem').find('input.hdPollID').each(function() {
        var id = $(this).val();
        WebServiceP.GetPollResultsIfVoted(id, em_g_cult, em_g_lang, onPollGetResultsIfVotedSuccess, onPollGetResultsIfVotedError, id);
    });
}

function onPollGetResultsIfVotedSuccess(ret, id) {





    if (ret.Code == 0) {
        //ok, not voted
        return;
    }
    if (ret.Code != 1) {
        // error
        logError('onPollGetResultsIfVotedSuccess ' + ret.Message, true);
        return;
    } // get results
    var _c = $("#cPoll" + id.toLowerCase()).html(ret.Data).hide();
    animateChart(id.toLowerCase(), 'val', _c.find('div.cPollRes a')[0]);
}
function onPollGetResultsIfVotedError() {
}


/* gallery */
function openGalleryAnimal(cwp, i, _t) {
    $.fancybox.showActivity();
    var _items = new Array();
    Array.add(_items, em_g_cult);
    Array.add(_items, cwp);
    Array.add(_items, em_g_lang);
    Array.add(_items, i);
    WebServiceP.RenderView(_items, 2, onGalleryPopupSuccessLoad, onFancyError, _t);
}


function onGalleryPopupSuccessLoad(ret, _t) {
    if (ret.Code == 0) {
        $.fancybox.hideActivity();
        setModalLoaderErr(ret.Message, true);
        return;
    }



    $.fancybox(ret.Data, {
        'overlayOpacity': 0.9,
        'overlayColor': '#999',
        width: 0,
        //'autoScale' : true,
        //'autoDimensions' : true,
        'scrolling': 'no',
        //'transitionIn': 'elastic',
        //'transitionOut': 'elastic',
        //'easingIn': 'easeOutBack',
        //'easingOut': 'easeInBack',
        'onStart': function () {
            //alert('test');
            //$('#fancybox-wrap').hide();
            $.fancybox.showActivity();
            $('#fancybox-outer').css('width', '0px');
            //$.fancybox.resize();
        },
        'onComplete': function () {
            //            $('#fancybox-wrap').hide();
            $.fancybox.showActivity();
            $('#fancybox-wrap').css('width', '0px');
            $('#fancybox-outer').css('width', '0px');
            galLoadCarousel();
            galLoadMainImage();
        }
    });

    scroll(0, 0);

}

function onFancyError(err) {
    $.fancybox.hideActivity();
    logError(err);
    setModalLoaderErr("Pri�lo je do napake. Prosim poskusite ponovno.", true);
}

function onAnimalEditSuccessLoad(ret) {
    if (ret.Code == 1) {
        setModalLoader(ret.Data);
        closeModalLoading();
    }
    else if (ret.Code == 0) {
        setModalLoaderErr(ret.Message, true);
        return;
    }
    $("#divModalLoader" + mpl).init_mpl().dialog('open').dialog('option', 'width', 810).dialog('option', 'position', 'center').parent().find("div.ui-dialog-titlebar").hide();

    $(".cAnimalEdit textarea").htmlarea({
        toolbar: ["html", "|", "bold", "italic", "underline", "|", "orderedlist", "unorderedlist", "|", "indent", "outdent", "|", "h3", "h4", "|", "link", "unlink"]
    });
} 