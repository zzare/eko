﻿using System;
using System.Collections;
using System.Collections.Generic ;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SM.EM.UI.CMS;

namespace SM.EM.UI.Controls
{
    public partial class _cmsGalleryEdit : BaseControl
    {
        public event EventHandler OnGalleryEdited;
        public event EventHandler OnGalleryEditCanceled;
        public string UserName = "";

        public int _smapID = -1;
        public string _langID = "-1";
        public const int MAX_IMAGE_WIDTH = 600;

        public int GalleryID { get { if (ViewState["GalleryID"] == null) return -1; return int.Parse(ViewState["GalleryID"].ToString()); } set { ViewState["GalleryID"] = value; } }
        //protected int TypeID { get { return int.Parse(ddlTypes.SelectedValue); } }

        protected void Page_Load(object sender, EventArgs e)
        {
            // init image
            imgMain.Attributes["onload"] = "SetMaxImageWidth(this, " + MAX_IMAGE_WIDTH + ")";
            objImageSelect.OnSelect += new SM.UI.Sandbox.Controls._cmsImageSelect.OnSelectEventHandler(objImageSelect_OnSelect);

            objImageSelect.MultipleSelect = false;
            objImageSelect.GalleryID = GalleryID;
            if (!string.IsNullOrEmpty(hfImgID.Value))
                objImageSelect.ExcludeImageID = int.Parse(hfImgID.Value);


        }

        void objImageSelect_OnSelect(object sender, SM.BLL.Common.ClickIDEventArgs e)
        {
            // single mode
            if (e.ID > 0)
            {
                hfImgID.Value = e.ID.ToString();
                IMAGE img = IMAGE.GetImageByID(e.ID, SM.BLL.Common.ImageType.CMSthumb);
                imgMain.Src = img.IMG_URL;
            }

        }

        public void BindData() {


            LoadGallery();

            LoadGalleryDesc();

            LoadImageTypes();
        }


        protected void LoadGallery() { 
            // load image data for selected image type
            GALLERY  gal = GALLERY.GetGalleryByID (GalleryID );

            if (gal.MAIN_IMG_ID != null){
                IMAGE img = IMAGE.GetImageByID(gal.MAIN_IMG_ID.Value, SM.BLL.Common.ImageType.CMSthumb);
                if (img != null)
                {
                    imgMain.Src = img.IMG_URL;
                    hfImgID.Value = img.IMG_ID.ToString();
                }
            }
        }

        protected void LoadGalleryDesc() {
            List<vw_GalleryDesc> list = GALLERY.GetGalleryDescsByIDEmenik(GalleryID, UserName);
            lvList.DataSource = list;
            lvList.DataBind();        
        }

        protected void LoadImageTypes() {
            List<IMAGE_TYPE> list =  GALLERY.GetImageTypesFromGallery(GalleryID);
            lvImageType.DataSource = list;
            lvImageType.DataBind();

            // bind ddl types
            ddlType.Items.Clear();
            ddlType.DataSource = GALLERY.GetImageTypesFromGalleryInvert(GalleryID);

            ddlType.Items.Add(new ListItem("- Izberi tip -", ""));
            ddlType.DataTextField = "TYPE_TITLE";
            ddlType.DataValueField= "TYPE_ID";
            ddlType.DataBind();
        
        }

        public void Cancel()
        {
            OnGalleryEditCanceled (this, new EventArgs());
        }
        public void Save() {

            int i = 0;
            // save image title changes
            foreach (ListViewItem item in lvList.Items)
            {
                HiddenField hfLang = item.FindControl("hfLang") as HiddenField ;

                TextBox tbTitle = item.FindControl("tbTitle") as TextBox;
                TextBox tbDesc = item.FindControl("tbDesc") as TextBox;
                
                // save desc
                GALLERY.SaveGalleryDesc(GalleryID , hfLang.Value, tbTitle.Text, tbDesc.Text);

                i++;
            }

            // save gallery data
            eMenikDataContext db = new eMenikDataContext();

            GALLERY  g = (from gal in db.GALLERies   where gal.GAL_ID == GalleryID  select gal).SingleOrDefault();
            if (!string.IsNullOrEmpty(hfImgID.Value))
                g.MAIN_IMG_ID  = int.Parse(hfImgID.Value) ;
            db.SubmitChanges();

            OnGalleryEdited (this, new EventArgs());
        
        }

        protected void btSelectImage_OnClick(object sender, EventArgs e)
        {
            objImageSelect.GalleryID = GalleryID;
            objImageSelect.ImageTypeID = SM.BLL.Common.ImageType.CMSthumb;
            objImageSelect.OpenPopup();

        }

        protected string RenderDimensions(object id) {
            string width = "" ;
            string height = "";

            IMAGE_TYPE.RenderImageTypeDimensions(int.Parse(id.ToString()),out  width,out  height, "px");


            string ret =  "širina: " + width ;
            ret += " | višina: " + height;

            return ret;
        }

        protected void btAddType_Click(object sender, EventArgs e)
        {
            int type = -1;
            
            if (!int.TryParse(ddlType.SelectedValue, out type)) return;

            GALLERY.AddImageTypeToGallery(type, GalleryID);

            LoadImageTypes();

        }
    }
}