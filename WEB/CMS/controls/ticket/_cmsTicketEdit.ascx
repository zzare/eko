﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_cmsTicketEdit.ascx.cs" Inherits="SM.UI.Controls._cmsTicketEdit" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="SM" TagName="FileList" Src="~/_ctrl/module/file/_cntFileList.ascx"  %>

<div class = "qContEdit clearfix">
 



    
    <asp:Panel ID="panEdit" runat="server" CssClass="floatR">
        <div>
                   
            <asp:Button ValidationGroup="ordredit" ID="btSave" runat="server" Text="Save" onclick="btSave_Click" CssClass="btnOrange " />

            <asp:Button ID="btCancel" runat="server" Text="Cancel" onclick="btCancel_Click" CssClass="btnRed"/>
            <br />
            <h2>Status: <asp:DropDownList ID="ddlStatus" runat="server" AppendDataBoundItems="true"></asp:DropDownList>
            </h2>
    
            Št. ticketa: <asp:TextBox ID="tbOrderRefID" ReadOnly="true" runat="server"></asp:TextBox>
            <br />
            Uporabnik: <a runat="server" id="lnkUser" target="_blank" title="edit user" >user</a>
            <br />
        
        </div>
    </asp:Panel>
    
    <br />
    
    <strong><asp:Literal ID="litTitle" runat="server"></asp:Literal></strong>
    <br />
    <asp:Literal ID="litDesc" runat="server"></asp:Literal>
    <br />
    <br />
    <h3>Datoteke</h3>
    <div style="width:500px">
        <SM:FileList id="objFileList" runat="server" />
    
    </div>    
    


    

    
    

    
</div>

