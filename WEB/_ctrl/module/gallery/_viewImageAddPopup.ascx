﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewImageAddPopup.ascx.cs" Inherits="SM.EM.UI.Controls._viewImageAddPopup" %>
<%@ Register Namespace="SM.UI.Controls" TagPrefix="SM" %>
<%@ Register TagPrefix="SM" TagName="ImageAddNew" Src="~/_ctrl/module/gallery/_viewImageAddNew.ascx"   %>

<script type="text/javascript" language="javascript">
    var refrImgList = false;
    
    $(function() {
        $("#tabsImageAdd").tabs();
    });



    function closeImageAdd(_cwp) {
        closeModalLoader();

        if (refrImgList) {
            if (typeof imgAddPopClbck == "function") {
                imgAddPopClbck();
                imgAddPopClbck = null;
            }
            reloadCWPImgList(_cwp);
        }
    }
    function reloadCWPImgList(_cwp) {
        var _items = new Array();
        Array.add(_items, _cwp );
        Array.add(_items, em_g_lang);
        WebServiceCWP.RenderViewAut(_items, 11, onImageEditSuccessSave, onError, _cwp );
    }

</script>  



<div>
<div class="modPopup_header modPopup_headerSettings">
        <h4>DODAJ SLIKO</h4>
        <p>V galerijo lahko dodaš posamezno sliko, ki jo preneseš iz svojega računalnika ali pa izbereš sliko iz že obstoječe galerije</p>
    </div>
    <div class="modPopup_main">

       
       
       
      <div id="tabsImageAdd">
        <ul>
	        <li><a href="#tabs-1">Dodaj NOVO sliko</a></li>
	        <%--<li><a href="#tabs-2">Izberi slike iz galerij</a></li>--%>
        </ul>
        <div id="tabs-1">
	        <SM:ImageAddNew ID="objAddNew" runat="server" />
        </div>
<%--        <div id="tabs-2">
	        <p>Morbi tincidunt, dui sit amet facilisis feugiat, odio metus gravida ante, ut pharetra massa metus id nunc. Duis scelerisque molestie turpis. Sed fringilla, massa eget luctus malesuada, metus eros molestie lectus, ut tempus eros massa ut dolor. Aenean aliquet fringilla sem. Suspendisse sed ligula in ligula suscipit aliquam. Praesent in eros vestibulum mi adipiscing adipiscing. Morbi facilisis. Curabitur ornare consequat nunc. Aenean vel metus. Ut posuere viverra nulla. Aliquam erat volutpat. Pellentesque convallis. Maecenas feugiat, tellus pellentesque pretium posuere, felis lorem euismod felis, eu ornare leo nisi vel felis. Mauris consectetur tortor et purus.</p>
        </div>--%>
    </div>
       

    <div class="clearing"></div>
</div>
<div class="modPopup_footer"> 
    <div class="buttonwrapperModal">
        <a class="lbutton lbuttonEdit" href="#" onclick="closeImageAdd('<%= CwpID %>'); return false"><span>Zapri</span></a>
    </div>
</div>
</div>