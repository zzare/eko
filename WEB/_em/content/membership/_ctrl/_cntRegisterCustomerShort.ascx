﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_cntRegisterCustomerShort.ascx.cs" Inherits="_ctrl_content_membership_ctrl_cntRegisterCustomerShort" %>
<%@ Register Namespace="SM.UI.Controls" TagPrefix="SM"  %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="EM" TagName="MailConfirmation" Src="~/_mailTemplates/_ctrlNewUserVerificationMail.ascx"  %>


<script type="text/javascript">
    $(document).ready(function () {

        $('.tbRegPass').watermark('watermark');

    });

</script>

<asp:Panel ID="panLogin" runat="server" DefaultButton="btRegister">
        <table class="TBloginbox" cellpadding="0">
            <tr>
                <td>
                    <asp:TextBox class="TBXlogin TBXlogin_new" runat="server" ID="tbName" />
                    <%--<br />--%>
                    <cc1:TextBoxWatermarkExtender ID="weName" runat="server" WatermarkCssClass="WMmainLogin_new"  WatermarkText="<%$Resources:Modules, RegisterName %>" TargetControlID ="tbName"></cc1:TextBoxWatermarkExtender>
                    <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="tbName" SetFocusOnError="true" Display="Dynamic"
                        ErrorMessage="<%$Resources:Modules, RegisterName %>" ToolTip="<%$Resources:Modules, RegisterName %>" ValidationGroup="regShort"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox class="TBXlogin TBXlogin_new" runat="server" ID="tbLastName" />
                    <%--<br />--%>
                    <cc1:TextBoxWatermarkExtender ID="weLastname" runat="server" WatermarkCssClass="WMmainLogin_new"  WatermarkText="<%$Resources:Modules, RegisterLastname %>" TargetControlID ="tbLastName"></cc1:TextBoxWatermarkExtender>
                    <asp:RequiredFieldValidator ID="rfvLastname" runat="server" ControlToValidate="tbLastName" SetFocusOnError="true" Display="Dynamic"
                        ErrorMessage="<%$Resources:Modules, RegisterLastname %>" ToolTip="<%$Resources:Modules, RegisterLastname %>" ValidationGroup="regShort"></asp:RequiredFieldValidator>
                
                </td>
            </tr>

            <tr>
                <td>
                    <asp:TextBox class="TBXlogin TBXlogin_new" runat="server" ID="tbPhone" />
                    <%--<br />--%>
                    <cc1:TextBoxWatermarkExtender ID="wePhone" runat="server" WatermarkCssClass="WMmainLogin_new"  WatermarkText="<%$Resources:Modules, RegisterPhone %>" TargetControlID ="tbPhone"></cc1:TextBoxWatermarkExtender>
                    <asp:RequiredFieldValidator ID="rfvPhone" runat="server" ControlToValidate="tbPhone" SetFocusOnError="true" Display="Dynamic"
                        ErrorMessage="<%$Resources:Modules, RegisterPhone %>" ToolTip="<%$Resources:Modules, RegisterPhone %>" ValidationGroup="regShort"></asp:RequiredFieldValidator>
                
                </td>
            </tr>


            <tr>
                <%--<td class="TD_right"><asp:Label runat="server" ID="lblEmail" AssociatedControlID="Email" Text="E-pošta:" /></td>--%>
                <td>
                    <asp:TextBox class="TBXlogin TBXlogin_new" runat="server" ID="Email" />
                    <%--<br />--%>
                    <cc1:TextBoxWatermarkExtender ID="weEmail" runat="server" WatermarkCssClass="WMmainLogin_new"  WatermarkText="<%$Resources:Modules, RegisterEmail %>" TargetControlID ="Email"></cc1:TextBoxWatermarkExtender>
                    <asp:RequiredFieldValidator ID="valRequireEmail" runat="server" ControlToValidate="Email" SetFocusOnError="true" Display="Dynamic"
                        ErrorMessage="<%$Resources:Modules, RegisterEmail %>" ToolTip="<%$Resources:Modules, RegisterEmail %>" ValidationGroup="regShort"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator runat="server" ID="valEmailPattern"  Display="Dynamic" SetFocusOnError="true" ValidationGroup="regShort"
                        ControlToValidate="Email" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ErrorMessage="<%$Resources:Modules, RegisterEmailErrorInvalid %>"></asp:RegularExpressionValidator>
                    <asp:CustomValidator ValidationGroup="regShort" ID="cvEmail" runat="server" Display="Dynamic" ErrorMessage="<%$Resources:Modules, RegisterEmailErrorInvalid %>" OnServerValidate="ValidateEmail"></asp:CustomValidator>
                    <asp:CustomValidator ValidationGroup="regShort" ID="cvUsername" runat="server" Display="Dynamic" ErrorMessage="<%$Resources:Modules, RegisterEmailErrorTaken %>" OnServerValidate="ValidateDuplicate"></asp:CustomValidator>
                                        
                    &nbsp;
                </td>
            </tr>
            <%--<tr class="tr_validation">
                
                <td>
                    <asp:RequiredFieldValidator ID="valRequireEmail" runat="server" ControlToValidate="Email" SetFocusOnError="true" Display="Dynamic"
                        ErrorMessage="* Vpiši e-poštni naslov." ToolTip="* Vpiši e-poštni naslov." ValidationGroup="regShort"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator runat="server" ID="valEmailPattern"  Display="Dynamic" SetFocusOnError="true" ValidationGroup="regShort"
                        ControlToValidate="Email" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ErrorMessage="* E-poštni naslov ni pravilne oblike."></asp:RegularExpressionValidator>
                    <asp:CustomValidator ValidationGroup="regShort" ID="cvEmail" runat="server" Display="Dynamic" ErrorMessage="* E-poštni naslov ni veljaven." OnServerValidate="ValidateEmail"></asp:CustomValidator>
                    <asp:CustomValidator ValidationGroup="regShort" ID="cvUsername" runat="server" Display="Dynamic" ErrorMessage="* E-poštni naslov je že zaseden." OnServerValidate="ValidateDuplicate"></asp:CustomValidator>
                                        
                    &nbsp;
                </td>
            </tr> --%>
            <tr>
                <%--<td class="TD_right"><asp:Label runat="server" ID="lblPassword" AssociatedControlID="Password" Text="Geslo:" /></td>--%>
                <td>
                    <asp:TextBox class="TBXlogin  TBXlogin_new tbRegPass" runat="server" ID="Password" TextMode="Password"  title="<%$Resources:Modules, RegisterPassword %>" />
                    <%--<br />--%>
                    <cc1:TextBoxWatermarkExtender ID="wePass" runat="server" WatermarkCssClass="WMmainLogin_new"  WatermarkText="<%$Resources:Modules, RegisterPassword %>" TargetControlID ="Password"></cc1:TextBoxWatermarkExtender>
   
    <asp:RequiredFieldValidator ID="valRequirePassword" runat="server" ControlToValidate="Password" SetFocusOnError="true" Display="Dynamic" EnableClientScript="false"
                        ErrorMessage="<%$Resources:Modules, RegisterPassword %>" ToolTip="<%$Resources:Modules, RegisterPassword %>" ValidationGroup="regShort"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="valPasswordLength" runat="server" ControlToValidate="Password" SetFocusOnError="true" Display="Dynamic"
                    ValidationExpression=".{5,}" ErrorMessage="<%$Resources:Modules, RegisterPasswordErrorLength %>" ToolTip="<%$Resources:Modules, RegisterPasswordErrorLength %>"
                    ValidationGroup="regShort"></asp:RegularExpressionValidator>&nbsp;

                </td>
            </tr>
            <tr >
                
                <td>
                </td>
            </tr>

           <tr>
                <td>
                   <SM:LinkButtonDefault ID="btRegister" CssClass="Astepnext" runat="server"   ValidationGroup="regShort"    onclick="btRegister_Click"><span><%= GetGlobalResourceObject("Modules", "RegisterButton")%></span></SM:LinkButtonDefault>
    
                </td>
            </tr>
        </table>
<%--    <div style="padding:10px 0px 0px 0px;">
        <SM:LinkButtonDefault ID="btRegister" CssClass="Astepnext" runat="server"   ValidationGroup="regShort"    onclick="btRegister_Click"><span>REGISTRIRAJ SE</span></SM:LinkButtonDefault>
    
    </div>--%>
    <div class="clearing"></div>
</asp:Panel>



<EM:MailConfirmation ID="objMailConfirmation" runat="server" Visible="false" />