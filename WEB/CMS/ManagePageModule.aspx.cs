﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace SM.EM.UI.CMS
{

    public partial class ManagePageModule : BasePage_CMS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack){
            }
        }

        protected void dvModule_ItemInserted(object sender, DetailsViewInsertedEventArgs e)
        {

            if (HandleErrors(e.Exception))
            {
                e.ExceptionHandled = true;
                e.KeepInInsertMode = true;
            }
            else {
                // update cbl
               // var pmodID = (int)e.Values["PMOD_ID"];

               // this.InsertModulsForPageModule(pmodID);

                //refresh
                gvModule.DataBind();

            }
        }

        protected void gvModule_RowUpdated(object sender, GridViewUpdatedEventArgs e)
        {

        }
        protected void gvModule_SelectedIndexChanged(object sender, EventArgs e)
        {
            dvModule.ChangeMode(DetailsViewMode.Edit);
        }
        protected void dvModule_ItemUpdated(object sender, DetailsViewUpdatedEventArgs e)
        {

            if (HandleErrors(e.Exception))
            {
                e.ExceptionHandled = true;
                e.KeepInEditMode = true;
            }
            else
            {
                // update cbl
                var pmodID = (int)e.Keys[0];

                PAGE_MODULE.DeleteModulsForPageModule(pmodID);
                this.InsertModulsForPageModule(pmodID);

                // refresh
                dvModule.ChangeMode(DetailsViewMode.Insert);
                gvModule.DataBind();

                // clear cache
                SM.EM.BLL.BizObject.PurgeCacheItems(SM.EM.Caching.Key.PageModule());
                SM.EM.BLL.BizObject.PurgeCacheItems(SM.EM.Caching.Key.PageModuleID ());

                SM.EM.Caching.RemoveMenuCache();
                CMSEditSiteMapProvider smpCms = (CMSEditSiteMapProvider)SiteMap.Providers["CMSEdit"];
                smpCms.OnSitemapChanged();
                RefreshCMSSitemapProvider();

            }
        }

        // page moduls
        public void InsertModulsForPageModule(int pmodID)
        {
            CheckBoxList cblPmod = dvModule.FindControl("cblPmod") as CheckBoxList;

            eMenikDataContext db = new eMenikDataContext();

            if (cblPmod == null) { return; }

            foreach (ListItem li in cblPmod.Items)
            {
                if (li.Selected){
                    db.PAGEM_MODs.InsertOnSubmit(new PAGEM_MOD { PMOD_ID = pmodID, MOD_ID = int.Parse( li.Value) });
                }
            }

            db.SubmitChanges();
        }



        protected void dvModule_DataBound(object sender, EventArgs e)
        {
            if (dvModule.CurrentMode == DetailsViewMode.Edit)
            {
                CheckBoxList cblPmod = dvModule.FindControl("cblPmod") as CheckBoxList;

                if (cblPmod == null) { return; }

                foreach (PAGEM_MOD  pmod in PAGE_MODULE.GetModulsForPageModuls(Convert.ToInt32( gvModule.SelectedDataKey.Value )))
                {
                    cblPmod.Items.FindByValue(pmod.MOD_ID.ToString()).Selected = true;
                }
            }

        }
        protected void gvModule_RowDeleted(object sender, GridViewDeletedEventArgs e)
        {
            if (HandleErrors(e.Exception))
            {
                e.ExceptionHandled = true;
            }
            else
            {
                
            }
        }
        protected void gvModule_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            
            
                PAGE_MODULE.DeleteModulsForPageModule(Convert.ToInt32(e.Keys["PMOD_ID"]));



        }
        protected void ldsModule_Inserted(object sender, LinqDataSourceStatusEventArgs e)
        {
            if (e.Exception == null)
            {
                var pmod = (PAGE_MODULE )e.Result;

                this.InsertModulsForPageModule(pmod.PMOD_ID );
                
            }
            else
            {
                HandleErrors(e.Exception);
            }

        }


        protected void ldsModules_Deleted(object sender, LinqDataSourceStatusEventArgs e)
        {
            if (HandleErrors(e.Exception))
            {
                e.ExceptionHandled = true;
            }
            
        }
}
}
