﻿<%@ WebHandler Language="C#" Class="FileUpload" %>

using System;
using System.Web;
using System.IO;
using System.Text;
using System.Web.Script.Serialization;





public class FileUpload : IHttpHandler {

    public class FileUploadResponse {
        public int Result { get; set; }
        public string Response { get; set; }
        public string Data { get; set; }
    
    }
    
    

    public void ProcessRequest(HttpContext context)
    {
        if (context.Request.Files.Count == 0)
            return;
        
        
        // get file
        string strFileName = Path.GetFileName(context.Request.Files[0].FileName);
        string strExtension = Path.GetExtension(context.Request.Files[0].FileName).ToLower();


        SM.EM.Ajax.ReturnData ret = ProcessTicket(context, strFileName, strExtension);

        // set response        
        JavaScriptSerializer serializer = new JavaScriptSerializer();

        //context.Response.ContentType = "application/json";
        context.Response.ContentType = "text/html";
        context.Response.Charset = "utf-8";
        context.Response.Write(serializer.Serialize(ret));

        
        
    }

    protected SM.EM.Ajax.ReturnData ProcessTicket(HttpContext context, string fileName, string fileExt)
    {

        SM.EM.Ajax.ReturnData ret = new SM.EM.Ajax.ReturnData { Code = SM.EM.Ajax.ReturnCode.Failure, Message = " no go! " };
        
        // check user
        if (!context.User.Identity.IsAuthenticated)
            return ret;

        //if (context.Request.QueryString["t"] == null)
        //    return;

        //if (!SM.EM.Helpers.IsGUID( context.Request.QueryString["t"]))
        //    return;
                
        //Guid ticketID = new Guid( context.Request.QueryString["t"]) ;
        

        //EM_USER usr = EM_USER.GetUserByUserName(context.User.Identity.Name);
        EM_USER usr = EM_USER.Current.GetCurrentPage();
        if (usr == null)
            return ret;


        // refID
        Guid RefID = Guid.Empty  ;
        if (context.Request.Params["RefID"] != null)
        {
            RefID = new Guid(context.Request.Params["RefID"]);
        }
        // type
        short fType = -1;
        if (context.Request.Params["FType"] != null)
        {
            fType  = short.Parse((context.Request.Params["FType"]));
        }   
        // lang
        string LangID = "";
        if (context.Request.Params["LangID"] != null)
        {
            LangID  = context.Request.Params["LangID"];
        }               
        
        
        // get folder to save
        string user = "annonymous";
        
            user = usr.PAGE_NAME ;

        // fileid
        Guid fileid = Guid.NewGuid();

        // upload folder
        string folder = FILE.Common.PRODUCT_FILES_FOLDER + user + "/";
        if (!System.IO.Directory.Exists(context.Server.MapPath(folder)))
            System.IO.Directory.CreateDirectory(context.Server.MapPath(folder));
        string file = "";
        file = folder + fileid + FILE.Common.SECURE_FILE_ENDING;
        // save file
        context.Request.Files[0].SaveAs(context.Server.MapPath(file));   

        
        // add file to db
        eMenikDataContext db = new eMenikDataContext();
        
        FILE r = new FILE();
        r.FileID = fileid;
        r.DateAdded = DateTime.Now;
        r.UserId = usr.UserId;
        // file
        r.Name = fileName;
        r.Folder = folder;
        r.Type = context.Request.Files[0].ContentType;
        r.Size = context.Request.Files[0].ContentLength;
        r.FileType = FILE.Common.FileType.TICKET_ORDER;
        r.RolesDownload = "?";
        
        if (RefID != Guid.Empty )
            r.RefID = RefID;
        if (fType != -1)
            r.FileType = fType ;
        
        r.Desc = "";

        // save to db
        db.FILEs.InsertOnSubmit(r);
        db.SubmitChanges();
        
        // insert product file lang
        if (fType == FILE.Common.FileType.PRODUCT) {
            FileRep frep = new FileRep();
            frep.AddProductFileLang(new PRODUCT_FILE_LANG { ProdFileID = Guid.NewGuid(), FileID = r.FileID, LANG_ID = LangID, PRODUCT_ID = RefID });
            frep.Save();
        }
        
        
        // set response    
        ret.Message = "Datoteka je dodana";
        ret.Code = SM.EM.Ajax.ReturnCode.OK;
        ret.Data = FILE.Common.Render.FileLinkOrder(SM.BLL.Common.ResolveUrl("~/" + fileid + FILE.Common.SECURE_FILE_ENDING), r.Name); ;
        
        
        //FileUploadResponse res = new FileUploadResponse ();
        //res.Result = 1;
        //res.Response = "Datoteka je dodana";
        //res.Data = FILE.Common.Render.FileLinkOrder(SM.BLL.Common.ResolveUrl("~/" + fileid + FILE.Common.SECURE_FILE_ENDING), r.Name);

        //JavaScriptSerializer serializer = new JavaScriptSerializer();


        ////context.Response.ContentType = "application/json";
        //context.Response.ContentType = "text/html";
        //context.Response.Charset = "utf-8";
        //context.Response.Write(serializer.Serialize(res));

        return ret;
    
    } 
    
    

 
    public bool IsReusable {
        get {
            return false;
        }
    }

}