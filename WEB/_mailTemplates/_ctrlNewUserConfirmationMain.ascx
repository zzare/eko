﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_ctrlNewUserConfirmationMain.ascx.cs" Inherits="_mailTemplates_ctrlNewUserConfirmationMain" %>


<p>
    Pozdravljeni!
    <br />    
    <br />     
    Registrirali ste se v sistemu <%= SM.BLL.Common.Emenik.Data.PortalName()%>
    <br />
    ---------------------------------------
    <br />    
    <br />    
    Uporabniško ime:
    <br />
    <%=UserName %>
    <br />    
    <br />    
    Geslo:
    <br />
    <%=Password %>
    <br />    
    <br />

    <a href='<%= PageHref %>'><%= PageHref %></a>

    <br />    
    <br />

    Lep pozdrav,
    ekipa <%= SM.BLL.Common.Emenik.Data.PortalName () %>

</p>