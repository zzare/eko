﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_stepShippingEko.ascx.cs" Inherits="_em_content_order__ctrl__stepShippingEko" %>
<%@ Register Namespace="SM.EM.UI.Controls" TagPrefix="EM"  %>
<%@ Register TagPrefix="SM" TagName="EditModulePublic" Src="~/_ctrl/module/_edit/editModulePublic.ascx"   %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
 
<script type="text/javascript" >

    $(document).ready(function () {
        if ($('#<%= cbShipping.ClientID %>').is(':checked'))
            $('.cObdarovanec').show();
        else
            $('.cObdarovanec').hide();


        $('#<%= cbShipping.ClientID %>').change(function () {
            $('.cObdarovanec').toggle();
        });

    });


    function copyAddrC(t) {
        $('#<%= tbCFirstname.ClientID %>').val($(t).parent().find('span.firstname').html());
        $('#<%= tbCLastname.ClientID %>').val($(t).parent().find('span.lastname').html());
        $('#<%= tbCAddress.ClientID %>').val($(t).parent().find('span.addr').html());
        $('#<%= tbCCity.ClientID %>').val($(t).parent().find('span.city').html());
        $('#<%= tbCTititle.ClientID %>').val($(t).parent().find('span.title').html());
        $('#<%= tbCTax.ClientID %>').val($(t).parent().find('span.tax').html());
        $('#<%= tbCPhone.ClientID %>').val($(t).parent().find('span.phone').html());

    }


</script>



<SM:EditModulePublic ID="objEditPublic" runat="server" />

<div class="stepHeaderW">
<h1><%= GetGlobalResourceObject("Product", "step2Title1") %></h1>
</div>

<div class="stepShoppingCart" >
    <div class="contentRightW">
        <%--<span class="contentRightHeadT_Text">PODATKI ZA RAČUN</span>--%>
            <div class="contentRightMI">

                <asp:PlaceHolder ID="phRacun" runat="server" Visible="false">
                <div id="divCompany">                    
                    <div class="step3L">
                    <table class="tableOrder_step3_company" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <%= GetGlobalResourceObject("Product", "step2Name") %>*:    
                            </td>
                            <td>
                                <asp:TextBox ID="tbCFirstname" MaxLength="256"  runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator EnableClientScript="false" ValidationGroup="vgCompany" ControlToValidate="tbCFirstname" ID="RequiredFieldValidator8" runat="server" ErrorMessage="* Vpišite ime"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%= GetGlobalResourceObject("Product", "step2Lastname") %>*:    
                            </td>
                            <td>
                                <asp:TextBox ID="tbCLastname" MaxLength="256"  runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator EnableClientScript="false" ValidationGroup="vgCompany" ControlToValidate="tbCLastname" ID="RequiredFieldValidator9" runat="server" ErrorMessage="* Vpišite priimek"></asp:RequiredFieldValidator>
                            </td>
                        </tr>


                                             
                        <tr>
                            <td>
                                <%= GetGlobalResourceObject("Product", "step2Address") %>*:
                            </td>
                            <td>
                                <asp:TextBox ID="tbCAddress" MaxLength="256"  runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator EnableClientScript="false" ValidationGroup="vgCompany" ControlToValidate="tbCAddress" ID="RequiredFieldValidator5" runat="server" ErrorMessage="* Vpišite naslov"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%= GetGlobalResourceObject("Product", "step2Postal") %>*:
                            </td>
                            <td>
                                <asp:TextBox ID="tbCCity" MaxLength="256"  runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator EnableClientScript="false" ValidationGroup="vgCompany" ControlToValidate="tbCCity" ID="RequiredFieldValidator6" runat="server" ErrorMessage="* Vpišite pošto in kraj"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%= GetGlobalResourceObject("Product", "step2Phone") %>*:
                            </td>
                            <td>
                                <asp:TextBox ID="tbCPhone" MaxLength="256"  runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator EnableClientScript="false" ValidationGroup="vgCompany" ControlToValidate="tbCPhone" ID="RequiredFieldValidator4" runat="server" ErrorMessage="* Vpišite telefon"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%= GetGlobalResourceObject("Product", "step2Company") %>:    
                            </td>
                            <td>
                                <asp:TextBox ID="tbCTititle" MaxLength="256"  runat="server"></asp:TextBox>
<%--                                <asp:RequiredFieldValidator EnableClientScript="false" ValidationGroup="vgCompany" ControlToValidate="tbCTititle" ID="RequiredFieldValidator4" runat="server" ErrorMessage="* Vpišite naziv"></asp:RequiredFieldValidator>--%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%= GetGlobalResourceObject("Product", "step2Tax") %>:    
                            </td>
                            <td>
                                <asp:TextBox ID="tbCTax" MaxLength="256"  runat="server"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator EnableClientScript="false" ValidationGroup="vgCompany" ControlToValidate="tbCTax" ID="RequiredFieldValidator7" runat="server" ErrorMessage="* Vpišite davčno št."></asp:RequiredFieldValidator>--%>
                            </td>
                        </tr>   
<%--                        <tr>
                            <td>DDV zavezanec:</td>
                            <td>
                                
                                <asp:CheckBox class="cbZavezanec" ID="cbZavezanec" runat="server" Text = "&nbsp;Sem zavezanec za DDV" />
                            </td>
                        </tr> --%>

                    </table> 
                    </div>
                    <div class="step3R">
                    <asp:PlaceHolder ID="phCopyC" runat="server">
                        <div class="ord_prevdataW">
                            <div class="ord_prevdataT"></div>
                            <div class="ord_prevdataM">
                                <div class="ord_prevdataMI">
                                    <%= GetGlobalResourceObject("Product", "step2UseMyData") %>
                                    <br />
                                    <asp:ListView ID="lvListCAddr" runat="server" >
                                        <LayoutTemplate>
                                                <asp:PlaceHolder ID="itemPlaceholder" runat="server"/>
                                            
                                        </LayoutTemplate>
                                        
                                        <ItemTemplate>
                                            <div >
                                                <a href="#" onclick="copyAddrC(this); return false" >&laquo;&nbsp;<%# RenderDisplayName(Container.DataItem ) %></a>
                                                <span style="display:none" class="firstname" ><%# Eval("FirstName")%></span>
                                                <span style="display:none" class="lastname" ><%# Eval("LastName")%></span>
                                                <span style="display:none" class="addr" ><%# Eval("Address")%></span>
                                                <span style="display:none" class="city"><%# Eval("City")%></span>
                                                <span style="display:none" class="title"><%# Eval("Company")%></span>
                                                <span style="display:none" class="tax"><%# Eval("TaxNo")%></span>
                                                <span style="display:none" class="phone"><%# Eval("Phone")%></span>
<%--                                                <span style="display:none" class="zav"><%# Eval("Zavezanec").ToString().ToLower()%></span>--%>
                                            </div >
                                        
                                        
                                        </ItemTemplate>
                                        
                                    </asp:ListView>
                                </div>    
                            </div>
                            <div class="ord_prevdataB"></div>
                        </div>
                    </asp:PlaceHolder>                                       
                </div>
                <div class="clearing"></div>
                </div>  

                </asp:PlaceHolder>



                <span class="contentRightHeadT_Text"><%= GetGlobalResourceObject("Product", "step2Title2") %></span>
            
<% if (LANGUAGE.GetCurrentLang() == "si"){%>
                <p><b>Izbrane izdelke vam lahko dostavimo na dom, v službo ali na kakšno drugo izbrano lokacijo.</b></p>
<%}else if (LANGUAGE.GetCurrentLang() == "en"){ %>  
                <%--<p><b>Izbrane izdelke vam lahko dostavimo na dom, v službo ali na kakšno drugo izbrano lokacijo.</b></p>--%>
<%}else if (LANGUAGE.GetCurrentLang() == "it"){ %>  
                <%--<p><b>Izbrane izdelke vam lahko dostavimo na dom, v službo ali na kakšno drugo izbrano lokacijo.</b></p>--%>
<%} %>

                <table class="tableOrder_step3" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <%= GetGlobalResourceObject("Product", "step2Name") %>:
                        </td>
                        <td>
                            <%= Customer.FIRST_NAME  %>  <%= Customer.LAST_NAME  %>
                        </td>
                    </tr>
                </table>

               
                    
            
                <div id="divShipping">
                    <div id="cShipping"  >
                        <div class="step3L">
                            <%= GetGlobalResourceObject("Product", "step2TitleAddress") %>:

                            
                            <table class="tableOrder_step3" border="0" cellpadding="0" cellspacing="0">
                            <%--visible if not payed by delivery--%>
                            <% if (Order != null && Order.PaymentType != SHOPPING_ORDER.Common.PaymentType.DELIVERY){%>
                                 <tr  >
                                    <td>
                                        <asp:CheckBox class="order_cbCompany order_cbFriend" ID="cbShipping" runat="server" Text="&nbsp;Želim obdariti prijatelja" />:
                                    </td>
                                    <td >
                                        <div class="cObdarovanec">
                                            <asp:TextBox ID="tbObdarovanec" MaxLength="256"  runat="server" ></asp:TextBox>
                                             <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" WatermarkCssClass="WMmainLogin_new"  WatermarkText="vpiši ime in priimek" TargetControlID ="tbObdarovanec"></cc1:TextBoxWatermarkExtender>
        
                                            <asp:RequiredFieldValidator EnableClientScript="false" ValidationGroup="vgShipping" ControlToValidate="tbObdarovanec" ID="rfvObdarovanec" runat="server" ErrorMessage="* Vpišite ime obdarovanca"></asp:RequiredFieldValidator>
                                        </div>                                        
                                    </td>
                                </tr>
                                <%--<tr class="cObdarovanec" >
                                    <td colspan="2">
                                        <span>Vpišite podatke obdarovanca</span>
                                    </td>
                                </tr>--%>
                                <%} %>
                                <tr>
                                    <td>
                                        <%= GetGlobalResourceObject("Product", "step2Date") %> *:
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlShipping" runat="server" AppendDataBoundItems="true">
                                        
                                           <%-- <asp:ListItem Text=" - Izberi - " Value="" ></asp:ListItem>--%>
                                            <%--<asp:ListItem Text="9.11.2011" Value="9.11.2011" ></asp:ListItem>
                                            <asp:ListItem Text="16.11.2011" Value="16.11.2011" ></asp:ListItem>
                                            <asp:ListItem Text="23.11.2011" Value="23.11.2011" ></asp:ListItem>--%>
                                        </asp:DropDownList>

                                        <asp:RequiredFieldValidator EnableClientScript="false" ValidationGroup="vgShipping" ControlToValidate="ddlShipping" ID="rfvDate" runat="server" ErrorMessage="* Izberite datum"></asp:RequiredFieldValidator>
                
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <%= GetGlobalResourceObject("Product", "step2Hour") %> *:
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlHour" runat="server">

                                            <%--<asp:ListItem Text=" - Izberi - " Value="" ></asp:ListItem>--%>
                                        
                                            
                                           <%-- <asp:ListItem Text="10:00 - 12:00" Value="10:00 - 12:00" ></asp:ListItem>
                                            <asp:ListItem Text="12:00 - 14:00" Value="12:00 - 14:00" ></asp:ListItem>--%>
                                           <%-- <asp:ListItem Text="14:00 - 16:00" Value="14:00 - 16:00" ></asp:ListItem>
                                            <asp:ListItem Text="16:00 - 18:00" Value="16:00 - 18:00" ></asp:ListItem>
                                            <asp:ListItem Text="18:00 - 20:00" Value="18:00 - 20:00" ></asp:ListItem>
                                            <asp:ListItem Text="20:00 - 22:00" Value="20:00 - 22:00" ></asp:ListItem>--%>
                                        </asp:DropDownList>

                                        <%--<asp:TextBox ID="tbTime" MaxLength="256"  runat="server"></asp:TextBox>--%>
                                        <asp:RequiredFieldValidator EnableClientScript="false" ValidationGroup="vgShipping" ControlToValidate="ddlHour" ID="rfvHour" runat="server" ErrorMessage="* Izberite želeno uro"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <%= GetGlobalResourceObject("Product", "step2Region") %> *:
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlRegion" runat="server">
                                        
                                           <%-- <asp:ListItem Text=" - Izberi - " Value="" ></asp:ListItem>--%>
<%--                                            <asp:ListItem Text="Ljubljana in okolica (brezplačna dostava)" Value="1" ></asp:ListItem>
                                            <asp:ListItem Text="Nova Gorica in okolica (brezplačna dostava)" Value="2" ></asp:ListItem>
                                            <asp:ListItem Text="* Ostalo (doplačilo za dostavo 15€)" Value="3" ></asp:ListItem>--%>
                                        </asp:DropDownList>

                                        <%--<asp:TextBox ID="tbTime" MaxLength="256"  runat="server"></asp:TextBox>--%>
                                        <asp:RequiredFieldValidator EnableClientScript="false" ValidationGroup="vgShipping" ControlToValidate="ddlRegion" ID="rfvRegion" runat="server" ErrorMessage="* Izberite regijo"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <%= GetGlobalResourceObject("Product", "step2City") %> <span class="cObdarovanec"><%= GetGlobalResourceObject("Product", "step2Prejemnika")%> </span>*:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tbCity" MaxLength="256"  runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator EnableClientScript="false" ValidationGroup="vgShipping" ControlToValidate="tbCity" ID="rfvCity" runat="server" ErrorMessage="* Vpišite kraj"></asp:RequiredFieldValidator>
                
                                      </td>
                                </tr>

                                <tr>
                                    <td>
                                        <%= GetGlobalResourceObject("Product", "step2Address") %> <span class="cObdarovanec"><%= GetGlobalResourceObject("Product", "step2Prejemnika")%> </span>*:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tbAddress" MaxLength="256"  runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator EnableClientScript="false" ValidationGroup="vgShipping" ControlToValidate="tbAddress" ID="rfvAddress" runat="server" ErrorMessage="* Vpišite naslov"></asp:RequiredFieldValidator>
                
                                      </td>
                                </tr>
                                <tr>
                                    <td>
                                        <%= GetGlobalResourceObject("Product", "step2Phone") %> <span class="cObdarovanec"><%= GetGlobalResourceObject("Product", "step2Prejemnika") %> </span>*:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tbPhone" MaxLength="256"  runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator EnableClientScript="false" ValidationGroup="vgShipping" ControlToValidate="tbPhone" ID="rfvPhone" runat="server" ErrorMessage="* Vpišite telefon"></asp:RequiredFieldValidator>
                
                                      </td>
                                </tr>
                                <tr>
                                    <td>
                                        <%= GetGlobalResourceObject("Product", "step2Notes") %>:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tbNotes" MaxLength="256"  runat="server"></asp:TextBox>
                                    </td>
                                </tr>


                            </table>
                        </div>

                        <div class="clearing"></div>
                    </div>
                </div>
                

                
                <%--<asp:CheckBox class="order_cbCompany" ID="cbCompany" runat="server" Text="&nbsp;Račun želim prejeti na podjetje" />--%>
              
                <br />                                
                <div class="stepnextbuttonW">
                <a class="Astepnext" runat="server" onserverclick="btNext_Click" ><span><%= GetGlobalResourceObject("Product", "step2Next") %> &raquo;</span></a>
                </div>
                <div class="clearing"></div>
            </div>
    </div>



</div>



