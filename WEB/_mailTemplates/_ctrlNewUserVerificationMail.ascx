﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_ctrlNewUserVerificationMail.ascx.cs" Inherits="_mailTemplates_ctrlNewUserVerificationMail" %>


<table width="668" cellpadding="0" cellspacing="0" border="0" style="background:#fff;">
    <tbody>
        <tr>
            <td>
                <%--<img align="right" alt="eko" src='<%= SM.BLL.Common.ResolveUrl("~/_em/templates/_custom/kolpasan/_inc/images/mailtemplate/kolpa_mailtemp_head.jpg") %>' />--%>
                <br />    
                Pozdravljeni!
                <br />    
                <br />     
                To je potrditveni email. Prejeli ste ga, ker ste se registrirali na strani <%=PageHref%>.
                <br />    
                <br />    
                 Uporabniško ime:
                <br />
                <%=UserName %>
                <br />    
                <br />    
                Geslo:
                <br />
                <%=Password %>
                <br />    
                <br />   
                Preden lahko končate z nakupom, morate s klikom na spodnjo povezavo potditi svoj email naslov. S tem bo postopek registracije zaključen.
                <br />    
                <br />

                <a href='<%= VerifyUrl %>'><%= VerifyUrl %></a>

                <br />    
                <br />

                Lep pozdrav, 
                <%= PageName %>
                <br />
                <br /><br />
            </td>
        </tr>
<%--        <tr>
            <td>
                <img alt="eko" src='<%= SM.BLL.Common.ResolveUrl("~/_em/templates/_custom/kolpasan/_inc/images/mailtemplate/kolpa_mailtemp_foot.jpg") %>' />
            </td>
        </tr>--%>

    </tbody>
</table>