﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewArticleList2.ascx.cs" Inherits="_ctrl_module_product_viewArticleList2" %>

<div id="divArticles" runat="server" >
    <asp:ListView ID="lvList" runat="server" >
        <LayoutTemplate>
            
                <asp:PlaceHolder ID="itemPlaceholder" runat="server"/>

        </LayoutTemplate>
        <ItemTemplate>
        
            <asp:PlaceHolder ID="phFirst" runat="server" Visible='<%# (bool) (Container.DataItemIndex == 0) %>'>



                <div class="artBox <%# RenderArtExposedClass(Container.DataItem) %>">
                <div class="artboxI">
                        <h2 class="articleH2"><a href='<%#  RenderArtHref(Container.DataItem) %>'><%#  RenderArtTitle(Container.DataItem) %></a></h2>
<% if (ShowDesc){%>                    
                        <span class="artBox_date"><%#  RenderDatePublished(Container.DataItem)%></span>
<%} %>                    
                        <p><span class="art_main_abst">  <%#  RenderArtDesc(Container.DataItem) %></span></p>
                
                <div id="Div2" runat="server"  visible='<%# (bool)( ShowImage && HasImage(  Container.DataItem)) %>' >            
<% if (IsModeCMS) {%>
                    <a style="position:absolute;" class="divGalImgThumb_btnEdit" href="#" onclick="editCustomCrop('<%# Eval("IMG_ID") %>', '<%# Eval("TYPE_ID") %>'); return false" ></a>
<%} %>
                    <a  class="AartBox_img" href='<%#  RenderArtHref(Container.DataItem) %>' title='<%#  RenderArtTitle(Container.DataItem) %>'>
                        <img src='<%# SM.BLL.Common.ResolveUrl ( Eval("ART_IMAGE").ToString() )  %>' alt='<%#  RenderArtTitle(Container.DataItem) %>' />
                    </a>
                </div>
<% if (IsModeCMS)
   {%>
                    <a  class="btEditArticle" href="#" onclick="editArticleLoad('<%= CwpID.ToString() %>', <%# Eval("ART_ID") %>); return false" ></a>
<%} %>    
                    <%#  RenderArtShortBody(Container.DataItem, Container.DataItemIndex)%>
                    <a class="art_more" href='<%#  RenderArtHref(Container.DataItem) %>'>Več&nbsp;&raquo;</a>
                    <div class="clearing"></div>

                
                
                </div>
                </div>
                <span class="art_sepa"></span>

            </asp:PlaceHolder>
            
            <asp:PlaceHolder ID="phRest" runat="server" Visible='<%# (bool) (Container.DataItemIndex > 0) %>'>
                <div class="artBox <%# RenderArtExposedClass(Container.DataItem) %>">
                    <div class="artBoxI">
                        <h2 class="articleH2"><a href='<%#  RenderArtHref(Container.DataItem) %>'><%#  RenderArtTitle(Container.DataItem) %></a></h2>
                        <span class="artBox_date"><%#  RenderDatePublished(Container.DataItem)%></span>

                        <div id="Div1" runat="server"  visible='<%# (bool)( ShowImage && HasImage(  Container.DataItem)) %>' >
    <% if (IsModeCMS) {%>
                            <a style="position:absolute;" class="divGalImgThumb_btnEdit" href="#" onclick="editCustomCrop('<%# Eval("IMG_ID") %>', '<%# Eval("TYPE_ID") %>'); return false" ></a>
    <%} %>
                            <a class="AartBox_img" href='<%#  RenderArtHref(Container.DataItem) %>' title='<%#  RenderArtTitle(Container.DataItem) %>'>
                                <img src='<%# SM.BLL.Common.ResolveUrl ( Eval("ART_IMAGE").ToString() )  %>' alt='<%#  RenderArtTitle(Container.DataItem) %>' />
                            </a>
                        </div>
<% if (IsModeCMS){%>
                    <a  class="btEditArticle" href="#" onclick="editArticleLoad('<%= CwpID.ToString() %>', <%# Eval("ART_ID") %>); return false" ></a>

<%} %>                        

                                        
                                                                   
                        <p><%#  RenderArtDesc(Container.DataItem) %></p>
                        <a class="art_more" href='<%#  RenderArtHref(Container.DataItem) %>'>Več&nbsp;&raquo;</a>
                        <div class="clearing"></div>

                    
                    </div>
                    
                    
                    
                    <div class="clearing"></div>
    <% if (ShowFooter){%>                
                    <div class="artFoot" >
                        <div id="artFootWrapper" class="clearfix" > 
                            <div id="artFootTwoCols" class="clearfix"> 
                                <div id="artFootMain" >            
                                    <%# RenderComments(Container.DataItem ) %>
                                </div>
                                <div id="artFootRight" >
                                    <%# RenderDatePublished(Container.DataItem ) %>
                                </div>
                            </div> 
                            <div id="artFootLeft" >
                                <%# RenderAuthor(Container.DataItem ) %>
                            </div>
                        </div>
                    </div>
    <%} %>                
                </div>        
            <span class="art_sepa"></span>
            </asp:PlaceHolder>
            

            
        </ItemTemplate>
    </asp:ListView>
    
</div>

<asp:PlaceHolder ID="phEmpty" runat="server" Visible="false">
    Seznam je prazen
</asp:PlaceHolder>

<asp:PlaceHolder ID="phPager" runat="server" >
    <div class="pagesortW">
        <div class="pagesort">
            <a <%= Pager.RenderPageFullUrlHREF( 1) %> class="pagesort_fbw">&nbsp;</a>
            <a <%= Pager.RenderPageFullUrlHREF( Pager.GetCurrentPage()-1) %> class="pagesort_bw">&nbsp;</a>
            
            <%= Pager.RenderPageLinks(-1) %> 
            <a <%= Pager.RenderPageFullUrlHREF( Pager.GetCurrentPage()+1) %> class="pagesort_fwd">&nbsp;</a>
            <a <%= Pager.RenderPageFullUrlHREF( Pager.TotalPages) %> class="pagesort_ffwd">&nbsp;</a>
            <div class="clearing"></div>
        </div>
    </div> 
</asp:PlaceHolder>