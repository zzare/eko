﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewProductManufacturerLookup.ascx.cs" Inherits="_ctrl_module_product_viewProductManufacturerLookup" %>

<script language = "javascript" type="text/javascript">

    $(document).ready(function() {
    $('#lkpProductManufacturer li').click(function() {
            
            var tit = $('#<%= TitleID %>');
            var hf = $('#<%= ValueID %>');
            tit.html($(this).children("span").eq(0).html());
            hf.val(this.id);

            closeModalLoader();
            return false;
        });
    });

</script>



<div class="modPopup_header modPopup_headerEdit">
    <h4>IZBERI PROIZVAJALCA</h4>
    <p>Izberi proizvajalca izdelka</p>
</div>
<div class="modPopup_main">
    <div id="lkpProductManufacturer" class="lkpProductSmap">
        <%= RenderItems() %>
    </div>

</div>   

<div class="modPopup_footer"> 
    <div class="buttonwrapperModal">
<%--        <a class="lbutton lbuttonConfirm" href="#" onclick="selectLookup(); return false"><span>Dodaj nov izdelek</span></a>--%>
        <a class="lbutton lbuttonDelete" href="#" onclick="closeModalLoader(); return false" ><span>Prekliči</span></a>
    </div>
</div>
