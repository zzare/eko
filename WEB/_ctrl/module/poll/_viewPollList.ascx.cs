﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _ctrl_module_poll_viewPollList : BaseControlView
{

    public object Data;
    public object[] Parameters;
    public string LangID;
    public bool ShowResultsIfVoted = true;



    protected void Page_Load(object sender, EventArgs e)
    {
        LoadList();
    }



    public void LoadList() {

        if (Data == null)
            return;

        if (Parameters != null)
        {
            if (Parameters[0] != null)
                IsModeCMS = bool.Parse(Parameters[0].ToString());
            if (Parameters.Length > 1 && Parameters[1] != null)
                LangID = Parameters[1].ToString();
        }

        lvList.DataSource = Data; 
        lvList.DataBind();

        if (lvList.Items.Count <= 0)
            phEmpty.Visible = true;

    }



    protected string RenderPoll(object d ) {
        POLL data = (POLL)d;
        return ViewManager.RenderView("~/_ctrl/module/poll/_viewPoll.ascx", data, new object[] {IsModeCMS, LangID , data.CWP_ID,  ShowResultsIfVoted   });
    
    }

}
