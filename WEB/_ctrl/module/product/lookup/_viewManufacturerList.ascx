﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewManufacturerList.ascx.cs" Inherits="SM.UI.Controls._ctrl_module_product_lookup_viewManufacturerList" %>

<ul   >
    <asp:ListView ID="lvSort" runat="server" >
        <LayoutTemplate>
            <asp:PlaceHolder ID="itemPlaceHolder" runat="server"></asp:PlaceHolder>
        </LayoutTemplate>
                             
        <ItemTemplate >
           <li id='<%# Eval("ManufacturerID") %>' >
                <span><%# Eval("Title") %></span>
            </li>            
        </ItemTemplate>
    
    </asp:ListView>

</ul>