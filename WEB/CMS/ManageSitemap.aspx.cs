﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;


namespace SM.EM.UI.CMS
{

    public partial class ManageSiteMap : BasePage_CMS
    {
        public int MenuID {
            get { 
                int _menuID = -1;
                if (ddlMenu.SelectedIndex >= 0)
                {
                    if (Int32.TryParse(ddlMenu.SelectedValue, out _menuID))
                        return _menuID;
                }
                return _menuID;
            }
        }

        SITEMAP fvSitemap;
        protected int ParentID{
        get{
            if (ViewState["ParentID"] == null)
                return -1;
            return Int32.Parse(ViewState["ParentID"].ToString());
            //return int.Parse(hfParentID.Value);
        }
            set{
                ViewState["ParentID"] = value;
                //hfParentID.Value = value.ToString();
                //SetError(hfParentID.Value);
            }
            
        }
        protected int SitemapID
        {
            get
            {
                if (ViewState["SitemapID"] == null)
                    return -1;
                return Int32.Parse(ViewState["SitemapID"].ToString());
            }
            set { 
                ViewState["SitemapID"] = value;
                ToggleForSitemapId();
            }
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // load values
                lSitemapID.Text = "";


                InitData();
                
                // LoadParentTreeview AFTER DDLs
                LoadParentTreeview(tvParent);
                LoadParentTreeview(tvSitemap);

                // init form AFTER treeviews
                InitForm();


                // load Roles cbl
                cblRoles.DataSource =  Roles.GetAllRoles();
                cblRoles.DataBind();
            }

            // if Sitemap not set, hide some controls
            ToggleForSitemapId();
          
        }
        protected void ToggleForSitemapId() {
            if (SitemapID < 0)
            {
                panSmapLang.Visible = false;
                panSmapLangHD.Visible = false;
            }
            else
            {
                panSmapLang.Visible = true;
                panSmapLangHD.Visible = true;
            } 
        
        }
        protected void InitData() {
            // load DDLs
//            ddlMenu.DataSource = MENU.GetMenusBackend();
            ddlMenu.DataSource = MENU.GetMenusActive ();
            ddlMenu.DataTextField = "MENU_TITLE";
            ddlMenu.DataValueField = "MENU_ID";
            ddlMenu.DataBind();
            if (Request.QueryString["m"]!= null){
                string _menu = Request.QueryString["m"].ToString();
                    ((ListItem)ddlMenu.Items.FindByValue(_menu)).Selected = true;
            }

//            ddlModule.DataSource = PAGE_MODULE.GetModulesCms();
            ddlModule.DataSource = PAGE_MODULE.GetModules();
            ddlModule.DataTextField = "PMOD_TITLE";
            ddlModule.DataValueField = "PMOD_ID";
            ddlModule.DataBind();
        
        }

        protected void LoadParentTreeview(TreeView tv) {

            tv.Nodes.Clear();

            // get root value
            SITEMAP root = (SITEMAP)SITEMAP.GetRootByMenu(MenuID);

            if (root == null) return;

            // add child nodes
            TreeNode rootNode = new TreeNode (root.SMAP_TITLE, root.SMAP_ID.ToString());
            AddChildrenNodes(rootNode);

            TreeNode rootNodeSM = new TreeNode() ;
            rootNodeSM = rootNode;
            // add root node to tv
            tv.Nodes.Add(rootNode);
            tv.DataBind();

            // init parent
            //ParentID = root.SMAP_ID ;

        }

        protected void AddChildrenNodes(TreeNode parent ) { 

            // add all the child nodes        
            IEnumerable <SITEMAP> childs = SITEMAP.GetSiteMapChildsByID(Int32.Parse(parent.Value), MenuID);
            foreach (SITEMAP sm in childs) {
                TreeNode node = new TreeNode(sm.SMAP_TITLE, sm.SMAP_ID.ToString() );
                parent.ChildNodes.Add(node);
                AddChildrenNodes(node);
                if (sm.SMAP_ID == ParentID )
                    node.Select();
            }        
        }

        protected void gvSiteMap_RowUpdated(object sender, GridViewUpdatedEventArgs e)
        {
            if (HandleErrors(e.Exception))
            {
                e.ExceptionHandled = true;
                e.KeepInEditMode = true;
            }
        }
        protected void btSave_Click(object sender, EventArgs e)
        {
            UpdateSitemap();
        }
        protected void btNew_Click(object sender, EventArgs e)
        {
            InitForm();
            SetStatus("Add new Sitemap Node");
            gvSmapLang.DataBind();

            dvSmapLang.Visible = false;

            //dvSmapLang.DataBind();
            

        }
        protected void btDelete_Click(object sender, EventArgs e)
        {
            DeleteSitemap();
        }
        protected void btRefresh_Click(object sender, EventArgs e)
        {
            RefreshControls();
            RefreshCMSSitemapProvider("m", ddlMenu.SelectedValue);
        }
        protected void DeleteSitemap()
        {
            if (SitemapID == -1 )
                return;


            eMenikDataContext db = new eMenikDataContext();

            var sm = from s in db.SITEMAPs where s.SMAP_ID == SitemapID select s;
            db.SITEMAPs.DeleteAllOnSubmit(sm);
            SitemapID = -1;
            SaveData (db, true);
            
        }

        protected bool ValidateSitemap() {
            Page.Validate("newSmap");
            return Page.IsValid;
        }

        protected void UpdateSitemap() {
            if (!ValidateSitemap())
                return;
            eMenikDataContext db = new eMenikDataContext();

            // insert new
            if (SitemapID == -1)
            {
                if (fvSitemap == null){
                    fvSitemap = new SITEMAP();
                }
                PreSave();


                db.SITEMAPs.InsertOnSubmit(fvSitemap);
                SaveData(db, false );

                this.InsertModulsForSitemapModule(fvSitemap.SMAP_ID );
                SITEMAP.InsertSitemapLang(fvSitemap.SMAP_ID, LANGUAGE.GetCurrentLang(), fvSitemap.SMAP_TITLE, fvSitemap.SMAP_TITLE, true, fvSitemap.SMAP_TITLE, "", 100);

                RefreshCMSSitemapProvider("m", ddlMenu.SelectedValue);


            }
            else {
                // update old
                if (fvSitemap == null)
                {
                    //fvSitemap = new SITEMAP();
                    fvSitemap = db.SITEMAPs.Single(s => s.SMAP_ID == SitemapID);
                }
                PreSave();

                // insert modules
                SITEMAP.DeleteModulsForSitemapModule(SitemapID);
                this.InsertModulsForSitemapModule(SitemapID); 
                
                
                SaveData(db, true);
                


                InitForm();
            }

        }

        protected void PreSave() {


            fvSitemap.MENU_ID = MenuID;
            fvSitemap.PMOD_ID = Int32.Parse(ddlModule.SelectedValue);
            fvSitemap.SMAP_TITLE = tbTitle.Text;
            fvSitemap.PARENT = ParentID; 

            fvSitemap.WWW_ROLES = tbWwwRoles.Text;
            fvSitemap.CMS_ROLES = tbCmsRoles.Text;
            fvSitemap.SMAP_ORDER = Int32.Parse(tbOrder.Text);
            fvSitemap.SMAP_ACTIVE = cbActive.Checked;
            fvSitemap.WWW_VISIBLE = cbWwwVisible.Checked;
            fvSitemap.SMAP_IMAGE = tbImage.Text;
            fvSitemap.CUSTOM_CLASS = tbClass.Text;
            fvSitemap.THEME = tbTheme.Text;

            if (!String.IsNullOrEmpty(tbWWW_URL.Text))
                fvSitemap.WWW_URL = tbWWW_URL.Text;
            else
                fvSitemap.WWW_URL = null;
            
        }

        protected void SaveData(eMenikDataContext db, bool refresh)
        {
            try {
                SM.EM.Caching.RemoveMenuCache(Convert.ToInt32(ddlMenu.SelectedValue));
                db.SubmitChanges();

            }
            catch (Exception ex){
                HandleErrors(ex);
            }

            if (refresh) {

                RefreshCMSSitemapProvider("m", ddlMenu.SelectedValue);
            }
            
        }

        // page moduls
        public void InsertModulsForSitemapModule(int smapID)
        {
            eMenikDataContext db = new eMenikDataContext();

            foreach (ListItem li in cblModules.Items)
            {
                if (li.Selected){
                    db.SITEMAP_MODULEs.InsertOnSubmit(new SITEMAP_MODULE  { SMAP_ID  = smapID, MOD_ID = int.Parse(li.Value) });
                }
            }

            db.SubmitChanges();
            SM.EM.Caching.RemoveMenuCache(Convert.ToInt32(ddlMenu.SelectedValue));
        }

        protected void RefreshControls() {
            LoadSitemapData(SitemapID);
            gvSiteMap.DataBind();
            LoadParentTreeview(tvParent);
            LoadParentTreeview(tvSitemap);
        
        }

        protected void InitForm() {
            fvSitemap = null;
            SitemapID = -1;
            lSitemapID.Text = "";
            tbTitle.Text = "";
            tbWwwRoles.Text="?";
            tbCmsRoles.Text = "?";
            tbOrder.Text = "100";
            cbActive.Checked = true;
            cbWwwVisible.Checked = true;
            tbImage.Text = "";
            tbClass.Text = "";
            tbTheme.Text = "";
            ParentID = -1;

            // if menu has allready root, enable parent validator
            if (tvSitemap.Nodes.Count < 1)
            {
            lParent.Text = "ROOT";
            }
            else {
                lParent.Text = "";

            }


            SetStatus("Select Sitemap Node");
        
        }
        protected void tvParent_SelectedNodeChanged(object sender, EventArgs e)
        {
            lParent.Text = tvParent.SelectedNode.Text;
            ParentID = Int32.Parse(tvParent.SelectedNode.Value);
        }


        protected void tvSitemap_SelectedNodeChanged(object sender, EventArgs e)
        {
            LoadSitemapData(Int32.Parse(tvSitemap.SelectedNode.Value));
            LoadParentTreeview(tvParent);
        }
        protected void LoadSitemapData(int id) {
            SitemapID = id;
            if (id == -1)                return;

            fvSitemap = (SITEMAP)SITEMAP.GetSiteMapByID(id);
            lSitemapID.Text = SitemapID.ToString();
            tbTitle.Text = fvSitemap.SMAP_TITLE ;
            tbWwwRoles.Text = fvSitemap.WWW_ROLES ;
            tbCmsRoles.Text = fvSitemap.CMS_ROLES;
            tbOrder.Text = fvSitemap.SMAP_ORDER.ToString();
            cbActive.Checked = fvSitemap.SMAP_ACTIVE;
            cbWwwVisible.Checked = fvSitemap.WWW_VISIBLE ;
            tbImage.Text = fvSitemap.SMAP_IMAGE ;
            tbClass.Text = fvSitemap.CUSTOM_CLASS ?? "";
            tbTheme.Text = fvSitemap.THEME ?? "";
            tbWWW_URL.Text = fvSitemap.WWW_URL ?? "";

            ParentID = (int)fvSitemap.PARENT;
            if (ParentID > 1) lParent.Text = SITEMAP.GetSiteMapByID(ParentID).SMAP_TITLE; else lParent.Text = ParentID.ToString();
            ddlModule.SelectedValue = fvSitemap.PMOD_ID.ToString();

            // load Modules RBL
            foreach (SITEMAP_MODULE  smod in SITEMAP.GetModulsForSitemapModuls(SitemapID ))
            {
                cblModules.Items.FindByValue(smod.MOD_ID.ToString()).Selected = true;
            }

            // load sitemap lang controls
            gvSmapLang.DataBind();

            dvSmapLang.Visible = true;
            
            dvSmapLang.DataBind();

            // display status
            SetStatus(fvSitemap.SMAP_TITLE);
        
        }

        protected void ddlModule_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            cblModules.DataBind();
            // preselect modules rbl
            foreach (PAGEM_MOD pmod in PAGE_MODULE.GetModulsForPageModuls(Convert.ToInt32( ddlModule.SelectedValue )))
            {
                cblModules.Items.FindByValue(pmod.MOD_ID.ToString()).Selected = true;
            }


        }



        protected void ddlMenu_SelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshControls();
        }
        protected void ldsSmapLang_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            eMenikDataContext db = new eMenikDataContext();

            var langs = from l in db.SITEMAP_LANGs where l.SMAP_ID == SitemapID select l;

            e.Result = langs;
        }
        protected void dvSmapLang_DataBound(object sender, EventArgs e)
        {
            if (SitemapID < 0) { 
                dvSmapLang.Visible = false;
                return;
            } 
            dvSmapLang.Visible = true;

            // set ID
            Literal smap = (Literal)dvSmapLang.FindControl("lSmapId");
            if (smap != null)
                smap.Text = SitemapID.ToString();

            // bind DDL LANG
            DropDownList ddlLanguage = (DropDownList)dvSmapLang.FindControl("ddlLanguage");
            if (ddlLanguage != null)
            {
                ddlLanguage.DataSource = LANGUAGE.GetSmapLangsNew(SitemapID);
                ddlLanguage.DataTextField = "LANG_ID";
                ddlLanguage.DataValueField = "LANG_ID";
                ddlLanguage.DataBind();

                // hide formview if all languages are set
                if (ddlLanguage.Items.Count == 1) 
                    dvSmapLang.Visible = false;
            }

            // default values
            CheckBox cbActive = (CheckBox)dvSmapLang.FindControl("cbActive");
            cbActive.Checked = true;
                

        }
        protected void dvSitemapLang_ItemInserted(object sender, EventArgs e)
        {
            gvSmapLang.DataBind();
            SM.EM.Caching.RemoveMenuCache(Convert.ToInt32(ddlMenu.SelectedValue));
            RefreshCMSSitemapProvider("m", ddlMenu.SelectedValue);
        }

        protected void dvSmapLang_ItemInserting(object sender, DetailsViewInsertEventArgs e)
        {
            if (Page.IsValid) {
                e.Values["LANG_ID"] = ((DropDownList)((DetailsView)sender).FindControl("ddlLanguage")).SelectedValue;

                if (e.Values["SML_ORDER"] == null || e.Values["SML_ORDER"].ToString() == "")
                    e.Values["SML_ORDER"] = 100;
            }
        }
        protected void gvSmapLang_RowDeleted(object sender, GridViewDeletedEventArgs e)
        {
            dvSmapLang.DataBind();
            SM.EM.Caching.RemoveMenuCache(Convert.ToInt32(ddlMenu.SelectedValue));
            RefreshCMSSitemapProvider();
        }
        protected void gvSmapLang_RowUpdated(object sender, GridViewUpdatedEventArgs e)
        {
            SM.EM.Caching.RemoveMenuCache(Convert.ToInt32(ddlMenu.SelectedValue));
            RefreshCMSSitemapProvider();
        }
        protected void gvSiteMap_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadSitemapData(Int32.Parse(gvSiteMap.SelectedDataKey.Value.ToString() ));
            LoadParentTreeview(tvParent);
        }
        protected void ValidateParent(object sender, ServerValidateEventArgs e) {
            if ( tvSitemap.Nodes.Count > 0 && lParent.Text == "")
            {
                e.IsValid = false;
                return;

            }

            e.IsValid = true;
        }
}
}