﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="popupContactFormSettings.ascx.cs" Inherits="SM.EM.UI.Controls.popupContactFormSettings" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="EM" TagName="Settings" Src="~/_ctrl/module/contact-form/cwpContactFormSettings.ascx"  %>

<asp:HiddenField ID="hfPopup" runat="server" />

        
<asp:Panel CssClass="modPopup" ID="panSettings" runat="server" style="display:none;">
    <div class="modPopup_header modPopup_headerSettings">
        <h4>NASTAVITVE ELEMENTA KONTAKTNI OBRAZEC</h4>
        <p>Uredi naziv elementa. Določi podatke e-maila, kamor se pošlje povpraševanje.</p>
    </div>
    <div class="modPopup_main">

        <EM:Settings id="objSettings" runat="server"></EM:Settings>
    </div>    
    <div class="modPopup_footer">
        <div class="buttonwrapperModal">
            <asp:LinkButton ID="btSaveEdit" runat="server" CssClass="lbutton lbuttonConfirm" OnClick="btSaveEdit_Click" ><span>Shrani</span></asp:LinkButton>
            <asp:LinkButton ID="btCancelEdit" runat="server" CssClass="lbutton lbuttonDelete"><span>Prekliči</span></asp:LinkButton>
        </div>
    </div>
</asp:Panel>

           
<cc1:ModalPopupExtender RepositionMode="RepositionOnWindowResizeAndScroll" Y ="50"  BehaviorID="mpeContactFormSettings"  ID="mpeContactFormSettings" runat="server" TargetControlID="hfPopup" PopupControlID="panSettings"
                        BackgroundCssClass="modBcg" CancelControlID="btCancelEdit" >
</cc1:ModalPopupExtender> 

           



