﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;


namespace SM.UI.Sandbox.Controls
{
    public partial class _cmsImageList : BaseControl
    {
        public delegate void OnEditEventHandler(object sender,SM.BLL.Common.ClickIDEventArgs e);
        public event OnEditEventHandler OnEdit;
        public event OnEditEventHandler OnDelete;
        public bool ShowCheckboxColumn { get; set; }
        public bool ShowManageColumn { get; set; }
        public int PageSize = 30;
        public bool MultipleSelect = true;
        public bool SelectMode { get; set; }

        public int SelectedImageID { get; set; }
        public int ImageTypeID { get; set; }
        public int GalleryID { get; set; }
        public string LangID { get; set; }
        public string SearchText { get; set; }
        public bool? Active { get; set; }
        public int ExcludeImageID { get; set; }
        
        public List<int> GetSelectedItems {
            get { 
                List<int> list = new List<int>();
                if (!ShowCheckboxColumn ) return list;

                // get all selected groups
                foreach (ListViewItem item in lvList.Items ){
                    CheckBox cbSelect = (CheckBox)item.FindControl("cbSelect");
                    if (cbSelect == null) return list;

                    HiddenField hfID = (HiddenField)item.FindControl("hfID");
                    // add group to list
                    if (cbSelect.Checked)
                        list.Add(int.Parse (hfID.Value));
                }

                return list;
            }
        }


        
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        protected void ldsList_OnSelecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            eMenikDataContext db = new eMenikDataContext();

            // apply language
            //string lang = "";
            //if (!string.IsNullOrEmpty(LangID))
            //    lang = LangID;
            //else
            //    lang = LANGUAGE.GetDefaultLang();
            //    lang = CULTURE.GetDefaultUserCulture(ParentPage.em_user)
            

            var  list = (from l in db.vw_Images  where l.TYPE_ID == ImageTypeID  select l);


            // apply filters
            if (Active != null)
                list = list.Where(w => w.IMG_ACTIVE  == Active);
            if (GalleryID > 0)
                list = list.Join(db.IMAGE_GALLERies.Where(w => w.GAL_ID  == GalleryID ), g => g.IMG_ID , p => p.IMG_ID , (g, p) => g);
            //if (!String.IsNullOrEmpty(SearchText))
            //    list = list.Where(w => (w.IMG_TITLE.Contains(SearchText) || w.IMG_DESC.Contains(SearchText) ));

            // exclude
            if (ExcludeImageID > 0 )
                list = list.Where(l => l.IMG_ID != ExcludeImageID );


            // result
            e.Result = list.OrderByDescending(o => o.DATE_ADDED);
        }


        protected void lvList_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            if (e.CommandName == "edit")
            {
                SM.BLL.Common.ClickIDEventArgs arg = new SM.BLL.Common.ClickIDEventArgs();
                arg.ID = int.Parse(e.CommandArgument.ToString());
                // raise event
                OnEdit(this, arg);
            }
            
        }

        public void BindData()
        {

            lvList.DataBind();

            // init page size
            DataPager pager = (DataPager)lvList.FindControl("dpPager");
            if (pager != null)
                pager.PageSize = PageSize;
        }


        protected void lvList_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem) return;

            // select current item
            ListViewDataItem dataitem = (ListViewDataItem)e.Item;
            vw_Image img = (vw_Image)dataitem.DataItem;
            HtmlControl divImgThumbItem = (HtmlControl)e.Item.FindControl("divImgThumbItem");
            //if (img.IMG_ID == SelectedImageID)
            //{
            //    divImgThumbItem.Attributes["class"]= "imgThumbItem imgSel";
            //}
            //else
            //    divImgThumbItem.Attributes["class"] = "imgThumbItem";

            if ( MultipleSelect || !ShowCheckboxColumn) return;

            CheckBox cbSelect = (CheckBox)e.Item.FindControl("cbSelect");
            string script = "SetUniqueCheckbox('lvList.*cbSelect',this)";
            cbSelect.Attributes.Add("onclick", script);

        }

    }

}