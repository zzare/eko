﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="cwpGallerySettings.ascx.cs" Inherits="cwpGallerySettings" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="FredCK.FCKeditorV2" Namespace="FredCK.FCKeditorV2" TagPrefix="FCKeditorV2" %>




<span class="modPopup_mainTitle" >Naziv galerije</span>
<div style="float:left;">
    <asp:TextBox ID="tbTitle" class="modPopup_TBXsettings" runat="server"></asp:TextBox>
</div>
<div style="float:left; padding-top:3px;">
    <asp:CheckBox ID="cbShowTitle" CssClass="modPopup_CBXsettings" runat="server" Text = "&nbsp;&nbsp;Prikaži naziv na spletni strani" />
        <cc1:ToggleButtonExtender ID="ToggleEx" runat="server"
        TargetControlID="cbShowTitle" 
        ImageWidth="19" 
        ImageHeight="19"
        CheckedImageAlternateText="Odkljukaj"
        UncheckedImageAlternateText="Obkljukaj"
        UncheckedImageUrl="~/_inc/images/design/checkbox_off.gif" 
        CheckedImageUrl="~/_inc/images/design/checkbox_on.gif"
        DisabledCheckedImageUrl="~/_inc/images/design/checkbox_disabled.gif"
        DisabledUncheckedImageUrl="~/_inc/images/design/checkbox_disabled.gif" />

</div>
<br />
<div class="clearing"></div>        
<span class="modPopup_mainTitle" >Število prikazanih slik na strani (ostale slike so vidne v galeriji)</span>
<asp:TextBox ID="tbPageSize" class="modPopup_TBXsettings" Width="20px" runat="server"></asp:TextBox> <i>* Ostale so vidne v galeriji ko le-to odprete s klikom na eno od slik.</i>
<cc1:FilteredTextBoxExtender runat="server"  FilterType="Numbers" TargetControlID="tbPageSize"></cc1:FilteredTextBoxExtender>

<br />
<div class="clearing"></div>        
<span class="modPopup_mainTitle" >Velikost majhne slike v seznamu (thumbnail)</span>

<asp:ListView ID="lvType" runat="server">
    <LayoutTemplate>
        <asp:PlaceHolder ID="itemPlaceholder" runat="server"/>
    </LayoutTemplate>

    <ItemTemplate>
        <label>
            <input  <%# RenderChecked(Container.DataItem) %> type="radio" name="galsettings" value='<%# Eval("TYPE_ID") %>' />
            <span ><%# Eval("DESC") %></span>
            <div class="clearing"></div>
        </label>
        
    </ItemTemplate>
</asp:ListView>



<br />




