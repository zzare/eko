﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SM.EM.BLL;
using SM.UI.Controls;
namespace SM.EM.UI.Controls
{
    public partial class _ctrl_module_content_cwpContent : BaseControl_CmsWebPart              
    {



        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            // init content
            if (cwp != null)
                objContent.CwpID = cwp.CWP_ID;
            objContent.AutoLoad = false;


        }

        public override void LoadData()
        {
            base.LoadData();


            // init header
            InitEditMode();

            // load data
            LoadContent();
        }


        protected void InitEditMode() {


            // edit mode
            if (ParentPage.IsModeCMS )
            {
                divEdit.Visible = true;
                objContent.HideInactive = false;
                //panEdit.Visible = true;


               // button actions
                //if (Page.User.IsInRole("admin") || Page.User.IsInRole("em_admin"))
                //{
                //    btEdit.Attributes["onclick"] = "editContentLoad('" + cwp.CWP_ID.ToString() + "'); return false";
                //    btEdit.HRef = "#";
                //    btEdit.Attributes["onserverclick"] = "";
                    
                //}

                btShow.HRef = "javascript:onToggleCwpActive('" + btShow.ClientID + "','" + btHide.ClientID + "', '" + cwp.CWP_ID.ToString() + "' ,true);";
                btHide.HRef = "javascript:onToggleCwpActive('" + btShow.ClientID + "','" + btHide.ClientID + "','" + cwp.CWP_ID.ToString() + "', false);";

                // initial hiding of buttons
                if (cwp.ACTIVE)
                {
                    btShow.Attributes["style"] = "display:none";
                    btHide.Attributes["style"] = "display:";
                }
                else
                {
                    btShow.Attributes["style"] = "display:";
                    btHide.Attributes["style"] = "display:none";
                }

            }
            else
            {
                divEdit.Visible = false;
                objContent.HideInactive = true;
                //panEdit.Visible = false;
            }


            // title
            litTitle.Text = cwp.TITLE;
            divTitleContent.Visible = cwp.SHOW_TITLE;

        }


        protected void LoadContent() {

            objContent.CwpID = cwp.CWP_ID;
            objContent.AutoLoad = false;

            // load content
            objContent.BindData();
            //fckBody.Value = Server.HtmlDecode(objContent.ContentBody);
        
        }


        public void OnEdit(object sender, EventArgs  e)
        {
 
            CwpEdit edit = ParentPage.EditContent;
            if (edit == null)
                return;

            edit.cwp = cwp;
            edit.LoadData();
        }



        //protected void btDelete_Click(object sender, EventArgs e) {
        //    if (!ParentPage.CanSave) return;

        //    cwp.Delete();    
        
        //    // reload zone
        //    ParentPage.ReLoadZone(ParentPage.Zones[cwp.ZONE_ID], true);
        //}

        protected void btSettings_Click(object sender, EventArgs e)
        {
            CwpEdit edit = ParentPage.ContentSettings ;
            if (edit == null) return;

            edit.cwp = cwp;
            edit.CwpID = cwp.CWP_ID;
            edit.LoadData();
            edit.Zone = cwp.ZONE_ID;
        }
    }
}