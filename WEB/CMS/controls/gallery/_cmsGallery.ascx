﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_cmsGallery.ascx.cs" Inherits="SM.EM.UI.CMS.Controls.CMS_cmsGallery" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="CMS" TagName="ImageNew" Src="~/cms/controls/gallery/_cmsImageNew.ascx"  %>
<%@ Register TagPrefix="CMS" TagName="ImageEdit" Src="~/cms/controls/gallery/_cmsImageEdit.ascx"  %>
<%@ Register TagPrefix="CMS" TagName="ImageList" Src="~/cms/controls/gallery/_cmsImageList.ascx"  %>
<%@ Register TagPrefix="CMS" TagName="GalleryEdit" Src="~/cms/controls/gallery/_cmsGalleryEdit.ascx"  %>

<asp:LinkButton ID="btNewImage" runat="server" OnClick="btNewImage_OnClick">Add NEW image(s)</asp:LinkButton>
<asp:LinkButton  ID="btEditGallery" runat="server" OnClick="btEditGallery_OnClick"  Text="EDIT gallery" />


<%--<asp:UpdatePanel ID="upImageNew" runat="server" UpdateMode="Conditional">
    <ContentTemplate>--%>
        <CMS:ImageNew ID="objImageNew" runat="server" Visible="false" />
        
        <asp:LinkButton Visible="false" ValidationGroup="file" ID="btUpload" runat="server" OnClick="btUpload_Click"  Text="Add image(s)" />

        <asp:LinkButton Visible="false" CausesValidation="false" ID="btCancelNew" runat="server" OnClick="btCancelNew_Click"  Text="Cancel" />        
 <%--   </ContentTemplate>
    
    <Triggers>
        <asp:PostBackTrigger ControlID="btUpload" />
    </Triggers>
</asp:UpdatePanel>--%>
        
<CMS:ImageEdit ID="objImageEdit" runat="server" Visible="false" />
<asp:LinkButton ID="btSaveImage" Visible="false" runat="server" onclick="btSaveImage_Click">save image</asp:LinkButton>
<asp:LinkButton ID="btCancelImage" Visible="false" runat="server" onclick="btCancelImage_Click">cancel</asp:LinkButton>

<CMS:GalleryEdit ID="objGalleryEdit" runat="server" Visible="false" />
<asp:LinkButton ID="btSaveGallery" Visible="false" runat="server" onclick="btSaveGallery_Click">save gallery</asp:LinkButton>
<asp:LinkButton ID="btCancelGallery" Visible="false" runat="server" onclick="btCancelGallery_Click">cancel</asp:LinkButton>


<CMS:ImageList ID="objImageList" runat="server" ShowCheckboxColumn="true"  />
<asp:LinkButton ID="btDelete" runat="server" onclick="btDelete_OnClick">delete selected</asp:LinkButton>
    
<asp:Literal ID="litStatus" runat="server" EnableViewState="false"></asp:Literal>

<%-- dummy file upload for having another one in sub user control for first time postback file upload--%>
<asp:FileUpload ID="dummyFU" runat="server" style="visibility:hidden;" />
