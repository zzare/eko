﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _ctrl_module_poll_viewPollResult : BaseControlView
{

    public object Data;
    public object[] Parameters;
    protected List<POLL_CHOICE> PollList;
    protected int TotalVotes;
    protected string LangID;
    protected Guid PollID;




    protected void Page_Load(object sender, EventArgs e)
    {
        LoadPoll();
    }



    protected void LoadPoll() {

        if (Data == null)
            return;

        // data
        PollList = ((IEnumerable <POLL_CHOICE >)Data).ToList();
        TotalVotes = (from t in PollList select t.VoteCountFemale + t.VoteCountMale).Sum();

        if (Parameters != null)
        {
            if (Parameters[0] != null)
                PollID = new Guid (Parameters[0].ToString());
            if (Parameters.Length > 1 && Parameters[1] != null)
                LangID = Parameters[1].ToString();
        }


        // load poll choices
        lvList.DataSource = PollList;
        lvList.DataBind();
    }

    protected decimal GetPercent(POLL_CHOICE pc, string type)
    {
        decimal percent = 10;

        if (TotalVotes == 0)
            return 0;

        if (type == "F")
            percent = pc.VoteCountFemale;
        else if (type == "M")
            percent = pc.VoteCountMale;
        else
            percent = pc.VoteCountMale + pc.VoteCountFemale;

        percent = (percent * 100) / TotalVotes;

        
        return percent;
    }

    protected string RenderWidth( POLL_CHOICE pc, string type ) {
        string ret = "";
        decimal percent = GetPercent(pc, type);
        //if (percent == 0)
        //    percent = 1;


        if (percent > 100)
            percent = 100;

        ret = "width:" + Math.Round(percent).ToString() + "%;";

        return ret;

    }



}
