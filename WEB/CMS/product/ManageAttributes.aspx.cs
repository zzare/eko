﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace SM.UI.CMS.EM
{
    public partial class CMS_ManageProductAttributes : SM.EM.UI.CMS.BasePage_CMS 
    {

        protected override void OnInit(EventArgs e)
        {
            CurrentSitemapID = 1426;
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void ldsList_OnSelecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            eMenikDataContext db = new eMenikDataContext();

            var list = from l in db.GROUP_ATTRIBUTEs  orderby l.PageId,  l.Title   ascending select l;


            e.Result = list;
        }


        protected void ldsList_Inserting(object sender, LinqDataSourceInsertEventArgs e)
        {
            GROUP_ATTRIBUTE c = (GROUP_ATTRIBUTE)e.NewObject;

            if (string.IsNullOrEmpty(((TextBox)lvList.InsertItem.FindControl("tbPageID")).Text))
                c.PageId = null;

            c.GroupAttID = Guid.NewGuid();



            // set logo and header type
            //MASTER  c = (MASTER)e.NewObject;
            //c.IMG_TYPE_ID = Int32.Parse(((DropDownList)lvList.InsertItem.FindControl("ddlLogo")).SelectedValue);
            //int imgHeader = Int32.Parse(((DropDownList)lvList.InsertItem.FindControl("ddlHeader")).SelectedValue);
            //if (imgHeader > 0)
            //{
            //    c.HDR_TYPE_ID = imgHeader;
            //    c.HAS_HEADER_IMAGE = true;
            //}
            //else
            //    c.HAS_HEADER_IMAGE = false;

        }
        protected void lvList_ItemEditing(object sender, ListViewEditEventArgs e)
        {
            // hide insert item
            lvList.InsertItemPosition = InsertItemPosition.None;


       }
        protected void lvList_ItemUpdated(object sender, ListViewUpdatedEventArgs e)
        {
            // show insert item
            lvList.InsertItemPosition = InsertItemPosition.LastItem;

        }
        protected void lvList_ItemCanceling(object sender, ListViewCancelEventArgs e)
        {
            // show insert item
            lvList.InsertItemPosition = InsertItemPosition.LastItem;

        }


        protected void ldsList_Updating(object sender, LinqDataSourceUpdateEventArgs e)
        {
            GROUP_ATTRIBUTE c = (GROUP_ATTRIBUTE)e.NewObject;

            TextBox tb = (TextBox)lvList.Items[lvList.EditIndex].FindControl("tbPageID");

            if (string.IsNullOrEmpty(( tb.Text ) ))
                c.PageId = null;

            
            
            // set header type
            

            //MASTER c = (MASTER)e.NewObject;
            //int imgHeader = Int32.Parse(((DropDownList)lvList.Items[lvList.EditIndex].FindControl("ddlHeader")).SelectedValue);
            //if (imgHeader > 0)
            //{
            //    c.HDR_TYPE_ID = imgHeader;
            //    c.HAS_HEADER_IMAGE = true;
            //}
            //else
            //    c.HAS_HEADER_IMAGE = false;

            // clear cache
            SM.EM.BLL.BizObject.PurgeCacheItems(SM.EM.Caching.Key.ProductAttribute());

        }
        protected void lvList_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            ListViewDataItem dataItem = (ListViewDataItem)e.Item;
            if (lvList.EditItem != null)
            {
                if (dataItem.DisplayIndex == lvList.EditIndex)
                {

                    GROUP_ATTRIBUTE item = (GROUP_ATTRIBUTE)dataItem.DataItem;

                    ProductRep prep = new ProductRep();
                    ListView listAttr = (ListView)e.Item.FindControl("lvListAttr");
                    RefreshListAttr(listAttr, item.GroupAttID);


                }

            }

        }



        protected void lvListAttr_ItemEditing(object sender, ListViewEditEventArgs e)
        {
            // hide insert item
            ListView lv = (ListView)sender;
            lv.InsertItemPosition = InsertItemPosition.None;

            HiddenField hfID = (HiddenField)((ListView)sender).Parent.FindControl("hfID");


            lv.EditIndex = e.NewEditIndex;
            RefreshListAttr(lv, new Guid(hfID.Value));
            //lv.DataBind();

        }
        protected void lvListAttr_ItemUpdating(object sender, ListViewUpdateEventArgs  e)
        {
            // show insert item
            ListView lv = (ListView)sender;

            HiddenField hfID = (HiddenField)((ListView)sender).Parent.FindControl("hfID");
            ProductRep prep = new ProductRep();

            string attrID = ((HiddenField)lv.EditItem.FindControl("hfID")).Value;

            ATTRIBUTE att = prep.GetAttributeByID(new Guid(attrID));
            att.Title = ((TextBox)lv.EditItem.FindControl("tbTitle")).Text;
            att.Desc = ((TextBox)lv.EditItem.FindControl("tbDesc")).Text;
            att.Code = ((TextBox)lv.EditItem.FindControl("tbCode")).Text;
            att.UrlSmall = ((TextBox)lv.EditItem.FindControl("tbUrlSmall")).Text;
            att.UrlBig = ((TextBox)lv.EditItem.FindControl("tbUrlBig")).Text;
            int ordr = -1;
            int.TryParse(((TextBox)lv.EditItem.FindControl("tbOrdr")).Text, out ordr);
            att.Ordr = ordr;

            prep.Save();

            // find ID
            ListView listAttr = (ListView)sender;
            listAttr.EditIndex = -1;
            listAttr.InsertItemPosition = InsertItemPosition.LastItem;
            RefreshListAttr(listAttr, new Guid(hfID.Value));



        }
        protected void lvListAttr_ItemCanceling(object sender, ListViewCancelEventArgs e)
        {
            // show insert item
            ListView lv = (ListView)sender;
            lv.InsertItemPosition = InsertItemPosition.LastItem;

        }
        protected void lvListAttr_ItemInserting(object sender, ListViewInsertEventArgs  e)
        {
            // find ID
            HiddenField hfID = (HiddenField )((ListView)sender).Parent.FindControl("hfID");

            

            ProductRep prep = new ProductRep();

            ATTRIBUTE att = new ATTRIBUTE();
            att.GroupAttID = new Guid( hfID.Value);
            att.AttributeID = Guid.NewGuid();
            att.Title = ((TextBox ) e.Item.FindControl("tbTitle")).Text ;
            att.Desc = ((TextBox)e.Item.FindControl("tbDesc")).Text;
            att.Code = ((TextBox)e.Item.FindControl("tbCode")).Text;
            att.UrlSmall = ((TextBox)e.Item.FindControl("tbUrlSmall")).Text;
            att.UrlBig = ((TextBox)e.Item.FindControl("tbUrlBig")).Text;
            int ordr = 0;
            int.TryParse(((TextBox ) e.Item.FindControl("tbOrdr")).Text , out ordr);
            att.Ordr = ordr;

            prep.AddAttribute(att);
            prep.Save();

            // find ID
            ListView listAttr = (ListView)sender;
            RefreshListAttr(listAttr, new Guid(hfID.Value));


            listAttr.InsertItemPosition = InsertItemPosition.LastItem;

        }



        protected void lvListAttr_ItemDeleting(object sender, ListViewDeleteEventArgs e)
        {
            // find ID
            HiddenField hfID = (HiddenField)((ListView)sender).Parent.FindControl("hfID");

            ProductRep prep = new ProductRep();
            ListView listAttr = (ListView)sender;
            ATTRIBUTE att = prep.GetAttributeByID(new Guid(listAttr.DataKeys[e.ItemIndex].Value.ToString()));
            prep.DeleteAttr(att);
            prep.Save();

            RefreshListAttr(listAttr, new Guid(hfID.Value ));

        }

        protected void RefreshListAttr(ListView lv, Guid id) {
            ProductRep prep = new ProductRep();
            lv.DataSource = prep.GetAttributes(id);
            lv.DataBind();
        
        }






}
}
