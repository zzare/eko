
--begin transaction
update SITEMAP set PARENT = 1470, MENU_ID = 5 where SMAP_ID = 1482
update SITEMAP_LANG set SML_TITLE = 'KLEPETALNICA', SML_DESC = 'KLEPETALNICA'  where SMAP_ID = 1482 
--commit transaction
--rollback transaction


-- desc
-- update SITEMAP_LANG set SML_META_DESC = N'V spletni trgovini Terra-Gourmet so zbrani izdelki, ki so zelo zdravi, kakovostni in <br />jih je prakti�no nemogo�e kupiti kje drugje.'  where SMAP_ID = 1473 
update SITEMAP_LANG set SML_META_DESC = N'V spletni trgovini Terra-Gourmet so zbrani zelo zdravi, kakovostni in buti�ni izdelki, ki jih je zelo te�ko najti.'  where SMAP_ID = 1473 
update SITEMAP_LANG set SML_META_DESC = N'V neokrnjenem okolju pa�nikov hriba Korada, kjer oko rado po�ije na odprtem razgledu Gori�kih brd, Jadranskega morja, Alp, Furlanije, So�ke doline in predalpskega sveta, veselo muka govedo francoske pasme Limousine. Najprej �talca, potlej kravca? Ne, ne, pozabimo na �talco. <br /><br />Limuzinke, kot jim pravimo bli�nji, skozi celo leto pasemo izklju�no na prostem, v naravi; pozimi jih pred mrazom varuje ljubezen in njihova gosta dlaka. Celoletna prosta pa�a na ekoklimi mediteransko-alpske idile in stalno gibanje goveda se brez pardona ka�eta tudi v (�e tako opevani) kakovosti mesa te pasme � meso poleg vrhunskega okusa odlikuje tudi izredno nizka vsebnost ma��ob.'  where SMAP_ID = 1475 
update SITEMAP_LANG set SML_TITLE = N'OLJ�NA OLJA', SML_DESC = N'OLJ�NA OLJA', SML_META_DESC = N'Izbor olj�nih olj, ki ga nudi Terra-Gourmet, se pona�a z vrhunsko kvaliteto primorskih olj�nih olj, ki se po mnenju poznavalcev uvr��ajo v sam svetovni vrh.'  where SMAP_ID = 1485
update SITEMAP_LANG set SML_META_DESC = N'Izbor vrhunskih vin iz osr�ja Gori�kih Brd.'  where SMAP_ID = 1476 
update SITEMAP_LANG set SML_META_DESC = N'Prvovrstni siri iz neokrnjene narave, pripravljeni na �isto poseben na�in.'  where SMAP_ID = 1486 
update SITEMAP_LANG set SML_TITLE =N'BUTI�N!ZBOR',  SML_DESC = N'BUTI�N!ZBOR', SML_META_DESC = N'Edinstveni, nepogre�ljivi izdelki, ki bodo ne�no zaokro�ili okuse vsake izbrane pojedine.'  where SMAP_ID = 1487 
update SITEMAP_LANG set SML_TITLE = 'KLEPETALNICA',  SML_DESC = 'KLEPETALNICA', SML_META_DESC = N'V klepetalnici se izmenjujejo mnenja, recepti in vtisi �lanov Terra-Gourmet. <br />Klepetalnica je zaprtega tipa in dostopna le �lanom spletne de�ele. Vljudno vas vabimo k sodelovanju. Va�im zapisom lahko dodate tudi fotografije, �esar bomo zelo veseli.'  where SMAP_ID = 1482
update SITEMAP_LANG set SML_TITLE = 'Mediji',  SML_DESC = 'Mediji', SML_META_DESC = N'V klepetalnici se izmenjujejo mnenja, recepti in vtisi �lanov Terra-Gourmet. <br />Klepetalnica je zaprtega tipa in dostopna le �lanom spletne de�ele. Vljudno vas vabimo k sodelovanju. Va�im zapisom lahko dodate tudi fotografije, �esar bomo zelo veseli.'  where SMAP_ID = 1484
update SITEMAP_LANG set  SML_META_DESC = N'V klepetalnici se izmenjujejo mnenja, recepti in vtisi �lanov Terra-Gourmet. <br />Klepetalnica je zaprtega tipa in dostopna le �lanom spletne de�ele. Vljudno vas vabimo k sodelovanju. Va�im zapisom lahko dodate tudi fotografije, �esar bomo zelo veseli.'  where SMAP_ID = 1483



--update SITEMAP_LANG set SML_DESC = CAST( 'OLJ�NA OLJA' as nvarchar(4000)), SML_META_DESC = 'Izbor olj�nih olj, ki ga nudi Terra-Gourmet, se pona�a s svojim severnim, gori�kim poreklom. Zaradi vi�inske razlike imajo �isto poseben in izrazit okus, ki se mo�no razlikuje od tistih olj�nih olj, ki jih iztisnejo obmorski Primorci.'  where SMAP_ID = 1485
--update SITEMAP_LANG set SML_DESC = 'OLJ�NA OLJA', SML_META_DESC = 'Izbor olj�nih olj, ki ga nudi Terra-Gourmet, se pona�a s svojim severnim, gori�kim poreklom. Zaradi vi�inske razlike imajo �isto poseben in izrazit okus, ki se mo�no razlikuje od tistih olj�nih olj, ki jih iztisnejo obmorski Primorci.'  where SMAP_ID = 1485


-- additional PMODS
--insert into MASTER_PAGEM values(2, 51, 'Product List dodatno')
-- set visible TERRA-GOURMET, set PAGEM



-- PRODUCT
-- order
update PRODUCT set IMPORTANCE = -10 where id_int = 222

update PRODUCT set IMPORTANCE = -10 where id_int = 175
update PRODUCT set IMPORTANCE = -10 where id_int = 181
update PRODUCT set IMPORTANCE = -10 where id_int = 174

update PRODUCT_DESC set DESCRIPTION_SHORT = '<p>  Terra Gourmet vam nudi prvovrstno olj�no olje Gori&scaron;kih brd. Zaradi zna�ilne lege pokrajine in nadmorske vi&scaron;ine 400 metrov, ima olj�no olje Gori&scaron;kih brd specifi�en, intenziven okus in izrazito barvo. Trenutno imamo na zalogi le 200 l tega izjemnega olj�nega olja, ki je prava redkost.</p> '  where PRODUCT_ID  = 'B91C4AFA-FC4F-4D41-B8FE-B9B6592DDD86' and LANG_ID = 'si'



-- BLOG
update ARTICLE set ART_BODY = '&lt;p&gt;&lt;a target=&quot;_blank&quot; href=&quot;http://youtu.be/8cN6rh8jj-8&quot;&gt;Predstavitev limuzink 1/4&lt;/a&gt;&lt;/p&gt; &lt;p&gt;&lt;a target=&quot;_blank&quot; href=&quot;http://youtu.be/wrl6SICvsm8&quot;&gt;T-bone na �aru 2/4&lt;/a&gt;&lt;/p&gt; &lt;p&gt;&lt;a target=&quot;_blank&quot; href=&quot;http://youtu.be/lrwbnbImEkw&quot;&gt;Tajski satay 3/4&lt;/a&gt;&lt;/p&gt; &lt;p&gt;&lt;a target=&quot;_blank&quot; href=&quot;http://youtu.be/iys4dMtaSkA&quot;&gt;Carpaccio 4/4&lt;/a&gt;&lt;/p&gt;' where art_id = 12
update ARTICLE set ART_ABSTRACT = 'Novinarka in fotograf sta pre�ivela dan na Koradi in strnila vtise v �lanku, ki je bil objavljen v Nedelu, vi pa si ga lahko preberete tu. <br/> <a href="/userfiles/documents/20_1218_NED.pdf">1. del (.pdf)</a> <br/> <a href="/userfiles/documents/21_1218_NED.pdf">2. del (.pdf)</a>' where art_id = 13
-- set paging to 10, enable paging