﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SM.UI.Controls;

public partial class _em_content__ctrl_pmod_comb_3_1_3_1_ : BaseControl 
{
    protected void Page_Load(object sender, EventArgs e)
    {


        mn_1_Left.CurrentWebPartZone = "wpz_1";
        mn_1_Main.CurrentWebPartZone = "wpz_2";
        mn_1_Right.CurrentWebPartZone = "wpz_3";

        mn_2_Main.CurrentWebPartZone = "wpz_4";

        mn_3_Left.CurrentWebPartZone = "wpz_5";
        mn_3_Main.CurrentWebPartZone = "wpz_6";
        mn_3_Right.CurrentWebPartZone = "wpz_7";
        mn_4_Main.CurrentWebPartZone = "wpz_8";
        
    }
}
