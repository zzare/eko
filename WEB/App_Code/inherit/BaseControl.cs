﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for BaseControl
/// </summary>
public abstract class BaseControl : System.Web.UI.UserControl
{
    public int CurrentSitemapID = -1;
    public SM.EM.UI.BasePage ParentPage;
    protected override void OnInit(System.EventArgs e)
    {
        if (Page is SM.EM.UI.BasePage)
            ParentPage = (SM.EM.UI.BasePage)this.Page;

#if DEBUG
#endif

        base.OnInit(e);
    }

    protected override void OnLoad(EventArgs e)
    {
        if (ParentPage != null)
            CurrentSitemapID = ParentPage.CurrentSitemapID;
        
        base.OnLoad(e);
    }

}
