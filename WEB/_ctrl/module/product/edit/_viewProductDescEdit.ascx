﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewProductDescEdit.ascx.cs" Inherits="_ctrl_module_product_edit_viewProductDescEdit" %>


<script language = "javascript" type="text/javascript">
    function editProductDescSave(id) {

        var _items = new Array();
        $("#prodDescTabs div.item").each(function (i, val) {
            var _lan = '' + $(this).find('input.hfLang:hidden').val();
            var _title = '' + $(this).find('input:text').val();
            //            var _desc = '' + $(this).find('textarea').val();
            var _desc = '' + $(this).find('textarea.longD').val();
            var _short = '' + $(this).find('textarea.shortD').val();
            var _item = new Object();
            _item.Lang = _lan;
            _item.Title = _title;

            if (_desc == 'undefined')
                _desc = null;
            _item.Desc = _desc;
            _item.Abstract = _short;
            Array.add(_items, _item);
            //alert(_lan + ' , ' +_desc);
        });


        openModalLoading('Shranjujem...');
        WebServiceCWP.SaveProductDesc(id, em_g_lang, _items, 1, onProductEditSuccessSave, onProductSaveError, id);

    } 
</script>


<div id="cEditProduct" >
    <div class="modPopup_header modPopup_headerEdit">
        <h4>UREDI OPIS</h4>
        <p>Uredi opise izdelka za različne jezikovne različice</p>
    </div>
    <div class="modPopup_main">
    
        <div id="prodDescTabs">
            <ul>
                <asp:ListView ID="lvListHeader" runat="server"  InsertItemPosition="None" >
                    <LayoutTemplate>
                        <asp:PlaceHolder id="itemPlaceholder" runat="server"></asp:PlaceHolder>
                    </LayoutTemplate>
                    
                    <ItemTemplate>
                         <li><a href="#tabs-<%# Container.DataItemIndex %>"><%# Eval("DESC")  %></a></li>
                    </ItemTemplate>
                </asp:ListView>	 	        
	        </ul>  
    	    
    	    
                <asp:ListView ID="lvListDetail" runat="server"  InsertItemPosition="None" >
                    <LayoutTemplate>
                        <asp:PlaceHolder id="itemPlaceholder" runat="server"></asp:PlaceHolder>
                    </LayoutTemplate>
                    
                    <ItemTemplate>
                        <div id="tabs-<%# Container.DataItemIndex %>" class="item" >
                            <input class="hfLang"  type="hidden" value= '<%#  Eval("LANG_ID")%>' />
                               
                            <span style="font-size:14px;">Naziv izdelka (* obvezno)</span>
                            <br />
                            <input style="width:95%" class="modPopup_TBXsettings "  id="tbTitle" maxlength="256" type="text" value='<%#  Eval("NAME")%>' />
                            <br />
                            <span style="font-size:14px;">Kratek opis izdelka</span> 
                            <br />
                            <textarea class="shortD" style="width:95%"  rows="6" cols="50"   ><%# Eval("DESCRIPTION_SHORT")%></textarea>
                            <br />
                            <%if (ShowLongDesc) {%>
                            <span style="font-size:14px;">Dolg podroben opis izdelka</span> 
                            <br />
                            <textarea class="longD" style="width:95%"  rows="12" cols="50"   ><%# Eval("DESCRIPTION_LONG")%></textarea>
                            <%} %>
                        </div>
                    </ItemTemplate>
                </asp:ListView>	    


        </div>

      <%-- <a href="#" onclick="initHtmlArea(); return false">init</a>--%>


    </div>   

    <div class="modPopup_footer"> 
        <div class="buttonwrapperModal">
            <a class="lbutton lbuttonConfirm" href="#" onclick="editProductDescSave('<%= ProductID %>'); return false"><span>Shrani</span></a>
            <a class="lbutton lbuttonDelete" href="#" onclick="closeModalLoader(); return false" ><span>Prekliči</span></a>
        </div>
    </div>
</div>