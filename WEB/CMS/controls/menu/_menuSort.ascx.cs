﻿using System;
using System.Collections;
using System.Collections.Generic ;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


namespace SM.UI.CMS.Controls
{
    public partial class _menuSort : BaseControl
    {
        public event EventHandler OnSortChanged;
        public bool ShowSaveButton { get; set; }

        public int ParentID
        {
            get
            {
                int res = -1;
                if (ViewState["PARENT"] == null) return res;
                if (!int.TryParse(ViewState["PARENT"].ToString(), out res)) return res;

                return res;
            }
            set
            {
                ViewState["PARENT"] = value;
            }
        }
        public string  Culture { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            // data binding
            rlSiteMap.DataSource = GetItems();
            rlSiteMap.DataBind();

            if (!IsPostBack)
            {
                // show/hide save button
                btSaveSort.Visible = ShowSaveButton;
                //                BindData();
            }

        }

        public void BindData()
        {

            ClearItems();

            // load items
            rlSiteMap .DataSource = GetItems();
            rlSiteMap.DataBind();

        }

        public void ClearItems()
        {
            HttpContext.Current.Session.Remove("smapItemsSort");
        }

        //Loads the items to the Session Scope
        public void LoadItems()
        {
            HttpContext.Current.Session["smapItemsSort"] = GetItems();
        }
        //Returns the Items - either from database or from Session scope (if its there!)

        public List<em_GetSitemapNavByParentByLangWWWResult> GetItems()
        {
            if (HttpContext.Current.Session["smapItemsSort"] == null)
            {

                List<em_GetSitemapNavByParentByLangWWWResult> list = SITEMAP.GetSiteMapNavByParentByLangWWWByCulture(ParentID, Culture).ToList();
                return list;
            }
            return (List<em_GetSitemapNavByParentByLangWWWResult>)HttpContext.Current.Session["smapItemsSort"];
        }
        //Save the wItems to the Session Scope

        public void SaveItems(List<em_GetSitemapNavByParentByLangWWWResult> items)
        {
            HttpContext.Current.Session["smapItemsSort"] = items;
        }



        protected void rlSiteMap_ItemReorder(object sender, AjaxControlToolkit.ReorderListItemReorderEventArgs e)
        {

            SaveItems((List<em_GetSitemapNavByParentByLangWWWResult>)rlSiteMap.DataSource);
            //OnSortChanged(this, new EventArgs());

        }
        protected void btSaveSort_Click(object sender, EventArgs e)
        {
            SaveSort();
        }
        public void SaveSort()
        {

            eMenikDataContext db = new eMenikDataContext();

            // update all questions with new order
            int i = 100;
            foreach (em_GetSitemapNavByParentByLangWWWResult item in GetItems())
            {
                db.cms_SitemapUpdateSort(item.SMAP_ID, i); 
                i++;
            }
            db.SubmitChanges();

            // clear Session
            ClearItems();

            // raise event
            OnSortChanged(this, new EventArgs());

        }

    }
}
