﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewShoppingCart.ascx.cs" Inherits="_ctrl_module_product_cart_viewShoppingCart" %>


 

<asp:ListView ID="lvList" runat="server" >
    <LayoutTemplate>
        <span class="ci_maskSpan"><span class="ci_separatorS"></span></span>
        <asp:PlaceHolder ID="itemPlaceholder" runat="server"/>
        <span class="ci_separatorS"></span>
        <div class="cartBottomW clearfix" runat="server">
            <div class="cartBottomL" style='<%= IsPopup ? "" : "display:none" %>'  >
            </div>
            <% if (ShowPriceWithShipping){%>
                <div class="cPriceWithShipping">
                    <div class="cartBottomR">
                        <span class="ci_pricesumaS"><%= GetGlobalResourceObject("Product", "shipping")%>: <%= SM.EM.Helpers.FormatPrice(GetShipping)%></span>
                    </div>
                    <div class="clearing"></div>
                </div>
            <%} %>

            <div class="cartBottomR">
                <span class="ci_pricesumaS cPriceTotalSum "><%= GetGlobalResourceObject("Product", "priceFinal")%>: <%= SM.EM.Helpers.FormatPrice (GetTotalSum) %></span>
                <span class="ci_pricesumaS cPriceTotalSumWithShipping"><%= GetGlobalResourceObject("Product", "priceFinal")%>: <%= SM.EM.Helpers.FormatPrice(GetTotalSumWithShipping)%></span>
                <% if (ShowTax){%>(<span class="ci_priceS"><%= SM.EM.Helpers.FormatPrice(GetTotalTax)%></span>) <%} %>
            </div>
            <div class="clearing"></div>
            <% if (LANGUAGE.GetCurrentLang() == "si"){%>
            <div class="cartBottomB_btn" style='<%= IsPopup ? "" : "display:none" %>'  >
                <a class="AcarthovFinish" href='<%= SM.BLL.Common.ResolveUrl( SM.EM.Rewrite.OrderProductUrl(PageID,Guid.Empty, 1)) %>'><span><%= GetGlobalResourceObject("Product", "checkOut")%></span></a>
            </div>
            <%} %> 
        </div>
        
    </LayoutTemplate>
    
    <ItemTemplate>
    
    
    
        <div class="ci<%# Eval("CartItemID")  %> cartItemW clearfix">    
            <div class="cartItemL">
                <a style="display:block;" href='<%# SM.BLL.Common.ResolveUrl(SM.EM.Rewrite.EmenikUserProductDetailsUrl(   new Guid(Eval("PageId").ToString()), new Guid(Eval("ProductID").ToString()), Eval("ProductTitle").ToString() ))  %>'  title='<%# SM.EM.Helpers.StripHTML( Eval("ProductTitle").ToString())  %>' >
                    <img src='<%# SM.BLL.Common.ResolveUrl ( Eval("ProductUrl").ToString()) %>' alt='<%# SM.EM.Helpers.StripHTML( Eval("ProductTitle").ToString())  %>' />
                </a>
            </div>
            <div class="cartItemR clearfix">
                <div class="cartItemRT">
                    <span class="ci_titleS">
                        <a href='<%# SM.BLL.Common.ResolveUrl(SM.EM.Rewrite.EmenikUserProductDetailsUrl(   new Guid(Eval("PageId").ToString()), new Guid(Eval("ProductID").ToString()), Eval("ProductTitle").ToString() ))  %>'  title='<%# SM.EM.Helpers.StripHTML( Eval("ProductTitle").ToString())  %>' >
                            <%# SM.EM.Helpers.StripHTML( Eval("ProductTitle").ToString())  %>
                            <%# RenderProductAttributesPrice(Container.DataItem) %>
                        </a>
                    </span>
                     
                    

                    <%# RenderProductAttributes(Container.DataItem) %>
                    
                    
                </div>
                
                <div class="cartItemRL"  >
                    <div style='<%= ShowEditDelete ? "" : "display:none" %>' >
                        <a href="#" class="ci_Adelete" onclick="deleteCartItem('<%# Eval("CartItemID")  %>'); return false" ><%= GetGlobalResourceObject("Product", "removeCartItem")%></a>&nbsp;
                        <a href="#" class="ci_Aedit" onclick="editCartItem('<%# Eval("CartItemID")  %>'); return false" ><%= GetGlobalResourceObject("Product", "editCartItem")%></a>&nbsp;&nbsp;&nbsp;
                    </div>
                    
                </div>
                <div class="cartItemRR">
                    <input class="hfCartItem" type="hidden" value='<%# Eval("CartItemID")  %>' />
                    <span class="ci_quantityS"><%# Eval("Quantity") %>x</span>
                    <a style='<%= ShowEditDelete ? "" : "display:none" %>' class="btn_ci_qu_up" href="#" onclick="changeQuantity(1, this);return false;"></a>
                    <a style='<%= ShowEditDelete ? "" : "display:none" %>' class="btn_ci_qu_down" href="#" onclick="changeQuantity(-1, this);return false;"></a>
                    <span class="ci_priceS"><%# SM.EM.Helpers.FormatPrice (Eval("PriceTotal"))%></span>
                    <% if (ShowTax){%>(<span class="ci_priceS"><%# SM.EM.Helpers.FormatPriceEdit (Eval("Tax"))%></span>) <%} %>
                    <div class="clearing"></div>
                </div>
            </div>
            
        </div>
        <span class="ci_separatorS"></span>
    </ItemTemplate>
    
</asp:ListView>

<asp:PlaceHolder ID="phEmpty" Visible="false" runat="server">
    <span class="cartEmptyS"><%= GetGlobalResourceObject("Product", "msgEmptyBasket")%></span>

</asp:PlaceHolder>



