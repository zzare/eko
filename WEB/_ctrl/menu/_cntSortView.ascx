﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_cntSortView.ascx.cs" Inherits="SM.UI.Controls._cntSortView" %>

    <asp:ListView ID="lvSort" runat="server" >
        <LayoutTemplate>
            <asp:PlaceHolder ID="itemPlaceHolder" runat="server"></asp:PlaceHolder>
        </LayoutTemplate>
                             
        <ItemTemplate >
           <li id='<%# Eval("SMAP_ID") %>' class="sortItem" >

                <%# Eval("SML_TITLE") %>
                
                
                <ul  id="p<%# Eval("SMAP_ID") %>" >
                <% if (SM.EM.Security.Permissions.IsAdvancedCms()){%>
                    <li class="unmovable"/>
                <% } %>
                    
                    <%# RenderChilds(Container.DataItem ) %>

                                    
                </ul>
            </li>            
        </ItemTemplate>
    
    </asp:ListView>
