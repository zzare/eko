﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Security.Principal;
using System.Web.Caching;
using System.Net.Mail;
using System.Collections.Generic;
using System.Threading;

/// <summary>
/// Summary description for PAYMENT
/// </summary>

public partial class PAYMENT
{
    public static void LogPayment(Guid user, DateTime date, decimal price, int type, string notes, string username, Guid? refID) {
        eMenikDataContext db = new eMenikDataContext();

        PAYMENT p = new PAYMENT();

        p.DatePayed = date;
        p.DateAdded = DateTime.Now;
        p.UserId = user;
        p.AddedBy = username;
        p.Type = type;
        p.Notes = notes;
        p.Price = price;
        if (refID != null)
            if (refID != Guid.Empty)
                p.RefID = refID;

        db.PAYMENTs.InsertOnSubmit(p);
        db.SubmitChanges();
    }

    public static PAYMENT GetPaymentByID(int id) {
        eMenikDataContext db = new eMenikDataContext();
        return db.PAYMENTs.SingleOrDefault(w => w.PaymentID == id);    
    }

    public static bool IsFirstTimeBonus(Guid userID, Guid bonusUserID)
    {
        eMenikDataContext db = new eMenikDataContext();
        return (db.PAYMENTs.Count(w => w.UserId == userID && w.RefID == bonusUserID && w.Type == Common.PaymentType.Bonus) == 0);
    }


    public class Common {
        public class PaymentType {

            public const int  Payment = 1;
            public const int Bonus = 2;
            public  const int Promotion = 3;
            public const int BonusMarketing = 4;
            public const int BonusAffiliate = 5;
             
        }

        public static string RenderPaymentType(int id)
        {
            switch (id)
            {
                case PaymentType.Payment:
                    return "Payment";
                case PaymentType.Bonus:
                    return "Bonus";
                case PaymentType.Promotion:
                    return "Promotion";
                case PaymentType.BonusAffiliate:
                    return "Affiliate priliv";
                case PaymentType.BonusMarketing:
                    return "Affiliate priliv (marketing)";
                default:
                    return "neznano";
            }

        }
    }


}
