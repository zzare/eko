﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _em_templates__custom_eko__inc_content_pmodHome : BaseControl_CmsWebPart
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //mn1.CurrentWebPartZone = "wpz_1";
        //mn2.CurrentWebPartZone = "wpz_2";
        //mn3.CurrentWebPartZone = "wpz_3";
        //mn4.CurrentWebPartZone = "wpz_4";
        //mn5.CurrentWebPartZone = "wpz_5";
        //mn6.CurrentWebPartZone = "wpz_6";

        

        // product item
        //ProductRep prep = new ProductRep ();
        //var list = prep.GetProductDescList(SM.BLL.Custom.Smap.ROOT_MAIN , LANGUAGE.GetCurrentLang(), true, ParentPage.em_user.UserId, SM.BLL.Common.ImageType.ProductListBigLow)//.Where(w => w.CAT_ACTION == true)
        //    .OrderByDescending(o => o.CAT_ACTION)
        //    .OrderByDescending(o=>o.CAT_SALE).ThenByDescending(o=>o.DATE_MODIFIED ) .Take(1).ToList();
        //if (list.Count > 0)
        //{
        //    objProductItem1.Prod = list[0];
        //    objProductItem1.LoadList();
        //}
        //if (list.Count > 1)
        //{
        //    objProductItem2.Prod = list[1];
        //    objProductItem2.LoadList();
        //}
        //if (list.Count > 2)
        //{
        //    objProductItem3.Prod = list[2];
        //    objProductItem3.LoadList();
        //}


        // galerry view
        SM.EM.Helpers.AddLinkedStyleSheet(Page, SM.BLL.Common.ResolveUrl("~/_em/templates/_custom/eko/css/jquery.galleryview-3.0.css"), true);

        SM.EM.Helpers.RegisterScriptIncludeAJAX(SM.BLL.Common.ResolveUrl("~/Javascript/jquery/jquery.galleryview-3.0.js"), Page, "jGalelryView");
        SM.EM.Helpers.RegisterScriptIncludeAJAX(SM.BLL.Common.ResolveUrl("~/Javascript/jquery/jquery.timers-1.2.js"), Page, "jTimers");
        
        // init cycle plugin
        //SM.EM.Helpers.RegisterScriptIncludeAJAX(SM.BLL.Common.ResolveUrl("~/Javascript/CyclePlugin/jquery.cycle.all.min.js"), Page, "jCyclePlugin");
        //SM.EM.Helpers.RegisterScriptIncludeAJAX(SM.BLL.Common.Emenik.ResolveMainDomainUrl("~/JavaScript/fancybox/jquery.easing-1.3.pack.js"), Page, "easing");


        // blog
        ArticleRep arep = new ArticleRep();

        //var list = arep.GetArticleListHierarchyWithImage(1482, LANGUAGE.GetCurrentLang(), true, SM.BLL.Custom.MainUserID, SM.BLL.Common.ImageType.ArticleThumbDefault);


        var rec = from a in arep.GetArticlesByActive(true)
                  where a.APPROVED == true
                  orderby a.SortWeight descending , a.RELEASE_DATE descending, a.DATE_MODIFY descending, a.DATE_ADDED descending
                  select a;
        var list =  arep.GetArticleListHierarchyWithImage(1482, LANGUAGE.GetCurrentLang(), true, SM.BLL.Custom.MainUserID, rec, SM.BLL.Common.ImageType.ArticleThumbDefault);



        objBlogList.ShowImage = true;
        objBlogList.PageSize= 5;
        objBlogList.em_user = ParentPage.em_user;
        objBlogList.Data = list;
        objBlogList.BindData();




        
        
    }


    protected override void OnPreRender(EventArgs e)
    {
        Page.Title = ParentPage.TitleRoot;

        base.OnPreRender(e);
    }
}

