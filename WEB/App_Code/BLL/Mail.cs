﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Net.Mail;
/// <summary>
/// Summary description for MAIL
/// </summary>
namespace SM.EM
{

    public class Mail
    {
        public class Account {
            public static class Narocila {
                public static string Email = Common.GetEmailFromSettings(ConfigurationSettings.AppSettings["MAIL_orders"]);
                public static string Pass = Common.GetPassFromSettings(ConfigurationSettings.AppSettings["MAIL_orders"]);
            }

            public static class Info
            {
                public static string Email = Common.GetEmailFromSettings(ConfigurationSettings.AppSettings["MAIL_info"]);
                public static string Pass = Common.GetPassFromSettings(ConfigurationSettings.AppSettings["MAIL_info"]);
            }


            public static class Help
            {
                public static string Email = Common.GetEmailFromSettings(ConfigurationSettings.AppSettings["MAIL_help"]);
                public static string Pass = Common.GetPassFromSettings(ConfigurationSettings.AppSettings["MAIL_help"]);
            }

            public static class Support
            {
                public static string Email = Common.GetEmailFromSettings(ConfigurationSettings.AppSettings["MAIL_support"]);
                public static string Pass = Common.GetPassFromSettings(ConfigurationSettings.AppSettings["MAIL_support"]);
            }
            public static class Send
            {
                public static string Email = Common.GetEmailFromSettings(ConfigurationSettings.AppSettings["MAIL_send"]);
                public static string Pass = Common.GetPassFromSettings(ConfigurationSettings.AppSettings["MAIL_send"]);
            }
            public static class SendGmail
            {
                public static string Email = Common.GetEmailFromSettings(ConfigurationSettings.AppSettings["MAIL_send_gmail"]);
                public static string Pass = Common.GetPassFromSettings(ConfigurationSettings.AppSettings["MAIL_send_gmail"]);
            }
            public static class Offer
            {
                public static string Email = Common.GetEmailFromSettings(ConfigurationSettings.AppSettings["MAIL_offer"]);
                public static string Pass = Common.GetPassFromSettings(ConfigurationSettings.AppSettings["MAIL_offer"]);
            }
        
        }



        public class Template
        {

            public static MailMessage GetPredracun(EM_USER usr, PREDRACUN p)
            {

                // template file
                MailDefinition template = new MailDefinition();
                template.BodyFileName = "~/_MailTemplates/Predracun.htm";
                template.From = SM.EM.Mail.Account.Narocila.Email;

                // fields to be replaced
                Dictionary<string, string> data = new Dictionary<string, string>();
                data.Add("<%SumTotal%>", SM.EM.Helpers.FormatPrice( p.Price));
                data.Add("<%RefNo%>", usr.CODE.ToString("000000"));

                data.Add("<%TRR%>", SM.BLL.Common.Emenik.Data.TRR());
                data.Add("<%TaxNo%>", SM.BLL.Common.Emenik.Data.TaxNo());

                data.Add("<%UserName%>", usr.USERNAME);
                data.Add("<%PageName%>", usr.PAGE_NAME);

                return template.CreateMailMessage(usr.EMAIL, data, new LiteralControl());


            }

            public static MailMessage GetNotifyToExpire(EM_USER usr)
            {
                // template file
                MailDefinition template = new MailDefinition();
                template.BodyFileName = "http://" + SM.BLL.Common.Emenik.Data.PortalHostDomain() + "/_MailTemplates/NotifyToExpire.htm";
                //template.BodyFileName = "~/_MailTemplates/Predracun.htm";
                template.From = SM.EM.Mail.Account.Info.Email;

                


                // fields to be replaced
                Dictionary<string, string> data = new Dictionary<string, string>();
                //data.Add("<%SumTotal%>", p.Price.ToString());
                //data.Add("<%RefNo%>", usr.CODE.ToString("000000"));

                //data.Add("<%TRR%>", SM.BLL.Common.Emenik.Data.TRR());
                //data.Add("<%TaxNo%>", SM.BLL.Common.Emenik.Data.TaxNo());

                data.Add("<%UserName%>", usr.USERNAME);
                data.Add("<%PageName%>", usr.PAGE_NAME);
                data.Add("<%PortalName%>", SM.BLL.Common.Emenik.Data.PortalName());
                
                if (usr.DATE_VALID != null)
                    data.Add("<%DateValid%>", usr.DATE_VALID.Value.ToShortDateString());

                return template.CreateMailMessage(usr.EMAIL, data, new LiteralControl());

            }


            public static string GetNotifyToExpireBodyNoContext(EM_USER usr)
            {
                string template = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">";
                template += "<html xmlns=\"http://www.w3.org/1999/xhtml\">";
                template += "<head><title>" + SM.BLL.Common.Emenik.Data.PortalName() + "- vaša stran bo potekla</title></head>";
                template += "<body><p>";
                template += "Pozdravljeni!    <br /><br />";

                template += "    Vaša spletna stran bo potekla.<br />";
                template += "    Valjavnost spletne strani:<%DateValid%><br /><br />    ";
                template += "    Uporabniško ime:<%UserName%><br />    ";
                template += "Naziv strani:<%PageName%><br /><br />";
                template += "Lep pozdrav, ekipa <%PortalName%> ";
                template += "</p></body></html>";


                // fields to be replaced
                Dictionary<string, string> data = new Dictionary<string, string>();

                data.Add("<%UserName%>", usr.USERNAME);
                data.Add("<%PageName%>", usr.PAGE_NAME);
                data.Add("<%PortalName%>", SM.BLL.Common.Emenik.Data.PortalName());

                if (usr.DATE_VALID != null)
                    data.Add("<%DateValid%>", usr.DATE_VALID.Value.ToShortDateString());



                foreach (KeyValuePair <string, string> item in data  ){
                    template  =  template.Replace(item.Key, item.Value);
                }
                return template;
            }

            public static MailMessage GetNotifyFriend(string msg, EM_USER usr)
            {

                // template file
                MailDefinition template = new MailDefinition();
                template.BodyFileName = "~/_MailTemplates/NotifyFriend.htm";
                template.From = SM.EM.Mail.Account.Info.Email;


                // fields to be replaced
                Dictionary<string, string> data = new Dictionary<string, string>();

                data.Add("<%Message%>", msg);
                data.Add("<%HREF%>", SM.BLL.Common.ResolveUrl( SM.EM.Bonus.GetBonusUserHrefRegister( usr.UserId)));

                if (usr.DATE_VALID != null)
                    data.Add("<%DateValid%>", usr.DATE_VALID.Value.ToShortDateString());

                return template.CreateMailMessage(usr.EMAIL, data, new LiteralControl());
            }

            public static MailMessage GetNewUserConfirmation(string password, EM_USER usr)
            {

                // template file
                MailDefinition template = new MailDefinition();
                template.BodyFileName = "~/_MailTemplates/NewUserConfirmationMail.htm";

                template.From = SM.EM.Mail.Account.Info.Email;


                // fields to be replaced
                Dictionary<string, string> data = new Dictionary<string, string>();

                data.Add("<%UserName%>", usr.USERNAME);
                data.Add("<%Password%>", password );

                data.Add("<%PageHref%>", SM.BLL.Common.ResolveUrl(SM.EM.Rewrite.EmenikUserUrl(usr.PAGE_NAME )));


                return template.CreateMailMessage(usr.EMAIL, data, new LiteralControl());
            }

        }



    }

    public class Common{
    
    
            public static string GetEmailFromSettings(string val){

                if (val.IndexOf(";" ) < 0) return "";
                return val.Substring(0, val.IndexOf(";"));
            }
            public static string GetPassFromSettings(string val){

                if (val.IndexOf(";" ) < 0) return "";
                return val.Substring(val.IndexOf(";")+1);
            }
    }




}
