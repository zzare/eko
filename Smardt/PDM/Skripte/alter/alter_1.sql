alter table dbo.GROUP_ATTRIBUTE 
add
   
   GrType               smallint             not null default 0,
   "Order"              int                  not null default 0
   
go

alter table dbo.ATTRIBUTE 
add
   ValMin               int                  null,
   ValMax               int                  null,
   ValDefault           int                  null,
   Price                money                null
go

alter table dbo.PRODUCT_ATTRIBUTE 
add
   ValMin               int                  null,
   ValMax               int                  null,
   ValDefault           int                  null,
   Price                money                null

go

alter table dbo.CART_ITEM_ATTRIBUTE 
add
   Value                int                  null,
   PriceAttribute       money                null
   
go





/*==============================================================*/
/* Table: PRODUCT_ATTRIBUTE_DESC                                */
/*==============================================================*/
create table dbo.PRODUCT_ATTRIBUTE_DESC (
   PRODUCT_ID           uniqueidentifier     not null default newid(),
   AttributeID          uniqueidentifier     not null,
   LANG_ID              nchar(2)             not null,
   Title                nvarchar(256)        null,
   DescLong             nvarchar(max)        null,
   "Desc"               nvarchar(4000)       null,
   PDescID              int                  identity
)
go

alter table dbo.PRODUCT_ATTRIBUTE_DESC
   add constraint PK_PRODUCT_ATTRIBUTE_DESC primary key (PRODUCT_ID, AttributeID, LANG_ID)
go

/*==============================================================*/
/* Index: XAK_PROD_ATR_DESC_ID                                  */
/*==============================================================*/
create unique index XAK_PROD_ATR_DESC_ID on dbo.PRODUCT_ATTRIBUTE_DESC (
PDescID ASC
)
go

alter table dbo.PRODUCT_ATTRIBUTE_DESC
   add constraint FK_PRODUCT__REFERENCE_LANGUAGE foreign key (LANG_ID)
      references dbo.LANGUAGE (LANG_ID)
go