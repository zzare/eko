﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _ctrl_module_product_viewProductDetailRating : BaseControlView
{

    public object Data;
    public object[] Parameters;
    public vw_Product Prod;



    protected void Page_Load(object sender, EventArgs e)
    {
        

        //LoadList();
    }

    protected override void OnPreRender(EventArgs e)
    {
        LoadList();
        base.OnPreRender(e);
    }

    public void LoadList() {

        if (Data == null)
            return;
        Prod = Data as vw_Product;

        if (Parameters != null)
        {
            if (Parameters[0] != null)
                IsModeCMS = bool.Parse(Parameters[0].ToString());
            //if (Parameters.Length > 1 && Parameters[1] != null)
            //    LangID = Parameters[1].ToString();
        }




    }


    protected string RenderSelectedRating(int num) {
        string ret = "";
        int  avg = (int) Math.Round( VOTE.GetAverage(Prod.TOTAL_RATING, Prod.VOTES));
        
        if ((avg == null && num == 3) ||(avg == num))
            ret = "selected=\"selected\"";

        return ret;
    
    }


    protected string RenderDisabled()
    {
        VOTE v = VOTE.GetVoteByID (Prod.PRODUCT_ID);
        string ret = "";

        if (v == null)
            return ret;

        if (!v.CanVote(Context.Request.UserHostAddress))
            ret = "disabled=\"disabled\"";

        return ret;
    }
    



}
