﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
namespace SM.EM.UI.Controls
{
    public partial class _selLanguage : BaseControl
    {

        protected void Page_Load(object sender, EventArgs e)
        {

            // preselect language
            if (ddlLanguage.SelectedIndex > -1)
                ddlLanguage.SelectedValue = LANGUAGE.GetCurrentCulture();

        }

        public void InitData() {
        
            ddlLanguage.DataSource = CULTURE.GetCulturesAll();
            ddlLanguage.DataTextField = "DESC";
            ddlLanguage.DataValueField = "UI_CULTURE";
            ddlLanguage.DataBind();

        }
    }
}

