﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.Linq ;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using SM.EM.BLL;

/// <summary>
/// Summary description for  Module
/// </summary>
public partial class EM_USER
{

    public bool IsPageActive() {
        if (!this.ACTIVE_WWW ) return false;
        if (this.DATE_VALID < DateTime.Now.Date) return false;


        return true;
    }

    public decimal CurrentCredit()
    {
        if (this.DATE_PAYED == null) return 0;
        if (this.DATE_VALID == null) return 0;
        if (this.DATE_VALID < DateTime.Now.Date ) return 0;

        // get days left
        TimeSpan diff = ((DateTime)this.DATE_VALID) - DateTime.Now ;
        //diff.Days;


        decimal credit = this.GetDbModulesPriceSum() * (decimal)diff.TotalDays  / 365;

        return credit;
    }

    public decimal GetDbModulesPriceSum()
    {
        return USER_MODULE.GetUserModulePriceSum(this.UserId);
    }

    public decimal GetDbModulesPriceSumDiscount()
    {
        return USER_MODULE.GetUserModulePriceSumDiscount(this);
    }

    public double GetUserDiscount() {
        //return SM.BLL.Common.Emenik.Data.DiscountPercent() + this.DISCOUNT_PERCENT;
        return SM.BLL.Common.Emenik.Data.DiscountPercent() ;
    }

    public void RemoveFromCache() {
        SM.EM.Caching.RemoveEmUserByPageName(this.PAGE_NAME);
        SM.EM.Caching.RemoveEmUserByUserName(this.USERNAME);
        SM.EM.Caching.RemoveEmUserByCustomDomain(this.CUSTOM_DOMAIN );
        SM.EM.Caching.RemoveEmUserByUserID(this.UserId);
    
    }

    public void AddToCache()
    {
        RemoveFromCache();
        BizObject.Cache[SM.EM.Caching.GetEmUserCacheKeyUserName(this.USERNAME)] = this;
        BizObject.Cache[SM.EM.Caching.GetEmUserCacheKeyPageName(this.PAGE_NAME)] = this;
        BizObject.Cache[SM.EM.Caching.GetEmUserCacheKeyCustomDomain(this.CUSTOM_DOMAIN )] = this;
        BizObject.Cache[SM.EM.Caching.GetEmUserCacheKey (this.UserId)] = this;
        AddToContext();
    }

    //public void AddToCacheUsername()
    //{
    //    RemoveFromCache();
    //    BizObject.Cache[SM.EM.Caching.GetEmUserCacheKeyUserName(this.USERNAME )] = this;

    //}

    public void AddToContext() {
        HttpContext.Current.Items["em_user"] = this;
    }

    
    
    /**********************       STATIC FUNCTIONS    *********************/
    public static EM_USER CreateNewUser(Guid id,  string username, string email, Guid bonus) {

        // insert address
        eMenikDataContext db = new eMenikDataContext();


        EM_USER usr = new EM_USER();
        usr.USERNAME = username;
        usr.PAGE_NAME = username;
        usr.PAGE_TITLE = username;
        usr.CONT_NAME = username;
        usr.UserId = id;
        usr.LastUpdatedDate = DateTime.Now;
        usr.EMAIL = email;
        usr.CONT_EMAIL  = email;
        usr.DATE_REG = DateTime.Now;
        usr.DATE_ORDER_CHANGED = null;
        usr.DATE_PAYED = null;
        usr.DATE_VALID = null;

        usr.SITEMAP_COUNT = SM.BLL.Common.Emenik.SITEMAP_COUNT_DEFAULT;
        usr.STEPS_DONE = 0;
        usr.ACTIVE_USER = false;
        usr.ACTIVE_WWW = false;
        usr.PORTAL_ID = (short)SM.BLL.Common.Emenik.Data.PortalID();

        usr.SHOW_NAME = true;
        usr.SHOW_ADDRESS = false;
        usr.SHOW_EMAIL = true;
        usr.SHOW_PHONE = false;
        usr.SHOW_MOBILE = false;
        usr.SHOW_FAX = false;
        usr.SHOW_LOGO = false;
        usr.SHOW_TEXT = true;
        usr.IS_DEMO = false;
        usr.IS_EXAMPLE_TEMPLATE = false;

        if (bonus != Guid.Empty)
        {
            usr.BONUS_USER_ID = bonus;
            // check for discount

            //double? discount = (from c in db.MARKETING_CAMPAIGNs where c.CampaignID == usr.BONUS_USER_ID select c.UserDiscount).SingleOrDefault();
            //if (discount != null && discount > 0)
            //{
            //    usr.DISCOUNT_PERCENT = discount.Value;
            //    usr.DISCOUNT_TYPE = Common.DiscountType.OneTime; 
            //}
        }

        db.EM_USERs.InsertOnSubmit(usr);
        db.SubmitChanges();

        // insert default menus
        MENU.InsertDefaultMenusForUser(usr.UserId, usr.USERNAME);

        // ad user to role
        Roles.AddUserToRole(username, "em_user");

        return usr;
    
    }


    public static void InsertDefaultUserData(EM_USER em_user)
    {
        if (em_user.STEPS_DONE == 0)
        {
            // set default design
            EM_USER.ChangeDesign(em_user.UserId, TEMPLATES.Common.DEFAULT_THEME);

            // insert default modules
            MODULE.InsertDefaultModulesForUser(em_user.UserId);

            // update user step
            EM_USER.UpdateUserStep(em_user.UserId, 8);

            // update user active
            EM_USER.UpdateActiveUser(em_user.UserId, true);
        }
    }


    public static EM_USER GetUserByPageName(string pageName) {

        if (SM.EM.Globals.Settings.ST.EnableCaching)
        {

            // if user in cache, return it
            if (BizObject.Cache[SM.EM.Caching.GetEmUserCacheKeyPageName(pageName)] != null)
                return (EM_USER)BizObject.Cache[SM.EM.Caching.GetEmUserCacheKeyPageName(pageName)];

        }
        // user not in cache, retrieve it from DB
        eMenikDataContext db = new eMenikDataContext();
        //SM.HelperClasses.TraceTextWriter traceWriter = new SM.HelperClasses.TraceTextWriter();
        //db.Log = traceWriter; 

        EM_USER emu = db.EM_USERs.Where(u => u.PAGE_NAME == pageName).FirstOrDefault();

        // add user to cache
//        BizObject.Cache[SM.EM.Caching.GetEmUserCacheKeyPageName(pageName)] = emu;
        if (emu != null)
            emu.AddToCache();

        return emu;
    }

    public static EM_USER GetUserByUserName(string userName)
    {

        if (SM.EM.Globals.Settings.ST.EnableCaching)
        {
            // if user in cache, return it
            if (BizObject.Cache[SM.EM.Caching.GetEmUserCacheKeyUserName(userName)] != null)
                return (EM_USER)BizObject.Cache[SM.EM.Caching.GetEmUserCacheKeyUserName(userName )];

        }

        // user not in cache, retrieve it from DB
        eMenikDataContext db = new eMenikDataContext();

        EM_USER emu = db.EM_USERs.Where(u => u.USERNAME  == userName).FirstOrDefault();
        if (emu != null)
            emu.AddToCache();

        return emu;
    }

    public static EM_USER GetUserByCustomDomain(string customDomain)
    {

        if (SM.EM.Globals.Settings.ST.EnableCaching)
        {
            // if user in cache, return it
            if (BizObject.Cache[SM.EM.Caching.GetEmUserCacheKeyCustomDomain(customDomain )] != null)
                return (EM_USER)BizObject.Cache[SM.EM.Caching.GetEmUserCacheKeyCustomDomain(customDomain )];

        }

        // user not in cache, retrieve it from DB
        eMenikDataContext db = new eMenikDataContext();

        EM_USER emu = db.EM_USERs.Where(u => u.CUSTOM_DOMAIN  == customDomain ).FirstOrDefault();
        if (emu != null)
            emu.AddToCache();

        return emu;
    }


    public static EM_USER GetUserByID(Guid id)
    {

        if (SM.EM.Globals.Settings.ST.EnableCaching)
        {
            // if user in cache, return it
            if (BizObject.Cache[SM.EM.Caching.GetEmUserCacheKey (id)] != null)
                return (EM_USER)BizObject.Cache[SM.EM.Caching.GetEmUserCacheKey(id)];

        }

        // user not in cache, retrieve it from DB
        eMenikDataContext db = new eMenikDataContext();

        EM_USER emu = db.EM_USERs.Where(u => u.UserId == id ).SingleOrDefault ();
        if (emu != null)
            emu.AddToCache();

        return emu;
    }

    public static List<EM_USER> GetLastPublishedPages(int num)
    {
        // user not in cache, retrieve it from DB
        eMenikDataContext db = new eMenikDataContext();
        return db.EM_USERs.Where(u => u.ACTIVE_WWW == true && u.IS_DEMO == false && u.IS_EXAMPLE_TEMPLATE == false && u.DATE_VALID > DateTime.Now ).OrderByDescending(w => w.DATE_PAYED).Take(num).ToList();
    }
    public static List<EM_USER> GetLastPublishedPages(int num, int portalID)
    {
        // user not in cache, retrieve it from DB
        eMenikDataContext db = new eMenikDataContext();
        return db.EM_USERs.Where(u => u.PORTAL_ID == (short) portalID &&  u.ACTIVE_WWW == true && u.IS_DEMO == false && u.IS_EXAMPLE_TEMPLATE == false && u.DATE_VALID > DateTime.Now).OrderByDescending(w => w.DATE_PAYED).Take(num).ToList();
    }

    
    public static List<EM_USER> GetLastExampleTemplates(int num)
    {
        // user not in cache, retrieve it from DB
        eMenikDataContext db = new eMenikDataContext();
        return db.EM_USERs.Where(u => u.ACTIVE_WWW == true && u.IS_DEMO == false && u.IS_EXAMPLE_TEMPLATE == true).OrderByDescending(w => w.DATE_PAYED).Take(num).ToList();
    }

    public static List<EM_USER> GetUsersWithCustomDomain()
    {
        // payed and valid
        eMenikDataContext db = new eMenikDataContext();
        return db.EM_USERs.Where(u => u.CUSTOM_DOMAIN != null &&  u.DATE_VALID > DateTime.Now && u.PORTAL_ID == SM.BLL.Common.Emenik.Data.PortalID()  ).OrderByDescending(w => w.DATE_PAYED).ToList();
    }
     

    public static void UpdateUserPayment(Guid userID, decimal payedPrice, int type, string notes, string username, Guid refID) {
        UpdateUserPayment(userID, payedPrice, DateTime.Now, type, notes, username, refID  );
    }

    public static void UpdateUserPayment(Guid userID, decimal payedPrice, DateTime datePayed, int type, string notes, string username, Guid refID)
    {
        eMenikDataContext db = new eMenikDataContext();

        EM_USER user = db.EM_USERs.Where(u => u.UserId == userID).SingleOrDefault();

        user.PAYED_PRICE = payedPrice;  // new payed price
        user.PAYED = true;
        user.DATE_PAYED = datePayed ;   // date payed


        // total sum - if is normal payment - add DISCOUNT from user and portal, else -> NO DISOUNT
        decimal dbModulesSum;
        decimal najemPayedPrice = payedPrice;
        if (type == PAYMENT.Common.PaymentType.Payment)
        {
            dbModulesSum = user.GetDbModulesPriceSumDiscount();
            //  get order
           // ORDER_ITEM  ord = ORDER.GetOrderItemsByItem (refID, ORDER.Common.ModuleItems.NAJEM );
            //if (ord != null)
            //{
            //    najemPayedPrice = ord.Price;
            //}
            //else
                najemPayedPrice = 0;

        }
        else
            dbModulesSum = user.GetDbModulesPriceSum();
        
        // calculate new valid date
        DateTime DateValid = DateTime.Now;
        if (user.DATE_VALID != null)
            DateValid = user.DATE_VALID.Value;

        if (najemPayedPrice != 0)
            user.DATE_VALID = DateValid.AddDays(GetNewDaysCredit(najemPayedPrice, dbModulesSum));

        db.SubmitChanges();

        // remove from cache
        user.RemoveFromCache();

        // log payment
        PAYMENT.LogPayment(userID, datePayed, payedPrice, type, notes, username, refID );

        // if someone has bonus, update his payment
        if (type == PAYMENT.Common.PaymentType.Payment && user.BONUS_USER_ID != null && user.BONUS_USER_ID != Guid.Empty) {
            
            // bonus is from USER AFFILLIATE
            // give bonus to USER only first time

            // if it is first time user payed
            if (PAYMENT.IsFirstTimeBonus(user.BONUS_USER_ID.Value, user.UserId))
            {
                // if is a CAMPAIGN
                MARKETING_CAMPAIGN c = MARKETING_CAMPAIGN.GetCampaignByID(user.BONUS_USER_ID.Value);
                if (c != null)
                {
                    // update marketing credit
                    if (c.MarketingUserId != null)
                    {
                        decimal percent = (decimal)c.MarketingPercent;
                        if (percent > MARKETING_CAMPAIGN.Common.MAX_MARKETING_PERCENT ) 
                            percent = MARKETING_CAMPAIGN.Common.MAX_MARKETING_PERCENT;

                        decimal marketing = 0;
                        if (c.AffilliateType == 0) // percent
                            marketing = (payedPrice * (decimal)percent) / 100;
                        else
                            if (c.AffilliateType == 1) // fixed value
                            {
                                marketing = (decimal )c.MarketingPercent ;
                                if (payedPrice < (decimal)c.MarketingPercent)
                                    marketing = payedPrice;                            
                            }
                        EM_USER.UpdateUserAffiliateCredit(c.MarketingUserId.Value, marketing, PAYMENT.Common.PaymentType.BonusMarketing, user.UserId, datePayed);
                    }
                    // update affiliate credit
                    if (c.AffilliateUserId != null)
                    {
                        decimal percent = (decimal)c.AffilliatePercent ;
                        if (percent > MARKETING_CAMPAIGN.Common.MAX_AFFILIATE_PERCENT)
                            percent = MARKETING_CAMPAIGN.Common.MAX_AFFILIATE_PERCENT;

                        decimal affiliate = 0;

                        if (c.AffilliateType == 0) // percent
                            affiliate = (payedPrice * (decimal)percent) / 100;
                        else
                            if (c.AffilliateType == 1) // fixed value
                            {
                                affiliate = (decimal)c.AffilliatePercent ;
                                if (payedPrice < (decimal)c.AffilliatePercent )
                                    affiliate = payedPrice;
                            }

                        EM_USER.UpdateUserAffiliateCredit(c.AffilliateUserId.Value, affiliate, PAYMENT.Common.PaymentType.BonusAffiliate, user.UserId, datePayed);
                    }

                    // remove user discount if set on campaign
                    //if (user.DISCOUNT_PERCENT > 0 && user.DISCOUNT_TYPE == Common.DiscountType.OneTime )
                    //    EM_USER.UpdateUserDiscount(user.UserId, 0, Common.DiscountType.OneTime );

                }
                else // normal user AFFIlLIATE
                    UpdateUserPayment(user.BONUS_USER_ID.Value, SM.EM.Bonus.GetBonusPrice(payedPrice), datePayed, PAYMENT.Common.PaymentType.Bonus, "samodejno", user.USERNAME, user.UserId);

            }
            else
            {
                //user has payed before - only marketing affiliate has bonus
                // if is a CAMPAIGN
                MARKETING_CAMPAIGN c = MARKETING_CAMPAIGN.GetCampaignByID(user.BONUS_USER_ID.Value);
                if (c != null)
                {
                    // update marketing credit
                    if (c.MarketingUserId != null && c.MarketingPercentNextYear > 0)
                    {
                        decimal percent = (decimal)c.MarketingPercentNextYear;
                        if (percent > MARKETING_CAMPAIGN.Common.MAX_MARKETING_PERCENT)
                            percent = MARKETING_CAMPAIGN.Common.MAX_MARKETING_PERCENT;

                        decimal marketing = (payedPrice * (decimal)percent) / 100;
                        EM_USER.UpdateUserAffiliateCredit(c.MarketingUserId.Value, marketing, PAYMENT.Common.PaymentType.BonusMarketing, user.UserId, datePayed);
                    }
                }
            }
        }


    }
    public static void SetValidationDate(Guid userID, DateTime dateValid) {
        eMenikDataContext db = new eMenikDataContext();

        EM_USER user = db.EM_USERs.Where(u => u.UserId == userID).SingleOrDefault();
        user.DATE_VALID = dateValid;
        user.DATE_ORDER_CHANGED = DateTime.Now;
        db.SubmitChanges();
        user.RemoveFromCache();
    }

    public static void UpdateImageLogo(Guid userID, IMAGE img)
    {
        eMenikDataContext db = new eMenikDataContext();

        EM_USER user = db.EM_USERs.Where(u => u.UserId == userID).SingleOrDefault();
        user.IMG_LOGO_ID  = img.IMG_ID  ;
        user.TYPE_LOGO_ID = img.TYPE_ID ;
        user.IMG_LOGO = img.IMG_URL;

        db.SubmitChanges();
        user.AddToCache();
    }
    public static void UpdateUserAffiliateCredit(Guid userID, decimal bonus, int type, Guid refID, DateTime datePayed)
    {
        //eMenikDataContext db = new eMenikDataContext();

        //EM_USER user = db.EM_USERs.Where(u => u.UserId == userID).SingleOrDefault();
        //decimal credit = user.AFFILIATE_CREDIT ?? 0;

        //user.AFFILIATE_CREDIT = credit + bonus;

        //db.SubmitChanges();
        //user.AddToCache();

        //// log payment ( as affiliate)
        //PAYMENT.LogPayment(userID, datePayed, bonus, type, "auto bonus", "auto", refID);
    }

    public static void UpdateUserAddressCompany(Guid userID, int company, int address, int type)
    {
        eMenikDataContext db = new eMenikDataContext();

        EM_USER user = db.EM_USERs.Where(u => u.UserId == userID).SingleOrDefault();
        if (company < 1)
            user.COMPANY_ID = null;
        else
            user.COMPANY_ID = company  ;

        if (address  < 1)
            user.ADDR_ID = null;
        else        
            user.ADDR_ID = address;

        user.USER_TYPE = type;
        
            db.SubmitChanges();
            //user.RemoveFromCache();
            user.AddToCache();
    }
    public static void UpdateUserFistName(Guid userID, string firstName, string lastName, string email)
    {
        eMenikDataContext db = new eMenikDataContext();

        EM_USER user = db.EM_USERs.Where(u => u.UserId == userID).SingleOrDefault();

        user.FIRSTNAME = firstName;
        user.LASTNAME = lastName;
        user.EMAIL = email;
        db.SubmitChanges();
        //user.RemoveFromCache();
        user.AddToCache();
    }

    public static void UpdateUserStep(Guid userID, int step)
    {
        eMenikDataContext db = new eMenikDataContext();

        EM_USER user = db.EM_USERs.Where(u => u.UserId == userID).SingleOrDefault();
        if (user.STEPS_DONE < step)
        {
            user.STEPS_DONE = step;

            db.SubmitChanges();
            //user.RemoveFromCache();
            user.AddToCache();
        }
    }
    //public static void UpdateOrderDomain(Guid userID, string orderDomain, short orderDomainStatus)
    //{
    //    eMenikDataContext db = new eMenikDataContext();

    //    EM_USER user = db.EM_USERs.Where(u => u.UserId == userID).SingleOrDefault();
    //    user.ORDERED_DOMAIN = orderDomain;
    //    user.ORDERED_DOMAIN_STATUS = orderDomainStatus;

    //    db.SubmitChanges();
    //    user.AddToCache();
    //}
    //public static void UpdateOrderDomainStatus(Guid userID, short orderDomainStatus)
    //{
    //    eMenikDataContext db = new eMenikDataContext();

    //    EM_USER user = db.EM_USERs.Where(u => u.UserId == userID).SingleOrDefault();
    //    user.ORDERED_DOMAIN_STATUS = orderDomainStatus;

    //    db.SubmitChanges();
    //    user.AddToCache();
    //}
    public static void UpdateBonusUserID(Guid userID, Guid bonusID)
    {
        eMenikDataContext db = new eMenikDataContext();

        EM_USER user = db.EM_USERs.Where(u => u.UserId == userID).SingleOrDefault();
        user.BONUS_USER_ID = bonusID ;

        db.SubmitChanges();
        user.AddToCache();
    }
    //public static void UpdateUserDiscount(Guid userID, double discount, short discountType)
    //{
    //    eMenikDataContext db = new eMenikDataContext();

    //    EM_USER user = db.EM_USERs.Where(u => u.UserId == userID).SingleOrDefault();
    //    user.DISCOUNT_PERCENT = discount ;
    //    user.DISCOUNT_TYPE = discountType;

    //    db.SubmitChanges();
    //    user.AddToCache();
    //}


    //public static EM_USER UpdateNewsletterEnabled(Guid userID, bool enabled, bool addToCache)
    //{
    //    eMenikDataContext db = new eMenikDataContext();

    //    EM_USER user = db.EM_USERs.Where(u => u.UserId == userID).SingleOrDefault();
    //    if (user == null)
    //        return user;

    //    user.NEWSLETTER_ENABLED = enabled;

    //    db.SubmitChanges();
    //    //user.RemoveFromCache();
    //    if (addToCache )
    //        user.AddToCache();

    //    return user;
    //}

    //public static bool UpdateAdvancedSettings(Guid userID, string pageName, string pageTitle)
    //{
    //    eMenikDataContext db = new eMenikDataContext();
    //    EM_USER user ;
    //        user = db.EM_USERs.Where(u => u.UserId == userID).SingleOrDefault();

    //    string pName = SM.EM.Rewrite.ConvertToFriendlyURL(pageName, "." );
    
    //    if (ExistDuplicatePageName(userID, pName )) 
    //        return false;

    //    // remove existing from cache
    //    if (user.PAGE_NAME != pName)
    //    {
    //        SetPageName(pName);
    //        user.RemoveFromCache();
    //    }
        
    //    user.PAGE_NAME = pName;
    //    if (pageTitle == "")
    //        user.PAGE_TITLE = pageName;
    //    else
    //        user.PAGE_TITLE = pageTitle;
    //    db.SubmitChanges();

    //    user.AddToCache();

    //    return true;
    //}

    //public static void UpdateCustomDomain(Guid userID, string domain)
    //{
    //    // format domain
    //    string d = SM.EM.Helpers.FormatDomain(domain );

    //    // update domain        
    //    eMenikDataContext db = new eMenikDataContext();
    //    EM_USER user = db.EM_USERs.Where(u => u.UserId == userID).SingleOrDefault();

    //    if (string.IsNullOrEmpty(d))
    //    {
    //        user.CUSTOM_DOMAIN_ACTIVATED = false;
    //        user.CUSTOM_DOMAIN = null;
    //    }
    //    else
    //    {
    //        // if domain is set, check if exists
    //        user.CUSTOM_DOMAIN = d;
    //        user.CUSTOM_DOMAIN_ACTIVATED = SM.EM.Helpers.IsDomainActive(d);
    //    }

    //    db.SubmitChanges();
    //    user.AddToCache();

    //    //// add rewrite rules
    //    //if (!string.IsNullOrEmpty(d))
    //    //{
    //    //    SM.EM.Rewrite.Domain.DeleteDomainRewrites(user.USERNAME);
    //    //    SM.EM.Rewrite.Domain.AddDomainRewrites(user.CUSTOM_DOMAIN, user.PAGE_NAME, user.USERNAME);
    //    //}

    //    // remove menu cache 
    //    SM.EM.Caching.RemoveEmenikUserMenuCacheKey(user.PAGE_NAME);


    //}

    public static void SetPageName(string pname) {

        if (HttpContext.Current == null) return;

        HttpContext.Current.Items["pname"] = pname;


    }

    public static bool ExistDuplicatePageName(Guid userID, string pageName)
    {
        eMenikDataContext db = new eMenikDataContext();

        EM_USER dupl = db.EM_USERs.Where(w => w.PAGE_NAME == pageName && w.UserId != userID).SingleOrDefault();
        if (dupl == null) return false;

        return true;
    }
    public static bool ExistPageName(string pageName)
    {
        eMenikDataContext db = new eMenikDataContext();

        EM_USER dupl = db.EM_USERs.Where(w => w.PAGE_NAME == pageName ).SingleOrDefault();
        if (dupl == null) return false;

        return true;
    }

    public static bool ExistDuplicateDomain(Guid userID, string domain)
    {
        eMenikDataContext db = new eMenikDataContext();

        EM_USER dupl = db.EM_USERs.Where(w => w.CUSTOM_DOMAIN  == domain && w.PAYED == true  && w.UserId != userID).SingleOrDefault();
        if (dupl == null) return false;

        return true;
    }


    public static void UpdateImageHeader(Guid userID, IMAGE img)
    {
        eMenikDataContext db = new eMenikDataContext();

        EM_USER user = db.EM_USERs.Where(u => u.UserId == userID).SingleOrDefault();
        user.IMG_HEADER_ID  = img.IMG_ID;
        user.TYPE_HEADER_ID = img.TYPE_ID;
        user.IMG_HEADER = img.IMG_URL;

        db.SubmitChanges();
        //user.RemoveFromCache();
        user.AddToCache();
    }
    public static void UpdateShowLogo(Guid userID, bool showLogo, bool showText)
    {
        eMenikDataContext db = new eMenikDataContext();

        EM_USER user = db.EM_USERs.Where(u => u.UserId == userID).SingleOrDefault();
        user.SHOW_LOGO = showLogo;
        user.SHOW_TEXT = showText;

        db.SubmitChanges();
        //user.RemoveFromCache();
        user.AddToCache();
    }
    public static void UpdateMultiLingual(Guid userID, bool isMulti)
    {
        eMenikDataContext db = new eMenikDataContext();

        EM_USER user = db.EM_USERs.Where(u => u.UserId == userID).SingleOrDefault();
        user.IS_MULTILINGUAL = isMulti;

        db.SubmitChanges();
        //user.RemoveFromCache();
        user.AddToCache();
    }
    public static void UpdateActiveUser(Guid userID, bool active)
    {
        eMenikDataContext db = new eMenikDataContext();

        EM_USER user = db.EM_USERs.Where(u => u.UserId == userID).SingleOrDefault();
        user.ACTIVE_USER  = active ;

        db.SubmitChanges();
        user.AddToCache();
    }

    public static EM_USER  UpdateActiveWWW(Guid userID, bool active)
    {
        eMenikDataContext db = new eMenikDataContext();

        EM_USER user = db.EM_USERs.Where(u => u.UserId == userID).SingleOrDefault();
        user.ACTIVE_WWW = active;

        db.SubmitChanges();
        user.AddToCache();

        return user;
    }
    public static EM_USER UpdateDomainActive(Guid userID, bool active)
    {
        eMenikDataContext db = new eMenikDataContext();

        EM_USER user = db.EM_USERs.Where(u => u.UserId == userID).SingleOrDefault();
        user.CUSTOM_DOMAIN_ACTIVATED  = active;

        db.SubmitChanges();
        user.AddToCache();

        return user;
    }

    public static void ChangeDesign(Guid userID, int themeID)
    {
        eMenikDataContext db = new eMenikDataContext();
        THEME t = TEMPLATES.GetThemeByID(themeID);
        

        EM_USER user = db.EM_USERs.Where(u => u.UserId == userID).SingleOrDefault();
        user.THEME_ID  = t.THEME_ID;

        // if master is different, get/set new data
        if (user.MASTER_ID != t.MASTER_ID)
        {
            MASTER m = TEMPLATES.GetMasterByID(t.MASTER_ID);
            user.MASTER_ID = t.MASTER_ID;


            // if image logo exists and image type is different on new master, make new image type if doesn't exist
            if (user.IMG_LOGO_ID != null && user.TYPE_LOGO_ID != null)
            {
                if (user.TYPE_LOGO_ID != m.IMG_TYPE_ID)
                {
                    IMAGE img = IMAGE.Func.AutoCreateImageForType(user.IMG_LOGO_ID.Value, m.IMG_TYPE_ID.Value, true);
                    if (img != null)
                    {
                        user.IMG_LOGO = img.IMG_URL;
                        user.TYPE_LOGO_ID = img.TYPE_ID;
                    }
                }
            }
            else {
                // set masters page image type
                user.TYPE_LOGO_ID = m.IMG_TYPE_ID;
            
            }

            // if masterpage has header image, set image type
            if (m.HAS_HEADER_IMAGE && m.HDR_TYPE_ID != null) { 
                // if user already has logo, change it to fit type
                
                if (user.TYPE_HEADER_ID != null && user.TYPE_HEADER_ID != m.HDR_TYPE_ID && user.IMG_HEADER_ID != null)
                {
                    
                    IMAGE img = IMAGE.Func.AutoCreateImageForType(user.IMG_HEADER_ID .Value, m.HDR_TYPE_ID.Value, true);
                    if (img != null)
                    {
                        user.IMG_HEADER = img.IMG_URL;
                        user.TYPE_HEADER_ID = m.HDR_TYPE_ID;
                    }
                    else
                    {
                        //user.TYPE_HEADER_ID = null;
                        //user.IMG_HEADER = null;
                    }
                }
            }


            // check all sitemaps if same pagemodules exist on new masterpage
            //List<SITEMAP> sml = SITEMAP.GetSiteMapsByUserName(user.USERNAME).ToList();





            List<SITEMAP> sml = (from smm in db.SITEMAPs join mm in db.MENUs on smm.MENU_ID equals mm.MENU_ID where mm.USERNAME == user.USERNAME  select smm).ToList();

            foreach (SITEMAP sm in sml) { 
                // if PageModule exists in Masters PageModules
                PAGE_MODULE  mp = PAGE_MODULE.GetPageModuleByMasterByID(user.MASTER_ID.Value, sm.PMOD_ID);



                // Module doesn't exist, find new one
                if (mp == null) {
                    // original page module
                    PAGE_MODULE pmsmp = PAGE_MODULE.GetPageModuleByByID(sm.PMOD_ID);

                    // find all modules
                    List<PAGE_MODULE> pmods = PAGE_MODULE.GetModulesByTypeByMaster(SM.BLL.Common.PageModuleTypes.Emenik_User, user.MASTER_ID.Value).ToList();
                    int diff = 5;
                    int nDiff = -1;
                    int nPmod = -1;
                    foreach (PAGE_MODULE pm in pmods) {
                        if (pm.EM_COL_NO != null && pmsmp.EM_COL_NO != null)
                        {
                            nDiff = (int)Math.Abs(pm.EM_COL_NO.Value - pmsmp.EM_COL_NO.Value);
                            // if new difference in ColNumbers is smaller than before, use it
                            if (nDiff < diff)
                            {
                                diff = nDiff;
                                nPmod = pm.PMOD_ID;
                                if (diff == 0) break;
                            }
                        }
                    }
                    // set new PageModule
                    if (diff > 2 || nPmod < 1)
                        throw new System.Exception("Can't find suitable pagemodule !");
                    sm.PMOD_ID = nPmod;


                
                }

            
            }


        }

        db.SubmitChanges();
        //user.RemoveFromCache();
        user.AddToCache();
    }
    

    public static double  GetNewDaysCredit(decimal payedPrice, decimal yearlySum){

        if (payedPrice == 0) return 0;

        //decimal modulesSum = USER_MODULE.GetUserModulePriceSum(userID);

        if (yearlySum == 0) return 365;

        return (double) (365 * (payedPrice / yearlySum));
    }


    public static EM_USER  GetFromContext()
    {
        if (HttpContext.Current .Items["em_user"] != null)
            return ((EM_USER)HttpContext.Current.Items["em_user"]);

        return null;

    }
    public static EM_USER GetFromCache(string pagename)
    {
        return (EM_USER)BizObject.Cache[SM.EM.Caching.GetEmUserCacheKeyPageName(pagename)];
    }

    public static string GetPageStatus(EM_USER usr) {
        if (usr == null) return "";

        if (!usr.ACTIVE_USER) return "V izdelavi";

        if (!usr.ACTIVE_WWW) return "Še neobjavljeno <a href=\"#publish-site\" onclick=\"publishSite()\">objavi zdaj!</a>";

        if (!usr.PAYED) return "V testiranju [do " + usr.DATE_REG.Value.AddDays(SM.BLL.Common.Emenik.EMENIK_TEST_PERIOD_DAYS ).ToShortDateString() + "]"  ; 

        if (usr.DATE_VALID == null || usr.DATE_VALID < DateTime.Now.Date) {
            if (usr.DATE_VALID == null)
                return "Stran je potekla";
            else
                return "Stran je potekla dne " + usr.DATE_VALID.Value.ToShortDateString(); 
        }

        // to be expired
        if (usr.DATE_VALID < DateTime.Now.AddDays(SM.BLL.Common.Emenik.EMENIK_TO_BE_EXPIRED_DAYS))
            return "Stran bo potekla " + usr.DATE_VALID.Value.ToShortDateString() + " <a href=\"#publish-site\" onclick=\"publishSite()\"><span style=\"color:#FF6600\">PODALJŠAJ ZDAJ!</span></a>";


        return "Objavljena do " + usr.DATE_VALID.Value.ToShortDateString();

    }


    public static IQueryable<EM_USER> GetUsers()
    {
        eMenikDataContext db = new eMenikDataContext();

        // all open
        var  list = (from t in db.EM_USERs select t);
        return list;
    }


    // all to notify
    public static List<EM_USER> GetUsersToNotifyToExpire(int daysBefore)
    {
        if (daysBefore < 1)
            daysBefore = 30;

        eMenikDataContext db = new eMenikDataContext();

        // all open
        List<EM_USER > list = (from t in db.EM_USERs  where (t.ACTIVE_WWW  == true && t.PAYED == true && t.DATE_VALID > DateTime.Now.Date  && t.DATE_VALID < DateTime.Now.AddDays(daysBefore ) ) select t).ToList();
        return list;
    }
    // only those that have not been notified in last week (during last daysLastNotify days)
    public static List<EM_USER> GetUsersToNotifyToExpire(int daysBefore, Guid jobID, int daysLastNotify)
    {
        if (daysBefore < 1)
            daysBefore = 30;

        eMenikDataContext db = new eMenikDataContext();

        // all open
        List<EM_USER> list = (from t in db.EM_USERs
                              where t.ACTIVE_WWW == true && t.PAYED == true && t.DATE_VALID > DateTime.Now.Date && t.DATE_VALID < DateTime.Now.AddDays(daysBefore)
                              && !(from e in db.JOB_EMAILs
                                   from l in db.JOB_LOGs
                                   where e.JobLogID == l.JobLogID && l.JobID == jobID && e.ReferenceID == t.UserId && e.Email == t.EMAIL  && e.DateSent > DateTime.Now.AddDays(-daysLastNotify)
                                   select 1
                                       ).Contains(1)
                              select t).ToList();
        
        return list;
    }

    //public void Detach()
    //{
    //    this.PropertyChanged = null;

    //    this.PropertyChanging = null;

    //    // Assuming there's a foreign key from Employee to Boss
    //    this.ADDRESS = default( EntityRef<ADDRESS>);
    //    this.MASTER = default(System.Data.Linq.EntityRef<MASTER>);
    //    this.THEME = default(System.Data.Linq.EntityRef<THEME>);
    //    this.COMPANY = default(System.Data.Linq.EntityRef<COMPANY>);
    //    // Similarly set child objects to default as well

    //    this.EM_USER_CULTUREs = default(System.Data.Linq.EntitySet<EM_USER_CULTUREs>);
    //    this.USER_MODULEs = default(System.Data.Linq.EntitySet<USER_MODULEs>);
    //}




    public class Current {
        public static  EM_USER GetCurrentPage(  ) {
            HttpContext context = HttpContext.Current;

            if (context == null)
                return null;

        
            // first check context items and QS
            string pname = SM.EM.BLL.BizObject.GetPageName();

            EM_USER em_user = null;
            
            
            if (string.IsNullOrEmpty(pname)) {



                string current_host = context.Request.Url.Host.ToLower();
                int posSub = -1;
                posSub = current_host.IndexOf("." + SM.BLL.Common.Emenik.Data.PortalHostDomain());


                // try get pname from url referrer
                //if (!SM.BLL.Common.Emenik.Data.EnableSubDomainUsers())
                //{
                //    if (context.Request.UrlReferrer.Segments[1].ToLower() == "web/")
                //        pname = context.Request.UrlReferrer.Segments[2].TrimEnd("/".ToCharArray());
                //    else
                //        pname = context.Request.UrlReferrer.Segments[1].TrimEnd("/".ToCharArray());

                //    // check if is valid page name
                //    em_user = EM_USER.GetUserByPageName(pname);
                //    if (em_user == null)
                //        pname = "";

                //} 

                // try subdomain
                if (string.IsNullOrEmpty(pname) && SM.BLL.Common.Emenik.Data.EnableSubDomainUsers() && posSub > 0)
                {
                    pname = current_host.Substring(0, posSub);
                    
                }
                // try custom domain
                if (string.IsNullOrEmpty(pname) && SM.BLL.Common.Emenik.Data.CustomDomainsEnabled())
                {
                    EM_USER _em_user = EM_USER.GetUserByCustomDomain(current_host);
                    if (_em_user != null)
                    {
                        pname = _em_user.PAGE_NAME;                        
                    }
                    else if (!current_host.Contains(SM.BLL.Common.Emenik.Data.PortalHostDomain()))
                    {
                        // custom domain that user came from doesn't exist in DB
                        return null;
                    }
                }
                // try get pname from url referrer
                else if (!SM.BLL.Common.Emenik.Data.EnableSubDomainUsers())
                {
                    if (context.Request.UrlReferrer.Segments.Length >= 3 && context.Request.UrlReferrer.Segments[1].ToLower() == "web/")
                        pname = context.Request.UrlReferrer.Segments[2].TrimEnd("/".ToCharArray());
                    else
                        pname = context.Request.UrlReferrer.Segments[1].TrimEnd("/".ToCharArray());
                }    
                
            }

            if (string.IsNullOrEmpty(pname))
                return null;

            // add to context
            EM_USER.SetPageName(pname);

            // return user
            return EM_USER.GetUserByPageName(pname);
        }
    }

    public class Common
    {
        public static string RenderCMSHref(Guid user)
        {
            return "~/cms/ManageUser.aspx?u=" + user.ToString() + "&smp=173";
        }

        public class DiscountType
        {
            public const int OneTime = 0;
            public const int Permanent = 1;

        }

        public class UserType {
            public const int NotSet = 0;
            public const int Pravna = 1;
            public const int Fizicna = 2;

            public const int Animal = 3;

            public static int  GetUserType(string type){
                if (type == "P")
                    return Pravna ;
                else
                    return Fizicna;
            
            }
        }

    }

}
