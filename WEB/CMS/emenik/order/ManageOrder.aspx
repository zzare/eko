﻿<%@ Page Language="C#" MasterPageFile="~/CMS/CMS.master" AutoEventWireup="true" CodeFile="ManageOrder.aspx.cs" Inherits="SM.UI.CMS.CMS_Intranet_ManageOrder" Title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register TagPrefix="SB" TagName="OrderEdit" Src="~/CMS/controls/order/_cmsOrderEdit.ascx"   %>
<%@ Register TagPrefix="SB" TagName="OrderFilter" Src="~/CMS/controls/order/_cmsOrderFilter.ascx"   %>
<%@ Register TagPrefix="SB" TagName="OrderList" Src="~/CMS/controls/order/_cmsOrderList.ascx"   %>

<asp:Content ID="c1" ContentPlaceHolderID="cphNavL" runat="server">
    
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cph" Runat="Server">

    <asp:UpdatePanel ID="upList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>

            <asp:Panel ID="panSListHD" runat="server" CssClass="cpeListHD" >
                <asp:Image ID="imgCollapse" runat="server" />
               order list
            </asp:Panel>

            <asp:Panel ID="panSList" runat="server">

                <div class="qFilter">
                    <asp:UpdatePanel ID="upFilter" runat="server" UpdateMode="Always">
                        <ContentTemplate>
                            <div class="floatR">
                                <SB:OrderFilter ID="objFilter" runat="server" />
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>

                <SB:OrderList ID="objOrderList" runat="server" ShowCheckboxColumn="false" ShowManageColumn="true" ShowEditColumn="true" />
            </asp:Panel>    

            <cc1:CollapsiblePanelExtender ID="cpeQList" runat="server" Collapsed="false" CollapseControlID="panSListHD" CollapsedImage="~/_res/images/icon/plus.gif" ExpandedImage="~/_res/images/icon/minus.gif" ImageControlID="imgCollapse" ExpandControlID="panSListHD" TargetControlID="panSList" ExpandDirection="Vertical" CollapsedText="Expand" ExpandedText="Collapse" >
            </cc1:CollapsiblePanelExtender>    
                
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="objFilter" EventName="OnFilterChanged"   />
            <asp:AsyncPostBackTrigger ControlID="tabOrder$tpOrder$objOrderEdit" EventName="OnUserChanged"   />
            <asp:AsyncPostBackTrigger ControlID="tabOrder$tpOrder$objOrderEdit" EventName="OnCancel"   />
        </Triggers>
    </asp:UpdatePanel>

        
    <br />

    <asp:UpdatePanel ID="upEdit" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
        <ContentTemplate>
            <cc1:TabContainer ID="tabOrder" runat="server" ActiveTabIndex="0"  >
                <cc1:TabPanel ID="tpOrder" HeaderText="Order DATA" runat="server"  >
                    <ContentTemplate>
                        
                         <SB:OrderEdit   id="objOrderEdit" runat="server" />
                    
                    </ContentTemplate>
                </cc1:TabPanel>    
                
<%--                <cc1:TabPanel ID="tpPayment" HeaderText="PAYMENTS" runat="server" Visible="true"  >
                    <ContentTemplate>
                    
                        <EM:PaymentList id="objPayment" runat="server"></EM:PaymentList>
                        <EM:PaymentEdit id="objPaymentEdit" runat="server"></EM:PaymentEdit>
                            
                    </ContentTemplate>
                </cc1:TabPanel>  --%>                 
            </cc1:TabContainer>

        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger EventName="OnEdit" ControlID="objOrderList" />
        </Triggers>        
    </asp:UpdatePanel>              





</asp:Content>