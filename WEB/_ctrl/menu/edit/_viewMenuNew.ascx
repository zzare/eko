﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewMenuNew.ascx.cs" Inherits="_ctrl_menu_edit_viewMenuNew" %>

<script language = "javascript" type="text/javascript">
    function addMenuSave(act) {
        var t = $("#<%= tbTitle.ClientID %>").val();
        var s = $("#hfParentMenu").val();
        if (s == null || s == undefined)
            s = -1;

        openModalLoading('Shranjujem...');
        WebServiceEMENIK.SaveMenuNew(t, s, em_g_lang, act, onMenuNewSuccessSaveRedirect, onSaveError, act);

    }

    function onMenuNewSuccessSaveRedirect(ret, c) {
        if (ret.Code == 0) {
            setModalLoaderErrValidation('', ret.Message);
            return;
        }
        else if (ret.Code != 1) {
            setModalLoaderErr(ret.Message, true);
            return;
        }
        closeModalLoader();
        closeModalLoading();

        // soft refresh
        if (c) {
            if (c == 'soft_r') {
                $('#pme' + ret.ID).replaceWith(ret.Data).sm_animate_change();
                return;
            } 
        }

        openModalLoading('Osvežujem...');
        if (ret.Data != null && ret.Data.length > 0)
            document.location.href = ret.Data;
        else {      // todo: porpavi na ajax
            //document.location.reload(false);
            window.location.href = window.location.href;
             
        }
    }
       
</script>



<div class="modPopup_header modPopup_headerEdit">
    <h4>DODAJ NOVO PODSTRAN</h4>
    <p>Dodaj novo podstran</p>
</div>
<div class="modPopup_main">
    
    <span class="modPopup_mainTitle">Naziv:</span> 
    <input class="modPopup_TBXsettings" style="width:500px" id="tbTitle" maxlength="256" runat="server" type="text" />
    <br />
    <% if (SM.EM.Security.Permissions.IsPortalAdmin ()){%>
    <span class="modPopup_mainTitle">Lokacija:</span> 
    <span id="menuSmapTitle" ><%= SitemapLang.SML_TITLE %></span> 
    <a onclick="openLookup('menuSmapTitle', 'hfParentMenu', 2, <%= SitemapLang.SMAP_ID%>); return false" title="prebrskaj" class="Alookup" href="#"></a>    
    <% } %>
    <input id="hfParentMenu" type="hidden" value='<%= ( SitemapLang != null) ? SitemapLang.SMAP_ID : -1  %>' /> 

</div>   

<div class="modPopup_footer"> 
    <div class="buttonwrapperModal">
        <a class="lbutton lbuttonConfirm" href="#" onclick="addMenuSave(''); return false"><span>Dodaj novo podstran</span></a>
        <% if (SM.EM.Security.Permissions.IsAdvancedCms()){ %>
            <a class="lbutton lbuttonConfirm" href="#" onclick="addMenuSave('soft_r'); return false"><span>Dodaj hitro</span></a>
        <%} %>        
        <%--<a class="lbutton lbuttonConfirm" href="#" onclick="addProductSave('edit'); return false"><span>Dodaj nov izdelek in ga UREDI</span></a>--%>
        <a class="lbutton lbuttonDelete" href="#" onclick="closeModalLoader(); return false" ><span>Prekliči</span></a>
    </div>
</div>
