﻿using System;
using System.Collections;
using System.Collections.Generic ;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SM.EM.UI.CMS;

namespace SM.EM.UI.CMS.Controls
{
    public partial class CMS_cmsImageEdit : BaseControl
    {
        public event EventHandler OnImageEdited;
        public event EventHandler OnImageEditCanceled;

        public int _smapID = -1;
        public string _langID = "-1";
        public const int MAX_IMAGE_WIDTH = 600;

        public int ImageID { get { if (ViewState["ImageID"] == null) return -1; return int.Parse(ViewState["ImageID"].ToString()); } set { ViewState["ImageID"] = value; } }
        protected int TypeID { get { return int.Parse(ddlTypes.SelectedValue); } }

        protected void Page_Load(object sender, EventArgs e)
        {
            // init image
            imgMain.Attributes["onload"] = "SetMaxImageWidth(this, " + MAX_IMAGE_WIDTH + ")";

        }

        public void BindData() {

            LoadImageTypes();

            LoadImage();

            LoadImageDesc();
        }

        public void LoadImageTypes() { 
            // load data
            if (ddlTypes.Items.Count < 1) {
                ddlTypes.DataSource = IMAGE.GetImageTypesByImage(ImageID);
                ddlTypes.DataTextField = "TYPE_TITLE";
                ddlTypes.DataValueField = "TYPE_ID";
                ddlTypes.DataBind();            
            }
        }

        protected void LoadImage() { 
            // load image data for selected image type
            vw_Image image = IMAGE.GetvwImageByID(ImageID, TypeID);

            imgMain.Src = image.IMG_URL;
            cbActive.Checked = image.IMG_ACTIVE;
            tbAuthor.Text = image.IMG_AUTHOR;

        }

        protected void LoadImageDesc() {
            List<vw_ImageDesc> list = IMAGE.GetImageDescsByID(ImageID);
            lvList.DataSource = list;
            lvList.DataBind();        
        }

        public void Cancel()
        {
            OnImageEditCanceled(this, new EventArgs());
        }
        public void Save() {

            int i = 0;
            // save image title changes
            foreach (ListViewItem item in lvList.Items)
            {
                HiddenField hfLang = item.FindControl("hfLang") as HiddenField ;

                TextBox tbTitle = item.FindControl("tbTitle") as TextBox;
                TextBox tbDesc = item.FindControl("tbDesc") as TextBox;
                
                // save desc
                IMAGE.SaveImageDesc(ImageID, hfLang.Value , tbTitle.Text, tbDesc.Text);

                i++;
            }

            // save image orig data
            eMenikDataContext db = new eMenikDataContext();

            IMAGE_ORIG o = (from im in db.IMAGE_ORIGs where im.IMG_ID == ImageID select im).SingleOrDefault();
            o.IMG_ACTIVE = cbActive.Checked;
            o.IMG_AUTHOR = tbAuthor.Text;
            db.SubmitChanges();

            OnImageEdited(this, new EventArgs());
        
        }

        //protected void btSave_Click(object sender, EventArgs e)
        //{
        //    Save();

        //}
        //protected void btCancel_Click(object sender, EventArgs e)
        //{
        //    Cancel();

        //}
        protected void ddlTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadImage();
        }

        protected void btUserCrop_OnClick(object source, EventArgs e)
        {

            // init popup data

            popupCrop.ImgID = ImageID;
            popupCrop.ImgTypeID = TypeID;
            popupCrop.ImgTargetClientID = imgMain.ClientID;
            popupCrop.TargetVirtualURLClientID = hfImgVirtualUrl.ClientID;
            popupCrop.AutoSaveImage = true;
            popupCrop.OpenPopup();

        }

    }
}