﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewProductDetailContact.ascx.cs" Inherits="_ctrl_module_product_viewProductDetailContact" %>
<%@ Register TagPrefix="SM" TagName="ProductAttribute" Src="~/_ctrl/module/product/attribute/_viewProductAttributeList.ascx"   %>

<script type="text/javascript">


    function notWatermark(value, element) {
        return (value != $(element).attr('title'));
    }

    $.validator.addMethod("notWatermark", notWatermark, "");



    $(document).ready(function () {

        $('#cProdContact').find('.wm').watermark('watermark');

    });



    function prodContactSendMail(v, id) {

        var $group = $('#' + v).find('.validationGroup');
        var isValid = true;

        $group.find(':input').each(function (i, item) {
            if (!$(item).valid())
                isValid = false;
        });

        var _text = $('#' + v).find('.CoFoTextP').val();

        if (_text == $('#' + v).find('.CoFoTextP').attr('title')) {
           // alert('Vpiši');
            return;
        }


        //alert(isValid);
        if (isValid) {
            var add = null;
            openModalLoading('Pošiljam...');
            WebServiceP.SubmitContactFormShort(id, '<%= Cust.FIRST_NAME + " " + Cust.LAST_NAME %>', _text, '<%= Cust.EMAIL %>' + ' (<%= Cust.TELEPHONE %>)', em_g_cult, em_g_lang, add, '', 'povpraševanje- <%= SM.EM.Helpers.StripHTML(Prod.NAME) %>',
            function (res) {
                closeModalLoading();
                if (res.Code == 1) {
                    setModalMsg('<%=SM.BLL.Common.Emenik.Data.PortalName()  %>', res.Data, true);
                }
                else {
                    setModalMsg('Napaka', res.Message, true);
                }


            }, onError);
        }

    }
   


</script>

<%--KONTAKTIRAJTE NAS--%>
<div id="cProdContact">

<%  if (Cust.CustomerID == Guid.Empty){%>

<% if (LANGUAGE.GetCurrentLang() == "si"){%>
    <b>Prosimo, da se pred nakupom</b> <a class=""  href="<%= SM.BLL.Common.ResolveUrl(SM.EM.Rewrite.EmenikCustomerLogin(SM.BLL.Custom.MainUserID,SM.BLL.Common.GetReturnUrl() ))%>"><span class="font_bevan">prijavite</span></a> oziroma <a class=""  href="<%= SM.BLL.Common.ResolveUrl(SM.EM.Rewrite.EmenikCustomerRegister(SM.BLL.Custom.MainUserID,SM.BLL.Common.GetReturnUrl() ))%>"><span class="font_bevan">registrirate</span></a>.
<%}else if (LANGUAGE.GetCurrentLang() == "en"){ %>  
    <%--<b>Please </b> <a class=""  href="<%= SM.BLL.Common.ResolveUrl(SM.EM.Rewrite.EmenikCustomerLogin(SM.BLL.Custom.MainUserID,SM.BLL.Common.GetReturnUrl() ))%>"><span class="font_bevan">login</span></a> or <a class=""  href="<%= SM.BLL.Common.ResolveUrl(SM.EM.Rewrite.EmenikCustomerRegister(SM.BLL.Custom.MainUserID,SM.BLL.Common.GetReturnUrl() ))%>"><span class="font_bevan">sign up</span></a> to purchase products.--%>
<%}else if (LANGUAGE.GetCurrentLang() == "it"){ %>  
    <%--<b>Per cortesia, </b> <a class=""  href="<%= SM.BLL.Common.ResolveUrl(SM.EM.Rewrite.EmenikCustomerRegister(SM.BLL.Custom.MainUserID,SM.BLL.Common.GetReturnUrl() ))%>"><span class="font_bevan">registrarsi</span></a> prima dell’acquisto.--%>
<%} %>


<%} else
    {%>

    <table class="tbl_contact_form validationGroup" border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <%--<td class="td_text td_veralign"><span>Moj predlog*:</span></td>--%>
                <td class="td_input td_nopadd"><textarea title="Vpišite želje in si naročite svoj paket, lahko pa nas pokličete na tel.: +386 (0) 40 602 222" rows="4" class="val CoFoTextP wm"   name="CoFoTextP"></textarea></td>
                <td class="td_error"></td>
            </tr>
        </tbody>
    </table>

    <div class="contact_form_btnW"><a class="btn_send" href="#" onclick="prodContactSendMail('cProdContact', '<%= Prod.PRODUCT_ID %>'); return false;"><span class="font_bevan">POŠLJI</span></a></div>
    <div class="clearing"></div>
<%} %>
</div>