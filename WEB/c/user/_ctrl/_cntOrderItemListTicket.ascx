﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_cntOrderItemListTicket.ascx.cs" Inherits="SM.UI.Controls.c_user_ctrl_cntOrderItemListTicket" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Namespace="SM.UI.Controls" TagPrefix="SM" %>

<div class="qContList">

        
        <SM:smListView ID="lvList" runat="server" InsertItemPosition="None"   >
            <LayoutTemplate>
                <table  class="tblData Table_payment" cellpadding="0" cellspacing="0">
                        <thead>
                            <tr id="headerSort" runat="server">
                                <th >Postavka</th>
                                <th >Znesek</th>
                                <th >DDV</th>
                                <th >Znesek z DDV</th>
                                <th >Podrobnosti</th>
                            </tr>
                        </thead>
                        <tbody id="itemPlaceholder" runat="server"></tbody>
                    </table>
            </LayoutTemplate>
            
            <ItemTemplate>
                <tr  class='<%#  Container.DataItemIndex % 2 == 0 ? "  " : " odd " %>'  onmouseover ="this.className='<%#  Container.DataItemIndex % 2 == 0 ? "hovRow" : "hovRowodd" %>'" onmouseout ="this.className = GetCssClass(<%#  Container.DataItemIndex %>)">
                    <td>
                        <%# Eval("ItemName")%>
                    </td>
                    <td class="tdR">
                        <%# SM.EM.Helpers.FormatPrice(Eval("Price"))%> 
                    </td>
                    <td class="tdR">
                        <%# SM.EM.Helpers.FormatPrice(Eval("Tax"))%> 
                    </td>
                    <td class="tdR">
                        <%# SM.EM.Helpers.FormatPrice(Eval("PriceTotal"))%> 
                    </td>
                    <td >
                        <%# RenderTicketLink(Eval("TicketID"))%>
                    </td>
                </tr>  
            
            </ItemTemplate>
            
            <EmptyDataTemplate>
                
                <div class="odd">
                    Naročilo je prazno
            
                </div>
                
            </EmptyDataTemplate>
        </SM:smListView>

</div>
