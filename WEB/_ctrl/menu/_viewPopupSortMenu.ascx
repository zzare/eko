﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewPopupSortMenu.ascx.cs" Inherits="SM.UI.Controls._viewPopupSortMenu" %>
<%@ Register TagPrefix="CMS" TagName="MenuSort" Src="~/_ctrl/menu/_viewSortMenu.ascx"   %>



<div id="cMenuSort" >
        <div class="modPopup_header modPopup_headerSettings">
        <h4>UREDI VRSTNI RED V MENIJU</h4>
        <p>Z miško primi podstran in jo odvleci na želeno mesto.</p>
    </div>
    <div class="modPopupDrag_main">
    <span class="modPopup_mainTitle" >Z miško primi podstran in jo odvleci na želeno mesto</span>
    <br />
    
        <CMS:MenuSort ID="objSort" runat="server" ShowSaveButton="false"/>
    </div>
    <br />
    <div class="modPopup_footer">
        <div class="buttonwrapperModal">
            <a class="lbutton lbuttonConfirm" href="#" onclick="openModalLoading('Osvežujem...');document.location.reload(true); return false" ><span>Zapri</span></a>
        </div>
    </div>

</div>