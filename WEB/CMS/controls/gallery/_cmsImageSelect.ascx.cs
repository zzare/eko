﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;


namespace SM.UI.Sandbox.Controls
{
    public partial class _cmsImageSelect : BaseControl
    {
        public delegate void OnSelectEventHandler(object sender, SM.BLL.Common.ClickIDEventArgs e);
        public event OnSelectEventHandler OnSelect;
        public List<int> GetSelectedItems { get { return objImageList.GetSelectedItems; } }
        public int PageSize = 30;
        public bool MultipleSelect = true;

        public int SelectedImageID { get; set; }
        public int ImageTypeID { get; set; }
        public int GalleryID { get; set; }
        //    public string LangID { get; set; }
        //public string SearchText { get; set; }
        //public bool? Active { get; set; }
        public int ExcludeImageID { get; set; }
        protected int _selectedImage = -1;
        



        
        protected void Page_Load(object sender, EventArgs e)
        {

            objImageList.OnEdit += new _cmsImageList.OnEditEventHandler(objImageList_OnEdit);

            // hide add selected button if single select mode
            if (!MultipleSelect)
            {
                btAddSelectedImage.Visible = false;
            }

        }

        void objImageList_OnEdit(object sender, SM.BLL.Common.ClickIDEventArgs e)
        {
            OnSelect(this, e);
            mpeSelect.Hide();
        }




        public void OpenPopup()
        {
            // init data
            objImageList.GalleryID = GalleryID;
            objImageList.ImageTypeID = ImageTypeID;
            objImageList.ExcludeImageID = ExcludeImageID ;
            objImageList.MultipleSelect = MultipleSelect;
            objImageList.ShowCheckboxColumn = MultipleSelect;
            

            objImageList.BindData();

            mpeSelect.Show();
        }

        protected void btAddSelectedImage_Click(object sender, EventArgs e)
        {

            SM.BLL.Common.ClickIDEventArgs a =  new SM.BLL.Common.ClickIDEventArgs();

            OnSelect(this, a);
            mpeSelect.Hide();
        }

    }

}