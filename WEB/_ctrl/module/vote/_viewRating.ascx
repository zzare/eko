﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewRating.ascx.cs" Inherits="_ctrl_module_product_attribute_viewProductAttribute" %>


    <asp:ListView ID="lvList" runat="server" >
        <LayoutTemplate>
            
                <asp:PlaceHolder ID="itemPlaceholder" runat="server"/>

        </LayoutTemplate>
        
        <ItemTemplate>
            <img alt='<%# RenderAvg() %>' src='<%# RenderRatingSrc(Container.DataItemIndex) %>' />
        </ItemTemplate>
    </asp:ListView>


