﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Net;
using SM.UI.Controls;

/// <summary>
/// Summary description for BasePage_USER_CUSTOMER
/// </summary>
namespace SM.EM.UI
{

    public class BasePage_USER_CUSTOMER : BasePage_EMENIK
    {

        // emenik user data


        protected override void OnPreInit(EventArgs e)
        {
            //this.ForceMainPageDomain = true;
            base.OnPreInit(e);

            if (!User.Identity.IsAuthenticated) {
                RequestLogin();
            }
            
        }


   }
}