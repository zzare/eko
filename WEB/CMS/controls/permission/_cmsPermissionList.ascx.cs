﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;


namespace SM.UI.Controls
{
    public partial class _ctrl_permission_cmsPermissionList : BaseControl 
    {
        public delegate void OnEditEventHandler(object sender, SM.BLL.Common.Args .IDguidEventArgs e);
        public event OnEditEventHandler OnEdit;
        public event EventHandler  OnDelete;
        public string OnEditTarget = "_self";
        public bool ShowCheckboxColumn { get; set; }
        public bool ShowManageColumn { get; set; }
        public bool ShowEditColumn { get; set; }
        public int PageSize = 20;
        public bool DeleteAutomatically = true;
        public bool MultipleSelect = true;

        public string SearchText { get; set; }
        public bool? Active { get; set; }
        public string RoleID { get; set; }

        public Guid ExcludeUserID { get; set; }
        public bool IsAdmin { get; set; }
        public List<EM_USER> dsList;

        public Guid UID
        {
            get
            {
                Guid res = Guid.Empty;
                if (ViewState["UID"] == null) return res;
                if (!SM.EM.Helpers.IsGUID(ViewState["UID"].ToString())) return res;

                return new Guid(ViewState["UID"].ToString());
            }
            set
            {
                ViewState["UID"] = value;
            }
        }

        
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void LoadData() { 
        
        
        
        
        }

        public void BindData()
        {
            PermissionRep perr = new PermissionRep();

            lvList.DataSource = perr.GetPermissionByUser(UID);
            lvList.DataBind();

        }



        protected void lvList_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            if (e.CommandName == "del") {
                PermissionRep perr = new PermissionRep();
                perr.Delete(int.Parse(e.CommandArgument.ToString()));
                perr.Save();
                OnDelete(this, new EventArgs ());        
            }

        }






}


}