﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
namespace SM.BLL
{


    public class Custom
    {

        public static Guid MainUserID = new Guid("953CCF41-FB4D-44E8-A6F4-0BA3ECD6159E");
        public static string ForceMainUserPageName = "eko";

        public static decimal MinimumOrderSum = 50m;




        //public class AnimalStatus {
        //    public const short NOT_SET = 0;
        //    public const short CAKA_LASTNIKA = 1;
        //    public const short V_KARANTENI = 2;
        //    public const short PRIPRAVLJEN_NA_ODDAJO = 3;
        //    public const short ODDAN = 4;
        //    public const short EVTANAZIRAN = 5;
        //    public const short POGIN = 6;
        //    public const short DRUGO = 7;


        //}

        public class Settings
        {
            public static int NovostiDaysMargin = -7;

            public static int SmpNovosti()
            {
                int ret = -1;
                int.TryParse(ConfigurationManager  .AppSettings["SmpNovosti"], out ret);
                return ret;
            }
            public static int SmpKlepetalnica()
            {
                int ret = -1;
                int.TryParse(ConfigurationManager.AppSettings["SmpKlepetalnica"], out ret);
                return ret;
            }
        }

        

        public class Url {

            public static string LoginLanding ="~/prijava";

            public static string RegisterCustomer = "~/registriraj";
        
        
        }

        public class Permission {
            public static bool CanAdminCustomers() {
                return true;
            }
        
        }



        public class Smap
        {
            public const int ROOT_MAIN = 1432;
        }


        public class Rewrite
        {

            //public static string PortalShoppingOrders(string pageName)
            //{
            //    string url;
            //    string path = "ba-orders";

            //    if (SM.EM.Rewrite.Helpers.IsDefaultDomain() && !SM.BLL.Common.Emenik.Data.EnableSubDomainUsers() && !SM.BLL.Common.Emenik.Data.CustomDomainsEnabled())
            //    {
            //        url = "~/" + pageName + "/" + path + ".aspx";
            //    }
            //    else
            //        url = "~/" + path + "";
            //    return url;
            //}



        }


        //public class Render {


        //    //public static string AnimalVelikost(short? id)
        //    //{

        //    //    string ret = "";
        //    //    if (id == null)
        //    //        return ret;

        //    //    switch (id)
        //    //    {
        //    //        case Custom.AnimalVelikost.MAJHEN :
        //    //            ret = "Majhen";
        //    //            break;
        //    //        case Custom.AnimalVelikost.SREDNJE_VELIK:
        //    //            ret = "Srednje velik";
        //    //            break;
        //    //        case Custom.AnimalVelikost.VELIK:
        //    //            ret = "Velik";
        //    //            break;
        //    //    }
        //    //    return ret;
        //    //}



        //}







        //public static class CustomExtensions
        //{

        //    public static short GetCurrentAnimalStatus(this EM_USER animal)
        //    {
        //        // if OWNER did not show up after DAYS_FOR_OWNER days
        //        if (animal.AnimalStatus == SM.BLL.Custom.AnimalStatus.CAKA_LASTNIKA && animal.DateArrival < DateTime.Now.AddDays( - Custom.Common.DAYS_FOR_OWNER)  )
        //            return SM.BLL.Custom.AnimalStatus.V_KARANTENI ;

        //        return animal.AnimalStatus;
        //    }


        //}

    }
}