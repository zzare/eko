﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using SM.EM.UI.Controls;

public partial class _em_settings__ctrl_step_language : BaseWizardStep {

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        //Description = "Izberite jezike, v katerih boste imeli svojo spletno predstavitev.";
        //Description += "<br/> 1. V primeru da želite imeti spletno predstavitev v več jezikih, kliknite na \"omogoči večjezičnost\"";
        //Description += "<br/> 2. Novo jezikovno različico dodate tako da izberete želeno jezikovno različico v sekciji Dodaj nov jezik in kliknete gumb Dodaj jezik";
        //Description += "<br/> 3. Privzeta jezikovna različica je različica vaše spletne predstavitve, ki je privzeta ko obiskovalec pride na vašo spletno predstavitev. Med vsemi jezikovnimi različicami pa lahko obiskovalec poljubno izbira. (npr: če je privzeta jezikovna različica Slovensko to pomeni da bo obiskovalec vašo spletno predstavitev videl v slovenskem jeziku)";
        
        Description = "Spletna stran ima lahko več jezikov. Če želiš dodati nov jezik potem obkljukaj \"Omogoči večjezičnost\". Potem enostavno izberi jezik in klikni na \"Dodaj jezik\". ";
        Description += "V kolikor trenutno nisi prepričan ali želiš dodaten jezik ali pa morda še nimaš pripravljenih prevodov, lahko ta korak preskočiš in se nanj vrneš s klikom na gumb \"Nastavitve spletne strani\", ko boš pripravljen.";
        
        
        //Description = "<ul><li>";
        //Description += "Drugi korak je namenjen izbiri jezika. " + "<a href=\"" + Page.ResolveUrl("~/default.aspx") + "\">1dva3.si</a> " + "vam ponuja več jezikovnih različic. " + "<i>\"Vendar pozor, za prevode boš moral poskrbeti sam.\"</i>" + " Če ne želiš imeti spletno stran v večih jezikih enostavno preskoči ta korak. ";
        //Description += "</li><li>";
        //Description += "OMOGOČI VEČJEZIČNOST: Če želiš dodati nov jezik potem obkljukaj \"omogoči večjezičnost\". To bo odprlo možnost dodajanja novega jezika. Potem enostavno izberi jezik in klikni na \"dodaj jezik\".";
        //Description += "</li><li>";
        //Description += "PRIVZETI JEZIK: Privzeti jezik (jezik v katerem se odpre tvoja spletna stran) je slovenščina. To lahko poljubno spremeniš s klikom na gumb \"nastavi privzet\" pri izbranem jeziku.";
        //Description += "</li><li>";
        //Description += "V kolikor trenutno nisi prepričani ali želiš dodaten jezik ali pa morda še nimaš pripravljenih prevodov, lahko ta korak preskočiš in se nanj vrneš s klikom na gumb \"Nastavitve\", ko boš pripravljen.";
        //Description += "</li></ul>";
        
        ShortDesc = "Uredi jezikovne različice na spletni strani";



        StepID = 5;
        AllwaysHideSaveButton = true;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public override void LoadData()
    {

        // load data
        lvList.DataSource = CULTURE.GetUserCultures(ParentPageEmenik.em_user.UserId);
        lvList.DataBind();

        ddlCulture.Items.Clear();
        ddlCulture.DataSource = CULTURE.GetUserCulturesInvert(ParentPageEmenik.em_user.UserId);
        ddlCulture.DataTextField = "Desc";
        ddlCulture.DataValueField = "LCID";
        ddlCulture.DataBind();
        if (ddlCulture.Items.Count == 0)
        {
            phAdd.Visible  = false;
        }
        else {
            phAdd.Visible = true;
        }


        // set checkbox
        cbMultilingual.Checked = ParentPageEmenik.em_user.IS_MULTILINGUAL;

    }


    public override bool SaveData(int step)
    {
        UpdateUserStep(step);


        return true;
        
    }

    public override bool Validate()
    {
        return true;
            
    }



    protected void btAdd_OnClick(object sender, EventArgs e) {

        // if is demo, can't save
        if (!CheckCanSave())
            return;


        int lcid = -1;

        if (! int.TryParse(ddlCulture.SelectedValue , out lcid )) return;

        CULTURE.InsertUserCulture(ParentPageEmenik.em_user.UserId, lcid, false, false);

        List<SITEMAP> smaps = SITEMAP.GetSiteMapsByUserName(ParentPageEmenik.em_user.USERNAME ).ToList();
        CULTURE cult = CULTURE.GetCultureByID(lcid);
        foreach (SITEMAP smap in smaps) {
            MENU.ChangeSitemapLang(smap.SMAP_ID, smap.SMAP_TITLE, cult.LANG_ID, false);
        }
        
  
        // refresh list
        LoadData();

        // refresh language control on master page
        ParentPageEmenik.mp_em.LoadData();

    }

    protected void lvList_OnItemCommand(object sender, ListViewCommandEventArgs e)
    {

        if (e.CommandName == "del")
        {
            int lcid = int.Parse(e.CommandArgument.ToString());
            CULTURE.DeleteUserCulture(ParentPageEmenik.em_user.UserId, lcid);

            LoadData();

            // refresh language control on master page
            ParentPageEmenik.mp_em.LoadData();

        }
        else if (e.CommandName == "default")
        {
            int lcid = int.Parse(e.CommandArgument.ToString());
            CULTURE.SetDefaultUserCulture(ParentPageEmenik.em_user.UserId, lcid);

            LoadData();
        }

    }

    protected void cbActive_CheckedChanged(object sender, EventArgs e)
    {
        SM.UI.Controls.CheckBoxID cb = (SM.UI.Controls.CheckBoxID)sender;

        if (cb == null) return;

        // set default culture
        int lcid = cb.IDint ;
        CULTURE.SetDefaultUserCulture(ParentPageEmenik.em_user.UserId, lcid);

        LoadData();

    }


    protected void cbMultilingual_CheckedChanged(object sender, EventArgs e)
    {
        // if is demo, can't save
        if (!CheckCanSave())
            return;


        // update user data
        EM_USER.UpdateMultiLingual(ParentPageEmenik.em_user.UserId, cbMultilingual.Checked);
        // refresh language control on master page
        ParentPageEmenik.mp_em.LoadData();
    }
}
