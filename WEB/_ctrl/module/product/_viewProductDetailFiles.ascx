﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewProductDetailFiles.ascx.cs" Inherits="_ctrl_module_product_viewProductDetailFiles" %>


<script type="text/javascript" >
    $(document).ready(function() {

        $(".prodFileList a").addClass("file"); // default class for files

        $(".prodFileList a[name$=pdf]").removeClass().addClass("pdf");
        $(".prodFileList a[name$=xls]").removeClass().addClass("xls");
        $(".prodFileList a[name$=zip]").removeClass().addClass("zip");

        $(".prodFileList a[name$=doc]").removeClass().addClass("doc");
        $(".prodFileList a[name$=docx]").removeClass().addClass("doc");
    
});
</script>

    <div  class="prodFileList"  >
        <asp:ListView ID="lvFileList" runat="server"  InsertItemPosition="None" >
            <LayoutTemplate>
                <asp:PlaceHolder id="itemPlaceholder" runat="server"></asp:PlaceHolder>
            </LayoutTemplate>
                    
            <ItemTemplate>

                    <a name='<%# Eval("Name") %>' style="display:block;" target="_blank" href='<%#  SM.BLL.Common.ResolveUrl("~/") + Eval("FileID").ToString() + ".secFile" %>' ><%# Eval("Name")%></a>

                   <%-- <br />--%>
                
            </ItemTemplate>
        </asp:ListView>	 	        
    </div>