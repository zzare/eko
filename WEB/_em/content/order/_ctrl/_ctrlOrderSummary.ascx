﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_ctrlOrderSummary.ascx.cs" Inherits="_em_content_order__ctrl__ctrlOrderSummary" %>
<%@ Register Namespace="SM.EM.UI.Controls" TagPrefix="EM"  %>
<%@ Register TagPrefix="SM" TagName="OrderDetail" Src="~/_em/content/order/_ctrl/_ctrlOrderDetail.ascx"    %>

<asp:PlaceHolder ID="phStatus" runat="server">
    <h3>Status naročila</h3>            

    <%--Naročilo je --%><%= SHOPPING_ORDER.Common.RenderOrderStatus(Order.OrderStatus) %>
    <br />
</asp:PlaceHolder>

<h3>Naročnik</h3>
<%= SHOPPING_ORDER.Common.RenderShoppingOrderUserData(Order)  %>
<br />
     
<h3>Način plačila</h3>
<%= RenderPayment()  %>

<h3>Način dostave</h3>
<%--<%= RenderShipping()  %>--%>
<%= SHOPPING_ORDER.Common.RenderShoppingOrderShippingDetail (Order)  %>


<h3>Vsebina naročila</h3>
<SM:OrderDetail ID="objOrderDetail" runat="server" />

