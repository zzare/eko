﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using System.Net;
using System.Text;
using System.Net.Sockets;
using System.IO;


namespace SM.EM
{
    /// <summary>
    /// Helping static functions
    /// </summary>
    public static class Helpers
    {
        public static string CutString(string str, int len) {
            return CutString(str, len, "");
        }
        public static string CutString(string str, int len, string postfix) {
            if (len > 0 && str.Length > len)
            {
                int end = 0;
                str = str.Substring(0, len - postfix.Length );
                end = str.LastIndexOf(" ");
                if (end < 0) end = str.Length;
                str = str.Substring(0, end);
                str = str + postfix;
            }

            return str;
        }
        public static  string StripHTML(string htmlString)
        {
            return StripHTML(htmlString, false);
        }


        public static  string StripHTML(string htmlString, bool removeScript)
        {
            string lOut = htmlString;

            string pattern = @"<[^>]+>";

            if (removeScript )
                lOut = Regex.Replace(htmlString, "<script.*?</script>", "", RegexOptions.Singleline | RegexOptions.IgnoreCase);


            lOut = Regex.Replace(lOut, pattern, string.Empty);

            lOut = lOut.Replace("<", "&lt;");
            lOut = lOut.Replace(">", "&gt;");
            return lOut;
        }
        // CMS Get URL to be rewrited
        public static string GetUrlCMSsmardt(int smpID, string title){
            return "~/cms/smardtCMS/" + smpID.ToString() + "/" + title.Replace(" ", "-").ToLower() + ".aspx";
        }
        public static string FormatDate(object date)
        {
            DateTime d = Convert.ToDateTime(date);
            if (d == DateTime.MinValue) return "";
            return d.ToShortDateString();
        }
        public static string FormatDateLong(object date)
        {
            DateTime d = Convert.ToDateTime(date);
            if (d == DateTime.MinValue) return "";
            return d.ToString();
        }
        
        public static string FormatDateMedium(object date)
        {
            DateTime d = Convert.ToDateTime(date);
            if (d == DateTime.MinValue) return "";
            return string.Format("{0:dd.MM.yyyy ob HH:mm}", d);
        }

        public class Date {

            public static DateTime ReValidateSqlDateTime(DateTime date) {

                DateTime ret = date;
                if (date <= (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue)
                    ret = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;

                if (date >= (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue)
                    ret = (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue;

                return ret;
            }
        
        }




        public static string GetQueryString(string key){
            string ret = "";
            if (HttpContext.Current == null )
                return ret;

            if (HttpContext.Current.Request.QueryString [key] == null)
                return null;

            ret = HttpContext.Current.Request.QueryString[key];
            if (ret.Contains(","))
            {
                ret = ret.Split(',')[0];
            }

            return ret;
        }


        /// <summary>
        /// Converts the input plain-text to HTML version, replacing carriage returns
        /// and spaces with <br /> and &nbsp;
        /// </summary>
        public static string ConvertToHtml(string content)
        {
            content = HttpUtility.HtmlEncode(content);
            content = content.Replace("  ", "&nbsp;&nbsp;").Replace(
               "\t", "&nbsp;&nbsp;&nbsp;").Replace("\n", "<br>");
            return content;
        }

        public static void AddLinkedStyleSheet(Page page, string styleSheetUrl)
        {
            AddLinkedStyleSheet(page, styleSheetUrl, true);
        }

        public static void AddLinkedStyleSheet(Page page, string styleSheetUrl, bool addVersion)
        {
            HtmlLink link = new HtmlLink();
            link.Attributes["rel"] = "stylesheet";
            link.Attributes["type"] = "text/css";
            link.Href = page.ResolveUrl(styleSheetUrl);
            if (addVersion )
                link.Href += "?" + Helpers.GetCssVersion();


            page.Header.Controls.Add(link);
        }

        public static void AddLinkedStyleSheet(MasterPage page, string styleSheetUrl)
        {
            AddLinkedStyleSheet(page.Page, styleSheetUrl);
        }

        public static void RegisterScriptIncludeAJAX(string url, Page pg, string key, bool addVersion)
        {
            if (addVersion )
                url = url + "?" + Helpers.GetJavascriptVersion();
            
            RegisterScriptIncludeAJAX(url, pg, key);
        
        }

        public static  void RegisterScriptIncludeAJAX(string url, Page pg, string key)
        {
            
            ScriptManager sm = ScriptManager.GetCurrent(pg);
            if (sm == null || !sm.IsInAsyncPostBack)
                ScriptManager.RegisterClientScriptInclude(pg, typeof(Page), key, url);
            else
                pg.ClientScript.RegisterClientScriptInclude(key, url);
        }
        public static void AddJavaScript(Page page, string javascriptUrl){
            HtmlGenericControl myJs = new HtmlGenericControl();
            myJs.TagName = "script";
            myJs.Attributes.Add("type", "text/javascript");
            myJs.Attributes.Add("language", "javascript"); //don't need it usually but for cross browser.
            myJs.Attributes.Add("src", page.ResolveUrl(javascriptUrl));
            page.Header.Controls.Add(myJs);
        }
        // get raw url with set new query string (replace old one variable if exists)
        public static string GetNewQueryStringRawUrl(string qs, string val) {
            String currurl = HttpContext.Current.Request.RawUrl;
            String querystring = null;
            
            if (string.IsNullOrEmpty(qs))
                return currurl;

            // If query string variables not set, add new one
            int iqs = currurl.IndexOf('?');
            if (iqs == -1){
                return  currurl + "?" + qs + "=" + val;                
            }
            // If query string variables exist, put them in a string.
            else if (HttpContext.Current.Request.QueryString[qs] == null)
                return currurl + "&" + qs + "=" + val;        
            else if (iqs >= 0)
            {
                querystring = (iqs < currurl.Length - 1) ? currurl.Substring(iqs + 1) : String.Empty;
            }

            // Parse the query string variables into a NameValueCollection.
            NameValueCollection qscoll = HttpUtility.ParseQueryString(querystring);

            foreach (String s in qscoll.AllKeys){
                if (s == qs) {
                    qscoll[s] = val;
                }
            }

            return currurl.Substring(0,iqs+1) +  qscoll.ToString(); 
        }
        // checkes is string is GUID
        public static bool IsGUID(string expression)
        {
            if (expression != null)
            {
                Regex guidRegEx = new Regex(@"^(\{{0,1}([0-9a-fA-F]){8}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){12}\}{0,1})$");

                return guidRegEx.IsMatch(expression);
            }
            return false;
        }

        // add css version
        public static string GetCssVersion(){
            if (!string.IsNullOrEmpty(ConfigurationSettings.AppSettings["CSSVersion"] ))
                return ConfigurationSettings.AppSettings["CSSVersion"].ToString();
            
            return "";
        }
        public static string GetJavascriptVersion()
        {
            if (!string.IsNullOrEmpty(ConfigurationSettings.AppSettings["JSVersion"]))
                return ConfigurationSettings.AppSettings["JSVersion"].ToString();

            return "";
        }



        public static string FormatPrice(object price)
        {
            // return string.Format("{0:C}", Convert.ToDecimal(price));
//            return Convert.ToDecimal(price).ToString("N") + " " + Globals.Settings.ST.CurrencyCode;
            return Convert.ToDecimal(price).ToString("N") + " " + "€";
        }
        public static string FormatPriceEdit(object price)
        {
            // return string.Format("{0:C}", Convert.ToDecimal(price));
            //            return Convert.ToDecimal(price).ToString("N") + " " + Globals.Settings.ST.CurrencyCode;
            return Convert.ToDecimal(price).ToString("N") ;
        }
        public static string FormatPriceSaving(object price, object discount)
        {
            decimal lPrice = Convert.ToDecimal(price);
            decimal lDiscount = Convert.ToDecimal(discount);

            return Convert.ToDecimal((lPrice * lDiscount) / 100).ToString("N") + " " + "€";
        }
        public static string FormatPriceFinal(object price, object discount) 
        {
            decimal lPrice = Convert.ToDecimal(price);
            decimal lDiscount = Convert.ToDecimal(discount);

            return Convert.ToDecimal(lPrice - ((lPrice * lDiscount) / 100)).ToString("N") + " " + "€";
        }
        public static string FormatDiscount(object discount)
        {
            return Convert.ToDecimal(discount).ToString("N") + " " + "%";
        }

        // check if domain is active - if IP points on current application IP
        public static bool IsDomainActive(string domain)
        {

            IPHostEntry he;
            try
            {
                he = Dns.GetHostEntry(domain);
            }
            catch (Exception ex)
            {
                return false;
            }
            if (he == null) return false;

            foreach (IPAddress ip in he.AddressList)
            {
                if (ip.ToString() == SM.BLL.Common.Emenik.Data.PortalIP())
                {
                    return true;
                }
            }

            return false;
        }

        // format domain : remove http, www, ...
        public static string FormatDomain(string domain)
        {
            // format domain
            string d = domain.Trim();
            d = d.Replace("http://", "");
            d = d.Replace("www.", "");
            d = d.Trim();

            if (d.Contains("/"))
                d = d.Substring(0, d.IndexOf("/"));

            if (d.Contains(SM.BLL.Common.Emenik.Data.PortalHostDomain()))
                d = d.Substring(0, d.IndexOf(SM.BLL.Common.Emenik.Data.PortalHostDomain()));

            return d;


        }


        /// <summary>
        /// Encodes a string to be represented as a string literal. The format
        /// is essentially a JSON string.
        /// 
        /// The string returned includes outer quotes 
        /// Example Output: "Hello \"Rick\"!\r\nRock on"
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string EncodeJsString(string s)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("\"");
            foreach (char c in s)
            {
                switch (c)
                {
                    case '\"':
                        sb.Append("\\\"");
                        break;
                    case '\\':
                        sb.Append("\\\\");
                        break;
                    case '\b':
                        sb.Append("\\b");
                        break;
                    case '\f':
                        sb.Append("\\f");
                        break;
                    case '\n':
                        sb.Append("\\n");
                        break;
                    case '\r':
                        sb.Append("\\r");
                        break;
                    case '\t':
                        sb.Append("\\t");
                        break;
                    default:
                        int i = (int)c;
                        if (i < 32 || i > 127)
                        {
                            sb.AppendFormat("\\u{0:X04}", i);
                        }
                        else
                        {
                            sb.Append(c);
                        }
                        break;
                }
            }
            sb.Append("\"");

            return sb.ToString();
        }




        public static bool IsValidEmail(string mail, bool checkDomainName)
        {

            if (mail != null)
            {
                mail = mail.Trim();

                Regex guidRegEx = new Regex(@"^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$");

                bool IsOK = guidRegEx.IsMatch(mail);

                if (!checkDomainName) return IsOK;

                string domain = mail.Substring(mail.IndexOf("@") + 1);

                bool isValid =  ExistsDomain(domain, true);
                if (!isValid)
                    return false;

                
                return isValid;

            }
            return false;
        }

        public static bool ExistsDomain(string domain, bool checkWWW){
            bool IsOk = false;
            domain = domain.Trim();

            string[] validDomains = {"gmail.com", "hotmail.com", "guest.arnes.si", "yahoo.com", "uni-lj.si" };

            if (validDomains.Contains(domain))
                return true;


            string ip = "";
                try
                {

                    ip = Dns.Resolve(domain).AddressList[0].ToString();
                    IsOk = true;
                }
                catch (Exception)
                {
                    if (!checkWWW || domain.StartsWith("www.") )
                        return false;

                    // check www subdomain
                    try
                    {

                        ip = Dns.Resolve("www." + domain).AddressList[0].ToString();
                        IsOk = true;
                    }
                    catch (Exception)
                    {
                        return false;
                    }


                }
                return IsOk;
        
        }

        public static string[] GetEmailListAsArray(string emailList)
        {
            string[] sep = { ",", "\n" };
            return emailList.Split(sep, StringSplitOptions.RemoveEmptyEntries);

        }


        /// <summary>
        /// Finds a Control recursively. Note finds the first match and exists
        /// </summary>
        /// <param name="ContainerCtl"></param>
        /// <param name="IdToFind"></param>
        /// <returns></returns>
        public static Control FindControlRecursive(Control Root, string Id)
        {
            if (Root.ID == Id)
                return Root;

            foreach (Control Ctl in Root.Controls)
            {
                Control FoundCtl = FindControlRecursive(Ctl, Id);
                if (FoundCtl != null)
                    return FoundCtl;
            }

            return null;
        }



        public static string WhoIs(string hostname)
        {
                XElement WhoisResult = XElement.Load("http://www.webservicex.net/whois.asmx/GetWhoIS?HostName=" + hostname);
                return WhoisResult.ToString();

                
        }

        public static bool IsDomainTaken(string hostname)
        {
            string ret = WhoIs(hostname).ToUpper();

            if (ret.Contains("NAME SERVER")) return true;
            if (ret.Contains("NAMESERVER")) return true;

            return false;
        }


        public static  class Treeview {
            public static TreeNode GetRootNodeForNode(TreeNode node, TreeView tv)
            {
                if (node.Parent == null) return node;
                else return GetRootNodeForNode(node.Parent, tv);
            }
        
        }
    }
}