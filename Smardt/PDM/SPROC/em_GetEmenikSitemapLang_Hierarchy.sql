IF EXISTS (SELECT * FROM sysobjects WHERE name='em_GetEmenikSitemapLang_Hierarchy' and type='P')
DROP Procedure em_GetEmenikSitemapLang_Hierarchy

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		zare
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].em_GetEmenikSitemapLang_Hierarchy
	-- Add the parameters for the stored procedure here

	@PageName         nvarchar(256), 
	@UICulture             nvarchar(5)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	WITH Hierarchy(SMAP_ID, PARENT, MENU_ID, SMAP_TITLE, WWW_ROLES, SMAP_ORDER, THEME, CUSTOM_CLASS, SML_TITLE  , SML_DESC , HLevel)
	AS
	(
		SELECT S.SMAP_ID, S.PARENT, S.MENU_ID, S.SMAP_TITLE, S.WWW_ROLES, S.SMAP_ORDER, S.THEME, S.CUSTOM_CLASS, CAST('root' AS nvarchar(256)) as SML_TITLE , CAST('' AS nvarchar(256)) AS SML_DESC, 0 as HLevel
		  FROM SITEMAP S, MENU M, EM_USER U
		 WHERE S.MENU_ID = M.MENU_ID AND
				M.USERNAME = U.USERNAME AND
				U.PAGE_NAME = @PageName AND
				S.PARENT = -1 AND
				S.SMAP_ACTIVE = 1 AND
				S.WWW_VISIBLE = 1

	UNION ALL
		SELECT S.SMAP_ID, S.PARENT, S.MENU_ID, S.SMAP_TITLE, S.WWW_ROLES, S.SMAP_ORDER, S.THEME, S.CUSTOM_CLASS, SML.SML_TITLE  , SML.SML_DESC ,  H.HLevel+1
		  FROM SITEMAP S, Hierarchy H,
				SITEMAP_LANG  SML, CULTURE C 
		  WHERE S.PARENT = H.SMAP_ID AND
				S.SMAP_ID = SML.SMAP_ID AND 
				SML.LANG_ID = C.LANG_ID AND 
				C.UI_CULTURE = @UICulture AND 
				SML.SML_ACTIVE = 1 AND	
			    S.SMAP_ACTIVE = 1 AND
				S.WWW_VISIBLE = 1
	)
	SELECT * FROM Hierarchy	
	
	
	END
GO
