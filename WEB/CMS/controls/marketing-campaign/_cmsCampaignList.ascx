﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_cmsCampaignList.ascx.cs" Inherits="SM.UI.Controls._cmsCampaigntList" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Namespace="SM.UI.Controls" TagPrefix="SM" %>

<div class="qContList">

        <asp:LinqDataSource ID="ldsList" runat="server" OnSelecting="ldsList_OnSelecting" >
        </asp:LinqDataSource>
        
        <SM:smListView ID="lvList" runat="server" DataSourceID="ldsList" 
             InsertItemPosition="None"  onitemcommand="lvList_ItemCommand" >
            <LayoutTemplate>
                <table  class="tblData" cellpadding="0" cellspacing="0">
                        <thead>
                            <tr id="headerSort" runat="server">
                                <th ><asp:LinkButton ID="LinkButton2" CommandName="sort" CommandArgument="CampaignID" runat="server">CampaignID</asp:LinkButton></th>
                                <th ><asp:LinkButton ID="LinkButton8" CommandName="sort" CommandArgument="Name" runat="server">Name</asp:LinkButton></th>
                                <th ><asp:LinkButton ID="LinkButton1" CommandName="sort" CommandArgument="Desc" runat="server">Desc</asp:LinkButton></th>
                                <th ><asp:LinkButton ID="LinkButton3" CommandName="sort" CommandArgument="DateCreated" runat="server">DateCreated</asp:LinkButton></th>
                                <th ><asp:LinkButton ID="LinkButton6" CommandName="sort" CommandArgument="CreatedBy" runat="server">CreatedBy</asp:LinkButton></th>
                                <th width="100px" colspan="2" runat="server"  id="headManage" >Uredi</th>
                            </tr>
                        </thead>
                        <tbody id="itemPlaceholder" runat="server"></tbody>
                        <tfoot>
                            <tr>
                                <th style="text-align:right" colspan="8"  >
                                    <asp:DataPager runat="server" ID="DataPager" >
                                        <Fields>
                                            <asp:NextPreviousPagerField  />
                                            <asp:NumericPagerField ButtonCount="5" />
                                        </Fields>
                                    </asp:DataPager>
                                </th>
                            </tr>
                        </tfoot>
                    </table>
            </LayoutTemplate>
            
            <ItemTemplate>
                <tr  class='<%#  Container.DataItemIndex % 2 == 0 ? "  " : " odd " %>'  onmouseover ="this.className='<%#  Container.DataItemIndex % 2 == 0 ? "hovRow" : "hovRowodd" %>'" onmouseout ="this.className = GetCssClass(<%#  Container.DataItemIndex %>)">
                    <td>
                        <%# Eval("CampaignID")%>
                    </td>
                    <td>
                        <%# Eval("Name")%>
                    </td>
                    <td>
                        <%# Eval("Desc")%>
                    </td>
                    <td>
                        <%# SM.EM.Helpers.FormatDate(Eval("DateCreated"))%>
                    </td>
                    <td>
                        <%# Eval("CreatedBy")%>
                    </td>
                    <td width="50px" runat="server"  visible='<%# ShowManageColumn && ShowEditColumn %>'>
                        <a target='<%# OnEditTarget %>' href='<%# Page.ResolveUrl( MARKETING_CAMPAIGN.Common.GetCampaignDetailsCMSHref(new Guid(Eval("CampaignID").ToString()))) %>' title="edit" >uredi</a>
                    </td>
                </tr>  
            
            </ItemTemplate>
            
            <EmptyDataTemplate>
                
                <div class="odd">
                    no campaign
            
                </div>
                
            </EmptyDataTemplate>
        </SM:smListView>

</div>
