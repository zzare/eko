﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;


namespace SM.UI.Controls
{
    public partial class _cmsPaymentList : BaseControl 
    {
        public delegate void OnEditEventHandler(object sender, SM.BLL.Common.Args .IDguidEventArgs e);
        public event OnEditEventHandler OnEdit;
        public event OnEditEventHandler OnDelete;
        public string OnEditTarget = "_self";
        public bool ShowCheckboxColumn { get; set; }
        public bool ShowManageColumn { get; set; }
        public bool ShowEditColumn { get; set; }
        public int PageSize = 20;
        public bool DeleteAutomatically = true;

        public string SearchText { get; set; }
        public bool IsAdmin { get; set; }
        public Guid UserId { get; set; }

        protected List<int> _filterType;
        public List<int> FilterType
        {
            get
            {
                if (_filterType == null)
                    _filterType = new List<int>();

                return _filterType;
            }

        }

        
        protected void Page_Load(object sender, EventArgs e)
        {

        }





        public void BindData()
        {
            eMenikDataContext db = new eMenikDataContext();
            var list = (from l in db.PAYMENTs  select l);

            if (UserId != Guid.Empty)
            {
                list = list.Where(w => w.UserId == UserId);
            }
            else 
                return;

            // filter by type (exclude all types)
            if (FilterType.Count > 0) {
                foreach (int type in FilterType) {
                    int t = type;
                    list = list.Where(w => w.Type != t);                               
                }
            }


            lvList.DataSource = list.OrderByDescending(o=>o.DatePayed);
            lvList.DataBind();

        }


        protected string RenderType(object data) { 
            
            PAYMENT p = (PAYMENT )data;
        
        
            string user = " ";
            string type = "";
            // bonus
            if (p.Type == PAYMENT.Common.PaymentType.Bonus ){
                type = PAYMENT.Common.RenderPaymentType(p.Type);

                if (p.RefID != null){
                    EM_USER usr = EM_USER.GetUserByID(p.RefID.Value  );
                    user += "[ " +  usr.PAGE_NAME + " ]"; 
                }
            } // bonus affiliate
            else if (p.Type == PAYMENT.Common.PaymentType.BonusAffiliate || p.Type == PAYMENT.Common.PaymentType.BonusMarketing)
            {
                type = PAYMENT.Common.RenderPaymentType(p.Type);

                if (p.RefID != null){
                    EM_USER usr = EM_USER.GetUserByID(p.RefID.Value);
                    if (usr != null)
                    {
                        //MARKETING_CAMPAIGN c = MARKETING_CAMPAIGN.GetCampaignByID(p.RefID.Value);
                        user += "[ " + usr.USERNAME  + " ]";
                    }
                }
            
            
            }
            // order payment
            //else if (p.Type == PAYMENT.Common.PaymentType.Payment && p.RefID != null )
            //{
            //    ORDER ord = ORDER.GetOrderByID(p.RefID.Value );
            //    if (ord != null)
            //    {
            //        type = "naročilo:";
            //        user = string.Format("<a href=\"{0}\" >{1}</a>", Page.ResolveUrl( ORDER.Common.GetOrderDetailsHref(p.RefID.Value)), ORDER.Common.RenderOrderRef(ord.OrderRefID));
            //    }
            //}

            return type + user;
        }
    }

}