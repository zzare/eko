﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

/// <summary>
/// Summary description for Caching
/// </summary>
namespace SM.EM
{

    public class Caching
    {
        public static string GetMenuCacheKey(int menu)
        {
            string key = "m-" + menu.ToString() + "_" + "l-" + LANGUAGE.GetCurrentCulture();
            return key;
        }

        public static void RemoveMenuCache(int menu)
        {
            SM.EM.BLL.BizObject.PurgeCacheItems("m-" + menu.ToString());

        }
        public static void RemoveMenuCache()
        {
            SM.EM.BLL.BizObject.PurgeCacheItems("m-");

        }

        public static string GetEmenikUserMenuCacheKey(string pageName)
        {

            return GetEmenikUserMenuCacheKey(pageName , -1);
        }
        public static string GetEmenikUserMenuCacheKey(string pageName, int menuID)
        {
            string key = "e-usr-" + pageName + "_" + "m-" + menuID.ToString()+ "-" + "l-" + LANGUAGE.GetCurrentCulture();
            return key;
        }

        public static void RemoveEmenikUserMenuCacheKey(string pageName, int menuID)
        {
            SM.EM.BLL.BizObject.PurgeCacheItems("e-usr-" + pageName + "_m" + menuID .ToString() + "-");

        }
        public static void RemoveEmenikUserMenuCacheKey(string pageName)
        {
            SM.EM.BLL.BizObject.PurgeCacheItems("e-usr-" + pageName);

        }

        public static void RemoveEmenikUserMenuCacheKey()
        {
            SM.EM.BLL.BizObject.PurgeCacheItems("e-usr-");

        }

        // EM USER
        public static string GetEmUserCacheKeyPageName(string pageName)
        {
            string key = "em-user-" + pageName;
            return key;

        }
        public static string GetEmUserCacheKeyUserName(string userName)
        {
            string key = "em-username-" + userName;
            return key;

        }
        public static string GetEmUserCacheKeyCustomDomain(string customDomain)
        {
            string key = "em-userCustomD-" + customDomain ;
            return key;

        }
        public static string GetEmUserCacheKey(Guid userID)
        {
            string key = "em-user-" + userID.ToString() + "/";
            return key;

        }

        public static void RemoveEmUserByPageName(string pageName)
        {
            SM.EM.BLL.BizObject.Cache.Remove(GetEmUserCacheKeyPageName(pageName));
        }
        public static void RemoveEmUserByUserName(string userName)
        {
            SM.EM.BLL.BizObject.Cache.Remove(GetEmUserCacheKeyUserName(userName));
        }
        public static void RemoveEmUserByCustomDomain(string customDomain)
        {
            SM.EM.BLL.BizObject.Cache.Remove(GetEmUserCacheKeyCustomDomain(customDomain ));
        }
        public static void RemoveEmUserByUserID(Guid userID)
        {
            SM.EM.BLL.BizObject.Cache.Remove(GetEmUserCacheKey(userID ));
        }


        public class Remove {
            public static void CartItemList(Guid userid)
            {
                SM.EM.BLL.BizObject.PurgeCacheItems(Key.CartItemList() + userid.ToString() );
            }
        
        
        }

        public class Key {
            public static string ProductAttribute()
            {
                return "em-pattribute-";
            }
            public static string ProductAttribute(int id)
            {
                string key = ProductAttribute() + id.ToString() + "/";
                return key;
            }

            public static string MembershipUser()
            {
                return "asp-memb-user-";
            }
            public static string MembershipUser(string username)
            {
                string key = MembershipUser() + username + "/";
                return key;
            }




            public static string Master()
            {
                return "em-master-";
            }
            public static string Master(int id)
            {
                string key = Master()  + id.ToString() + "/";
                return key;
            }

            public static string Theme()
            {
                return "em-theme-";
            }
            public static string Theme(int id)
            {
                string key = Theme() +  id.ToString() + "/";
                return key;
            }

            public static string PageModule()
            {
                return "em-pagemodule-smp-";
            }
            public static string PageModule(int id)
            {
                string key = PageModule() + id.ToString() + "/";
                return key;
            }

            public static string PageModuleID()
            {
                return "em-pagemodule-id-";
            }
            public static string PageModuleID(int id)
            {
                string key = PageModuleID() + id.ToString() + "/";
                return key;
            }

            public static string ImageTypeID()
            {
                return "em-imagetype-id-";
            }
            public static string ImageTypeID(int id)
            {
                string key = ImageTypeID() + id.ToString() + "/";
                return key;
            }

            public static string Culture(string  id)
            {
                string key = "em_culture-" + id + "/";
                return key;
            }


            public static string CartItemList()
            {
                return "em-cartitemlist-usr";
            }
            public static string CartItemList(Guid userid, string lang, Guid? pageID, Guid? orderID)
            {
                string orderid = "";
                if (orderID != null)
                    orderid = orderID.Value.ToString() ;
                return CartItemList() + userid.ToString() + "l-" + lang + "-pgid-" + (pageID ?? Guid.Empty).ToString() + orderid ;
            }



            //public static string PageModule(int id, Guid userid)
            //{
            //    string key = PageModule() + id.ToString() + "-"  + userid.ToString() + "/";
            //    return key;
            //}
        
        
        }


        public class Dependency{
            public class Key {
                public static string PageRefID()
                {
                    return "cd-PageRefID";
                }
                public static string PageRefID(Guid  refID)
                {
                    string key = PageRefID() + refID.ToString() + "/";
                    return key;
                }
            
            
            
            }
        
        
        
        }

    }
}
