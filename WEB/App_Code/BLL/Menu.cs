﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

    /// <summary>
    /// Summary description for Menu
    /// </summary>
    public partial class MENU
    {
     /// <summary>  
     /// Gets Menu by ID
     /// </summary>  
        //public static List<MENU> GetMenuById(int aID)
        //{
        //    using (eMenikDataContext db = new eMenikDataContext())
        //    {
        //        var menu = from m in db.MENUs
        //                   where m.MENU_ID == aID
        //                   select m;
        //        return menu.ToList<MENU>();
        //    }
        //} 

        public static IEnumerable<MENU> GetMenusActive()
        {
            eMenikDataContext db = new eMenikDataContext();
            IEnumerable<MENU> menus;

            menus = from m in db.MENUs where m.MENU_ACTIVE == true select m;
            return menus;
        }
        public static IEnumerable<MENU> GetMenusBackend()
        {
            eMenikDataContext db = new eMenikDataContext();
            IEnumerable<MENU> menus;

            menus = from m in db.MENUs where m.USERNAME == null && m.MENU_ACTIVE == true orderby m.MENU_CMS ascending   select m;
            return menus;
        }
        public static IEnumerable<MENU> GetMenusActiveWWW()
        {
            eMenikDataContext db = new eMenikDataContext();
            IEnumerable<MENU> menus;

            menus = from m in db.MENUs where m.MENU_ACTIVE == true && m.MENU_CMS == false select m;
            return menus;
        }

        public static MENU GetMenuByUsername(string username)
        {
            eMenikDataContext db = new eMenikDataContext();
            MENU menu;

            menu = (from m in db.MENUs where m.USERNAME == username select m).FirstOrDefault();
            return menu;
        }
        public static List<Data.MenuSmap > GetMenusByPage(Guid pageId)
        {
            eMenikDataContext db = new eMenikDataContext();
            var menu = (from m in db.MENUs
                        from u in db.EM_USERs
                        from s in db.SITEMAPs
                        where u.UserId == pageId && m.USERNAME == u.USERNAME && s.PARENT == -1 && s.MENU_ID == m.MENU_ID
                        select new Data.MenuSmap { MENU_ID = m.MENU_ID, MENU_TITLE = m.MENU_TITLE , 
                            USERNAME = m.USERNAME, MENU_ACTIVE = m.MENU_ACTIVE, MENU_CMS = m.MENU_CMS, ROOT_SMAP = s.SMAP_ID  
                        });
            return menu.ToList();
        }
        public static MENU GetMenuByUsernameBySitemap(string username, int smap)
        {
            eMenikDataContext db = new eMenikDataContext();
            MENU menu;

            menu = (from s in db.SITEMAPs  from m in db.MENUs where s.SMAP_ID == smap && m.MENU_ID == s.MENU_ID &&  m.USERNAME == username select m).SingleOrDefault();
            return menu;
        }
        public static MENU GetMenuByID(int id)
        {
            eMenikDataContext db = new eMenikDataContext();
            return db.MENUs.SingleOrDefault(w=>w.MENU_ID == id);
        }

        public static void InsertDefaultMenusForUser(Guid userid, string username) {

            InsertMenuForUser(userid, username, "Glavni meni", true);
        
        }


        public static void InsertMenuForUser(Guid  userid, string username, string title, bool onlyIfFirst) {

            eMenikDataContext db = new eMenikDataContext();

            // if user already has menu, don't insert nothing
            if (onlyIfFirst && MENU.GetMenuByUsername(username) != null) return;


            // insert default culture for user
            if (onlyIfFirst)
            {
                EM_USER_CULTURE cul = new EM_USER_CULTURE();
                cul.LCID = LANGUAGE.GetCurrentCultureLCID();
                cul.UserId = userid;
                cul.DEFAULT = true;
                cul.ACTIVE = true;
                cul.SORT = 0;

                db.EM_USER_CULTUREs.InsertOnSubmit(cul);
            }


            // insert new menu for user
            MENU m = new MENU();
            m.USERNAME = username;
            m.MENU_TITLE = title;
            m.MENU_ACTIVE = true;
            m.MENU_CMS = false;

            db.MENUs.InsertOnSubmit(m);

            db.SubmitChanges();

            // insert root sitemap 
            SITEMAP r = new SITEMAP();
            r.MENU_ID = m.MENU_ID;
            r.PARENT = -1;
            r.SMAP_GUID = Guid.NewGuid();

            r.PMOD_ID = SM.BLL.Common.PageModule.TwoColumn;  // two column
            r.SMAP_TITLE = username + "root";
            r.SMAP_ACTIVE = true;
            r.WWW_ROLES = "?,*";
            r.CMS_ROLES = "";
            r.SMAP_ORDER = 10;
            r.WWW_VISIBLE = true;

            db.SITEMAPs.InsertOnSubmit(r);
            db.SubmitChanges();


            
                // insert new sitemap (1)
                SITEMAP s1 = new SITEMAP();
                s1.MENU_ID = m.MENU_ID;
                s1.PARENT = r.SMAP_ID;
                s1.SMAP_GUID = Guid.NewGuid();

                s1.PMOD_ID = SM.BLL.Common.PageModule.TwoColumn;  // two column
                s1.SMAP_TITLE = "Domov";
                s1.SMAP_ACTIVE = true;
                s1.WWW_ROLES = "?,*";
                s1.CMS_ROLES = "admin, em_cms";
                s1.SMAP_ORDER = 10;
                s1.WWW_VISIBLE = true;

                db.SITEMAPs.InsertOnSubmit(s1);
                db.SubmitChanges();


                // insert sitemap modules
                List<SITEMAP_MODULE> sm1 = new List<SITEMAP_MODULE>();
                sm1.Add(new SITEMAP_MODULE { SMAP_ID = s1.SMAP_ID, MOD_ID = SM.BLL.Common.Module.Article });
                sm1.Add(new SITEMAP_MODULE { SMAP_ID = s1.SMAP_ID, MOD_ID = SM.BLL.Common.Module.Content });
                sm1.Add(new SITEMAP_MODULE { SMAP_ID = s1.SMAP_ID, MOD_ID = SM.BLL.Common.Module.Gallery });

                db.SITEMAP_MODULEs.InsertAllOnSubmit(sm1);


                // insert sitemap for language
                ChangeSitemapLang(s1.SMAP_ID, "Domov", LANGUAGE.GetCurrentLang(), true);


                // insert new sitemap (2)
                SITEMAP s2 = new SITEMAP();
                s2.MENU_ID = m.MENU_ID;
                s2.PARENT = r.SMAP_ID;
                s2.SMAP_GUID = Guid.NewGuid();

                s2.PMOD_ID = SM.BLL.Common.PageModule.TwoColumn;  // two column
                s2.SMAP_TITLE = "Ponudba";
                s2.SMAP_ACTIVE = true;
                s2.WWW_ROLES = "?,*";
                s2.CMS_ROLES = "admin, em_cms";
                s2.SMAP_ORDER = 20;
                s2.WWW_VISIBLE = true;

                db.SITEMAPs.InsertOnSubmit(s2);
                db.SubmitChanges();


                // insert sitemap modules
                List<SITEMAP_MODULE> sm2 = new List<SITEMAP_MODULE>();
                sm2.Add(new SITEMAP_MODULE { SMAP_ID = s2.SMAP_ID, MOD_ID = SM.BLL.Common.Module.Article });
                sm2.Add(new SITEMAP_MODULE { SMAP_ID = s2.SMAP_ID, MOD_ID = SM.BLL.Common.Module.Content });
                sm2.Add(new SITEMAP_MODULE { SMAP_ID = s2.SMAP_ID, MOD_ID = SM.BLL.Common.Module.Gallery });

                db.SITEMAP_MODULEs.InsertAllOnSubmit(sm2);


                // insert sitemap for language
                ChangeSitemapLang(s2.SMAP_ID, "Ponudba", LANGUAGE.GetCurrentLang(), true);


                // insert new sitemap (3)
                SITEMAP s3 = new SITEMAP();
                s3.MENU_ID = m.MENU_ID;
                s3.PARENT = r.SMAP_ID;
                s3.SMAP_GUID = Guid.NewGuid();

                s3.PMOD_ID = SM.BLL.Common.PageModule.TwoColumn;  // two column
                s3.SMAP_TITLE = "Kontakt";
                s3.SMAP_ACTIVE = true;
                s3.WWW_ROLES = "?,*";
                s3.CMS_ROLES = "admin, em_cms";
                s3.SMAP_ORDER = 30;
                s3.WWW_VISIBLE = true;

                db.SITEMAPs.InsertOnSubmit(s3);
                db.SubmitChanges();


                // insert sitemap modules
                List<SITEMAP_MODULE> sm3 = new List<SITEMAP_MODULE>();
                sm3.Add(new SITEMAP_MODULE { SMAP_ID = s3.SMAP_ID, MOD_ID = SM.BLL.Common.Module.Article });
                sm3.Add(new SITEMAP_MODULE { SMAP_ID = s3.SMAP_ID, MOD_ID = SM.BLL.Common.Module.Content });
                sm3.Add(new SITEMAP_MODULE { SMAP_ID = s3.SMAP_ID, MOD_ID = SM.BLL.Common.Module.Gallery });

                db.SITEMAP_MODULEs.InsertAllOnSubmit(sm3);


                ChangeSitemapLang(s3.SMAP_ID, "Kontakt", LANGUAGE.GetCurrentLang(), true);
            


            // save
            db.SubmitChanges();


            // add default modules
            // CONTENT with help
            string con = "";

            con += "<h1>To je element VSEBINA.</h1><p>Tukaj lahko ureja&scaron; <strong>BESEDILO in SLIKE</strong>, na klasičen <em>WORD-način. </em></p><p>Za <strong>urejanje </strong>klikni <strong>ikono UREDI&nbsp;<img width=\"37\" height=\"39\" align=\"absBottom\" alt=\"\" src=\"/userfiles/admin.cms/image/bt_edit.jpg\" />&nbsp;(</strong>v <strong>opravilni vrstici</strong> elementa,<strong> zgoraj)<strong>.</strong></strong></p><h3>&nbsp;</h3><h1>Možnosti urejanje besedila</h1><p>- <strong>krepko</strong>, <em>ležeče</em>, <u>podčrtano</u> in ostale pogoste stile besedila</p><p>- urejanje oblike (sloga za naslove):</p><h1>Naslov1</h1><h2>Naslov2</h2><h3>Naslov3</h3><p>&nbsp;</p><h1>Možnosti urejanje slike</h1><p>V opravilni vrstici klikni na ikono&nbsp;<strong>vstavi/uredi&nbsp;</strong>sliko.&nbsp;</p><p><img width=\"562\" height=\"58\" alt=\"\" src=\"/userfiles/admin.cms/image/fck_img(1).jpg\" /></p><p>Nato lahko&nbsp;izbere&scaron; staro sliko, doda&scaron; novo na strežnik, nastavi&scaron; velikosti slike (najbolje je, da je slika predpripravljena in istih dimenzij, kot bo prikazana)</p><p>&nbsp;</p><h1>Kaj lahko delam z vsakim ELEMENTOM?</h1><p>&nbsp;</p><table width=\"99%\" border=\"0\" cellpadding=\"1\" cellspacing=\"1\">    <tbody>        <tr>            <td style=\"border:0 !important\"><img width=\"37\" height=\"39\" align=\"absBottom\" alt=\"\" src=\"/userfiles/admin.cms/image/bt_show.jpg\" /></td>            <td style=\"border:0 !important\">PRIKAŽI/SKRIJ ELEMENT na strani</td>        </tr>        <tr>            <td style=\"border:0 !important\"><img width=\"37\" height=\"39\" align=\"absBottom\" alt=\"\" src=\"/userfiles/admin.cms/image/bt_delete.jpg\" /></td>            <td style=\"border:0 !important\">ZBRI&Scaron;I ELEMENT</td>        </tr>        <tr>            <td style=\"border:0 !important\"><img width=\"37\" height=\"39\" align=\"absBottom\" alt=\"\" src=\"/userfiles/admin.cms/image/bt_options.jpg\" /></td>            <td style=\"border:0 !important\">NASTAVITVE ELEMENTA - splo&scaron;ne nastavitve elementa</td>        </tr>        <tr>            <td style=\"border:0 !important\"><img width=\"37\" height=\"39\" align=\"absBottom\" alt=\"\" src=\"/userfiles/admin.cms/image/bt_edit.jpg\" /></td>            <td style=\"border:0 !important\">UREDI ELEMENT- nekateri elementi imajo možnost urejanja&nbsp;</td>        </tr>    </tbody></table><p>&nbsp;</p><h3>&nbsp;</h3><h3>&nbsp;Na vsako stran lahko doda&scaron; poljubno &scaron;tevilo ELEMENTOV</h3><p>&nbsp;</p>";

            
            CMS_WEB_PART.CreateNewCwpContent(con, s1.SMAP_GUID , LANGUAGE.GetCurrentLang(), MODULE.Common.ContentModID, "wpz_1", userid, "", "", false, false, -1, false, false, 0);

            // CONTACT FORM
            CMS_WEB_PART.CreateNewCwpContactForm(s3.SMAP_GUID, LANGUAGE.GetCurrentLang(), MODULE.Common.ContactFormModID, "wpz_1", userid, "Kontaktni obrazec", "", false, false, -1, false, false, 3);
        }


        /// <summary>
        /// Inserts or updates Sitemap Lang
        /// </summary>
        /// 
        public static void ChangeSitemapLang(int smapId, string title, string langId, bool active)
        {
            ChangeSitemapLang(smapId, title, "", "", "", langId, active, false);
        }
        public static void ChangeSitemapLang(int smapId, string title, string langId, bool active, bool isAdmin)
        {
            ChangeSitemapLang(smapId, title, "", "", "", langId, active, isAdmin );
        }

        public static void ChangeSitemapLang(int smapId, string title, string desc, string meta, string kw, string langId, bool active, bool isAdmin) {
            eMenikDataContext db = new eMenikDataContext();

            // insert sitemap for language
            SITEMAP_LANG sl = db.SITEMAP_LANGs.SingleOrDefault(w => w.LANG_ID == langId && w.SMAP_ID == smapId);

            string _desc = desc;
            string _meta = meta ;
            string _kw = kw;
            
            // insert if doesn't exist
            if (sl == null)
            {
                sl = new SITEMAP_LANG();
                db.SITEMAP_LANGs.InsertOnSubmit(sl);
                sl.SML_ORDER = 50;
                sl.SML_KEYWORDS = "";
                if (string.IsNullOrEmpty(desc.Trim()))
                    _desc = title;


            }
            else
            {   // update desc only for changed title and when not set desc
                if (title != sl.SML_TITLE && sl.SML_DESC == sl.SML_TITLE   && string.IsNullOrEmpty(desc))
                    _desc = title;
            }

            sl.SML_ACTIVE = active;
            sl.SMAP_ID = smapId;
            sl.LANG_ID = langId;
            sl.SML_TITLE = title;
            if (!string.IsNullOrEmpty(_desc.Trim()))
                sl.SML_DESC = _desc;

            if (isAdmin)
            {
                sl.SML_META_DESC = _meta;
                sl.SML_KEYWORDS = _kw;
            }
            sl.DIRTY = true;

            db.SubmitChanges();       
        }

        public static SITEMAP_LANG  UpdateSitemapLangDesc(int smapId, string langId, string title, string desc)
        {
            eMenikDataContext db = new eMenikDataContext();

            // insert sitemap for language
            SITEMAP_LANG sl = db.SITEMAP_LANGs.SingleOrDefault(w => w.LANG_ID == langId && w.SMAP_ID == smapId);

            // insert if doesn't exist
            if (sl == null)
            {
                return null;
            }
            if (!string.IsNullOrWhiteSpace(title))
            {
                sl.SML_TITLE = title;
                sl.SML_DESC = title;

            }
            if (!string.IsNullOrEmpty(desc))
                sl.SML_META_DESC  = desc;

            sl.DIRTY = true;

            db.SubmitChanges();

            return sl;
        }

        public static void UpdateOrder(int id, int order)
        {
            eMenikDataContext db = new eMenikDataContext();

            db.cms_SitemapUpdateSort(id, order < 0 ? 0 : order);
        }
        public static void UpdateParent(int id, int parent)
        {
            eMenikDataContext db = new eMenikDataContext();

            SITEMAP smp = db.SITEMAPs.SingleOrDefault(w => w.SMAP_ID == id);
            smp.PARENT = parent;

            db.SubmitChanges();
        }


        public class Move
        {


            public static string  MoveSmapToPosition(int id, int pos, string user)
            {
                MENU  m = MENU.GetMenuByUsernameBySitemap(user, id);
                if (m == null) return "";
                
                return MoveSmapToPosition(m.MENU_ID, id, pos, user);

            }

            public static string  MoveSmapToPosition(int menuID, int id, int pos, string user)
            {
                string res =""; 
                int step = 5;
                int newOrder = step;// order for first item in list

                eMenikDataContext db = new eMenikDataContext();

                // get all menuitems
                var list = (from s in db.SITEMAPs where s.MENU_ID == menuID   orderby s.SMAP_ORDER ascending  select new { s.SMAP_ID , s.SMAP_ORDER , s.PARENT  }).ToList();
                // ...without current one
                list = list.Where(w => w.SMAP_ID  != id && w.PARENT != -1 ).ToList();


                EM_USER usr = EM_USER.GetUserByUserName(user);
                if (usr != null)
                {
                    // remove menu cache for current user
                    SM.EM.Caching.RemoveEmenikUserMenuCacheKey(usr.PAGE_NAME);

                    // redirect to same page
                    res = SM.EM.Rewrite.EmenikUserSettingsUrlByStep(usr.PAGE_NAME, 3);

                }
                // item is only one
                if (list == null)
                {
                    UpdateOrder( id, newOrder);
                    return res;
                }

                // item is last one
                if (list.Count <= pos)
                {
                    newOrder = list[list.Count - 1].SMAP_ORDER + step;
                    UpdateOrder(id, newOrder);
                    return res;
                }

                int ord = step;
                // reorder entire list
                for (int i = 0; i < list.Count(); i++)
                {

                    if (i == pos || (pos < 0 && i == 0))
                    {
                        newOrder = ord;
                        ord += step;
                    }
                    UpdateOrder(list[i].SMAP_ID, ord);

                    ord += step;
                }

                UpdateOrder(id, newOrder);


                return res;

            }

            public static string MoveSmapToPositionParent(int id, int pos, int parent, string user)
            {
                MENU m = MENU.GetMenuByUsernameBySitemap(user, id);
                if (m == null) return "";

                return MoveSmapToPositionParent(m.MENU_ID, id, pos, parent, user);

            }


            public static string MoveSmapToPositionParent(int menuID, int id, int pos, int parent, string user)
            {
                string res = "";
                int step = 5;
                int newOrder = step;// order for first item in list

                if (id == parent)
                    return res;

                eMenikDataContext db = new eMenikDataContext();

                // get all menuitems of same parent
                var list = (from s in db.SITEMAPs where s.MENU_ID == menuID && s.PARENT == parent orderby s.SMAP_ORDER ascending select new { s.SMAP_ID, s.SMAP_ORDER, s.PARENT }).ToList();
                // ...without current one
                list = list.Where(w => w.SMAP_ID != id && w.PARENT != -1).ToList();


                // update new parent
                UpdateParent(id, parent);


                EM_USER usr = EM_USER.GetUserByUserName(user);
                if (usr != null)
                {
                    // remove menu cache for current user
                    SM.EM.Caching.RemoveEmenikUserMenuCacheKey(usr.PAGE_NAME);

                    // redirect to same page later
                    res = SM.EM.Rewrite.EmenikUserSettingsUrlByStep(usr.PAGE_NAME, 3);

                }
                // item is only one
                if (list == null)
                {
                    UpdateOrder(id, newOrder);
                    return res;
                }

                // item is last one
                if (list.Count <= pos)
                {
                    newOrder = list[list.Count - 1].SMAP_ORDER + step;
                    UpdateOrder(id, newOrder);
                    return res;
                }

                int ord = step;
                // reorder entire list
                for (int i = 0; i < list.Count(); i++)
                {

                    if (i == pos || (pos < 0 && i == 0))
                    {
                        newOrder = ord;
                        ord += step;
                    }
                    UpdateOrder(list[i].SMAP_ID, ord);

                    ord += step;
                }

                UpdateOrder(id, newOrder);


                return res;

            }

            

        }


        public class Data {
            public class MenuSmap {
                public int MENU_ID { get; set; }
                public string MENU_TITLE { get; set; }
                public string USERNAME { get; set; }
                public bool MENU_ACTIVE { get; set; }
                public bool MENU_CMS { get; set; }
                public int ROOT_SMAP { get; set; }            
            }
        
        
        }


        public class Render {

            public static string MenuEditHierarchy(int parent, string lang) {
                string ret = "";

                object data = SITEMAP.GetSiteMapChildsByIDByLang(parent, lang);

                ret = ViewManager.RenderView("~/_ctrl/menu/lookup/_viewMenuLevelHierarchyEdit.ascx", data, new object[] { parent });

                return ret;
            
            }

            public static string MenuEdit(int smp, EM_USER page)
            {
                string ret = "";

                object data = SITEMAP.GetSiteMapByID(smp);
                var list = SITEMAP.GetSitemapLangsBySitemap(smp);

                ret = ViewManager.RenderView("~/_ctrl/menu/edit/_viewMenuEdit.ascx", data, new object[] { list, page });

                return ret;

            }
        
        
                
        }

    }
