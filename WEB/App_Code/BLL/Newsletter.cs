﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Threading;
using System.Web.Profile;
using System.Text.RegularExpressions;
using System.Net.Mail;
using System.Threading;
using System.Web.Caching;

/// <summary>
/// Summary description for NEWSLETTER
/// </summary>
public partial class NEWSLETTER
{
    public static IQueryable<NEWSLETTER> GetPendingNewsletters()
    {
        eMenikDataContext db = new eMenikDataContext();


        return db.NEWSLETTERs.Where(w => w.Status == Common.Status.SENDING).OrderBy(o => o.DateSent);
    }


    public static NEWSLETTER GetNewsletterByID(Guid id)
    {
        eMenikDataContext db = new eMenikDataContext();

        return db.NEWSLETTERs.Where(w => w.NewsletterID  == id).SingleOrDefault();
    }

    public static IEnumerable< NEWSLETTER_EMAIL> GetNewsletterEmailsByID(Guid id)
    {
        eMenikDataContext db = new eMenikDataContext();

        return db.NEWSLETTER_EMAILs.Where(w => w.NewsletterID == id);
    }

    public static IEnumerable<NEWSLETTER_EMAIL> GetNewsletterEmailsByIDByStatus(Guid id, short status)
    {
        eMenikDataContext db = new eMenikDataContext();

        return db.NEWSLETTER_EMAILs.Where(w => w.NewsletterID == id && w.Status == status);
    }

    public static NEWSLETTER UpdateStatus(Guid id, short status)
    {
        eMenikDataContext db = new eMenikDataContext();

        NEWSLETTER n =  db.NEWSLETTERs.Where(w => w.NewsletterID == id).SingleOrDefault();
        if (n == null)
            return n;
        n.Status = status;
        n.DateSent = DateTime.Now;

        db.SubmitChanges();
        return n;
    }

    public static void  UpdateEmailStatus(int id, short status, string notes)
    {
        eMenikDataContext db = new eMenikDataContext();

        NEWSLETTER_EMAIL n = db.NEWSLETTER_EMAILs.Where(w => w.EmailID == id).SingleOrDefault();
        if (n == null)
            return ;
        n.Status = status;
        n.Notes = notes ;

        db.SubmitChanges();
    }



    public static NEWSLETTER  CopyAsNew(Guid id, Guid  user)
    {
        eMenikDataContext db = new eMenikDataContext();

        NEWSLETTER old = db.NEWSLETTERs.SingleOrDefault(w => w.NewsletterID == id);
        if (old == null)
            return null;

        NEWSLETTER newN = new NEWSLETTER();
        newN.Title = old.Title;
        newN.Desc = old.Desc;
        newN.Subject = old.Subject;
        newN.Email = old.Email;
        newN.EmailName = old.EmailName;
        newN.Body = old.Body;
        newN.Password = old.Password;
        newN.AttachmentFileName = old.AttachmentFileName ;

        newN.NewsletterID = Guid.NewGuid();
        newN.CreatedBy = user;
        newN.DateCreated = DateTime.Now;        
        newN.Status = 0;
        db.NEWSLETTERs.InsertOnSubmit(newN);
        db.SubmitChanges();

        return newN;

    }
    public static string  InsertContactsForNewsletter(Guid id, string emailList)
    {
        eMenikDataContext db = new eMenikDataContext();

        // get existing emails
        var existingEmails = from e in db.NEWSLETTER_EMAILs where e.NewsletterID == id select e.Email;

        
        string[] list = SM.EM.Helpers.GetEmailListAsArray (emailList );

        List<string> invalid = new List<string>();

        foreach (string item in list){
            string newEmail = item.Trim().Trim("<>".ToCharArray()).Trim().ToLower();
            // check if is valid
            if (SM.EM.Helpers.IsValidEmail(newEmail, false ))
            {
                // insert only new items
                if (!existingEmails.Contains(newEmail))
                    db.NEWSLETTER_EMAILs.InsertOnSubmit(new NEWSLETTER_EMAIL { Email = newEmail, NewsletterID = id, Status = 0 });
            }
            else
                invalid.Add( newEmail);

        }

        // save to DB
        db.SubmitChanges();

        return  String.Join(", ", invalid.ToArray());

    }

    public static string ImportUsersForNewsletter(Guid id, string emailList, short portalID)
    {
        eMenikDataContext db = new eMenikDataContext();

        // get existing emails
        var users = (from u in db.CUSTOMERs where u.NEWSLETTER_ENABLED == true 
                     && !u.SentWelcomeMail && u.GeneratedPass != null
                     && u.EMAIL != null
                     
                     select new { u.EMAIL, UserId =  u.CustomerID })
            
            //.Take(10) // TMP!!!!!!!!!!!
            ;


        string[] list = SM.EM.Helpers.GetEmailListAsArray(emailList);

        List<string> validList = new List<string>();

        foreach (var item in users)
        {
            string newEmail = item.EMAIL.Trim().ToLower();
            // check if is valid
            if (SM.EM.Helpers.IsValidEmail(newEmail, false))
            {
                if (!CheckEmailExists(id, newEmail))
                {
                    db.NEWSLETTER_EMAILs.InsertOnSubmit(new NEWSLETTER_EMAIL { UserId = item.UserId, Email = newEmail, NewsletterID = id, Status = 0 });
                    // save to DB
                    db.SubmitChanges();
                    validList.Add(newEmail);
                }
            }

        }



        //return String.Join(", ", validList.ToArray());
        return "";

    }

    public static bool  CheckEmailExists(Guid id, string email)
    {
        eMenikDataContext db = new eMenikDataContext();

        bool r = db.NEWSLETTER_EMAILs.Where(w => w.NewsletterID  == id && w.Email == email.Trim() ).Any() ;
        return r;

    }



    public static void DeleteNewsletterEmail(int id)
    {
        eMenikDataContext db = new eMenikDataContext();

        var r =  db.NEWSLETTER_EMAILs.SingleOrDefault (w => w.EmailID == id);
        db.NEWSLETTER_EMAILs.DeleteOnSubmit(r);

        db.SubmitChanges();
    }

    public static void DeleteAllNewsletterEmails(Guid  id)
    {
        eMenikDataContext db = new eMenikDataContext();

        var r = db.NEWSLETTER_EMAILs.Where(w => w.NewsletterID  == id);
        db.NEWSLETTER_EMAILs.DeleteAllOnSubmit(r);
        db.SubmitChanges();
    }


    public static void SendAllPendingNewsletters() { 
    
    
        // check if pending newsletter exists
        var nl = GetPendingNewsletters().FirstOrDefault();
        
        // no pending newsletters
        if (nl == null)
            return;

    
        // send newsletter
        SendNewsletter(nl.NewsletterID, "", null);

        // reschedule
        Schedule.RegisterNextPackageForSending();
    
    }



    public static void SendNewsletter(Guid id, string testEmail, Guid? userid)
    {
        Sending.Lock.AcquireWriterLock(Timeout.Infinite);
        Sending.TotalMails = -1;
        Sending.SentMails = 0;
        Sending.InvalidMails  = 0;
        Sending.PercentageCompleted = 0.0;
        Sending.IsSending = true;
        Sending.Lock.ReleaseWriterLock();

        // if the HTML body is an empty string, use the plain-text body converted to HTML
        //if (htmlBody.Trim().Length == 0)
        //    htmlBody = SM.EM. Helpers.ConvertToHtml(plainTextBody);



        //// send the newsletters asynchronously
        object[] parameters = new object[] { id, HttpContext.Current, testEmail, userid  };
        //ParameterizedThreadStart pts = new ParameterizedThreadStart(SendEmails);
        //Thread thread = new Thread(pts);
        //thread.Name = "SendEmails";
        //thread.Priority = ThreadPriority.BelowNormal;
        //thread.Start(parameters);


        // test send
        //if (!string.IsNullOrEmpty(testEmail))
        SendEmails(parameters);
        //else
        //{
        //    // send the newsletters asynchronously
        //    ParameterizedThreadStart pts = new ParameterizedThreadStart(SendEmails);
        //    Thread thread = new Thread(pts);
        //    thread.Name = "SendEmails";
        //    thread.Priority = ThreadPriority.BelowNormal;
        //    thread.Start(parameters);
        //}





    }

    /// <summary>
    /// Sends the newsletter e-mails to all subscribers
    /// </summary>
    private static void SendEmails(object data)
    {
        object[] parameters = (object[])data;
        Guid NewsletterID = new Guid(parameters[0].ToString());
        HttpContext context = (HttpContext)parameters[1];
        string testEmail = parameters[2].ToString();

        Guid userId = Guid.Empty;
        if (parameters[3] != null)
            userId = new Guid(parameters[3].ToString());


        NEWSLETTER news = NEWSLETTER.GetNewsletterByID(NewsletterID);

        Sending.Lock.AcquireWriterLock(Timeout.Infinite);
        Sending.TotalMails = 0;
        Sending.Lock.ReleaseWriterLock();




        var mailsNotSent = GetNewsletterEmailsByIDByStatus(NewsletterID, Common.Status.NOT_SENT);
        int countNotSent = mailsNotSent.Count();
        int currentPackageSent = 0;


        // create user list
        List<NEWSLETTER_EMAIL> mailList;

        if (string.IsNullOrEmpty(testEmail))
            mailList = mailsNotSent.Take(Common.SendingOptions.PACKAGE_SIZE).ToList();
        else
        {
            // if is Test
            mailList = new List<NEWSLETTER_EMAIL>();
            mailList.Add(new NEWSLETTER_EMAIL { Email = testEmail, UserId = userId, NewsletterID = NewsletterID });
        }


        //subscribers = GetSubscribers(false, "", groups);
        Sending.Lock.AcquireWriterLock(Timeout.Infinite);
        Sending.TotalMails = mailList.Count;
        Sending.Lock.ReleaseWriterLock();

        // send the newsletter
        SmtpClient smtpClient = new SmtpClient();
        foreach (NEWSLETTER_EMAIL item in mailList)
        {

            


            // check if mail is valid
            if (!SM.EM.Helpers.IsValidEmail(item.Email, false))
            {

                // todo: flag mail as bad one
                if (item.UserId != null)
                {
                    //EM_USER.UpdateNewsletterEnabled(item.UserId.Value, false, false);
                    UpdateEmailStatus(item.EmailID, NEWSLETTER.Common.Status.ERROR, "email not valid");
                }

                //mail send counter
                Sending.Lock.AcquireWriterLock(Timeout.Infinite);
                Sending.InvalidMails += 1;
                Sending.PercentageCompleted = (double)Sending.TotalSentMails * 100 / (double)Sending.TotalMails;
                Sending.Lock.ReleaseWriterLock();

                continue;
            }

            // create mail
            MailMessage mail = new MailMessage();
            string body = news.Body;

            try
            {

                mail.Sender = new MailAddress(SM.EM.Mail.Account.Info.Email, news.EmailName ?? news.Email);
                mail.From = new MailAddress(news.Email, news.EmailName ?? news.Email);
                mail.To.Add(item.Email);
                //mail.To.Add("zzzare@gmail.com"); // TMP!!!!!
                
                mail.ReplyTo = new MailAddress(news.Email);
                mail.Subject = news.Subject;
                mail.Priority = MailPriority.Low;

            }
            catch (Exception e)
            {
                //EM_USER.UpdateNewsletterEnabled(item.UserId.Value, false, false);
                UpdateEmailStatus(item.EmailID, NEWSLETTER.Common.Status.ERROR, "email not valid");

                //mail send counter
                Sending.Lock.AcquireWriterLock(Timeout.Infinite);
                Sending.InvalidMails += 1;
                Sending.PercentageCompleted = (double)Sending.TotalSentMails * 100 / (double)Sending.TotalMails;
                Sending.Lock.ReleaseWriterLock();


                ERROR_LOG.LogError(e, "Newsletter.SendEmails create mail :" + item.Email);

                continue;

            }


            // personal placeholders
            bool htmlIsPersonalized = HasPersonalizationPlaceholders(body, true);
            CustomerRep crep = new CustomerRep();
            CUSTOMER usr = null;
            if (htmlIsPersonalized && item.UserId != null)
            {
                usr = crep.GetCustomerByID(item.UserId.Value);

                //check if unsubscribed
                if (!usr.NEWSLETTER_ENABLED && string.IsNullOrEmpty(testEmail))
                {
                    continue;
                }

                body = ReplacePersonalizationPlaceholders(body, usr, true);
            }
            mail.Body = body;
            mail.IsBodyHtml = true;

            // attachment
            if (news.AttachmentFileName != null && !String.IsNullOrEmpty(news.AttachmentFileName.Trim()))
                mail.Attachments.Add(new Attachment(System.Web.Hosting.HostingEnvironment.MapPath(news.AttachmentFileName)));

            try
            {
                // if local, use gmail, else use server SMTP
                //if (context != null && context.Request.IsLocal)
                {
                    smtpClient.Credentials = new System.Net.NetworkCredential(news.Email, news.Password);
                    smtpClient.Port = 587;//or use 587            
                    smtpClient.Host = "smtp.gmail.com";

                    smtpClient.EnableSsl = true;
                }
                //else
                //{
                //    //smtpClient.DeliveryMethod = SmtpDeliveryMethod.PickupDirectoryFromIis;

                //    // network delivery for propely setting SENDER header in mail
                //    //smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                //    smtpClient.Host = "1dva3.si";
                //}

                // finally - send
                smtpClient.Send(mail);
                currentPackageSent++;

                // update mail was sent
                NEWSLETTER.UpdateEmailStatus(item.EmailID, NEWSLETTER.Common.Status.SENT, "");

                // add delay (ca. 0.1 sec)
                //System.Threading.Thread.Sleep(Common.SendingOptions.NEXT_MAIL_SLEEP_MS);

                // save sent
                if (usr != null) {
                    usr.DateMailSent = DateTime.Now;
                    usr.SentWelcomeMail = true;
                    if (string.IsNullOrEmpty(testEmail))
                        crep.Save();
                }

            }
            catch (Exception e)
            {
                ERROR_LOG.LogError(e, "Newsletter.SendEmails");
                UpdateEmailStatus(item.EmailID, NEWSLETTER.Common.Status.ERROR, e.Message.Substring(0, 200));

            }

            //mail send counter
            Sending.Lock.AcquireWriterLock(Timeout.Infinite);
            Sending.SentMails += 1;
            Sending.PercentageCompleted = (double)Sending.TotalSentMails * 100 / (double)Sending.TotalMails;
            Sending.Lock.ReleaseWriterLock();
        }

        // if all mails are sent for this newsletter, update nl status
        if (countNotSent == mailList.Count)
            NEWSLETTER.UpdateStatus(NewsletterID, NEWSLETTER.Common.Status.SENT);


        Sending.Lock.AcquireWriterLock(Timeout.Infinite);
        Sending.IsSending = false;
        Sending.Lock.ReleaseWriterLock();
    }




    /// <summary>
    /// Returns whether the input text contains personalization placeholders
    /// </summary>
    private static bool HasPersonalizationPlaceholders(string text, bool isHtml)
    { 
//        if (isHtml)
//        {
            if (Regex.IsMatch(text, @"&lt;%\s*email\s*%&gt;", RegexOptions.IgnoreCase | RegexOptions.Compiled))
                return true;
            if (Regex.IsMatch(text, @"&lt;%\s*firstname\s*%&gt;", RegexOptions.IgnoreCase | RegexOptions.Compiled))
                return true;
            if (Regex.IsMatch(text, @"&lt;%\s*lastname\s*%&gt;", RegexOptions.IgnoreCase | RegexOptions.Compiled))
                return true;
            if (Regex.IsMatch(text, @"&lt;%\s*username\s*%&gt;", RegexOptions.IgnoreCase | RegexOptions.Compiled))
                return true;
            if (Regex.IsMatch(text, @"&lt;%\s*sign-out\s*%&gt;", RegexOptions.IgnoreCase | RegexOptions.Compiled))
                return true;
//        }
//        else
//        {
            if (Regex.IsMatch(text, @"<%\s*email\s*%>", RegexOptions.IgnoreCase | RegexOptions.Compiled))
                return true;
            if (Regex.IsMatch(text, @"<%\s*firstname\s*%>", RegexOptions.IgnoreCase | RegexOptions.Compiled))
                return true;
            if (Regex.IsMatch(text, @"<%\s*lastname\s*%>", RegexOptions.IgnoreCase | RegexOptions.Compiled))
                return true;
            if (Regex.IsMatch(text, @"<%\s*username\s*%>", RegexOptions.IgnoreCase | RegexOptions.Compiled))
                return true;
            if (Regex.IsMatch(text, @"<%\s*sign-out\s*%>", RegexOptions.IgnoreCase | RegexOptions.Compiled))
                return true;
            if (Regex.IsMatch(text, @"<%\s*pass\s*%>", RegexOptions.IgnoreCase | RegexOptions.Compiled))
                return true;

        //        }
        return false;
    }

    /// <summary>
    /// Replaces the input text's personalization placeholders
    /// </summary>
    private static string ReplacePersonalizationPlaceholders(string text, CUSTOMER user, bool isHtml)
    {
        //if (isHtml)
        //{
            text = Regex.Replace(text, @"&lt;%\s*email\s*%&gt;", user.EMAIL, RegexOptions.IgnoreCase | RegexOptions.Compiled);
            text = Regex.Replace(text, @"&lt;%\s*firstname\s*%&gt;", (user.FIRST_NAME ?? "uporabnik " + SM.BLL.Common.Emenik.Data.PortalName() ), RegexOptions.IgnoreCase | RegexOptions.Compiled);
            text = Regex.Replace(text, @"&lt;%\s*lastname\s*%&gt;", user.LAST_NAME ?? "", RegexOptions.IgnoreCase | RegexOptions.Compiled);
            text = Regex.Replace(text, @"&lt;%\s*username\s*%&gt;", user.USERNAME, RegexOptions.IgnoreCase | RegexOptions.Compiled);
            text = Regex.Replace(text, @"&lt;%\s*sign-out\s*%&gt;", Common.GetNewsletterSignOutHrefLink(user.CustomerID, "tukaj"), RegexOptions.IgnoreCase | RegexOptions.Compiled);
            text = Regex.Replace(text, @"&lt;%\s*pass\s*%&gt;", user.GeneratedPass ?? "", RegexOptions.IgnoreCase | RegexOptions.Compiled);
            //}
        //else
        //{
            text = Regex.Replace(text, @"<%\s*email\s*%>", user.EMAIL, RegexOptions.IgnoreCase | RegexOptions.Compiled);
            text = Regex.Replace(text, @"<%\s*firstname\s*%>", (user.FIRST_NAME ?? "uporabnik " + SM.BLL.Common.Emenik.Data.PortalName()), RegexOptions.IgnoreCase | RegexOptions.Compiled);
            text = Regex.Replace(text, @"<%\s*lastname\s*%>", user.LAST_NAME ?? "", RegexOptions.IgnoreCase | RegexOptions.Compiled);
            text = Regex.Replace(text, @"<%\s*username\s*%>", user.USERNAME, RegexOptions.IgnoreCase | RegexOptions.Compiled);
            text = Regex.Replace(text, @"<%\s*sign-out\s*%>", Common.GetNewsletterSignOutHrefLink(user.CustomerID, "tukaj"), RegexOptions.IgnoreCase | RegexOptions.Compiled);
            text = Regex.Replace(text, @"<%\s*pass\s*%>", user.GeneratedPass ?? "", RegexOptions.IgnoreCase | RegexOptions.Compiled);
            //}
        return text;
    }









    public class Sending {
        public static ReaderWriterLock Lock = new ReaderWriterLock();

        public static bool IsSending
        {
            get;
            set;
        }

        public static double PercentageCompleted
        {
            get;
            set;
        }

        public static int TotalMails
        {
            get;
            set;
        }

        public static int SentMails
        {
            get;
            set;
        }
    
        public static int InvalidMails{get;set;}
    

        public static int TotalSentMails { get { return SentMails + InvalidMails; } }
    
    }



    public class Schedule {

        public const string DummyCacheItemKey = "nlSchdSendAllPending";

        public static bool IsCacheScheduleInCache()
        {
            if (HttpRuntime.Cache[DummyCacheItemKey] != null) return true;

            return false;
        }

        public static bool RegisterNextPackageForSending()
        {
//            if (!SM.BLL.Common.AppSettings.EnableSchedule) return false;
            if (IsCacheScheduleInCache()) return false;

            HttpRuntime.Cache.Insert(DummyCacheItemKey, "", null, DateTime.MaxValue, TimeSpan.FromMinutes(Common.SendingOptions.NEXT_PACKAGE_DELAY_MIN ), CacheItemPriority.Normal, new CacheItemRemovedCallback(CacheItemRemovedCallback));

            return true;
        }

        public static void CacheItemRemovedCallback(string key, object value, CacheItemRemovedReason reason)
        {
//            if (!SM.BLL.Common.AppSettings.EnableSchedule) return;

//            RegisterCacheEntry();

            // Do the service works
            //Task.IntranetSendReminders();
            SendAllPendingNewsletters();
        }
    
    
    
    
    
    
    }






    public class Common {
        public class Status
        {
            public const short NOT_SENT = 0;
            public const short SENDING = 3;
            public const short SENT = 1;
            public const short ERROR = 2;
        }

        public class SendingOptions {
            public const int PACKAGE_SIZE = 30;
            public const int NEXT_PACKAGE_DELAY_MIN = 5;
            public const int NEXT_MAIL_SLEEP_MS = 0;
        
        
        }

        public class NewsletterType
        {
            public const short EMAIL = 1;
            public const short PORTAL_NEWSLETTER = 0;
        }

        public static string RenderStatus(object stat)
        {
            short status = short.Parse(stat.ToString());
            string ret = "";
            switch (status)
            {
                case NEWSLETTER .Common.Status.NOT_SENT:
                    ret = "NOT SENT";
                    break;
                case NEWSLETTER.Common.Status.SENT:
                    ret = "SENT";
                    break;
                case NEWSLETTER.Common.Status.SENDING :
                    ret = "SENDING...";
                    break;
            }
            return ret;
        }


        public static string GetNewsletterDetailsCMSHref(Guid o)
        {
            string ret = "~/cms/emenik/newsletter/ManageNewsletter.aspx?nl=" + o.ToString();

            return ret;
        }


        public static string GetNewsletterSignOutHref(Guid o)
        {
            string key = HttpUtility.UrlEncode (SM.EM.Security.Encryption.Hex(o.ToString()));
            string signOutUrl = "http://" + SM.BLL.Common.Emenik.Data.PortalHostDomain() + "/c/a/newsletter-signout.aspx?id=" + key;
            return signOutUrl;
        }

        public static string GetNewsletterSignOutHrefLink(Guid o, string text)
        {
            string href = GetNewsletterSignOutHref(o);

            return "<a href=\"" + href +"\" >" + text + "</a>";
        }



    }

    //partial void OnValidate(System.Data.Linq.ChangeAction action)
    //{
    //}

}
