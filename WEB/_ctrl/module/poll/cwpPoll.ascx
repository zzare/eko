﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="cwpPoll.ascx.cs" Inherits="SM.EM.UI.Controls.cwpPoll" %>
<%--<%@ Register TagPrefix="SM" TagName="PollList" Src="~/_ctrl/module/poll/_viewPollList.ascx"   %>--%>
<%@ Register TagPrefix="SM" TagName="Poll" Src="~/_ctrl/module/poll/_viewPoll.ascx"   %>
<%@ Register TagPrefix="SM" TagName="PollCWP" Src="~/_ctrl/module/poll/_viewPollCWP.ascx"   %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>




<div id='<%= cwp.CWP_ID.ToString() %>' class='<%= "cwp" + (ParentPage.IsModeCMS ? " cwpEdit " : " ") + CMS_WEB_PART.Common.RenderHiddenClass(cwp) %>'>
    <div id="divEdit" runat="server" class="cwpHeader cwpMove" visible="false" >
        <div class="cwpHeaderRight"> &nbsp;</div>

        <div style="float:right; ">
            <a id="btNew" href="#" onclick="newPollLoad('<%= Guid.Empty %>', '<%= cwp.CWP_ID.ToString() %>'); return false" title="dodaj novo anketo"  class="btNew"></a>
<% if (PERMISSION.User.Common.CanShowHideModule()){%>            
            <a id="btShow"  title="pokaži" runat="server" class="btShow"></a>
            <a id="btHide" title="skrij" runat="server" class="btHide"></a>
<%} %>
<% if (PERMISSION.User.Common.CanDeleteModule()){%>
            <a id="btDelete" href="#" title="izbriši" class="btDelete" onclick="deleteEL('<%= cwp.CWP_ID.ToString() %>'); return false" ></a>
<%} %>
<% if (PERMISSION.User.Common.CanEditModuleSettings ()){%>
            <a id="btSettings" href="#" onclick="settingsLoad('<%= cwp.CWP_ID.ToString() %>'); return false" title="nastavitve" class="btSettings"  ></a>
<%} %>
        </div>

        <div class="cwpHeaderLeft"> &nbsp;</div>

        <a id="btMove" class="btMove"></a>

        <div class="cwpHeaderTitle">
            Anketa
            <a href="javascript:void(0)" class="btsettStatusHelp btcwpHeadHelp"><img src='<%=Page.ResolveUrl("~/_inc/images/icon/help.png") %>' /></a>
            <div class="settStatusWrapp cwpHeadHelpWrapp" >
                <div class="settStatus cwpHeadHelp">
                    Element anketa omogoča urejanje anket. Z anketo lahko pritegnete obiskovalce, vi pa lahko dobite odgovore na ključna vprašanja.
                    <br /><br />
                    Z elementom ankete lahko:
                    <br />
                    - dodajaš/urejaš anekete<br />
                    - pregleduješ statistiko odgovorov
                </div>
            </div>
        </div>
    </div>
    <div  class="clearing"></div>

    <div class='<%=  "cPoll " + ( ParentPage.IsModeCMS ? "cwpContainer" : "") %>'>
        <div class="cPollCWP">
            <SM:PollCWP ID="objPollCWP" runat="server"  />
        </div>
            <div class="contPoll">
                <div class="cPollItem">
                <SM:Poll ID="objPoll" runat="server"  />
                </div>

                <div id="divArchive" class="divArchive" runat="server">
                    <a href='<%= RenderArhivHREF() %>' ><%= GetGlobalResourceObject("Modules", "AllArticles") %>&nbsp;&raquo;</a>
                </div>  
            </div>
    </div>
</div>