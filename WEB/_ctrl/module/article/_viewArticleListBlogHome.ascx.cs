﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SM.EM.BLL;
using SM.EM.UI.Controls;

public partial class _ctrl_module_product_viewArticleListBlogHome : BaseControlView
{

    public object Data;
    public object[] Parameters;
    public string LangID;

    public int CurrentSitemapID{get; set;}
    public EM_USER  em_user { get; set; }
    public Guid CwpID { get; set; }


    public bool ShowDate { get; set; }

    public bool ShowCategory { get; set; }

    public bool ShowAddNewBlog { get; set; }


    public bool ShowFooter { get; set; }
    public int? ImgTypeID { get; set; }

    protected CustomPager Pager;
    public bool EnablePager { get; set; }

    public string BaseCanonicalUrl { get; set; }

    #region Property


    //private string containerCssClass = "contArticles";
    //public string ContainerCssClass
    //{
    //    get { return this.containerCssClass; }
    //    set { this.containerCssClass = value; }
    //}
    private string appendToTitle = "";
    public string AppendToTitle
    {
        get { return this.appendToTitle; }
        set { this.appendToTitle = value; }
    }

    private bool showImage = false;
    public bool ShowImage
    {
        get { return this.showImage; }
        set { this.showImage = value; }
    }
    private bool showDesc = true;
    public bool ShowDesc
    {
        get { return this.showDesc; }
        set { this.showDesc = value; }
    }
    private int titleLength = -1;
    public int TitleLength
    {
        get { return this.titleLength; }
        set { this.titleLength = value; }
    }
    private int descLength = -1;
    public int DescLength
    {
        get { return this.descLength; }
        set { this.descLength = value; }
    }

    private string articleDetailsPath = "~/c/ArticleDetails.aspx";
    public string ArticleDetailsPath
    {
        get { return this.articleDetailsPath; }
        set { this.articleDetailsPath = value; }
    }
    //private bool isEditMode = false;
    //public bool IsEditMode
    //{
    //    get { return this.isEditMode; }
    //    set { this.isEditMode = value; }
    //}
    //private bool autoLoad = true;
    //public bool AutoLoad
    //{
    //    get { return this.autoLoad; }
    //    set { this.autoLoad = value; }
    //}

    public int PageNum    {        get ;        set;    }
    public int PageSize    {        get ;        set;    }


    #endregion



    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        // init pager
        Pager = new CustomPager();
        Pager.OrderColumns = "";

        Pager.SelectedClass = "pagesel";
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        //phPager.Visible = false;

        //// set base url (for SEO)
        //// if set
        //if (!string.IsNullOrEmpty(BaseCanonicalUrl))
        //    Pager.BaseAbsoluteUrl = BaseCanonicalUrl;
        //else
        //{

        //    if (Page is SM.EM.UI.BasePage_EMENIK)
        //    {
        //        // use page default base canonical URL
        //        SM.EM.UI.BasePage_EMENIK ppem = (SM.EM.UI.BasePage_EMENIK)Page;
        //        Pager.BaseAbsoluteUrl = ppem.GetDefaultCanonicalUrl();

        //    }
        //}


        BindData();
    }



    public void BindData()
    {

        if (Data == null)
            return;

        //if (lvList.Items.Count > 0)
        //    return;

        if (Parameters != null)
        {
            if (Parameters.Length > 0 && Parameters[0] != null)
                CwpID  = (Guid )Parameters[0];
            if (Parameters.Length > 1 && Parameters[1] != null)
                em_user = (EM_USER)Parameters[1];
            if (Parameters.Length > 2 && Parameters[2] != null)
                LangID = Parameters[2].ToString();
            if (Parameters.Length > 3 && Parameters[3] != null)
                CurrentSitemapID = (int)(Parameters[3]);
            if (Parameters.Length > 4 && Parameters[4] != null)
                IsModeCMS = bool.Parse(Parameters[4].ToString());
        }

        IQueryable<Data.ArticleData> list = (IQueryable<Data.ArticleData>)Data;

        int skip = 0;
        if (EnablePager)
        {
            // set pager TOTAL COUNT
            Pager.TotalCount = list.Count();
            Pager.PageSize = PageSize;

            // set paging to list
            skip = Pager.PageSize * (Pager.GetCurrentPage() - 1);
        }
        list = list.Skip(skip).Take(PageSize); 


        lvList.DataSource = list;
        lvList.DataBind();

        if (lvList.Items.Count <= 0)
        {
            divArticles.Visible = false;
            phEmpty.Visible = true;
        }

        // show pager
        //if (EnablePager && Pager.TotalPages > 1)
        //    phPager.Visible = true;



        // set canonical
        if (EnablePager && Page is SM.EM.UI.BasePage_EMENIK)
        {
            // use page default base canonical URL
            SM.EM.UI.BasePage_EMENIK ppem = (SM.EM.UI.BasePage_EMENIK)Page;

            // set canonical page URL
            if (Pager.GetCurrentOrder() == 1 && Pager.GetCurrentDirection() == Pager.GetDefaultDirectionByID(1) && Pager.GetCurrentPage() > 1)
            {
                ppem.RelCanonical = Pager.RenderSortPageFullUrl(Pager.GetCurrentOrder(), Pager.GetCurrentPage(), false, "");
            }
        }
    }


    protected string RenderArtHref(object item)
    {
        Data.ArticleData art = (Data.ArticleData)item;

        int smp = CurrentSitemapID;
        if (art.CAT_SMAP_ID > 0)
            smp = art.CAT_SMAP_ID;

        if (!String.IsNullOrEmpty(em_user.PAGE_NAME ))
            return Page.ResolveUrl(SM.EM.Rewrite.EmenikUserArticleDetailsUrl(em_user.PAGE_NAME, smp, SM.BLL.Common.PageModule.ArticleDetails, art.ART_ID, art.ART_TITLE));
        else
            return Page.ResolveUrl(ArticleDetailsPath + "?art=" + art.ART_ID.ToString() + "&smp=" + art.SMAP_ID.ToString());

    }
    protected string RenderArtTitle(object item)
    {
        Data.ArticleData art = (Data.ArticleData)item;
        return SM.EM.Helpers.CutString(art.ART_TITLE, TitleLength - AppendToTitle.Length) + AppendToTitle;

    }
    protected string RenderArtDesc(object item)
    {
        if (!ShowDesc) return "";

        Data.ArticleData art = (Data.ArticleData)item;
        string desc = art.ART_ABSTRACT;
        if (string.IsNullOrEmpty(desc))
        {
            // use body
            desc = SM.EM.Helpers.CutString(SM.EM.Helpers.StripHTML(Server.HtmlDecode(art.ART_BODY)), 250, " ...");
            //desc = art.ART_ABSTRACT;
        }
        return SM.EM.Helpers.CutString(desc, DescLength);

    }

    protected string RenderAuthor(object item)
    {
        Data.ArticleData art = (Data.ArticleData)item;
        string author = art.ADDED_BY;
        if (!string.IsNullOrEmpty(author))
            author = "napisal: " + author;
        return author;
    }
    protected string RenderComments(object item)
    {
        Data.ArticleData a = (Data.ArticleData)item;
        int count = COMMENT.GetCommentsCount(a.ART_ID);

        return string.Format("komentarji: <a href=\"{0}\">{1}</a>", RenderArtHref(item) + "#comments" + a.ART_ID.ToString(), count.ToString());
    }
    protected string RenderDatePublished(object item)
    {

        return ARTICLE.Render.RenderDatePublished(item);

        //Data.ArticleData a = (Data.ArticleData)item;

        //DateTime date = a.DATE_ADDED;
        //if (a.RELEASE_DATE != null && a.RELEASE_DATE.Value.Date > a.DATE_ADDED.Date)
        //    date = a.RELEASE_DATE.Value;

        ////if (date == date.Date) // if time is not set, show only date part
        //    return string.Format("{0:dddd, dd.MM.yyyy} ", date);
        ////else
        //  //  return string.Format("{0:dddd, dd.MM.yyyy ob HH:mm} ", date);
    }

    protected string RenderAdditionalInfo(object item)
    {
        Data.ArticleData a = (Data.ArticleData)item;
        string ret = "";
        if (!ShowCategory && !ShowDate)
            return ret;
        if (ShowCategory)
            ret = ARTICLE.Render.RenderCategoryHref(a, "artBox_artCategory", em_user.PAGE_NAME);

        if (!string.IsNullOrEmpty(ret) && ShowDate)
            ret = "&nbsp;|&nbsp;" + ret;
        if (ShowDate  )
            ret = ARTICLE.Render.RenderDatePublished(item) + ret;

        return ret;
    }

    protected bool HasImage(object o)
    {
        Data.ArticleData art = (Data.ArticleData)o;
        if (art.IMG_ID == null)
            return false;

        if (ARTICLE.HasVideo(art.EMBEDD))
            return false;

        return true;


    }

    protected bool HasVideo(object o)
    {
        Data.ArticleData art = (Data.ArticleData)o;

        return ARTICLE.HasVideo(art.EMBEDD);
    }

    protected string RenderArtExposedClass(object item)
    {
        Data.ArticleData a = (Data.ArticleData)item;
        if (!a.EXPOSED)
            return "";

        return " exp";
    }
    protected string RenderVideo(object item)
    {
        Data.ArticleData art = (Data.ArticleData)item;
        string ret = "";


        ret = ARTICLE.Render.RenderVideoEmbed(art, 352, 215);

        return ret;
    }

}
