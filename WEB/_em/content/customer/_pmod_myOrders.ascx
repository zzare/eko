﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_pmod_myOrders.ascx.cs" Inherits="_em_content__customer_pmod_myOrders" %>
<%@ Register Namespace="SM.EM.UI.Controls" TagPrefix="EM"  %>
<%@ Register TagPrefix="SM" TagName="OrderList" Src="~/_em/content/order/_ctrl/_ctrlOrderList.ascx"   %>

<div class="myOrders">
<div class="stepHeaderW">
<h1>Moji nakupi</h1>
</div>

<div class="stepShoppingCart" >
    <div class="cont_headW">
        <span class="cont_headHeadT_Text">ODPRTI NAKUPI</span>
            <div class="cont_headMI">
                
                <SM:OrderList ID="objOrderListActive" runat="server" />
            
            </div>
    </div>
</div>

<div class="stepShoppingCart" >
    <div class="cont_headW">
        <span class="cont_headHeadT_Text">ZAKLJUČENI NAKUPI</span>
            <div class="cont_headMI">
            
                <SM:OrderList ID="objOrderListActiveFinished" runat="server" />
            
            </div>
    </div>
</div>
</div>