﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

namespace SM.UI.Controls
{
    public partial class _cmsUserEdit : BaseControl 
    {
        public event EventHandler OnUserChanged;
        public event EventHandler OnCancel;

        public Guid UID {
            get
            {
                Guid res = Guid.Empty;
                if (ViewState["UID"] == null) return res;
                if (!SM.EM.Helpers.IsGUID(ViewState["UID"].ToString())) return res;

                return new Guid(ViewState["UID"].ToString());
            }
            set{
                ViewState["UID"] = value;
            }
        }
        public Guid ProjectID { get; set; }
        public Guid SiteID { get; set; }
        public Guid GroupID { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {

            }

        }

        // default values
        protected void LoadDefaultValues() {
            UID = Guid.Empty;

            litCode.Text = "";

            ParentPage.mp.StatusText = "Add NEW USER";

            btDeleteUser.Visible = false;
        }



        // bind data
        public void BindData() {
            this.Visible = true;

            if (rblDiscount.Items.Count < 1) {
                rblDiscount.Items.Add(new ListItem("ENKRATEN", EM_USER.Common.DiscountType.OneTime.ToString()));
                rblDiscount.Items.Add(new ListItem("STALEN", EM_USER.Common.DiscountType.Permanent.ToString()));
            }

            if (ddlTheme.Items.Count <= 0) {
                ddlTheme.DataTextField = "THEME";
                ddlTheme.DataValueField= "THEME_ID";
                ddlTheme.DataSource = TEMPLATES.GetThemesAll();
                ddlTheme.DataBind();
            }


            if (UID == Guid.Empty)
            {
                LoadDefaultValues();
            }
            else {
                LoadData();
            
            }
            LoadRoles();

        }

        // LOAD Question
        protected void LoadData() {


            EM_USER usr = EM_USER.GetUserByID (UID);
            tbUserName.Text = usr.USERNAME;
            tbEmail.Text = usr.EMAIL;
            tbPageName.Text = usr.PAGE_NAME ;

            cbActive.Checked = usr.ACTIVE_USER;
            cbActiveWWW.Checked = usr.ACTIVE_WWW;
            cbDemo.Checked = usr.IS_DEMO;
            cbExampleTemplate.Checked = usr.IS_EXAMPLE_TEMPLATE;
            litCode.Text = usr.CODE.ToString("000000");
            //tbDiscountPercent.Text = usr.DISCOUNT_PERCENT.ToString();
            tbCustomCSS.Text = usr.CUSTOM_CSS_RULES;
            tbCustomDomain.Text = usr.CUSTOM_DOMAIN;
            //tbOrderedDomain.Text = usr.ORDERED_DOMAIN;

            tbGA.Text = usr.GA_TRACKER;

            //ddlOrderDomainStatus.SelectedValue = usr.ORDERED_DOMAIN_STATUS.ToString();

            rblDiscount.SelectedValue = usr.DISCOUNT_TYPE.ToString();

            tbMenuCount.Text = usr.SITEMAP_COUNT.ToString();

            ddlPortalID.SelectedValue = usr.PORTAL_ID.ToString();

            ddlTheme.SelectedValue = usr.THEME_ID.ToString(); 
            
            // set status text
            string status = "";
            status = "Managing USER: " + usr.PAGE_NAME ;

            panEdit.Visible = true;

            btDeleteUser.Visible = Roles.IsUserInRole("admin");

            // load bonus
            if (usr.BONUS_USER_ID != null) {
                string type = "";
                string href = "";
                string name = "";
                // check if is a campaign
                MARKETING_CAMPAIGN c = MARKETING_CAMPAIGN.GetCampaignByID(usr.BONUS_USER_ID.Value);
                if (c != null)
                {
                    type = "kampanja: ";
                    href = MARKETING_CAMPAIGN.Common.GetCampaignDetailsCMSHref(usr.BONUS_USER_ID.Value);
                    name = c.Name;
                }
                else {
                    type = "user affilliate - bonus";
                    href = EM_USER.Common.RenderCMSHref(usr.BONUS_USER_ID.Value);
                }
                litBonus.Text = string.Format("<a href=\"" + Page.ResolveUrl( href) +"\" >" + type + name  +"</a>");
            
            }



        }

        protected void LoadRoles() {
            // load roles if admin
            //if (SM.BLL.BizObject.IsAdmin)
            //{
                List<string> list = Roles.GetAllRoles().ToList();
                if (list.Contains("admin"))
                    if (!Roles.IsUserInRole("admin"))
                        list.Remove("admin");
                
            cblRoles.DataSource = list;
                cblRoles.DataBind();


                foreach (ListItem item in cblRoles.Items)
                {

                    if (Roles.IsUserInRole(tbUserName.Text, item.Value))
                        item.Selected = true;
                }

        }

        public void Save() {
            
            // validation
            Page.Validate("usredit");
            if (!Page.IsValid) return;

            
            eMenikDataContext db = new eMenikDataContext();

            EM_USER usr; 
            // add new user
            if (UID == Guid.Empty)
            {
                return;


            }
            else
            {
                usr = db.EM_USERs.SingleOrDefault(w=>w.UserId == UID );
            }



            usr.ACTIVE_USER = cbActive.Checked;
            usr.ACTIVE_WWW = cbActiveWWW.Checked;
            usr.IS_DEMO = cbDemo.Checked;
            usr.IS_EXAMPLE_TEMPLATE = cbExampleTemplate.Checked;
            usr.EMAIL = tbEmail.Text;
            usr.PAGE_NAME = tbPageName.Text;


            usr.CUSTOM_DOMAIN = tbCustomDomain.Text;
            //usr.ORDERED_DOMAIN = tbOrderedDomain.Text;
            //usr.ORDERED_DOMAIN_STATUS = short.Parse(ddlOrderDomainStatus.SelectedValue );
            usr.DISCOUNT_TYPE = short.Parse(rblDiscount.SelectedValue);

            usr.PORTAL_ID =(short) int.Parse( ddlPortalID.SelectedValue) ; 

            int discount = 0;
            if (!int.TryParse(tbDiscountPercent.Text, out discount))
                discount = 0;
            //usr.DISCOUNT_PERCENT = discount;

            if (string.IsNullOrEmpty(tbCustomCSS.Text.Trim()))
                usr.CUSTOM_CSS_RULES = null;
            else
                usr.CUSTOM_CSS_RULES = tbCustomCSS.Text.Trim();

            if (string.IsNullOrEmpty(tbGA.Text.Trim()))
                usr.GA_TRACKER = null;
            else
                usr.GA_TRACKER = tbGA.Text.Trim();


            // menu count
            int menuCount = 0;
            if (int.TryParse(tbMenuCount.Text, out menuCount))
            {
                usr.SITEMAP_COUNT = menuCount;
            }


            // save
            db.SubmitChanges();

            
            // save roles
            foreach(ListItem  item in cblRoles.Items  ){
                if (item.Selected) {
                    if (!Roles.IsUserInRole(tbUserName.Text, item.Value))
                        Roles.AddUserToRole(tbUserName.Text, item.Value);
                }
                else if (Roles.IsUserInRole(tbUserName.Text, item.Value))
                    Roles.RemoveUserFromRole(tbUserName.Text, item.Value);
            }


            // set status text
            ParentPage.mp.StatusText = "User has been SAVED ";

            // remove from cache
            usr.RemoveFromCache();


            
            // raise changed event
            OnUserChanged (this, new EventArgs());

        }

        // GROUP
        protected void btSave_Click(object sender, EventArgs e)
        {
            Save();
        }
        protected void btCancel_Click(object sender, EventArgs e)
        {
            //this.Visible = false;
            OnCancel(this, new EventArgs());
            // remove status text
            ParentPage.mp.StatusText = "";

            
        }
        protected void btDelete_Click(object sender, EventArgs e)
        {
            if (!Roles.IsUserInRole("admin")) return;

            //USER_DATA.DeleteUserData(UID);


            //Membership.DeleteUser(tbUserName.Text, true);

            // raise changed event
            OnUserChanged(this, new EventArgs());
        }

        protected void btClearCache_Click(object sender, EventArgs e)
        {
            EM_USER usr = EM_USER.GetUserByID(UID);
            usr.RemoveFromCache();
        }



        protected void btTheme_Click(object sender, EventArgs e)
        {
            EM_USER.ChangeDesign(UID , int.Parse(ddlTheme.SelectedValue));

            // raise changed event
            OnUserChanged(this, new EventArgs());
        }


}
}