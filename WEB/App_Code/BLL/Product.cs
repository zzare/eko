﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Security.Principal;
using System.Web.Caching;
using System.Net.Mail;
using System.Collections.Generic;
using System.Threading;
using System.Linq.Expressions;


/// <summary>
/// Summary description for PRODUCT
/// </summary>

public partial class PRODUCT
{

    public decimal GetPriceWithDiscount() {
        return Convert.ToDecimal(this.PRICE  - (( this.PRICE  * (decimal)this.DISCOUNT_PERCENT ) / 100));
    }
    public decimal GetPriceNoTaxWithDiscount()
    {
        return Convert.ToDecimal(this.PRICE_NO_TAX - ((this.PRICE_NO_TAX * this.DISCOUNT_PERCENT) / 100));
    }

    public decimal GetPriceWithDiscount(decimal price)
    {
        return Convert.ToDecimal(price - ((price * (decimal)this.DISCOUNT_PERCENT) / 100));
    }
    public decimal GetPriceNoTaxWithDiscount(decimal price)
    {
        return Convert.ToDecimal(GetPriceWithDiscount(price) * (decimal)(1 - this.TAX_RATE));
    }



    public string RenderStock() {
        return Common.RenderStock(this.UNITS_IN_STOCK);
    }

    public bool IsActive()
    {
        return (this.ACTIVE == true && this.DELETED == false);
    }


    // PREDICATES
    public static Expression<Func<PRODUCT, bool>> IsActiveP()
    {
        return p => (p.ACTIVE == true) && (p.DELETED == false);
    }
    public static Expression<Func<vw_Product, bool>> IsActiveVW()
    {
        return p => (p.ACTIVE == true) && (p.DELETED == false) && (p.UNITS_IN_STOCK > 0);
    }



    public bool IsValid
    {
        get { return (GetRuleViolations().Count() == 0); }
    }

    public IEnumerable<RuleViolation> GetRuleViolations()
    {
        if (this.ID_INT == null || this.ID_INT < 1)
        {
            // new product


        }
        else {

            if (string.IsNullOrEmpty(CODE.Trim()))
                yield return new RuleViolation("Šifra ni določena", "CODE");
            if (PRICE <= 0)
                yield return new RuleViolation("Cena ni določena", "PRICE");
        
            }

        // common
        if (SMAP_ID < 1)
            yield return new RuleViolation("Podkategorija izdelka ni izbrana", "SMAP_ID");
        if (PageId  == Guid.Empty )
            yield return new RuleViolation("Lastništvo izdelka ni določeno", "PageId");


        yield break;
    }

    partial void OnValidate(System.Data.Linq.ChangeAction action)
    {
        if (!IsValid)
            throw new ApplicationException("Podatki niso pravilno vneseni.");
    }



    public class ProductType
    {
        public const int NORMAL = 0;
        public const int NO_PRICE = 5;
        public const int PRICE_DEPENDS_ON_ATRIBUTE = 10;


    }

    public class TaxRate
    {
        public const double FOOD = 0.085;
        public const double OTHER = 0.2;
    }


    public class Common
    {
        public const string MAIN_GALLERY_ZONE = "prod_main_gal";

        public const bool EditShowSort = true;
        public const bool EditShowManufacturer = false;
        public const bool EditShowAdditionalList = false;

        public const bool ProductPriceHasTax = true;


        public static string RenderStock( int unitsInStock ) {
            string ret = "na zalogi";

            if (unitsInStock <= 0)
                ret = "3-5 dni";

            return ret;
        }

        public static int PageSizeDefault()
        {
            return int.Parse(ConfigurationSettings.AppSettings["ProductPageSize"]);

        }


    }


}


public partial class PRODUCT_DESC
{
    public bool IsValid
    {
        get { return (GetRuleViolations().Count() == 0); }
    }

    public IEnumerable<RuleViolation> GetRuleViolations()
    {
 

        // common
        if (this.LANG_ID.Length != 2)
            yield return new RuleViolation("Jezik ni pravilno določen", "LANG_ID");
        if (string.IsNullOrEmpty( this.NAME.Trim()) )
            yield return new RuleViolation("Vpiši naziv izdelka [" + this.LANG_ID + "]", "NAME");


        yield break;
    }

    partial void OnValidate(System.Data.Linq.ChangeAction action)
    {
        if (!IsValid)
            throw new ApplicationException("Podatki niso pravilno vneseni.");
    }




}
