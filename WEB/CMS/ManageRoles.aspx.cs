﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace SM.EM.UI.CMS
{
    public partial class CMS_ManageRoles : BasePage_CMS 
    {

        protected override void OnInit(EventArgs e)
        {
            CurrentSitemapID = 785;

            base.OnInit(e);

        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                LoadData();
            }

        }

        protected void LoadData() {

            lvList.DataSource = Roles.GetAllRoles().ToList();
            lvList.DataBind();
        }
 
        protected void lvList_ItemEditing(object sender, ListViewEditEventArgs e)
        {
            // hide insert item
            lvList.InsertItemPosition = InsertItemPosition.None;
        }
        protected void lvList_ItemUpdated(object sender, ListViewUpdatedEventArgs e)
        {
            // show insert item
            lvList.InsertItemPosition = InsertItemPosition.LastItem;

        }
        protected void lvList_ItemCanceling(object sender, ListViewCancelEventArgs e)
        {
            // show insert item
            lvList.InsertItemPosition = InsertItemPosition.LastItem;

        }
#region Validation
        protected void ValidateDuplicates(object sender, ServerValidateEventArgs e)
        {

            string id = "";

            if (lvList.EditIndex >= 0)
                id =  lvList.DataKeys[lvList.EditIndex].Value.ToString() ;
 
            string name = e.Value;

            if ( !Roles.RoleExists(e.Value)){
                e.IsValid = true;
                return;
            }
            e.IsValid = false;

            CustomValidator val = (CustomValidator)sender;
            val.ErrorMessage = "* Role '" + e.Value  + "' already exists.";
        }


#endregion

        protected void lvList_ItemInserting(object sender, ListViewInsertEventArgs e)
        {
            TextBox tbRole =(TextBox ) e.Item.FindControl("tbRole");

            if (!Roles.RoleExists(tbRole.Text ))
                Roles.CreateRole(tbRole.Text);

            // refresh list
            LoadData();
        }

        protected void lvList_ItemDeleteing(object sender, ListViewDeleteEventArgs e)
        {
            Literal litRole = (Literal)lvList.Items[e.ItemIndex].FindControl("litRole");

            if (Roles.RoleExists(litRole.Text))
            {
                Roles.RemoveUsersFromRole(Roles.GetUsersInRole(litRole.Text), litRole.Text);
                Roles.DeleteRole(litRole.Text);
            }

            // refresh list
            LoadData();
        }
}
}
