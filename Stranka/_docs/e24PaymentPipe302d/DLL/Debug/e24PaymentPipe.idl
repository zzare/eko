//////////////////////////////////////////////////////////////////////
//  e24PaymentPipe.idl
//
//  This file is the IDL source for e24PaymentPipe.dll.
//  
//  Copyright 1998 FullMoon Multi-Media, Inc.
//////////////////////////////////////////////////////////////////////
#include <olectl.h>

// This file will be processed by the MIDL tool to
// produce the type library (e24PaymentPipe.tlb) and marshalling code.
import "oaidl.idl";
import "ocidl.idl";

	//////////////////////////////////////////////////////////////
	// interface Ie24PaymentPipeCtl
	//
	// This is the interface to the e24PaymentPipe ActiveX control.
	//////////////////////////////////////////////////////////////
	[
		object,
		uuid(78EEF9ED-38E9-11D5-9B13-00E0B8184571),
		dual,
		helpstring("Ie24PaymentPipeCtl Interface"),
		pointer_default(unique)
	]
	interface Ie24PaymentPipeCtl : IDispatch
	{
		[id(1), helpstring("method PerformTransaction")] HRESULT PerformTransaction([out, retval] short* pResult);
		[propget, id(2), helpstring("property ErrorMsg")] HRESULT ErrorMsg([out, retval] BSTR *pVal);
		[propget, id(3), helpstring("property Result")] HRESULT Result([out, retval] BSTR *pVal);
		[propget, id(4), helpstring("property Auth")] HRESULT Auth([out, retval] BSTR *pVal);
		[propget, id(5), helpstring("property ResponseUrl")] HRESULT ResponseUrl([out, retval] BSTR *pVal);
		[propput, id(5), helpstring("property ResponseUrl")] HRESULT ResponseUrl([in] BSTR newVal);
		[propget, id(6), helpstring("property Avr")] HRESULT Avr([out, retval] BSTR *pVal);
		[propget, id(7), helpstring("property Date")] HRESULT Date([out, retval] BSTR *pVal);
		[propget, id(8), helpstring("property Ref")] HRESULT Ref([out, retval] BSTR *pVal);
		[propget, id(9), helpstring("property TransId")] HRESULT TransId([out, retval] BSTR *pVal);
		[propput, id(9), helpstring("property TransId")] HRESULT TransId([in] BSTR newVal);
		[propget, id(10), helpstring("property TrackId")] HRESULT TrackId([out, retval] BSTR *pVal);
		[propput, id(10), helpstring("property TrackId")] HRESULT TrackId([in] BSTR newVal);
		[propget, id(11), helpstring("property ID")] HRESULT ID([out, retval] BSTR *pVal);
		[propput, id(11), helpstring("property ID")] HRESULT ID([in] BSTR newVal);
		[propget, id(12), helpstring("property Password")] HRESULT Password([out, retval] BSTR *pVal);
		[propput, id(12), helpstring("property Password")] HRESULT Password([in] BSTR newVal);
		[propget, id(13), helpstring("property Action")] HRESULT Action([out, retval] BSTR *pVal);
		[propput, id(13), helpstring("property Action")] HRESULT Action([in] BSTR newVal);
		[propget, id(14), helpstring("property Amt")] HRESULT Amt([out, retval] BSTR *pVal);
		[propput, id(14), helpstring("property Amt")] HRESULT Amt([in] BSTR newVal);
		[propget, id(15), helpstring("property Udf1")] HRESULT Udf1([out, retval] BSTR *pVal);
		[propput, id(15), helpstring("property Udf1")] HRESULT Udf1([in] BSTR newVal);
		[propget, id(16), helpstring("property Udf2")] HRESULT Udf2([out, retval] BSTR *pVal);
		[propput, id(16), helpstring("property Udf2")] HRESULT Udf2([in] BSTR newVal);
		[propget, id(17), helpstring("property Udf3")] HRESULT Udf3([out, retval] BSTR *pVal);
		[propput, id(17), helpstring("property Udf3")] HRESULT Udf3([in] BSTR newVal);
		[propget, id(18), helpstring("property Udf4")] HRESULT Udf4([out, retval] BSTR *pVal);
		[propput, id(18), helpstring("property Udf4")] HRESULT Udf4([in] BSTR newVal);
		[propget, id(19), helpstring("property Udf5")] HRESULT Udf5([out, retval] BSTR *pVal);
		[propput, id(19), helpstring("property Udf5")] HRESULT Udf5([in] BSTR newVal);
		[propput, id(20), helpstring("property WebAddress")] HRESULT WebAddress([in] BSTR newVal);
		[propput, id(21), helpstring("property Port")] HRESULT Port([in] long newVal);
		[propput, id(22), helpstring("property PortStr")] HRESULT PortStr([in] BSTR newVal);
		[propget, id(23), helpstring("property PaymentPage")] HRESULT PaymentPage([out, retval] BSTR *pVal);
		[propget, id(24), helpstring("property PaymentId")] HRESULT PaymentId([out, retval] BSTR *pVal);
		[propput, id(24), helpstring("property PaymentId")] HRESULT PaymentId([in] BSTR newVal);
		[propget, id(25), helpstring("property RawResponse")] HRESULT RawResponse([out, retval] BSTR *pVal);
		[propput, id(26), helpstring("property Context")] HRESULT Context([in] BSTR newVal);
		[id(27), helpstring("method PerformInitTransaction")] HRESULT PerformInitTransaction([out, retval] short* pResult);
		[propget, id(28), helpstring("property Currency")] HRESULT Currency([out, retval] BSTR *pVal);
		[propput, id(28), helpstring("property Currency")] HRESULT Currency([in] BSTR newVal);
		[propget, id(29), helpstring("property ErrorUrl")] HRESULT ErrorUrl([out, retval] BSTR *pVal);
		[propput, id(29), helpstring("property ErrorUrl")] HRESULT ErrorUrl([in] BSTR newVal);
		[propget, id(30), helpstring("property Language")] HRESULT Language([out, retval] BSTR *pVal);
		[propput, id(30), helpstring("property Language")] HRESULT Language([in] BSTR newVal);
	};

	//////////////////////////////////////////////////////////////
	// library e24PaymentPipeLib
	//
	// This is the e24PaymentPipe type library
	//////////////////////////////////////////////////////////////
[
	uuid(78EEF9E1-38E9-11D5-9B13-00E0B8184571),
	version(1.0),
	helpstring("e24PaymentPipe 1.0 Type Library")
]
library e24PaymentPipeLib
{
	importlib("stdole32.tlb");
	importlib("stdole2.tlb");

	//////////////////////////////////////////////////////////////
	// dispinterface e24PaymentPipeEvents
	//
	// This is the interface of the connection points for events
	// fired by the e24PaymentPipe ActiveX control.
	//////////////////////////////////////////////////////////////
	[
		uuid(78EEF9EF-38E9-11D5-9B13-00E0B8184571),
		version(1.0),
		helpstring("e24PaymentPipe Event Interface")
	]
	dispinterface e24PaymentPipeEvents
	{
		properties:
		methods:
		[id(1)] void StatusMessage(BSTR* Msg);
	};
	//////////////////////////////////////////////////////////////
	// coclass e24PaymentPipeCtl
	//
	// This is the e24PaymentPipe Control class.
	//////////////////////////////////////////////////////////////
	[
		uuid(78EEF9EE-38E9-11D5-9B13-00E0B8184571),
		helpstring("e24PaymentPipe Control")
	]
	coclass e24PaymentPipeCtl
	{
		[default] interface Ie24PaymentPipeCtl;
		[default, source] dispinterface e24PaymentPipeEvents;
	};
};
