﻿<%@ Page Language="C#" MasterPageFile="~/CMS/CMS.master" AutoEventWireup="true" CodeFile="ManageTheme.aspx.cs" Inherits="SM.UI.CMS.EM.CMS_ManageTheme" Title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Namespace="SM.UI.Controls" TagPrefix="SM" %>

<asp:Content ID="Content3" ContentPlaceHolderID="cph" Runat="Server">

    <asp:UpdatePanel ID="upList" runat="server" ChildrenAsTriggers="true">
        <ContentTemplate>

            <asp:LinqDataSource ID="ldsList" runat="server" 
                ContextTypeName="eMenikDataContext" EnableDelete="True" EnableInsert="True" 
                EnableUpdate="True" TableName="THEMEs" 
                OnSelecting="ldsList_OnSelecting" oninserting="ldsList_Inserting" >
            </asp:LinqDataSource>
            
            <asp:LinqDataSource ID="ldsMaster" runat="server" 
                ContextTypeName="eMenikDataContext" TableName="MASTERs">
            </asp:LinqDataSource>
            
            <SM:smListView ID="lvList" runat="server" DataSourceID="ldsList" 
                DataKeyNames="THEME_ID" InsertItemPosition="LastItem" 
                onitemcanceling="lvList_ItemCanceling" onitemediting="lvList_ItemEditing" 
                onitemupdated="lvList_ItemUpdated" 
                 >
                <LayoutTemplate>
                    <table  class="tblData">
                            <thead>
                                <tr id="headerSort" runat="server">
                                    <th width="50px"><asp:LinkButton ID="LinkButton1" CommandName="sort" CommandArgument="THEME_ID" runat="server">ID</asp:LinkButton></th>
                                    <th ><asp:LinkButton ID="LinkButton3" CommandName="sort" CommandArgument="Theme" runat="server">Theme</asp:LinkButton></th>
                                    <th ><asp:LinkButton ID="LinkButton2" CommandName="sort" CommandArgument="THEME_DESC" runat="server">Desc</asp:LinkButton></th>
                                    <th ><asp:LinkButton ID="LinkButton4" CommandName="sort" CommandArgument="MASTER.MASTER_TITLE" runat="server">Master</asp:LinkButton></th>
                                    <th ><asp:LinkButton ID="LinkButton6" CommandName="sort" CommandArgument="IMG_BIG" runat="server">Thumb</asp:LinkButton></th>
                                    <th ><asp:LinkButton ID="LinkButton7" CommandName="sort" CommandArgument="IMG_THUMB" runat="server">Image</asp:LinkButton></th>
                                    <th width="50px"><asp:LinkButton ID="LinkButton5" CommandName="sort" CommandArgument="ORDER" runat="server">Order</asp:LinkButton></th>
                                    <th width="50px"><asp:LinkButton ID="LinkButton8" CommandName="sort" CommandArgument="ACTIVE" runat="server">Active</asp:LinkButton></th>
                                    <th width="50px"><asp:LinkButton ID="LinkButton9" CommandName="sort" CommandArgument="IS_PUBLIC" runat="server">IsPublic</asp:LinkButton></th>
                                    <th  colspan="2">Manage</th>
                                </tr>
                            </thead>
                            <tbody id="itemPlaceholder" runat="server"></tbody>
                            <tfoot>
                                <tr>
                                    <th style="text-align:right" colspan="11">
                                        <asp:DataPager runat="server" ID="DataPager" PageSize="120">
                                            <Fields>
                                                <asp:NextPreviousPagerField  />
                                                <asp:NumericPagerField ButtonCount="5" />
                                            </Fields>
                                        </asp:DataPager>
                                    </th>
                                </tr>
                            </tfoot>
                        </table>
                </LayoutTemplate>
                
                <ItemTemplate>
                    <tr  class='<%#  Container.DataItemIndex % 2 == 0 ? " " : " odd " %>'  onmouseover ="this.className='<%#  Container.DataItemIndex % 2 == 0 ? "hovRow" : "hovRowodd" %>'" onmouseout ="this.className = GetCssClass(<%#  Container.DataItemIndex %>)">
                        <td>
                            <%# Eval("THEME_ID")%>
                        </td>
                        <td>
                            <%# Eval("Theme")%>
                        </td>
                        <td>
                            <%# Eval("THEME_DESC") == null ? "" : SM.EM.Helpers.CutString(Eval("THEME_DESC").ToString(), 50, " ...")%>
                        </td>
                        <td>
                            <%# Eval("MASTER.MASTER_TITLE")%>
                        </td>
                        <td>
                            <%# Eval("IMG_THUMB") %>
                        </td>
                        <td>
                            <%# Eval("IMG_BIG") %>
                        </td>
                        <td>
                            <%# Eval("ORDER") %>
                        </td>
                        <td>
                            <asp:CheckBox ID="CheckBox1" runat="server" Enabled="false" Checked= ' <%# Eval("ACTIVE") %>' />
                        </td>
                        <td>
                            <asp:CheckBox ID="CheckBox2" runat="server" Enabled="false" Checked= ' <%# Eval("IS_PUBLIC") %>' />
                        </td>
                        <td >
                            <asp:LinkButton ID="lbEdit" CommandName="Edit" runat="server">edit</asp:LinkButton>
                        </td>
                        <td>
                            <asp:LinkButton ID="lbDelete" OnClientClick="javascript: return confirm('Are you sure you want to delete this item?');" CommandName="Delete" runat="server">delete</asp:LinkButton>
                        </td>                
                    </tr>        
                
                </ItemTemplate>
                
                <InsertItemTemplate>
                    <tr class="insertRow">
                        <td colspan="2">
                            <asp:TextBox MaxLength="256" Width="99%" ID="tbMaster" runat="server" Text='<%# Bind("Theme") %>'></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="512" Width="99%" ID="tbDesc" runat="server" Text='<%# Bind("THEME_DESC") %>'></asp:TextBox>
                        </td>
                        <td> 
                            <asp:DropDownList ID="ddlMaster" Width="99%" runat="server"  AppendDataBoundItems="true"  DataSourceID="ldsMaster" DataTextField="MASTER_TITLE" DataValueField="MASTER_ID">
                                <asp:ListItem Text=" - Select Masterpage - " Value="" ></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="256" Width="99%" ID="tbImgThumb" runat="server" Text='<%# Bind("IMG_THUMB") %>'></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="256" Width="99%" ID="tbImgBig" runat="server" Text='<%# Bind("IMG_BIG") %>'></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="9" Width="99%" ID="tbOrder" runat="server" Text='<%# Bind("ORDER") %>'></asp:TextBox>
                            <cc1:FilteredTextBoxExtender TargetControlID="tbOrder" ID="ftbeOrder" runat="server" FilterType="Numbers"></cc1:FilteredTextBoxExtender>
                        </td>
                        <td>
                            <asp:CheckBox ID="CheckBox1" runat="server"  Checked= '<%# Bind("ACTIVE") %>' />
                        </td>
                        <td>
                            <asp:CheckBox ID="CheckBox2" runat="server"  Checked= '<%# Bind("IS_PUBLIC") %>' />
                        </td>                        

                        <td colspan="2">
                            <asp:Button ID="btSave" CommandName="Insert" runat="server" Text="Add new" ValidationGroup="insert" ></asp:Button>
                        </td>
                    </tr>     
                    <tr class="errorRow">
                        <td colspan="2">
                            <asp:RequiredFieldValidator ValidationGroup="insert" ID="reqState" ControlToValidate="tbMaster" Display="Dynamic"  runat="server" ErrorMessage="* Enter theme"></asp:RequiredFieldValidator>
                            <asp:CustomValidator ValidationGroup="insert" ID="cvState" ControlToValidate="tbMaster" Display="Dynamic" runat="server" ErrorMessage="* Theme already exists" OnServerValidate="ValidateDuplicates"></asp:CustomValidator>
                        </td>
                        <td colspan="1">
                            <asp:RequiredFieldValidator ValidationGroup="insert" ID="RequiredFieldValidator2" ControlToValidate="tbDesc" Display="Dynamic"  runat="server" ErrorMessage="* Enter desc"></asp:RequiredFieldValidator>
                        </td>
                        <td colspan="3">
                            <asp:RequiredFieldValidator ValidationGroup="insert" ID="RequiredFieldValidator1" ControlToValidate="ddlMaster" Display="Dynamic"  runat="server" ErrorMessage="* Select masterpage"></asp:RequiredFieldValidator>
                        </td>
                        <td colspan="3">
                            <asp:RegularExpressionValidator ValidationGroup="insert" ID="regvRisk" ControlToValidate="tbOrder" ValidationExpression="^[0-9]+" Display="Dynamic" runat="server" ErrorMessage="* Enter valid order"></asp:RegularExpressionValidator>
                            <asp:RequiredFieldValidator ValidationGroup="insert" ID="rfvRisk" ControlToValidate="tbOrder" runat="server" ErrorMessage="* Enter order" Display="Dynamic" ></asp:RequiredFieldValidator>
                        </td>
                    </tr>   
                </InsertItemTemplate>
                
                <EditItemTemplate>
                
                    <tr class="editRow">
                       <td colspan="2">
                            <asp:TextBox MaxLength="256" Width="99%" ID="tbMaster" runat="server" Text='<%# Bind("Theme") %>'></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="512" Width="99%" ID="tbDesc" runat="server" Text='<%# Bind("THEME_DESC") %>'></asp:TextBox>
                        </td>
                        <td> 
                            <asp:DropDownList ID="ddlMasterE" Width="99%" runat="server" SelectedValue='<%# Bind("MASTER_ID") %>'  AppendDataBoundItems="true"  DataSourceID="ldsMaster" DataTextField="MASTER_TITLE" DataValueField="MASTER_ID">
                                <asp:ListItem Text=" - Select Masterpage - " Value="" ></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="256" Width="99%" ID="tbImgThumb" runat="server" Text='<%# Bind("IMG_THUMB") %>'></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="256" Width="99%" ID="tbImgBig" runat="server" Text='<%# Bind("IMG_BIG") %>'></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="9" Width="99%" ID="tbOrder" runat="server" Text='<%# Bind("ORDER") %>'></asp:TextBox>
                            <cc1:FilteredTextBoxExtender TargetControlID="tbOrder" ID="ftbeOrder" runat="server" FilterType="Numbers"></cc1:FilteredTextBoxExtender>
                        </td>
                        <td>
                            <asp:CheckBox ID="CheckBox1" runat="server"  Checked= '<%# Bind("ACTIVE") %>' />
                        </td>
                        <td>
                            <asp:CheckBox ID="CheckBox2" runat="server"  Checked= '<%# Bind("IS_PUBLIC") %>' />
                        </td>                        
                        <td >
                            <asp:Button ID="btSave" CommandName="Update" runat="server" Text="Save" ValidationGroup="edit" ></asp:Button>
                        </td>
                        <td >
                            <asp:Button ID="btCancel" CommandName="Cancel" runat="server" Text="Cancel" ></asp:Button>
                        </td>                
                    </tr>     
                    <tr class="errorRow">
                        <td colspan="2">
                            <asp:RequiredFieldValidator ValidationGroup="edit" ID="reqState" ControlToValidate="tbMaster" Display="Dynamic"  runat="server" ErrorMessage="* Enter theme"></asp:RequiredFieldValidator>
                            <asp:CustomValidator ValidationGroup="edit" ID="cvState" ControlToValidate="tbMaster" Display="Dynamic" runat="server" ErrorMessage="* Theme already exists" OnServerValidate="ValidateDuplicates"></asp:CustomValidator>
                        </td>
                        <td colspan="1">
                            <asp:RequiredFieldValidator ValidationGroup="edit" ID="RequiredFieldValidator2" ControlToValidate="tbDesc" Display="Dynamic"  runat="server" ErrorMessage="* Enter desc"></asp:RequiredFieldValidator>
                        </td>
                        <td colspan="3">
                            <asp:RequiredFieldValidator ValidationGroup="edit" ID="RequiredFieldValidator1" ControlToValidate="ddlMasterE" Display="Dynamic"  runat="server" ErrorMessage="* Select masterpage"></asp:RequiredFieldValidator>
                        </td>
                        <td colspan="3">
                            <asp:RegularExpressionValidator ValidationGroup="edit" ID="regvRisk" ControlToValidate="tbOrder" ValidationExpression="^[0-9]+" Display="Dynamic" runat="server" ErrorMessage="* Enter valid order"></asp:RegularExpressionValidator>
                            <asp:RequiredFieldValidator ValidationGroup="edit" ID="rfvRisk" ControlToValidate="tbOrder" runat="server" ErrorMessage="* Enter order" Display="Dynamic" ></asp:RequiredFieldValidator>
                        </td>
                    </tr>   
                </EditItemTemplate>
            
            </SM:smListView>

        </ContentTemplate>
    </asp:UpdatePanel>


</asp:Content>


