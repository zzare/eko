﻿<%@ Page ValidateRequest="false" Language="C#" MaintainScrollPositionOnPostback="false" MasterPageFile="~/_em/templates/fall/fall.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_em_settings_Default" Title="Untitled Page" %>
<%@ Register TagPrefix="EM" TagName="StepDesign" Src="~/_em/settings/_ctrl/step_design.ascx"  %>
<%@ Register TagPrefix="EM" TagName="StepModuli" Src="~/_em/settings/_ctrl/step_moduli.ascx"  %>
<%@ Register TagPrefix="EM" TagName="StepFooter" Src="~/_em/settings/_ctrl/step_footer.ascx"  %>
<%@ Register TagPrefix="EM" TagName="StepHeader" Src="~/_em/settings/_ctrl/step_header.ascx"  %>
<%@ Register TagPrefix="EM" TagName="StepLanguage" Src="~/_em/settings/_ctrl/step_language.ascx"  %>
<%@ Register TagPrefix="EM" TagName="StepMenu" Src="~/_em/settings/_ctrl/step_menu.ascx"  %>
<%@ Register TagPrefix="EM" TagName="StepAdvanced" Src="~/_em/settings/_ctrl/step_advanced.ascx"  %>
<%@ Register TagPrefix="EM" TagName="StepFinish" Src="~/_em/settings/_ctrl/step_finish.ascx"  %>

<%@ Register TagPrefix="SM" TagName="EditModulePublic" Src="~/_ctrl/module/_edit/editModulePublic.ascx"   %>



<asp:Content ID="Content1" ContentPlaceHolderID="cphWizzard" runat="server">

    <SM:EditModulePublic ID="objEditPublic" runat="server" />


    <div class="settNavCenterWrapp">
        <div id="divNavCenter" runat="server" class="settNavCenter">
            <span class="settNavCenter_sepa"></span>
<% if (SM.EM.Security.Permissions.IsAdvancedCms()){ %>
            <a <%= RenderHref(1) %>   onclick="<%= RenderClick(1) %>" class='<%= RenderClass(1) %>' >Oblika strani</a><span class="settNavCenter_sepa"></span>
            <a <%= RenderHref(2) %>  onclick="<%= RenderClick(2) %>" class='<%= RenderClass(2) %>' >Jeziki</a><span class="settNavCenter_sepa"></span>
<% } %>
            <a <%= RenderHref(3) %>  onclick="<%= RenderClick(3) %>" class='<%= RenderClass(3) %>' >Navigacija</a><span class="settNavCenter_sepa"></span>
<% if (SM.EM.Security.Permissions.IsAdvancedCms()){ %>
            <a <%= RenderHref(4) %>  onclick="<%= RenderClick(4) %>" class='<%= RenderClass(4) %>' >Motiv strani</a><span class="settNavCenter_sepa"></span>
            <a <%= RenderHref(5) %>  onclick="<%= RenderClick(5) %>" class='<%= RenderClass(5) %>' >Elementi</a><span class="settNavCenter_sepa"></span>
            <a <%= RenderHref(6) %>  onclick="<%= RenderClick(6) %>" class='<%= RenderClass(6) %>' >Podpis strani</a><span class="settNavCenter_sepa"></span>
            <a <%= RenderHref(7) %>   onclick="<%= RenderClick(7) %>" class='<%= RenderClass(7) %>' >Dodatne nastavitve</a><span class="settNavCenter_sepa"></span>
<% } %>

<%--            <asp:LinkButton EnableViewState="true" ID="btNav1" runat="server" class="btNav1" OnClick="NavLoadStep" CommandArgument="1" CausesValidation="false" ></asp:LinkButton>
            <asp:LinkButton EnableViewState="true" ID="btNav2" runat="server" class="btNav2" OnClientClick="highlight('#cLang a');" OnClick="NavLoadStep" CommandArgument="2" CausesValidation="false"></asp:LinkButton>
            <asp:LinkButton EnableViewState="true" ID="btNav3" runat="server" class="btNav3" OnClientClick="highlight('#cMenu a');" OnClick="NavLoadStep" CommandArgument="3" CausesValidation="false"></asp:LinkButton>
            <asp:LinkButton EnableViewState="true" ID="btNav4" runat="server" class="btNav4" OnClientClick="highlight('#cHeader img, #cLogo img');" OnClick="NavLoadStep" CommandArgument="4" CausesValidation="false"></asp:LinkButton>
            <asp:LinkButton EnableViewState="true" ID="btNav5" runat="server" class="btNav5" OnClientClick="highlight('#main');" OnClick="NavLoadStep" CommandArgument="5" CausesValidation="false"></asp:LinkButton>
            <asp:LinkButton EnableViewState="true" ID="btNav6" runat="server" class="btNav6" OnClientClick="highlight('#footer');" OnClick="NavLoadStep" CommandArgument="6" CausesValidation="false"></asp:LinkButton>
            <asp:LinkButton EnableViewState="true" ID="btNav7" runat="server" class="btNav7" OnClick="NavLoadStep" CommandArgument="7" CausesValidation="false"></asp:LinkButton>--%>
            <br />
        </div>
    </div>
    
    <div class="settNavBottomWrapp">
        <div id="divNavBottom" runat="server" class="settNavBottom">
            <div class="settNavBottomI">
            <div id="divStatMainWiz" runat="server">
 
                <h4><%= CurrentStepID  %>. KORAK: <%= CurrentStep.ShortDesc  %></h4>
                <%= CurrentStep.Description   %>
            </div>
            <div id="divStatMainNorm" runat="server" class="divStatMainNorm png">

                <h4><%= CurrentStep.ShortDesc  %></h4>
                <%= CurrentStep.Description   %>
            </div>
            </div>
        </div>
    </div>
    
    <div id="divError" class="settErrorWrap" runat="server" enableviewstate="false" visible="false">
        <div class="settError" >
            <asp:Literal ID ="litError" runat="server" EnableViewState = "false"></asp:Literal>
        </div>
    </div></asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="cph" Runat="Server">

<asp:ScriptManagerProxy ID="smProxy" runat="server">
<Services>
     <asp:ServiceReference Path="~/_inc/webservice/WebServiceEMENIK.asmx" />
</Services>
</asp:ScriptManagerProxy>

   
    <div class="wizz_main"> 

        <div class="wizz_settStatus">
            <a href="javascript:void(0)" class="btsettStatusHelp" style="display:none">Pomoč</a>
            <div class="settStatusWrapp" >
                <div id="divStatus" runat="server" class="settStatus"></div>
            </div>
        </div>

        <div class="wizz_main_steps">
            <asp:Panel ID="panNormalNavTop" runat="server">
                <div class="buttonwrapper">    
                    <asp:LinkButton EnableViewState="false" ID="btSaveTop" runat="server" CssClass="lbutton lbuttonConfirm" OnClick="btSave_Click"><span>Shrani spremembe</span></asp:LinkButton>
                </div> 
            </asp:Panel>
            
            <asp:Panel ID="panWizardNavTop" runat="server">
                <div class="buttonwrapper">    
                    <asp:LinkButton EnableViewState="false" ID="btPrevTop" runat="server" CssClass="lbutton lbuttonPrevious" OnClick="btPrev_Click" CausesValidation="false"><span>Nazaj</span></asp:LinkButton>
                    <asp:LinkButton EnableViewState="false" ID="btNextTop" runat="server" CssClass="lbutton lbuttonNext" OnClick="btNext_Click"><span>Naprej</span></asp:LinkButton>
                </div> 
            </asp:Panel>        
        
        
            <EM:StepDesign ID="StepDesign" runat="server" />
            <EM:StepModuli ID="StepModuli" runat="server" />
            <EM:StepFooter ID="StepFooter" runat="server" />
            <EM:StepHeader ID="StepHeader" runat="server" />
            <EM:StepLanguage ID="StepLanguage" runat="server" />
            <EM:StepMenu ID="StepMenu" runat="server" />
            <EM:StepAdvanced ID="StepAdvanced" runat="server" />
            <EM:StepFinish  ID="StepFinish" runat="server" />
            <br />
            <br />
            <asp:Panel ID="panNormalNav" runat="server">
                <div class="buttonwrapper">    
                    <asp:LinkButton ID="btSave" runat="server" CssClass="lbutton lbuttonConfirm" OnClick="btSave_Click"><span>Shrani spremembe</span></asp:LinkButton>
                </div> 
            </asp:Panel>
            
            <asp:Panel ID="panWizardNav" runat="server">
                <div class="buttonwrapper">    
                    <asp:LinkButton ID="btPrev" runat="server" CssClass="lbutton lbuttonPrevious" OnClick="btPrev_Click" CausesValidation="false"><span>Nazaj</span></asp:LinkButton>
                    <asp:LinkButton ID="btNext" runat="server" CssClass="lbutton lbuttonNext" OnClick="btNext_Click"><span>Naprej</span></asp:LinkButton>
                    <asp:LinkButton ID="btFinish" runat="server" CssClass="lbutton lbuttonNext" OnClick="btFinish_Click"><span>Končaj</span></asp:LinkButton>
                </div> 
            </asp:Panel>
        </div>
    </div>
</asp:Content>

