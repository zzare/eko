﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SM.EM.UI.Controls;

public partial class _em_settings__ctrl_step_footer : BaseWizardStep {

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        //Description = "Določite podatke, ki se prikazujejo v nogi spletne strani. Podatki se lahko razlikujejo od podatkov podjetja kamor se pošlje račun za spletno stran.";

        Description = "V nogi spletne strani se nahaja podpis. ";
        Description += "Vpiši svoje podatke in odkljukaj katere od teh želiš imeti objavljene na svoji spletni strani. Ti podatki so objavljeni v \"nogi\" vaše spletne strani, levo spodaj. ";

        //Description = "<ul><li>";
        //Description += "V tem koraku vpiši svoje podatke in odkljukajte katere od teh želite imeti objavljene na svoji spletni strani.Ti podatki so objavljeni v \"nogi\" vaše spletne strani, levo spodaj. ";
        //Description += "</li><li>";
        //Description += "Podatki se lahko razlikujejo od podatkov podjetja kamor se pošlje račun za spletno stran. ";
        //Description += "</li></ul><br/><br/>";

        ShortDesc = "Določi podatke v podpisu (nogi) spletne strani";

        //ShortDesc = "Določi podatke v nogi strani";

        StepID = 3;
        AllwaysShowSaveButton = true; // todo : remove

    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public override void LoadData()
    {
        // load data
        tbName.Text = Server.HtmlDecode(ParentPageEmenik.em_user.CONT_NAME);
        tbStreet.Text = Server.HtmlDecode(ParentPageEmenik.em_user.CONT_STREET);
        tbPostalCode.Text = Server.HtmlDecode(ParentPageEmenik.em_user.CONT_POSTAL_CODE);
        tbCity.Text = Server.HtmlDecode(ParentPageEmenik.em_user.CONT_CITY);
        tbConEmail.Text = Server.HtmlDecode(ParentPageEmenik.em_user.CONT_EMAIL);
        tbConPhone.Text = Server.HtmlDecode(ParentPageEmenik.em_user.CONT_PHONE);
        tbConFax.Text = Server.HtmlDecode(ParentPageEmenik.em_user.CONT_FAX);
        tbConMobile.Text = Server.HtmlDecode(ParentPageEmenik.em_user.CONT_MOBILE);


        // checkboxes
        cbName.Checked = ParentPageEmenik.em_user.SHOW_NAME;
        cbAddress.Checked = ParentPageEmenik.em_user.SHOW_ADDRESS;
        cbEmail.Checked = ParentPageEmenik.em_user.SHOW_EMAIL;
        cbPhone.Checked = ParentPageEmenik.em_user.SHOW_PHONE;
        cbFax.Checked = ParentPageEmenik.em_user.SHOW_FAX;
        cbMobile.Checked = ParentPageEmenik.em_user.SHOW_MOBILE;
    }

    public override bool SaveData(int step)
    {
        UpdateUserStep(step);

        eMenikDataContext db = new eMenikDataContext();

        EM_USER usr = db.EM_USERs.SingleOrDefault(w=>w.PAGE_NAME == ParentPageEmenik.PageName );
        
        usr.CONT_NAME = Server.HtmlEncode( tbName.Text);
        usr.CONT_STREET = Server.HtmlEncode(tbStreet.Text);
        usr.CONT_POSTAL_CODE = Server.HtmlEncode(tbPostalCode.Text);
        usr.CONT_CITY = Server.HtmlEncode(tbCity.Text);
        usr.CONT_EMAIL = Server.HtmlEncode(tbConEmail.Text);
        usr.CONT_PHONE = Server.HtmlEncode(tbConPhone.Text);
        usr.CONT_FAX = Server.HtmlEncode(tbConFax.Text);
        usr.CONT_MOBILE = Server.HtmlEncode(tbConMobile.Text);

        usr.SHOW_NAME = cbName.Checked;
        usr.SHOW_ADDRESS = cbAddress.Checked;
        usr.SHOW_EMAIL = cbEmail.Checked;
        usr.SHOW_PHONE = cbPhone.Checked;
        usr.SHOW_FAX = cbFax.Checked;
        usr.SHOW_MOBILE = cbMobile.Checked;

        db.SubmitChanges();

        usr.RemoveFromCache();
        //usr.AddToCache();

        return true;
        
    }

    public override bool Validate()
    {
        // enable validation of address only if show on page is shown
        rfvStreet.Enabled = cbAddress.Checked;



        Page.Validate("footer");
        if (!Page.IsValid)
            return false;
        return true;
            
    }

}
