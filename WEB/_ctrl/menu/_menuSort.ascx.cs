﻿using System;
using System.Collections;
using System.Collections.Generic ;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


namespace SM.UI.Controls
{
    public partial class _menuSort : BaseControl
    {
        public object Data;



        public int ParentID
        {
            get
            {
                int res = -1;
                if (ViewState["PARENT"] == null) return res;
                if (!int.TryParse(ViewState["PARENT"].ToString(), out res)) return res;

                return res;
            }
            set
            {
                ViewState["PARENT"] = value;
            }
        }
        public string  Language { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {


            if (!IsPostBack)
            {
            }

        }

        public void BindData() 
        {

            //object data = SITEMAP.GetSiteMapChildsByIDByLang(ParentID, Language );
            //divSortM.InnerHtml = ViewManager.RenderView("~/_ctrl/menu/_cntSortView.ascx", data, new object[]{ParentID });


        }

        protected string RenderItems() {

            object data = SITEMAP.GetSiteMapChildsByIDByLang(ParentID, Language);
            return ViewManager.RenderView("~/_ctrl/menu/_cntSortView.ascx", data, new object[] { ParentID });
        
        
        }

    }
}
