﻿
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewGalleryShow.ascx.cs" Inherits="SM.EM.UI.Controls._popup_viewGalleryShow" %>
<%@ Register Namespace="SM.UI.Controls" TagPrefix="SM"  %>


<script type="text/javascript">
    var currIndex = <%= CurrIndex %>;
    var data = <%= GetJsonData()  %>;
    var hl = 1;

    function galSelectImage(index) {
        currIndex = index;
        galLoadMainImage();

        galScrollThumbs(currIndex - hl);
    }
    function galLoadImage(step) {
        currIndex += step;
        if (currIndex > data.length - 1)
            currIndex = data.length - 1;
        if (currIndex < 0)
            currIndex = 0;

        galScrollThumbs(currIndex - hl);

        galLoadMainImage();

        return false;
    }
    
    function galLoadMainImage() {
        
        var imgB = $("#fb_imgGalBig")[0];
        if (data[currIndex]) {
            var pr = $("#fb_cProgress");
            var cont = $("#fb_cImgContent");

            pr.height(cont.height());
            cont.hide().css('display', 'none');
            $.fancybox.showActivity();
            
            //pr.show().css('display', 'block');            
            
            $(imgB).remove();
            var imgN = new Image();
            
            // title
            $("#fb_txtTitle").html(data[currIndex].Title);
            $("#fb_txtDesc").html(data[currIndex].Desc);
            
            if ((data[currIndex].Title == null || data[currIndex].Title == '')  && ( data[currIndex].Desc == null || data[currIndex].Desc == '') ){
                $('div.fb_txtData').hide();
            }
            else
            {
                $('.fb_txtData').show();
            }            
            
            
            $(imgN).hide().load(function () {
                $('div.fb_cGalImgBigImg').html(this);
                $(this).attr('id', 'fb_imgGalBig');
                galOnImageLoaded(this);
//                $(this).fadeIn();
//                $.fancybox.resize();
            }).attr('src', data[currIndex].UrlBig);


            imgN.alt = data[currIndex].Title;

            if (currIndex == 0) $("a.fb_aPrev").hide();
            else $("a.fb_aPrev").show();

            if (currIndex < data.length - 1) $("a.fb_aNext").show();
            else $("a.fb_aNext").hide();
            
        }
    }    
    
    function galScrollThumbs(pos) {
        var scrlIndex  = pos;
        if (scrlIndex < 0) scrlIndex = 0;
        $("#" + scrlIndex).click();
        //alert(scrlIndex);
        //carousel.scroll(jQuery.jcarousel.intval(scrlIndex ));

        $("#fb_cGalThumb img.fb_galImgThumb").removeClass('fb_selThumb').eq(currIndex).addClass('fb_selThumb');
    }    



    function galOnImageLoaded(img) {
         $('#fancybox-wrap').css('width','');
//        return;
        var pad = 20;
        if (img) {
        //alert('12');
            if (img.height <=0)
                return;
                $.fancybox.hideActivity();
                $('#fancybox-outer').css('width','');
            //alert('onImageLoaded');
            //$('#fancybox-outer').show();

//            $("#fb_cProgress").hide().css('display','none');
            $("#fb_cImgContent").show().css('display', 'block').fadeIn();
//            $("#fb_cImgContent").fadeIn();

            $(img).fadeIn();

            
            var hov = $('#fb_galHovNav')[0];
            hov.style.height = img.height + 'px';
            hov.style.top = img.offsetTop + 'px';
            hov.style.left = img.offsetLeft + 'px';
            hov.style.width = img.width + 'px';   
            
            
            var l =  ( $(window).width() - (img.width + pad) ) / 2+$(window).scrollLeft();
            //var h = img.height + pad;
            var w = img.width + pad;
            
            var cw = $("div.fb_jCarousel").width();
            var cl = (img.width / 2) - (cw / 2); 
            
            
            $("#fancybox-wrap, #fancybox-outer").css({'width': w + 'px'});
            $("div.fb_txtData").css({'width': img.width + 'px'});
            
            $("#fancybox-wrap").css({'left': l + 'px'});


            
            // reinit hover
            var hovnc = $("#fb_galHovNav");
            hovnc.html(hovnc.html());
            
                         
        }
            
        if (data.length == 1)
             $("#fancybox-wrap").css({'top': '10px'});
        else
        $("#fancybox-wrap").css({'top': '140px'});

            $("#fancybox-wrap").css({'height': $('div.fb_cPopupGal').height() + pad + 'px'});
        
        $("div.fb_jCarousel").css({'left': cl + 'px'});
        
        //ie6
        if ($.browser.msie && $.browser.version.substr(0, 1) < 7) {
            $("#fb_hovPrev").css({'height': img.height + 'px'});
            $("#fb_hovNext").css({'height': img.height + 'px'});
        } 
        //$.fancybox.center();
    }
    
    
    
    function galLoadCarousel() {
        var cVisible = 5;

        //alert('galLoadCarousel');
        $('#fancybox-outer').css('width','0px');
        $('#fancybox-wrap').css('width', '0px');

        //alert('onGalLoadCarousel');

        $.fancybox.showActivity();

        var strt = currIndex - hl;
        if (strt < 0) strt = 0;


        //alert(data.length);

        if (data.length == 1){
            $('div.fb_jCarouselGal').hide();
        }
        else {

            carousel = $('#fb_cGalThumb').jcarousel({ start: strt, visible: cVisible, scroll: 5, animation: 400, /*auto:15 ,*/
            initCallback: galC_initCallback

        });


                if (data.length <= cVisible){
                    cVisible = data.length;
                    var _w = 130*cVisible;
                    $('.fb_jCarousel .jcarousel-skin-tango .jcarousel-clip-horizontal, .fb_jCarousel .jcarousel-skin-tango .jcarousel-container-horizontal').css('width', _w + 'px');
                    $('.fb_jCarousel, .fb_jCarouselGal').css('width', (_w + 20) +  'px');
                    $('.jcarousel-prev, .jcarousel-next').hide();

                }
//            }
//        else{



        $("#fb_cGalThumb img.fb_galImgThumb").removeClass('fb_selThumb').eq(currIndex).addClass('fb_selThumb');
        }

        //$('#fancybox-outer').hide();
        // reposition hover - first time load
        galOnImageLoaded($("#fb_imgGalBig")[0]);

    }

    function galC_initCallback(carousel) {
//        $("#cPrev").click(function() { carousel.prev(); return false; });
//        $("#cNext").click(function() { carousel.next(); return false; });

        for (var i = 0; i < data.length; i++) {
            //            btgo[i] = "#" + i;
            var a = $("<a ></a>").attr({ id: i + '' }).hide();
            $("#fb_cGalImgBig").append(a);
            a.bind('click', function() {
                carousel.scroll(jQuery.jcarousel.intval(this.id));
                return false;
            });
        }
        //alert('resize - init');
        //$.fancybox.resize();

        $('div.fb_jCarousel').hide().css('position', '').fadeIn();
    } 


</script>

<div class="fb_cPopupGalWrapp" >




    <div id="fb_cPopupGal" class="fb_cPopupGal">    
<%--        <div class="fb_galData">
            <span id="fb_galTitle"></span>    <br />
            <span id="fb_galDesc"></span>
        </div>
--%>        
        <div style="position:absolute; left:-9999px" class="fb_jCarousel">
                <div  class="fb_jCarouselGal">
                    <div id="popupedge-bg-n" class="popupedge-bg"></div>
                    <div id="popupedge-bg-ne" class="popupedge-bg"></div>
                    <div id="popupedge-bg-e" class="popupedge-bg"></div> 
                    <div id="popupedge-bg-se" class="popupedge-bg"></div>
                    <div id="popupedge-bg-s" class="popupedge-bg"></div>
                    <div id="popupedge-bg-sw" class="popupedge-bg"></div>
                    <div id="popupedge-bg-w" class="popupedge-bg"></div>
                    <div id="popupedge-bg-nw" class="popupedge-bg"></div>
                    <ul  id="fb_cGalThumb" class="jcarousel-skin-tango jcarousel-skin-galpopup" >
                        <%-- tukej se not nafilajo thumbnail slike  --%> 
                        <asp:ListView ID="lvList" runat="server" InsertItemPosition="None"  >
                            <LayoutTemplate>
                                <div id="itemPlaceholder" runat="server"></div>
                                    
                            </LayoutTemplate>
                            
                            <ItemTemplate>
                                <li >
                                    <a id="aC<%# Container.DataItemIndex.ToString()  %>" href="javascript:galSelectImage(<%# Container.DataItemIndex.ToString()  %>);">
                                        <img class="fb_galImgThumb" alt='<%# Eval("Title") %>' src='<%# SM.BLL.Common.ResolveUrl( Eval("UrlThumb").ToString()) %>' />
                                    </a>
                                </li>

                            </ItemTemplate>
                            
                        
                        </asp:ListView>                    
                    </ul>
                </div> 
            <div class="clearing"></div>
        </div>
        <div class="clearing"></div>
<%--        <div class="fb_imagePN">
            <div class="fb_imagePNleft">
                <a class="fb_pPrev fb_aPrev" onclick="LoadImage(-1); return false" href="#" ><%= GetGlobalResourceObject("Modules", "PreviousPhoto")%></a>
            </div>
            <div class="fb_imagePNright">
                 <a class="fb_pNext fb_aNext" onclick="LoadImage(1); return false" href="#" ><%= GetGlobalResourceObject("Modules", "NextPhoto")%></a>
            </div>
            <div class="clearing"></div>
        </div>--%>
        
        <div id="fb_cImgContent" style="display:none">
            <%-- big image  --%> 
            <div id="fb_cGalImgBig">
                <div class="fb_cGalImgBigImg" >
                    <img style="" id="fb_imgGalBig" src='<%= RenderMainImageSrc() %>' />
                </div>
            </div>
            <%-- image hover buttons  --%> 
            
            <div id="fb_galHovNav" >
                <a id="fb_hovPrev" class="fb_aPrev" title="&laquo; <%= GetGlobalResourceObject("Modules", "PreviousPhoto") %>" onclick="galLoadImage(-1); return false" href="#"  ><span id="fb_hovPrev_ico"></span></a>
                <a id="fb_hovNext" class="fb_aNext" title="<%= GetGlobalResourceObject("Modules", "NextPhoto") %> &raquo;"  onclick="galLoadImage(1); return false" href="#" ><span id="fb_hovNext_ico"></span></a>
            </div>
            <div class="clearing"></div>
            
            <div class="fb_txtData">
                <span id="fb_txtTitle"></span><br />
                <span id="fb_txtDesc"></span>
            </div>      
            
<%--            <div class="btnwrappModalGallery">
                <a class="fb_btClose" href="#" onclick="return false;"></a>
            </div>--%>
        </div>
<%--        <a id="fb_hovPrev" class="fancybox-left" title="&laquo; <%= GetGlobalResourceObject("Modules", "PreviousPhoto") %>" onclick="galLoadImage(-1); return false" href="#"  ><span>&nbsp;</span></a>
        <a id="fb_hovNext" class="fancybox-title" title="<%= GetGlobalResourceObject("Modules", "NextPhoto") %> &raquo;"  onclick="galLoadImage(1); return false" href="#" ><span>&nbsp;</span></a>
--%>
        <div id="fb_cProgress" style="display:none;">
            <img id="fb_imgProgress" style="" src='<%= SM.BLL.Common.ResolveUrl("~/cms/_res/images/icon/ajax-loader.gif") %>' />
            
        </div>
        
    </div>
</div>