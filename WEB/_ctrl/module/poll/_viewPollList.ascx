﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewPollList.ascx.cs" Inherits="_ctrl_module_poll_viewPollList" %>


    <asp:ListView ID="lvList" runat="server" >
        <LayoutTemplate>
            
                <asp:PlaceHolder ID="itemPlaceholder" runat="server"/>

        </LayoutTemplate>
        
        <ItemTemplate>

            <div class="poll_render">
                <%# RenderPoll(Container.DataItem ) %>
            </div>
                       
        </ItemTemplate>
    </asp:ListView>

    <asp:PlaceHolder ID="phEmpty" runat="server" Visible="false">
        Dodane ni nobene ankete.

    </asp:PlaceHolder>

