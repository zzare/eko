﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SM.EM.BLL;
using SM.UI.Controls;
using System.Globalization;

namespace SM.EM.UI.Controls
{
    public partial class cwpMap : BaseControl_CmsWebPart              
    {
        public int ViewHeight { get; set; }
        public int EditHeight { get; set; }
        public const int MIN_HEIGHT = 200;


        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            // init content
            //if (cwp != null)
            //    objContent.CwpID = cwp.CWP_ID;
            //objContent.AutoLoad = false;


        }

 
        public override void LoadData()
        {
            base.LoadData();


            // init header
            InitEditMode();

            // load data
            LoadContent();
        }


        protected void InitEditMode() {


            // edit mode
            if (ParentPage.IsModeCMS )
            {
                divEdit.Visible = true;
                panSettings.Visible = true;
                //objContent.HideInactive = false;
                mpeMapEdit.BehaviorID = "mpeMapEdit" + cwp.CWP_ID.ToString();


               // button actions
                btShow.HRef = "javascript:onToggleCwpActive('" + btShow.ClientID + "','" + btHide.ClientID + "', '" + cwp.CWP_ID.ToString() + "' , true);";
                btHide.HRef = "javascript:onToggleCwpActive('" + btShow.ClientID + "','" + btHide.ClientID + "','" + cwp.CWP_ID.ToString() + "', false);";

                // initial hiding of buttons
                if (cwp.ACTIVE)
                {
                    btShow.Attributes["style"] = "display:none";
                    btHide.Attributes["style"] = "display:";
                }
                else
                {
                    btShow.Attributes["style"] = "display:";
                    btHide.Attributes["style"] = "display:none";
                }

            }
            else
            {
                divEdit.Visible = false;
                panSettings.Visible = false ;

                //objContent.HideInactive = true;
            }


            // title
            litTitle.Text = cwp.TITLE;
            divTitleContent.Visible = cwp.SHOW_TITLE;

        }


        protected void LoadContent() {


            CWP_MAP map = CMS_WEB_PART.GetCwpMapByID(cwp.CWP_ID);
            if (map == null) return;



            ViewHeight = map.HEIGHT ?? MIN_HEIGHT  ;
            EditHeight = ViewHeight + 300;

            CultureInfo usc = new CultureInfo("en-US");

            string init = "";

            double  lat, lng, mLat, mLng;
            int zoom, height, width;

            lat = map.LAT;
            lng = map.LNG;
            mLat = map.MARKER_LAT ?? 0;
            mLng = map.MARKER_LNG ?? 0;
            zoom = map.ZOOM;    
            height = map.HEIGHT ?? -1;
            width = map.WIDTH ?? -1;

            string initIframe = string.Format(" loadMapView('{0}', '{1}'); ", cwp.CWP_ID, Page.ResolveUrl(CWP_MAP.Common.RenderMapViewHref(cwp.CWP_ID, Page.Theme )));

            init = string.Format(" map_load('{0}', $('#{1}').find(\"#map\")[0], parseFloat('{2}'), parseFloat('{3}'), {4}, parseFloat('{5}'), parseFloat('{6}'), GetMarkerHtml('{7}', '{8}'), {9}, {10}, {11}, '{12}'); ",
                 map.CWP_ID,
                 map.CWP_ID,
                 string.Format(usc, "{0:F16}", lat),
                 string.Format(usc, "{0:F16}", lng ),
                 zoom ,
                 string.Format(usc, "{0:F16}", mLat ),
                 string.Format(usc, "{0:F16}", mLng ),
                 map.TITLE,
                 map.ADDRESS,
                 height ,
                 -1,
                 map.SHOW_INFO.ToString() .ToLower(),
                 map.MAP_TYPE 
                 );


                if (map.LAT == 0 || map.LNG == 0)
                    //init += string.Format("showAddress('{1}', '{2}');", map.CWP_ID, map.SRCH_ADDRESS, map.TITLE, map.ADDRESS);
                init += string.Format("editID = '{0}'; showAddress('{1}', GetMarkerHtml('{2}', '{3}') );", map.CWP_ID, map.SRCH_ADDRESS, map.TITLE, map.ADDRESS);


                ParentPage.RegisterDocumentReadyScriptEnd += initIframe;
        }





        //protected void btDelete_Click(object sender, EventArgs e) {
        //    if (!ParentPage.CanSave) return;

        //    cwp.Delete();    
        
        //    // reload zone
        //    ParentPage.ReLoadZone(ParentPage.Zones[cwp.ZONE_ID], true);
        //}

        protected void btSettings_Click(object sender, EventArgs e)
        {
            CwpEdit edit = ParentPage.MapSettings ;
            if (edit == null) return;

            edit.cwp = cwp;
            edit.CwpID = cwp.CWP_ID;
            edit.LoadData();
            edit.Zone = cwp.ZONE_ID;
        }
    }
}