﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_cntFileList.ascx.cs" Inherits="_cntFileList" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Namespace="SM.UI.Controls" TagPrefix="SM" %>

<SM:InlineScript runat="server">
<script type="text/javascript" >
    $(document).ready(function() {

    $("table.fIcon tbody tr td:first-child a").addClass("file"); // default class for files

    $("table.fIcon tbody a[@innerHTML$=pdf]").removeClass().addClass("pdf");
    $("table.fIcon tbody a[@innerHTML$=xls]").removeClass().addClass("xls");
    $("table.fIcon tbody a[@innerHTML$=zip]").removeClass().addClass("zip");

    $("table.fIcon tbody a[@innerHTML$=doc]").removeClass().addClass("doc");
    $("table.fIcon tbody a[@innerHTML$=docx]").removeClass().addClass("doc");

    $("table.fIcon tbody a[@innerHTML$=png]").removeClass().addClass("jpg");
    $("table.fIcon tbody a[@innerHTML$=bmp]").removeClass().addClass("jpg");
    $("table.fIcon tbody a[@innerHTML$=gif]").removeClass().addClass("jpg");
    $("table.fIcon tbody a[@innerHTML$=jpg]").removeClass().addClass("jpg");
    
    
});

</script>
</SM:InlineScript>

    <div class="cFileList">
            
            <SM:smListView ID="lvList" runat="server" InsertItemPosition="None" >
                <LayoutTemplate>
                    <table class="tblData fIcon" cellpadding="0" cellspacing="0">
                            <thead>
                                <tr id="headerSort" runat="server">
                                    <th >DATOTEKA</th>
                                    <th >DATUM</th>
                                    <th >VELIKOST</th>
                                </tr>
                            </thead>
                            <tbody id="itemPlaceholder" runat="server"></tbody>
                        </table>
                </LayoutTemplate>
                
                <ItemTemplate>
                    <tr  class='<%#  Container.DataItemIndex % 2 == 0 ? "  " : " odd " %>'  onmouseover ="this.className='<%#  Container.DataItemIndex % 2 == 0 ? "hovRow" : "hovRowodd" %>'" onmouseout ="this.className = GetCssClass(<%#  Container.DataItemIndex %>)">
                        <td>
                            <a style="display:block;" target="_blank" href='<%# Page.ResolveUrl("~") + Eval("FileID").ToString() + ".secFile" %>' ><%# Eval("Name")%></a>
                        </td>
                        <td>
                            <%#  SM.EM.Helpers.FormatDate(Eval("DateAdded"))%>
                        </td>
                        <td>
                            <%# (decimal.Parse(Eval("Size").ToString()) / 1024).ToString("###,###.##")%> KB
                        </td>
                    </tr>  
                           
                
                </ItemTemplate>
                
                <EmptyDataTemplate>
                    
                    <div class="odd">
                        ni datotek.
                
                    </div>
                    
                </EmptyDataTemplate>
                
            
            </SM:smListView>

    </div>
