﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _em_content__ctrl_pmodOneColumn : BaseControl 
{

    protected void Page_Load(object sender, EventArgs e)
    {
        mnMain.CurrentWebPartZone = "wpz_1";
        
    }



    protected string RenderDescription() {
        string ret = "";

        if (ParentPage.CurrentSitemapLang == null)
            return ret;
        
        
        if (!string.IsNullOrWhiteSpace(ParentPage.CurrentSitemapLang.SML_META_DESC))
            ret = "<p class='p_catdesc'>" + ParentPage.CurrentSitemapLang.SML_META_DESC + "</p>";

        return ret;
    }
}
