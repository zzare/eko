﻿<%@ Page Language="C#" MasterPageFile="~/CMS/CMS.master" AutoEventWireup="true" CodeFile="ManageCampaign.aspx.cs" Inherits="SM.UI.CMS.ManageCampaign" Title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register TagPrefix="SB" TagName="CampaignEdit" Src="~/CMS/controls/marketing-campaign/_cmsCampaignEdit.ascx"   %>
<%@ Register TagPrefix="SB" TagName="UserFilter" Src="~/CMS/controls/user/_cmsUserFilter.ascx"   %>
<%@ Register TagPrefix="SB" TagName="CampaignList" Src="~/CMS/controls/marketing-campaign/_cmsCampaignList.ascx"   %>

<asp:Content ID="c1" ContentPlaceHolderID="cphNavL" runat="server">
    
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cph" Runat="Server">

            <asp:Panel ID="panSListHD" runat="server" CssClass="cpeListHD" >
                <asp:Image ID="imgCollapse" runat="server" />
               CAMPAIGN list
            </asp:Panel>

            <asp:Panel ID="panSList" runat="server">

                <div class="qFilter">
                    <div class="floatR">
                        <SB:UserFilter ID="objFilter" runat="server" />
                    </div>
                </div>

                <SB:CampaignList ID="objList" runat="server" ShowCheckboxColumn="false" ShowManageColumn="true" ShowEditColumn="true" />
            </asp:Panel>    

            <cc1:CollapsiblePanelExtender ID="cpeQList" runat="server" Collapsed="false" CollapseControlID="panSListHD" CollapsedImage="~/_res/images/icon/plus.gif" ExpandedImage="~/_res/images/icon/minus.gif" ImageControlID="imgCollapse" ExpandControlID="panSListHD" TargetControlID="panSList" ExpandDirection="Vertical" CollapsedText="Expand" ExpandedText="Collapse" >
            </cc1:CollapsiblePanelExtender>    
                

        <asp:Button ValidationGroup="marketing" ID="btNew" runat="server" Text="NEW CAMPAIGN" onclick="btNew_Click" CssClass="btnOrange " />

    
    <br />

                        
    <SB:CampaignEdit   id="objEdit" runat="server" />



</asp:Content>