//////////////////////////////////////////////////////////////////////
//  Ie24PaymentPipe.h
//
//  This file contains the public C++ interfaces to the
//  e24PaymentPipe C++ component.
//  
//  Copyright 1998 FullMoon Multi-Media, Inc.
//////////////////////////////////////////////////////////////////////

#ifndef _IE24PAYMENTPIPE_
#define _IE24PAYMENTPIPE_

//////////////////////////////////////////////////////////////////////
//  class Ie24PaymentPipeStatus
//
//  Purpose:  This is a pure virtual interface.  Any class that wants
//            asynchronous status notifications from the e24PaymentPipe
//            C++ component must derive from this class and implement
//            its methods.
//
//////////////////////////////////////////////////////////////////////
class Ie24PaymentPipeStatus
{
public:
	// this is the method that is called on an object that receives
	// asynchronous status messages during a transaction.
	virtual void OnStatusUpdate(const char* pMsg) = 0;
};

//////////////////////////////////////////////////////////////////////
//  class Ie24PaymentPipe
//
//  Purpose:  This is a pure virtual interface.  It is implemented by
//            the C++ e24PaymentPipe component.It provides methods for
//            accessing the setup parameters and results, all as
//            properties of the object.  An instance of the e24PaymentPipe
//            C++ component is created using the factory method named
//            Createe24PaymentPipe, in this header file.  An instance of
//            this object can NOT be created using the new operator.
//            Likewise, the delete operator can not be used to delete
//            and instance of the object.  Instead, call the Release
//            method to delete an instance of the object.
//
//////////////////////////////////////////////////////////////////////
class Ie24PaymentPipe
{
public:
	// This method is used to release the instance of the object.  Remember
	// that the delete operator can not be used.
	virtual void Release() = 0;

	// set the WebAddress
	virtual void SetWebAddress(LPCSTR webAddress) = 0;

	// set the Port
	virtual void SetPort(long port) = 0;

	// set the PortStr
	virtual void SetPortStr(LPCSTR portStr) = 0;

	// set the Web server context
	virtual void SetContext(LPCSTR context) = 0;

	// set/get the ID
	virtual void SetID(LPCSTR id) = 0;
	virtual char* GetID() = 0;

	// set/get the PASSWORD
	virtual	void SetPassword(LPCSTR password) = 0;
	virtual	char* GetPassword() = 0;
	
	// set/get the Response URL
	virtual	void SetResponseUrl(LPCSTR responseurl) = 0;
	virtual	char* GetResponseUrl() = 0;
	
	// set/get the Error URL
	virtual	void SetErrorUrl(LPCSTR errorurl) = 0;
	virtual	char* GetErrorUrl() = 0;
	
	// get the Payment Page
	virtual	char* GetPaymentPage() = 0;
	
	// set/get the ACTION
	virtual	void SetAction(LPCSTR action) = 0;
	virtual	char* GetAction() = 0;
	
	// set/get the Payment Id
	virtual	void SetPaymentId(LPCSTR paymentid) = 0;
	virtual char* GetPaymentId() = 0;
	
	// set/get the TRANSID
	virtual	void SetTransId(LPCSTR transid) = 0;
	virtual char* GetTransId() = 0;
	
	// set/get the AMT
	virtual	void SetAmt(LPCSTR amt) = 0;
	virtual	char* GetAmt() = 0;
	
	// set/get the Currency
	virtual	void SetCurrency(LPCSTR currency) = 0;
	virtual	char* GetCurrency() = 0;
	
	// set/get the Language
	virtual	void SetLanguage(LPCSTR langauge) = 0;
	virtual	char* GetLanguage() = 0;
	
	// set/get the TRACKID
	virtual	void SetTrackId(LPCSTR trackid) = 0;
	virtual char* GetTrackId() = 0;

	// set/get the UDF1
	virtual	void SetUdf1(LPCSTR udf1) = 0;
	virtual char* GetUdf1() = 0;

	// set/get the UDF2
	virtual	void SetUdf2(LPCSTR udf2) = 0;
	virtual char* GetUdf2() = 0;

	// set/get the UDF3
	virtual	void SetUdf3(LPCSTR udf3) = 0;
	virtual char* GetUdf3() = 0;

	// set/get the UDF4
	virtual	void SetUdf4(LPCSTR udf4) = 0;
	virtual char* GetUdf4() = 0;

	// set/get the UDF5
	virtual	void SetUdf5(LPCSTR udf5) = 0;
	virtual char* GetUdf5() = 0;

	// get the RESULT
	virtual char* GetResult() = 0;

	// get the AUTH
	virtual char* GetAuth() = 0;
	
	// get the AVR
	virtual char* GetAvr() = 0;
	
	// get the DATE
	virtual char* GetDate() = 0;
	
	// get the REF
	virtual char* GetRef() = 0;
	
	// get the Error Message
	virtual char* GetErrorMsg() = 0;

	// get the raw response
	virtual char* GetRawResponse() = 0;

	// This method is used to perform the transaction.  Pass a NULL
	// for pStatus if asynchronous status notifications aren't desired.
	// returns 0 if successfull, -1 otherwise.
	virtual short PerformInitTransaction(Ie24PaymentPipeStatus* pStatus) = 0;

	// This method is used to perform the transaction.  Pass a NULL
	// for pStatus if asynchronous status notifications aren't desired.
	// returns 0 if successfull, -1 otherwise.
	virtual short PerformTransaction(Ie24PaymentPipeStatus* pStatus) = 0;

};

//////////////////////////////////////////////////////////////////////
//  function Createe24PaymentPipe
//
//  Purpose:  This is the factory function used to create an instance
//            of the e24PaymentPipe C++ component.
//
//////////////////////////////////////////////////////////////////////
Ie24PaymentPipe* Createe24PaymentPipe();


//////////////////////////////////////////////////////////////////////
//  typedef SCFACTORY
//
//  Purpose:  This is the typedef of the function pointer to the
//            Createe24PaymentPipe function.  It can be used by applications
//            that load this DLL dynamically.
//
//////////////////////////////////////////////////////////////////////
typedef Ie24PaymentPipe* (WINAPI *SCFACTORY)();

#endif

