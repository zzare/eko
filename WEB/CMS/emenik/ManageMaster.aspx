﻿<%@ Page Language="C#" MasterPageFile="~/CMS/CMS.master" AutoEventWireup="true" CodeFile="ManageMaster.aspx.cs" Inherits="SM.UI.CMS.EM.CMS_ManageMaster" Title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Namespace="SM.UI.Controls" TagPrefix="SM" %>

<asp:Content ID="Content3" ContentPlaceHolderID="cph" Runat="Server">

    <asp:UpdatePanel ID="upList" runat="server" ChildrenAsTriggers="true">
        <ContentTemplate>

            <asp:LinqDataSource ID="ldsImageType" runat="server" ContextTypeName="eMenikDataContext" TableName="IMAGE_TYPEs">
            </asp:LinqDataSource>

            <asp:LinqDataSource ID="ldsList" runat="server" 
                ContextTypeName="eMenikDataContext" EnableDelete="True" EnableInsert="True" 
                EnableUpdate="True" TableName="MASTERs" 
                OnSelecting="ldsList_OnSelecting" oninserting="ldsList_Inserting" 
                onupdating="ldsList_Updating">
            </asp:LinqDataSource>
            
            <SM:smListView ID="lvList" runat="server" DataSourceID="ldsList" 
                DataKeyNames="MASTER_ID" InsertItemPosition="LastItem" 
                onitemcanceling="lvList_ItemCanceling" onitemediting="lvList_ItemEditing" 
                onitemupdated="lvList_ItemUpdated" onitemdatabound="lvList_ItemDataBound" >
                <LayoutTemplate>
                    <table  class="tblData">
                            <thead>
                                <tr id="headerSort" runat="server">
                                    <th width="50px"><asp:LinkButton ID="LinkButton1" CommandName="sort" CommandArgument="MASTER_ID" runat="server">ID</asp:LinkButton></th>
                                    <th ><asp:LinkButton ID="LinkButton3" CommandName="sort" CommandArgument="Master" runat="server">Master page</asp:LinkButton></th>
                                    <th ><asp:LinkButton ID="LinkButton4" CommandName="sort" CommandArgument="MASTER_TITLE" runat="server">Title</asp:LinkButton></th>
                                    <th ><asp:LinkButton ID="LinkButton2" CommandName="sort" CommandArgument="MASTER_DESC" runat="server">Desc</asp:LinkButton></th>
                                    <th width="50px"><asp:LinkButton ID="LinkButton5" CommandName="sort" CommandArgument="ACTIVE" runat="server">Active</asp:LinkButton></th>
                                    <th width="50px"><asp:LinkButton ID="LinkButton7" CommandName="sort" CommandArgument="HAS_HEADER_IMAGE" runat="server">Header</asp:LinkButton></th>
                                    <th width="50px"><asp:LinkButton ID="LinkButton8" CommandName="sort" CommandArgument="ALLWAYS_SHOW_TITLE" runat="server">Allways Title</asp:LinkButton></th>
                                    <th width="50px"><asp:LinkButton ID="LinkButton6" CommandName="sort" CommandArgument="ORDER" runat="server">Order</asp:LinkButton></th>
                                    <th colspan="2">Manage</th>
                                </tr>
                            </thead>
                            <tbody id="itemPlaceholder" runat="server"></tbody>
                            <tfoot>
                                <tr>
                                    <th style="text-align:right" colspan="10">
                                        <asp:DataPager runat="server" ID="DataPager" PageSize="20">
                                            <Fields>
                                                <asp:NextPreviousPagerField  />
                                                <asp:NumericPagerField ButtonCount="5" />
                                            </Fields>
                                        </asp:DataPager>
                                    </th>
                                </tr>
                            </tfoot>
                        </table>
                </LayoutTemplate>
                
                <ItemTemplate>
                    <tr  class='<%#  Container.DataItemIndex % 2 == 0 ? " " : " odd " %>'  onmouseover ="this.className='<%#  Container.DataItemIndex % 2 == 0 ? "hovRow" : "hovRowodd" %>'" onmouseout ="this.className = GetCssClass(<%#  Container.DataItemIndex %>)">
                        <td>
                            <%# Eval("MASTER_ID") %>
                        </td>
                        <td>
                            <%# Eval("Master") %>
                        </td>
                        <td>
                            <%# Eval("MASTER_TITLE") %>
                        </td>
                        <td>
                            <%# Eval("MASTER_DESC") == null ? "" : SM.EM.Helpers.CutString(Eval("MASTER_DESC").ToString(), 50, " ...")%>
                        </td>
                        <td>
                            <asp:CheckBox ID="cbActive" runat="server" Checked='<%# Eval("ACTIVE") %>' Enabled="false" />
                        </td>
                        <td>
                            <asp:CheckBox ID="cbHdrImg" runat="server" Checked='<%# Eval("HAS_HEADER_IMAGE") %>' Enabled="false" />
                        </td>
                        <td>
                            <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Eval("ALLWAYS_SHOW_TITLE") %>' Enabled="false" />
                        </td>
                        <td>
                            <%# Eval("ORDER") %>
                        </td>
                        <td >
                            <asp:LinkButton ID="lbEdit" CommandName="Edit" runat="server">edit</asp:LinkButton>
                        </td>
                        <td>
                            <asp:LinkButton ID="lbDelete" OnClientClick="javascript: return confirm('Are you sure you want to delete this item?');" CommandName="Delete" runat="server">delete</asp:LinkButton>
                        </td>                
                    </tr>        
                
                </ItemTemplate>
                
                <InsertItemTemplate>
                    <tr class="insertRow">
                        <td colspan="2">
                            <asp:TextBox MaxLength="256" Width="99%" ID="tbMaster" runat="server" Text='<%# Bind("Master") %>'></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="256" Width="99%" ID="tbTitle" runat="server" Text='<%# Bind("MASTER_TITLE") %>'></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="512" Width="99%" ID="tbDesc" runat="server" Text='<%# Bind("MASTER_DESC") %>'></asp:TextBox>
                        </td>
                        <td>
                            <asp:CheckBox ID="cbActive" runat="server" Checked='<%# Bind("ACTIVE") %>'  />
                        </td>
                        <td>
                            <asp:CheckBox ID="cbHdrImg" runat="server" Checked='<%# Bind("HAS_HEADER_IMAGE") %>'  />
                        </td>
                        <td>
                            <asp:CheckBox ID="CheckBox2" runat="server" Checked='<%# Bind("ALLWAYS_SHOW_TITLE") %>'  />
                        </td>
                        <td>
                            <asp:TextBox MaxLength="9" Width="99%" ID="tbOrder" runat="server" Text='<%# Bind("ORDER") %>'></asp:TextBox>
                            <cc1:FilteredTextBoxExtender TargetControlID="tbOrder" ID="ftbeOrder" runat="server" FilterType="Numbers"></cc1:FilteredTextBoxExtender>
                        </td>
                        <td colspan="2">
                            <asp:Button ID="btSave" CommandName="Insert" runat="server" Text="Add new" ValidationGroup="insert" ></asp:Button>
                        </td>
                    </tr>     
                    <tr class="errorRow">
                        <td colspan="2">
                            <asp:RequiredFieldValidator ValidationGroup="insert" ID="reqState" ControlToValidate="tbMaster" Display="Dynamic"  runat="server" ErrorMessage="* Enter master page"></asp:RequiredFieldValidator>
                            <asp:CustomValidator ValidationGroup="insert" ID="cvState" ControlToValidate="tbMaster" Display="Dynamic" runat="server" ErrorMessage="* Master page already exists" OnServerValidate="ValidateDuplicates"></asp:CustomValidator>
                        </td>
                        <td colspan="2">
                            <asp:RequiredFieldValidator ValidationGroup="insert" ID="RequiredFieldValidator2" ControlToValidate="tbTitle" Display="Dynamic"  runat="server" ErrorMessage="* Enter title"></asp:RequiredFieldValidator>
                        </td>
                        <td colspan="3">
                            <asp:RegularExpressionValidator ValidationGroup="insert" ID="regvRisk" ControlToValidate="tbOrder" ValidationExpression="^[0-9]+" Display="Dynamic" runat="server" ErrorMessage="* Enter valid risk value"></asp:RegularExpressionValidator>
                            <asp:RequiredFieldValidator ValidationGroup="insert" ID="rfvRisk" ControlToValidate="tbOrder" runat="server" ErrorMessage="* Enter order" Display="Dynamic" ></asp:RequiredFieldValidator>
                        </td>
                    </tr>   
                    <tr class="insertRow">
                        <td colspan="2"> 
                            <asp:DropDownList ID="ddlLogo" Width="99%" runat="server"   AppendDataBoundItems="true"  DataSourceID="ldsImageType" DataTextField="TYPE_TITLE" DataValueField="TYPE_ID">
                                <asp:ListItem Text=" - Select Logo type - " Value="" ></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td colspan="2"> 
                            <asp:DropDownList ID="ddlHeader" Width="99%" runat="server"   AppendDataBoundItems="true"  DataSourceID="ldsImageType" DataTextField="TYPE_TITLE" DataValueField="TYPE_ID">
                                <asp:ListItem Text=" - Select Header type - " Value="-1" ></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td colspan="6">
                        </td>
                    </tr>   
                    <tr class="errorRow">
                        <td colspan="2">
                            <asp:RequiredFieldValidator ValidationGroup="insert" ID="rvLogo" ControlToValidate="ddlLogo" Display="Dynamic"  runat="server" ErrorMessage="* Choose logo type"></asp:RequiredFieldValidator>
                        </td>
                        <td colspan="8">
                        </td>
                    </tr>                         
                </InsertItemTemplate>
                
                <EditItemTemplate>
                
                    <tr class="editRow">
                        <td colspan="2">
                            <asp:TextBox MaxLength="256" Width="99%" ID="tbMaster" runat="server" Text='<%# Bind("Master") %>'></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="256" Width="99%" ID="tbTitle" runat="server" Text='<%# Bind("MASTER_TITLE") %>'></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="512" Width="99%" ID="tbDesc" runat="server" Text='<%# Bind("MASTER_DESC") %>'></asp:TextBox>
                        </td>
                        <td>
                            <asp:CheckBox ID="cbActive" runat="server" Checked='<%# Bind("ACTIVE") %>'  />
                        </td>
                        <td>
                            <asp:CheckBox ID="cbHdrImg" runat="server" Checked='<%# Bind("HAS_HEADER_IMAGE") %>'  />
                        </td>
                        <td>
                            <asp:CheckBox ID="CheckBox3" runat="server" Checked='<%# Bind("ALLWAYS_SHOW_TITLE") %>'  />
                        </td>
                        <td>
                            <asp:TextBox MaxLength="9" Width="99%" ID="tbOrder" runat="server" Text='<%# Bind("ORDER") %>'></asp:TextBox>
                            <cc1:FilteredTextBoxExtender TargetControlID="tbOrder" ID="ftbeOrder" runat="server" FilterType="Numbers"></cc1:FilteredTextBoxExtender>
                        </td>
                        <td >
                            <asp:Button ID="btSave" CommandName="Update" runat="server" Text="Save" ValidationGroup="edit" ></asp:Button>
                        </td>
                        <td >
                            <asp:Button ID="btCancel" CommandName="Cancel" runat="server" Text="Cancel" ></asp:Button>
                        </td>                
                    </tr>     
                    <tr class="errorRow">
                        <td colspan="2">
                            <asp:RequiredFieldValidator ValidationGroup="edit" ID="reqState" ControlToValidate="tbMaster" Display="Dynamic"  runat="server" ErrorMessage="* Enter master page"></asp:RequiredFieldValidator>
                            <asp:CustomValidator ValidationGroup="edit" ID="cvState" ControlToValidate="tbMaster" Display="Dynamic" runat="server" ErrorMessage="* Master page already exists" OnServerValidate="ValidateDuplicates"></asp:CustomValidator>
                        </td>
                        <td colspan="2">
                            <asp:RequiredFieldValidator ValidationGroup="edit" ID="RequiredFieldValidator2" ControlToValidate="tbTitle" Display="Dynamic"  runat="server" ErrorMessage="* Enter title"></asp:RequiredFieldValidator>
                        </td>
                        <td colspan="3">
                            <asp:RegularExpressionValidator ValidationGroup="edit" ID="regvRisk" ControlToValidate="tbOrder" ValidationExpression="^[0-9]+" Display="Dynamic" runat="server" ErrorMessage="* Enter valid risk value"></asp:RegularExpressionValidator>
                            <asp:RequiredFieldValidator ValidationGroup="edit" ID="rfvRisk" ControlToValidate="tbOrder" runat="server" ErrorMessage="* Enter order" Display="Dynamic" ></asp:RequiredFieldValidator>
                        </td>
                    </tr>  
                    <tr class="editRow">
                        <td colspan="2"> 
                            <asp:DropDownList ID="ddlLogo" Width="99%" runat="server" SelectedValue='<%# Bind("IMG_TYPE_ID") %>'   AppendDataBoundItems="true"  DataSourceID="ldsImageType" DataTextField="TYPE_TITLE" DataValueField="TYPE_ID">
                                <asp:ListItem Text=" - Select Logo type - " Value="" ></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td colspan="2"> 
                            <asp:DropDownList ID="ddlHeader" Width="99%" runat="server" SelectedValue='<%# Eval("HDR_TYPE_ID") == null ? "-1" : Eval("HDR_TYPE_ID") %>'  AppendDataBoundItems="true"  DataSourceID="ldsImageType" DataTextField="TYPE_TITLE" DataValueField="TYPE_ID">
                                <asp:ListItem Text=" - Select Header type - " Value="-1" ></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td colspan="6">
                        </td>
                    </tr>   
                    <tr class="errorRow">
                        <td colspan="2">
                            <asp:RequiredFieldValidator ValidationGroup="edit" ID="rvLogo" ControlToValidate="ddlLogo" Display="Dynamic"  runat="server" ErrorMessage="* Choose logo type"></asp:RequiredFieldValidator>
                        </td>
                        <td colspan="8">
                        </td>
                    </tr>                       
                </EditItemTemplate>
            
            </SM:smListView>

        </ContentTemplate>
    </asp:UpdatePanel>


</asp:Content>


