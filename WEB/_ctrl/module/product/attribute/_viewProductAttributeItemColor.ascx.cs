﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _ctrl_module_product_attribute_viewProductAttributeItemColor : BaseControlView
{

    public object Data;
    public object[] Parameters;
    public Guid ProductID;
    public ProductRep.Data.GroupAttributeLang GroupAttribute;
    public List<ProductRep.Data.wAttrubute> ProductAttributeList;
    public Guid? SelectedAttribute;


    protected void Page_Load(object sender, EventArgs e)
    {
        LoadData();
    }



    public void LoadData()
    {
        this.Visible = true;



        if (Data == null)
        {
            this.Visible = false;
            return;
        }
        ProductID  = (Guid )Data;

        if (Parameters != null)
        {
            if (Parameters.Length > 0 &&   Parameters[0] != null)
                IsModeCMS = bool.Parse(Parameters[0].ToString());
            if (Parameters.Length > 1 && Parameters[1] != null)
                GroupAttribute = (ProductRep.Data.GroupAttributeLang)Parameters[1];
            if (Parameters.Length > 2 && Parameters[2] != null)
                ProductAttributeList = (List<ProductRep.Data.wAttrubute>)Parameters[2];
            if (Parameters.Length > 3 && Parameters[3] != null)
                SelectedAttribute  = (Guid?)(Parameters[3]);

        }
        // hide if not set
        if (GroupAttribute == null || ProductAttributeList == null)
        {
            this.Visible = false;
            return;
        }

        //// bind data
        //lvList.DataSource = ProductAttributeList;
        //lvList.DataBind();

        //if (lvList.Items.Count == 0 && !IsModeCMS )
        //{
        //    this.Visible = false;
        //    return;
        //}


    }

    protected string RenderSelected(object o)
    {
        string ret = "";
        if ( SelectedAttribute == null ||  SelectedAttribute == Guid.Empty)
            return ret;

        Guid  att = (Guid )o;
        if (att == SelectedAttribute)
            ret = "selected=\"selected\"";         
     
        return ret;
    }



}
