﻿<%@ Page Language="C#" MasterPageFile="~/CMS/CMS.master" AutoEventWireup="true" CodeFile="manage-customer.aspx.cs" Inherits="SM.UI.CMS.CMS_backend_user_Manage_customer" Title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register TagPrefix="SB" TagName="UserEdit" Src="~/c/user/customer/_ctrl/_cntUserEdit.ascx"   %>
<%@ Register TagPrefix="SB" TagName="UserFilter" Src="~/c/user/customer/_ctrl/_cntUserFilter.ascx"   %>
<%@ Register TagPrefix="SB" TagName="UserList" Src="~/c/user/customer/_ctrl/_cntUserList.ascx"   %>
<%@ Register TagPrefix="SB" TagName="OrderList" Src="~/c/user/shopping-order/_ctrl/_cntOrderList.ascx"   %>
<%--<%@ Register TagPrefix="SM" TagName="UserSettings" Src="~/c/user/_ctrl/settings/_cntUserSettings.ascx"  %>--%>

<asp:Content ID="c1" ContentPlaceHolderID="cphNavL" runat="server">
    
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cph" Runat="Server">

    <asp:UpdatePanel ID="upList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>

            <asp:Panel ID="panSListHD" runat="server" CssClass="cpeListHD" >
                <asp:Image ID="imgCollapse" runat="server" />
               Seznam strank
            </asp:Panel>

            <asp:Panel ID="panSList" runat="server">

                <div class="qFilter">
                    <asp:UpdatePanel ID="upFilter" runat="server" UpdateMode="Always">
                        <ContentTemplate>
                            <div class="floatR">
                                <SB:UserFilter ID="objFilter" runat="server" />
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>

                <SB:UserList ID="objUserList" runat="server" ShowCheckboxColumn="false" ShowManageColumn="true" ShowEditColumn="true" />
            </asp:Panel>    

            <cc1:CollapsiblePanelExtender ID="cpeQList" runat="server" Collapsed="false" CollapseControlID="panSListHD" CollapsedImage="~/_res/images/icon/plus.gif" ExpandedImage="~/_res/images/icon/minus.gif" ImageControlID="imgCollapse" ExpandControlID="panSListHD" TargetControlID="panSList" ExpandDirection="Vertical" CollapsedText="Expand" ExpandedText="Collapse" >
            </cc1:CollapsiblePanelExtender>    
                
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="objFilter" EventName="OnFilterChanged"   />
            <asp:AsyncPostBackTrigger ControlID="tabUser$tpUser$objUserEdit" EventName="OnUserChanged"   />
            <asp:AsyncPostBackTrigger ControlID="tabUser$tpUser$objUserEdit" EventName="OnCancel"   />
        </Triggers>
    </asp:UpdatePanel>

        
    <br />

    <asp:UpdatePanel ID="upEdit" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
        <ContentTemplate>
            <cc1:TabContainer ID="tabUser" runat="server" ActiveTabIndex="0"  >
                <cc1:TabPanel ID="tpUser" HeaderText="OSNOVNI PODATKI" runat="server"  >
                    <ContentTemplate>
                        
                         <SB:UserEdit   id="objUserEdit" runat="server" />
                    
                    </ContentTemplate>
                </cc1:TabPanel>    
                
                <cc1:TabPanel ID="tpOrder" HeaderText="NAROČILA" runat="server" Visible="true"  >
                    <ContentTemplate>
                    
                        <SB:OrderList ID="objOrderList" runat="server" ShowCheckboxColumn="false" ShowManageColumn="true" ShowEditColumn="true" />
                            
                    </ContentTemplate>
                </cc1:TabPanel>   
                
                <cc1:TabPanel ID="tpContact" HeaderText="CONTACT" runat="server" Visible="false"  >
                    <ContentTemplate>
                        
                        <%--<SM:UserSettings ID="objUserSettings" runat="server" />--%>
                            
                    </ContentTemplate>
                </cc1:TabPanel>                                 
            </cc1:TabContainer>

        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger EventName="OnEdit" ControlID="objUserList" />
        </Triggers>        
    </asp:UpdatePanel>              



</asp:Content>