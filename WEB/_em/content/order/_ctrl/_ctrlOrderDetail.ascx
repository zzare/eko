﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_ctrlOrderDetail.ascx.cs" Inherits="_em_content_order__ctrl__ctrlOrderDetail" %>
<%@ Register Namespace="SM.EM.UI.Controls" TagPrefix="EM"  %>


<asp:ListView ID="lvList" runat="server" >
    <LayoutTemplate>
        <table class="tableOrder_step45">
            <asp:PlaceHolder ID="itemPlaceholder" runat="server"/>
        
            <tr id="Tr1"  runat="server" >
            <td></td>
            <td><%= GetGlobalResourceObject("Product", "shipping")%>: </td>
            <td><%= SM.EM.Helpers.FormatPrice(GetShipping)%></td>
            </tr>


        <tr  runat="server">
            <td></td>
            <td>Končna cena: </td>
            <td><%= SM.EM.Helpers.FormatPrice (GetTotalSumWithShipping) %></td>
            <%--<td><%= SM.EM.Helpers.FormatPrice (GetTotalSum) %></td>--%>
        </tr>
        </table>
        
    </LayoutTemplate>
    
    <ItemTemplate>
    
        <tr>
            <td style="padding:0px 3px 0px 0px;">
                <a target="_blank" href='<%# SM.BLL.Common.ResolveUrl(SM.EM.Rewrite.EmenikUserProductDetailsUrl(   new Guid(Eval("PageId").ToString()), new Guid(Eval("ProductID").ToString()), Eval("ProductTitle").ToString() ))  %>'  title='<%# Eval("ProductTitle") %>' >
                    <%#  Eval("ProductTitle")  %>
                    <%# RenderProductAttributesPrice(Container.DataItem) %> 
                </a> 
                          
            </td>
            <td>
                <%# RenderProductAttributes(Container.DataItem) %>
            </td>
            <td>
                Kol.: <%# Eval("Quantity") %>
            </td>
            <td>
                <%# SM.EM.Helpers.FormatPrice (Eval("PriceTotal"))%>
            </td>
        </tr>
    
    
    
    </ItemTemplate>
    
</asp:ListView>

<asp:PlaceHolder ID="phEmpty" Visible="false" runat="server">
    <span class="cartEmptyS">Košarica je prazna.</span>

</asp:PlaceHolder>

