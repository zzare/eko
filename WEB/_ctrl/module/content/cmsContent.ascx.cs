﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Microsoft.Security.Application;
namespace SM.EM.UI.Controls
{
    public partial class _ctrl_module_content_cmsContent : BaseControl
    {
        CONTENT con;
        public int ConID {get; set;}
        public Guid CwpID = Guid.Empty;
        public bool HideInactive = true;
        public bool AutoLoad {get; set;}
        public string ContentBody{
            get { return divContent.InnerHtml; }
        }
        public bool Active{
            get{
                object obj = ViewState["conActive" + ConID.ToString() ];
                return (obj == null) ? true : (bool)obj;
            }
            set{
                ViewState["conActive" + ConID.ToString()] = value;
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                if (AutoLoad) BindData();
        }

        public void BindData() {
            // if ID is set, get content
            if (ConID > 0)
                con = CONTENT.GetContentByID(ConID, HideInactive);
            else if (CwpID != Guid.Empty) // web part
            {
                con = CONTENT.GetContentByCwpID(CwpID);
            }
            else// get first content for current sitemap id
                con = CONTENT.GetContent(CurrentSitemapID);

            if (con != null)
            {

                divContent.InnerHtml = Server.HtmlDecode(con.CONT_BODY);
                Active = con.ACTIVE;

            }
        }
    }
}
