﻿<%@ Page Language="C#" ValidateRequest="false" MasterPageFile="~/_em/templates/fall/fall.master" AutoEventWireup="true" CodeFile="ArticleList.aspx.cs" Inherits="ArticleList" Title="Untitled Page" %>
<%--<%@ Register TagPrefix="SM" TagName="ArticleEdit" Src="~/_ctrl/module/article/cmsArticleEdit.ascx"   %>--%>
<%--<%@ Register TagPrefix="SM" TagName="ArticleList" Src="~/_ctrl/module/article/cmsArticleList.ascx"  %>--%>
<%@ Register TagPrefix="SM" TagName="ArticleList" Src="~/_ctrl/module/article/_viewArticleList.ascx"   %>
<%@ Register TagPrefix="EM" TagName="EditModule" Src="~/_ctrl/module/_edit/editModule.ascx"  %>
<%--<%@ Register TagPrefix="EM" TagName="ArticleEdit" Src="~/_ctrl/module/article/cmsArticleEdit.ascx"   %>--%>
<%@ Register TagPrefix="EM" TagName="SubMenu" Src="~/_em/templates/_custom/eko/_ctrl/_ctrlSubMenu.ascx"  %>
<%--<%@ Register TagPrefix="SM" TagName="RightAbout" Src="~/_em/templates/_custom/zavetisceljubljana/_ctrl/common/_viewRightColAbout.ascx"   %>--%>


<asp:Content ID="Content2" ContentPlaceHolderID="cph" Runat="Server">

<%--<div class="mainL">
    <EM:SubMenu ID="objSubMenu" runat="server" />
</div>

<div class="mainR">
--%>

                                    <div class="articleListOutW">
                                        <div class="art_padding">
                                       <%-- <h1>Aktualno iz Zavetišča Ljubljana</h1>--%>
                                        <span class="art_sepa"></span>
                                        <EM:EditModule ID="objEditModule" runat="server" />

                    <%--    <div class="cont_wideW">

                            <div class="cont_wideT"></div>
                                <div class="cont_wideM">
                                    <div class="cont_wideMI">--%>

                     
                                        <SM:ArticleList ID="objArticleList" runat="server" />
                                        
                                        
                                        <div class="pagesortW">
                                            <div class="pagesort">
                                                <a <%= Pager.RenderPageFullUrlHREF( 1) %> class="pagesort_fbw">&laquo;</a>
                                                <a <%= Pager.RenderPageFullUrlHREF( Pager.GetCurrentPage()-1) %> class="pagesort_bw">&lsaquo;</a>
                                                
                                                <%= Pager.RenderPageLinks(-1) %> 
                                                <a <%= Pager.RenderPageFullUrlHREF( Pager.GetCurrentPage()+1) %> class="pagesort_fwd">&rsaquo;</a>
                                                <a <%= Pager.RenderPageFullUrlHREF( Pager.TotalPages) %> class="pagesort_ffwd">&raquo;</a>
                                                <div class="clearing"></div>
                                            </div>
                                        </div>                     
                                        
                                        
                                        
                                        
                                        
                                        <br />
                                        <a class="btBackwthText" href='<%= Page.ResolveUrl(SM.EM.Rewrite.EmenikUserUrl(PageName, CurrentSitemapID)) %>' ><%= GetGlobalResourceObject("Modules", "BackToPage")%></a>
                                        
                                        
                                        <div class="clearing"></div>
                                        </div>                                    
                                </div>
<%--</div>                    
--%>                    
                    
                    
<%--                </div>    
            </div>
        <div class="cont_wideB"></div>
    </div>--%>

</asp:Content>
