﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewProductDetailBuy.ascx.cs" Inherits="_ctrl_module_product_viewProductDetailBuy" %>
<%@ Register TagPrefix="SM" TagName="ProductAttribute" Src="~/_ctrl/module/product/attribute/_viewProductAttributeList.ascx"   %>




<script type="text/javascript">

    function spinVal(v, id) {
        $(id).each(function() {
            var c = parseInt($(this).val());
            if (isNaN(c))
                c = 1;
            else
                c = c + v;
            if (c < 1)
                c = 1;

            $(this).val(c);
        });
    
    }


    $(document).ready(function() {

        $("form").validate({
            onsubmit: false,
            rules: {
                quantity: {required:true,number:true}
            },
            messages: {
                quantity:{
                    required: '<%= GetGlobalResourceObject("Modules", "valEnterQuantity")%>',
                    number: '<%= GetGlobalResourceObject("Modules", "valEnterNumber")%>'                  
                }
            }
        });


        $('.vgAddToCart select').each(function() {
            $(this).rules('add', {
                required: true,
                messages: {
                    required: '<%= GetGlobalResourceObject("Modules", "valChoose")%>'
                }
            });
        });


        $('.vgAddToCart input.hfAttr').each(function () {
            $(this).rules('add', {
                required: true,
                messages: {
                    required: '<%= GetGlobalResourceObject("Modules", "valChoose")%>'
                }
            });
        });




    });


    function addToCart(pid, t) {
        var isValid = $(".vgAddToCart :input").valid();
        if (isValid) {

            var q = $(".vgAddToCart").find("#quantity").val();
            var _items = new Array();
            $("#vgAddToCart select").each(function () {
                var _item = new Object();
                _item.ID = $(this).attr('name');
                _item.Value = $(this).val();
                _item.ValSet = null;
                Array.add(_items, _item);
            });

            $(".vgAddToCart .cAttrSlider").each(function () {
                var _item = new Object();

                var sl = $(this).find("div.attrSlider");

                _item.ID = $(this).attr('name');
                _item.Value = sl.attr('name');
                _item.ValSet = sl.slider("option", "value");
                Array.add(_items, _item);
            });

            $(".vgAddToCart .cAttrColor").each(function () {
                var _item = new Object();

                _item.ID = $(this).find("input.hfGrAttr").val();
                _item.Value = $(this).find("input.hfAttr").val();
                _item.ValSet = null;
                Array.add(_items, _item);
            });

//            $(".vgAddToCart .cAttrPrice input:checked").each(function () {
            $(".vgAddToCart .cAttrPrice").each(function () {
                var _item = new Object();
                // var $itm = $(this).parents('.itmProdAttrPrice');

                _item.ID = $(this).find("input.hfGrAttr").val();
                _item.Value = $(this).find("input.hfAttr").val();

 
                //                _item.ID = $(this).attr('name');
                //                _item.Value = $itm.attr('id');
                //                //_item.Price = $itm.attr('data-price');
                _item.ValSet = null;
                Array.add(_items, _item);
            });

            $(t).hide();

            flytoBasket();
             WebServiceP.AddToCart(pid, q, _items, em_g_cult, em_g_lang, em_g_pid, onCartUpdateSuccess, onCartUpdateError, t);
        }

    }

    function flytoBasket() {
        // scroll to top if needed
        var scrollTop = $(window).scrollTop();
        if (scrollTop > 0) {
            $('body,html').animate({
                scrollTop: 0
            }, 300);            
        }


        var orig = $('div.prod_detailLI').find('img.prodImgBig');

        if (orig.length <= 0)
            return;

        var basket = $('#cartPopup a.Acart span');
        var fly = orig.clone().css('top', orig.offset().top + 'px').css('left', orig.offset().left + 'px').css('position', 'absolute').appendTo('body');

        basket.css('backgroundPosition', 'bottom right');
        basket.parent().css('backgroundPosition', 'bottom left');
        

        fly.animate({
        left: basket.offset().left,
        top: basket.offset().top,
            height: '1',
            opacity: 0

        }, 1000, function() { $(this).hide() });

        //orig.effect("pulsate", { times: 1 }, 300);
    }



</script>
<div class="prod_detailB">
    <div class="prod_detailBI">

        <div class="vgAddToCartI vgAddToCart" >
            <SM:ProductAttribute ID="objProductAttribute" runat="server" />
        </div>


        <span class="pd_pricefull"><%= GetGlobalResourceObject("Product", "priceRegular")%>: <span class="pd_pricefull_number"><%= SM.EM.Helpers.FormatPrice( Prod.PRICE) %></span></span>
        <%--<span class="pd_pricefinal"><%= GetGlobalResourceObject("Product", "priceYour")%>: <%= SM.EM.Helpers.FormatPriceFinal( Prod.PRICE, Prod.DISCOUNT_PERCENT)%></span>--%>
<%--        <span class="pd_pricesave">
            Prihranek: <%= SM.EM.Helpers.FormatPriceSaving( Prod.PRICE, Prod.DISCOUNT_PERCENT) %>&nbsp;
            (<%= SM.EM.Helpers.FormatDiscount( Prod.DISCOUNT_PERCENT) %>)
        </span>
--%>    
        <div  id="vgAddToCart" class="vgAddToCart">

            <div style="display:none;" class="pd_RML_quantityL"><%= GetGlobalResourceObject("Product", "quantity")%> <input name="quantity" id="quantity" value="1" class="quantityInput" /></div>
            <div style="display:none;" class="pd_RML_quantityR">
                <a class="pd_RML_quantityUpA" href="#" onclick=" spinVal(1, '#quantity' ); return false" ></a>
                <a class="pd_RML_quantityDownA" href="#" onclick=" spinVal(-1, '#quantity' ); return false" ></a>
            </div>
       </div>
       <div class="clearing"></div> 
    </div>
    <div class="clearing"></div>
</div>
<div class="prod_detailBot">* <%= GetGlobalResourceObject("Product", "priceIncludesTax")%></div>
    

<% if (LANGUAGE.GetCurrentLang() == "si"){%>
<div class="pd_cartbtnW">                           
<a href="#" onclick="addToCart('<%= Prod.PRODUCT_ID %>', this); return false;" id="btToCart" class="AcartAdd font_bevan"><span><%= GetGlobalResourceObject("Product", "addToBasket")%></span></a>
</div> 
<%}else if (LANGUAGE.GetCurrentLang() == "en"){ %>  
<%}else if (LANGUAGE.GetCurrentLang() == "it"){ %>  
<%} %>
  
<div class="clearing"></div>     