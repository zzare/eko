﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="cwpGallery.ascx.cs" Inherits="SM.EM.UI.Controls.cwpGalleryImageList" %>
<%@ Register TagPrefix="SM" TagName="GalleryList" Src="~/_ctrl/module/gallery/cmsGalleryImageList.ascx"   %>
<%@ Register TagPrefix="SM" TagName="ImageList" Src="~/_ctrl/module/gallery/_viewGalleryImageList.ascx"   %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>



<div id='<%= cwp.CWP_ID.ToString() %>' class='<%= "cwp" + (ParentPage.IsModeCMS ? " cwpEdit " : " ") + CMS_WEB_PART.Common.RenderHiddenClass(cwp) %>'>
    <div id="divEdit" runat="server" class="cwpHeader cwpMove" visible="false" >
<%--                    <img class="" src='<%= Page.ResolveUrl("~/_inc/images/icon/move.png") %>'  alt="move" title="move"  />
--%>                 
        <div class="cwpHeaderRight"> &nbsp;</div>

        <div style="float:right; ">
            <a id="btNew" href="#" onclick="addImageLoad('<%= cwp.CWP_ID.ToString() %>', true); return false"  title="dodaj"  class="btNew"></a>
<% if (PERMISSION.User.Common.CanShowHideModule()){%>            
            <a id="btShow"  title="pokaži" runat="server" class="btShow"></a>
            <a id="btHide" title="skrij" runat="server" class="btHide"></a>
<%} %>
<% if (PERMISSION.User.Common.CanDeleteModule()){%>
            <a id="btDelete" href="#" title="izbriši" class="btDelete" onclick="deleteEL('<%= cwp.CWP_ID.ToString() %>'); return false" ></a>
<%} %>
<% if (PERMISSION.User.Common.CanEditModuleSettings ()){%>
            <a id="btSettings" href="#" onclick="settingsLoad('<%= cwp.CWP_ID.ToString() %>'); return false" title="nastavitve" class="btSettings"  ></a>
<%} %>
        </div>
        <div class="cwpHeaderLeft"> &nbsp;</div>
        <a id="btMove" class="btMove"></a>
        
        <div class="cwpHeaderTitle">
            Galerija
            <a href="javascript:void(0)" class="btsettStatusHelp btcwpHeadHelp"><img src='<%=Page.ResolveUrl("~/_inc/images/icon/help.png") %>' /></a>
            <div class="settStatusWrapp cwpHeadHelpWrapp" >
                <div class="settStatus cwpHeadHelp">
                    Dinamična spletna fotogalerija
                    <br />
                    Slika pove več kot 1000 besed.
                    <br /><br />
                    Element fotogalerije omogoča:
                    <br />
                    - neomejeno dodajanje slik <br />
                    - urejanje albumov slik <br />
                    - urejanje vrstnega reda slik<br />
                    - urejanje posameznih slik (klikni na slikico v levem zgornjem kotu na posamezni slikici)
                </div>
            </div>
        </div>
    </div>
    <div class="clearing"></div>
    
    <div class='<%= "cGal " + ( ParentPage.IsModeCMS ? "cwpContainer" : "" ) %>'>
<%--        <div id="divTitleGallery"  class="cwpH1" runat="server">
            <h1><asp:Literal ID="litTitle" runat="server" ></asp:Literal></h1>
        </div>--%>
        <SM:ImageList  ID="objList" runat="server"   />

        <%--<SM:GalleryList ID="objGalleryList" runat="server"   />--%>
    </div>    
</div>



