﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;



    public partial class _cntFileList : BaseControl 
    {
        //public Guid CategoryID { get; set; }
        public Guid RefID { get; set; }
        public string SearchText { get; set; }
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }
        protected bool _bind = false;
        
        protected void Page_Load(object sender, EventArgs e)
        {
        }


        public  void BindData()
        {
            //if (!_bind) {
            //    e.Cancel = true;
            //    return;
            //}

            eMenikDataContext db = new eMenikDataContext();

            var list = (from l in db.FILEs where l.RefID == RefID select l);

            // apply filters

            // result
            lvList.DataSource  = list.OrderByDescending(o => o.DateAdded);
            lvList.DataBind();
        }





   
    }
