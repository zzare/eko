﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace SM.UI.CMS.EM
{
    public partial class CMS_ManageImageType : SM.EM.UI.CMS.BasePage_CMS 
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void ldsList_OnSelecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            eMenikDataContext db = new eMenikDataContext();

            var list = from l in db.IMAGE_TYPEs  orderby l.TYPE_TITLE   ascending select l;

            //if (_langID != "-1") contents = contents.Where(c => c.LANG_ID == _langID);

            e.Result = list;
        }


        protected void ldsList_Inserting(object sender, LinqDataSourceInsertEventArgs e)
        {
            //// set logo and header type
            //MASTER  c = (MASTER)e.NewObject;
            //c.IMG_TYPE_ID = Int32.Parse(((DropDownList)lvList.InsertItem.FindControl("ddlLogo")).SelectedValue);
            //int imgHeader = Int32.Parse(((DropDownList)lvList.InsertItem.FindControl("ddlHeader")).SelectedValue);
            //if (imgHeader > 0)
            //{
            //    c.HDR_TYPE_ID = imgHeader;
            //    c.HAS_HEADER_IMAGE = true;
            //}
            //else
            //    c.HAS_HEADER_IMAGE = false;

        }
        protected void lvList_ItemEditing(object sender, ListViewEditEventArgs e)
        {
            // hide insert item
            lvList.InsertItemPosition = InsertItemPosition.None;
        }
        protected void lvList_ItemUpdated(object sender, ListViewUpdatedEventArgs e)
        {
            // show insert item
            lvList.InsertItemPosition = InsertItemPosition.LastItem;

        }
        protected void lvList_ItemCanceling(object sender, ListViewCancelEventArgs e)
        {
            // show insert item
            lvList.InsertItemPosition = InsertItemPosition.LastItem;

        }
#region Validation
        protected void ValidateDuplicates(object sender, ServerValidateEventArgs e)
        {

            //int id = -1;

            //if (lvList.EditIndex >= 0)
            //    id =  (Int32 )lvList.DataKeys[lvList.EditIndex].Value ;
 
            //string name = e.Value;
            //eMenikDataContext db = new eMenikDataContext();
            //var recs = (from r in db.MASTERs  where r.Master  == name   select r );

            //// if editing change SQL
            //if (id != null)
            //    recs = recs.Where(w => w.MASTER_ID  != id);

            //var rec = recs.FirstOrDefault();

            //if (rec==null){
            //    e.IsValid = true;
            //    return;
            //}
            //e.IsValid = false;

            //CustomValidator val = (CustomValidator)sender;
            //val.ErrorMessage = "* Master page ";
            //if (rec.Master  == e.Value)
            //    val.ErrorMessage += " '" + rec.Master   + "' already exists.";
        }


#endregion

        protected void ldsList_Updating(object sender, LinqDataSourceUpdateEventArgs e)
        {
            // set header type
            

            //MASTER c = (MASTER)e.NewObject;
            //int imgHeader = Int32.Parse(((DropDownList)lvList.Items[lvList.EditIndex].FindControl("ddlHeader")).SelectedValue);
            //if (imgHeader > 0)
            //{
            //    c.HDR_TYPE_ID = imgHeader;
            //    c.HAS_HEADER_IMAGE = true;
            //}
            //else
            //    c.HAS_HEADER_IMAGE = false;

            // clear cache
            SM.EM.BLL.BizObject.PurgeCacheItems(SM.EM.Caching.Key.ImageTypeID());

        }
        protected void lvList_ItemDataBound(object sender, ListViewItemEventArgs e)
        {

        }
}
}
