﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="emModuleNew.ascx.cs" Inherits="SM.EM.UI.Controls.emModuleNew" %>

<div style="padding:7px 5px 7px 5px;">
    <div class="moduleNew" >
        <div class="moduleNewLeft">&nbsp;</div>
        <div style="float:left">
            <asp:DropDownList ID="ddlUserModule" runat="server" AppendDataBoundItems="true">
            </asp:DropDownList>
        </div>
        <div style="float:left; width:40px;">
            <asp:LinkButton ValidationGroup="modNew" ID="btAdd" class="btAddModule" runat="server" OnClick="btAdd_Click"></asp:LinkButton>
        </div>
        <div style="float:left;">
            <a href="javascript:void(0)" class="btsettStatusHelp btcwpHeadHelp"><img style="margin-top:8px;" src='<%=Page.ResolveUrl("~/_inc/images/icon/help.png") %>' /></a>
            <div class="settStatusWrapp cwpHeadHelpWrapp" >
                <div class="settStatus cwpHeadHelp">
                    Dodaš lahko poljubno število posameznih elementov.
                    <br />
                    Vsak element ki ga dodaš ima v opravilni vrstici (moder ali siv pas na vrhu vsakega elementa) več gumbov za upravljanje z le-tem.
                    <br /><br />
                    Opis gumbov:
                    <br />
                    - premik: premakni element na želeno mesto<br />
                    - dodaj: dodaj novo vsebino oz sliko<br />
                    - uredi: uredi obstoječo vsebino elementa<br />
                    - prikaži: element bo prikazan na spletni strani (opravilna vrstica v elementu je modra)<br />
                    - skrij: element ne bo prikazan na spletni strani (opravilna vrstica v elementu je siva)<br />
                    - izbriši: izbriši element iz spletne strani<br />
                    - nastav.: specifične nastavitve posameznega elementa
                </div>
            </div>
        </div>    
        <div class="moduleNewRight">&nbsp;</div>
        <div class="clearing"></div>
    </div>
    <asp:RequiredFieldValidator Display="Dynamic" ValidationGroup="modNew" ID="rfvModNew" runat="server" ErrorMessage="* Izberi element" ControlToValidate="ddlUserModule"></asp:RequiredFieldValidator>        
</div>