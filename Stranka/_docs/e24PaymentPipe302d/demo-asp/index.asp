<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML>
<HEAD>
<TITLE>Colors of Success</TITLE>

<!--#INCLUDE FILE = "header.inc"-->

<FORM name="form1" ACTION="details.asp" METHOD="POST">

<input type="hidden" name="SKU"   value="ACI-203">
<input type="hidden" name="NAME"  value="Munsingwear Shirt">
<input type="hidden" name="PRICE" value="33.00">

<TABLE border="0">
    <TBODY>
        <TR>
            <TD width="23" colspan="3"><img src="products.png"></TD>
            <TD width="155"></TD>
            <TD width="53"></TD>
            <TD width="116"></TD>
            <TD width="29"></TD>
        </TR>
        <TR>
            <TD colspan="7"><HR></TD>
        </TR>
        <TR>
            <TD width="23"></TD>
            <TD width="155"></TD>
            <TD></TD>
            <TD width="155"></TD>
            <TD width="53"></TD>
            <TD width="116"></TD>
            <TD width="29"></TD>
        </TR>
        <TR>
            <TD width="23"></TD>
            <TD width="155" rowspan="5"><img src="jacket.gif"></TD>
            <TD></TD>
            <TD width="155"><B>Munsingwear Shirt</B></TD>
            <TD width="53"></TD>
            <TD width="116"></TD>
            <TD width="29"></TD>
        </TR>
        <TR>
            <TD width="23"></TD>
            <TD></TD>
            <TD colspan="3" rowspan="3">Show the caddies who's boss in this eye-catching tribute to leisure. This windshirt, with its 100% black suede nylon shell, V-neck collar, cuff, and waistband will kepp you toasty and warm on the links. It boasts side seam, and set-in pockets.</TD>
            <TD width="29"></TD>
        </TR>
        <TR>
            <TD width="23"></TD>
            <TD></TD>
            <TD width="29"></TD>
        </TR>
        <TR>
            <TD width="23"></TD>
            <TD></TD>
            <TD width="29"></TD>
        </TR>
        <TR>
            <TD width="23"></TD>
            <TD></TD>
            <TD align="right" width="155">Price (EUR):</TD>
            <TD align="left" width="53">�33.00</TD>
            <TD width="116"></TD>
            <TD width="29"></TD>
        </TR>
        <TR>
            <TD width="23"></TD>
            <TD width="155"></TD>
            <TD></TD>
            <TD align="right" width="155">Qty:</TD>
            <TD align="left" width="53"><INPUT name="QTY" size="8" value="1" align="right"></TD>
            <TD width="116"></TD>
            <TD width="29"></TD>
        </TR>
        <TR>
            <TD width="23"></TD>
            <TD
                width="155" align="right"></TD>
            <TD></TD>
            <TD width="155"></TD>
            <TD align="left" width="53"><INPUT TYPE="Submit" Value="Purchase"></TD>
            <TD width="116"></TD>
            <TD width="29"></TD>
        </TR>
    </TBODY>
</TABLE>
</FORM>

<!--#INCLUDE FILE = "footer.inc"-->

</BODY>
</HTML>
