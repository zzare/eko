﻿<%@ Control  Language="C#" AutoEventWireup="true" CodeFile="_cntLastPagesDetailedList.ascx.cs" Inherits="_ctrl_main_page_cntLastPagesDetailedList" %>

<asp:ListView ID="lvList" runat="server" onitemdatabound="lvList_ItemDataBound">

    <LayoutTemplate>
        <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
    </LayoutTemplate>

    <ItemTemplate>
        <a target="_blank" style="font-size:16px" class="btUsersLogin" href='<%# Page.ResolveUrl( SM.EM.Rewrite.EmenikUserUrl(Container.DataItem as EM_USER )) %>'><%#  (Eval("PAGE_TITLE") != null && Eval("PAGE_TITLE") != "") ? Eval("PAGE_TITLE") + " &rsaquo;" : Eval("PAGE_NAME") + "& rsaquo;"%></a>
        <div class="clearing" ></div>
        
        <asp:ListView ID="lvListSub" runat="server">

            <LayoutTemplate>
                <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
            </LayoutTemplate>

            <ItemTemplate>
                <%# RenderPageMenuDetails(Container.DataItem, Container.DataItemIndex  ) %>
                <%# RenderLanguage(Container.DataItem, Container.DataItemIndex)%>
                 <%# RenderMenulink(Container.DataItem) %>
            </ItemTemplate>
            
            <ItemSeparatorTemplate>
            &nbsp;&nbsp;
            </ItemSeparatorTemplate>
        
        
        </asp:ListView>
        
        
        <%--<p><%# RenderPageDetails(Container.DataItem ) %> </p>--%>
        <br />
    </ItemTemplate>
    
    <ItemSeparatorTemplate>
        <div style="line-height:0px; font-size:0px; min-height:5px; height:auto !important; height:5px; ">&nbsp;</div>
    </ItemSeparatorTemplate>

</asp:ListView>