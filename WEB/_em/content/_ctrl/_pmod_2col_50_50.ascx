﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_pmod_2col_50_50.ascx.cs" Inherits="_em_content__ctrl_pmod_2col_50_50" %>
<%@ Register Namespace="SM.EM.UI.Controls" TagPrefix="EM"  %>
<%@ Register TagPrefix="EM" TagName="ModuleNew" Src="~/_ctrl/emenik/emModuleNew.ascx"  %>

    <div class="cont_wideW">
        <div class="cont_wideT"></div>
        <div class="cont_wideM">
            <div class="cont_wideMI">

    <div id="contTwoColsWrapper_50_50" class="clearfix">
        <div id="contTwoColsMain_50_50">
            <EM:ModuleNew ID="mnMain" runat="server" />

            <EM:CwpZone ID="wpz_1" runat="server">
            </EM:CwpZone>

            

        </div>

        <div id="contTwoColsRight_50_50">
      
            <EM:ModuleNew ID="mnRight" runat="server" />
            
            <EM:CwpZone ID="wpz_2" runat="server">
            </EM:CwpZone>

        </div>
    </div>
            </div>    
        </div>
        <div class="cont_wideB"></div>
    </div>
