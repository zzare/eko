﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SM.EM.UI;
using SM.EM.UI.Controls;
using SM.EM;

public partial class _content_PollList : SM.EM.UI.BasePage_CmsWebPart
{
    //protected Guid CwpID;

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        CurrentSitemapID = -1;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        // get CWP id from QS
        //if (Request.QueryString["cwp"] != null)
        //    if (SM.EM.Helpers.IsGUID(Request.QueryString["cwp"]))
        //        CwpID = new Guid(Request.QueryString["cwp"]);



    }

    protected override void OnLoadComplete(EventArgs e)
    {
        // load after complete (for proper IsModeCms initialization)
        //if (CwpID != Guid.Empty)
            LoadList();
        

        base.OnLoadComplete(e);
    }


    protected void LoadList() {

        objList.Data = POLL.GetPollListByUserLang(em_user.UserId , LANGUAGE.GetCurrentLang(), IsModeCMS).OrderByDescending(w=>w.DateAdded ).Skip(0).Take(50);
        objList.Parameters = new object[] { IsModeCMS, LANGUAGE.GetCurrentLang() };
        objList.LoadList();
    
    }

    protected override void LoadTitle()
    {
        base.LoadTitle();

        // append article to title
        Page.Title = "Arhiv anket | " + TitleRoot ;

    }

    protected override void SetRelCanonical()
    {
        RelCanonical = Rewrite.EmenikUserUrlGetLeftPart(this.em_user) + Rewrite.EmenikUserPollArchiveUrl(this.PageName).TrimStart("~".ToCharArray());
    }

}
