﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using SM.EM.BLL;

/// <summary>
/// Summary description for MegaPos
/// </summary>
public partial class MegaPos
{

    public static string GetTxId(int id) {


        return "sm" + id.ToString() + "ks";
    }

    public static string CertificatePath(){

        return @ConfigurationSettings.AppSettings["OnlineCert"];

        //return @"D:\_test\certifikati_za_trgovino_test_2\test\test.cer";
    }
    public static string Store()
    {
        return ConfigurationSettings.AppSettings["OnlineStore"];
    }
    public static bool TestMode()
    {
        return !(ConfigurationSettings.AppSettings["OnlineTestMode"] == "0");
    }

    public static string UpdateURL()
    {
        return "~/_inc/mp_update.aspx";
    }

    public static int StatusRefreshInterval()
    {
        return 8;
    }
    


   public class Common
    {
        public static string RenderUpdateHref(int txId)
        {
            return UpdateURL() + "?txId=" + txId.ToString();
        }
        public static string RenderStatusHref(Guid ordrID)
        {
            return SM.BLL.Common.ResolveUrlFull(SM.EM.Rewrite.OrderProductUrl(SM.BLL.Custom.MainUserID , ordrID, 5));
            //return "~/_inc/mp_update.aspx?txId=" + txId.ToString();
        }


        public class Status {
            public static short NOT_SET = 0;
            public static short INITIALIZING = 1;
            public static short INITIALIZED = 2;
            public static short PROCESSED = 3;
            public static short FAILED = 4;
            public static short CANCELLED = 5;       
        }

        public static  short GetStatusFromTrans(string transStat) {
            short ret = Status.FAILED;

            switch (transStat)
            {
                case "INITIALIZING":
                    ret = Status.INITIALIZING;
                    break;
                case "INITIALIZED":
                    ret = Status.INITIALIZED;
                    break;
                case "PROCESSED":
                    ret = Status.PROCESSED;
                    break;
                case "FAILED":
                    ret = Status.FAILED;
                    break;
                case "CANCELLED":
                    ret = Status.CANCELLED;
                    break;
                default:
                    ret = Status.FAILED;
                    break;
            }


            return ret;
        
        }
    
    
    }

    //partial void OnValidate(System.Data.Linq.ChangeAction action)
    //{
    //}




}

/// <summary>
/// Summary description for MP_TRANS
/// </summary>
public partial class MP_TRANS
{

    public string GetTxId()
    {
        return "";
    }


}
