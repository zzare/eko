﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace SM.EM.UI
{
    public partial class _em_templates_Fall_Fall : BaseMasterPage_EMENIK
    {
        public override string StatusText
        {
            get;
            set;
        }
        public override string ErrorText
        {
            get;
            set;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            // add CSS
            CSSMain.Href += "?" + Helpers.GetCssVersion();
            CSSMenu.Href += "?" + Helpers.GetCssVersion();
            CSSIEMenu6.Href += "?" + Helpers.GetCssVersion();
            //CSSTemplate.Href += "?" + Helpers.GetCssVersion();

            smdsMain.Provider = ppem.SmpEmenik;


        }

        public override void LoadData()
        {
            // load logo
            if (ppem.em_user.SHOW_LOGO)
            {
                imgLogo.Src = ppem.em_user.IMG_LOGO;
                imgLogo.Alt = ppem.em_user.PAGE_TITLE ;
                lnkLogo.Title = ppem.em_user.PAGE_TITLE;
                lnkLogo.HRef = Page.ResolveUrl(Rewrite.EmenikUserUrl(ppem.PageName));
                lnkLogo.Visible = true;
            }
            else
                lnkLogo.Visible = false;

            // load header
            if (!string.IsNullOrEmpty(ppem.em_user.IMG_HEADER))
                imgHeader.Src = ppem.em_user.IMG_HEADER;

            // languages
            objLanguages.LoadLanguages();

            base.LoadData();
        }



        protected void mTop_DataBound(object sender, EventArgs e)
        {
            Menu m = (Menu)sender;
            SelectCurrentMenuItem(m, smdsMain.Provider);
        }
        
        // for custom class in menu
        protected void mTop_MenuItemDataBound(object sender, MenuEventArgs e)
        {
            SiteMapNode node = e.Item.DataItem as SiteMapNode;
            if (node != null)
            {
                if (!string.IsNullOrEmpty(node["class"]))
                    e.Item.ToolTip += ";" + node["class"];
            }
        }
    }
}