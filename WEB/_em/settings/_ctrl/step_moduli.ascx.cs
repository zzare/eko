﻿using System;
using System.Collections;
using System.Collections.Generic ;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SM.EM.UI.Controls;

public partial class _em_settings__ctrl_step_moduli : BaseWizardStep
{

    protected bool IsPayedAndValid { 
        get {
            return (ParentPageEmenik.em_user.PAYED && ParentPageEmenik.em_user.DATE_VALID >= DateTime.Now.Date);
        } 
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        Description = "Elementi so različni tipi spletnih vsebin (besedilo, novica, galerija, kontaktni obrazec, anketa, zemljevid,...), ki gradijo spletno stran. " ;
        Description += "<br/><i>Namig: S klikom na ime ELEMENTA se ti odprejo dodatne informacije.</i>";
        ShortDesc = "Elementi (tip vsebine) na spletni strani";

        //Description = "<ul><li>";
        //Description += "Svoji spletni strani lahko poleg osnovnih funkcionalnosti dodeliš še nove. To storiš z nakupom dodatnih ELEMENTOV, ki razširijo uporabnost in privlačnost tvoje spletne strani. Zate smo jih pripravili kar nekaj, v prihodnosti pa jih mislimo dodati še več. ";
        //Description += "</li><li>";
//        Description += "NAKUP MODULA: Za nakup MODULA enostavno odkljukajte kljukico ob MODULU. Znesek se bo samodejno prištel k vaši letni ceni. Račun vam bomo poslali takoj, tako da boste kupljeni modul začeli uporabljati v najkrajšem možnem času (vklop MODULA se izvrši po prejetem plačilu). Po vsakem nakupu/spremembi, ne pozabite klikniti na gumb \"Shrani spremembe\", ki se nahaja čisto spodaj.";
        //Description += "NAKUP ELEMENTA: Za nakup ELEMENTA enostavno odkljukaj kljukico ob ELEMENTU. Znesek se bo samodejno prištel k tvoji letni ceni. ELEMENT lahko pričneš uporabljati TAKOJ. Če si dodal NOV ELEMENT, se ti bo ustrezno skrajšal čas veljavnosti spletne strani (če ste ga odkljukali, se bo ustrezno podaljšal čas veljavnosti). Po vsakem nakupu/spremembi, ne pozabi klikniti na gumb \"Shrani spremembe\", ki se nahaja čisto spodaj.";
        //Description += "</li><li>";
        //Description += "Namig: S klikom na ime ELEMENTA se ti odprejo dodatne informacije.";
        //Description += "</li></ul>";

        //ShortDesc = "Izberi elemente, ki jih potrebuješ";

        StepID = 3;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        

    }

    public override void LoadData()
    {
        // load data
        lvList.DataSource =  MODULE.GetModulesEmenik();
        lvList.DataBind();

        // load database data
        txtStanjeSum.Text = SM.EM.Helpers.FormatPrice( ParentPageEmenik.em_user.CurrentCredit());

        txtValid.Text = "Tvoja stran je trenutno veljavna do: ";
        txtValidDate.Text = SM.EM.Helpers.FormatDate(ParentPageEmenik.em_user.DATE_VALID);

        LoadNewPageCredit();
        //txtSum.Text = SM.EM.Helpers.FormatPrice(ParentPageEmenik.em_user.GetDbModulesPriceSum());

        panChange.Visible = false;
        panExpired.Visible = false;

        // if user is new and hasn't payed, hide top panel
        if (!IsPayedAndValid)
            panCurrentState.Visible = false;
        else
            panCurrentState.Visible = true; 

        // if page has expried, remind user
        if (ParentPageEmenik.em_user.DATE_VALID < DateTime.Now) {
            panExpired.Visible = true;
            txtExprired.Text = "Tvoja stran je potekla. Če želiš še naprej uporabljati svojo spletno stran, moraš poravnati račun za novo leto. V nasprotnem primeru bo tvoja stran umaknjena iz spleta.";

        
        }

    }

    // calculate new user options
    protected void LoadNewPageCredit() { 
        
        panChange.Visible = IsPayedAndValid;

        decimal sum = 0;
        decimal sumTotal = 0;

        foreach (ListViewItem item in lvList.Items)
        {
            CheckBox cbModule = (CheckBox)item.FindControl("cbModule");
            if (cbModule == null) continue;

            HiddenField hfModID = (HiddenField)item.FindControl("hfModID");
            HiddenField hfModPrice = (HiddenField)item.FindControl("hfModPrice");
            HiddenField hfModBuyType = (HiddenField)item.FindControl("hfModBuyType");

            // get module id of the row
            int modID = Int32.Parse(hfModID.Value);
            decimal price = Convert.ToDecimal(hfModPrice.Value);
            int buyType = Int32.Parse(hfModBuyType.Value);


            if (cbModule.Checked)
            {
                switch (buyType)
                {
                    case 1: // yearly price
                        sum += price;
                        break;
                    case 2: // price for quantity
                        sum += price * 1; // price x quantity
                        break;
                    default:
                        break;
                }
            }
        }


        // new sum
        txtSum.Text = SM.EM.Helpers.FormatPrice(sum ) + " +DDV";

        sumTotal = sum;


        // calculate new date valid
        txtValidNew.Text = "Po shranitvi sprememb bo vaša stran veljavna do: ";
        txtValidDateNew.Text = SM.EM.Helpers.FormatDate(DateTime.Now.AddDays(EM_USER.GetNewDaysCredit(ParentPageEmenik.em_user.CurrentCredit(),sum)));



        // discount  - disabled currnty
        //if (SM.BLL.Common.Emenik.Data.DiscountPercent() > 0 || ParentPageEmenik.em_user.DISCOUNT_PERCENT > 0)
        //{
        //    divDiscount.Visible = true;

        //    if (SM.BLL.Common.Emenik.Data.DiscountPercent() > 0)
        //        litDiscountGlobal.Text = SM.BLL.Common.Emenik.Data.DiscountPercent().ToString() + "%";
        //    else
        //        litDiscountGlobal.Text = ParentPageEmenik.em_user.DISCOUNT_PERCENT.ToString() + "%";

             
        //    if (SM.BLL.Common.Emenik.Data.DiscountPercent() > 0 && ParentPageEmenik.em_user.DISCOUNT_PERCENT > 0) {
        //        phPersonal.Visible = true;
        //        litDiscountPersonal.Text = ParentPageEmenik.em_user.DISCOUNT_PERCENT.ToString() + "%";
        //        litDiscountTotal.Text = ParentPageEmenik.em_user.GetUserDiscount().ToString() + "%";
        //    }
        //    else
        //        phPersonal.Visible = false ;

        //    // new price
        //    sumTotal = sum * ((decimal) (100-ParentPageEmenik.em_user.GetUserDiscount())/100) ;
        //    txtSumDiscount.Text = SM.EM.Helpers.FormatPrice(sumTotal) + " +DDV";
             
        //}
        //else
        //    divDiscount.Visible = false;

        // monthly
        litMonthly.Text = SM.EM.Helpers.FormatPrice((decimal)sumTotal / 12) + " +DDV";


        
    }

    public override bool SaveData(int step)
    {
        UpdateUserStep(step);

        eMenikDataContext db = new eMenikDataContext();

        decimal sum = 0;
        // save current credit before module update
        decimal currentCredit = ParentPageEmenik.em_user.CurrentCredit();

        // update user modules
        foreach (ListViewItem  item in  lvList.Items  ){


            CheckBox cbModule = (CheckBox)item.FindControl("cbModule");
            if (cbModule == null) continue;

            HiddenField hfModID =  (HiddenField)item.FindControl("hfModID");
            HiddenField hfModPrice = (HiddenField)item.FindControl("hfModPrice");


            // get module id of the row
            int modID = Int32.Parse( hfModID.Value );
            
            // get module data
            MODULE module = MODULE.GetModuleByID (modID);

            USER_MODULE mod = db.USER_MODULEs.SingleOrDefault(m => m.MOD_ID == modID && m.UserId == ParentPageEmenik.em_user.UserId);
            if (cbModule.Checked ) {
                
                // if module is new, insert it
                if (mod == null)
                {
                    mod = new USER_MODULE();
                    db.USER_MODULEs.InsertOnSubmit(mod);
                }

                // update
                mod.UserId = ParentPageEmenik.em_user.UserId;
                mod.MOD_ID = module.MOD_ID ;
                mod.EM_PRICE = module.EM_PRICE;
                if (mod.QUANTITY > 0)
                    mod.QUANTITY += module.EM_BUY_QUANTITY;
                else
                    mod.QUANTITY = module.EM_BUY_QUANTITY;

                db.SubmitChanges();

                // sum price
                switch (module.EM_BUY_TYPE)
                {
                    case 1: // yearly price
                        sum += module.EM_PRICE;
                        break;
                    case 2: // price for quantity
                        sum += module.EM_PRICE * 1; // price x quantity
                        break;
                    default:
                        break;
                }
            }
            else if (mod != null)
            {
                // delete user module                
                db.USER_MODULEs.DeleteOnSubmit(mod);
                db.SubmitChanges();
            }
        
        }


        // modules are updated, calculate new validation date
        if (IsPayedAndValid )
            EM_USER.SetValidationDate(ParentPageEmenik.em_user.UserId, DateTime.Now.AddDays(EM_USER.GetNewDaysCredit(currentCredit, sum)));


        //this.Profile.Save();
        return true;
        
    }

    public override bool Validate()
    {
        return true;
            
    }

    protected void lvList_OnItemDataBound(object sender, ListViewItemEventArgs  e) {
        if (e.Item.ItemType != ListViewItemType.DataItem) return;

        CheckBox cbModule = (CheckBox)e.Item.FindControl("cbModule");
        HiddenField hfModID = (HiddenField)e.Item.FindControl("hfModID");

        if (cbModule == null) return;

        int modID = Int32.Parse(hfModID.Value);
        USER_MODULE uMod = USER_MODULE.GetUserModuleByID(ParentPageEmenik.em_user.UserId, modID);
        MODULE mod = (MODULE)((ListViewDataItem)e.Item).DataItem;

        // preselect module if exists
        if (uMod != null)
            cbModule.Checked = true;

        // disable selection if is free module
        if (mod.EM_FREE)
        {
            cbModule.Enabled = false;
            cbModule.Checked = true;
            cbModule.Text = "";
        }

    
    }


    protected void cbModule_OnCheckChanged(object sender, EventArgs e) {

        LoadNewPageCredit();


    }

    protected void btCancel_OnClick(object sender, EventArgs e) {
        LoadData();    
    }
    

    protected void placaj_OnClick(object sender, EventArgs e) {
    //    int price = 0;

    //    int.TryParse(tbPrice.Text, out price);

    //    EM_USER.UpdateUserPayment(ParentPageEmenik.em_user.UserId, price, PAYMENT.Common.PaymentType.Payment, "", Page.User.Identity.Name, ParentPageEmenik.em_user.UserId);

    //    LoadData();
    //}
    //protected void clear_OnClick(object sender, EventArgs e)
    //{
    //    eMenikDataContext db = new eMenikDataContext();
    //    EM_USER u = db.EM_USERs.Where(us => us.UserId == ParentPageEmenik.em_user.UserId).FirstOrDefault();
    //    u.DATE_PAYED = null;
    //    u.DATE_VALID = null;
    //    u.DATE_ORDER_CHANGED = null;
    //    u.PAYED = false;

    //    db.SubmitChanges();


    //    LoadData();

    }


    protected string GetPrice(object item) {
        MODULE mod = (MODULE)item;

        return SM.EM.Helpers.FormatPrice(mod.EM_PRICE);
    }

    protected override void Render(HtmlTextWriter writer)
    {

        Page.ClientScript.RegisterForEventValidation(lvList.UniqueID);
        base.Render(writer);
    }

}
