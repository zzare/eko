﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _ctrl_module_product_viewProductManufacturerLookup : BaseControlView
{

    public object Data;
    public object[] Parameters;

    public int ParentID;
    public string TitleID;
    public string ValueID;


    protected void Page_Load(object sender, EventArgs e)
    {


        if (Parameters != null)
        {

            if (Parameters.Length > 0 && Parameters[0] != null)
                TitleID = Parameters[0].ToString();
            if (Parameters.Length > 1 && Parameters[1] != null)
                ValueID = Parameters[1].ToString();
        }
       
    }



    protected string RenderItems()
    {

        if (Data == null)
            return "";
        string ret = "";

        ret = ViewManager.RenderView("~/_ctrl/module/product/lookup/_viewManufacturerList.ascx", Data, Parameters);

        return ret;

    }





}
