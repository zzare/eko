var map = null;
var mLat = null;
var mLong = null;
var iOpen = true;
var editID = null;

function getMapTypeByUrlArg(u) {
    if (!u)
        return G_NORMAL_MAP;

    if (u == 'm')
        return G_NORMAL_MAP;
    if (u == 'k')
        return G_SATELLITE_MAP;
    if (u == 'h')
        return G_HYBRID_MAP;
    if (u == 'p')
        return G_PHYSICAL_MAP;
    if (u == 'e')
        return G_SATELLITE_3D_MAP;

    return G_NORMAL_MAP;
}

function map_edit(id, c, lat, lng, z, mx, my, title, addr, h, w, srch, i, t) {
    var hMin = 200;
    if (h < hMin) h = hMin;

    editID = id;
    var mhtml = GetMarkerHtml(title, addr);
    map_load(id, c, lat, lng, z, mx, my, mhtml, h, w, i, t);

    $(c).width($(c).width());
    $(c).resizable({
        handles: 's, se',
        //                alsoResize: $(c).parent(),
        //                alsoResize: $("body"),
        minWidth: w,
        maxWidth: w,
        minHeight: hMin,
        stop: function(event, ui) { map.checkResize(); }
    });


    $("#tbTitle").val(title);
    $("#tbAddress").val(addr);

    if (!lat || !lng) {
        showAddress(srch, GetMarkerHtml(title, addr));
        $("#tbSrch").val(srch);
    }


}

function map_load(id, c, lat, lng, z, mx, my, mhtml, h, w, i, t) {
    if (GBrowserIsCompatible()) {

        var hMin = 200;
        if (h < hMin) h = hMin;

        if (w > 1)
            $(c).width(w);
        $(c).height(h);


        map = new GMap2(c);

        var loc = new GLatLng(lat, lng);
        map_loadMarker(map, mx, my, mhtml, i);
        map.setCenter(loc, z);
        map.setUIToDefault();
        map.addMapType(G_SATELLITE_3D_MAP);
        map.setMapType(getMapTypeByUrlArg(t));
    }

}
function map_loadMarker(mp, lat, lng, mhtml, openI) {

    var marker = new GMarker(new GLatLng(lat, lng), { draggable: true });
    mLat = lat;
    mLong = lng;
    GEvent.addListener(marker, "dragstart", function() { mp.closeInfoWindow(); });
    GEvent.addListener(marker, "dragend", function() {
        marker.openInfoWindowHtml(mhtml, { maxWidth: 150 });
        mLat = marker.getPoint().lat();
        mLong = marker.getPoint().lng();
    });
    GEvent.addListener(marker, "click", function() {
        marker.openInfoWindowHtml(mhtml, { maxWidth: 150 });
    });
    GEvent.addListener(mp, "infowindowclose", function() { iOpen = false; });
    GEvent.addListener(mp, "infowindowopen", function() { iOpen = true; });

    mp.addOverlay(marker);
    if (openI == true)
        marker.openInfoWindowHtml(mhtml, { maxWidth: 150 });


}


function showAddress(address, mhtml) {
    if (mhtml == null) mhtml = address;
    var geocoder = new GClientGeocoder();

    if (geocoder) {
        geocoder.getLatLng(address, function(point) {
            if (!point) {
                alert(" Naslov: " + address + " ni najden");
            } else {
                map.setCenter(point, 13);
                map_loadMarker(map, point.lat(), point.lng(), mhtml, true);
            }
        });
    }
}

function saveMap() {

    var lat = map.getCenter().lat();
    var lng = map.getCenter().lng();
    var z = map.getZoom();
    var c = $('#map');

    var tit = $("#tbTitle").val();
    var addr = $("#tbAddress").val();
    var srch = $("#tbSrch").val();
    var msg = GetMarkerHtml(tit, addr);

    WebServiceEMENIK.UpdateCwpMapEnc(k, editID, lat, lng, map.getZoom(), mLat, mLong, tit, addr, srch, c.height(), -1, iOpen, map.getCurrentMapType().getUrlArg(), OnMapUpdated);
}
function OnMapUpdated() {
    parent.window.location.href = ru;
}

function GetMarkerHtml(title, addr) {
    if (addr && addr.length != 1)
        addr = "<p>" + addr + "</p>";
    return "<h3>" + title + "</h3>" + addr;
}