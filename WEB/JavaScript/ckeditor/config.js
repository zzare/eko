﻿CKEDITOR.editorConfig = function (config) {
    config.toolbar = 'ProductDesc';

    config.toolbar_ProductDesc =
[
    ['Source'],
    ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Print'],
    ['Undo', 'Redo', '-', 'Find', 'Replace', '-', 'SelectAll', 'RemoveFormat'],
    ['Table' /*, 'Image', 'Flash', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak'*/],
    ['Maximize', 'ShowBlocks'],
    '/',
    ['Format', 'Bold', 'Italic', 'Underline', 'Strike', '-', 'Subscript', 'Superscript'],
    ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', 'Blockquote'],
    ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
    ['Link', 'Unlink', 'Anchor'],
    //'/',
    /*['Font', 'Styles', , 'FontSize'],*/
//    ['TextColor', 'BGColor'],

];

//    var _baseP = "/web/fckeditor/editor/";

//    config.filebrowserBrowseUrl = _baseP + 'filemanager/browser/default/browser.html?Connector=' + encodeURIComponent(_baseP + 'filemanager/connectors/aspx/connector.aspx');
//    config.filebrowserImageBrowseUrl = _baseP+ 'filemanager/browser/default/browser.html?Type=Image&Connector=' + encodeURIComponent(_baseP + 'filemanager/connectors/aspx/connector.aspx');
//    config.filebrowserUploadUrl = _baseP+ 'filemanager/connectors/aspx/upload.aspx';
//    config.filebrowserImageUploadUrl = _baseP + 'filemanager/connectors/aspx/upload.aspx?Type=Image';


};