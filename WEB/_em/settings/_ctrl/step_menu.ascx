﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="step_menu.ascx.cs" Inherits="_em_settings__ctrl_step_menu" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Namespace="SM.UI.Controls" TagPrefix="SM" %>




<SM:InlineScript ID="InlineScript1"  runat="server">



<script type="text/javascript">
function SetUniqueRadioButton(nameregex, current)
{
   re = new RegExp(nameregex);
   for(i = 0; i < document.forms[0].elements.length; i++){
      elm = document.forms[0].elements[i]
      if (elm.type == 'radio')
      {
         if (re.test(elm.name))
         {
            elm.checked = false;
         }
      }
   }
   current.checked = true;


}

//function newMenuLoad(smp) {
//    setModalLoaderLoading();
//    openModalLoading('Nalagam');
//    var _items = new Array();
//    Array.add(_items, smp);
//    Array.add(_items, em_g_lang);
//    Array.add(_items, '1. nivo');
//    WebServiceCWP.RenderViewAut(_items, 8, onPopupSuccessLoad, onError);
//}
//function editMenuLoad(smp) {
//    setModalLoaderLoading();
//    openModalLoading('Nalagam');
//    var _items = new Array();
//    Array.add(_items, smp);
//    Array.add(_items, em_g_lang);
//    WebServiceCWP.RenderViewAut(_items, 9, onPopupSuccessLoad, onError);
//}

function openSortMenu(p) {
        setModalLoaderLoading();
        openModalLoading('Nalagam');
        var _items = new Array();
        Array.add(_items, p);
        Array.add(_items, em_g_lang);
        WebServiceCWP.RenderViewAut(_items, 7, onPopupSuccessLoad, onError);
    }

    $(document).ready(function() {
        //alert('ready');
        $("#editMenuTabs").tabs();

    });

</script>
</SM:InlineScript>


<h1>Uredi menijsko strukturo spletne strani
    <a href="javascript:void(0)" class="btsettStatusHelp"><img src='<%=Page.ResolveUrl("~/_inc/images/icon/help.png") %>' /></a>
    <div class="settStatusWrapp" >
    <div class="settStatus">
        Uredi navigacijo na spletni strani. To pomeni, da boš poljubno uredil in dodal podstrani kot so o nas, blog, galerija...
    </div>
    </div>
</h1>

<% if (SM.EM.Security.Permissions.IsAdmin()) { %>
<h2>Dodaj nov menu</h2>
Naziv:<asp:TextBox ID="tbTitle" runat="server"></asp:TextBox>
<a id="btAddNewMenu" href="#" runat="server" onserverclick="btAddNewMenu_Click" >Dodaj nov menu</a>

<%} %>




<div id="editMenuTabs">
    <ul>
        <asp:ListView ID="lvListHeader" runat="server"  InsertItemPosition="None" >
            <LayoutTemplate>
                <asp:PlaceHolder id="itemPlaceholder" runat="server"></asp:PlaceHolder>
            </LayoutTemplate>
            
            <ItemTemplate>
                 <li><a href="#tabs-<%# Container.DataItemIndex %>"><%# Eval("MENU_TITLE")  %> <%# (SM.EM.Security.Permissions.IsAdvancedCms() ? "(id=" + Eval("MENU_ID") + ")" : "" )%> </a></li>
            </ItemTemplate>
        </asp:ListView>	 	        
    </ul>  
    
    
        <asp:ListView ID="lvListDetail" runat="server"  InsertItemPosition="None" >
            <LayoutTemplate>
                <asp:PlaceHolder id="itemPlaceholder" runat="server"></asp:PlaceHolder>
            </LayoutTemplate>
            
            <ItemTemplate>
                <div id="tabs-<%# Container.DataItemIndex %>" class="item" >
                    
                    <div class="wizzNav clearfix">
                        <div class="wizzNav_Twocols clearfix""> 
	                        <div class="wizzNav_Maincol" >
	                            <h2>Dodajanje nove podstrani:
	                                <a href="javascript:void(0)" class="btsettStatusHelp"><img src='<%# Page.ResolveUrl("~/_inc/images/icon/help.png") %>' /></a>
                                    <div class="settStatusWrapp" >
                                    <div class="settStatus">
                                    Novo podstran dodaš tako da klikneš na gumb "Dodaj podstran" in vpišeš ime nove podstrani ter klikneš na gumb "Dodaj".                
                                    </div></div>
	                            </h2>
	                            <p>* Na voljo imaš <%# ParentPageEmenik.em_user.SITEMAP_COUNT   %> podstrani</p>
	                            <div class="buttonwrapper">
                                    <%--<a href="javascript:void(0);" ID="btNew" class="lbutton lbuttonAdd"  runat="server" ><span>Dodaj podstran</span></a>--%>
                                    
                                    <a href="#" onclick="newMenuLoad(<%# Eval("ROOT_SMAP") %>); return false" class="lbutton lbuttonAdd"  ><span>Dodaj podstran</span></a>
                                    
                                </div>

	                        </div>
	                        <div class="wizzNav_Rightcol" >
	                            <h2>Vrstni red podstrani:
	                                <a href="javascript:void(0)" class="btsettStatusHelp"><img src='<%#Page.ResolveUrl("~/_inc/images/icon/help.png") %>' /></a>
                                    <div class="settStatusWrapp" >
                                    <div class="settStatus">
                                        Za urejanje vrstnega reda podstrani klikni na gumb "Uredi vrstni red" in potem s sistemom povleci & spusti uredi vrstni red podstrani (urejanje vrstnega reda spreminja vrstni red podstrani tudi na spletni strani).
                                    </div>
                                    </div>
	                            </h2>
	                            <p>* Poljubno uredi vrstni red podstrani</p>
	                            <div class="buttonwrapper">
	                                <a href="#" class="lbutton lbuttonEdit" onclick="openSortMenu(<%# Eval("ROOT_SMAP") %>); return false"><span>Uredi vrstni red</span></a>
	                                <%--<asp:LinkButton ID="btSort" runat="server" CssClass="lbutton lbuttonEdit" onclick="btSort_Click"><span>Uredi vrstni red</span></asp:LinkButton>--%>
                                </div>
                                <asp:HiddenField ID="hfSort" runat="server" />
	                        </div>
                        </div> 
	                    <div class="wizzNav_Leftcol" >
                        <div class="wizzNav_Leftcol_Menu"></div>    
                            <h2>Menijska struktura in tip postavitve:
   	                            <a href="javascript:void(0)" class="btsettStatusHelp"><img src='<%# Page.ResolveUrl("~/_inc/images/icon/help.png") %>' /></a>
                                <div class="settStatusWrapp" >
                                <div class="settStatus">
                                Za urejanje že ustvarjenih podstrani klikni na podstran, ki jo želiš urediti. Urediš lahko ime podstrani, njen prevod v tuj jezik in postavitev strani (določiš koliko stolpcev za vsebine želiš na podstrani).
                                </div></div>
                            </h2>
                            <p>* Klikni na želeno podstran za urejanje imena za vse izbrane jezike ter za določitev tipa strani.</p>

                            <div class="wizzNav_Leftcol_Menu">
                                <%# MENU.Render.MenuEditHierarchy( ((int)Eval("ROOT_SMAP")) , LANGUAGE.GetCurrentLang()) %>
	                        </div>
	                    </div>
                    </div>

                </div>
            </ItemTemplate>
        </asp:ListView>	    


</div>




<div class="wizzNav_Separator"></div>



