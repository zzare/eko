﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Collections.Specialized;
using System.Configuration.Provider;
using System.Security.Permissions;
using System.Web.Util;
using SM.EM.Util;

/// <summary>
/// Summary description for SmardtStaticSitemapProvider
/// </summary>
public abstract class SmardtStaticSitemapProvider : SiteMapProvider
{
    private readonly object _lock = new object();

    // Fields
    public Hashtable _childNodeCollectionTable;
    public Hashtable _keyTable;
    public Hashtable _parentNodeTable;
    public Hashtable _urlTable;

    // Methods
    protected SmardtStaticSitemapProvider()
    {
    }

    protected override void AddNode(SiteMapNode node)
    {
        this.AddNode(node, null);
    }




    protected  override void AddNode(SiteMapNode node, SiteMapNode parentNode)
    {
        if (node == null)
        {
            throw new ArgumentNullException("node");
        }
        lock (_lock)
        {
            bool flag = false;
            string url = node.Url;
            if (!string.IsNullOrEmpty(url))
            {
                if (HttpRuntime.AppDomainAppVirtualPath != null)
                {
                    
                    if (!UrlPath.IsAbsolutePhysicalPath(url))
                    {
                        url = UrlPath.MakeVirtualPathAppAbsolute(UrlPath.Combine(HttpRuntime.AppDomainAppVirtualPath.ToString(), url));
                    }
                    if (this.UrlTable[url] != null)
                    {
                        throw new InvalidOperationException("XmlSiteMapProvider_Multiple_Nodes_With_Identical_Url");
                    }
                }
                flag = true;
            }
            string key = node.Key;
            if (this.KeyTable.Contains(key))
            {
                throw new InvalidOperationException("XmlSiteMapProvider_Multiple_Nodes_With_Identical_Key");
            }
            this.KeyTable[key] = node;
            if (flag)
            {
                this.UrlTable[url] = node;
            }
            if (parentNode != null)
            {
                this.ParentNodeTable[node] = parentNode;
                if (this.ChildNodeCollectionTable[parentNode] == null)
                {
                    this.ChildNodeCollectionTable[parentNode] = new SiteMapNodeCollection();
                }
                ((SiteMapNodeCollection)this.ChildNodeCollectionTable[parentNode]).Add(node);
            }
        }
    }





 

    public abstract SiteMapNode BuildSiteMap();
    
    protected virtual void Clear()
    {
        lock (_lock)
        {
            if (this._childNodeCollectionTable != null)
            {
                this._childNodeCollectionTable.Clear();
            }
            if (this._urlTable != null)
            {
                this._urlTable.Clear();
            }
            if (this._parentNodeTable != null)
            {
                this._parentNodeTable.Clear();
            }
            if (this._keyTable != null)
            {
                this._keyTable.Clear();
            }
        }
    }


    public override SiteMapNode FindSiteMapNode(string rawUrl)
    {
        if (rawUrl == null)
        {
            throw new ArgumentNullException("rawUrl");
        }
        rawUrl = rawUrl.Trim();
        if (rawUrl.Length == 0)
        {
            return null;
        }
        if (UrlPath.IsAppRelativePath(rawUrl))
        {
            rawUrl = UrlPath.MakeVirtualPathAppAbsolute(rawUrl);
        }
        this.BuildSiteMap();
        return ReturnNodeIfAccessible((SiteMapNode)this.UrlTable[rawUrl]);
    }




    public override SiteMapNode FindSiteMapNodeFromKey(string key)
    {
        SiteMapNode node = base.FindSiteMapNodeFromKey(key);
        if (node == null)
        {
            node = (SiteMapNode)this.KeyTable[key];
        }
        return ReturnNodeIfAccessible(node);
    }

    public override SiteMapNodeCollection GetChildNodes(SiteMapNode node)
    {
        if (node == null)
        {
            throw new ArgumentNullException("node");
        }
        this.BuildSiteMap();
        SiteMapNodeCollection collection = (SiteMapNodeCollection)this.ChildNodeCollectionTable[node];
        if (collection == null)
        {
            SiteMapNode node2 = (SiteMapNode)this.KeyTable[node.Key];
            if (node2 != null)
            {
                collection = (SiteMapNodeCollection)this.ChildNodeCollectionTable[node2];
            }
        }
        if (collection == null)
        {
            return null;
        }
        if (!base.SecurityTrimmingEnabled)
        {
            return SiteMapNodeCollection.ReadOnly(collection);
        }
        HttpContext current = HttpContext.Current;
        SiteMapNodeCollection nodes2 = new SiteMapNodeCollection(collection.Count);
        foreach (SiteMapNode node3 in collection)
        {
            if (node3.IsAccessibleToUser(current))
            {
                nodes2.Add(node3);
            }
        }
        return SiteMapNodeCollection.ReadOnly(nodes2);
    }

    public override SiteMapNode GetParentNode(SiteMapNode node)
    {
        if (node == null)
        {
            throw new ArgumentNullException("node");
        }
        this.BuildSiteMap();
        SiteMapNode parentNode = (SiteMapNode)this.ParentNodeTable[node];
        if (parentNode == null)
        {
            SiteMapNode node3 = (SiteMapNode)this.KeyTable[node.Key];
            if (node3 != null)
            {
                parentNode = (SiteMapNode)this.ParentNodeTable[node3];
            }
        }
        if ((parentNode == null) && (this.ParentProvider != null))
        {
            parentNode = this.ParentProvider.GetParentNode(node);
        }
        return ReturnNodeIfAccessible(parentNode);
    }



    // Properties
    public IDictionary ChildNodeCollectionTable
    {
        get
        {
            if (this._childNodeCollectionTable == null)
            {
                lock (_lock)
                {
                    if (this._childNodeCollectionTable == null)
                    {
                        this._childNodeCollectionTable = new Hashtable();
                    }
                }
            }
            return this._childNodeCollectionTable;
        }
    }

    public IDictionary KeyTable
    {
        get
        {
            if (this._keyTable == null)
            {
                lock (_lock)
                {
                    if (this._keyTable == null)
                    {
                        this._keyTable = new Hashtable();
                    }
                }
            }
            return this._keyTable;
        }
    }

    public IDictionary ParentNodeTable
    {
        get
        {
            if (this._parentNodeTable == null)
            {
                lock (_lock)
                {
                    if (this._parentNodeTable == null)
                    {
                        this._parentNodeTable = new Hashtable();
                    }
                }
            }
            return this._parentNodeTable;
        }
    }

    public IDictionary UrlTable
    {
        get
        {
            if (this._urlTable == null)
            {
                lock (_lock)
                {
                    if (this._urlTable == null)
                    {
                        this._urlTable = new Hashtable(StringComparer.OrdinalIgnoreCase);
                    }
                }
            }
            return this._urlTable;
        }
    }

    protected SiteMapNode ReturnNodeIfAccessible(SiteMapNode node)
    {
        if ((node != null) && node.IsAccessibleToUser(HttpContext.Current))
        {
            return node;
        }
        return null;
    }





 

}


