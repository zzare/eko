﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_cmsCampaignEdit.ascx.cs" Inherits="SM.UI.Controls._cmsCampaignEdit" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="SM" TagName="UserSelect" Src="~/CMS/controls/user/_cmsUserSelectNoUP.ascx"  %>

<div class = "qContEdit clearfix">
        
    <asp:Button ValidationGroup="marketing" ID="btSave" runat="server" Text="SAVE" onclick="btSave_Click" CssClass="btnOrange " />
    <asp:Button  ID="Button1" runat="server" Text="Cancel" onclick="btCancel_Click" CssClass="btnOrange " />

    <br />
    <br />
    NAZIV: 
    <br />
    <asp:TextBox ID="tbName"  runat="server" Rows="3"></asp:TextBox>

    <br />
    OPIS: 
    <br />
    <asp:TextBox ID="tbDesc"  runat="server" TextMode="MultiLine" Rows="3"></asp:TextBox>
    <br />    

    <br />
    PROMOCIJSKA KODA: 
    <br />
    <asp:TextBox ID="tbPromoCode"  runat="server" ></asp:TextBox>
    <br />    
    
    <br />
    DIREKTEN PROMOCIJSKI LINK: 
    <br />
    <asp:TextBox ID="tbPromoLink"  runat="server" ReadOnly = "true" Width="500" ></asp:TextBox>
    <br />       

    <br />
    VELJAVNOST OD: 

    <asp:TextBox  ID="tbDateFrom" runat="server"></asp:TextBox>
    <asp:ImageButton ID="btDateFrom" ImageUrl="~/CMS/_res/images/button/calendar.png" runat="server" />&nbsp
    <cc1:CalendarExtender ID="ceDateFrom" runat="server" TargetControlID="tbDateFrom" PopupPosition="BottomLeft" PopupButtonID ="btDateFrom">
    </cc1:CalendarExtender>                            
    <cc1:MaskedEditExtender ID="meeDateFrom" runat="server" TargetControlID="tbDateFrom" Mask="99/99/9999" MessageValidatorTip="true" 
            MaskType="Date" DisplayMoney="Left" AcceptNegative="Left" ErrorTooltipEnabled="True"  CultureName="sl-SI" />                            
    <cc1:MaskedEditValidator ID="mevDateFrom" runat="server" ControlExtender="meeDateFrom" ControlToValidate="tbDateFrom"  InvalidValueMessage="* Datum je neveljaven"
        Display="Dynamic" TooltipMessage="" EmptyValueBlurredText="" InvalidValueBlurredMessage=""  ValidationGroup="marketing" />   


    VELJAVNOST DO: 

    <asp:TextBox  ID="tbDateTo" runat="server"></asp:TextBox>
    <asp:ImageButton ID="btDateTo" ImageUrl="~/CMS/_res/images/button/calendar.png" runat="server" />&nbsp
    <cc1:CalendarExtender ID="ceDateTo" runat="server" TargetControlID="tbDateTo" PopupPosition="BottomLeft" PopupButtonID ="btDateTo">
    </cc1:CalendarExtender>                            
    <cc1:MaskedEditExtender ID="meeDateTo" runat="server" TargetControlID="tbDateTo" Mask="99/99/9999" MessageValidatorTip="true" 
            MaskType="Date" DisplayMoney="Left" AcceptNegative="Left" ErrorTooltipEnabled="True"  CultureName="sl-SI" />                            
    <cc1:MaskedEditValidator ID="mevDateTo" runat="server" ControlExtender="meeDateTo" ControlToValidate="tbDateTo"  InvalidValueMessage="* Datum je neveljaven"
        Display="Dynamic" TooltipMessage="" EmptyValueBlurredText="" InvalidValueBlurredMessage=""  ValidationGroup="marketing" />   
    <br />
    <br />    
    
    
    
    <strong>POPUST ZA NOVEGA UPORABNIKA:</strong>
    <br />
    <asp:TextBox ID="tbUserDiscount"  runat="server" ></asp:TextBox>%
    &nbsp;&nbsp;&nbsp;
    NANAŠA SE NA: 
    <asp:DropDownList ID="ddlUserDiscount" runat="server">
        <asp:ListItem Text="- izberi vrsti popusta -" Value="" ></asp:ListItem>
    </asp:DropDownList>
    
    <br />    



    <br />


    <strong>AFFILLIATE PROVIZIJA</strong>
    Vrsta provizije 
    <asp:DropDownList ID="ddlType" runat="server" >
        <asp:ListItem Text="Procent" Value="0" ></asp:ListItem>
        <asp:ListItem Text="€" Value="1" ></asp:ListItem>
    </asp:DropDownList> 
    <br />
    MARKETING provizijo dobi uporabnik:
    <asp:ImageButton ID="btSetMarketing" OnClick="btSetMarketing_Click" runat="server" ImageUrl="~/_inc/images/button/lookup.png" />
    <asp:Literal ID="tbMarketingUser"  runat="server"></asp:Literal>
    <asp:HiddenField ID="hfMarketing" runat="server" />
    &nbsp;&nbsp;&nbsp;
    Provizija:
    <asp:TextBox ID="tbMarketingPercent"  runat="server"></asp:TextBox>
    <cc1:FilteredTextBoxExtender runat="server" TargetControlID = "tbMarketingPercent"  FilterType="Custom,Numbers" ValidChars=","></cc1:FilteredTextBoxExtender>

    <br />
    AFFILIATE provizijo dobi uporabnik::
    <asp:ImageButton ID="ImageButton1" OnClick="btSetAffiliate_Click" runat="server" ImageUrl="~/_inc/images/button/lookup.png" />
    <asp:Literal ID="tbAffiliateUser"  runat="server"></asp:Literal>
    <asp:HiddenField ID="hfAffiliate" runat="server" />
    &nbsp;&nbsp;&nbsp;
    Provizija:
    <asp:TextBox ID="tbAffiliatePercent"  runat="server"></asp:TextBox>
    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID = "tbAffiliatePercent" FilterType="Numbers,Custom" ValidChars=","></cc1:FilteredTextBoxExtender>

    
    
    

    
</div>

<SM:UserSelect ID="objSelect" runat="server"  />