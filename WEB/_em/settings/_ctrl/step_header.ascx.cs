﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SM.EM.UI.Controls;
using System.Drawing;

public partial class _em_settings__ctrl_step_header : BaseWizardStep {
    public bool HasStepChanged { 
        get {
            if (ViewState["HasStepChanged"] == null) return false;
            return Boolean.Parse(ViewState["HasStepChanged"].ToString());
        } 
        set {
            ViewState["HasStepChanged"] = value;
        } 
    }
    public int ImgID
    {
        get
        {
            int id;
            if (!Int32.TryParse(hfImg.Value, out id)) return -1;
            return id;
        }
        set
        {
            hfImg.Value = value.ToString();
        }
    }
    public int ImgTypeID
    {
        get
        {
            int id;
            if (!Int32.TryParse(hfType.Value, out id)) return -1;
            return id;
        }
        set
        {
            hfType.Value = value.ToString();
        }
    }

    public int ImgHID
    {
        get
        {
            int id;
            if (!Int32.TryParse(hfImgH.Value, out id)) return -1;
            return id;
        }
        set
        {
            hfImgH.Value = value.ToString();
        }
    }
    public int ImgTypeHID
    {
        get
        {
            int id;
            if (!Int32.TryParse(hfTypeH.Value, out id)) return -1;
            return id;
        }
        set
        {
            hfTypeH.Value = value.ToString();
        }
    }

    protected const int MAX_IMAGE_WIDTH = 600;

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        //Description = "Določite slike, ki se prikazujejo v glavi vaše spletne strani.<br/>Določite logo in glavno sliko, če je to možno.";

        Description = "V glavi spletne strani se nahaja slika, ki ji pravimo motiv, natančneje motiv spletne strani. ";
        Description += "Iz svojega računalniko prenesi sliko, ki ti ustreza. ";
        Description += "Na ali ob motiv je možno dodati naziv spletne strani ali pa dodate logotip vašega podjetja. ";


        ShortDesc = "Izberi sliko za v motiv spletne strani";

        //Description = "<ul><li>";
        //Description += "V tem koraku boste določili kaj se bo pojavilo v glavi vaše spletne strani. Na izbiro imate, da ta prostor pustite prazen, da napišete tekst ali pa da naložite logotip vašega podjetja. V primeru da logotipa nimate, se lahko obrnete na nas <a href=\"http://www.smardt.si\">Smardt d.o.o.</a> in poslali vam bomo ponudbo  " + "za izdelavo logotipa";
        //Description += "</li><li>";
        //Description += "Če se odločiš, da ne boš imel ničesar enostavno obkljukaj \"Ne želim ne loga ne teksta\". ";
        //Description += "</li><li>";
        //Description += "Če želiš imeti samo tekst potem odkljukaj \"V glavi strani želim imeti tekst - moj naziv\", le tega se uredi v sedmem koraku tega čarovnika. Za predogled pa bo uporabljen privzeti naslov Moja spletna stran.";
        //Description += "</li><li>";
        //Description += "DODAJANJE LOGOTIPA V GLAVO STRANI: V primeru, da želiš na strani imeti objavljen svoj logotip ali logotip vašega podjetja, odkljukaj \"V glavi strani želim prikazan logo\". To bo odprlo možnost nalaganja vašega logotipa. Enostavno klikni \"Browse/Prebrskaj\" na računalniku poišči logotip (format logotipa mora biti .jpg, .png ali .gif) in kliknite \"ok/vredu\". Za začetek nalaganja klikni \"Prenesi\" in sistem bo samodejno prenesel sliko z računalnika in jo po potrebi pomanjšal. ";
        //Description += "</li><li>";
        //Description += "UREJANJE LOGOTIPA: Sliko lahko urediš tudi sam s klikom na \"Popravi sliko\". Sistem bo samodejno ponudil višino in širino, ki ustreza obliki vaše spletne strani, ti polje le premakneš tako, da zajamete želeni del slike. Potem klikni \"Shrani\". ";
        //Description += "</li><li>";
        //Description += "Za potrditev celotnega koraka moraš pritisniti še na gumb \"Shrani spremembe\".";
        //Description += "</li></ul>";
        //ShortDesc = "Uredi glavo svoje strani";

        StepID = 4;
        //AllwaysShowSaveButton = true; // todo : remove

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!IsPostBack)
         // load javascript
        imgMain.Attributes["onload"] = "SetMaxImageWidth(this, " + MAX_IMAGE_WIDTH + ")";
        imgMainH.Attributes["onload"] = "SetMaxImageWidth(this, " + MAX_IMAGE_WIDTH + ")";
      
    }



    public override void LoadData()
    {
        //if (IsPostBack) return;

        // init check boxes
        cbShowLogo.Checked = ParentPageEmenik.em_user.SHOW_LOGO;
        if (ParentPageEmenik.AllwaysShowTitle)
        {
            // both logo and text can be set
            cbShowLogo.Attributes["onclick"] = "simulateCB(); toggleLogo(this)";
            cbShowText.Attributes["onclick"] = "simulateCB()";
            cbShowText.Checked = ParentPageEmenik.em_user.SHOW_TEXT;
        }
        else
        {
            // only one, logo or text can be set
            cbShowLogo.Attributes["onclick"] = "simulateRB(this); toggleLogo(this)"; // simulates RADIO button - only one option
            cbShowText.Attributes["onclick"] = "simulateRB(this)";
            cbShowText.Checked = ( !ParentPageEmenik.em_user.SHOW_LOGO &&  ParentPageEmenik.em_user.SHOW_TEXT); 

            litNote.Text = "&nbsp;* opomba: pri trenutnem designu lahko izbirate med sliko LOGOTIPA ali TEKSTOM (nazivom). Trenuten design ne omogoča obojega hkrati.";
        }

        // select option NOTHING if no other is set
        if (!cbShowLogo.Checked && !cbShowText.Checked)
            cbNothing.Checked = true;

        
        // if image not set, hide image and manipulation buttons
        if (ParentPageEmenik.em_user.IMG_LOGO == null || ParentPageEmenik.em_user.IMG_LOGO_ID == null || ParentPageEmenik.em_user.TYPE_LOGO_ID == null || String.IsNullOrEmpty(ParentPageEmenik.em_user.IMG_LOGO))
        {

            panImageLogo.Visible = false;
        }
        else // load image data
        {
            panImageLogo.Visible = true;
            imgMain.Src = ParentPageEmenik.em_user.IMG_LOGO;

        }
        // set type and logo
        if (ParentPageEmenik.em_user.TYPE_LOGO_ID != null){
            ImgTypeID = ParentPageEmenik.em_user.TYPE_LOGO_ID.Value;
        }
        if (ParentPageEmenik.em_user.IMG_LOGO_ID != null){
            ImgID = ParentPageEmenik.em_user.IMG_LOGO_ID.Value;
        }


        
        // load image sizes
        if (ParentPageEmenik.em_user.TYPE_LOGO_ID != null)
        {


            string height = "";
            string width = "";
            IMAGE_TYPE.RenderImageTypeDimensions(ParentPageEmenik.em_user.TYPE_LOGO_ID.Value, out width, out height, "px");

            litHight.Text = "višina: " + height;
            litWidth.Text = "širina: " + width;
        }


        // load manual cropping
        //imgBig.Src = ParentPageEmenik.em_user.IMAGE.IMAGE_ORIG.IMG_URL;


        // HEADER image
        MASTER m  = TEMPLATES.GetMasterByID(ParentPageEmenik.em_user.MASTER_ID.Value);
        if (m.HAS_HEADER_IMAGE){
            panHeader.Visible = true;

            // if image not set, hide image and manipulation buttons
            if (ParentPageEmenik.em_user.IMG_HEADER  == null || ParentPageEmenik.em_user.IMG_HEADER_ID == null || ParentPageEmenik.em_user.TYPE_HEADER_ID== null || String.IsNullOrEmpty(ParentPageEmenik.em_user.IMG_HEADER ))
            {

                panImageH.Visible = false;
            }
            else // load image data
            {
                panImageH.Visible = true;
                imgMainH.Src = ParentPageEmenik.em_user.IMG_HEADER ;

            }
            // set type and logo
            // cehck master
            ImgTypeHID = m.HDR_TYPE_ID.Value ;
            if (ParentPageEmenik.em_user.IMG_HEADER_ID  != null)
            {
                ImgHID = ParentPageEmenik.em_user.IMG_HEADER_ID.Value;
            }


            // load image sizes
            if (m.HDR_TYPE_ID != null)
            {



                string height = "";
                string width = "";
                IMAGE_TYPE.RenderImageTypeDimensions(m.HDR_TYPE_ID.Value, out width, out height, "px");


                litHeightH.Text = "višina: " + height;
                litWidthH.Text = "širina: " + width;
            }

        }
        else {
            panHeader.Visible = false;
        }





    }

    protected void btUpload_OnClick(object sender, EventArgs e) {

        // if is demo, can't save
        if (!CheckCanSave())
            return ;


        // file is specified
        if (fuImage.HasFile)
        {
            try
            {
                // validate file type
                cvFileType.Validate();
                if (!cvFileType.IsValid) return;


                // validate file size
                cvFileSize.Validate();
                if (!cvFileSize.IsValid) return;

                string fileType = System.IO.Path.GetExtension(fuImage.FileName);

                IMAGE img = IMAGE.Func.AddNewImage(System.Drawing.Image.FromStream(fuImage.PostedFile.InputStream), ImgTypeID, true, fileType , "");
                imgMain.Src = img.IMG_URL;

                ImgID = img.IMG_ID;
                ImgTypeID = img.TYPE_ID;


                // clear popup data
                hfLogoVirtualUrl.Value = "";

                // show panel with image
                panImageLogo.Visible = true;

            }
            catch (Exception ex)
            {
                throw;
            }
        }
        else
        {
            //Label1.Text = "You have not specified a file.";
        }

    
    }

    protected void btUploadH_OnClick(object sender, EventArgs e)
    {
        // if is demo, can't save
        if (!CheckCanSave())
            return ;



        // file is specified
        if (fuImageH.HasFile)
        {
            try
            {
                // validate file type
                cvFileTypeH.Validate();
                if (!cvFileTypeH.IsValid) return;


                // validate file size
                cvFileSizeH.Validate();
                if (!cvFileSizeH.IsValid) return;

                string fileType = System.IO.Path.GetExtension(fuImageH.FileName);

                IMAGE img = IMAGE.Func.AddNewImage(System.Drawing.Image.FromStream(fuImageH.PostedFile.InputStream), ImgTypeHID, true, fileType, "" );
                imgMainH.Src = img.IMG_URL;

                ImgHID = img.IMG_ID;
                ImgTypeHID = img.TYPE_ID;


                // clear popup data
                hfHeaderVirtualURL.Value = "";

                // show panel with image
                panImageH.Visible = true;

            }
            catch (Exception ex)
            {
                throw;
            }
        }
        else
        {
            //Label1.Text = "You have not specified a file.";
        }


    }



    public override bool SaveData(int step)
    {

        // if is demo, can't save
        if (!CheckCanSave())
            return false;

        UpdateUserStep(step);

        // LOGO
        EM_USER.UpdateShowLogo(ParentPageEmenik.em_user.UserId, cbShowLogo.Checked, cbShowText.Checked );

        // if image was custom cropped, change it
        if (!string.IsNullOrEmpty(hfLogoVirtualUrl.Value ))
        {
            IMAGE.Func.ChangeImage(ImgID, ImgTypeID, hfLogoVirtualUrl.Value);
        }

        IMAGE img = IMAGE.GetImageByID(ImgID, ImgTypeID); 
        if (img != null)
            EM_USER.UpdateImageLogo(ParentPageEmenik.em_user.UserId, img);

        // HEADER
        if (panHeader.Visible)
        {
            // if image was custom cropped, change it
            if (!string.IsNullOrEmpty(hfHeaderVirtualURL.Value))
            {
                IMAGE.Func.ChangeImage(ImgHID, ImgTypeHID, hfHeaderVirtualURL.Value);

            }
            IMAGE imgH = IMAGE.GetImageByID(ImgHID, ImgTypeHID);
            if (imgH != null)
                EM_USER.UpdateImageHeader(ParentPageEmenik.em_user.UserId, imgH);

        }


        if (!ParentPageEmenik.em_user.ACTIVE_USER) step++;
        // redirect to same page( or next page, depending on wizard mode) to refresh image in header
        Response.Redirect(SM.EM.Rewrite.EmenikUserSettingsUrlByStep(ParentPageEmenik.PageName, step ));

        return true;        
    }

    public override bool Validate()
    {


        bool isValid = true;
        
        MASTER m  = TEMPLATES.GetMasterByID(ParentPageEmenik.em_user.MASTER_ID.Value);

        ErrorText = "Podatki niso pravilni. ";
        
        if (cbShowLogo.Checked)
        {
            // there must be LOGO image set, if LOGO is checked
            if (string.IsNullOrEmpty(imgMain.Src.Trim()))
            {
                rfvFile.IsValid = false;
                isValid = false;
                ErrorText += "Vnesite sliko loga. ";

            }
         
        }

        // op: validation of HEADER is DISABLED. Every master has default HEADER PIC...
        //if (m.HAS_HEADER_IMAGE ) {
        //    // there must be HEADER image set
        //    if (string.IsNullOrEmpty(imgMainH .Src.Trim()))
        //    {
        //        rfvFileH.IsValid = false;
        //        isValid = false;
        //        ErrorText += "Vnesite sliko motiva. ";
        //    }
        //}

            
        return isValid ;
            
    }



    protected void ValidateFileType(object source, ServerValidateEventArgs args)
    {
        args.IsValid = false;
        if (fuImage.HasFile)
            if (fuImage.PostedFile.ContentType == "image/gif" || fuImage.PostedFile.ContentType == "image/png" || fuImage.PostedFile.ContentType == "image/x-png" || fuImage.PostedFile.ContentType == "image/jpeg" || fuImage.PostedFile.ContentType == "image/pjpeg" || fuImage.PostedFile.ContentType == "image/jpg")
                args.IsValid = true;
    }
    protected void ValidateFileSize(object source, ServerValidateEventArgs args)
    {
        args.IsValid = false;
        CustomValidator cv = (CustomValidator)source;
        cv.ErrorMessage = "* Slika mora biti manjša od " + (IMAGE.Common.MAX_IMAGE_UPLOAD_SIZE / 1024).ToString() + "KB. Prosim, uporabite manjšo sliko.";
        if (fuImage.HasFile)
            if (fuImage.PostedFile.ContentLength < IMAGE.Common.MAX_IMAGE_UPLOAD_SIZE  )
                args.IsValid = true;

    }
    protected void ValidateFileTypeH(object source, ServerValidateEventArgs args)
    {
        args.IsValid = false;
        if (fuImageH.HasFile)
            if (fuImageH.PostedFile.ContentType == "image/gif" || fuImageH.PostedFile.ContentType == "image/png" || fuImageH.PostedFile.ContentType == "image/x-png" || fuImageH.PostedFile.ContentType == "image/jpeg" || fuImageH.PostedFile.ContentType == "image/pjpeg" || fuImageH.PostedFile.ContentType == "image/jpg")
                args.IsValid = true;
    }
    protected void ValidateFileSizeH(object source, ServerValidateEventArgs args)
    {
        args.IsValid = false;
        CustomValidator cv = (CustomValidator)source;
        cv.ErrorMessage = "* Slika mora biti manjša od " + (IMAGE.Common.MAX_IMAGE_UPLOAD_SIZE / 1024).ToString() + "KB. Prosim, uporabite manjšo sliko.";
        if (fuImageH.HasFile)
            if (fuImageH.PostedFile.ContentLength < IMAGE.Common.MAX_IMAGE_UPLOAD_SIZE)
                args.IsValid = true;

    }

    protected void btUserCrop_OnClick(object source, EventArgs e) {

        // init popup data
        popupCrop.ImgID = ImgID;
        //popupCrop.AutoSaveImage = true;
        popupCrop.ImgTypeID = ImgTypeID;
        popupCrop.ImgTargetClientID = imgMain.ClientID;
        popupCrop.TargetVirtualURLClientID = hfLogoVirtualUrl.ClientID;
        popupCrop.OpenPopup();
    
    }

    protected void btUserCropH_OnClick(object source, EventArgs e)
    {

        // init popup data
        popupCrop.ImgID = ImgHID;
        popupCrop.ImgTypeID = ImgTypeHID;
        popupCrop.ImgTargetClientID = imgMainH.ClientID;
        popupCrop.TargetVirtualURLClientID = hfHeaderVirtualURL.ClientID;
        popupCrop.OpenPopup();

    }

}
