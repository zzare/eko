﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;


namespace SM.UI.Controls
{
    public partial class _cmsErrorList : BaseControl 
    {
        public delegate void OnEditEventHandler(object sender, SM.BLL.Common.Args .IDguidEventArgs e);
        public event OnEditEventHandler OnEdit;
        public event OnEditEventHandler OnDelete;
        public string OnEditTarget = "_self";
        public bool ShowCheckboxColumn { get; set; }
        public bool ShowManageColumn { get; set; }
        public bool ShowEditColumn { get; set; }
        public int PageSize = 20;
        public bool DeleteAutomatically = true;

        public string SearchText { get; set; }
        public bool IsAdmin { get; set; }
        public int ErrorID { get; set; }

        
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        protected void ldsList_OnSelecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            eMenikDataContext db = new eMenikDataContext();

            var list = db.ERROR_LOGs.OrderByDescending(w => w.DateError);

            //if (!String.IsNullOrEmpty(SearchText))
            //    list = list.Where(w => ((w.FIRSTNAME ?? "").Contains(SearchText) || (w.LASTNAME ?? "").Contains(SearchText) || w.EMAIL.Contains(SearchText) || w.USERNAME.Contains(SearchText) || w.PAGE_NAME.Contains(SearchText)   )).ToList();


            // hide admin users for non admin users
            //if (!Page.User.IsInRole("admin"))
            //    list = list.Where(w => !Roles.IsUserInRole(w.USERNAME , "admin")).ToList();


            // result
            e.Result = list;
        }



        protected string RenderDetailHref(int id) {
            return Page.ResolveUrl("~/cms/ManageError.aspx?smp=459&id=" + id.ToString());
        }




    }

}