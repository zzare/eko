﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="popupNotifyFriend.ascx.cs" Inherits="SM.EM.UI.Controls.popupNotifyFriend" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Namespace="SM.UI.Controls" TagPrefix="SM"  %>


<asp:HiddenField ID="hfPopup" runat="server" />

        
<asp:Panel CssClass="modPopup" ID="panSettings" runat="server" style="display:none;">
    <div class="modPopup_header modPopup_headerSettings">
        <h4>PRIDOBI BONUS</h4>
        <p> </p>
    </div>
    <div class="modPopup_main">
    <p>
        <br />
        Od vsakega uporabnika, ki ga pridobiš in si naredi spletno stran, ti pridobiš bonus na svoj račun. Takoj, ko je evidentirano njegovo plačilo, se tebi samodejno podaljša datum veljavnosti tvoje spletne strani.
        <br />
        **  v sporočilo bo samodejno dodana povezava, ki bo omogočila tvoj bonus.
        <br />
        <br />
        Email:
        <asp:RequiredFieldValidator ValidationGroup="notify" ID="valRequireEmail" runat="server" ControlToValidate="tbEmail" SetFocusOnError="true" Display="Dynamic"
        ErrorMessage="Vpiši e-mail." ToolTip="Vpiši e-mail." >*</asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator runat="server" ID="valEmailPattern"  Display="Dynamic" SetFocusOnError="true" ValidationGroup="notify"
        ControlToValidate="tbEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ErrorMessage="Email naslov ni pravilne oblike.">*</asp:RegularExpressionValidator>
        <br />
        
        <asp:TextBox ID="tbEmail" Width="500" class="modPopup_TBXsettings" runat="server"></asp:TextBox>
        <br />
        <br />
        Sporočilo:
        <br />
        <asp:TextBox ID="tbMessage" Width="500" class="modPopup_TBXsettings" runat="server" TextMode="MultiLine" Rows="5"></asp:TextBox>    
        
        
        <asp:Literal ID="litError" runat="server" EnableViewState = "false"></asp:Literal>

        <br /> 
        </p>
    </div>
    <div class="modPopup_footer">
        <div class="buttonwrapperModal">
            <asp:LinkButton ValidationGroup="AdvSet" ID="btSend" runat="server" CssClass="lbutton lbuttonConfirm" OnClick="btSaveEdit_Click" ><span>Pošlji</span></asp:LinkButton>
            <asp:LinkButton  ID="btCancel" runat="server" CssClass="lbutton lbuttonDelete"><span>Prekliči</span></asp:LinkButton>
        </div>
    </div>
</asp:Panel>

           
<cc1:ModalPopupExtender RepositionMode="RepositionOnWindowResize"  BehaviorID="mpeSendNotifyFriend"  ID="mpeSendNotifyFriend" runat="server" TargetControlID="hfPopup" PopupControlID="panSettings"
                        BackgroundCssClass="modBcg" CancelControlID="btCancel" >
</cc1:ModalPopupExtender> 


