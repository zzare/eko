﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _ctrl_module_product_viewProductDetailFiles : BaseControlView
{

    public object Data;
    public object[] Parameters;
    public vw_Product Prod;



    protected void Page_Load(object sender, EventArgs e)
    {
        

        //LoadList();
    }

    protected override void OnPreRender(EventArgs e)
    {
        LoadList();
        base.OnPreRender(e);
    }

    public void LoadList() {

        if (Data == null)
            return;
        Prod = Data as vw_Product;

        if (Parameters != null)
        {
            if (Parameters[0] != null)
                IsModeCMS = bool.Parse(Parameters[0].ToString());
            //if (Parameters.Length > 1 && Parameters[1] != null)
            //    LangID = Parameters[1].ToString();
        }


        // attribute
        ProductRep prep = new ProductRep();

        FileRep frep = new FileRep();

        var data = frep.GetFileListByProductLang(Prod.PRODUCT_ID, Prod.LANG_ID );

        lvFileList.DataSource = data;
        lvFileList.DataBind();  
        
    }



}
