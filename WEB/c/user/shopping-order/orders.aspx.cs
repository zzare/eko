﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class c_user_shopping_order_order : SM.EM.UI.BasePage_USER_EMENIK
{
    protected override void OnPreInit(EventArgs e)
    {

        CurrentSitemapID = 1446;

        base.OnPreInit(e);

    }

    protected Guid OrderID
    {
        get
        {
            if (Request.QueryString["o"] == null)
                return Guid.Empty;
            if (!SM.EM.Helpers.IsGUID(Request.QueryString["o"]))
                return Guid.Empty;

            return new Guid(Request.QueryString["o"]);
        }
    }

    protected Guid PageId { get { return SM.BLL.Custom.MainUserID; } }

    protected void Page_Load(object sender, EventArgs e)
    {
        // event handlers
        objOrderEdit.OnUserChanged += new EventHandler(objOrderEdit_OnOrderChanged);
        objOrderEdit.OnCancel += new EventHandler(objOrderEdit_OnCancel);
        objFilter.OnFilterChanged += new EventHandler(objFilter_OnFilterChanged);
        objOrderList.OnDelete += new SM.UI.Controls._shoppuing_order_cntOrderList .OnEditEventHandler(objOrderList_OnDelete);
        objOrderList.OnEdit += new SM.UI.Controls._shoppuing_order_cntOrderList.OnEditEventHandler(objOrderList_OnEdit);

        // init
        objOrderEdit.PageID = PageId;
        objOrderList.PageID = PageId;


        ApplyFilters();

        if (!IsPostBack)
        {
            // if Order is set as QS, show edit Order
            if (OrderID != Guid.Empty)
            {
                LoadData();

            }

            else // normal load
            {
                HideTabs();
                cpeQList.Collapsed = false;
                objOrderList.BindData();
            }

        }
    }


    protected void LoadData()
    {

        objOrderEdit.OrderID = OrderID;
        objOrderEdit.BindData();
        cpeQList.Collapsed = true;
        cpeQList.ClientState = "true";

        objOrderList.BindData();



        ShowTabs();
    }

    void objOrderEdit_OnCancel(object sender, EventArgs e)
    {
        HideTabs();

        cpeQList.Collapsed = false;
        cpeQList.ClientState = "false";

    }


    protected void ApplyFilters()
    {
        // list
        objOrderList.SearchText = objFilter.SearchText;
        objOrderList.Active = objFilter.Active;
        objOrderList.Status = objFilter.Status;

        objOrderEdit.OrderID = OrderID;
        //objOrderList.RoleID = objFilter.Role;

        //objOrderEdit.ProjectID = ProjectID;
        //objOrderEdit.SiteID = SiteID;



    }
    // filter
    void objFilter_OnFilterChanged(object sender, EventArgs e)
    {
        // filters
        ApplyFilters();

        // refresh question lists
        objOrderList.BindData();



    }
    //edit
    void objOrderEdit_OnOrderChanged(object sender, EventArgs e)
    {
        // refresh Order lists
        ApplyFilters();

        // refresh Order lists
        objOrderList.BindData();

        // refresh edit
        objOrderEdit.BindData();


    }

    //list
    void objOrderList_OnEdit(object sender, SM.BLL.Common.Args.IDguidEventArgs e)
    {
        // show tabs
        ShowTabs();


        // apply filters
        ApplyFilters();



    }
    void objOrderList_OnDelete(object sender, SM.BLL.Common.Args.IDguidEventArgs e)
    {
        ApplyFilters();

        // refresh group lists
        objOrderList.BindData();

    }




    protected void ShowTabs()
    {
        tabOrder.Visible = true;
        tabOrder.ActiveTabIndex = 0;

    }
    protected void HideTabs()
    {
        tabOrder.Visible = false;

    }


}