﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_cntRegisterShort.ascx.cs" Inherits="_ctrl_main_page_cntRegisterShort" %>
<%@ Register Namespace="SM.UI.Controls" TagPrefix="SM"  %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="EM" TagName="MailConfirmation" Src="~/_mailTemplates/_ctrlNewUserConfirmationMain.ascx"  %>


<asp:Panel ID="panLogin" runat="server" DefaultButton="btRegister">
        <table class="TABLE_quickregister" cellpadding="0">
            <tr>
                <td class="TD_right"><asp:Label runat="server" ID="lblUserName" AssociatedControlID="UserName" Text="Ime:" /></td>
                <td>
                    <asp:TextBox runat="server" ID="UserName" /><br />
                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" WatermarkCssClass="modPopup_mainWatermark"  WatermarkText="vpiši ime" TargetControlID ="UserName"></cc1:TextBoxWatermarkExtender>

                </td>
            </tr>
            <tr class="tr_validation">
                <td></td>
                <td>
                    <asp:RequiredFieldValidator ID="valRequireUserName" runat="server" ControlToValidate="UserName" SetFocusOnError="true" Display="Dynamic"
                                            ErrorMessage="* Vpiši želeno uporabniško ime." ToolTip ="* Vpiši želeno uporabniško ime." ValidationGroup="regShort"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator runat="server" ID="revUsername"  Display="Dynamic" SetFocusOnError="true" ValidationGroup="regShort"
                                            ControlToValidate="UserName" ValidationExpression="\w+([-+.']\w+)*" ToolTip="* Uporabniško ime lahko vsebuje le črke in znake:- _ ." ErrorMessage="* Uporabniško ime lahko vsebuje le črke in znake:- _ ."></asp:RegularExpressionValidator>
                    <asp:CustomValidator ValidationGroup="regShort" ID="cvUsername" runat="server" Display="Dynamic" ErrorMessage="* Uporabniško ime je že zasedeno." OnServerValidate="ValidateDuplicate"></asp:CustomValidator>
                    <asp:CustomValidator ValidationGroup="regShort" ID="cvUsernameValid" runat="server" Display="Dynamic" ErrorMessage="* Uporabniško ime ni pravilne oblike." OnServerValidate="ValidateUserName"></asp:CustomValidator>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="TD_right"><asp:Label runat="server" ID="lblPassword" AssociatedControlID="Password" Text="Geslo:" /></td>
                <td>
                    <asp:TextBox runat="server" ID="Password" TextMode="Password" /><br />
                </td>
            </tr>
            <tr class="tr_validation">
                <td></td>
                <td>
                    <asp:RequiredFieldValidator ID="valRequirePassword" runat="server" ControlToValidate="Password" SetFocusOnError="true" Display="Dynamic"
                        ErrorMessage="* Vpiši želeno geslo." ToolTip="* Vpiši želeno geslo." ValidationGroup="regShort"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="valPasswordLength" runat="server" ControlToValidate="Password" SetFocusOnError="true" Display="Dynamic"
                    ValidationExpression=".{5,}" ErrorMessage="* Geslo mora biti dolgo vsaj pet (5) znakov." ToolTip="* Geslo mora biti dolgo vsaj pet (5) znakov."
                    ValidationGroup="regShort"></asp:RegularExpressionValidator>&nbsp;
                </td>
            </tr>
            <tr>
                <td class="TD_right"><asp:Label runat="server" ID="lblEmail" AssociatedControlID="Email" Text="E-pošta:" /></td>
                <td>
                    <asp:TextBox runat="server" ID="Email" /><br />
                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" WatermarkCssClass="modPopup_mainWatermark "  WatermarkText="vpiši pravilen e-poštni naslov" TargetControlID ="Email"></cc1:TextBoxWatermarkExtender>
                </td>
            </tr>
            <tr class="tr_validation">
                <td></td>
                <td>
                    <asp:RequiredFieldValidator ID="valRequireEmail" runat="server" ControlToValidate="Email" SetFocusOnError="true" Display="Dynamic"
                        ErrorMessage="* Vpiši e-poštni naslov." ToolTip="* Vpiši e-poštni naslov." ValidationGroup="regShort"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator runat="server" ID="valEmailPattern"  Display="Dynamic" SetFocusOnError="true" ValidationGroup="regShort"
                        ControlToValidate="Email" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ErrorMessage="* E-poštni naslov ni pravilne oblike."></asp:RegularExpressionValidator>
                    <asp:CustomValidator ValidationGroup="regShort" ID="cvEmail" runat="server" Display="Dynamic" ErrorMessage="* E-poštni naslov ni veljaven." OnServerValidate="ValidateEmail"></asp:CustomValidator>
                    &nbsp;
                </td>
            </tr>
            <tr><td></td>
                <td class="cbx" >
                    <asp:CheckBox  TextAlign ="Right" ID="cbAccept" runat="server" Text = "&nbsp;Strinjam se s "   />
                    <a href='<%= Page.ResolveUrl("~/si/pogoji-uporabe.aspx") %>' target="_blank">pogoji uporabe</a>.
                    <cc1:ToggleButtonExtender ID="ToggleEx" runat="server"
                        TargetControlID="cbAccept" 
                        ImageWidth="19" 
                        ImageHeight="19" 
                        CheckedImageAlternateText="Odkljukaj"
                        UncheckedImageAlternateText="Obkljukaj"
                        UncheckedImageUrl="~/_inc/images/design/checkbox_off.gif" 
                        CheckedImageUrl="~/_inc/images/design/checkbox_on.gif"
                        DisabledCheckedImageUrl="~/_inc/images/design/checkbox_disabled.gif"
                        DisabledUncheckedImageUrl="~/_inc/images/design/checkbox_disabled.gif"    />
                </td>
            </tr>            
            <tr class="tr_validation">
                <td></td>
                <td>
                    <asp:CustomValidator ValidationGroup="regShort" ID="cvAccept" runat="server" Display="Dynamic" ErrorMessage="* Strinjati se morate s pogoji uporabe." OnServerValidate="ValidateAccept"></asp:CustomValidator>
                    
                </td>
            </tr>
        </table>
    <div style="padding:10px 0px 0px 0px;">
        <SM:LinkButtonDefault ID="btOrder" CssClass="BTN_quickregister BTN_quickregister_new" runat="server"   ValidationGroup="regShort"    onclick="btOrder_Click">NAROČI IZDELAVO<br /><span>naroči spletno stran</span></SM:LinkButtonDefault>  
        <SM:LinkButtonDefault ID="btRegister" CssClass="BTN_quickregister BTN_quickregister_new BTN_quickregister_fliped" runat="server"   ValidationGroup="regShort"    onclick="btRegister_Click">IZDELAJ SAM<br /><span>preizkusi brezplačno</span></SM:LinkButtonDefault>
    
    </div>
    <div class="clearing"></div>
</asp:Panel>



<EM:MailConfirmation ID="objMailConfirmation" runat="server" Visible="false" />