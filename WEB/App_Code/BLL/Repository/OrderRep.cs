﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.IO;
using System.Net.Mail;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography;
using System.Globalization;

/// <summary>
/// Summary description for OrderRep
/// </summary>
public class OrderRep
{

    private eMenikDataContext  db = new eMenikDataContext ();

   
    // Query Methods




    public SHOPPING_ORDER GetOrderByID(Guid id)
    {
        return db.SHOPPING_ORDERs.SingleOrDefault(w => w.OrderID == id);
    }

    public SHOPPING_ORDER GetOrderByRefID(int id)
    {
        return db.SHOPPING_ORDERs.SingleOrDefault(w => w.OrderRefID == id);
    }

    public SHOPPING_ORDER EnsureOpenOrderByUser(Guid userid, Guid pageID)
    {
        // get existing order
        SHOPPING_ORDER ordr = GetOpenOrderByUser(userid, pageID  );
        // add new order
        if (ordr == null) {
            ordr = new SHOPPING_ORDER ();
            ordr.UserId = userid;
            ordr.PageId = pageID;
            ordr.OrderStatus = SHOPPING_ORDER.Common.Status.STEP_2;

            AddOrder(ordr);
            Save();
        }

        return ordr;
    }

    public SHOPPING_ORDER GetOpenOrderByUser(Guid userid, Guid pageID)
    {
        return db.SHOPPING_ORDERs.Where(w => w.UserId == userid && w.PageId == pageID  && w.OrderStatus > SHOPPING_ORDER.Common.Status.CANCELED && w.OrderStatus < SHOPPING_ORDER.Common.Status.STEP_CONFIRMED).OrderByDescending(w => w.DateStart).FirstOrDefault();
    }



    public IQueryable <Data.ShippingAddress> GetShippingAddressesByUser(Guid userID) {

        var list = from s in db.SHOPPING_ORDERs
                   where s.OrderStatus >= SHOPPING_ORDER.Common.Status.CONFIRMED  && s.UserId == userID  && s.ShippingType == SHOPPING_ORDER.Common.ShippingType.SHIPPING && s.UseDifferentShippingAddress == true
                   orderby s.DateStart descending 
                   group s by new { s.ShippingFirstName, s.ShippingAddress, s.ShippingCity, s.ShippingPhone  } into sg
                   select new Data.ShippingAddress { Title = sg.Key.ShippingFirstName, Address = sg.Key.ShippingAddress, City = sg.Key.ShippingCity, Phone = sg.Key.ShippingPhone   };
        return list;
    }

    public IQueryable<Data.ShippingAddress> GetShippingAddressesByUserEko(Guid userID)
    {

        var list = from s in db.SHOPPING_ORDERs
                   where s.OrderStatus >= SHOPPING_ORDER.Common.Status.CONFIRMED && s.UserId == userID && s.UseDifferentShippingAddress == false // && s.ShippingType == SHOPPING_ORDER.Common.ShippingType.SHIPPING 
                   orderby s.DateStart descending
                   group s by new { s.ShippingFirstName, s.ShippingAddress, s.ShippingCity, s.ShippingPhone, s.ShippingRegion } into sg
                   select new Data.ShippingAddress { Title = sg.Key.ShippingFirstName, Address = sg.Key.ShippingAddress, City = sg.Key.ShippingCity, Phone = sg.Key.ShippingPhone, Region = sg.Key.ShippingRegion ?? 0  };
        return list;
    }

    public IQueryable<Data.ShippingAddress> GetOrderUserAddressesByUser(Guid userID)
    {

        var list = from s in db.SHOPPING_ORDERs
                   where s.OrderStatus >= SHOPPING_ORDER.Common.Status.CONFIRMED                    &&
                   s.UserId == userID && s.OrderAddress != null
                   group s by new { s.OrderFirstName, s.OrderLastName,  s.OrderAddress, s.OrderCity, s.OrderPhone, s.OrderCompany, s.OrderTax   } into sg
                   select new Data.ShippingAddress { FirstName = sg.Key.OrderFirstName, LastName = sg.Key.OrderLastName, Address = sg.Key.OrderAddress, City = sg.Key.OrderCity, Phone = sg.Key.OrderPhone, Company = sg.Key.OrderCompany , TaxNo = sg.Key.OrderTax };
        return list;
    }

    public IQueryable<Data.CompanyDetails> GetCompanyDetailsByUser(Guid userID)
    {

        var list = from s in db.SHOPPING_ORDERs
                   from c in db.COMPANies 
                   where s.OrderStatus >= SHOPPING_ORDER.Common.Status.CONFIRMED && s.UserId == userID && s.ShippingType == SHOPPING_ORDER.Common.ShippingType.SHIPPING
                   && c.COMPANY_ID == s.COMPANY_ID
                   group c by new { c.NAME, c.TAX_NUMBER, c.ADDRESS.STREET, c.ADDRESS.CITY, c.ZAVEZANEC   } into sg
                   select new Data.CompanyDetails { Title = sg.Key.NAME , Address = sg.Key.STREET , City = sg.Key.CITY, Tax=sg.Key.TAX_NUMBER, Zavezanec = sg.Key.ZAVEZANEC   };
        return list;
    }


    public void AddOrder(SHOPPING_ORDER  ordr)
    {

        ordr.DateStart = DateTime.Now;
        if (ordr.OrderID  == Guid.Empty )
            ordr.OrderID = Guid.NewGuid();
        
        if (ordr.Payed == null)
            ordr.Payed = false;
        if (ordr.OrderStatus == null)
            ordr.OrderStatus = SHOPPING_ORDER.Common.Status.NEW ;
        if (ordr.PaymentType == null)
            ordr.PaymentType = null;
        if (ordr.ShippingType  == null)
            ordr.ShippingType = null;
        if (ordr.Total == null)
            ordr.Total = 0;
        if (ordr.Tax == null)
            ordr.Tax = 0;
        if (ordr.ShippingAddress == null)
            ordr.ShippingAddress = "";
        if (ordr.ShippingCity  == null)
            ordr.ShippingCity = "";
        if (ordr.ShippingCountry  == null)
            ordr.ShippingCountry = "";
        if (ordr.ShippingFirstName  == null)
            ordr.ShippingFirstName = "";
        if (ordr.ShippingLastName  == null)
            ordr.ShippingLastName = "";
        if (ordr.ShippingPostal  == null)
            ordr.ShippingPostal = "";
        if (ordr.ShippingPrice  == null)
            ordr.ShippingPrice = 0;


        db.SHOPPING_ORDERs.InsertOnSubmit(ordr );

    }


    //
    // Persistence

    public bool  Save()
    {
        try
        {
            db.SubmitChanges();
        }
        catch (Exception ex)
        {
            ERROR_LOG.LogError( ex, "Save order");

            return false;
        }
        return true;        
    }


    public List<RuleViolation> SubmitOrder(SHOPPING_ORDER order)
    { 
        // cehck status
        List<RuleViolation> errlist = new List<RuleViolation>();

        //if (order.OrderStatus != SHOPPING_ORDER.Common.Status.CONFIRMED  )
        //{
        //    return errlist ;
        //}
        
        // get order items (and reupdate them )
        ProductRep prep = new ProductRep();
        var items = prep.UpdateCartItemUser(SM.EM.BLL.Membership.GetCurrentUserID(), SM.EM.BLL.Membership.GetCurrentUserID(), order.PageId, order.OrderID );

        // if none exists, error
        if (items == null ||items.Count == 0)
        {
            errlist .Add( new RuleViolation("V košarici ni izdelkov", "CART_ITEMs"));
            return errlist;
        }


        decimal total = items.Sum(w => w.Price);
        decimal total_tax = items.Sum(w => w.Tax);

        // get shipping
        decimal shipping = order.CalculateShippingPrice(total); //SHOPPING_ORDER.GetShippingPriceByTotal(total);
        // todo : add shipping tax
        decimal shipping_tax = Convert.ToDecimal((shipping * 0.2m ) );



        order.Total = total + (shipping - shipping_tax);
        order.Tax = total_tax + shipping_tax;
        order.ShippingPrice = shipping;        
//        order.TotalWithTax  = items.Sum(w => w.PriceTotal );


        // error
        if (!order.IsValid)
        {
            errlist = order.GetRuleViolations().ToList();
            return errlist;
        }        
        // check if fake user - testing
        bool isFake = false;
        if (HttpContext.Current != null)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
                if (SM.BLL.Common.Emenik.Data.FakeUserName() == HttpContext.Current.User.Identity.Name)
                {
                    isFake = true;
                }
        }

        // get new order number
        order.OrderNumber = SHOPPING_ORDER.GetNexOrderNum();

        //save order
        if (this.Save())
        {
            // save items
            prep.Save();

            // update stock
            var products = prep.GetProductsByOrder(order.OrderID).Distinct();
            foreach (var item in products)
            {
                if (!isFake)
                {
                    var quantity = items.Where(p => p.ProductID == item.PRODUCT_ID).Sum(s => s.Quantity);
                    item.UNITS_IN_STOCK -= quantity;
                }
            }
            // save new stock
            prep.Save();

            // send mail to customer
            SendMailOrderConfirmation(order.PageId.Value, order.UserId, order, "", "");

            // send mail to shop admin
            if (!isFake && !HttpContext.Current.Request.IsLocal)
            {
                SendMailOrderConfirmation(order.PageId.Value, order.UserId, order, SM.EM.Mail.Account.Narocila.Email, "- novo naročilo v spletni trgovini", true);
            }

            if (order.PaymentType == SHOPPING_ORDER.Common.PaymentType.PREDRACUN)
            {
                // send predracun
                CustomerRep crep = new CustomerRep();
                SendMailUserOrderPredracun(EM_USER.GetUserByID(order.PageId.Value), crep.GetCustomerByID(order.UserId), order);
            }

        }
    
    

        return errlist;

    
    }

    public List<RuleViolation> SubmitOrderOnlinePaymentStart(SHOPPING_ORDER order)
    {
        // cehck status
        List<RuleViolation> errlist = new List<RuleViolation>();
        // tmp
        if (order.OrderStatus != SHOPPING_ORDER.Common.Status.STEP_4)
        {
            return errlist;
        }



        // error
        if (!order.IsValid)
        {
            errlist = order.GetRuleViolations().ToList();
            return errlist;
        }

        //save order
        //if (this.Save())
        {
            // get order items (and reupdate them )
            ProductRep prep = new ProductRep();
            // remove cache
            //SM.EM.Caching.Remove.CartItemList(SM.EM.BLL.Membership.GetCurrentUserID());
            var items = prep.GetCartItemList(SM.EM.BLL.Membership.GetCurrentUserID(), LANGUAGE.GetDefaultLang(), order.PageId, null);
            //var items = prep.UpdateCartItemUser(SM.EM.BLL.Membership.GetCurrentUserID(), SM.EM.BLL.Membership.GetCurrentUserID(), order.PageId, order.OrderID);

            // if none exists, error
            if (items == null || items.Count == 0)
            {
                errlist.Add(new RuleViolation("V košarici ni izdelkov", "CART_ITEMs"));
                return errlist;
            }
            order.Total = items.Sum(w => w.Price);
            //        order.TotalWithTax  = items.Sum(w => w.PriceTotal );
            order.Tax = items.Sum(w => w.Tax);
            // save items
            prep.Save();

            // get new order number
            order.OrderNumber = SHOPPING_ORDER.GetNexOrderNum();

            // save order
            this.Save();



            

            // create new TRANSACTION
            OnlineE24PipeRep olrep = new OnlineE24PipeRep();

            E24_TRAN trans = new E24_TRAN();

            trans.RefID = order.OrderID;
            trans.Result = OnlineE24Payment .Common.Status.NOT_SET;
            trans.DateInit = DateTime.Now;
            trans.Store = OnlineE24Payment.Store();

            //data
            trans.CustName = order.OrderFirstName ;
            trans.CustSurname = order.OrderLastName ;
            trans.PaymentGateway = "/";
            trans.UpdateURL = SM.BLL.Common.ResolveUrlFull(OnlineE24Payment .UpdateURL());
            trans.StatusURL = OnlineE24Payment.Common.RenderStatusHref(order.OrderID);
            trans.ErrorURL = OnlineE24Payment.Common.RenderErrorHref(order.OrderID);
            trans.TestMode = OnlineE24Payment.TestMode();
            trans.TxType = OnlineE24Payment.Action();
            trans.TxId = "";

            // e24
            trans.Currency = "978"; // Euro
            trans.Amount = order.TotalWithTax.Value;

            // id
            trans.TrackID = order.OrderRefID.ToString();

            //trans.Amount = 1;//tmp
            //trans.Currency = "EUR";


            // save, get ID
            olrep.AddTrans(trans);
            olrep.Save();





            // INIT online payment
            //string trackID = "..."; // Genera un track ID
            e24PaymentPipeLib.e24PaymentPipeCtlClass pipe = new e24PaymentPipeLib.e24PaymentPipeCtlClass();
            pipe.Action = trans.TxType ;
            pipe.Currency = trans.Currency ; // Euro
            pipe.Amt = String.Format(CultureInfo.GetCultureInfo("en-US").NumberFormat, "{0:f2}", trans.Amount);
            pipe.Language = "SLO";
            pipe.ResponseUrl = trans.UpdateURL;  //String.Format("http://{0}/TransactionNotification.aspx?result=Success", Request.Url.Authority);
            pipe.ErrorUrl = trans.ErrorURL; //String.Format("http://{0}/TransactionNotification.aspx?result=Cancel", Request.Url.Authority);

            // store
            pipe.WebAddress = OnlineE24Payment.WebAddress();// "test4.constriv.com";
            pipe.PortStr = OnlineE24Payment.Port();// "443";
            pipe.Context = OnlineE24Payment.Context(); //"/cg"; //"/cg301";
            pipe.ID = OnlineE24Payment.ID();// "89025555";
            pipe.Password = OnlineE24Payment.Pass();// "test";

            pipe.TrackId = trans.TrackID;
            pipe.Udf1 = trans.TransID.ToString();
            pipe.Udf2 = trans.RefID.ToString();



            //pipe.Alias = "..."; // Indicare l'alias
            //pipe.ResourcePath = "..."; // Indicare l'ubicazione del file "resource.cgn"
            
            Int16 result = pipe.PerformInitTransaction();
            var RawResponse = pipe.RawResponse;
            var ErrorMsg = pipe.ErrorMsg;

            if (result != 0)
            {
                // Errore...
                // Esegui il logging
                //trans.Status = OnlineE24Payment.Common.Status.FAILED;
                trans.Result = OnlineE24Payment.Common.Status.FAILED;
                trans.DateInit = DateTime.Now;
                trans.PaymentID = "";
                trans.RetSuccess = false;
                trans.RetCode = result.ToString();
                trans.RetMsg = ErrorMsg;
                trans.Result = result.ToString();
                trans.PaymentGateway = RawResponse;

                // save
                olrep.Save();

   
                // add error
                errlist.Add(new RuleViolation("Napaka pri vzpostavljanju povezave s plačilnim centrom", "ONLINE_PAYMENT"));
                return errlist;
            }

            String paymentID = pipe.PaymentId;
            String paymentUrl = pipe.PaymentPage;

            if (String.IsNullOrEmpty(paymentID) || String.IsNullOrEmpty(paymentUrl))
            {
                // Errore...
                //trans.Status = OnlineE24Payment.Common.Status.FAILED;
                trans.Result = OnlineE24Payment.Common.Status.FAILED;

                trans.DateInit = DateTime.Now;
                trans.PaymentID = paymentID;
                trans.RetSuccess = false;
                trans.RetCode = result.ToString();
                trans.RetMsg = ErrorMsg;
                trans.Result = result.ToString();
                trans.PaymentGateway = RawResponse;

                // save
                olrep.Save();

                // add error
                errlist.Add(new RuleViolation("Napaka pri vzpostavljanju povezave s plačilnim centrom", "ONLINE_PAYMENT"));
                return errlist;
            }

            // Memorizza ora da quale parte la relazione tra l'id
            // dell'ordine e l'id del pagamento.
            // Ad esempio
            //TransactionTrackerService.Store(paymentID, order.ID);
            //Response.Redirect(String.Format("{0}?PaymentID={1}", paymentUrl, paymentID));

            //update trans
            //trans.Status  = OnlineE24Payment.Common.Status.INITIALIZED;
            trans.Result = OnlineE24Payment.Common.Status.INITIALIZED;
            trans.DateInit = DateTime.Now;
            trans.PaymentID = paymentID;
            trans.PaymentGateway = RawResponse;

            //trans.StateResult = txStateData.result;
            //trans.StateResultType = txStateData.resultType;
            // save
            olrep.Save();

            // update order trans
            order.OrderStatus = SHOPPING_ORDER.Common.Status.ONLINE_PAYMENT_IN_PROGRESS;
            order.TransIDe24 = trans.TransID;
            this.Save();

            // update cart items
            var cart_items = prep.UpdateCartItemUser(SM.EM.BLL.Membership.GetCurrentUserID(), SM.EM.BLL.Membership.GetCurrentUserID(), order.PageId, order.OrderID);
            
            // redirect to PAYMENT
            HttpContext.Current.Response.Redirect(String.Format("{0}?PaymentID={1}", paymentUrl, paymentID));
            

           
        }



        return errlist;


    }


    public List<RuleViolation> SubmitOrderOnlinePaymentStartMP(SHOPPING_ORDER order)
    {
        // cehck status
        List<RuleViolation> errlist = new List<RuleViolation>();
        // tmp
        if (order.OrderStatus != SHOPPING_ORDER.Common.Status.STEP_4)
        {
            return errlist;
        }




        // error
        if (!order.IsValid)
        {
            errlist = order.GetRuleViolations().ToList();
            return errlist;
        }

        //save order
        //if (this.Save())
        {


            // get order items (and reupdate them )
            ProductRep prep = new ProductRep();
            // remove cache
            //SM.EM.Caching.Remove.CartItemList(SM.EM.BLL.Membership.GetCurrentUserID());
            var items = prep.GetCartItemList(SM.EM.BLL.Membership.GetCurrentUserID(), LANGUAGE.GetDefaultLang(), order.PageId, null);
            //var items = prep.UpdateCartItemUser(SM.EM.BLL.Membership.GetCurrentUserID(), SM.EM.BLL.Membership.GetCurrentUserID(), order.PageId, order.OrderID);

            // if none exists, error
            if (items == null || items.Count == 0)
            {
                errlist.Add(new RuleViolation("V košarici ni izdelkov", "CART_ITEMs"));
                return errlist;
            }
            order.Total = items.Sum(w => w.Price);
            //        order.TotalWithTax  = items.Sum(w => w.PriceTotal );
            order.Tax = items.Sum(w => w.Tax);
            // save items
            prep.Save();

            // get new order number
            order.OrderNumber = SHOPPING_ORDER.GetNexOrderNum();

            // save order
            this.Save();


            // start online payment
            si.megapos.service.MegaposProcessorService service = new si.megapos.service.MegaposProcessorService();

            X509Certificate cert = X509Certificate.CreateFromCertFile(MegaPos.CertificatePath());
            service.ClientCertificates.Add(cert);

            // create new transaction
            MegaPosRep mpr = new MegaPosRep();

            MP_TRAN trans = new MP_TRAN();

            trans.RefID = order.OrderID;
            trans.Status = MegaPos.Common.Status.NOT_SET;
            trans.DateInit = DateTime.Now;
            trans.Store = MegaPos.Store();

            //data
            trans.CustName = order.ShippingFirstName;
            trans.CustSurname = order.ShippingLastName;
            trans.PaymentGateway = "BANKART_PGW";
            trans.UpdateURL = SM.BLL.Common.ResolveUrlFull(MegaPos.UpdateURL());
            trans.StatusURL = MegaPos.Common.RenderStatusHref(order.OrderID);
            trans.TestMode = MegaPos.TestMode();
            trans.TxType = "ORDER";
            trans.TxId = "";


            trans.Amount = order.TotalWithTax.Value;

            //trans.Amount = 1;//tmp
            trans.Currency = "EUR";


            // save, get ID
            mpr.AddTrans(trans);
            mpr.Save();

            // set additional data (after ID is set)
            //trans.UpdateURL = MegaPos.Common.RenderUpdateHref(trans.TransID);
            //mpr.Save();



            // data
            si.megapos.service.TxInitData data = new si.megapos.service.TxInitData();
            data.addInfo = "test_tomaz";
            data.custName = trans.CustName;
            data.custSurname = trans.CustSurname;
            data.paymentGateway = trans.PaymentGateway;
            data.statusURL = trans.StatusURL;
            data.testMode = trans.TestMode;
            data.txType = trans.TxType;
            data.updateURL = trans.UpdateURL;

            // pdata
            si.megapos.service.TxProcessData pdata = new si.megapos.service.TxProcessData();
            pdata.amount = trans.Amount;
            pdata.currency = trans.Currency;








            // INIT
            si.megapos.service.TxOperationResult initTxResult = service.initTx(MegaPos.Store(), MegaPos.GetTxId(trans.TransID), data, pdata);
            /* ce se na tem koraku zgodi NullPointerException - ce je tak odgovor od Megapos20, je mogoce treba popravit :
             * - data.testMode = true; 
             * - data.paymentGateway = "EFUNDS";  ne EFUNDS_PGW */

            String opResult = "<br>";

            if (initTxResult.success == true)
            {

                si.megapos.service.Tx tx = initTxResult.tx;
                si.megapos.service.TxInitData txInitData = tx.initData;
                si.megapos.service.TxStateData txStateData = tx.stateData;
                si.megapos.service.TxProcessData txProcessData = txStateData.processData;

                //update trans
                trans.Status = MegaPos.Common.Status.INITIALIZING;
                trans.DateInit = DateTime.Now;
                trans.TxId = tx.txId;
                trans.StateResult = txStateData.result;
                trans.StateResultType = txStateData.resultType;
                // save
                mpr.Save();


                // update order trans
                order.OrderStatus = SHOPPING_ORDER.Common.Status.ONLINE_PAYMENT_IN_PROGRESS;
                order.TransID = trans.TransID;
                this.Save();

                // update cart items
                var cart_items = prep.UpdateCartItemUser(SM.EM.BLL.Membership.GetCurrentUserID(), SM.EM.BLL.Membership.GetCurrentUserID(), order.PageId, order.OrderID);




                // redirect to PAYMENT
                HttpContext.Current.Response.Redirect(trans.StateResult);


                //opResult += "<br><b>RESPONSE data:</b><br> code: " + initTxResult.code + "<br> msg: " + initTxResult.msg + "<br> success: " + initTxResult.success + "<br> txId: " + initTxResult.txId;
                //opResult += "<br><br> store: " + tx.store;
                //opResult += "<br><br><b>INIT data:</b><br> addInfo: " + txInitData.addInfo + "<br> custName: " + txInitData.custName
                //            + "<br> custSurname: " + txInitData.custSurname + "<br> language: " + txInitData.language
                //            + "<br> paymentGateway: " + txInitData.paymentGateway + "<br> statusURL: " + txInitData.statusURL
                //            + "<br> testMode: " + txInitData.testMode + "<br> txType: " + txInitData.txType
                //            + "<br> updateURL: " + txInitData.updateURL;
                //opResult += "<br><br><b>STATE data:</b><br> amount: " + txProcessData.amount + "<br> currency: " + txProcessData.currency;
                //opResult += "<br><br> result: " + txStateData.result + "<br> resultType: " + txStateData.resultType
                //            + "<br> type: " + txStateData.type + "<br> timestamp: " + txStateData.timestamp.ToString();
            }
            else
            {
                //opResult += "<br> code: " + initTxResult.code + "<br> msg: " + initTxResult.msg + "<br> success: " + initTxResult.success + "<br> txId: " + initTxResult.txId;
                //update trans
                trans.Status = MegaPos.Common.Status.FAILED;
                trans.DateInit = DateTime.Now;
                trans.TxId = initTxResult.txId;
                trans.RetSuccess = false;
                trans.RetCode = initTxResult.code.ToString();
                trans.RetMsg = initTxResult.msg;
                // save
                mpr.Save();

                // cahnge order status
                //order.


                // add error
                errlist.Add(new RuleViolation("Napaka pri vzpostavljanju povezave s plačilnim centrom", "ONLINE_PAYMENT"));
                return errlist;
            }




        }



        return errlist;


    }




    public void SubmitOrderOnlinePaymentEnd(SHOPPING_ORDER order)
    {
        ProductRep prep = new ProductRep();

        //save order
        if (this.Save())
        {
            // save items
            //prep.Save();

            // get items
            var items = prep. GetCartItemList(order.UserId , LANGUAGE.GetDefaultLang(), order.PageId , order.OrderID );

            // update stock
            var products = prep.GetProductsByOrder(order.OrderID).Distinct();
            foreach (var item in products)
            {
                var quantity = items.Where(p => p.ProductID == item.PRODUCT_ID).Sum(s => s.Quantity);
                item.UNITS_IN_STOCK -= quantity;
            }
            // save new stock
            prep.Save();

            // send mail to customer
            SendMailOrderConfirmation(order.PageId.Value, order.UserId, order, "", "");

            // send mail to shop admin
            bool isFake = false;
            if (HttpContext.Current != null)
            {
                if (HttpContext.Current.User.Identity.IsAuthenticated)
                    if (SM.BLL.Common.Emenik.Data.FakeUserName() == HttpContext.Current.User.Identity.Name)
                    {
                        isFake = true;
                    }
            }
            if (!isFake && !HttpContext.Current.Request.IsLocal)
                SendMailOrderConfirmation(order.PageId.Value, order.UserId, order, SM.EM.Mail.Account.Narocila.Email, "- novo naročilo v spletni trgovini - ONLINE PLAČILO", true);
            


        }
    }
    


    public void SendMailOrderConfirmation(Guid pageid, Guid userid, SHOPPING_ORDER order, string forceEmail, string forceTitle, bool isAdmin= false) { 
    {

        object data = order;
        EM_USER page = EM_USER.GetUserByID(pageid);
        string pageHref = SM.EM.Rewrite.EmenikUserUrl(page);

        object parameters = new object[] {pageHref, page };
        string body = ViewManager.RenderView("~/_mailTemplates/order/_ctrlUserOrderConfirmed.ascx", data, parameters );
        CustomerRep crep = new CustomerRep();
        CUSTOMER usr = crep.GetCustomerByID(userid);

        string email = "";
        if (usr == null)
            email = page.EMAIL;
        else
            email = usr.USERNAME;


        // send mail
//        MailMessage message = SM.EM.Mail.Template.GetNewUserConfirmation(password, usr);
        MailMessage message = new MailMessage();
        message.Sender = new MailAddress(SM.EM.Mail.Account.Info.Email, page.PAGE_TITLE);
        message.From = new MailAddress(SM.EM.Mail.Account.Info.Email, page.PAGE_TITLE );
        message.To.Clear();

        // force 
        if (!string.IsNullOrEmpty(forceEmail.Trim()))
        {
            email = forceEmail;
            //message.CC.Add(new MailAddress("narocila.terra-gourmet@hotmail.com"));
            message.CC.Add(new MailAddress("narocila.terragourmet@gmail.com"));
        }
        message.To.Add(new MailAddress(email));

        // append data
        if (isAdmin ){
            body = body + "<br/><br/><br/><br/>";
            body += string.Format("<a href='{0}'>XML</a>", SM.BLL.Common.ResolveUrlFull(SHOPPING_ORDER.Common.Export.GetExportOrderXmlHref(order.OrderID )));
        }


        message.IsBodyHtml = true;
        message.Body = body ;
        message.Subject = page.PAGE_TITLE  + " - vaše naročilo v spletni trgovini";


        if (!string.IsNullOrEmpty(forceTitle.Trim()))
            message.Subject = page.PAGE_TITLE + forceTitle;

        // append users name
        if (isAdmin) {
            message.Subject += " (" + usr.FIRST_NAME + " " + usr.LAST_NAME + ")"; 
        }

        SmtpClient client = new SmtpClient();
        try
        {

            // if local, use gmail, else use server SMTP
           // if (HttpContext.Current != null && HttpContext.Current.Request.IsLocal)
            {
                client.Credentials = new System.Net.NetworkCredential(SM.EM.Mail.Account.Info.Email, SM.EM.Mail.Account.Info.Pass);
                client.Port = 587;//or use 587            
                client.Host = "smtp.gmail.com";
                client.EnableSsl = true;
            }
            //else
            //{
            //    //smtpClient.DeliveryMethod = SmtpDeliveryMethod.PickupDirectoryFromIis;

            //    // network delivery for propely setting SENDER header in mail
            //    client.DeliveryMethod = SmtpDeliveryMethod.Network;
            //    client.Host = "localhost";
            //}



            client.Send(message);
        }

        catch (Exception ex)
        {
            ERROR_LOG.LogError(ex, "SendMailOrderConfirmation");
        }


    }
    
    
    }


    public void SendMailUserOrderCompleted(EM_USER page, CUSTOMER cus, SHOPPING_ORDER order)
    {
        {

            object data = order;
            object parameters = new object[] { page };
            string body = ViewManager.RenderView("~/_mailTemplates/order/_ctrlOrderCompletedUserMail.ascx", data, parameters);
            CustomerRep crep = new CustomerRep();

            string email = "";
            if (cus == null)
                return;

            email = cus.USERNAME;

            // send mail
            //        MailMessage message = SM.EM.Mail.Template.GetNewUserConfirmation(password, usr);
            MailMessage message = new MailMessage();
            message.Sender = new MailAddress(SM.EM.Mail.Account.Info.Email, page.PAGE_TITLE);
            message.From = new MailAddress(SM.EM.Mail.Account.Info.Email, page.PAGE_TITLE);
            message.To.Clear();
            message.To.Add(new MailAddress(email));
            message.IsBodyHtml = true;
            message.Body = body;
            message.Subject = page.PAGE_TITLE + " - naročilo v spletni trgovini je " + SHOPPING_ORDER.Common.RenderOrderStatus(order.OrderStatus);


            SmtpClient client = new SmtpClient();
            try
            {

                // if local, use gmail, else use server SMTP
               // if (HttpContext.Current != null && HttpContext.Current.Request.IsLocal)
                {
                    client.Credentials = new System.Net.NetworkCredential(SM.EM.Mail.Account.Info.Email, SM.EM.Mail.Account.Info.Pass);
                    client.Port = 587;//or use 587            
                    client.Host = "smtp.gmail.com";
                    client.EnableSsl = true;
                }
                //else
                //{
                //    //smtpClient.DeliveryMethod = SmtpDeliveryMethod.PickupDirectoryFromIis;

                //    // network delivery for propely setting SENDER header in mail
                //    client.DeliveryMethod = SmtpDeliveryMethod.Network;
                //    client.Host = "1dva3.si";
                //}


                client.Send(message);
            }

            catch (Exception ex)
            {
                ERROR_LOG.LogError(ex, " napaka pri pošiljanju sporočila - SendMailUserOrderCompleted");
            }


        }


    }

    public void SendMailUserOrderPredracun(EM_USER page, CUSTOMER cus, SHOPPING_ORDER order)
    {

            object data = order;
            object parameters = new object[] { page };
            string body = ViewManager.RenderView("~/_mailTemplates/order/_cntPredracunDetailMail.ascx", data, parameters);
            CustomerRep crep = new CustomerRep();

            string email = "";
            if (cus == null)
                return;

            email = cus.USERNAME;

            // send mail
            //        MailMessage message = SM.EM.Mail.Template.GetNewUserConfirmation(password, usr);
            MailMessage message = new MailMessage();
            message.Sender = new MailAddress(SM.EM.Mail.Account.Info.Email, page.PAGE_TITLE);
            message.From = new MailAddress(SM.EM.Mail.Account.Info.Email, page.PAGE_TITLE);
            message.To.Clear();
            message.To.Add(new MailAddress(email));
            message.IsBodyHtml = true;
            message.Body = body;
            message.Subject = page.PAGE_TITLE + " - Predračun za naročilo št. " + SHOPPING_ORDER.Common.RenderShoppingOrderNumber(order);


            SmtpClient client = new SmtpClient();
            try
            {
                // if local, use gmail, else use server SMTP
                //if (HttpContext.Current != null && HttpContext.Current.Request.IsLocal)
                {
                    client.Credentials = new System.Net.NetworkCredential(SM.EM.Mail.Account.Info.Email, SM.EM.Mail.Account.Info.Pass);
                    client.Port = 587;//or use 587            
                    client.Host = "smtp.gmail.com";
                    client.EnableSsl = true;
                }
                //else
                //{
                //    //smtpClient.DeliveryMethod = SmtpDeliveryMethod.PickupDirectoryFromIis;

                //    // network delivery for propely setting SENDER header in mail
                //    client.DeliveryMethod = SmtpDeliveryMethod.Network;
                //    client.Host = "1dva3.si";
                //}



                client.Send(message);
            }

            catch (Exception ex)
            {
                ERROR_LOG.LogError(ex, " napaka pri pošiljanju sporočila - SendMailUserOrderPredracun");
            }

        
    }


    public class Data {
        public class ShippingAddress {
            public string Title { get; set; }
            public string Address { get; set; }
            public string City { get; set; }
            public string Phone { get; set; }
            public string Company { get; set; }
            public string TaxNo { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public int Region { get; set; }
        
        }

        public class CompanyDetails
        {
            public string Title { get; set; }
            public string Address { get; set; }
            public string City { get; set; }
            public string Tax { get; set; }
            public bool Zavezanec { get; set; }

        }
    
    
    
    }




}