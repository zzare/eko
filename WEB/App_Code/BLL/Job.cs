﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

/// <summary>
/// Summary description for JOB
/// </summary>
public partial class JOB
{

    public static JOB_LOG GetJobsLogOpenByJob(Guid jobID)
    {
        eMenikDataContext db = new eMenikDataContext();
        JOB_LOG job = (JOB_LOG)(from j in db.JOBs from r in db.JOB_LOGs where j.JobID == r.JobID && j.Active == true && r.Finished == false && (j.DateEnd == null || j.DateEnd > DateTime.Now) orderby r.DateStart select r).FirstOrDefault();
        return job;
    }
    public static JOB_LOG GetLastJobsLogByJob(Guid jobID)
    {
        eMenikDataContext db = new eMenikDataContext();
        JOB_LOG job = (JOB_LOG)(from j in db.JOBs from r in db.JOB_LOGs where j.JobID == r.JobID && j.Active == true && (j.DateEnd == null || j.DateEnd > DateTime.Now) orderby r.DateStart select r).FirstOrDefault();
        return job;
    }
    public static List<JOB_LOG> GetJobsToExecute(DateTime date)
    {
        eMenikDataContext db = new eMenikDataContext();
        // open tasks
        //List<JOB> rec = (from r in db.JOBs where r.Active == true && (r.DateEnd == null || r.DateEnd > date) && r.DateNext < date select r).ToList();
        List<JOB_LOG> ret = (from j in db.JOBs from r in db.JOB_LOGs where j.JobID == r.JobID && j.Active == true && r.Finished == false && (j.DateEnd == null || j.DateEnd > date) && r.DateStart < date select r).ToList();

        return ret;
    }

    public static List<JOB_LOG> GetJobsToExecute(DateTime date, Guid  jobID)
    {
        eMenikDataContext db = new eMenikDataContext();
        // open tasks
        //List<JOB> rec = (from r in db.JOBs where r.Active == true && (r.DateEnd == null || r.DateEnd > date) && r.DateNext < date select r).ToList();
        List<JOB_LOG> ret = (from j in db.JOBs from r in db.JOB_LOGs where j.JobID == jobID && j.JobID == r.JobID && j.Active == true && r.Finished == false && (j.DateEnd == null || j.DateEnd > date) && r.DateStart < date select r).ToList();

        return ret;
    }

    public static void CreateNewJobLogIfStopped(Guid jobID) {
        if (GetJobsLogOpenByJob(jobID) != null)
            return;

        eMenikDataContext db = new eMenikDataContext();
        
        JOB_LOG log = GetLastJobsLogByJob(jobID);
        JOB_LOG newLog = new JOB_LOG();
        newLog.JobID = jobID;
        newLog.DateStart = log.DateStart.AddMinutes(60);
        newLog.Notes = "new created ";
        newLog.Finished = false;

        db.JOB_LOGs.InsertOnSubmit(newLog);
        db.SubmitChanges();
    }

    public static void UpdateJobExecuted(int jobLogID) {
        eMenikDataContext db = new eMenikDataContext();
    
        // old job
        JOB_LOG old = (from j in db.JOB_LOGs where j.JobLogID == jobLogID select j).SingleOrDefault();
        old.Finished = true;
        old.DateExecution = DateTime.Now;

        // calculate next date
        JOB job = old.JOB; // (from j in db.JOBs where j.JobID == old.JobID  select j).SingleOrDefault();
        if (job == null) return;

        DateTime nextExec = DateTime.MinValue; ;

        switch (job.PeriodDatePart.ToLower() )
        {
            case "d": // daily
                nextExec = DateTime.Now.AddDays(job.PeriodNumber);
                break;
            case "h": // hourly
                nextExec = DateTime.Now.AddHours(job.PeriodNumber);
                break;
            case "min": // minutely
                nextExec = DateTime.Now.AddMinutes(job.PeriodNumber);
                break;

        }
        // if job is overdue, deactivate it
        job.DateNext = nextExec;
        if (job.DateEnd != null)
            if (nextExec > job.DateEnd)
                job.Active = false;

        db.SubmitChanges();

        List<JOB_LOG > newList = GetJobsToExecute(DateTime.Now, old.JobID);
        // if next job allready exists, return
        if (newList != null && newList.Count > 0) return;

        // insert new job
        JOB_LOG log = new JOB_LOG { JobID = job.JobID, DateStart = nextExec , Finished = false };
        db.JOB_LOGs.InsertOnSubmit(log);
        db.SubmitChanges();

    
    
    }
}
