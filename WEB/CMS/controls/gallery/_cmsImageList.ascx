﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_cmsImageList.ascx.cs" Inherits="SM.UI.Sandbox.Controls._cmsImageList" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Namespace="SM.UI.Controls" TagPrefix="SM" %>


<%--<SM:InlineScript runat="server">
    <script type="text/javascript" language="javascript">
    function SetUniqueCheckbox(nameregex, current)
    {
       re = new RegExp(nameregex);
       for(i = 0; i < document.forms[0].elements.length; i++){
          elm = document.forms[0].elements[i];
          
          if (elm.type == 'checkbox')
          {
             if (re.test(elm.name))
             {
                elm.checked = false;
             }
          }
       }
       current.checked = true;
       
    Sys.Application.notifyScriptLoaded();   
    }
    </script>
</SM:InlineScript>--%>


<div class="imgList">

        <asp:LinqDataSource ID="ldsList" runat="server"  
            ContextTypeName="SandBoxDataContext"   
            TableName="vw_ImageLang" 
            OnSelecting="ldsList_OnSelecting" >
        </asp:LinqDataSource>
        
        <SM:smListView ID="lvList" runat="server"  DataSourceID="ldsList" 
            DataKeyNames="IMG_ID" InsertItemPosition="None" 
            onitemcommand="lvList_ItemCommand" onitemdatabound="lvList_ItemDataBound" >
            <LayoutTemplate>
                <asp:LinkButton ID="LinkButton2" CommandName="sort" CommandArgument="DATE_ADDED" runat="server">Date Added</asp:LinkButton>
                <asp:LinkButton ID="LinkButton1" CommandName="sort" CommandArgument="IMG_AUTHOR" runat="server">Author</asp:LinkButton>
                
                <div class="imgListCont" >
                    <div id="itemPlaceholder" runat="server"></div>
                    <br />
                    <br />
                    <br />
                    <br />
                    
                </div>
                
                <asp:DataPager runat="server" ID="dpPager" PageSize="10"  >
                    <Fields>
                        <asp:NextPreviousPagerField  />
                        <asp:NumericPagerField ButtonCount="5" />
                    </Fields>
                </asp:DataPager>
            </LayoutTemplate>
            
            <ItemTemplate>
                <div id="divImgThumbItem" runat="server" class="imgThumbItem" >
                    
                    <asp:LinkButton style="display:block;" ID="btEdit" CommandName="edit" CommandArgument='<%# Eval("IMG_ID") %>' runat="server"><img id="Img1" runat="server" src='<%# Eval("IMG_URL") %>'  /></asp:LinkButton>
                    
                    <div runat="server" visible ='<%# ShowCheckboxColumn %>'>
                        <asp:CheckBox ID="cbSelect" runat="server" />
                        <asp:HiddenField ID="hfID" runat="server" Value='<%# Eval("IMG_ID") %>' />
                    </div>
                </div>
            
            </ItemTemplate>
            
            <EmptyDataTemplate>
                
                <div class="odd">
                    No images exist.
            
                </div>
                
            </EmptyDataTemplate>
            
        
        </SM:smListView>

</div>
