﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_menuSort.ascx.cs" Inherits="SM.UI.CMS.Controls._menuSort" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Button ID="btSaveSort" runat="server" Text="Save new order" 
    onclick="btSaveSort_Click" />
<div class="sortCont" >

    <cc1:ReorderList ID="rlSiteMap"   runat="server" AllowReorder="True" 
                     PostBackOnReorder="false" EnableViewState="true"
                     DragHandleAlignment="Left" SortOrderField="Order" 
                     DataKeyField="SMAP_ID" ItemInsertLocation="Beginning" 
                     onitemreorder="rlSiteMap_ItemReorder"  CallbackCssStyle="sortCal"  >
                             
        <ItemTemplate >
            <div class="sortItem" >
                <%# Eval("SML_TITLE") %> 
            </div>            
        </ItemTemplate>
        
        <EditItemTemplate>
            <asp:TextBox ID="TextBox1" runat="server" Text = '<%# Container.DataItemIndex %>'></asp:TextBox>
        </EditItemTemplate>
        
        <ReorderTemplate>
            <asp:Panel ID="panReorder" runat="server" CssClass="sortDrag" >
                <img  src='<%=Page.ResolveUrl("~/_res/images/icon/ff.png") %>'/> &nbsp; spusti tukaj
            </asp:Panel>
        
        </ReorderTemplate>
        <DragHandleTemplate>
            <div class="sortDragHandle"></div> 
        </DragHandleTemplate>
    </cc1:ReorderList>
    

</div>