﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _ctrl_login_viewLoggedOut : BaseControlView
{

    public object Data;
    public object[] Parameters;
    protected Guid PageId;


    protected void Page_Load(object sender, EventArgs e)
    {
        if (PageId == Guid.Empty )
            LoadData();
    }



    public void LoadData()
    {
        this.Visible = false;
        if (Data == null)
        {
            return;
        }
        PageId  = (Guid )Data;
        if (PageId == null || PageId == Guid.Empty)
        {
            return;
        }

        // load data
        this.Visible = true;


    }






}
