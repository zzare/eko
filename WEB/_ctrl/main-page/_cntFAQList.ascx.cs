﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _ctrl_main_page_cntFAQList : BaseControl 
{
    private string ArticleDetailsPath = "~/c/ArticleDetails.aspx";
    public int SitemapID = 34;
    private int _pagesize = 5;
    public int PageSize { get { if (_pagesize < 1) return 3; return _pagesize; } set { _pagesize = value; } }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            LoadData();
    }


    protected void LoadData() {
        lvList.DataSource = ARTICLE.GetArticlesPages(SitemapID, PageSize );
        lvList.DataBind();    
    
    }

    protected string RenderArtHref(object item)
    {
        ARTICLE art = (ARTICLE)item;
            return Page.ResolveUrl(ArticleDetailsPath + "?art=" + art.ART_ID.ToString() + "&smp=" + art.SMAP_ID.ToString());

    }


}
