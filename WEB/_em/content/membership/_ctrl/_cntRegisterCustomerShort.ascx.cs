﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Net.Mail;
using System.Text;
using System.IO;

public partial class _ctrl_content_membership_ctrl_cntRegisterCustomerShort : BaseControl_EMENIK 
{
    public bool AutoLogin = true ;
    public bool SendMail = true;

    public string UsrEmail
    {
        get
        {
            return Email.Text.Trim();
    
    
    
    } }

    public event  EventHandler OnRegistrationComplete;

    protected void Page_Load(object sender, EventArgs e)
    {

        // tmp
        //Membership.DeleteUser("zzzare@gmail.com");
        //Membership.DeleteUser("zare_man@hotmail.com");

        //Membership.DeleteUser("zare@smardt.si");



        if (!IsPostBack) { 
            // disable clientside validation if autenticated
            if (Page.User.Identity.IsAuthenticated)
            {
                valEmailPattern.EnableClientScript = false;
                valRequireEmail.EnableClientScript = false;
                valRequirePassword.EnableClientScript = false;
//                valRequireUserName.EnableClientScript = false;
//                revUsername.EnableClientScript = false;
                valPasswordLength.EnableClientScript = false;
            
            
            }
        
        
        }
        
    }



    protected bool RegisterUser() {


        // validate input
        Page.Validate("regShort");
        if (!Page.IsValid)
            return false;

        // insert membership user
        MembershipCreateStatus status;

        MembershipUser usr = Membership.CreateUser(UsrEmail, Password.Text, UsrEmail, "q", "a", true, out status);

        // user is successfully created
        if (status == MembershipCreateStatus.Success)
        {
            // check for bonus
            Guid? bonus = null;
            //if (this.Profile.Emenik.BonusID != null && this.Profile.Emenik.BonusID != Guid.Empty)
            //{
            //    bonus = this.Profile.Emenik.BonusID;
            //    //this.Profile.Save();
            //}


            // insert customer
            CustomerRep crep = new CustomerRep();
            CUSTOMER c = new CUSTOMER { };
            c.CustomerID = new Guid( usr.ProviderUserKey.ToString());
            c.USERNAME = UsrEmail;
            c.COMPANY_ID = null;
            c.PageID = ParentPage.em_user.UserId;
            c.NEWSLETTER_ENABLED = true;
            c.CUSTOMER_TYPE = 1;
            c.CampaignID = bonus;
            c.DISCOUNT_PERCENT = 0;
            c.DISCOUNT_PERCENT2 = 0;

            c.FIRST_NAME = tbName.Text.Trim();
            c.LAST_NAME = tbLastName.Text.Trim();
            c.EMAIL = UsrEmail;
            c.STREET = "";
            c.CITY = "";
            c.POSTAL_CODE  = "";
            c.COUNTRY = "";
            c.TELEPHONE = tbPhone.Text.Trim();



            c.SentWelcomeMail = false;
            c.UserConfirmed = false;

            crep.Add(c);
            // save
            if (!crep.Save())
            {
                Membership.DeleteUser(UsrEmail);
                return false;
            }
            

            // send confirmation mail
            SendConfirmationMail(usr, Password.Text);

            return true;

        }
        else
        {
            switch (status)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    cvUsername.IsValid = false;
                    break;
                case MembershipCreateStatus.DuplicateEmail:
                    Response.Write("Duplicate Email");
                    break;
                case MembershipCreateStatus.InvalidUserName:
                    Response.Write("Uporabniško ime je neveljavno");
                    break;
            }


        }

        return false;
    
    
    }

    protected void btRegister_Click(object sender, EventArgs e)
    {

        if (RegisterUser())
        // login new user
        {
            FormsAuthentication.SetAuthCookie(UsrEmail, false);
            
            ParentPage.RedirectBackIfSpecified();

            // if not spcified, redirect to confirmation
            Response.Redirect( Page.ResolveUrl(SM.EM.Rewrite.EmenikCustomerRegister(ParentPage.em_user.UserId) + "?ac=confirm"));
        
        }


//            SM.EM.Security.Login.LogIn(Email.Text, false, "");


        //OnRegistrationComplete(this, new EventArgs());
    }




    protected void ValidateDuplicate(object o, ServerValidateEventArgs args)
    {
        args.IsValid = true;

        // check for duplicate page name
        if (Membership.GetUser(Email.Text.Trim()) != null )
        {
            args.IsValid = false;
        
        }
        
    }


    protected void ValidateEmail(object o, ServerValidateEventArgs args)
    {
        args.IsValid = true;

        // check for duplicate page name
        if ( !SM.EM.Helpers.IsValidEmail(Email.Text, false))
        {
            args.IsValid = false;
        
        }
    }



    protected void SendConfirmationMail(MembershipUser usr, string pass)
    {
        if (!SendMail) return;

        if (usr == null) return;


        // construct the verification URL
        string key = Server.UrlEncode(SM.EM.Security.Encryption.Hex(usr.ProviderUserKey.ToString()));
        string verifyUrl = Request.Url.GetLeftPart(UriPartial.Authority) + Page.ResolveUrl(SM.EM.Rewrite.EmenikCustomerEmailVerify( ParentPage.em_user.UserId , key ) );



        StringBuilder sb = new StringBuilder();
        HtmlTextWriter writer = new HtmlTextWriter(new StringWriter(sb));

        objMailConfirmation.UserName = usr.UserName  ;
        objMailConfirmation.Password = pass;
        objMailConfirmation.VerifyUrl = verifyUrl;
        objMailConfirmation.PageName = SM.BLL.Common.Emenik.Data.PortalName();

        objMailConfirmation.PageHref = SM.EM.Rewrite.EmenikUserUrlGetLeftPart(ParentPage.em_user);
        objMailConfirmation.Visible = true;
        objMailConfirmation.RenderControl(writer);
        objMailConfirmation.Visible = false;

        // send mail
//        MailMessage message = SM.EM.Mail.Template.GetNewUserConfirmation(password, usr);
        MailMessage message = new MailMessage();
        message.Sender = new MailAddress(SM.EM.Mail.Account.Info.Email, SM.BLL.Common.Emenik.Data.PortalName());
        message.From = new MailAddress(SM.EM.Mail.Account.Info.Email, SM.BLL.Common.Emenik.Data.PortalName());
        message.To.Clear();
        message.To.Add(new MailAddress(usr.Email ));
        message.IsBodyHtml = true;
        message.Body = sb.ToString();
        message.Subject = ParentPage.em_user.PAGE_TITLE  + " - potrditev email naslova";


        SmtpClient client = new SmtpClient();
        try
        {
            // if local, use gmail, else use server SMTP
            //if (HttpContext.Current != null && HttpContext.Current.Request.IsLocal)
            //{
            //    client.Credentials = new System.Net.NetworkCredential(SM.EM.Mail.Account.Info.Email, SM.EM.Mail.Account.Info.Pass);
            //    client.Port = 587;//or use 587            
            //    client.Host = "smtp.gmail.com";
            //    client.EnableSsl = true;
            //}
            //client.Port = 587;
            //else
            {
                // network delivery for propely setting SENDER header in mail
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.Host = "1dva3.si";
            }


            client.Send(message);
        }

        catch (Exception ex)
        {
            // tmp
            //if (Request.IsLocal)
            //    Response.Redirect(verifyUrl);
            ERROR_LOG.LogError(ex, "SendConfirmationMail");
        }


    }

}
