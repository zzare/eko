﻿<%@ Page Language="C#" ValidateRequest="false" MasterPageFile="~/CMS/CMS.master" AutoEventWireup="true" CodeFile="EmailSend.aspx.cs" Inherits="SM.UI.CMS.EmailSend" Title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register TagPrefix="SB" TagName="Edit" Src="~/CMS/controls/newsletter/_cmsEmailEdit.ascx"   %>
<%@ Register TagPrefix="SB" TagName="Filter" Src="~/CMS/controls/newsletter/_cmsNewsletterFilter.ascx"   %>
<%@ Register TagPrefix="SB" TagName="List" Src="~/CMS/controls/newsletter/_cmsNewsletterList.ascx"   %>

<asp:Content ID="c1" ContentPlaceHolderID="cphNavL" runat="server">
    
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cph" Runat="Server">

            <asp:Panel ID="panSListHD" runat="server" CssClass="cpeListHD" >
                <asp:Image ID="imgCollapse" runat="server" />
               NEWSLETTER list
            </asp:Panel>

            <asp:Panel ID="panSList" runat="server">

                <div class="qFilter">
                    <div class="floatR">
                        <SB:Filter ID="objFilter" runat="server" />
                    </div>
                </div>

                <SB:List ID="objList" runat="server" ShowCheckboxColumn="false" ShowManageColumn="true" ShowEditColumn="true" />
            </asp:Panel>    

            <cc1:CollapsiblePanelExtender ID="cpeQList" runat="server" Collapsed="false" CollapseControlID="panSListHD" CollapsedImage="~/_res/images/icon/plus.gif" ExpandedImage="~/_res/images/icon/minus.gif" ImageControlID="imgCollapse" ExpandControlID="panSListHD" TargetControlID="panSList" ExpandDirection="Vertical" CollapsedText="Expand" ExpandedText="Collapse" >
            </cc1:CollapsiblePanelExtender>    
                

        <asp:Button ValidationGroup="marketing" ID="btNew" runat="server" Text="NEW NEWSLETTER" onclick="btNew_Click" CssClass="btnOrange " />

    
    <br />

                        
    <SB:Edit   id="objEdit" runat="server" />



</asp:Content>