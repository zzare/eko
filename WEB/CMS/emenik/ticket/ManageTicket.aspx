﻿<%@ Page Language="C#" MasterPageFile="~/CMS/CMS.master" AutoEventWireup="true" CodeFile="ManageTicket.aspx.cs" Inherits="SM.UI.CMS.CMS_Intranet_ManageTicket" Title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register TagPrefix="SB" TagName="Edit" Src="~/CMS/controls/ticket/_cmsTicketEdit.ascx"   %>
<%@ Register TagPrefix="SB" TagName="Filter" Src="~/CMS/controls/ticket/_cmsTicketFilter.ascx"   %>
<%@ Register TagPrefix="SB" TagName="List" Src="~/CMS/controls/ticket/_cmsTicketList.ascx"   %>

<asp:Content ID="c1" ContentPlaceHolderID="cphNavL" runat="server">
    
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cph" Runat="Server">

    <asp:UpdatePanel ID="upList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>

            <asp:Panel ID="panSListHD" runat="server" CssClass="cpeListHD" >
                <asp:Image ID="imgCollapse" runat="server" />
               ticket list
            </asp:Panel>

            <asp:Panel ID="panSList" runat="server">

                <div class="qFilter">
                    <asp:UpdatePanel ID="upFilter" runat="server" UpdateMode="Always">
                        <ContentTemplate>
                            <div class="floatR">
                                <SB:Filter ID="objFilter" runat="server" />
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>

                <SB:List ID="objList" runat="server" ShowCheckboxColumn="false" ShowManageColumn="true" ShowEditColumn="true" />
            </asp:Panel>    

            <cc1:CollapsiblePanelExtender ID="cpeQList" runat="server" Collapsed="false" CollapseControlID="panSListHD" CollapsedImage="~/_res/images/icon/plus.gif" ExpandedImage="~/_res/images/icon/minus.gif" ImageControlID="imgCollapse" ExpandControlID="panSListHD" TargetControlID="panSList" ExpandDirection="Vertical" CollapsedText="Expand" ExpandedText="Collapse" >
            </cc1:CollapsiblePanelExtender>    
                
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="objFilter" EventName="OnFilterChanged"   />
            <asp:AsyncPostBackTrigger ControlID="tabTicket$tpTicket$objEdit" EventName="OnUserChanged"   />
            <asp:AsyncPostBackTrigger ControlID="tabTicket$tpTicket$objEdit" EventName="OnCancel"   />
        </Triggers>
    </asp:UpdatePanel>

        
    <br />

    <asp:UpdatePanel ID="upEdit" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
        <ContentTemplate>
            <cc1:TabContainer ID="tabTicket" runat="server" ActiveTabIndex="0"  >
                <cc1:TabPanel ID="tpTicket" HeaderText="Ticket DATA" runat="server"  >
                    <ContentTemplate>
                        
                         <SB:Edit   id="objEdit" runat="server" />
                    
                    </ContentTemplate>
                </cc1:TabPanel>    
                
<%--                <cc1:TabPanel ID="tpPayment" HeaderText="PAYMENTS" runat="server" Visible="true"  >
                    <ContentTemplate>
                    
                        <EM:PaymentList id="objPayment" runat="server"></EM:PaymentList>
                        <EM:PaymentEdit id="objPaymentEdit" runat="server"></EM:PaymentEdit>
                            
                    </ContentTemplate>
                </cc1:TabPanel>  --%>                 
            </cc1:TabContainer>

        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger EventName="OnEdit" ControlID="objList" />
        </Triggers>        
    </asp:UpdatePanel>              





</asp:Content>