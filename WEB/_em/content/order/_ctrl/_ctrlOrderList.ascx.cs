﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SM.UI.Controls;
using SM.EM.UI.Controls;


public partial class _em_content_order__ctrl__ctrlOrderList : System.Web.UI.UserControl  
{

    public List<SHOPPING_ORDER> OrderList;
    public Guid PageID {get; set;}



    protected void Page_Load(object sender, EventArgs e)
    {



    }


    public void LoadData() {


        // bind data
        lvList.DataSource = OrderList;
        lvList.DataBind();

        // load empty
        if (OrderList.Count == 0)
            phEmpty.Visible = true;
    
    }





}
