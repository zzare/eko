﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_cmsOrderList.ascx.cs" Inherits="SM.UI.Controls._cmsOrderList" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Namespace="SM.UI.Controls" TagPrefix="SM" %>


<div class="qContList">

        <asp:LinqDataSource ID="ldsList" runat="server" OnSelecting="ldsList_OnSelecting" >
        </asp:LinqDataSource>
        
        <SM:smListView ID="lvList" runat="server" DataSourceID="ldsList" 
            DataKeyNames="UserId" InsertItemPosition="None"  >
            <LayoutTemplate>
                <table  class="tblData" cellpadding="0" cellspacing="0">
                        <thead>
                            <tr id="headerSort" runat="server">
                                <th ><asp:LinkButton ID="LinkButton5" CommandName="sort" CommandArgument="DateStart" runat="server">Datum</asp:LinkButton></th>
                                <th ><asp:LinkButton ID="LinkButton2" CommandName="sort" CommandArgument="OrderRefID" runat="server">Št. naročila</asp:LinkButton></th>
                                <th ><asp:LinkButton ID="LinkButton1" CommandName="sort" CommandArgument="UserId" runat="server">Uporabnik</asp:LinkButton></th>
                                <th ><asp:LinkButton ID="LinkButton3" CommandName="sort" CommandArgument="OrderStatus" runat="server">Status</asp:LinkButton></th>
                                <th ><asp:LinkButton ID="LinkButton4" CommandName="sort" CommandArgument="DatePayed" runat="server">Datum plačila</asp:LinkButton></th>
                                <th ><asp:LinkButton ID="LinkButton6" CommandName="sort" CommandArgument="Total" runat="server">Znesek</asp:LinkButton></th>
                                <th ><asp:LinkButton ID="LinkButton7" CommandName="sort" CommandArgument="TicketID" runat="server">Ticket</asp:LinkButton></th>
                                <th width="100px" colspan="2" runat="server"  id="headManage" >Uredi</th>
                            </tr>
                        </thead>
                        <tbody id="itemPlaceholder" runat="server"></tbody>
                        <tfoot>
                            <tr>
                                <th style="text-align:right" colspan="8" >
                                    <asp:DataPager runat="server" ID="DataPager" PageSize="30" >
                                        <Fields>
                                            <asp:NextPreviousPagerField  />
                                            <asp:NumericPagerField ButtonCount="5" />
                                        </Fields>
                                    </asp:DataPager>
                                </th>
                            </tr>
                        </tfoot>
                    </table>
            </LayoutTemplate>
            
            <ItemTemplate>
                <tr  class='<%#  Container.DataItemIndex % 2 == 0 ? "  " : " odd " %>'  onmouseover ="this.className='<%#  Container.DataItemIndex % 2 == 0 ? "hovRow" : "hovRowodd" %>'" onmouseout ="this.className = GetCssClass(<%#  Container.DataItemIndex %>)">
                    <td>
                        <%# SM.EM.Helpers.FormatDate(Eval("DateStart"))%>
                    </td>
                    <td>
                        <%# SHOPPING_ORDER.Common.RenderShoppingOrderNumber((SHOPPING_ORDER)Container.DataItem )%>
                    </td>
                    <td>
                        <a target='<%# OnEditTarget %>' href='<%# Page.ResolveUrl( EM_USER.Common.RenderCMSHref(new Guid(Eval("UserId").ToString()))) %>' title="edit user" ><%# Eval("EM_USER.USERNAME") %></a>
                    </td>
                    <td>
                        <%# SHOPPING_ORDER.Common.RenderOrderStatus(Eval("OrderStatus"))%>
                    </td>
                    <td>
                        <%# SM.EM.Helpers.FormatDate(Eval("DatePayed"))%>
                    </td>
                    <td>
                        <%#SM.EM.Helpers.FormatPrice(Eval("TotalWithTax")) + " ( " +  SM.EM.Helpers.FormatPrice(Eval("Total")) + " +ddv ) " %>
                    </td>
                    <td>
                        <a target='<%# OnEditTarget %>' href='<%# Page.ResolveUrl( TICKET.Common.GetTicketDetailsCMSHref(new Guid(Eval("TicketID").ToString()))) %>' title="edit ticket" >ticket</a>
                    </td>
                    <td width="50px" runat="server"  >
                        <a target='<%# OnEditTarget %>' href='<%# Page.ResolveUrl( SHOPPING_ORDER.Common.GetShoppingOrderDetailsCMSHref(new Guid(Eval("OrderID").ToString()))) %>' title="edit order" >naročilo</a>
                    </td>
                </tr>  
            
            </ItemTemplate>
            
            <EmptyDataTemplate>
                
                <div class="odd">
                    Seznam je prazen.
            
                </div>
                
            </EmptyDataTemplate>
            
        
        </SM:smListView>

</div>
