﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_cntUserSettings.ascx.cs" Inherits="c_user__ctrl_settings_cntUserSettings" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>




     
     <asp:RadioButtonList CssClass="register_RadioBL" ID="rblCustomerType" runat="server" OnSelectedIndexChanged="rblCustomerType_SelectedIndexChanged" AutoPostBack="True" RepeatDirection="Horizontal" Width="100%"  >
         <asp:ListItem Text="Posameznik" Value="F"  ></asp:ListItem>
         <asp:ListItem Text="Podjetje" Value="P" Selected="True"  ></asp:ListItem>
     </asp:RadioButtonList>
     <i style="font-size:13px;"><asp:Literal ID="Literal1" runat="server" >* &#268;e želiš prejeti predra&#269;un na naziv podjetja, izberi možnost "Podjetje" </asp:Literal></i>
     <br />
     <br />

            <table cellpadding="2" width="100%" class="register_TABLE" >

     <asp:MultiView ID="mvType" runat="server">
         <asp:View ID="vFizicna" runat="server">
                <tr>
                    <td width="110" class="fieldname"><asp:Label runat="server" ID="lblFirstName" AssociatedControlID="txtFirstName" Text="*Ime:" /></td>
                    <td width="300">                        
                       <asp:TextBox ID="txtFirstName" class="register_TBX" runat="server" Width="100%" /> 
                       <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1"  WatermarkText="Tukaj vpiši svoje ime" TargetControlID ="txtFirstName" WatermarkCssClass="register_Watermark" runat="server" ></cc1:TextBoxWatermarkExtender>
                    </td>
                    <td>
                       <asp:RequiredFieldValidator  ID="valRequireFirstName" runat="server" ControlToValidate="txtFirstName" SetFocusOnError="True" 
                          Text="Vnesite ime." ToolTip="Vnesite ime." Display="Dynamic" ValidationGroup="editUsrSett"></asp:RequiredFieldValidator>
                    </td>
                 </tr>
                 <tr>
                    <td class="fieldname"><asp:Label runat="server" ID="lblLastName" AssociatedControlID="txtLastName" Text="*Priimek:" /></td>
                    <td>
                       <asp:TextBox ID="txtLastName" class="register_TBX" runat="server" Width="100%" />
                       <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2"  WatermarkText="Tukaj vpiši svoj priimek" TargetControlID ="txtLastName" WatermarkCssClass="register_Watermark" runat="server" ></cc1:TextBoxWatermarkExtender>
                    </td>
                    <td>   
                       <asp:RequiredFieldValidator ID="valRequireLastName" runat="server" ControlToValidate="txtLastName" SetFocusOnError="True" 
                          Text="Vnesite priimek." ToolTip="Vnesite priimek." Display="Dynamic" ValidationGroup="editUsrSett"></asp:RequiredFieldValidator>
                    </td>
                 </tr>
                 
         
         </asp:View>
         <asp:View ID="vPravna" runat="server">

                 <tr>
                    <td width="110" class="fieldname"><asp:Label runat="server" ID="lbCName" AssociatedControlID="txtCName" Text="*Naziv podjetja:" /></td>
                    <td width="300">                        
                       <asp:TextBox ID="txtCName" class="register_TBX" runat="server" Width="100%" />
                       <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3"  WatermarkText="Tukaj vpiši naziv podjetja" TargetControlID ="txtCName" WatermarkCssClass="register_Watermark" runat="server" ></cc1:TextBoxWatermarkExtender>
                    </td>
                    <td>   
                       <asp:RequiredFieldValidator ID="valCName" runat="server" ControlToValidate="txtCName" SetFocusOnError="True" 
                          Text="Vnesite naziv podjetja." ToolTip="Vnesite naziv podjetja." Display="Dynamic" ValidationGroup="editUsrSett"></asp:RequiredFieldValidator>
                    </td>
                 </tr>

                 <tr>
                    <td width="110" class="fieldname"><asp:Label runat="server" ID="Label1" AssociatedControlID="txtCTax" Text="*ID za DDV:" /></td>
                    <td width="300">                        
                       <asp:TextBox ID="txtCTax" class="register_TBX" runat="server" Width="100%" /> 
                       <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender4"  WatermarkText="Tukaj vpiši ID za DDV" TargetControlID ="txtCTax" WatermarkCssClass="register_Watermark" runat="server" ></cc1:TextBoxWatermarkExtender>
                    </td>
                    <td>   
                       <asp:RequiredFieldValidator ID="valCTax" runat="server" ControlToValidate="txtCTax" SetFocusOnError="True" 
                          Text="Vnesite ID za DDV." ToolTip="Vnesite Id za DDV." Display="Dynamic" ValidationGroup="editUsrSett"></asp:RequiredFieldValidator>
                    </td>
                 </tr>


         </asp:View>
         
         
     </asp:MultiView>


                <tr>
                    <td class="fieldname"><asp:Label runat="server" ID="Label2" AssociatedControlID="txtStreet" Text="*Email:" /></td>
                    <td>
                       <asp:TextBox runat="server" class="register_TBX" ID="txtEmail" Width="100%" />
                       <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender10"  WatermarkText="Tukaj vpiši email" TargetControlID ="txtEmail" WatermarkCssClass="register_Watermark" runat="server" ></cc1:TextBoxWatermarkExtender>
                    </td>
                    <td>
                       <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtEmail" SetFocusOnError="True" 
                          Text="Vnesite email." ToolTip="Vnesite email." Display="Dynamic" ValidationGroup="editUsrSett"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator runat="server" ID="valEmailPattern"  Display="Dynamic" SetFocusOnError="true" ValidationGroup="editUsrSett"
                                                ControlToValidate="txtEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ErrorMessage="Email naslov ni pravilne oblike."></asp:RegularExpressionValidator>
                                              
                    </td>
                 </tr>

                 <tr>
                    <td class="fieldname"><asp:Label runat="server" ID="lblStreet" AssociatedControlID="txtStreet" Text="*Naslov:" /></td>
                    <td>
                       <asp:TextBox runat="server" class="register_TBX" ID="txtStreet" Width="100%" />
                       <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender5"  WatermarkText="Tukaj vpiši naslov" TargetControlID ="txtStreet" WatermarkCssClass="register_Watermark" runat="server" ></cc1:TextBoxWatermarkExtender>
                    </td>
                    <td>
                       <asp:RequiredFieldValidator ID="valRequireStreet" runat="server" ControlToValidate="txtStreet" SetFocusOnError="True" 
                          Text="Vnesite naslov." ToolTip="Vnesite naslov." Display="Dynamic" ValidationGroup="editUsrSett"></asp:RequiredFieldValidator>
                    </td>
                 </tr>
                 <tr>
                    <td class="fieldname"><asp:Label runat="server" ID="lblPostalCode" AssociatedControlID="txtPostalCode" Text="*Poštna številka:" /></td>
                    <td>
                       <asp:TextBox runat="server" class="register_TBX" ID="txtPostalCode" Width="100%" />
                       <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender6"  WatermarkText="Tukaj vpiši poštno številko" TargetControlID ="txtPostalCode" WatermarkCssClass="register_Watermark" runat="server" ></cc1:TextBoxWatermarkExtender>
                    </td>
                    <td>
                       <asp:RequiredFieldValidator ID="valRequirePostalCode" runat="server" ControlToValidate="txtPostalCode" SetFocusOnError="True" 
                          Text="Vnesite poštno številko." ToolTip="Vnesite poštno številko." Display="Dynamic" ValidationGroup="editUsrSett"></asp:RequiredFieldValidator>
                    </td>
                 </tr>
                 <tr>
                    <td class="fieldname"><asp:Label runat="server" ID="lblCity" AssociatedControlID="txtCity" Text="*Mesto:" /></td>
                    <td>
                       <asp:TextBox runat="server" class="register_TBX" ID="txtCity" Width="100%" />
                       <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender7"  WatermarkText="Tukaj vpiši mesto" TargetControlID ="txtCity" WatermarkCssClass="register_Watermark" runat="server" ></cc1:TextBoxWatermarkExtender>
                    </td>
                    <td>
                       <asp:RequiredFieldValidator ID="valRequireCity" runat="server" ControlToValidate="txtCity" SetFocusOnError="True" 
                          Text="Vnesite mesto." ToolTip="Vnesite mesto." Display="Dynamic" ValidationGroup="editUsrSett"></asp:RequiredFieldValidator>
                    </td>
                 </tr>
                 <tr>
                    <td class="fieldname"><asp:Label runat="server" ID="lblPhone" AssociatedControlID="txtPhone" Text="Telefon:" /></td>
                    <td>
                        <asp:TextBox runat="server" class="register_TBX" ID="txtPhone" Width="100%" />
                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender8"  WatermarkText="Tukaj vpiši telefonsko številko" TargetControlID ="txtPhone" WatermarkCssClass="register_Watermark" runat="server" ></cc1:TextBoxWatermarkExtender>
                    </td>
                 </tr>
                 <tr>
                    <td class="fieldname"><asp:Label runat="server" ID="lblFax" AssociatedControlID="txtFax" Text="Fax:" /></td>
                    <td>
                        <asp:TextBox runat="server" class="register_TBX" ID="txtFax" Width="100%" />
                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender9"  WatermarkText="Tukaj vpiši številko faksa" TargetControlID ="txtFax" WatermarkCssClass="register_Watermark" runat="server" ></cc1:TextBoxWatermarkExtender>
                    </td>
                 </tr>
                <tr>
                  <td>
                  
                  </td>
                  <td style="text-align:right;">
                    <i style="font-size:13px;"><asp:Literal ID="Literal2" runat="server" >Polja označena z <b>*</b> so obvezna.</asp:Literal></i>
                  </td>
                  </tr>

              </table>
                <br />
                
                
                
<%--    <div class="buttonwrapper">
        <asp:LinkButton ID="btSaveEdit" runat="server" CssClass="BTN_quickregister BTN_userdatasave" OnClick="btSaveEdit_Click" ValidationGroup="editUsrSett"><span>Shrani</span></asp:LinkButton>
    </div>                        
--%>