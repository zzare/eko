﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewPollSettings.ascx.cs" Inherits="_ctrl_module_poll_cntPollSettings" %>


<script language = "javascript" type="text/javascript">
    function settingsPollSave() {
        var t = $("#<%= tbTitle.ClientID %>").val();
        var cb = $("#<%= cbActive.ClientID %>").attr('checked');
        WebServiceCWP.SaveSettingsPoll(cwpID, t, cb, onPollSettingsSuccessSave, onCWPSettingsErrorSave);
    }

</script>

<div class="">

    <div class="modPopup_header modPopup_headerEdit">
        <h4>NASTAVITVE ELEMENTA ANKETA</h4>
         <p>Uredi naziv elementa. Določi aktivnost naziva elementa na spletni strani.</p>
    </div>
    <div class="modPopup_main">
        
        
        <span class="modPopup_mainTitle" >Naziv elementa anketa</span>
        <div style="float:left;">
            <input class="modPopup_TBXsettings"  id="tbTitle" maxlength="256" runat="server" type="text" />
        </div>
        <div style="float:left; padding-top:3px;">
            <label  for="<%= cbActive.ClientID %>"> <input  id="cbActive" runat="server" name ="cbActive" type="checkbox" />&nbsp;&nbsp;Prikaži naziv tega elementa na spletni strani</label>
        </div>

    </div>   

    <div class="modPopup_footer"> 
        <div class="buttonwrapperModal">
            <a class="lbutton lbuttonConfirm" href="#" onclick="settingsPollSave(); return false"><span>Shrani</span></a>
            <a class="lbutton lbuttonDelete" href="#" onclick="closeModalLoader(); return false" ><span>Prekliči</span></a>
        </div>
    </div>
</div>






