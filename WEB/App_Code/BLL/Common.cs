﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
namespace SM.BLL
{

    public class Common
    {
        public class ClickIDEventArgs : EventArgs { public int ID { get; set; } }
        public class StringEventArgs : EventArgs { public string Msg { get; set; } }

        public static class PageModule {
            public const int ArticleDetails = 29;
            public const int ProductDetails = 49;

            public const int Default = 25;
            public const int OneColumn = 32;
            public const int TwoColumn = 28;
            public const int ThreeColumn = 33;
            public const int TwoColumnExL = 34;
            public const int ThreeColumnExL = 35;

            public const int Col2_50_50 = 44;
            public const int Col2_33_66 = 45;
            public const int Comb_3_1_3_1 = 46;

            public const int PRODUCT_ADDITIONAL = 51;
            public const int ANIMAL_DEFAULT = 60;

        
        }
        public static class Module
        {
            public const int Content = 1;
            public const int Article = 2;
            public const int Gallery = 3;

        }
        public static class PageModuleTypes
        {
            public const int Admin = 0;
            public const int CMS = 1;
            public const int Emenik_User = 2;
            public const int Emenik = 3;

        }
        public static class Zones
        {
            public static string[] Items() {
                string[] lOut = new string[3] ;
                
                lOut[0] = wpz_3;
                lOut[1] = wpz_1;
                lOut[2] = wpz_2;
                return lOut;
            }

            public const string  wpz_3 = "wpz_3";
            public const string wpz_1 = "wpz_1";
            public const string wpz_2 = "wpz_2";

        }

        public static class Emenik {
            public const int SITEMAP_COUNT_DEFAULT = 500;
            public const string DEFAULT_ZIP_FOLDER = "~/userfiles/tmp/zip/";
            public const string DEFAULT_EXTRACT_FOLDER = "~/userfiles/tmp/extract/";
            public const int EMENIK_TEST_PERIOD_DAYS = 21;
            public const int EMENIK_TO_BE_EXPIRED_DAYS = 30;


            public static string ResolveMainDomainUrl(string url) {
                string main = "http://" + Data.PortalHostDomain();
                string ret = url.Replace("~", main).Trim();
                if (ret.StartsWith("/"))
                    ret = main + ret.TrimStart("/".ToCharArray());


#if DEBUG
        ret = ResolveUrl(url);
#endif

                return ret;
            
            }

            public class Data {
                public static string SMARDT_TITLE = "Razvojni center Brda - eko živinoreja, d.o.o.";
                public static string SMARDT_ADDRESS = "Snežatno 25";
                public static string SMARDT_CITY = "5211 Kojsko";
                public static string SMARDT_PHONE = "+386 (0) 40 602 222";
                public static string SMARDT_URL = "www.terra-gourmet.com";

                public static string TRR()
                {
                    return ConfigurationSettings.AppSettings["TRR"];
                }
                public static string TaxNo()
                {
                    return ConfigurationSettings.AppSettings["TaxNo"];
                }

                public static string PortalName()
                {
                    return ConfigurationSettings.AppSettings["PortalName"];

                }
                public static string PortalHostDomain()
                {
                    return ConfigurationSettings.AppSettings["PortalHostDomain"];

                }
                public static string PortalIP()
                {
                    return ConfigurationSettings.AppSettings["PortalIP"];
                }
                public static int PortalID()
                {
                    return  int.Parse(ConfigurationSettings.AppSettings["PortalID"]);

                }

                public static bool CustomDomainsEnabled()
                {
                    return (ConfigurationSettings.AppSettings["CustomDomainsEnabled"] == "1");
                }
                public static bool EnableSubDomainUsers()
                {
                    return (ConfigurationSettings.AppSettings["EnableSubDomainUsers"] == "1");
                }
                public static bool EnableErrorLogging()
                {
                    return (ConfigurationSettings.AppSettings["EnableErrorLogging"] == "1");
                }
                public static int NewsletterSendDelayMS()
                {
                    return int.Parse(ConfigurationSettings.AppSettings["NewsletterSendDelayMS"]);
                }


                public static string APIkey()
                {
                    return ConfigurationSettings.AppSettings["GOOGLE_API_KEY"];

                }
                public static int OutputCacheDuration()
                {
                    return int.Parse(ConfigurationSettings.AppSettings["OutputCacheDuration"]);
                }
                public static bool OutputCacheSliding()
                {
                    return (ConfigurationSettings.AppSettings["OutputCacheSliding"] == "1");
                }

                public static double DiscountPercent() {
                    return 0;
                        
                }

                public static string FCKEditorBasePath()
                {
                    string baseP = ConfigurationSettings.AppSettings["FCKeditor:BasePath"];


                    return baseP;
                }

                public static string FakeUserName()
                {
                    string ret = "";

                    if (System.Configuration.ConfigurationManager.AppSettings["FakeUserName"] != null)
                        ret = System.Configuration.ConfigurationManager.AppSettings["FakeUserName"];
                    return ret ;
                }

                public static bool IsTestMode()
                {
                    return ( System.Configuration.ConfigurationManager.AppSettings ["IsTestMode"] == "1");
                }

            }

            public static string RenderTemplateCSSHref()
            {

                string domain = "http://" + Data.PortalHostDomain() + "/_em/_common/CSS/Template.css?";

#if DEBUG
        domain = ResolveUrl("~/_em/_common/CSS/Template.css?");
#endif


                string ret = "<link href=\"" + domain +  SM.EM.Helpers.GetCssVersion() + "\" rel=\"stylesheet\" type=\"text/css\" />";                
                
                    
                return ret;            
            }


        }

        public static class EditModeStatus {
            public const int Help = 1;
            public const int Settings = 2;
            public const int View = 3;
            public const int CMS = 4;
        
        }

            public static class ImageType {
                // image types that are mandatory
                public static  List<int> MandatoryTypes
                {
                    get
                    {
                        List<int> list = new List<int>();
                        list.Add(ImageType.CMSthumb);

                        return list;
                    }
                }
                public const int CMSthumb = 5;
                public const int EmenikThumbDefault = 7;
                public const int EmenikGalleryThumbDefault = 7;
                public const int EmenikGalleryThumbCarousel = 6;
                public const int EmenikBigDefault = 4;
                public const int ProductDetail = 19;
                public const int ProductList = 11;
                public const int ProductListBig = 20;
                public const int ProductListBigLow = 21;

                public const int ProductCartSmall = 12;
                public const int ArticleThumbDefault = 22;
                public const int ArticleDetailsThumbDefault = 22;


                public const int AnimalListDefault = 14;
                public const int AnimalDetailDefault = 13;
                public const int AnimalDetailThumbSmall = 16;
                public const int AnimalHomeCarouselBig = 17;

                public const int NewGalleryCarousel = 15;

            }

            public static class GalleryType
            {
                public const int DefaultArticle = 1;
                public const int DefaultGallery = 1;
                public const int DefaultEmenikGallery = 1;
                public const int DefaultProductGallery = 2;
                public const int DefaultAnimalGallery = 3;

            }

            public class Args
            {
                public class IDintEventArgs : EventArgs { public int ID { get; set; } }
                public class IDguidEventArgs : EventArgs { public Guid ID { get; set; } }
                public class IDdateEventArgs : EventArgs { public DateTime ID { get; set; } }
            }


            public class AppSettings
            {
                public static bool EnableSchedule
                {
                    get
                    {
                        if (ConfigurationSettings.AppSettings["EnableSchedule"] == null) return false;
                        if (ConfigurationSettings.AppSettings["EnableSchedule"] != "1") return false;
                        return true;
                    }
                }


            }

            public static string ResolveUrlFull(string virt) {
                return ResolveUrl(virt);
            }

            public static string ResolveUrl(string virt) {

                if (virt.Contains("http://"))
                    return virt;
                return HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + "/" + virt.Replace("~", VirtualPathUtility.ToAbsolute("~")).TrimStart("/".ToCharArray());
            
            
            }

            public static string GetReturnUrl()
            {
                string ru = "";
                ru = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + HttpContext.Current.Request.Url.AbsolutePath;

                return "ru=" + HttpUtility.UrlEncode(ru);
            }
    }
}