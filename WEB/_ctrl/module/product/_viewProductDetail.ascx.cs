﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _ctrl_module_product_viewProductDetail : BaseControlView
{

    public object Data;
    public object[] Parameters;
    public vw_Product Prod;

    public bool HasImage { get {
        if (Prod == null)
            return false;
        if (string.IsNullOrEmpty(Prod.IMG_URL))
            return false;

        if (Prod.IMG_ID < 1)
            return false;
        return true;
    } }

    public bool IsActiveWWW
    {
        get
        {
            if (Prod == null)
                return false;

            if (Prod.DELETED)
                return false;

            if (IsModeCMS)
                return true;

            if (Prod.ACTIVE && Prod.UNITS_IN_STOCK > 0 )
                return true;
            
            return false;
        }
    }



    protected void Page_Load(object sender, EventArgs e)
    {
        

        //LoadList();
    }

    protected override void OnPreRender(EventArgs e)
    {
        LoadList();
        base.OnPreRender(e);
    }

    public void LoadList() {

        if (Data == null)
            return;
        Prod = Data as vw_Product;

        if (Parameters != null)
        {
            if (Parameters[0] != null)
                IsModeCMS = bool.Parse(Parameters[0].ToString());
            //if (Parameters.Length > 1 && Parameters[1] != null)
            //    LangID = Parameters[1].ToString();
        }


        // product buy
        objProductBuy.Data = Data;
        objProductBuy.Parameters = Parameters;
        // product contact
        objProductContact.Data = Data;
        objProductContact.Parameters = Parameters;


        // price
        objProductBuyPrice.Data = Data;
        objProductBuyPrice.Parameters = Parameters;
        objProductContactPrice.Data = Data;
        objProductContactPrice.Parameters = Parameters;

        // rating
        //objProductRating.Data = Data;
        //objProductRating.Parameters = Parameters;


        // product files
        objProductFiles.Data = Data;
        objProductFiles.Parameters = Parameters;


        // image gallery list
        if (HasImage)
        {
            var list = GALLERY.GetImageListByRefID(Prod.PRODUCT_ID, LANGUAGE.GetCurrentLang(), SM.BLL.Common.ImageType.ProductDetail, SM.BLL.Common.ImageType.EmenikBigDefault );

            //objGalleryItem2.ImageTypeID = SM.BLL.Common.ImageType.ProductDetail;
            //objGalleryItem2.ItemClass = "fancy Apd_bigimg";
            //objGalleryItem2.ItemRel = "prod";
            //objGalleryItem2.DefaultTitle = Prod.NAME;
            //objGalleryItem2.Data = list.Skip(1).Take(1);
            //objGalleryItem2.BindData();

            objGalleryList.ItemClass = "fancy";
            objGalleryList.ItemRel = "prod";
            objGalleryList.DefaultTitle = SM.EM.Helpers.StripHTML( Prod.NAME);
            objGalleryList.ImageTypeID = SM.BLL.Common.ImageType.ProductDetail;
            objGalleryList.Data = list.Skip(1);
//            objGalleryList.Data = list.Skip(2);
            objGalleryList.BindData();

        }
        


    }


    protected string RenderSelectedRating(int num) {
        string ret = "";
        int  avg = (int) Math.Round( VOTE.GetAverage(Prod.TOTAL_RATING, Prod.VOTES));
        
        if ((avg == null && num == 3) ||(avg == num))
            ret = "selected=\"selected\"";

        return ret;
    
    }

    protected string RenderClass()
    {
        string ret = "";

        if (!IsModeCMS)
            return ret;

        if (!Prod.ACTIVE)
            ret = " inactive";

        return ret;
    }

    protected string RenderImageTitle()
    {
        string ret = "";

        if (string.IsNullOrEmpty(Prod .IMG_TITLE))
            ret = SM.EM.Helpers.StripHTML(Prod.NAME);
        else
            ret = Prod.IMG_TITLE;

        return ret;
    }



}
