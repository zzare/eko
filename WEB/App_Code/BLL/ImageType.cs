﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using SM.EM.BLL;

/// <summary>
/// Summary description for  IMAGE_TYPE
/// </summary>
public partial class IMAGE_TYPE
{

    public static IMAGE_TYPE GetImageTypeByID(int typeID)
    {
        // check cache
        if (SM.EM.Globals.Settings.ST.EnableCaching)
        {
            // if user in cache, return it
            if (BizObject.Cache[SM.EM.Caching.Key.ImageTypeID(typeID)] != null)
                return (IMAGE_TYPE)BizObject.Cache[SM.EM.Caching.Key.ImageTypeID(typeID)];
        }

        eMenikDataContext db = new eMenikDataContext();
        IMAGE_TYPE typ = db.IMAGE_TYPEs.SingleOrDefault(w => w.TYPE_ID == typeID);

        // cache
        BizObject.CacheData(SM.EM.Caching.Key.ImageTypeID(typeID ), typ );

        return typ;

    }


    public static void RenderImageTypeDimensions(int typeID, out string width, out string height, string suffix)
    {

        IMAGE_TYPE type = IMAGE_TYPE.GetImageTypeByID(typeID);
        height = "";
        width = "";
        if (type == null) return;

        if (type.H_MIN > -1)
        {
            height = type.H_MIN.ToString() + suffix ;
            if (type.H_MAX > type.H_MIN)
                height += " - " + type.H_MAX.ToString() + suffix ;
        }
        else
            height = type.HEIGHT.ToString() + suffix ;
        if (type.W_MIN > -1)
        {
            width = type.W_MIN.ToString() + suffix ;
            if (type.W_MAX > type.W_MIN)
                width += " - " + type.W_MAX.ToString() + suffix ;
        }
        else
            width = type.WIDTH.ToString() + suffix;
    }


    public static class Common
    {
        public class Type
        {
            public const short NOT_SET = 1;
            public const short THUMB_PUBLIC = 2;
            public const short THUMB_ARTICLE = 2;
        }

    }

}
