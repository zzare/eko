﻿<%@ Page Language="C#" ValidateRequest="false" MasterPageFile="~/_em/templates/fall/fall.master" AutoEventWireup="true" CodeFile="search.aspx.cs" Inherits="_em_content_search" Title="Untitled Page" %>
<%@ Register TagPrefix="SM" TagName="ProductList" Src="~/_em/content/product/_ctrl/_pmodProductList.ascx"   %>
<%@ Register Namespace="SM.UI.Controls" TagPrefix="SM"  %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cph" Runat="Server">

        
<div class="searchContHeadW">
        <h1>Iskanje</h1>
        
        <asp:Panel ID="panSearch" runat="server" DefaultButton="btSearch">
            
            <table class="Tsearch" cellpadding="0" cellspacing="0" border="0">
                <tr>
                <td>
                    <asp:TextBox ID="tbSearch" runat="server" CssClass="TBXsearch"></asp:TextBox>
                    <cc1:TextBoxWatermarkExtender ID="tbwexheaderSearchInput" runat="server" WatermarkText="- vpišite iskalni pojem -" TargetControlID ="tbSearch" WatermarkCssClass="headerSearchInputWater"></cc1:TextBoxWatermarkExtender>
                </td>
                <td style="padding-left:10px;">
                    <SM:LinkButtonDefault ID="btSearch" OnClick="btSearch_Click" CssClass="Astepnext"  runat="server" ><span>POIŠČI</span></SM:LinkButtonDefault>
                </td>
                </tr>
            </table>            

            <div class="clearing"></div>
        </asp:Panel>

<% if (!string.IsNullOrEmpty(SearchString)){%>
    
    <br />

        <h3>Iskalni niz: <strong><%= SearchString%></strong></h3>
<%} %>
</div>

        <SM:ProductList ID="objList" runat="server" />
    

</asp:Content>
