﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewProductNew.ascx.cs" Inherits="_ctrl_module_product_edit_viewProductNew" %>

<script language = "javascript" type="text/javascript">
    function addProductSave(act) {
        var t = $("#<%= tbTitle.ClientID %>").val();
        var c = $("#<%= tbCode.ClientID %>").val();
        var s = $("#hfProdSmap").val();
        //alert(t + ' - ' + c);

        openModalLoading('Shranjujem...');
//        if (act == 'edit')
            WebServiceCWP.SaveProductNew(t, c, s, em_g_lang, act, onProductNewSuccessSaveRedirect, onProductSaveError);
//        else {
//            
//            WebServiceCWP.SaveProductNew(t, c, s, em_g_lang, act, onProductNewSuccessSave, onProductSaveError);
//        }

    }

       
</script>



<div class="modPopup_header modPopup_headerEdit">
    <h4>DODAJ NOV IZDELEK</h4>
    <p>Dodaj nov izdelek</p>
</div>
<div class="modPopup_main">
    
    <span class="modPopup_mainTitle">Naziv:</span> 
    <input class="modPopup_TBXsettings" style="width:500px" id="tbTitle" maxlength="256" runat="server" type="text" />
    <span class="modPopup_mainTitle">Šifra:</span> 
    <input class="modPopup_TBXsettings" style="width:100px" id="tbCode" maxlength="256" runat="server" type="text" />
    <br />
    <span class="modPopup_mainTitle">Oddelek:</span> 
    <span id="prodSmapTitle" ><%= SitemapLang.SML_TITLE %></span> 
    <input id="hfProdSmap" type="hidden" value='<%= ( SitemapLang != null) ? SitemapLang.SMAP_ID : -1  %>' /> 
    <img onclick="openLookup('prodSmapTitle', 'hfProdSmap', 2); return false" alt="prebrskaj" src='<%= SM.BLL.Common.ResolveUrl("~/_inc/images/button/lookup.png")%>' />

</div>   

<div class="modPopup_footer"> 
    <div class="buttonwrapperModal">
        <a class="lbutton lbuttonConfirm" href="#" onclick="addProductSave(''); return false"><span>Dodaj nov izdelek</span></a>
        <a class="lbutton lbuttonConfirm" href="#" onclick="addProductSave('edit'); return false"><span>Dodaj nov izdelek in ga UREDI</span></a>
        <a class="lbutton lbuttonDelete" href="#" onclick="closeModalLoader(); return false" ><span>Prekliči</span></a>
    </div>
</div>
