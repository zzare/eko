﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;


namespace SM.UI.Controls
{
    public partial class _ctrl_membership_cmsUserList : BaseControl 
    {
        public delegate void OnEditEventHandler(object sender, SM.BLL.Common.Args .IDguidEventArgs e);
        public event OnEditEventHandler OnEdit;
        public event OnEditEventHandler OnDelete;
        public string OnEditTarget = "_self";
        public bool ShowCheckboxColumn { get; set; }
        public bool ShowManageColumn { get; set; }
        public bool ShowEditColumn { get; set; }
        public int PageSize = 20;
        public bool DeleteAutomatically = true;
        public bool MultipleSelect = true;

        public string SearchText { get; set; }
        public bool? Active { get; set; }
        public string RoleID { get; set; }

        public Guid ExcludeUserID { get; set; }
        public bool IsAdmin { get; set; }
        public List<EM_USER> dsList;



        
        protected void Page_Load(object sender, EventArgs e)
        {




        }







        protected void lvList_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            if (e.CommandName == "Edit") {
                SM.BLL.Common.Args.IDguidEventArgs sbe = new SM.BLL.Common.Args.IDguidEventArgs();
                sbe.ID = new Guid(e.CommandArgument.ToString());
                // raise event
                OnEdit(this,sbe);
            }
            else if (e.CommandName == "login") {
                //MembershipUser user = Membership.GetUser(e.CommandArgument);
                //Session["is_admin"] = true;
                //SM.EM.Security.Login.LogIn(e.CommandArgument.ToString(), false, "", true);
                FormsAuthentication.SetAuthCookie(e.CommandArgument.ToString(), false);
                


                //FormsAuthentication.RedirectFromLoginPage(e.CommandArgument.ToString(), false);
        
            }

        }

        public void BindData()
        {
            //// init page size
            //DataPager pager = (DataPager)lvList.FindControl("DataPager");
            //pager.PageSize = PageSize;

            
            lvList.DataSource = Membership.GetAllUsers();
            lvList.DataBind();



        }




}


}