﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewAttributeEditPrice.ascx.cs" Inherits="_ctrl_module_product_attribute_viewAttributeEditPrice" %>

<script language = "javascript" type="text/javascript">

    function editProductAttributeSavePrice(id, grid) {
        var _items = new Array();
        var loc = new locale(',', '.', 2);
        var p = parseLcNum($("#cEditProduct").find("#tbPrice").val(), loc);


        $("#prodAttributesPrice").find("tbody tr.trItem").each(function (i, val) {
            var _item = new Object();
//            _item.ValMin = $(this).find("#tbaMin").val();
//            _item.ValMax = $(this).find("#tbaMax").val();
//            _item.ValDefault = $(this).find("#tbaDefault").val();

            _item.Price = parseLcNum($(this).find(".tbaPrice").val(), loc);
            _item.Title = $(this).find(".tbaTitle").val();
            _item.Desc = $(this).next().find(".tbaDesc").val();
            _item.Value = $(this).find("input.hfAtrID").val();
            _item.Stock = $(this).find(".tbaStock").val();
            _item.Code = $(this).find(".tbaCode").val();

            if ($(this).find('.cbPriceSel').is(':checked'))
                Array.add(_items, _item);
        });

        openModalLoading('Shranjujem...');
        WebServiceCWP.SaveProductAttributesPrice(id, grid, _items, em_g_lang, onProductEditSuccessSave, onProductSaveError, id);

    }



    $(document).ready(function() {
        $('input.valid-number').bind('keypress', function (e) {
            return (e.which != 45 && e.which != 44 && e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) ? false : true; //allow '-'(45)
        })


    });
       
</script>



<div class="modPopup_header modPopup_headerEdit">
    <h4>UREDI DODATNE ATRIBUTE</h4>
    <p>Uredi dodatne atribute za ta artikel</p>
</div>
<div class="modPopup_main">
    
    <span class="modPopup_mainTitle"><%= GroupAttribute.Title  %></span> 
    
    
    <asp:ListView ID="lvList" runat="server" >
        <LayoutTemplate>
            <div id="prodAttributesPrice">
<%--                <label>
                    <input id="cbAttrSelAll" type="checkbox"  />
                    &nbsp;Označi vse/nobenega
                </label>
                <br />--%>

                <table>
                    <thead>
                        <tr>
                            <th></th>
                            <th>Cena</th>
                            <th>Šifra</th>
                            <th>Zaloga</th>
                            <th>Naziv</th>
                            <%--<th>Opis</th>--%>
                        </tr>
                    </thead>
                    <tbody>
                        
                        <asp:PlaceHolder ID="itemPlaceholder" runat="server"/>
                    
                    </tbody>
                
                </table>
            </div>
        </LayoutTemplate>
        
        <ItemTemplate>
            

            <tr class="trItem">
                <td>
                    <label>
                        <input class="cbPriceSel" <%# RenderChecked(Container.DataItem) %> type="checkbox" value='<%# Eval("AttributeID") %>' />
                        &nbsp;<%# Eval("Title")  %>
                        
                        <input type="hidden" class="hfAtrID" value='<%# Eval("AttributeID")  %>' />
                    </label>
                </td>
                <td>
                    <input class="modPopup_TBXsettings valid-number tbaPrice" style="width:80px; margin-right:3px; text-align:right;"  maxlength="20"  type="text" value='<%# SM.EM.Helpers.FormatPriceEdit( GetAttribute(Container.DataItem).Price)  %>' />
                </td>
                <td>
                    <input class="modPopup_TBXsettings tbaCode" style="width:80px; margin-right:3px; text-align:right;"  maxlength="20"  type="text" value='<%#  GetAttribute(Container.DataItem).Code  %>' />
                </td>
                <td>
                    <input class="modPopup_TBXsettings valid-number tbaStock" style="width:80px; margin-right:3px; text-align:right;"  maxlength="20"  type="text" value='<%# GetAttribute(Container.DataItem).UnitsInStock  %>' />
                </td>
                <td>
                    <input class="modPopup_TBXsettings tbaTitle" style="width:280px; margin-right:3px; text-align:right;"  maxlength="220"  type="text" value='<%# GetAttributeDesc(Container.DataItem).Title %>' />
                </td>
                <%--<td>
                    <input class="modPopup_TBXsettings tbaDesc" style="width:280px; margin-right:3px; text-align:right;" maxlength="520"  type="text" value='<%# GetAttributeDesc(Container.DataItem).Desc %>' />
                </td>--%>

                <%--<td>
                    <input class="modPopup_TBXsettings valid-number" style="width:80px; margin-right:3px; text-align:right;" id="tbaMin" maxlength="20"  type="text" value='<%# GetAttribute(Container.DataItem).ValMin  %>' />
                </td>
                <td>
                    <input class="modPopup_TBXsettings valid-number" style="width:80px; margin-right:3px; text-align:right;" id="tbaMax" maxlength="20"  type="text" value='<%# GetAttribute(Container.DataItem).ValMax %>' />
                </td>
                <td>
                    <input class="modPopup_TBXsettings valid-number" style="width:80px; margin-right:3px; text-align:right;" id="tbaDefault" maxlength="20"  type="text" value='<%# GetAttribute(Container.DataItem).ValDefault %>' />
                </td>
--%>
            </tr> 
            
            <tr>
                <td></td>
                <%--<td>Opis:</td>--%>
            <td colspan="4">
                    <input class="modPopup_TBXsettings tbaDesc" style="width:600px; margin-right:3px; text-align:right;" maxlength="520"  type="text" value='<%# GetAttributeDesc(Container.DataItem).Desc %>' />
                </td>
            
            </tr>          

        </ItemTemplate>
    </asp:ListView>    
    
    

</div>   

<div class="modPopup_footer"> 
    <div class="buttonwrapperModal">
        <a class="lbutton lbuttonConfirm" href="#" onclick="editProductAttributeSavePrice('<%= ProductID %>', '<%= GroupAttribute.GroupAttID %>'); return false"><span>Shrani</span></a>
        <a class="lbutton lbuttonDelete" href="#" onclick="closeModalLoader(); return false" ><span>Prekliči</span></a>
    </div>
</div>
