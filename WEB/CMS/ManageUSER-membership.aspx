﻿<%@ Page Language="C#" MasterPageFile="~/CMS/CMS.master" AutoEventWireup="true" CodeFile="ManageUSER-membership.aspx.cs" Inherits="SM.UI.CMS.CMS_Intranet_ManageUSER_membership" Title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register TagPrefix="SB" TagName="UserEdit" Src="~/CMS/controls/user-membership/_cmsUserEdit.ascx"   %>
<%@ Register TagPrefix="SB" TagName="UserList" Src="~/CMS/controls/user-membership/_cmsUserList.ascx"   %>

<asp:Content ID="c1" ContentPlaceHolderID="cphNavL" runat="server">
    
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cph" Runat="Server">


            <asp:Panel ID="panSListHD" runat="server" CssClass="cpeListHD" >
                <asp:Image ID="imgCollapse" runat="server" />
               USER list
            </asp:Panel>

            <asp:Panel ID="panSList" runat="server">

<%--                <div class="qFilter">
                    <asp:UpdatePanel ID="upFilter" runat="server" UpdateMode="Always">
                        <ContentTemplate>
                            <div class="floatR">
                                <SB:UserFilter ID="objFilter" runat="server" />
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>--%>

                <SB:UserList ID="objUserList" runat="server" ShowCheckboxColumn="false"  ShowManageColumn="true" ShowEditColumn="true" />
            </asp:Panel>    

            <cc1:CollapsiblePanelExtender ID="cpeQList" runat="server" Collapsed="false" CollapseControlID="panSListHD" CollapsedImage="~/_res/images/icon/plus.gif" ExpandedImage="~/_res/images/icon/minus.gif" ImageControlID="imgCollapse" ExpandControlID="panSListHD" TargetControlID="panSList" ExpandDirection="Vertical" CollapsedText="Expand" ExpandedText="Collapse" >
            </cc1:CollapsiblePanelExtender>    
                
    <br />


                        
    <SB:UserEdit   id="objUserEdit" runat="server" />
                    
                 


</asp:Content>