﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SM.EM.UI.CMS;

namespace SM.EM.UI.CMS.Controls
{
    public partial class _cmsImageNew : BaseControl
    {
        public int _smapID = -1;
        public string _langID = "-1";

        public event EventHandler OnImageAdded;
        public event EventHandler OnImageAddCanceled;

        public int MAX_FILE_SIZE = 10485760; // 10 MB
        public int ImageTypeID { get; set; }
        public int GalleryID { get; set; }
        public Guid  CwpID { get; set; }
        //        public int GalleryTypeID { get; set; }

        protected  string _uploadFolder = "";
        public string UploadFolder { 
            get {
                if (!string.IsNullOrEmpty(_uploadFolder)) return _uploadFolder;

                return IMAGE.Common.GetUserImageFolder();
            }
            set
            {
                _uploadFolder = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public void BindData() { 
        
        }

        protected void ClearData() {
            cbActive.Checked = false;
            tbAuthor.Text = "";
        }

        protected void btCancel_Click(object o, EventArgs sender)
        {
            Cancel();
        }
        public void Cancel() {
            OnImageAddCanceled(this, new EventArgs());
        
        }
        protected void btUpload_Click(object o, EventArgs sender) {
            Save();
        }


        public void Save(){
            // check for image type
            if (ImageTypeID <= 0 && GalleryID <=0 && CwpID == Guid.Empty )
            {
                //throw new Exception("set image type");
                return;
            }


            // file is specified
            if (fuFile.HasFile)
            {
                try
                {
                    // validate file type
                    Page.Validate("file");
                    if (!Page.IsValid) return;

                    // validate file size
                    cvFileSize.Validate();
                    if (!cvFileSize.IsValid) return;

                    // if zip, extract it first
                    if (System.IO.Path.GetExtension(fuFile.FileName).ToLower() == ".zip")
                    {
                        string zipFile = Server.MapPath(SM.BLL.Common.Emenik.DEFAULT_ZIP_FOLDER + (Guid.NewGuid ()).ToString() + ".zip");
                        // create folder of not exist
                        if (!System.IO.Directory.Exists(Server.MapPath(SM.BLL.Common.Emenik.DEFAULT_ZIP_FOLDER))) 
                            System.IO.Directory.CreateDirectory(Server.MapPath(SM.BLL.Common.Emenik.DEFAULT_ZIP_FOLDER));

                        // save zip to zip folder
                        fuFile.PostedFile.SaveAs( zipFile);

                        // extract zip and process files in stream
                        ICSharpCode.SharpZipLib.Zip.ZipFile zip = new ICSharpCode.SharpZipLib.Zip.ZipFile(zipFile );
                        foreach (ICSharpCode.SharpZipLib.Zip.ZipEntry ze in zip)
                        {
                            // process zipped file
                            string target = System.IO.Path.Combine(Server.MapPath( IMAGE.Common.GetUserImageFolder()), ze.Name);
                            if (ze.IsFile)
                            {

                                // get file type
                                string fType = ze.Name.Substring(ze.Name.LastIndexOf("."));


                                // check valid format
                                if (IMAGE.Common.IsFormatValid(fType))
                                {
                                    //using (System.IO.FileStream fs = new System.IO.FileStream(target, System.IO.FileMode.Create, System.IO.FileAccess.Write, System.IO.FileShare.None))
                                    //{
                                    // format is valid, add image
                                    System.IO.Stream inputStream = zip.GetInputStream(ze);
                                        //                                        CopyStream(inputStream, fs);
                                    AddImage(inputStream, fType);
                                    //}
                                }
                            }
                        }
                        zip.Close();

                        // delete zip file
                        System.IO.File.Delete(zipFile);

                    
                    }
                    else // one image is uploaded
                    {
                        // process image
                        AddImage(fuFile.PostedFile.InputStream, System.IO.Path.GetExtension(fuFile.FileName));
                    }

                    // raise image added event
                    OnImageAdded(this, new EventArgs());
        


                }
                catch (Exception ex)
                {
                    throw;
                }
            }
            else
            {
                //Label1.Text = "You have not specified a file.";
            }

        
        }

        private void CopyStream(System.IO.Stream readStream, System.IO.Stream writeStream)
        {
            int Length = 256;
            Byte[] buffer = new Byte[Length];
            int bytesRead = readStream.Read(buffer, 0, Length);
            // write the required bytes
            while (bytesRead > 0)
            {
                writeStream.Write(buffer, 0, bytesRead);
                bytesRead = readStream.Read(buffer, 0, Length);
            }
            readStream.Close();
            writeStream.Close();
        }

        protected void AddImage(System.IO.Stream stream, string format ) {
            // if image type not set, get image type from gallery
            if (ImageTypeID < 1)
                ImageTypeID = GALLERY.GetImageTypeFromGalleryDefault(GalleryID);


            // save and manipulate image
            IMAGE_ORIG imgOrig = new IMAGE_ORIG {IMG_ACTIVE= cbActive.Checked , IMG_AUTHOR=tbAuthor.Text  };
            IMAGE img = IMAGE.Func.AddNewImage(System.Drawing.Image.FromStream(stream), IMAGE_TYPE.GetImageTypeByID(ImageTypeID), imgOrig, false, format, UploadFolder); 

            // for each image type in gallery add new image types
            if (GalleryID > 0){
                eMenikDataContext db = new eMenikDataContext();
                var types = from gt in db.GALLERY_IMAGE_TYPEs where gt.GAL_ID == GalleryID select gt.TYPE_ID;

                foreach (int imgType in types) {
                    IMAGE.Func.AutoCreateImageForType(img.IMG_ID, imgType, true);                
                }

                // add image to gallery
                GALLERY.AddImageToGallery(GalleryID, img.IMG_ID);
            
            }
        }

        protected void ValidateFileSize(object source, ServerValidateEventArgs args)
        {
            args.IsValid = false;
            CustomValidator cv = (CustomValidator)source;
            cv.ErrorMessage = "* Datoteka mora biti manjša od " + (MAX_FILE_SIZE / (1024*1024)).ToString() + "MB. Prosim, uporabite manjšo datoteko.";

            if (fuFile.HasFile)
                if (fuFile.PostedFile.ContentLength < MAX_FILE_SIZE)
                    args.IsValid = true;

        }

    }
}