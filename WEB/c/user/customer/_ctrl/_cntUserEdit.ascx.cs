﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

namespace SM.UI.Controls
{
    public partial class _customer_cntUserEdit : BaseControl 
    {
        public Guid PageID { get; set; }

        public event EventHandler OnUserChanged;
        public event EventHandler OnCancel;

        public Guid UID {
            get
            {
                Guid res = Guid.Empty;
                if (ViewState["UID"] == null) return res;
                if (!SM.EM.Helpers.IsGUID(ViewState["UID"].ToString())) return res;

                return new Guid(ViewState["UID"].ToString());
            }
            set{
                ViewState["UID"] = value;
            }
        }
        public Guid ProjectID { get; set; }
        public Guid SiteID { get; set; }
        public Guid GroupID { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {

            }

        }

        // default values
        protected void LoadDefaultValues() {
            UID = Guid.Empty;

//            litCode.Text = "";

            ParentPage.mp.StatusText = "Add NEW USER";

  //          btDeleteUser.Visible = false;
        }



        // bind data
        public void BindData() {
            this.Visible = true;




            if (UID == Guid.Empty)
            {
                LoadDefaultValues();
            }
            else {
                LoadData();
            
            }
            LoadRoles();

        }

        // LOAD Question
        protected void LoadData() {

            CustomerRep crep = new CustomerRep();
            CUSTOMER usr = crep.GetCustomerByID(UID);

            if (usr == null)
                return;
            if (usr.PageID != PageID)
                return;


            MembershipUser muser = Membership.GetUser(usr.USERNAME);

            tbUserName.Text = usr.USERNAME;
            tbEmail.Text = usr.USERNAME;
            tbName.Text = usr.FIRST_NAME;
            tbLastName.Text = usr.LAST_NAME;


            cbActive.Checked = muser.IsApproved ;
            
            // set status text
            string status = "";
            status = "Uporabnik: " + usr.USERNAME ;

            panEdit.Visible = true;

            // load bonus
            if (usr.CampaignID != null) {
                string type = "";
                string href = "";
                string name = "";
                // check if is a campaign
                MARKETING_CAMPAIGN c = MARKETING_CAMPAIGN.GetCampaignByID(usr.CampaignID.Value);
                if (c != null)
                {
                    type = "kampanja: ";
                    href = MARKETING_CAMPAIGN.Common.GetCampaignDetailsCMSHref(usr.CampaignID.Value);
                    name = c.Name;
                }
                else {
                    type = "user affilliate - bonus";
                    href = EM_USER.Common.RenderCMSHref(usr.CampaignID.Value);
                }
//                litBonus.Text = string.Format("<a href=\"" + Page.ResolveUrl( href) +"\" >" + type + name  +"</a>");
            
            }



        }

        protected void LoadRoles() {
            // load roles if admin
            //if (SM.BLL.BizObject.IsAdmin)
            //{
                //List<string> list = Roles.GetAllRoles().ToList();
                //if (list.Contains("admin"))
                //    if (!Roles.IsUserInRole("admin"))
                //        list.Remove("admin");
                
            //cblRoles.DataSource = list;
            //    cblRoles.DataBind();


            //    foreach (ListItem item in cblRoles.Items)
            //    {

            //        if (Roles.IsUserInRole(tbUserName.Text, item.Value))
            //            item.Selected = true;
            //    }

        }

        public void Save() {
            
            // validation
            Page.Validate("usredit");
            if (!Page.IsValid) return;

            
            eMenikDataContext db = new eMenikDataContext();

            CUSTOMER usr; 
            // add new user
            if (UID == Guid.Empty)
            {
                return;


            }
            else
            {
                usr = db.CUSTOMERs.SingleOrDefault(w=>w.CustomerID == UID );
            }



            //usr.act = cbActive.Checked;
            //usr.EMAIL = tbEmail.Text;

            usr.FIRST_NAME = tbName.Text.Trim();
            usr.LAST_NAME = tbLastName.Text.Trim();

            int discount = 0;


            // save
            db.SubmitChanges();



            // set user active
            MembershipUser userM = Membership.GetUser(usr.USERNAME);

            userM.IsApproved = cbActive.Checked;
            userM.UnlockUser();
            Membership.UpdateUser(userM);

            // change password
            if (!String.IsNullOrEmpty(tbChangePassWord.Text.Trim()))
                userM.ChangePassword(userM.GetPassword(), tbChangePassWord.Text);


            
            //// save roles
            //foreach(ListItem  item in cblRoles.Items  ){
            //    if (item.Selected) {
            //        if (!Roles.IsUserInRole(tbUserName.Text, item.Value))
            //            Roles.AddUserToRole(tbUserName.Text, item.Value);
            //    }
            //    else if (Roles.IsUserInRole(tbUserName.Text, item.Value))
            //        Roles.RemoveUserFromRole(tbUserName.Text, item.Value);
            //}


            // set status text
            ParentPage.mp.StatusText = "User has been SAVED ";

            // remove from cache
            //usr.RemoveFromCache();


            
            // raise changed event
            OnUserChanged (this, new EventArgs());

        }

        // GROUP
        protected void btSave_Click(object sender, EventArgs e)
        {
            Save();
        }
        protected void btCancel_Click(object sender, EventArgs e)
        {
            //this.Visible = false;
            OnCancel(this, new EventArgs());
            // remove status text
            ParentPage.mp.StatusText = "";

            
        }








}
}