﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EmenikStatusBar.ascx.cs" Inherits="SM.EM.UI.Controls.EmenikStatusBar" %>
<div class="settNavTopWrapp">
    <div id="divNavTop" runat="server" class="settNavTop clearfix">
        <div class="settNavTopLeft">
            <div class="settNavTopLeftI">
                <span>Spletni urejevalnik</span> 
                <a href='<%= SM.BLL.Common.Emenik.ResolveMainDomainUrl("~/")  %>'><img alt="" src='<%= SM.BLL.Common.ResolveUrl("~/_inc/images/design/builder/smardt_logo.png") %>' /></a>
                <a href="http://smardt.si">spletna trgovina smardt</a>
                <div class="clearing"></div>
            </div>        
        </div>
        
        <div class="settNavTopRight">
            <span>
                <asp:LoginView ID="LoginView1" runat="server"  >
                    <AnonymousTemplate>
                    </AnonymousTemplate>
                    <LoggedInTemplate>
                        Uporabnik: <a href='<%= Page.ResolveUrl(SM.EM.Rewrite.Main.UserSettingsURL()) %>' ><asp:LoginName ID="LoginName1" runat="server" CssClass="wizz_loginName" /></a>
                        &nbsp;
                        <a href='<%= Page.ResolveUrl(SM.EM.Rewrite.Main.UserSettingsURL()) %>' >[servisne strani]</a>

                        <% if (SM.BLL.Custom.Permission.CanAdminCustomers ()){%>
                        <a href='<%= Page.ResolveUrl(SM.BLL.Custom.Url.RegisterCustomer) %>' >[registriraj]</a>
                        <%} %>

                        &nbsp;&nbsp;&nbsp;    
                        <a id="btLogout" runat="server" onserverclick="btLogout_Logout" >ODJAVI SE</a>
                    </LoggedInTemplate>                
                </asp:LoginView>
            </span>
            <div class="clearing"></div> 
        </div>
    </div>

    
    
</div>
<div class="settNavMidW">
    <div class="settNavMid clearfix">
        <div class="settNavMidL">
        
<% if (SM.EM.Security.Permissions.IsPortalAdmin())
   { %>
               <a class="settNavTopRightOrange btSettings <%= RenderSelClassEdit() %>" href='<%= Page.ResolveUrl(SM.EM.Rewrite.EmenikUserSettingsUrl(ParentPage.PageName)) %>'><span>Nastavitve spletne strani</span></a>
<% } %>

               <asp:LinkButton ID="btEdit" runat="server" OnClick="btEdit_Click" CssClass="btEdit"><span>Uredi vsebino spletne strani</span></asp:LinkButton>
               <asp:LinkButton ID="btView" runat="server" OnClick="btView_Click" CssClass="btView"><span>Predogled spletne strani</span></asp:LinkButton>
            <div class="clearing"></div> 
        </div>
<%--        <div class="settNavMidR">
            <span class="settNavMidR_sepa"></span>
            <div class="settNavMidRI">
                <span class="pagename"><%=ParentPage.em_user.PAGE_NAME%>.<%=  SM.BLL.Common.Emenik.Data.PortalHostDomain()  %> <br /></span>
                <span class="icon">Status strani:&nbsp;<asp:Label ID="litStatus" runat="server" CssClass="litPageStatus" ></asp:Label>
                </span>
            </div>
            <span class="settNavMidR_sepa"></span>
        </div>--%>
    </div>
</div>


    <div id="phTooltip" runat="server" class="settNavBottomWrapp">
        <div id="divNavBottom" runat="server" class="settNavBottom">
            <div class="settNavBottomI">
                <asp:PlaceHolder ID="phCMS" runat="server">
                    <h4>Urejanje vsebine spletne strani</h4>
                    Vsebino spletne strani dodajate preko kontrol v zelenem pasu urejevalnika <strong>Dodaj NOV ELEMENT</strong>.
                    Izbirate med različnimi elementi: tekst, novice, galerije, zemljevide, ankete,...
                
                
                </asp:PlaceHolder>
            
                <asp:PlaceHolder ID="phView" runat="server">
                
                    <h4>Predogled spletne strani</h4>
                    Trenutno ste v načinu <strong>predogleda</strong> spletne strani.
                    Spletno stran vidite tako, kot jo bodo videli <strong>obiskovalci</strong>.
                                
                
                </asp:PlaceHolder>
                
            
            </div>
        </div>
    </div>

