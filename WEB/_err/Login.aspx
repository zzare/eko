﻿<%@ Page Language="C#" MasterPageFile="~/cms/cms.master" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="_err_Login" Title="Untitled Page" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cph" Runat="Server">

<asp:PlaceHolder ID="phLogin" runat="server">
    <h1>Prijava. </h1>
    <h2>Pred nadaljevanjem se moraš prijaviti.</h2>
    
    <h2>Še nimaš računa?</h2>
    <br />
    <a class="BTN_register BTN_notyetregistered" href='<%=Page.ResolveUrl("~/default.aspx") %>'>Registriraj se brezplačno »</a>


</asp:PlaceHolder>

<asp:PlaceHolder ID="phForbidden" runat="server">
    <h1>Dostop ni dovoljen. </h1>
    <h2>Do vsebine nimaš dostopa.</h2>


</asp:PlaceHolder>


</asp:Content>

