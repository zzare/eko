﻿<%@ Page Language="C#" MasterPageFile="~/CMS/CMS.master" AutoEventWireup="true" CodeFile="ManageImageType.aspx.cs" Inherits="SM.UI.CMS.EM.CMS_ManageImageType" Title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Namespace="SM.UI.Controls" TagPrefix="SM" %>

<asp:Content ID="Content3" ContentPlaceHolderID="cph" Runat="Server">

    <asp:UpdatePanel ID="upList" runat="server" ChildrenAsTriggers="true">
        <ContentTemplate>


            <asp:LinqDataSource ID="ldsList" runat="server" 
                ContextTypeName="eMenikDataContext" EnableDelete="True" EnableInsert="True" 
                EnableUpdate="True" TableName="IMAGE_TYPEs" 
                OnSelecting="ldsList_OnSelecting" oninserting="ldsList_Inserting" 
                onupdating="ldsList_Updating">
            </asp:LinqDataSource>
            
            <SM:smListView ID="lvList" runat="server" DataSourceID="ldsList" 
                DataKeyNames="TYPE_ID" InsertItemPosition="LastItem" 
                onitemcanceling="lvList_ItemCanceling" onitemediting="lvList_ItemEditing" 
                onitemupdated="lvList_ItemUpdated" onitemdatabound="lvList_ItemDataBound" >
                <LayoutTemplate>
                    <table  class="tblData">
                            <thead>
                                <tr id="headerSort" runat="server">
                                    <th width="50px"><asp:LinkButton ID="LinkButton1" CommandName="sort" CommandArgument="TYPE_ID" runat="server">ID</asp:LinkButton></th>
                                    <th ><asp:LinkButton ID="LinkButton3" CommandName="sort" CommandArgument="TYPE_TITLE" runat="server">Title</asp:LinkButton></th>
                                    <th ><asp:LinkButton ID="LinkButton4" CommandName="sort" CommandArgument="DESC" runat="server">Desc</asp:LinkButton></th>
                                    <th width="50px"><asp:LinkButton ID="LinkButton5" CommandName="sort" CommandArgument="HEIGHT" runat="server">Height</asp:LinkButton></th>
                                    <th width="50px"><asp:LinkButton ID="LinkButton2" CommandName="sort" CommandArgument="WIDTH" runat="server">Width</asp:LinkButton></th>
                                    <th width="50px"><asp:LinkButton ID="LinkButton10" CommandName="sort" CommandArgument="H_MIN" runat="server">MIN Height</asp:LinkButton></th>
                                    <th width="50px"><asp:LinkButton ID="LinkButton11" CommandName="sort" CommandArgument="H_MAX" runat="server">MAX Height</asp:LinkButton></th>
                                    <th width="50px"><asp:LinkButton ID="LinkButton8" CommandName="sort" CommandArgument="W_MIN" runat="server">MIN Width</asp:LinkButton></th>
                                    <th width="50px"><asp:LinkButton ID="LinkButton9" CommandName="sort" CommandArgument="W_MAX" runat="server">MAX Width</asp:LinkButton></th>
                                    <th width="50px"><asp:LinkButton ID="LinkButton12" CommandName="sort" CommandArgument="ORIG_HEIGHT" runat="server">ORIG Height</asp:LinkButton></th>
                                    <th width="50px"><asp:LinkButton ID="LinkButton13" CommandName="sort" CommandArgument="ORIG_WIDTH" runat="server">ORIG Width</asp:LinkButton></th>
                                    <th width="50px"><asp:LinkButton ID="LinkButton7" CommandName="sort" CommandArgument="ORIG_SIZE" runat="server">ORIG size</asp:LinkButton></th>
                                    <th width="50px"><asp:LinkButton ID="LinkButton14" CommandName="sort" CommandArgument="SIZE_MAX" runat="server">MAX size</asp:LinkButton></th>
                                    <th width="50px"><asp:LinkButton ID="LinkButton15" CommandName="sort" CommandArgument="CROP_POSITION" runat="server">crop pos</asp:LinkButton></th>
                                    <th width="50px"><asp:LinkButton ID="LinkButton16" CommandName="sort" CommandArgument="FILE_TYPE" runat="server">file TYPE</asp:LinkButton></th>
                                    <th width="50px"><asp:LinkButton ID="LinkButton6" CommandName="sort" CommandArgument="T_TYPE" runat="server">Type TYPE</asp:LinkButton></th>
                                    <th colspan="2">Manage</th>
                                </tr>
                            </thead>
                            <tbody id="itemPlaceholder" runat="server"></tbody>
                            <tfoot>
                                <tr>
                                    <th style="text-align:right" colspan="17">
                                        <asp:DataPager runat="server" ID="DataPager" PageSize="40">
                                            <Fields>
                                                <asp:NextPreviousPagerField  />
                                                <asp:NumericPagerField ButtonCount="5" />
                                            </Fields>
                                        </asp:DataPager>
                                    </th>
                                </tr>
                            </tfoot>
                        </table>
                </LayoutTemplate>
                
                <ItemTemplate>
                    <tr  class='<%#  Container.DataItemIndex % 2 == 0 ? " " : " odd " %>'  onmouseover ="this.className='<%#  Container.DataItemIndex % 2 == 0 ? "hovRow" : "hovRowodd" %>'" onmouseout ="this.className = GetCssClass(<%#  Container.DataItemIndex %>)">
                        <td>
                            <%# Eval("TYPE_ID") %>
                        </td>
                        <td>
                            <%# Eval("TYPE_TITLE") %>
                        </td>
                        <td>
                            <%# Eval("DESC") %>
                        </td>

                        <td>
                            <%# Eval("HEIGHT") %>
                        </td>
                        <td>
                            <%# Eval("WIDTH") %>
                        </td>
                        <td>
                            <%# Eval("H_MIN") %>
                        </td>
                        <td>
                            <%# Eval("H_MAX") %>
                        </td>
                        <td>
                            <%# Eval("W_MIN") %>
                        </td>
                        <td>
                            <%# Eval("W_MAX") %>
                        </td>
                        <td>
                            <%# Eval("ORIG_HEIGHT") %>
                        </td>
                        <td>
                            <%# Eval("ORIG_WIDTH") %>
                        </td>
                        <td>
                            <%# Eval("ORIG_SIZE") %>
                        </td>
                        <td>
                            <%# Eval("SIZE_MAX") %>
                        </td>
                        <td>
                            <%# Eval("CROP_POSITION") %>
                        </td>
                        <td>
                            <%# Eval("FILE_TYPE") %>
                        </td>
                        <td>
                            <%# Eval("T_TYPE") %>
                        </td>
                        <td >
                            <asp:LinkButton ID="lbEdit" CommandName="Edit" runat="server">edit</asp:LinkButton>
                        </td>
                        <td>
                            <asp:LinkButton ID="lbDelete" OnClientClick="javascript: return confirm('Are you sure you want to delete this item?');" CommandName="Delete" runat="server">delete</asp:LinkButton>
                        </td>                
                    </tr>        
                
                </ItemTemplate>
                
                <InsertItemTemplate>
                    <tr class="insertRow">
                        <td colspan="2">
                            <asp:TextBox MaxLength="256" Width="99%" ID="tbMaster" runat="server" Text='<%# Bind("TYPE_TITLE") %>'></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="512" Width="99%" ID="tbTitle" runat="server" Text='<%# Bind("DESC") %>'></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="9" Width="99%" ID="tbDesc" runat="server" Text='<%# Bind("HEIGHT") %>'></asp:TextBox>
                            <asp:RequiredFieldValidator ValidationGroup="insert" ID="RequiredFieldValidator1" ControlToValidate="tbDesc" Display="Dynamic"  runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="9" Width="99%" ID="TextBox1" runat="server" Text='<%# Bind("WIDTH") %>'></asp:TextBox>
                            <asp:RequiredFieldValidator ValidationGroup="insert" ID="RequiredFieldValidator3" ControlToValidate="TextBox1" Display="Dynamic"  runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="9" Width="99%" ID="TextBox2" runat="server" Text='<%# Bind("H_MIN") %>'></asp:TextBox>
                            <asp:RequiredFieldValidator ValidationGroup="insert" ID="RequiredFieldValidator4" ControlToValidate="TextBox2" Display="Dynamic"  runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="9" Width="99%" ID="TextBox3" runat="server" Text='<%# Bind("H_MAX") %>'></asp:TextBox>
                            <asp:RequiredFieldValidator ValidationGroup="insert" ID="RequiredFieldValidator5" ControlToValidate="TextBox3" Display="Dynamic"  runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="9" Width="99%" ID="TextBox4" runat="server" Text='<%# Bind("W_MIN") %>'></asp:TextBox>
                            <asp:RequiredFieldValidator ValidationGroup="insert" ID="RequiredFieldValidator6" ControlToValidate="TextBox4" Display="Dynamic"  runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="9" Width="99%" ID="TextBox5" runat="server" Text='<%# Bind("W_MAX") %>'></asp:TextBox>
                            <asp:RequiredFieldValidator ValidationGroup="insert" ID="RequiredFieldValidator7" ControlToValidate="TextBox5" Display="Dynamic"  runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="9" Width="99%" ID="TextBox7" runat="server" Text='<%# Bind("ORIG_HEIGHT") %>'></asp:TextBox>
                            <asp:RequiredFieldValidator ValidationGroup="insert" ID="RequiredFieldValidator8" ControlToValidate="TextBox7" Display="Dynamic"  runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="9" Width="99%" ID="TextBox6" runat="server" Text='<%# Bind("ORIG_WIDTH") %>'></asp:TextBox>
                            <asp:RequiredFieldValidator ValidationGroup="insert" ID="RequiredFieldValidator9" ControlToValidate="TextBox6" Display="Dynamic"  runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="9" Width="99%" ID="TextBox8" runat="server" Text='<%# Bind("ORIG_SIZE") %>'></asp:TextBox>
                            <asp:RequiredFieldValidator ValidationGroup="insert" ID="RequiredFieldValidator10" ControlToValidate="TextBox8" Display="Dynamic"  runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="9" Width="99%" ID="TextBox9" runat="server" Text='<%# Bind("SIZE_MAX") %>'></asp:TextBox>
                            <asp:RequiredFieldValidator ValidationGroup="insert" ID="RequiredFieldValidator11" ControlToValidate="TextBox9" Display="Dynamic"  runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="9" Width="99%" ID="TextBox10" runat="server" Text='<%# Bind("CROP_POSITION") %>'></asp:TextBox>
                            <asp:RequiredFieldValidator ValidationGroup="insert" ID="RequiredFieldValidator12" ControlToValidate="TextBox10" Display="Dynamic"  runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="9" Width="99%" ID="TextBox11" runat="server" Text='<%# Bind("FILE_TYPE") %>'></asp:TextBox>
                            <asp:RequiredFieldValidator ValidationGroup="insert" ID="RequiredFieldValidator13" ControlToValidate="TextBox11" Display="Dynamic"  runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="9" Width="99%" ID="TextBox12" runat="server" Text='<%# Bind("T_TYPE") %>'></asp:TextBox>
                            <asp:RequiredFieldValidator ValidationGroup="insert" ID="RequiredFieldValidator14" ControlToValidate="TextBox12" Display="Dynamic"  runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </td>                        

                        <td colspan="2">
                            <asp:Button ID="btSave" CommandName="Insert" runat="server" Text="Add new" ValidationGroup="insert" ></asp:Button>
                        </td>
                    </tr>     
                    <tr class="errorRow">
                        <td colspan="2">
                            <asp:RequiredFieldValidator ValidationGroup="insert" ID="reqState" ControlToValidate="tbMaster" Display="Dynamic"  runat="server" ErrorMessage="* Enter title"></asp:RequiredFieldValidator>
                        </td>
                        <td colspan="2">
                            <asp:RequiredFieldValidator ValidationGroup="insert" ID="RequiredFieldValidator2" ControlToValidate="tbTitle" Display="Dynamic"  runat="server" ErrorMessage="* Enter desc"></asp:RequiredFieldValidator>
                        </td>
                        <td colspan="13">
                        </td>
                    </tr>   
                        
                </InsertItemTemplate>
                
                <EditItemTemplate>
                    <tr class="editRow">
                        <td colspan="2">
                            <asp:TextBox MaxLength="256" Width="99%" ID="tbMaster" runat="server" Text='<%# Bind("TYPE_TITLE") %>'></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="512" Width="99%" ID="tbTitle" runat="server" Text='<%# Bind("DESC") %>'></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="9" Width="99%" ID="tbDesc" runat="server" Text='<%# Bind("HEIGHT") %>'></asp:TextBox>
                            <asp:RequiredFieldValidator ValidationGroup="edit" ID="RequiredFieldValidator1" ControlToValidate="tbDesc" Display="Dynamic"  runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="9" Width="99%" ID="TextBox1" runat="server" Text='<%# Bind("WIDTH") %>'></asp:TextBox>
                            <asp:RequiredFieldValidator ValidationGroup="edit" ID="RequiredFieldValidator3" ControlToValidate="TextBox1" Display="Dynamic"  runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="9" Width="99%" ID="TextBox2" runat="server" Text='<%# Bind("H_MIN") %>'></asp:TextBox>
                            <asp:RequiredFieldValidator ValidationGroup="edit" ID="RequiredFieldValidator4" ControlToValidate="TextBox2" Display="Dynamic"  runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="9" Width="99%" ID="TextBox3" runat="server" Text='<%# Bind("H_MAX") %>'></asp:TextBox>
                            <asp:RequiredFieldValidator ValidationGroup="edit" ID="RequiredFieldValidator5" ControlToValidate="TextBox3" Display="Dynamic"  runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="9" Width="99%" ID="TextBox4" runat="server" Text='<%# Bind("W_MIN") %>'></asp:TextBox>
                            <asp:RequiredFieldValidator ValidationGroup="edit" ID="RequiredFieldValidator6" ControlToValidate="TextBox4" Display="Dynamic"  runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="9" Width="99%" ID="TextBox5" runat="server" Text='<%# Bind("W_MAX") %>'></asp:TextBox>
                            <asp:RequiredFieldValidator ValidationGroup="edit" ID="RequiredFieldValidator7" ControlToValidate="TextBox5" Display="Dynamic"  runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="9" Width="99%" ID="TextBox7" runat="server" Text='<%# Bind("ORIG_HEIGHT") %>'></asp:TextBox>
                            <asp:RequiredFieldValidator ValidationGroup="edit" ID="RequiredFieldValidator8" ControlToValidate="TextBox7" Display="Dynamic"  runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="9" Width="99%" ID="TextBox6" runat="server" Text='<%# Bind("ORIG_WIDTH") %>'></asp:TextBox>
                            <asp:RequiredFieldValidator ValidationGroup="edit" ID="RequiredFieldValidator9" ControlToValidate="TextBox6" Display="Dynamic"  runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="9" Width="99%" ID="TextBox8" runat="server" Text='<%# Bind("ORIG_SIZE") %>'></asp:TextBox>
                            <asp:RequiredFieldValidator ValidationGroup="edit" ID="RequiredFieldValidator10" ControlToValidate="TextBox8" Display="Dynamic"  runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="9" Width="99%" ID="TextBox9" runat="server" Text='<%# Bind("SIZE_MAX") %>'></asp:TextBox>
                            <asp:RequiredFieldValidator ValidationGroup="edit" ID="RequiredFieldValidator11" ControlToValidate="TextBox9" Display="Dynamic"  runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="9" Width="99%" ID="TextBox10" runat="server" Text='<%# Bind("CROP_POSITION") %>'></asp:TextBox>
                            <asp:RequiredFieldValidator ValidationGroup="edit" ID="RequiredFieldValidator12" ControlToValidate="TextBox10" Display="Dynamic"  runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="9" Width="99%" ID="TextBox11" runat="server" Text='<%# Bind("FILE_TYPE") %>'></asp:TextBox>
                            <asp:RequiredFieldValidator ValidationGroup="edit" ID="RequiredFieldValidator13" ControlToValidate="TextBox11" Display="Dynamic"  runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="9" Width="99%" ID="TextBox13" runat="server" Text='<%# Bind("T_TYPE") %>'></asp:TextBox>
                            <asp:RequiredFieldValidator ValidationGroup="edit" ID="RequiredFieldValidator15" ControlToValidate="TextBox13" Display="Dynamic"  runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </td>

                        <td >
                            <asp:Button ID="btSave" CommandName="Update" runat="server" Text="Save" ValidationGroup="edit" ></asp:Button>
                        </td>
                        <td >
                            <asp:Button ID="btCancel" CommandName="Cancel" runat="server" Text="Cancel" ></asp:Button>
                        </td> 
                    </tr>     
                    <tr class="errorRow">
                        <td colspan="2">
                            <asp:RequiredFieldValidator ValidationGroup="edit" ID="reqState" ControlToValidate="tbMaster" Display="Dynamic"  runat="server" ErrorMessage="* Enter title"></asp:RequiredFieldValidator>
                        </td>
                        <td colspan="2">
                            <asp:RequiredFieldValidator ValidationGroup="edit" ID="RequiredFieldValidator2" ControlToValidate="tbTitle" Display="Dynamic"  runat="server" ErrorMessage="* Enter desc"></asp:RequiredFieldValidator>
                        </td>
                        <td colspan="13">
                        </td>
                    </tr>                
                
                </EditItemTemplate>
                

            </SM:smListView>

        </ContentTemplate>
    </asp:UpdatePanel>


</asp:Content>


