﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewProductSmapLookup.ascx.cs" Inherits="_ctrl_module_product_viewProductSmapLookup" %>

<script language = "javascript" type="text/javascript">

    $(document).ready(function() {
        $('#lkpProductSmap li').click(function() {

            var tit = $('#<%= TitleID %>');
            var hf = $('#<%= ValueID %>');
            tit.html($(this).children("span").eq(0).html());
            hf.val(this.id);

            closeModalLoader();
            return false;
        });
    });

    function selectLookup() {

        closeModalLoader();

    }
</script>



<div class="modPopup_header modPopup_headerEdit">
    <h4>IZBERI ODDELEK</h4>
    <p>Izberi glavni oddelek za izdelek</p>
</div>
<div class="modPopup_main">
    <div id="lkpProductSmap" class="lkpProductSmap">
        <%= RenderItems() %>
    </div>

</div>   

<div class="modPopup_footer"> 
    <div class="buttonwrapperModal">
<%--        <a class="lbutton lbuttonConfirm" href="#" onclick="selectLookup(); return false"><span>Dodaj nov izdelek</span></a>--%>
        <a class="lbutton lbuttonDelete" href="#" onclick="closeModalLoader(); return false" ><span>Prekliči</span></a>
    </div>
</div>
