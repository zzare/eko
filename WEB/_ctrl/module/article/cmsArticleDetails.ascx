﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="cmsArticleDetails.ascx.cs" Inherits="SM.EM.UI.Controls.cmsArticleDetails" %>
<%@ Register TagPrefix="SM" TagName="ArticleGallery" Src="~/_ctrl/module/gallery/product/cwpGalleryArticle.ascx"  %>

<% if (ParentPage.IsModeCMS ||  art.IsArticleOwner()){%>
   <%-- <a  class="btEditArticle" href="#" onclick="editArticleLoad('<%= art.CWP_ID.ToString() %>', <%= art.ART_ID %>); return false" ></a>--%>
    <a  class="btEditArticle" href="#" onclick="newBlogLoad('<%= art.CWP_ID.ToString() %>', <%= art.ART_ID %>, <%= art.ART_ID %>); return false" ></a>

<%} %>

<div id="divArticles" runat="server" >
    <div class="artBox">
        <div class="artText">
        
        
            
        
            <h1 class="articleH1"><%=  RenderArtTitle() %></h1>
            <%--<span class="artBox_date"><%=  RenderDatePublished()%></span>--%>
            <span class="artBox_date"><%=  RenderAdditionalInfo()%></span>


            <%--<div class="artAbstract"><p><%=  RenderArtDesc() %></p></div>--%> 
            
            <div class= "ArtDetailwthImg" <%= RenderGalleryStyle() %>>
                <SM:ArticleGallery  ID="objGalleryArticle" runat="server" />
               
                <%--<iframe width="560" height="315" src="http://www.youtube.com/embed/8cN6rh8jj-8?wmode=transparent" frameborder="0" allowfullscreen></iframe>--%>
            </div>

             <%= RenderVideo() %>
             <%--<iframe width="560" height="315" src="http://www.youtube.com/embed/8cN6rh8jj-8?wmode=transparent" frameborder="0" allowfullscreen></iframe>--%>
            
<%--            <div class="artBody">
--%>            
<div class="artBoxR">
            <p><%=  RenderArtDesc() %></p><%=  RenderArtBody() %>
</div>            
<%--            </div>
--%>
<div class="clearing"></div>
        </div>
        <div class="artImg"></div>
    </div>

</div>

