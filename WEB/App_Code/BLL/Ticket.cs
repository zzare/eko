﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

/// <summary>
/// Summary description for TICKET
/// </summary>
public partial class TICKET
{
    public static TICKET GetTicketByID(Guid id)
    {
        eMenikDataContext db = new eMenikDataContext();

        return  db.TICKETs.Where(w => w.TicketID  == id).SingleOrDefault();
    }
    public static TICKET GetTicketByIDByUser(Guid id, Guid user)
    {
        eMenikDataContext db = new eMenikDataContext();

        return db.TICKETs.Where(w => w.TicketID == id && w.UserId == user).SingleOrDefault();
    }

    public static IEnumerable<TICKET> GetTicketsByParent(Guid id)
    {
        eMenikDataContext db = new eMenikDataContext();

        return db.TICKETs.Where(w => w.ParentTicketID == id && w.Active == true );
    }



    public static TICKET CreateTicket(Guid ticketID, Guid user, string title, string desc, short type, int status, int statusType, Guid parentID, short priority, bool active ) {
        eMenikDataContext db = new eMenikDataContext();
        TICKET t = new TICKET();
        t.TicketID = ticketID;
        t.UserId = user;
        t.Title = title;
        t.Desc = desc;
        t.TicketTypeID = type;
        t.StatusID = status;
        t.StatusTypeID = statusType;
        if (parentID != Guid.Empty )
            t.ParentTicketID = parentID;
        else
            t.ParentTicketID = null;
        t.Priority = priority;
        t.Active = active;
        t.DateAdded = DateTime.Now;

        db.TICKETs.InsertOnSubmit(t);
        db.SubmitChanges();

        return t;
    }


    public static void  UpdateTicketStatus(Guid id, Guid user, int status)
    {
        eMenikDataContext db = new eMenikDataContext();

        TICKET t = (from tic in db.TICKETs where tic.TicketID  == id && tic.UserId == user select tic).SingleOrDefault();
        t.StatusID = status;
        db.SubmitChanges();

    }



    public class Common {
        public class Priority
        {
            public const short ORDER_ITEM = 20;

        }
        public class Type
        {
            public const short ORDER = 1;
            public const short ORDER_DOMAIN = 2;

        }

        public static string RenderTicketRef(object refID)
        {
            int code = int.Parse(refID.ToString());

            return code.ToString("000000");
        }
        
        public static string RenderTicketStatus(int statusID)
        {

            STATUS stat = STATUS.GetStatusByID(statusID);
            if (stat == null)
                return "";

            return stat.Title;
        }


        public static string GetTicketDetailsCMSHref(Guid o)
        {
            string ret = "~/cms/emenik/ticket/ManageTicket.aspx?t=" + o.ToString();

            return ret;
        }
    
    
    }

    //partial void OnValidate(System.Data.Linq.ChangeAction action)
    //{
    //}

}
