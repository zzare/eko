IF EXISTS (SELECT * FROM sysobjects WHERE name='em_GetUserCultures' and type='P')
DROP Procedure em_GetUserCultures

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		zare
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].em_GetUserCultures
	-- Add the parameters for the stored procedure here

	@UserId         uniqueidentifier

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON
	
	
SELECT *

  FROM vw_UserCulture
 WHERE UserId = @UserId
ORDER BY [DEFAULT] DESC, SORT ASC


END
GO
