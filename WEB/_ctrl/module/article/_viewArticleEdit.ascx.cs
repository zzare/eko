﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _ctrl_module_article_viewArticleEdit : BaseControlView
{

    public object Data;
    public object[] Parameters;
    public ARTICLE  Article;
    //protected int ArtID = -1;
    //public SITEMAP_LANG SitemapLang;

    //protected List<PRODUCT_GROUP_ATTRIBUTE> ProductGroupAttList;
    //protected List<SITEMAP_LANG > ProductSmapList;


    protected void Page_Load(object sender, EventArgs e)
    {
        LoadData();
    }



    public void LoadData()
    {



        if (Data  == null)
            Article = new ARTICLE { ART_TITLE="", ART_BODY = "", ACTIVE = true  };
        else
            Article = (ARTICLE)Data;



        if (Parameters != null)
        {
            //if (Parameters[0] != null)
            //    SitemapLang = (SITEMAP_LANG)Parameters[0];

            //if (Parameters.Length > 1 && Parameters[1] != null)
            //    LangID = Parameters[1].ToString();
        }



        fckBody.Config["FloatingPanelsZIndex"] = "200000";
        fckBody.Value = Server.HtmlDecode(Article.ART_BODY);

        cbComments.Checked = Article.COMMENTS_ENABLED;
        //cbApproved.Checked = Article.APPROVED;
        cbExposed.Checked = Article.EXPOSED ;
        cbSortWeight.Checked = Article.SortWeight > 0;

        




        //// load attributes
        //ProductRep prep = new ProductRep();
        //ProductGroupAttList = prep.GetProductGroupAttributesByProduct(Prod.PRODUCT_ID).ToList();
        
        //lvListAttr.DataSource = prep.GetGroupAttributesByPage(Prod.PageId);
        //lvListAttr.DataBind();

        //// load additional categories
        //ProductSmapList = prep.GetProductSitemapLangs(Prod.PRODUCT_ID, SitemapLang.LANG_ID).ToList();
        //lvProductSmap.DataSource = prep.GetSitemapLangsAdditionalByPM(Prod.PageId, PAGE_MODULE.Common.PRODUCT_ADDITIONAL , SitemapLang.LANG_ID);
        //lvProductSmap.DataBind();



    }

    //protected string RenderChecked( object o) {
    //    string ret = "";

    //    if (ProductGroupAttList == null)
    //        return ret;

    //    GROUP_ATTRIBUTE ga = (GROUP_ATTRIBUTE )o;

    //    if (ProductGroupAttList.Exists(w => w.GroupAttID == ga.GroupAttID))
    //        ret = " checked ";

    //    return ret;    
    
    //}


    //protected string RenderCheckedProdSmap(object o)
    //{
    //    string ret = "";
    //    if (ProductSmapList == null)
    //        return ret;

    //    SITEMAP_LANG ga = (SITEMAP_LANG)o;

    //    if (ProductSmapList.Exists(w => w.SMAP_ID  == ga.SMAP_ID ))
    //        ret = " checked ";

    //    return ret;
    //}


}
