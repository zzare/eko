﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _ctrl_module_file_edit_viewFileEditProduct: BaseControlView
{

    //public object Data;
    //public object[] Parameters;
    public Guid ProductID;
    //public IQueryable<FILE> Files;
    public List<vw_UserCulture> UserCultures;


    protected void Page_Load(object sender, EventArgs e)
    {


        LoadData();
    }



    public void LoadData()
    {



        if (Data  == null)
            return;
        UserCultures = (List<vw_UserCulture>)Data;

        if (Parameters != null)
        {
            //if (Parameters[0] != null)
            //    UserCultures = (List<vw_UserCulture>)Parameters[0];

            if (Parameters.Length > 0 && Parameters[0] != null)
                ProductID = new Guid(Parameters[0].ToString());
        }

        // language
        lvListHeader.DataSource = UserCultures;
        lvListHeader.DataBind();

        // file lang
        lvListDetail.DataSource = UserCultures ;
        lvListDetail.DataBind();  
    }


    protected string RenderItem(object o) { 
        vw_UserCulture item = (vw_UserCulture)o;

        string ret = "";

        FileRep frep = new FileRep();

        var data = frep.GetFileListByProductLang(ProductID, item.LANG_ID);
        ret = ViewManager.RenderView("~/_ctrl/module/file/edit/_viewFileEditList.ascx", data, new object[] { ProductID , item.LANG_ID, FILE.Common.FileType.PRODUCT   });

        return ret;
    }





}
