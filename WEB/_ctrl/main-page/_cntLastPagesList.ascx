﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_cntLastPagesList.ascx.cs" Inherits="_ctrl_main_page_cntLastPagesList" %>


<asp:ListView ID="lvList" runat="server">

    <LayoutTemplate>
        <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
    </LayoutTemplate>

    <ItemTemplate>
        <a target="_blank" href='<%# Page.ResolveUrl( SM.EM.Rewrite.EmenikUserUrl(Container.DataItem as EM_USER)) %>'><%#  (Eval("PAGE_TITLE") != null && Eval("PAGE_TITLE") != "") ? Eval("PAGE_TITLE") + " &rsaquo;" : Eval("PAGE_NAME") + "& rsaquo;"%></a>
            
    </ItemTemplate>
    
    <ItemSeparatorTemplate>
        <div style="line-height:0px; font-size:0px; min-height:5px; height:auto !important; height:5px; ">&nbsp;</div>
    </ItemSeparatorTemplate>

</asp:ListView>