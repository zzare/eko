﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _ctrl_module_product_viewProductListRelated : BaseControlView
{

    public object Data;
    public object[] Parameters;
    public string LangID;
    public bool ShowCategories = false;


    protected void Page_Load(object sender, EventArgs e)
    {
        LoadList();
    }



    public void LoadList() {
         this.Visible = false;

        if (Data == null)
            return;

        if (Parameters != null)
        {
            if (Parameters[0] != null)
                IsModeCMS = bool.Parse(Parameters[0].ToString());
            if (Parameters.Length > 1 && Parameters[1] != null)
                LangID = Parameters[1].ToString();
        }


        // load list
        objProductList.ShowCategories = ShowCategories;
        objProductList.Data = Data;
        objProductList.LoadList();

        // hide if empty
        if (objProductList.GetItemCount > 0)
            this.Visible = true;
           


    }



}
