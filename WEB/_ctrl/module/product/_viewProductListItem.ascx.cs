﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _ctrl_module_product_viewProductListItem : BaseControlView
{

    public object Data;
    public object[] Parameters;
    public vw_Product Prod;



    protected void Page_Load(object sender, EventArgs e)
    {
        LoadList();
        //string test = LANGUAGE.GetCurrentCulture();
    }



    public void LoadList() {

        if (Data == null)
            return;
        Prod = Data as vw_Product;

        if (Parameters != null)
        {
            if (Parameters[0] != null)
                IsModeCMS = bool.Parse(Parameters[0].ToString());
            //if (Parameters.Length > 1 && Parameters[1] != null)
            //    LangID = Parameters[1].ToString();
        }

        //objRating.AvgRating = (float)Prod.AvgRating;
        //objRating.Number = 5;


    }

    protected string RenderItemTitle()
    {
     
        string ret = "";
        string css = "";
        string title = "";


        if (Prod.CAT_ACTION)
        {
            title = GetGlobalResourceObject("Product", "actionOffer").ToString();
            css = "actionS";
        }

        if (!IsModeCMS)
            return ret;

        if (!Prod.ACTIVE)
        {
            title = "NEAKTIVEN ARTIKEL";
            css = "inactiveS";
        }
        else if (Prod.UNITS_IN_STOCK <= 0)
        {
            title = "NI NA ZALOGI";
            css = "nStockS";
        }

        ret = string.Format("<span class=\"{0}\">{1}</span>", css, title );


        return ret;
    }




}
