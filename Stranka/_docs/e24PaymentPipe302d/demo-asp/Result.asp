<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<HTML>
<HEAD>
<TITLE>Colors of Success</TITLE>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<META name="GENERATOR" content="IBM WebSphere Studio">
</HEAD>

<BODY>
<!--#INCLUDE FILE = "header.inc"-->
<CENTER>

<%
' get Merchant Notification parameters
Dim payID
payID = request.Querystring("PaymentID")

if IsEmpty(payID) Then
	response.Redirect("error.asp")
end if	

if ( request.Querystring("resultcode")="CAPTURED" or _
     request.Querystring("resultcode")="APPROVED" ) Then%>
	<CENTER>
		<FONT size="5" color="GREEN">
			Transaction completed successfully.<BR>
			Thank you for your order.
		</FONT>
	</CENTER>
<%  else  %>
		<FONT size="5" color="RED">
		    The transaction has been rejected.<BR>
			Please try again.
		</FONT>
		<P>
		<FONT color="BLUE"><A href="index.asp">Click here for Main Page</A></FONT>
<% end if %>

	<P><P>
	
  <TABLE>
    <TR> 
      <TD colspan="2" align="center"> <FONT size="4"><B>Transaction Details <br>
        (from Merchant Notification Message)</B></FONT> </TD>
    </TR>
    <TR> 
      <TD colspan="2" align="center"> <HR> </TD>
    </TR>
    <TR> 
      <TD width="40%">Track ID</TD>
      <TD><%=request.Querystring("TrackID")%></TD>
    </TR>
    <TR> 
      <TD>Auth Code</TD>
      <TD><%=request.Querystring("auth")%></TD>
    </TR>
    <TR> 
      <TD>Post Date</TD>
      <TD><%=request.Querystring("postdate")%></TD>
    </TR>
    <TR> 
      <TD>Result Code</TD>
      <TD><%=request.Querystring("resultcode")%></TD>
    </TR>
    <TR> 
      <TD>Payment ID</TD>
      <TD><%=request.Querystring("PaymentID")%></TD>
    </TR>
    <TR> 
      <TD>Transaction ID</TD>
      <TD><%=request.Querystring("TransID")%></TD>
    </TR>
  </TABLE>

</CENTER>

<!--#INCLUDE FILE = "footer.inc"-->

</BODY>
</HTML>

