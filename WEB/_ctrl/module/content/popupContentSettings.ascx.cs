﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SM.EM.BLL;
namespace SM.EM.UI.Controls
{
    public partial class popupContentSettings : CwpEdit
    {



        protected void Page_Load(object sender, EventArgs e)
        {

        }


        protected void InitData() {

        }

        public override void LoadData()
        {
            Show();
            objSettings.cwp = cwp;
            objSettings.CwpID = CwpID;
            objSettings.LoadData();
        }


        protected void btSaveEdit_Click(object sender, EventArgs e)
        {
            SaveData();

        }

        public void SaveData()
        {
           if (! objSettings.SaveData()) return;

           // reload zone
           ParentPage.ReLoadZone(ParentPage.Zones[Zone], true);

        }


        public void Show() {
            mpeContentSettings.Show();
            this.Visible = true;

        }
        public void Hide()
        {
            mpeContentSettings.Hide();
            this.Visible = false;
        }
    }
}
