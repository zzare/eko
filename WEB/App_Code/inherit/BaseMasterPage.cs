﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for BaseMasterPage
/// </summary>
namespace SM.EM.UI
{

    public abstract class BaseMasterPage : MasterPage
    {
        public abstract string StatusText{
            get; set;
        }
        public abstract string ErrorText{
            get; set;
        }

        //public int SelectedSitemapID = -1;
        public string MainSitemapProv = "";
        public string SubMenuSitemapProv = "";

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            if (!string.IsNullOrEmpty(MainSitemapProv))
            {
                SiteMapDataSource ds = (SiteMapDataSource)this.FindControl("smdsMain");
                if (ds != null)
                    ds.SiteMapProvider = MainSitemapProv;
            }
            if (!string.IsNullOrEmpty(SubMenuSitemapProv ))
            {
                SiteMapDataSource ds = (SiteMapDataSource)this.FindControl("smdsSub");
                if (ds != null)
                {
                    ds.SiteMapProvider = SubMenuSitemapProv;
                    ds.StartingNodeOffset = 0;
                }
            }


        }



        // set selected menu item
        public void SelectCurrentMenuItem(Menu m)
        {

            foreach (SiteMapProvider prov in SiteMap.Providers)
            {
                foreach (MenuItem item in m.Items)
                {
                    SelectCurrentMenuItem(item, m, prov);
                }
            }

        }
        public  void SelectCurrentMenuItem(Menu m, SiteMapProvider prov)
        {

            foreach (MenuItem item in m.Items)
            {
                SelectCurrentMenuItem(item, m, prov);
            }

        }
        protected void SelectCurrentMenuItem(MenuItem mi,   Menu m, SiteMapProvider   provider) {
            
            if (m.SelectedItem != null) return;
            
            if (provider.CurrentNode != null && mi.DataPath == provider.CurrentNode.Key ){
                mi.Selected = true;
                return;
            }
            if (mi.ChildItems == null)
                return;

            foreach (MenuItem item in mi.ChildItems) {
                    SelectCurrentMenuItem(item, m, provider);
            }
        }

        protected void SelectCurrentNodeTV(TreeView tv)
        {
            tv.CollapseAll();

            if (tv.SelectedNode == null) return;

            // expand only selected node
            tv.SelectedNode.Expand();
            // expand all the parent nodes
            ExpandAllParents(tv.SelectedNode);
        }

        // expand all the parent nodes
        protected void ExpandAllParents(TreeNode node)
        {
            if (node.Parent == null) return;

            node.Parent.Expand();
            ExpandAllParents(node.Parent);
        }
    }

}