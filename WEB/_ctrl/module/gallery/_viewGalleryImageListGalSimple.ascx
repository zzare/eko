﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewGalleryImageListGalSimple.ascx.cs" Inherits="SM.EM.UI.Controls._viewGalleryImageListGalSimple" %>
<%@ Register Namespace="SM.UI.Controls" TagPrefix="SM"  %>


<asp:ListView ID="lvList" runat="server" InsertItemPosition="None"  >
    <LayoutTemplate>
        <div id="itemPlaceholder" runat="server"></div>
            
    </LayoutTemplate>
    
    <ItemTemplate>
        <a href="javascript:void(0)" class='<%# ItemClass %>' title='<%# Eval("IMG_TITLE")  %>' onclick='<%# GALLERY.Common.RenderOpenGalleryPopupAnimal(GalCwpID, Container.DataItemIndex + GalleryOffset, "this" ) %>' ><img alt='<%# Eval("IMG_TITLE") + " | " + Eval("IMG_DESC") %>' id="Img2" src='<%# SM.BLL.Common.ResolveUrl (Eval("IMG_URL").ToString()) %>'  /></a>
    </ItemTemplate>
</asp:ListView>
