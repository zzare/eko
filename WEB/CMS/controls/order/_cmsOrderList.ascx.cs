﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;


namespace SM.UI.Controls
{
    public partial class _cmsOrderList : BaseControl 
    {
        public delegate void OnEditEventHandler(object sender, SM.BLL.Common.Args .IDguidEventArgs e);
        public event OnEditEventHandler OnEdit;
        public event OnEditEventHandler OnDelete;
        public string OnEditTarget = "_self";
        public int PageSize = 20;

        public string SearchText { get; set; }
        public bool? Active { get; set; }
        public short? Status {get; set;}
        public bool IsAdmin { get; set; }

        
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        protected void ldsList_OnSelecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            eMenikDataContext db = new eMenikDataContext();

            var list = from l in db.SHOPPING_ORDERs select l;

                // apply filters
                if (Active == true)
                    list = list.Where(w => !(w.OrderStatus >= SHOPPING_ORDER.Common.Status.NEW && w.OrderStatus <= SHOPPING_ORDER.Common.Status.STEP_4 ));
                else if (Active == false)
                    list = list.Where(w => (w.OrderStatus >= SHOPPING_ORDER.Common.Status.NEW && w.OrderStatus <= SHOPPING_ORDER.Common.Status.STEP_4));

            if (Status != null)
                    list = list.Where(w => w.OrderStatus == Status );


            //if (!String.IsNullOrEmpty(SearchText))
            //    list = list.Where(w => (w.OrderRefID.ToString().Contains(SearchText) || (w.EM_USER.USERNAME ).Contains(SearchText) ));



            // result
            e.Result = list.OrderByDescending(o => o.DateStart  );
        }


        public void BindData()
        {
            lvList.DataBind();


        }




    }


}