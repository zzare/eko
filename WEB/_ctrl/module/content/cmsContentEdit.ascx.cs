﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _ctrl_module_content_cmsContentEdit : SM.EM.UI.Controls.CwpEdit  
{
    //public Guid  CwpID
    //{
    //    get { return (ViewState["CwpID"] == null   ? Guid.Empty : new Guid( ViewState["CwpID"].ToString())); }
    //    set { ViewState["CwpID"] = value; }
    //}
    //public string Zone {
    //    get { return (string) (ViewState["Zone"] ?? ""); }
    //    set { ViewState["Zone"] = value; }        
    //}

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            fckBody.Config["FloatingPanelsZIndex"] = "200000";

    }

    public override void LoadData()
    {
        if (cwp == null) return;
        CwpID = cwp.CWP_ID;
        Zone = cwp.ZONE_ID;
        CONTENT con = CONTENT.GetContentByCwpID(cwp.CWP_ID);
        fckBody.Value = Server.HtmlDecode(con.CONT_BODY );

        mpeEditContent.Show();
        this.Visible = true;
    }

    protected void btSaveEdit_Click(object sender, EventArgs e)
    {

        if (!ParentPage.CanSave) return;


//        CONTENT.UpdateBody(CwpID, Server.HtmlEncode(fckBody.Value));
        CONTENT.UpdateBody(CwpID, fckBody.Value);
        //            objContent.BindData();
        //LoadData();
        mpeEditContent.Hide();
        this.Visible = false ;

        // reload zone
        ParentPage.ReLoadZone(ParentPage.Zones[Zone], true);
    }
}
