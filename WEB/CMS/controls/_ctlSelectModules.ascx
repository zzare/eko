﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_ctlSelectModules.ascx.cs" Inherits="SM.EM.UI.CMS.Controls.CMS_ctlSelectModules" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<script type="text/javascript" >    
    
    function SetRoles(){
        var lookup = document.getElementById('<%= tbModules.ClientID %>');
        var cbl = document.getElementById('divModulesP').getElementsByTagName('input');
        
        lookup.value = '';

        for (var i=0; i<cbl.length; i++){
            var cbx = cbl[i];
            if (cbx.type != 'checkbox')
                continue;
            
            if (cbx.checked){
                lookup.value += "," + cbx.nextSibling.innerHTML;
            }
        }
        if (lookup.value.length > 0)
            lookup.value = lookup.value.substring(1);
    }   
</script>     

<asp:TextBox ID="tbModules" runat="server"></asp:TextBox><asp:ImageButton ID="btSetValues" runat="server"  ImageUrl="~/_inc/images/button/lookup.jpg"  CausesValidation="false" />    
    
 <asp:Panel CssClass="modCMSPopup" ID="panPopup" runat="server" style="display:none;">
    <div id="divModulesP">
        <asp:CheckBoxList ID="cblModules" runat="server" AppendDataBoundItems="true">
        </asp:CheckBoxList>
    </div>
    <asp:Button ID="btOk" runat="server" Text="Save" CausesValidation="false"  />
    <asp:Button ID="btCancel" runat="server" Text="Cancel"  CausesValidation="false"/>
</asp:Panel>

<cc1:ModalPopupExtender RepositionMode="RepositionOnWindowResizeAndScroll" Y ="50" BehaviorID="mpePopup"  ID="mpePopup" runat="server" TargetControlID="btSetValues" PopupControlID="panPopup"
                        BackgroundCssClass="modCMSBcg" CancelControlID="btCancel" OkControlID="btOk" OnOkScript="SetRoles()" >
</cc1:ModalPopupExtender> 
            
