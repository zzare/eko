﻿<%@ Page Language="C#" ValidateRequest="false" MasterPageFile="~/_em/templates/fall/fall.master" AutoEventWireup="true" CodeFile="ArticleDetails.aspx.cs" Inherits="_em_content_ArticleDetails" Title="Untitled Page" %>
<%@ Register Namespace="SM.EM.UI.Controls" TagPrefix="EM"  %>
<%@ Register TagPrefix="EM" TagName="ModuleNew" Src="~/_ctrl/emenik/emModuleNew.ascx"  %>

<%@ Register TagPrefix="EM" TagName="EditModule" Src="~/_ctrl/module/_edit/editModule.ascx"  %>
<%@ Register TagPrefix="SM" TagName="ArticleDetails" Src="~/_ctrl/module/article/cmsArticleDetails.ascx"  %>

<%@ Register TagPrefix="SM" TagName="Comment" Src="~/_ctrl/module/comment/_cntCommentEdit.ascx"  %>
<%@ Register TagPrefix="SM" TagName="CommentList" Src="~/_ctrl/module/comment/_cntCommentList.ascx"  %>

<%@ Register TagPrefix="EM" TagName="SubMenu" Src="~/_em/templates/_custom/eko/_ctrl/_ctrlSubMenu.ascx"  %>
<%--<%@ Register TagPrefix="SM" TagName="RightAbout" Src="~/_em/templates/_custom/zavetisceljubljana/_ctrl/common/_viewRightColAbout.ascx"   %>--%>


<asp:Content ID="Content2" ContentPlaceHolderID="cph" Runat="Server">


    <EM:SubMenu ID="objSubMenu" runat="server" />

    <div class="articleOutW">

        <EM:EditModule ID="objEditModule" runat="server" />


<%--<div class="ArticleW">
    <div class="contWideGreyW">

        <div class="contWideGreyT"></div>
            <div class="contWideGreyM">
                <div class="contWideGreyMI">--%>
                <div class="art_padding ">
                
<%--                <div id="divEdit" runat="server" >
                    <asp:LinkButton ID="btEdit"  class="btEditArticle"  runat="server" onclick="btEdit_Click"></asp:LinkButton>
                </div>--%>  
                <SM:ArticleDetails ID="cmsArticleDetails" runat="server" />
                
                <br />
                </div>
                <div class="clearing"></div>
                <EM:ModuleNew ID="mnMain" runat="server" />

                <EM:CwpZone ID="wpz_art_1" runat="server">
                </EM:CwpZone>
                <div class="art_padding">
                    <%--<a class="btBackwthText" href='<%= Page.ResolveUrl(SM.EM.Rewrite.EmenikUserArticleArchiveUrl(PageName, CurrentSitemapID, cmsArticleDetails.art.CWP_ID.Value )) %>' ><%= GetGlobalResourceObject("Modules", "BackToList")%></a>    
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--%>
                    <%--<div class="artBackBtn">
                    <a class="btBackwthText" href='<%= RenderBackToPageHref() %>' ><%= GetGlobalResourceObject("Modules", "BackToPage")%></a>    
                    </div>
                    <br />--%>
                    
                    <asp:PlaceHolder ID="phComment" runat="server">
                        <span class="art_sepa"></span>
                        <h2 style="text-align:center; padding:5px 0px 40px 0px;"><%= GetGlobalResourceObject("Modules", "Comments") %> (<%= objCommentList.ComentsCount %>)</h2>
<div class="commentsL">

                        <p class="comments_p_head"><%= GetGlobalResourceObject("Modules", "CommentsAddText") %>:</p>
                        <SM:Comment ID="objComment" runat="server" />
</div>
<div class="commentsR">

                        <SM:CommentList ID="objCommentList" runat="server" />    
</div>            
<div class="clearing"></div>            
                    </asp:PlaceHolder>
                </div>
                    
<%--                </div>    
            </div>
        <div class="contWideGreyB"></div>
    </div>

</div>--%>





                                    <div class="clearing"></div>
                                    </div>                                    
    
</asp:Content>
