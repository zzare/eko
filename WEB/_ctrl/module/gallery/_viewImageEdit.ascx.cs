﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using SM.EM.BLL;
namespace SM.EM.UI.Controls
{
    public partial class _viewImageEdit : BaseControlView
    {

        public object Data;
        public object[] Parameters;

        public vw_Image Image;
        public const int MAX_IMAGE_WIDTH = 600;
        public EM_USER em_user;



        protected void Page_Load(object sender, EventArgs e)
        {

            BindData();
        }
        // bind data
        public void BindData()
        {

            if (Data == null && Image == null)
                return;

            // data
            if (Image == null)
                Image = (vw_Image)Data;
            if (Image == null)
                return;
            if (Parameters != null)
            {
                if (Parameters.Length > 0 && Parameters[0] != null)
                    em_user  = (EM_USER )(Parameters[0]);
                //if (Parameters.Length > 1 && Parameters[1] != null)
                //    CwpGallery = (CWP_GALLERY)Parameters[1];
                //if (Parameters.Length > 2 && Parameters[2] != null)
                //    ImageLangList = (IEnumerable<vw_ImageLang>)Parameters[2];
            }


            // image data
            imgMain.Src = SM.BLL.Common.ResolveUrl( Image.IMG_URL);
            cbActive.Checked = Image .IMG_ACTIVE;
            tbAuthor.Value  = Image .IMG_AUTHOR;

            
            // image descs
            List<vw_ImageDesc> list = IMAGE.GetImageDescsByIDEmenik(Image.IMG_ID , em_user.USERNAME  );
            lvList.DataSource = list;
            lvList.DataBind();  

        }

        protected override void Render(HtmlTextWriter writer)
        {
            if (Image == null)
                return;


            base.Render(writer);
        }




    }
}
