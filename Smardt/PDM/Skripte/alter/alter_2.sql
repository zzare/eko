alter table dbo.SITEMAP_LANG 
alter column   SML_META_DESC        nvarchar(4000)        null
go




/*==============================================================*/
/* Table: E24_TRANS                                             */
/*==============================================================*/
create table dbo.E24_TRANS (
   TransID              int                  identity,
   RefID                uniqueidentifier     not null,
   DateInit             datetime             not null,
   DateUpdate           datetime             null,
   DatePurchase         datetime             null,
   DateCancel           datetime             null,
   Status               smallint             not null,
   TxId                 nvarchar(256)        not null,
   Store                nvarchar(128)        not null,
   StatusURL            nvarchar(512)        not null,
   UpdateURL            nvarchar(512)        not null,
   ErrorURL             nvarchar(512)        not null,
   PaymentGateway       nvarchar(256)        not null,
   TestMode             bit                  not null,
   TxType               nvarchar(128)        not null,
   CustName             nvarchar(256)        not null,
   CustSurname          nvarchar(256)        not null,
   Amount               money                not null,
   Currency             nvarchar(128)        not null,
   RetSuccess           bit                  null,
   RetCode              nvarchar(128)        null,
   RetMsg               nvarchar(256)        null,
   StateType            nvarchar(128)        null,
   StateTimestamp       datetime             null,
   StateAmount          money                null,
   StateCurrency        nvarchar(128)        null,
   StateResultType      nvarchar(128)        null,
   StateResult          nvarchar(256)        null,
   Udf1                 nvarchar(256)        null,
   Udf2                 nvarchar(256)        null,
   Udf3                 nvarchar(256)        null,
   Udf4                 nvarchar(256)        null,
   Udf5                 nvarchar(256)        null,
   PaymentID            nvarchar(64)         null,
   TranID               nvarchar(64)         null,
   TrackID              nvarchar(256)        null,
   Ref                  nvarchar(256)        null,
   CardType             nvarchar(64)         null,
   PayInst              nvarchar(64)         null,
   Liability            nvarchar(16)         null,
   Result               nvarchar(64)         null,
   Auth                 nvarchar(64)         null,
   PostDate             nvarchar(64)         null
)
go

alter table dbo.E24_TRANS
   add constraint PK_E24_TRANS primary key (TransID)
go

/*==============================================================*/
/* Index: X_REF_ID                                              */
/*==============================================================*/
create index X_REF_ID on dbo.E24_TRANS (
RefID ASC
)
go

/*==============================================================*/
/* Index: X_DATE_INIT                                           */
/*==============================================================*/
create index X_DATE_INIT on dbo.E24_TRANS (
DateInit ASC
)
go

/*==============================================================*/
/* Index: X_PAYMENT                                             */
/*==============================================================*/
create index X_PAYMENT on dbo.E24_TRANS (
PaymentID ASC
)
go



alter table dbo.SHOPPING_ORDER 
add   
   TransIDe24           int                  null
go

alter table dbo.SHOPPING_ORDER
   add constraint FK_SHOPPING__E24_TRANS foreign key (TransIDe24)
      references dbo.E24_TRANS (TransID)
go






alter table dbo.PRODUCT_ATTRIBUTE 
add
   Code                 nvarchar(256)        null,
   UnitsInStock         int                  null,
   ImgID                int                  null
   
   go

alter table dbo.CART_ITEM 
add
   Code                 nvarchar(256)        null,
   UnitsInStock         int                  null,
   ImgID                int                  null   
   go
   
   
   ------
   
/*==============================================================*/
/* Table: ARTICLE_SMAP                                          */
/*==============================================================*/
create table dbo.ARTICLE_SMAP (
   ART_ID               int                  not null,
   SMAP_ID              int                  not null,
   LANG_ID              nchar(2)             not null,
   Ordr                 int                  not null default 0
)
go

alter table dbo.ARTICLE_SMAP
   add constraint PK_ARTICLE_SMAP primary key (ART_ID, SMAP_ID, LANG_ID)
go

/*==============================================================*/
/* Index: X_ORDER                                               */
/*==============================================================*/
create index X_ORDER on dbo.ARTICLE_SMAP (
Ordr ASC
)
go

alter table dbo.ARTICLE_SMAP
   add constraint FK_ARTICLE___ARTICLE_SMAP foreign key (ART_ID)
      references dbo.ARTICLE (ART_ID)
go

alter table dbo.ARTICLE_SMAP
   add constraint FK_ARTICLE___LANGUAGE foreign key (LANG_ID)
      references dbo.LANGUAGE (LANG_ID)
go

alter table dbo.ARTICLE_SMAP
   add constraint FK_ARTICLE___SITEMAP foreign key (SMAP_ID)
      references dbo.SITEMAP (SMAP_ID)
go
   
   
   -- ta del �e spu��en na sm_EKO, test_EKO, test_EKO2
   
alter table dbo.PRODUCT 
add
   PriceRangeMax        money                null
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.vw_Product')
            and   type = 'V')
   drop view dbo.vw_Product
go

/*==============================================================*/
/* View: vw_Product                                             */
/*==============================================================*/
create view dbo.vw_Product as
SELECT P.[PRODUCT_ID]
      ,P.[ID_INT]
      ,P.[PageId]
      ,P.[SMAP_ID]
      ,P.PMOD_ID
      ,P.[CODE]
      ,P.[PRICE]
      ,P.PRICE_NO_TAX
      ,P.TAX_RATE
      ,P.[DISCOUNT_PERCENT]
      ,P.[SPECIAL_DISCOUNT_PERCENT]
      ,P.[UNITS_IN_STOCK]
      ,P.[ADDED_BY]
      ,P.[ADDED_DATE]
      ,P.[DATE_MODIFIED]
      ,P.[ACTIVE]
      ,P.DELETED
      ,P.[PRODUCT_URL]
      ,P.[WARRANTY]
      ,P.[TOTAL_RATING]
      ,P.[VOTES]
      ,P.[IMPORTANCE]
      ,P.[PRODUCT_TYPE]
      ,P.[CAT_ACTION]
      ,P.[CAT_SALE]
      ,P.PriceRangeMax
      ,p.MainImgID
      ,PD.LANG_ID
      ,PD.NAME
      ,PD.DESCRIPTION_LONG
      ,PD.DESCRIPTION_SHORT
      ,PD.PDescID
      ,I.IMG_URL
      ,I.IMG_URL as IMG_URL_BIG
      ,ID.IMG_DESC
      ,ID.IMG_TITLE
      ,I.IMG_ID
      ,I.TYPE_ID
      ,P.PRODUCT_ID as CWP_GAL
      ,v.AvgRating
      ,p.ManufacturerID
      ,m.title as ManufacturerTitle

from PRODUCT P, PRODUCT_DESC PD, IMAGE I, IMAGE_LANG ID, vote v, manufacturer m
where P.PRODUCT_ID = PD.PRODUCT_ID AND I.TYPE_ID = 1 AND ID.IMG_ID = 1 AND ID.LANG_ID = 'si'
and v.VoteID = p.product_id and m.manufacturerid = p.manufacturerid
go
-------------------------------   