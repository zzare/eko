﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_cmsErrorList.ascx.cs" Inherits="SM.UI.Controls._cmsErrorList" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Namespace="SM.UI.Controls" TagPrefix="SM" %>

<div class="qContList">

        <asp:LinqDataSource ID="ldsList" runat="server" OnSelecting="ldsList_OnSelecting" >
        </asp:LinqDataSource>
        
        <SM:smListView ID="lvList" runat="server" DataSourceID="ldsList" InsertItemPosition="None"  >
            <LayoutTemplate>
                <table  class="tblData" cellpadding="0" cellspacing="0">
                        <thead>
                            <tr id="headerSort" runat="server">
                                <th ><asp:LinkButton ID="LinkButton2" CommandName="sort" CommandArgument="DateError" runat="server">Date</asp:LinkButton></th>
                                <th ><asp:LinkButton ID="LinkButton1" CommandName="sort" CommandArgument="Message" runat="server">Message</asp:LinkButton></th>
                                <th ><asp:LinkButton ID="LinkButton3" CommandName="sort" CommandArgument="Username" runat="server">Username</asp:LinkButton></th>
                                <th ><asp:LinkButton ID="LinkButton4" CommandName="sort" CommandArgument="IP" runat="server">IP</asp:LinkButton></th>
                                <th width="100px" colspan="2" runat="server"  id="headManage" >Uredi</th>
                            </tr>
                        </thead>
                        <tbody id="itemPlaceholder" runat="server"></tbody>
                        <tfoot>
                            <tr>
                                <th style="text-align:right" colspan="8"  >
                                    <asp:DataPager runat="server" ID="DataPager" PageSize="40" >
                                        <Fields>
                                            <asp:NextPreviousPagerField  />
                                            <asp:NumericPagerField ButtonCount="5" />
                                        </Fields>
                                    </asp:DataPager>
                                </th>
                            </tr>
                        </tfoot>
                    </table>
            </LayoutTemplate>
            
            <ItemTemplate>
                <tr  class='<%#  Container.DataItemIndex % 2 == 0 ? "  " : " odd " %>'  onmouseover ="this.className='<%#  Container.DataItemIndex % 2 == 0 ? "hovRow" : "hovRowodd" %>'" onmouseout ="this.className = GetCssClass(<%#  Container.DataItemIndex %>)">
                    <td>
                        <%#  DateTime.Parse(Eval("DateError").ToString()).ToString () %>
                    </td>
                    <td>
                        <%# Eval("Message")%>
                    </td>
                    <td>
                        <%# Eval("Username")%>
                    </td>                    
                    <td>
                        <%# Eval("IP")%>
                    </td>
                    <td width="50px" runat="server"  >
                        <a href='<%# RenderDetailHref((int) Eval("ErrorID") ) %>'>details...</a>
                    </td>
                </tr>  
            
            </ItemTemplate>
            
            <EmptyDataTemplate>
                
                <div class="odd">
                    ni napak
            
                </div>
                
            </EmptyDataTemplate>
        </SM:smListView>

</div>
