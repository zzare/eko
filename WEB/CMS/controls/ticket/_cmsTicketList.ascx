﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_cmsTicketList.ascx.cs" Inherits="SM.UI.Controls._cmsTicketList" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Namespace="SM.UI.Controls" TagPrefix="SM" %>


<div class="qContList">

        <asp:LinqDataSource ID="ldsList" runat="server" OnSelecting="ldsList_OnSelecting" >
        </asp:LinqDataSource>
        
        <SM:smListView ID="lvList" runat="server" DataSourceID="ldsList"             
            InsertItemPosition="None" onitemdatabound="lvList_ItemDataBound"  >
            <LayoutTemplate>
                <table  class="tblData" cellpadding="0" cellspacing="0">
                        <thead>
                            <tr id="headerSort" runat="server">
                                <th ><asp:LinkButton ID="LinkButton5" CommandName="sort" CommandArgument="DateAdded" runat="server">Datum</asp:LinkButton></th>
                                <th ><asp:LinkButton ID="LinkButton2" CommandName="sort" CommandArgument="TicketRefID" runat="server">Št.</asp:LinkButton></th>
                                <th ><asp:LinkButton ID="LinkButton6" CommandName="sort" CommandArgument="Title" runat="server">Naziv</asp:LinkButton></th>
                                <th ><asp:LinkButton ID="LinkButton1" CommandName="sort" CommandArgument="UserId" runat="server">Uporabnik</asp:LinkButton></th>
                                <th ><asp:LinkButton ID="LinkButton3" CommandName="sort" CommandArgument="StatusID" runat="server">Status</asp:LinkButton></th>
                                <th ><asp:LinkButton ID="LinkButton4" CommandName="sort" CommandArgument="DateEnd" runat="server">Datum zaključka</asp:LinkButton></th>
                                <th width="100px" colspan="2" runat="server"  id="headManage" >Uredi</th>
                            </tr>
                        </thead>
                        <tbody id="itemPlaceholder" runat="server"></tbody>
                        <tfoot>
                            <tr>
                                <th style="text-align:right" colspan="8" >
                                    <asp:DataPager runat="server" ID="DataPager" PageSize="30" >
                                        <Fields>
                                            <asp:NextPreviousPagerField  />
                                            <asp:NumericPagerField ButtonCount="5" />
                                        </Fields>
                                    </asp:DataPager>
                                </th>
                            </tr>
                        </tfoot>
                    </table>
            </LayoutTemplate>
            
            <ItemTemplate>
                <tr  class='<%#  Container.DataItemIndex % 2 == 0 ? "  " : " odd " %>'  onmouseover ="this.className='<%#  Container.DataItemIndex % 2 == 0 ? "hovRow" : "hovRowodd" %>'" onmouseout ="this.className = GetCssClass(<%#  Container.DataItemIndex %>)">
                    <td>
                        <%# SM.EM.Helpers.FormatDate(Eval("DateAdded"))%>
                    </td>
                    <td>
                        <%# TICKET.Common.RenderTicketRef(Eval("TicketRefID"))%>
                    </td>
                    <td>
                        <%# Eval("Title")%>
                    </td>                    
                    <td>
                        <a target='<%# OnEditTarget %>' href='<%# Page.ResolveUrl( EM_USER.Common.RenderCMSHref(new Guid(Eval("UserId").ToString()))) %>' title="edit user" ><%# Eval("EM_USER.USERNAME") %></a>
                    </td>
                    <td>
                        <%# RenderStatus(Container.DataItem )%>
                    </td>
                    <td>
                        <%# SM.EM.Helpers.FormatDate(Eval("DateEnd"))%>
                    </td>
                    <td width="50px" runat="server"  >
                        <a target='<%# OnEditTarget %>' href='<%# Page.ResolveUrl( TICKET.Common.GetTicketDetailsCMSHref(new Guid(Eval("TicketID").ToString()))) %>' title="edit ticket" >uredi</a>
                    </td>
                </tr>
                
                
                <SM:smListView ID="lvListSub" runat="server" InsertItemPosition="None"  >
                    <LayoutTemplate>
                            <asp:PlaceHolder id="itemPlaceholder" runat="server"></asp:PlaceHolder>
                    </LayoutTemplate>
                    
                    <ItemTemplate>
                        <tr  onmouseover ="this.className='<%#  Container.DataItemIndex % 2 == 0 ? "hovRow" : "hovRowodd" %>'" onmouseout ="this.className = ''">
                
                            <td>
                                &nbsp; -> podlistek
                            </td>
                            <td>
                                <%# TICKET.Common.RenderTicketRef(Eval("TicketRefID"))%>
                            </td>
                            <td colspan="2">
                                <%# Eval("Title")%>
                            </td>
                            <td>
                                <%# RenderStatus(Container.DataItem )%>
                            </td>
                            <td>
                                <%# SM.EM.Helpers.FormatDate(Eval("DateEnd"))%>
                            </td>
                            <td id="Td1" width="50px" runat="server"  >
                                <a target='<%# OnEditTarget %>' href='<%# Page.ResolveUrl( TICKET.Common.GetTicketDetailsCMSHref(new Guid(Eval("TicketID").ToString()))) %>' title="edit ticket" >uredi</a>
                            </td>
                        </tr>
                    </ItemTemplate>
                    
                    <EmptyDataTemplate>
                    </EmptyDataTemplate>
                    
                
                </SM:smListView>                
                
                
                  
            
            </ItemTemplate>
            
            <EmptyDataTemplate>
                
                <div class="odd">
                    Seznam je prazen.
            
                </div>
                
            </EmptyDataTemplate>
            
        
        </SM:smListView>

</div>
