﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using System.Xml;
using System.Globalization;
using System.Web;

/// <summary>
/// Summary description for Security
/// </summary>
namespace SM.EM
{

    public class Security
    {
        public class Permissions { 
        
            public static bool IsAdvancedCms(){
                HttpContext context = HttpContext.Current;
                if (context == null)
                    return false;

                if (!context.User.Identity.IsAuthenticated)
                    return false;

                if (context.User.IsInRole("usr_custom_cms"))
                    return true;
                if (context.User.IsInRole("admin"))
                    return true;

                if (context.Session != null)
                    if (context.Session["is_admin"] != null)
                        if (bool.Parse(context.Session["is_admin"].ToString()) == true)
                            return true;

                return false;
            }


            public static bool IsAdmin()
            {
                HttpContext context = HttpContext.Current;
                if (context == null)
                    return false;

                if (!context.User.Identity.IsAuthenticated)
                    return false;

                if (context.User.IsInRole("admin"))
                    return true;

                if (context.Session != null)
                    if (context.Session["is_admin"] != null)
                        if (bool.Parse(context.Session["is_admin"].ToString()) == true)
                            return true;

                return false;
            }

            public static bool IsPortalAdmin()
            {
                HttpContext context = HttpContext.Current;
                if (context == null)
                    return false;

                if (!context.User.Identity.IsAuthenticated)
                    return false;

                if (context.User.IsInRole("admin"))
                    return true;

                if (context.User.IsInRole("portal_admin"))
                    return true;

                if (context.Session != null)
                    if (context.Session["is_admin"] != null)
                        if (bool.Parse(context.Session["is_admin"].ToString()) == true)
                            return true;

                return false;
            }


            public static bool IsAnimalAdmin()
            {
                HttpContext context = HttpContext.Current;
                if (context == null)
                    return false;

                if (!context.User.Identity.IsAuthenticated)
                    return false;

                if (context.User.IsInRole("admin"))
                    return true;

                if (context.User.IsInRole("animal_admin"))
                    return true;

                if (context.Session != null)
                    if (context.Session["is_admin"] != null)
                        if (bool.Parse(context.Session["is_admin"].ToString()) == true)
                            return true;

                return false;
            }

            public static bool IsCMSeditor()
            {
                HttpContext context = HttpContext.Current;
                if (context == null)
                    return false;

                if (!context.User.Identity.IsAuthenticated)
                    return false;

                if (context.User.IsInRole("admin"))
                    return true;

                if (IsPortalAdmin())
                    return true;

                if (context.User.IsInRole("is_editor_all"))
                    return true;


                if (context.Session != null)
                    if (context.Session["is_admin"] != null)
                        if (bool.Parse(context.Session["is_admin"].ToString()) == true)
                            return true;

                return false;
            }



            public static bool CanAddBlog()
            {
                HttpContext context = HttpContext.Current;
                if (context == null)
                    return false;

                if (!context.User.Identity.IsAuthenticated)
                    return false;

                return true;
            }
        
        
        }








        public class Login {

            public static void LogIn(string username, bool persistent, string returnUrl)
            {
                LogIn(username, persistent, returnUrl, false);
            }

            public static void LogIn(string username, bool persistent, string returnUrl, bool isAdmin)
            {

                FormsAuthentication.SetAuthCookie(username, persistent);
                EM_USER usr = EM_USER.GetUserByUserName(username);


                // if user has custom domain, create cookies for both domains
                if (usr.CUSTOM_DOMAIN != null && !String.IsNullOrEmpty(usr.CUSTOM_DOMAIN) && usr.CUSTOM_DOMAIN_ACTIVATED)
                {

                    // get session guid
                    Guid id = Guid.NewGuid();

                    // store username and remember me CB value in cache
                    SetLoginParams(id.ToString(), usr.USERNAME, persistent, isAdmin); 
                    //HttpContext.Current.Cache.Insert(id + "username", usr.USERNAME, null, DateTime.Now.AddSeconds(60), TimeSpan.Zero);
                    //HttpContext.Current.Cache.Insert(id + "permanent", persistent, null, DateTime.Now.AddSeconds(60), TimeSpan.Zero);

                    // redirect to login page to autenticate
                    HttpContext.Current.Response.Redirect(SM.EM.Rewrite.EmenikUserUrlLogin(usr.CUSTOM_DOMAIN, id.ToString(), returnUrl));

                }

                // add cookie
                //Response.Cookies.Add(cookie);
                else if (SM.BLL.Common.Emenik.Data.EnableSubDomainUsers())
                {

                    // get session guid
                    Guid id = Guid.NewGuid();

                    // store username and remember me CB value in cache
                    SetLoginParams(id.ToString(), usr.USERNAME, persistent, isAdmin); 
                    //HttpContext.Current.Cache.Insert(id + "username", usr.USERNAME, null, DateTime.Now.AddSeconds(60), TimeSpan.Zero);
                    //HttpContext.Current.Cache.Insert(id + "permanent", persistent, null, DateTime.Now.AddSeconds(60), TimeSpan.Zero);

                    HttpContext.Current.Response.Redirect(SM.EM.Rewrite.EmenikUserUrlLoginSubdomain(usr.PAGE_NAME, id.ToString(), returnUrl));

                }

                // redirect if specified
                if (returnUrl != string.Empty)
                    HttpContext.Current.Response.Redirect(HttpUtility.UrlDecode(returnUrl));

                // redirect to default user page
                HttpContext.Current.Response.Redirect(SM.EM.Rewrite.EmenikUserUrl(usr.PAGE_NAME));

            }

            public static void LogOut() {

                if (HttpContext.Current != null)
                    if (HttpContext.Current.Session != null)
                        if (HttpContext.Current.Session["is_admin"] != null)
                            HttpContext.Current.Session.Remove("is_admin");



                //HttpCookie cookie = HttpContext.Current.Response.Cookies[FormsAuthentication.FormsCookieName];
                //cookie.Expires = DateTime.Now.AddYears(-10);
                //cookie.Value = "";
                //HttpContext.Current.Response.Cookies.Add(cookie );
            }

            protected static void  SetLoginParams(string id, string username, bool persistent, bool isAdmin){
                int validTicket = 90;
                // store username and remember me CB value in cache + ip
                HttpContext.Current.Cache.Insert(id + "username", username, null, DateTime.Now.AddSeconds(validTicket ), TimeSpan.Zero);
                HttpContext.Current.Cache.Insert(id + "permanent", persistent, null, DateTime.Now.AddSeconds(validTicket ), TimeSpan.Zero);
                HttpContext.Current.Cache.Insert(id + "ip", HttpContext.Current.Request.UserHostAddress, null, DateTime.Now.AddSeconds(validTicket), TimeSpan.Zero);
                if (isAdmin )
                    HttpContext.Current.Cache.Insert(id + "session", "is_admin", null, DateTime.Now.AddSeconds(validTicket), TimeSpan.Zero);
                else
                    HttpContext.Current.Cache.Insert(id + "session", "", null, DateTime.Now.AddSeconds(validTicket), TimeSpan.Zero);
            }


            public static String RenderLoginView(HttpContext context)
            { return RenderLoginView(new Guid("90743629-0457-41F1-8937-06B0873CDCAA")); }

            public static string RenderLoginView(Guid pageid) {
                object data = pageid;
                object[] parameters = new object[] { };
                return RenderLoginView(data, parameters);
            
            }
            
            public static string  RenderLoginView(object data, object[] parms){
                return ViewManager.RenderView("~/_ctrl/login/_viewLoginView.ascx", data, parms);
            
            }


        
        
        }



        public const int MAX_WS_REQUEST_PER_MINUTE = 100; // todo:popravi

        public static bool IsValidRequest()
        {
            HttpContext context = HttpContext.Current;
            if (context.Request.Browser.Crawler) return false;

            string key = context.Request.UserHostAddress;
            int hits = (int) (context.Cache[key] ?? 0);

            if (hits > MAX_WS_REQUEST_PER_MINUTE ) return false;
            else hits++;

            if (hits == 1)
                context.Cache.Add(key, hits, null, DateTime.Now.AddMinutes(1),
                   System.Web.Caching.Cache.NoSlidingExpiration,
                   System.Web.Caching.CacheItemPriority.Normal, null);
            else // update cache 
                context.Cache[key] = hits;

            return true;
        }



        public class Encryption
        {


            public static string DeHex(string hexstring)
            {

                string ret = String.Empty;
                StringBuilder sb = new StringBuilder(hexstring.Length / 2);
                for (int i = 0; i <= hexstring.Length - 1; i = i + 2)
                {
                    sb.Append((char)int.Parse(hexstring.Substring(i, 2), NumberStyles.HexNumber));
                }

                return sb.ToString();
            }



            public static string Hex(string sData)
            {

                string temp = String.Empty; ;
                string newdata = String.Empty;
                StringBuilder sb = new StringBuilder(sData.Length * 2);
                for (int i = 0; i < sData.Length; i++)
                {
                    if ((sData.Length - (i + 1)) > 0)
                    {
                        temp = sData.Substring(i, 2);
                        if (temp == @"\n") newdata += "0A";
                        else if (temp == @"\b") newdata += "20";
                        else if (temp == @"\r") newdata += "0D";
                        else if (temp == @"\c") newdata += "2C";
                        else if (temp == @"\\") newdata += "5C";
                        else if (temp == @"\0") newdata += "00";
                        else if (temp == @"\t") newdata += "07";
                        else
                        {
                            sb.Append(String.Format("{0:X2}", (int)(sData.ToCharArray())[i]));
                            i--;
                        }
                    }
                    else
                    {
                        sb.Append(String.Format("{0:X2}", (int)(sData.ToCharArray())[i]));
                    }
                    i++;
                }
                return sb.ToString();
            }

        }


        public class Cryptography
        {

            #region Fields

            private static byte[] key = { };
            private static byte[] IV = { 38, 55, 206, 48, 28, 64, 20, 16 };
            private static string stringKey = "!5663a#KN";

            #endregion

            #region Public Methods
            public static string Encrypt(string text)
            {

                try
                {
                    key = Encoding.UTF8.GetBytes(stringKey.Substring(0, 8));

                    DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                    Byte[] byteArray = Encoding.UTF8.GetBytes(text);

                    MemoryStream memoryStream = new MemoryStream();
                    CryptoStream cryptoStream = new CryptoStream(memoryStream, des.CreateEncryptor(key, IV), CryptoStreamMode.Write);

                    cryptoStream.Write(byteArray, 0, byteArray.Length);
                    cryptoStream.FlushFinalBlock();

                    return Convert.ToBase64String(memoryStream.ToArray());
                }

                catch (Exception ex)
                {
                    // Handle Exception Here

                }



                return string.Empty;

            }



            public static string Decrypt(string text)
            {

                try
                {

                    key = Encoding.UTF8.GetBytes(stringKey.Substring(0, 8));

                    DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                    Byte[] byteArray = Convert.FromBase64String(text);

                    MemoryStream memoryStream = new MemoryStream();
                    CryptoStream cryptoStream = new CryptoStream(memoryStream, des.CreateDecryptor(key, IV), CryptoStreamMode.Write);

                    cryptoStream.Write(byteArray, 0, byteArray.Length);
                    cryptoStream.FlushFinalBlock();


                    return Encoding.UTF8.GetString(memoryStream.ToArray());
                }

                catch (Exception ex)
                {

                    // Handle Exception Here

                }
                return string.Empty;
            }

            #endregion

        }

    }
}
