﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using System.IO;
using System.Text;
using System.Threading;
using System.Globalization;


namespace SM.EM.UI
{
    public partial class _em_templates__custom_eko : BaseMasterPage_EMENIK
    {
        public static Guid PageId;

        public override string StatusText
        {
            get;
            set;
        }
        public override string ErrorText
        {
            get;
            set;
        }

        protected override void OnInit(EventArgs e)
        {

            base.OnInit(e);

            // require login
            //if (ppem.PageModule != -11) {
            //    if (!Page.User.Identity.IsAuthenticated)
            //    {
            //        ppem.RequestCustomerLoginLanding();
            //        return;
            //    }
            //}


            // register menu sitemap prividers
            ppem.RegSmpEmenik.Add(5);
            ppem.RegSmpEmenik.Add(7);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            // only in this example
            PageId = SM.BLL.Custom.MainUserID;

            // add hover jquery
            SM.EM.Helpers.RegisterScriptIncludeAJAX(SM.BLL.Common.Emenik.ResolveMainDomainUrl("~/Javascript/jQuery/jquery.hoverIntent.minified.js"), Page, "jHoverIntent");

            // add cycle
            SM.EM.Helpers.RegisterScriptIncludeAJAX(SM.BLL.Common.Emenik.ResolveMainDomainUrl("~/Javascript/CyclePlugin/jquery.cycle.all.min.js"), Page, "jqueryCycle");

            // watermark
            SM.EM.Helpers.RegisterScriptIncludeAJAX(SM.BLL.Common.Emenik.ResolveMainDomainUrl("~/JavaScript/jQuery/jquery.tinywatermark-3.1.0.js"), Page, "jqueryWatermark");

            // add hover jquery
            SM.EM.Helpers.RegisterScriptIncludeAJAX(SM.BLL.Common.Emenik.ResolveMainDomainUrl("~/javascript/jquery/jquery.cookie.js"), Page, "jCookie",true);

            // fancyopts
            SM.EM.Helpers.RegisterScriptIncludeAJAX(Page.ResolveUrl("~/Javascript/jquery.fancyopts-0.5.min.js"), Page, "fancyopts", false);


            // add CSS
            CSSMain.Href += "?" + Helpers.GetCssVersion();
            CSSMenu.Href += "?" + Helpers.GetCssVersion();
            CSSIEMenu6.Href += "?" + Helpers.GetCssVersion();
            //CSSTemplate.Href += "?" + Helpers.GetCssVersion();

            // bind sitemap provider data
            smdsMain.Provider = ppem.SmpEmenikList[5];
            //smdsSek.Provider = ppem.SmpEmenikList[6];
            //            smdsLeft.Provider = ppem.SmpEmenik;
            //objBreadCrumbs.Provider  = ppem.SmpEmenik;



            // init content
            if (ppem.PageModule == -11)
            {
                cMenuContainer.Visible = false;
                cHeaderMain.Visible = false;
                cFooterCF.Visible = false;
                cFooterSeparator.Visible = false;
                //cLoginMenu.Visible = true;
            }


            // init CF
            var c = CUSTOMER.GetCurrentCustomer();
            if (c == null)
            {
                c = new CUSTOMER { };
            }
            objContactForm.Cust = c;
            objContactFormEN.Cust = c;
            objContactFormIT.Cust = c;


        }

        //protected override void OnPreRender(EventArgs e)
        //{
        //    base.OnPreRender(e);

        //    // jCorner
        //    SM.EM.Helpers.RegisterScriptIncludeAJAX(Page.ResolveUrl("~/Javascript/jcorner/jquery.corner.js"), Page, "corner");

        //    // hide left menu container if empty
        //    //if (mLeft.Items.Count <= 0) {
        //    //    cMenuLeft.Visible = false;            
        //    //}

        //    //// hide entire left side if nothing in placeholder
        //    //if (mLeft.Items.Count == 0 && cphLeft.Controls.Count == 1) {
        //    //    cMainL.Visible = false;
        //    //}



        //}


        public override void LoadData()
        {





            // languages
           objLanguages.LoadLanguages();

            base.LoadData();
        }

        protected string RenderNovIzdelek() {
            string ret = "";

            ProductRep prep = new ProductRep();
            int dateMargin = -7;
            var prod = prep.GetProductDescList(1473, LANGUAGE.GetCurrentLang(), true, ppem.em_user.UserId).Where(w => w.ADDED_DATE > DateTime.Today.AddDays(dateMargin))
                .OrderByDescending(o=>o.CAT_SALE ).ThenByDescending(w=> w.ADDED_DATE )
                .FirstOrDefault();

            if (prod == null)
                return ret;

            ret = string.Format("<a class='header_btn' href='{0}'>{1}</a>", SM.BLL.Common.ResolveUrl(SM.EM.Rewrite.EmenikUserProductDetailsUrl(prod.PageId, prod.ID_INT, prod.NAME)), GetGlobalResourceObject("Products", "masterNewProduct"));

            return ret;
        }

        protected string RenderNovosti()
        {
            string ret = "";

            ArticleRep arep = new ArticleRep();
            int dateMargin = SM.BLL.Custom.Settings.NovostiDaysMargin;
            var rec = (from a in arep.GetArticlesByActive(true)
                      where a.RELEASE_DATE > DateTime.Today.AddDays(dateMargin )
                      orderby a.SortWeight descending, a.RELEASE_DATE descending, a.DATE_MODIFY descending, a.DATE_ADDED descending
                      select a) ;
            var list = arep.GetArticleListHierarchy(SM.BLL.Custom.Settings.SmpKlepetalnica(), LANGUAGE.GetCurrentLang(), true, SM.BLL.Custom.MainUserID, rec);
            var art = list.FirstOrDefault();

            if (art == null)
                return ret;

            string novostiUrl = "";

            novostiUrl = SM.BLL.Common.ResolveUrl(SM.EM.Rewrite.EmenikUserUrl("eko", SM.BLL.Custom.Settings.SmpKlepetalnica(), Resources.Product.masterNew ));
            novostiUrl = novostiUrl + "?n=1";// +HttpUtility.UrlEncode(DateTime.Today.AddDays(dateMargin).ToShortDateString());

            ret = string.Format("<a class='header_btn btNovosti' href='{0}'>{1}</a>", novostiUrl, Resources.Product.masterNew
                //    SM.BLL.Common.ResolveUrl(SM.EM.Rewrite.EmenikUserArticleDetailsUrl(ppem.em_user.PAGE_NAME, art.ART_ID ,art.ART_TITLE))
            );

            return ret;
        }



        protected void mTop_DataBound(object sender, EventArgs e)
        {
            Menu m = (Menu)sender;
            SelectCurrentMenuItem(m, smdsMain.Provider);

            // select item if child is selected
            if (m.SelectedItem == null)
            {
                if (ppem.CurrentSmpEmenik != null && ppem.CurrentSmpEmenik.CurrentNode != null)
                {
                    foreach (MenuItem item in m.Items)
                    {
                        if (item.DataPath == ppem.CurrentSmpEmenik.CurrentNode.ParentNode.Key)
                        {
                            item.Selected = true;
                            return;
                        }
                        else if (ppem.CurrentSmpEmenik.CurrentNode.ParentNode.ParentNode != null && item.DataPath == ppem.CurrentSmpEmenik.CurrentNode.ParentNode.ParentNode.Key)
                        {
                            item.Selected = true;
                            return;
                        }
                    }
                }
            }
        }

        //protected void mBottom_DataBound(object sender, EventArgs e)
        //{
        //    Menu m = (Menu)sender;
        //    SelectCurrentMenuItem(m, smdsSek.Provider);
        //}

        //protected void mLeft_DataBound(object sender, EventArgs e)
        //{
        //    Menu m = (Menu)sender;
        //    SelectCurrentMenuItem(m, smdsLeft.Provider);

        //}


        // for custom class in menu
        protected void mTop_MenuItemDataBound(object sender, MenuEventArgs e)
        {
            SiteMapNode node = e.Item.DataItem as SiteMapNode;
            if (node != null)
            {
                if (!string.IsNullOrEmpty(node["class"]))
                    e.Item.ToolTip += ";" + node["class"];
            }
        }



        public static string RenderLoginView(HttpContext context)
        {
            object data = PageId;
            object[] parameters = new object[] { };
            string ret = "";
            ret = SM.EM.Security.Login.RenderLoginView(data, parameters);

            //context.Response.ContentEncoding = Encoding.UTF8;
            context.Response.Charset = "UTF-8"; // important !!
            //return Encoding.UTF8.GetString(ms.ToArray());
            return ret;

        }

        public static string RenderShoppingCart(HttpContext context)
        {
            // set culture
            string culture = LANGUAGE.ValidateCulture(LANGUAGE.GetCurrentCulture());
            System.Threading.Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
            System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo(culture);


            //context.Trace.Warn("RenderShoppingCart - start ");
            ProductRep prep = new ProductRep();

            Guid userid = SM.EM.BLL.Membership.GetCurrentUserID();
            //context.Trace.Warn("RenderShoppingCart - start - getItems");



            object data = prep.GetCartItemList(userid, LANGUAGE.GetCurrentLang(), PageId, null);
            //context.Trace.Warn("RenderShoppingCart - end - getItems");
            object[] parameters = new object[] { PageId };
            string ret = "";
            //context.Trace.Warn("RenderShoppingCart - start - render");
            ret = ViewManager.RenderView("~/_ctrl/module/product/cart/_viewShoppingCartPopup.ascx", data, parameters);
            //context.Trace.Warn("RenderShoppingCart -  end - render");

            context.Response.Charset = "UTF-8"; // important !!
            //context.Trace.Warn("RenderShoppingCart - end ");
            return ret;
        }


        //protected string RenderCustomClass()
        //{
        //    string ret = "";

        //    if (ppem.CurrentSitemapNode == null)
        //        return ret;

        //    return ppem.CurrentSitemapNode["class"];
        //}


        protected string RenderBodyClass()
        {

            string ret = "";
            if (ppem.IsEditMode)
                ret = " edit ";

            // settings
            if (ppem.IsEditMode && !ppem.IsModeCMS && !ppem.IsModePreview)
                ret += " edit_sett ";

            // current smp id
            string smp = "";
            if (ppem.CurrentSitemapNode != null)
                smp = "m_" + ppem.CurrentSitemapNode.Key;

            // if not first level, add first level smp id
            if (ppem.CurrentSitemapNode != null && ppem.CurrentSitemapNode.ParentNode != null && ppem.CurrentSitemapNode.ParentNode != ppem.CurrentSitemapNode.RootNode)
            {
                SiteMapNode node = ppem.CurrentSitemapNode.ParentNode;
                while (node.ParentNode != null && node.ParentNode != ppem.CurrentSitemapNode.RootNode)
                {
                    node = node.ParentNode;
                }

                smp += " m_" + node.Key + "_child";
            }

            // remove from product detail
            if (ppem.PageModule == SM.BLL.Common.PageModule.ProductDetails)
                smp = "";


            ret = ret + LANGUAGE.GetCurrentCulture().ToLower() + " " + smp;
            //Response.Write(ret);
            return ret;
        }

        protected string RenderMp3Url()
        {

            string ret = "";

            if (DateTime.Now.Hour < 20 && DateTime.Now.Hour >= 5 )
                ret = "_inc/sound/ptice-o.mp3";
            else
                ret = "_inc/sound/sova-o.mp3";

            ret = Page.ResolveUrl("~/") + ret;

            return ret;


        }


        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            if (ppem.PageModule != SM.BLL.Common.PageModule.ProductDetails)
            {
                AddFacebookGraphMeta(); // after canonical is set
            }
        }
        //protected override void OnPreRender(EventArgs e)
        //{
        //    base.OnPreRender(e);

        //    AddFacebookGraphMeta(); // after canonical is set
        //}
        protected void AddFacebookGraphMeta()
        {

            // title
            HtmlMeta title = new HtmlMeta();
            title.Attributes["property"] = "og:title";
            title.Content = ppem.Title ;
            ppem.Header.Controls.Add(title);


            // site name
            HtmlMeta site_name = new HtmlMeta();
            site_name.Attributes["property"] = "og:site_name";
            site_name.Content = SM.BLL.Common.Emenik.Data.PortalName();
            ppem.Header.Controls.Add(site_name);

            // type
            HtmlMeta type = new HtmlMeta();
            type.Attributes["property"] = "og:type";
            if (ppem.PageModule != SM.BLL.Common.PageModule.ArticleDetails)
            {
                type.Content = "website";
            }
            else
                type.Content = "article";

            ppem.Header.Controls.Add(type);

            // image

            string img = SM.BLL.Common.ResolveUrlFull("~/_em/templates/_custom/eko/_inc/images/design/ekorepublika.png");
            img = SM.BLL.Common.ResolveUrlFull("~/_em/templates/_custom/eko/_inc/images/logotipi/tg_bcg.jpg?v2");
            
            //img = "http://cnj.blob.core.windows.net/zoran-img/ZJ_facebook_slovenia.jpg";
            //if (!string.IsNullOrEmpty(Prod.IMG_URL))
            //    img = SM.BLL.Common.ResolveUrl(Prod.IMG_URL);

            HtmlMeta image = new HtmlMeta();
            image.Attributes["property"] = "og:image";
            image.Content = img;
            ppem.Header.Controls.Add(image);

            // admin        
            HtmlMeta admin = new HtmlMeta();
            admin.Attributes["property"] = "fb:admins";
            admin.Content = "759469529";
            ppem.Header.Controls.Add(admin);


            // app
            //HtmlMeta app = new HtmlMeta();
            //app.Attributes["property"] = "fb:app_id";
            //app.Content = Facebook.FacebookApplication.Current.AppId;
            //Header.Controls.Add(app);


            // url
            if (!string.IsNullOrEmpty(ppem.RelCanonical))
            {
                HtmlMeta url = new HtmlMeta();
                url.Attributes["property"] = "og:url";

                if (ppem.RelCanonical == "/")
                    url.Content = SM.BLL.Common.ResolveUrlFull("~/");
                else
                    url.Content = ppem.RelCanonical;
                ppem.Header.Controls.Add(url);
            }

        }






        //protected void btSearch_Click(object o, EventArgs e)
        //{
        //    if (!string.IsNullOrEmpty(tbSearch.Text.Trim()))
        //        Response.Redirect(Page.ResolveUrl(SM.EM.Rewrite.SearchProductEmenikUrl(ppem.em_user.UserId, tbSearch.Text)));
        //}
    }
}
