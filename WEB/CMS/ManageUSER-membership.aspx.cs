﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
namespace SM.UI.CMS
{
    public partial class CMS_Intranet_ManageUSER_membership : SM.EM.UI.CMS.BasePage_CMS 
    {



        protected Guid UserID
        {
            get
            {
                if (Request.QueryString["u"] == null)
                    return Guid.Empty;
                if (!SM.EM.Helpers.IsGUID(Request.QueryString["u"]))
                    return Guid.Empty;

                return new Guid(Request.QueryString["u"]);
            }
        }

        protected bool IsAdmin { get {

            return true; // todo: remove
            if (User.Identity.IsAuthenticated)
                if (User.IsInRole("admin"))
                    return true;
            return false;
            }
        }

        protected override void OnInit(EventArgs e)
        {
            CurrentSitemapID = 1431;

            base.OnInit(e);

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            // event handlers
            objUserEdit.OnUserChanged += new EventHandler(objUserEdit_OnUserChanged);
            objUserEdit.OnCancel += new EventHandler(objUserEdit_OnCancel);
            objUserList.OnDelete += new SM.UI.Controls._ctrl_membership_cmsUserList.OnEditEventHandler(objUserList_OnDelete);
            objUserList.OnEdit += new SM.UI.Controls._ctrl_membership_cmsUserList.OnEditEventHandler(objUserList_OnEdit);
            
            // init
            objUserList.IsAdmin = IsAdmin;
            objUserList.PageSize = 100;




            ApplyFilters();

            if (!IsPostBack)
            {
                // if user is set as QS, show edit user
                if (UserID != Guid.Empty)
                {
                    LoadData();

                }
                    //// if project is set, insert new site
                    //else if (ProjectID != Guid.Empty)
                    //{

                    //    objUserEdit.UID = Guid.Empty;
                    //    objUserEdit.BindData();
                    //    cpeQList.Collapsed = true;
                    //    objUserList .BindData();

                    //    ShowTabs();

                    //    // hide some tabs

                    //}
                    else // normal load
                {
                    LoadData();
                   
                    cpeQList.Collapsed = false;
                    cpeQList.ClientState = "false";
                    
                }

            }
        }


        protected void LoadData() {

            objUserEdit.UID = UserID;
            objUserEdit.BindData();
            cpeQList.Collapsed = true;
            cpeQList.ClientState = "true";
            objUserList.BindData();




            
        }

        void objUserEdit_OnCancel(object sender, EventArgs e)
        {
           

            cpeQList.Collapsed = false;
            cpeQList.ClientState = "false";
            
        }


        protected void ApplyFilters() {
            // list
            //objUserList.SearchText = objFilter.SearchText;
            //objUserList.Active = objFilter.Active;
            //objUserList.RoleID = objFilter.Role;



            // item
            //objProjectEdit.CategoryID = objProjectFilter.CategoryID;
        
        }
        // filter
        void objFilter_OnFilterChanged(object sender, EventArgs e)
        {
            // filters
            ApplyFilters();

            // refresh question lists
            objUserList.BindData();


        }
        //edit
        void objUserEdit_OnUserChanged(object sender, EventArgs e)
        {
            // refresh user lists
            ApplyFilters();

            // refresh user lists
            objUserList.BindData();

           


        }

        //list
        void objUserList_OnEdit(object sender, SM.BLL.Common.Args.IDguidEventArgs e)
        {


            // apply filters
            ApplyFilters();


            // load selected question
            //objSiteEdit.SID = e.ClickedID;
            //objSiteEdit.BindData();

        }
        void objUserList_OnDelete(object sender, SM.BLL.Common.Args.IDguidEventArgs e)
        {
            ApplyFilters();

            // refresh group lists
            objUserList.BindData();

        }







    }
}