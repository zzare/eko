﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="popupArticleSettings.ascx.cs" Inherits="SM.EM.UI.Controls.popupArticleSettings" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="EM" TagName="Settings" Src="~/_ctrl/module/article/cwpArticleSettings.ascx"  %>

<asp:HiddenField ID="hfPopup" runat="server" />

        
<asp:Panel CssClass="modPopup" ID="panSettings" runat="server" style="display:none;">
    <div class="modPopup_header modPopup_headerSettings">
        <h4>NASTAVITVE ELEMENTA DINAMIČNE VSEBINE</h4>
        <p>Uredi naziv elementa. Določi aktivnost naziva elementa na spletni strani. </p>
    </div>
    <div class="modPopup_main">
        <EM:Settings ID="objSettings" runat="server"></EM:Settings>
    </div>
    <div class="modPopup_footer">
        <div class="buttonwrapperModal">
            <asp:LinkButton ID="btSaveEdit" runat="server" CssClass="lbutton lbuttonConfirm" OnClick="btSaveEdit_Click" ><span>Shrani</span></asp:LinkButton>
            <asp:LinkButton ID="btCancelEdit" runat="server" CssClass="lbutton lbuttonDelete"><span>Prekliči</span></asp:LinkButton>
        </div>
    </div>
</asp:Panel>

           
<cc1:ModalPopupExtender RepositionMode="RepositionOnWindowResizeAndScroll" Y ="50"  BehaviorID="mpeArticleSettings"  ID="mpeArticleSettings" runat="server" TargetControlID="hfPopup" PopupControlID="panSettings"
                        BackgroundCssClass="modBcg" CancelControlID="btCancelEdit" >
</cc1:ModalPopupExtender> 

<%-- dummy file upload for having another one in sub user control for first time postback file upload--%>
<%--<asp:FileUpload ID="dummyFU" runat="server" style="visibility:hidden;" />  --%>           



