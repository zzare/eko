﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_pmod_2col_33_66.ascx.cs" Inherits="_em_content__ctrl_pmod_2col_33_66" %>
<%@ Register Namespace="SM.EM.UI.Controls" TagPrefix="EM"  %>
<%@ Register TagPrefix="EM" TagName="ModuleNew" Src="~/_ctrl/emenik/emModuleNew.ascx"  %>

    <div class="cont_wideW">
        <div class="cont_wideT"></div>
        <div class="cont_wideM">
            <div class="cont_wideMI">

    <div id="contTwoColsWrapper_33_66" class="clearfix">
        <div id="contTwoColsMain_33_66">
            <EM:ModuleNew ID="mnMain" runat="server" />

            <EM:CwpZone ID="wpz_2" runat="server">
            </EM:CwpZone>

            

        </div>

        <div id="contTwoColsRight_33_66">
      
            <EM:ModuleNew ID="mnRight" runat="server" />
            
            <EM:CwpZone ID="wpz_1" runat="server">
            </EM:CwpZone>

        </div>
    </div>
            </div>    
        </div>
        <div class="cont_wideB"></div>
    </div>
