/*==============================================================*/
/* DBMS name:      Microsoft SQL Server 2005                    */
/* Created on:     10.5.2010 12:07:38                           */
/*==============================================================*/


alter table dbo.ARTICLE
   drop constraint FK_ARTICLE__CMS_WEB_PART
go

alter table dbo.ARTICLE
   drop constraint FK_ARTICLE__GALLERY
go

alter table dbo.ARTICLE
   drop constraint FK_ARTICLE__LANGUAGE
go

alter table dbo.ARTICLE
   drop constraint FK_ARTICLE__SITEMAP
go

alter table dbo.ATTRIBUTE
   drop constraint FK_ATTRIBUTE__GROUP_ATTRIBUTE
go

alter table dbo.CART_ITEM
   drop constraint FK_CART_ITEM__ORDER
go

alter table dbo.CART_ITEM
   drop constraint FK_CART_ITEM__PRODUCT
go

alter table dbo.CART_ITEM_ATTRIBUTE
   drop constraint FK_CART_ITEM_ATTRIBUTE__ATTRIBUTE
go

alter table dbo.CART_ITEM_ATTRIBUTE
   drop constraint FK_CART_ITEM_ATTRIBUTE__CART_ITEM
go

alter table dbo.CART_ITEM_ATTRIBUTE
   drop constraint FK_CART_ITEM_ATTRIBUTE__GROUP_ATTRIBUTE
go

alter table dbo.CMS_WEB_PART
   drop constraint FK_CMS_WEB_PART__LANGUAGE
go

alter table dbo.CMS_WEB_PART
   drop constraint FK_CMS_WEB_PART__MODULE
go

alter table dbo.CMS_WEB_PART
   drop constraint FK_CMS_WEB__REFERENCE_EM_USER
go

alter table dbo.COMPANY
   drop constraint FK_COMPANY__ADDRESS
go

alter table dbo.COMPANY
   drop constraint FK_COMPANY_REFERENCE_EM_USER
go

alter table dbo.CONTENT
   drop constraint FK_CONTENT__CMS_WEB_PART
go

alter table dbo.CONTENT
   drop constraint FK_CONTENT__LANGUAGE
go

alter table dbo.CONTENT
   drop constraint FK_CONTENT__SITEMAP
go

alter table dbo.CULTURE
   drop constraint FK_CULTURE__LANGUAGE
go

alter table dbo.CUSTOMER
   drop constraint FK_CUSTOMER__COMPANY
go

alter table dbo.CUSTOMER
   drop constraint FK_CUSTOMER__EM_USER
go

alter table dbo.CUSTOMER
   drop constraint FK_CUSTOMER__MARKETING_CAMPAIGN
go

alter table dbo.CWP_ARTICLE
   drop constraint FK_CWP_ARTICLE__CMS_WEB_PART
go

alter table dbo.CWP_ARTICLE
   drop constraint FK_CWP_ARTICLE__IMAGE_TYPE
go

alter table dbo.CWP_CONTACT_FORM
   drop constraint FK_CWP_CONTACT_FORM__CMS_WEB_PART
go

alter table dbo.CWP_GALLERY
   drop constraint FK_CWP_GALLERY__CMS_WEB_PART
go

alter table dbo.CWP_GALLERY
   drop constraint FK_CWP_GALLERY__IMAGE_TYPE
go

alter table dbo.CWP_GALLERY
   drop constraint FK_CWP_GALLERY__IMAGE_TYPE_BIG
go

alter table dbo.CWP_MAP
   drop constraint FK_CWP_MAP__CMS_WEB_PART
go

alter table dbo.EM_USER
   drop constraint FK_EM_USER__ADDRESS
go

alter table dbo.EM_USER
   drop constraint FK_EM_USER__COMPANY
go

alter table dbo.EM_USER
   drop constraint FK_EM_USER__HEADER__IMAGE
go

alter table dbo.EM_USER
   drop constraint FK_EM_USER__IMAGE
go

alter table dbo.EM_USER
   drop constraint FK_EM_USER__MASTER
go

alter table dbo.EM_USER
   drop constraint FK_EM_USER__THEME
go

alter table dbo.EM_USER_CULTURE
   drop constraint FK_EM_USER_CULTURE__CULTURE
go

alter table dbo.EM_USER_CULTURE
   drop constraint FK_EM_USER_CULTURE__EM_USER
go

alter table dbo."FILE"
   drop constraint FK_FILE__EM_USER
go

alter table dbo.GALLERY
   drop constraint FK_GALLERY__GALLERY_TYPE
go

alter table dbo.GALLERY_IMAGE_TYPE
   drop constraint FK_GALLERY_IMAGE_TYPE__GALLERY
go

alter table dbo.GALLERY_IMAGE_TYPE
   drop constraint FK_GALLERY_IMAGE_TYPE__IMAGE_TYPE
go

alter table dbo.GALLERY_LANG
   drop constraint FK_GALLERY_LANG__GALLERY
go

alter table dbo.GALLERY_LANG
   drop constraint FK_GALLERY_LANG__LANGUAGE
go

alter table dbo.GALLERY_TYPE_IMAGE_TYPE
   drop constraint FK_GALLERY_TYPE_IMAGE_TYPE__GALLERY_TYPE
go

alter table dbo.GALLERY_TYPE_IMAGE_TYPE
   drop constraint FK_GALLERY_TYPE_IMAGE_TYPE__IMAGE_TYPE
go

alter table dbo.GROUP_ATTRIBUTE
   drop constraint FK_GROUP_ATTRIBUTE__EM_USER
go

alter table dbo.GROUP_ATTRIBUTE_LANG
   drop constraint FK_GROUP_ATTRIBUTE_LANG__GROUP_ATTRIBUTE
go

alter table dbo.GROUP_ATTRIBUTE_LANG
   drop constraint FK_GROUP_ATTRIBUTE_LANG__LANGUAGE
go

alter table dbo.IMAGE
   drop constraint FK_IMAGE__IMAGE_ORIG
go

alter table dbo.IMAGE
   drop constraint FK_IMAGE__IMAGE_TYPE
go

alter table dbo.IMAGE_GALLERY
   drop constraint FK_IMAGE_GALLERY__GALLERY
go

alter table dbo.IMAGE_GALLERY
   drop constraint FK_IMAGE_GALLERY__IMAGE_ORIG
go

alter table dbo.IMAGE_LANG
   drop constraint FK_IMAGE_LANG__IMAGE_ORDER
go

alter table dbo.IMAGE_LANG
   drop constraint FK_IMAGE_LANG__LANGUAGE
go

alter table dbo.JOB_EMAIL
   drop constraint FK_JOB_EMAIL__JOB_LOG
go

alter table dbo.JOB_LOG
   drop constraint FK_JOB_LOG__JOB
go

alter table dbo.MANUFACTURER
   drop constraint FK_MANUFACTURER__EM_USER
go

alter table dbo.MASTER
   drop constraint FK_MASTER__IMAGE_TYPE
go

alter table dbo.MASTER
   drop constraint FK_MASTER__MASTER_GROUP
go

alter table dbo.MASTER
   drop constraint FK_MASTER_REFERENCE_IMAGE_TY
go

alter table dbo.MASTER_PAGEM
   drop constraint FK_MASTER_PAGEM__MASTER
go

alter table dbo.MASTER_PAGEM
   drop constraint FK_MASTER_PAGEM__PAGE_MODULE
go

alter table dbo.NEWSLETTER_EMAIL
   drop constraint FK_NEWSLETTER_EMAIL__NEWSLETTER
go

alter table dbo.OPINION
   drop constraint FK_OPINION__LANGUAGE
go

alter table dbo.OPINION
   drop constraint FK_OPINION__PRODUCT
go

alter table dbo.PAGEM_MOD
   drop constraint FK_PAGEM_MOD__MODULE
go

alter table dbo.PAGEM_MOD
   drop constraint FK_PAGEM_MOD__PAGE_MODULE
go

alter table dbo.PAYMENT
   drop constraint FK_PAYMENT__EM_USER
go

alter table dbo.POLL
   drop constraint FK_POLL__CMS_WEB_PART
go

alter table dbo.POLL
   drop constraint FK_POLL__EM_USER
go

alter table dbo.POLL_CHOICE
   drop constraint FK_POLL_CHOICE__POLL_LANG
go

alter table dbo.POLL_IPADDRESS
   drop constraint FK_POLL_IPADDRESS__POLL
go

alter table dbo.POLL_LANG
   drop constraint FK_POLL_LANG__LANGUAGE
go

alter table dbo.POLL_LANG
   drop constraint FK_POLL_LANG__POLL
go

alter table dbo.PREDRACUN
   drop constraint FK_PREDRACUN__EM_USER
go

alter table dbo.PRODUCT
   drop constraint FK_PRODUCT__EM_USER
go

alter table dbo.PRODUCT
   drop constraint FK_PRODUCT__MANUFACTURER
go

alter table dbo.PRODUCT
   drop constraint FK_PRODUCT__PAGE_MODULE
go

alter table dbo.PRODUCT
   drop constraint FK_PRODUCT__SITEMAP
go

alter table dbo.PRODUCT_ATTRIBUTE
   drop constraint FK_PRODUCT_ATRTIBUTE__ATTRIBUTE
go

alter table dbo.PRODUCT_ATTRIBUTE
   drop constraint FK_PRODUCT_ATTRIBUTE__PRODUCT
go

alter table dbo.PRODUCT_DESC
   drop constraint FK_PRODUCT_DESC__PRODUCT
go

alter table dbo.PRODUCT_DESC
   drop constraint FK_PRODUCT___LANGUAGE
go

alter table dbo.PRODUCT_GROUP_ATTRIBUTE
   drop constraint FK_PRODUCT_GROUP_ATTR__GROUP_ATTRIBUTE
go

alter table dbo.PRODUCT_GROUP_ATTRIBUTE
   drop constraint FK_PRODUCT_GROUP_ATTR___PRODUCT
go

alter table dbo.PRODUCT_SMAP
   drop constraint FK_PRODUCT_SMAP__PRODUCT
go

alter table dbo.PRODUCT_SMAP
   drop constraint FK_PRODUCT_SMAP__SITEMAP
go

alter table dbo.SHOPPING_ORDER
   drop constraint FK_ORDER__EM_USER
go

alter table dbo.SHOPPING_ORDER
   drop constraint FK_ORDER__PREDRACUN
go

alter table dbo.SHOPPING_ORDER
   drop constraint FK_ORDER__TICKET
go

alter table dbo.SHOPPING_ORDER
   drop constraint FK_SHOPPING_ORDER__EM_USER
go

alter table dbo.SHOPPING_ORDER
   drop constraint FK_SHOPPING_ORDER___COMPANY
go

alter table dbo.SITEMAP
   drop constraint FK_SITEMAP__MENU
go

alter table dbo.SITEMAP
   drop constraint FK_SITEMAP__PAGEMODULE
go

alter table dbo.SITEMAP_GALLERY
   drop constraint FK_SITEMAP_GALLERY__GALLERY
go

alter table dbo.SITEMAP_GALLERY
   drop constraint FK_SITEMAP_GALLERY__SITEMAP
go

alter table dbo.SITEMAP_LANG
   drop constraint FK_SML___LANGUAGE
go

alter table dbo.SITEMAP_LANG
   drop constraint FK_SML___SITEMAP
go

alter table dbo.SITEMAP_MODULE
   drop constraint FK_SITEMAP___MODULE
go

alter table dbo.SITEMAP_MODULE
   drop constraint FK_SITEMAP_MODULE___SITEMAP
go

alter table dbo.THEME
   drop constraint FK_THEME__MASTER
go

alter table dbo.TICKET
   drop constraint FK_TICKET__EM_USER
go

alter table dbo.TICKET
   drop constraint FK_TICKET__STATUS
go

alter table dbo.USER_MODULE
   drop constraint FK_USER_MODULE__EM_USER
go

alter table dbo.USER_MODULE
   drop constraint FK_USER_MODULE__MODULE
go

alter table dbo.VOTE_IPADDRESS
   drop constraint FK_VOTE_IPADDRESS__VOTE
go

alter table dbo.ZONE_MODULE
   drop constraint FK_ZONE_MODULE__MODULE
go

alter table dbo.ZONE_MODULE
   drop constraint FK_ZONE_MODULE__PAGE_MODULE
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.vw_UserCulture')
            and   type = 'V')
   drop view dbo.vw_UserCulture
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.vw_Product')
            and   type = 'V')
   drop view dbo.vw_Product
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.vw_ImageLang')
            and   type = 'V')
   drop view dbo.vw_ImageLang
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.vw_ImageDesc')
            and   type = 'V')
   drop view dbo.vw_ImageDesc
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.vw_Image')
            and   type = 'V')
   drop view dbo.vw_Image
go

if exists (select 1
            from  sysobjects
           where  id = object_id('vw_GalleryDesc')
            and   type = 'V')
   drop view vw_GalleryDesc
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.ARTICLE')
            and   name  = 'FK_ARTICLE__SITEMAP'
            and   indid > 0
            and   indid < 255)
   drop index dbo.ARTICLE.FK_ARTICLE__SITEMAP
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.ARTICLE')
            and   name  = 'XFK_ARTICLE__LANGUAGE'
            and   indid > 0
            and   indid < 255)
   drop index dbo.ARTICLE.XFK_ARTICLE__LANGUAGE
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.ARTICLE')
            and   name  = 'XFK_CWP'
            and   indid > 0
            and   indid < 255)
   drop index dbo.ARTICLE.XFK_CWP
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.ARTICLE')
            and   name  = 'X_ARTICLE_1'
            and   indid > 0
            and   indid < 255)
   drop index dbo.ARTICLE.X_ARTICLE_1
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.ARTICLE')
            and   name  = 'X_ARTICLE_ACTIVE'
            and   indid > 0
            and   indid < 255)
   drop index dbo.ARTICLE.X_ARTICLE_ACTIVE
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.ARTICLE')
            and   name  = 'X_ARTICLE_APPROVED'
            and   indid > 0
            and   indid < 255)
   drop index dbo.ARTICLE.X_ARTICLE_APPROVED
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.ARTICLE')
            and   name  = 'X_ARTICLE_EXPIRE_DATE'
            and   indid > 0
            and   indid < 255)
   drop index dbo.ARTICLE.X_ARTICLE_EXPIRE_DATE
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.ARTICLE')
            and   name  = 'X_ARTICLE_RELEASE_DATE'
            and   indid > 0
            and   indid < 255)
   drop index dbo.ARTICLE.X_ARTICLE_RELEASE_DATE
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.ARTICLE')
            and   name  = 'X_ART_GUID'
            and   indid > 0
            and   indid < 255)
   drop index dbo.ARTICLE.X_ART_GUID
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.ARTICLE')
            and   name  = 'X_SORT_WEIGHT'
            and   indid > 0
            and   indid < 255)
   drop index dbo.ARTICLE.X_SORT_WEIGHT
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.ATTRIBUTE')
            and   name  = 'X_GROUP_ATTR'
            and   indid > 0
            and   indid < 255)
   drop index dbo.ATTRIBUTE.X_GROUP_ATTR
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.ATTRIBUTE')
            and   name  = 'X_ORDER'
            and   indid > 0
            and   indid < 255)
   drop index dbo.ATTRIBUTE.X_ORDER
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.CART_ITEM')
            and   name  = 'X_ORDER'
            and   indid > 0
            and   indid < 255)
   drop index dbo.CART_ITEM.X_ORDER
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.CART_ITEM')
            and   name  = 'X_PRODUCT_ID'
            and   indid > 0
            and   indid < 255)
   drop index dbo.CART_ITEM.X_PRODUCT_ID
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.CART_ITEM')
            and   name  = 'X_USER'
            and   indid > 0
            and   indid < 255)
   drop index dbo.CART_ITEM.X_USER
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.CART_ITEM_ATTRIBUTE')
            and   name  = 'X_ATTRIBUTE'
            and   indid > 0
            and   indid < 255)
   drop index dbo.CART_ITEM_ATTRIBUTE.X_ATTRIBUTE
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.CMS_WEB_PART')
            and   name  = 'X_ID_GUID'
            and   indid > 0
            and   indid < 255)
   drop index dbo.CMS_WEB_PART.X_ID_GUID
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.CMS_WEB_PART')
            and   name  = 'X_ID_INT'
            and   indid > 0
            and   indid < 255)
   drop index dbo.CMS_WEB_PART.X_ID_INT
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.CMS_WEB_PART')
            and   name  = 'X_LANG_ID'
            and   indid > 0
            and   indid < 255)
   drop index dbo.CMS_WEB_PART.X_LANG_ID
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.CMS_WEB_PART')
            and   name  = 'X_MOD_ID'
            and   indid > 0
            and   indid < 255)
   drop index dbo.CMS_WEB_PART.X_MOD_ID
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.CMS_WEB_PART')
            and   name  = 'X_REF_ID'
            and   indid > 0
            and   indid < 255)
   drop index dbo.CMS_WEB_PART.X_REF_ID
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.CMS_WEB_PART')
            and   name  = 'X_SMAP_ZONE'
            and   indid > 0
            and   indid < 255)
   drop index dbo.CMS_WEB_PART.X_SMAP_ZONE
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.CMS_WEB_PART')
            and   name  = 'X_USER_ID'
            and   indid > 0
            and   indid < 255)
   drop index dbo.CMS_WEB_PART.X_USER_ID
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.COMMENTS')
            and   name  = 'X_DATE'
            and   indid > 0
            and   indid < 255)
   drop index dbo.COMMENTS.X_DATE
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.COMMENTS')
            and   name  = 'X_REF_GUID'
            and   indid > 0
            and   indid < 255)
   drop index dbo.COMMENTS.X_REF_GUID
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.COMMENTS')
            and   name  = 'X_REF_INT'
            and   indid > 0
            and   indid < 255)
   drop index dbo.COMMENTS.X_REF_INT
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.COMPANY')
            and   name  = 'X_COMPANY_NAME'
            and   indid > 0
            and   indid < 255)
   drop index dbo.COMPANY.X_COMPANY_NAME
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.CONTACT_EMAIL')
            and   name  = 'X_OWNER_STATUS'
            and   indid > 0
            and   indid < 255)
   drop index dbo.CONTACT_EMAIL.X_OWNER_STATUS
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.CONTENT')
            and   name  = 'XFK_CONTENT_LANG'
            and   indid > 0
            and   indid < 255)
   drop index dbo.CONTENT.XFK_CONTENT_LANG
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.CONTENT')
            and   name  = 'XFK_CONTENT_SITEMAP'
            and   indid > 0
            and   indid < 255)
   drop index dbo.CONTENT.XFK_CONTENT_SITEMAP
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.CONTENT')
            and   name  = 'XFK_CONTENT__SMAP_LANG'
            and   indid > 0
            and   indid < 255)
   drop index dbo.CONTENT.XFK_CONTENT__SMAP_LANG
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.CONTENT')
            and   name  = 'X_CONTENT_ACTIVE'
            and   indid > 0
            and   indid < 255)
   drop index dbo.CONTENT.X_CONTENT_ACTIVE
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.CONTENT')
            and   name  = 'X_CWP'
            and   indid > 0
            and   indid < 255)
   drop index dbo.CONTENT.X_CWP
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.CULTURE')
            and   name  = 'XFK_CULTURE__LANG_ID'
            and   indid > 0
            and   indid < 255)
   drop index dbo.CULTURE.XFK_CULTURE__LANG_ID
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.CULTURE')
            and   name  = 'X_CULTURE_ORDER'
            and   indid > 0
            and   indid < 255)
   drop index dbo.CULTURE.X_CULTURE_ORDER
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.CULTURE')
            and   name  = 'X_CULTURE_UI_CULTURE'
            and   indid > 0
            and   indid < 255)
   drop index dbo.CULTURE.X_CULTURE_UI_CULTURE
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.CUSTOMER')
            and   name  = 'XFK_CUSTOMER__COMPANY'
            and   indid > 0
            and   indid < 255)
   drop index dbo.CUSTOMER.XFK_CUSTOMER__COMPANY
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.CUSTOMER')
            and   name  = 'XFK_PAGE_ID'
            and   indid > 0
            and   indid < 255)
   drop index dbo.CUSTOMER.XFK_PAGE_ID
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.CUSTOMER')
            and   name  = 'X_CAMPAIGN'
            and   indid > 0
            and   indid < 255)
   drop index dbo.CUSTOMER.X_CAMPAIGN
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.CUSTOMER')
            and   name  = 'X_USERNAME'
            and   indid > 0
            and   indid < 255)
   drop index dbo.CUSTOMER.X_USERNAME
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.CWP_GALLERY')
            and   name  = 'X_BIG'
            and   indid > 0
            and   indid < 255)
   drop index dbo.CWP_GALLERY.X_BIG
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.CWP_GALLERY')
            and   name  = 'X_THUMB'
            and   indid > 0
            and   indid < 255)
   drop index dbo.CWP_GALLERY.X_THUMB
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.EM_USER')
            and   name  = 'X_CUSTOM_DOMAIN'
            and   indid > 0
            and   indid < 255)
   drop index dbo.EM_USER.X_CUSTOM_DOMAIN
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.EM_USER')
            and   name  = 'X_PORTAL'
            and   indid > 0
            and   indid < 255)
   drop index dbo.EM_USER.X_PORTAL
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.EM_USER')
            and   name  = 'X_USER_ID'
            and   indid > 0
            and   indid < 255)
   drop index dbo.EM_USER.X_USER_ID
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.EM_USER')
            and   name  = 'X_USER_TYPE'
            and   indid > 0
            and   indid < 255)
   drop index dbo.EM_USER.X_USER_TYPE
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.EM_USER_CULTURE')
            and   name  = 'X_ACTIVE'
            and   indid > 0
            and   indid < 255)
   drop index dbo.EM_USER_CULTURE.X_ACTIVE
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.EM_USER_CULTURE')
            and   name  = 'X_DEFAULT'
            and   indid > 0
            and   indid < 255)
   drop index dbo.EM_USER_CULTURE.X_DEFAULT
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.EM_USER_CULTURE')
            and   name  = 'X_SORT'
            and   indid > 0
            and   indid < 255)
   drop index dbo.EM_USER_CULTURE.X_SORT
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.ERROR_LOG')
            and   name  = 'X_DATE'
            and   indid > 0
            and   indid < 255)
   drop index dbo.ERROR_LOG.X_DATE
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo."FILE"')
            and   name  = 'X_DATE'
            and   indid > 0
            and   indid < 255)
   drop index dbo."FILE".X_DATE
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo."FILE"')
            and   name  = 'X_DESC'
            and   indid > 0
            and   indid < 255)
   drop index dbo."FILE".X_DESC
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo."FILE"')
            and   name  = 'X_NAME'
            and   indid > 0
            and   indid < 255)
   drop index dbo."FILE".X_NAME
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo."FILE"')
            and   name  = 'X_REFID'
            and   indid > 0
            and   indid < 255)
   drop index dbo."FILE".X_REFID
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo."FILE"')
            and   name  = 'X_TYPE'
            and   indid > 0
            and   indid < 255)
   drop index dbo."FILE".X_TYPE
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.GALLERY')
            and   name  = 'XFK_GAL_TYPE'
            and   indid > 0
            and   indid < 255)
   drop index dbo.GALLERY.XFK_GAL_TYPE
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.GALLERY_TYPE')
            and   name  = 'X_IS_PUBLIC'
            and   indid > 0
            and   indid < 255)
   drop index dbo.GALLERY_TYPE.X_IS_PUBLIC
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.GROUP_ATTRIBUTE')
            and   name  = 'X_PAGE_ID'
            and   indid > 0
            and   indid < 255)
   drop index dbo.GROUP_ATTRIBUTE.X_PAGE_ID
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.IMAGE_GALLERY')
            and   name  = 'X_IMAGE_GAL_ORDER'
            and   indid > 0
            and   indid < 255)
   drop index dbo.IMAGE_GALLERY.X_IMAGE_GAL_ORDER
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.IMAGE_ORIG')
            and   name  = 'X_DATE_ADDED'
            and   indid > 0
            and   indid < 255)
   drop index dbo.IMAGE_ORIG.X_DATE_ADDED
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.IMAGE_ORIG')
            and   name  = 'X_IMAGE_ORIG_ACTIVE'
            and   indid > 0
            and   indid < 255)
   drop index dbo.IMAGE_ORIG.X_IMAGE_ORIG_ACTIVE
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.IMAGE_ORIG')
            and   name  = 'X_IMAGE_ORIG_ORDER'
            and   indid > 0
            and   indid < 255)
   drop index dbo.IMAGE_ORIG.X_IMAGE_ORIG_ORDER
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.IMAGE_ORIG')
            and   name  = 'X_USERNAME'
            and   indid > 0
            and   indid < 255)
   drop index dbo.IMAGE_ORIG.X_USERNAME
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.IMAGE_TYPE')
            and   name  = 'X_T_TYPE'
            and   indid > 0
            and   indid < 255)
   drop index dbo.IMAGE_TYPE.X_T_TYPE
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.JOB')
            and   name  = 'X_DATE_END'
            and   indid > 0
            and   indid < 255)
   drop index dbo.JOB.X_DATE_END
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.JOB_EMAIL')
            and   name  = 'X_DATE'
            and   indid > 0
            and   indid < 255)
   drop index dbo.JOB_EMAIL.X_DATE
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.JOB_EMAIL')
            and   name  = 'X_EMAIL'
            and   indid > 0
            and   indid < 255)
   drop index dbo.JOB_EMAIL.X_EMAIL
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.JOB_LOG')
            and   name  = 'XFK_JOB_ID'
            and   indid > 0
            and   indid < 255)
   drop index dbo.JOB_LOG.XFK_JOB_ID
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.JOB_LOG')
            and   name  = 'X_DATESTART'
            and   indid > 0
            and   indid < 255)
   drop index dbo.JOB_LOG.X_DATESTART
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.JOB_LOG')
            and   name  = 'X_DATE_EXECUTION'
            and   indid > 0
            and   indid < 255)
   drop index dbo.JOB_LOG.X_DATE_EXECUTION
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.LANGUAGE')
            and   name  = 'X_LANG_ORDER'
            and   indid > 0
            and   indid < 255)
   drop index dbo.LANGUAGE.X_LANG_ORDER
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.MANUFACTURER')
            and   name  = 'X_PAGE'
            and   indid > 0
            and   indid < 255)
   drop index dbo.MANUFACTURER.X_PAGE
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.MANUFACTURER')
            and   name  = 'X_TITLE'
            and   indid > 0
            and   indid < 255)
   drop index dbo.MANUFACTURER.X_TITLE
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.MARKETING_CAMPAIGN')
            and   name  = 'XFK_AFFILIATE'
            and   indid > 0
            and   indid < 255)
   drop index dbo.MARKETING_CAMPAIGN.XFK_AFFILIATE
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.MARKETING_CAMPAIGN')
            and   name  = 'XFK_MARKETING'
            and   indid > 0
            and   indid < 255)
   drop index dbo.MARKETING_CAMPAIGN.XFK_MARKETING
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.MASTER')
            and   name  = 'X_ACTIVE'
            and   indid > 0
            and   indid < 255)
   drop index dbo.MASTER.X_ACTIVE
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.MASTER')
            and   name  = 'X_IS_PUBLIC'
            and   indid > 0
            and   indid < 255)
   drop index dbo.MASTER.X_IS_PUBLIC
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.MASTER')
            and   name  = 'X_ORDER'
            and   indid > 0
            and   indid < 255)
   drop index dbo.MASTER.X_ORDER
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.MASTER_GROUP')
            and   name  = 'X_ORDER'
            and   indid > 0
            and   indid < 255)
   drop index dbo.MASTER_GROUP.X_ORDER
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.MENU')
            and   name  = 'X_MENU_ACTIVE'
            and   indid > 0
            and   indid < 255)
   drop index dbo.MENU.X_MENU_ACTIVE
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.MENU')
            and   name  = 'X_MENU_CMS'
            and   indid > 0
            and   indid < 255)
   drop index dbo.MENU.X_MENU_CMS
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.MENU')
            and   name  = 'X_MENU_USERNAME'
            and   indid > 0
            and   indid < 255)
   drop index dbo.MENU.X_MENU_USERNAME
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.MODULE')
            and   name  = 'X_ACTIVE'
            and   indid > 0
            and   indid < 255)
   drop index dbo.MODULE.X_ACTIVE
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.MODULE')
            and   name  = 'X_MOD_OBJ_ID'
            and   indid > 0
            and   indid < 255)
   drop index dbo.MODULE.X_MOD_OBJ_ID
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.MODULE')
            and   name  = 'X_MOD_ORDER'
            and   indid > 0
            and   indid < 255)
   drop index dbo.MODULE.X_MOD_ORDER
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.NEWSLETTER')
            and   name  = 'X_TYPE'
            and   indid > 0
            and   indid < 255)
   drop index dbo.NEWSLETTER.X_TYPE
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.NEWSLETTER_EMAIL')
            and   name  = 'XFK_NEWSLETTER'
            and   indid > 0
            and   indid < 255)
   drop index dbo.NEWSLETTER_EMAIL.XFK_NEWSLETTER
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.NEWSLETTER_EMAIL')
            and   name  = 'X_EMAIL'
            and   indid > 0
            and   indid < 255)
   drop index dbo.NEWSLETTER_EMAIL.X_EMAIL
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.OPINION')
            and   name  = 'XFK_OPINION__LANGUAGE'
            and   indid > 0
            and   indid < 255)
   drop index dbo.OPINION.XFK_OPINION__LANGUAGE
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.OPINION')
            and   name  = 'XFK_OPINION__PRODUCT'
            and   indid > 0
            and   indid < 255)
   drop index dbo.OPINION.XFK_OPINION__PRODUCT
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.PAGE_MODULE')
            and   name  = 'X_COL_NO'
            and   indid > 0
            and   indid < 255)
   drop index dbo.PAGE_MODULE.X_COL_NO
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.PAGE_MODULE')
            and   name  = 'X_MOD_TYPE'
            and   indid > 0
            and   indid < 255)
   drop index dbo.PAGE_MODULE.X_MOD_TYPE
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.PAGE_MODULE')
            and   name  = 'X_PMOD_ORDER'
            and   indid > 0
            and   indid < 255)
   drop index dbo.PAGE_MODULE.X_PMOD_ORDER
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.PAYMENT')
            and   name  = 'XFK_USER'
            and   indid > 0
            and   indid < 255)
   drop index dbo.PAYMENT.XFK_USER
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.PAYMENT')
            and   name  = 'X_DATE_ADDED'
            and   indid > 0
            and   indid < 255)
   drop index dbo.PAYMENT.X_DATE_ADDED
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.PAYMENT')
            and   name  = 'X_DATE_PAYED'
            and   indid > 0
            and   indid < 255)
   drop index dbo.PAYMENT.X_DATE_PAYED
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.PAYMENT')
            and   name  = 'X_REF_ID'
            and   indid > 0
            and   indid < 255)
   drop index dbo.PAYMENT.X_REF_ID
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.PERMISSION')
            and   name  = 'X_USER'
            and   indid > 0
            and   indid < 255)
   drop index dbo.PERMISSION.X_USER
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.POLL')
            and   name  = 'XFK_CWP_ID'
            and   indid > 0
            and   indid < 255)
   drop index dbo.POLL.XFK_CWP_ID
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.POLL')
            and   name  = 'X_DATE'
            and   indid > 0
            and   indid < 255)
   drop index dbo.POLL.X_DATE
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.POLL_CHOICE')
            and   name  = 'XFK_POLL_LANG'
            and   indid > 0
            and   indid < 255)
   drop index dbo.POLL_CHOICE.XFK_POLL_LANG
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.PREDRACUN')
            and   name  = 'XFK_USER'
            and   indid > 0
            and   indid < 255)
   drop index dbo.PREDRACUN.XFK_USER
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.PREDRACUN')
            and   name  = 'X_DATE_ADDED'
            and   indid > 0
            and   indid < 255)
   drop index dbo.PREDRACUN.X_DATE_ADDED
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.PRODUCT')
            and   name  = 'XFK_MANUFACTURER'
            and   indid > 0
            and   indid < 255)
   drop index dbo.PRODUCT.XFK_MANUFACTURER
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.PRODUCT')
            and   name  = 'X_ID_INT'
            and   indid > 0
            and   indid < 255)
   drop index dbo.PRODUCT.X_ID_INT
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.PRODUCT')
            and   name  = 'X_PAGE_ID'
            and   indid > 0
            and   indid < 255)
   drop index dbo.PRODUCT.X_PAGE_ID
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.PRODUCT')
            and   name  = 'X_PRICE'
            and   indid > 0
            and   indid < 255)
   drop index dbo.PRODUCT.X_PRICE
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.PRODUCT')
            and   name  = 'X_PRODUCT_CODE'
            and   indid > 0
            and   indid < 255)
   drop index dbo.PRODUCT.X_PRODUCT_CODE
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.PRODUCT')
            and   name  = 'X_SMAP_ID'
            and   indid > 0
            and   indid < 255)
   drop index dbo.PRODUCT.X_SMAP_ID
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.PRODUCT')
            and   name  = 'X_STOCK'
            and   indid > 0
            and   indid < 255)
   drop index dbo.PRODUCT.X_STOCK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.PRODUCT_DESC')
            and   name  = 'XAK_PDESC_ID'
            and   indid > 0
            and   indid < 255)
   drop index dbo.PRODUCT_DESC.XAK_PDESC_ID
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.PRODUCT_DESC')
            and   name  = 'X_NAME'
            and   indid > 0
            and   indid < 255)
   drop index dbo.PRODUCT_DESC.X_NAME
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.PRODUCT_SMAP')
            and   name  = 'X_ORDER'
            and   indid > 0
            and   indid < 255)
   drop index dbo.PRODUCT_SMAP.X_ORDER
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.SHOPPING_ORDER')
            and   name  = 'XFK_PAGE_ID'
            and   indid > 0
            and   indid < 255)
   drop index dbo.SHOPPING_ORDER.XFK_PAGE_ID
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.SHOPPING_ORDER')
            and   name  = 'XFK_USERID'
            and   indid > 0
            and   indid < 255)
   drop index dbo.SHOPPING_ORDER.XFK_USERID
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.SHOPPING_ORDER')
            and   name  = 'X_DATE'
            and   indid > 0
            and   indid < 255)
   drop index dbo.SHOPPING_ORDER.X_DATE
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.SHOPPING_ORDER')
            and   name  = 'X_ORDER_REF'
            and   indid > 0
            and   indid < 255)
   drop index dbo.SHOPPING_ORDER.X_ORDER_REF
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.SITEMAP')
            and   name  = 'XFK_SITEMAP_MENU'
            and   indid > 0
            and   indid < 255)
   drop index dbo.SITEMAP.XFK_SITEMAP_MENU
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.SITEMAP')
            and   name  = 'XFK_SITEMAP_MODULE'
            and   indid > 0
            and   indid < 255)
   drop index dbo.SITEMAP.XFK_SITEMAP_MODULE
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.SITEMAP')
            and   name  = 'X_SMAP_GUID'
            and   indid > 0
            and   indid < 255)
   drop index dbo.SITEMAP.X_SMAP_GUID
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.SITEMAP')
            and   name  = 'X_SMAP_ORDER'
            and   indid > 0
            and   indid < 255)
   drop index dbo.SITEMAP.X_SMAP_ORDER
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.SITEMAP')
            and   name  = 'X_SMAP_PARENT'
            and   indid > 0
            and   indid < 255)
   drop index dbo.SITEMAP.X_SMAP_PARENT
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.SITEMAP_GALLERY')
            and   name  = 'X_SMG_ACTIVE'
            and   indid > 0
            and   indid < 255)
   drop index dbo.SITEMAP_GALLERY.X_SMG_ACTIVE
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.SITEMAP_GALLERY')
            and   name  = 'X_SMG_ORDER'
            and   indid > 0
            and   indid < 255)
   drop index dbo.SITEMAP_GALLERY.X_SMG_ORDER
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.SITEMAP_LANG')
            and   name  = 'X_SML_ACTIVE'
            and   indid > 0
            and   indid < 255)
   drop index dbo.SITEMAP_LANG.X_SML_ACTIVE
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.SITEMAP_LANG')
            and   name  = 'X_SML_ORDER'
            and   indid > 0
            and   indid < 255)
   drop index dbo.SITEMAP_LANG.X_SML_ORDER
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.STATUS')
            and   name  = 'X_TYPE'
            and   indid > 0
            and   indid < 255)
   drop index dbo.STATUS.X_TYPE
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.THEME')
            and   name  = 'XFK_MASTER_ID'
            and   indid > 0
            and   indid < 255)
   drop index dbo.THEME.XFK_MASTER_ID
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.THEME')
            and   name  = 'X_ORDER'
            and   indid > 0
            and   indid < 255)
   drop index dbo.THEME.X_ORDER
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.THEME')
            and   name  = 'X_PUBLIC'
            and   indid > 0
            and   indid < 255)
   drop index dbo.THEME.X_PUBLIC
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.TICKET')
            and   name  = 'XFK_USERID'
            and   indid > 0
            and   indid < 255)
   drop index dbo.TICKET.XFK_USERID
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.VOTE')
            and   name  = 'X_AVG_RATING'
            and   indid > 0
            and   indid < 255)
   drop index dbo.VOTE.X_AVG_RATING
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.ZONE_MODULE')
            and   name  = 'X_MOD_ID'
            and   indid > 0
            and   indid < 255)
   drop index dbo.ZONE_MODULE.X_MOD_ID
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.ZONE_MODULE')
            and   name  = 'X_PMOD_ID'
            and   indid > 0
            and   indid < 255)
   drop index dbo.ZONE_MODULE.X_PMOD_ID
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.ZONE_MODULE')
            and   name  = 'X_ZONE'
            and   indid > 0
            and   indid < 255)
   drop index dbo.ZONE_MODULE.X_ZONE
go

alter table dbo.ADDRESS
   drop constraint PK_ADDRESS
go

alter table dbo.ARTICLE
   drop constraint PK_ARTICLE
go

alter table dbo.ATTRIBUTE
   drop constraint PK_ATTRIBUTE
go

alter table dbo.CART_ITEM
   drop constraint PK_CART_ITEM
go

alter table dbo.CART_ITEM_ATTRIBUTE
   drop constraint PK_CART_ITEM_ATTRIBUTE
go

alter table dbo.CMS_WEB_PART
   drop constraint PK_CMS_WEB_PART
go

alter table dbo.COMMENTS
   drop constraint AK_AK_REFID_COMMENTS
go

alter table dbo.COMMENTS
   drop constraint PK_COMMENTS
go

alter table dbo.COMPANY
   drop constraint PK_COMPANY
go

alter table dbo.CONTACT_EMAIL
   drop constraint PK_CONTACT_EMAIL
go

alter table dbo.CONTENT
   drop constraint PK_CONTENT
go

alter table dbo.CULTURE
   drop constraint PK_CULTURE
go

alter table dbo.CUSTOMER
   drop constraint PK_CUSTOMER
go

alter table dbo.CWP_ARTICLE
   drop constraint PK_CWP_ARTICLE
go

alter table dbo.CWP_CONTACT_FORM
   drop constraint PK_CWP_CONTACT_FORM
go

alter table dbo.CWP_GALLERY
   drop constraint PK_CWP_GALLERY
go

alter table dbo.CWP_MAP
   drop constraint PK_CWP_MAP
go

alter table dbo.EM_USER
   drop constraint AK_EM_USER_PAGE_NAME
go

alter table dbo.EM_USER
   drop constraint AK_EM_USER_USERNAME
go

alter table dbo.EM_USER
   drop constraint PK_EM_USER
go

alter table dbo.EM_USER_CULTURE
   drop constraint PK_EM_USER_CULTURE
go

alter table dbo.ERROR_LOG
   drop constraint PK_ERROR_LOG
go

alter table dbo."FILE"
   drop constraint PK_FILE
go

alter table dbo.GALLERY
   drop constraint PK_GALLERY
go

alter table dbo.GALLERY_IMAGE_TYPE
   drop constraint PK_GALLERY_IMAGE_TYPE
go

alter table dbo.GALLERY_LANG
   drop constraint PK_GALLERY_LANG
go

alter table dbo.GALLERY_TYPE
   drop constraint PK_GALLERY_TYPE
go

alter table dbo.GALLERY_TYPE_IMAGE_TYPE
   drop constraint PK_GALLERY_TYPE_IMAGE_TYPE
go

alter table dbo.GROUP_ATTRIBUTE
   drop constraint PK_GROUP_ATTRIBUTE
go

alter table dbo.GROUP_ATTRIBUTE_LANG
   drop constraint PK_GROUP_ATTRIBUTE_LANG
go

alter table dbo.IMAGE
   drop constraint PK_IMAGE
go

alter table dbo.IMAGE_GALLERY
   drop constraint PK_IMAGE_GALLERY
go

alter table dbo.IMAGE_LANG
   drop constraint PK_IMAGE_LANG
go

alter table dbo.IMAGE_ORIG
   drop constraint PK_IMAGE_ORIG
go

alter table dbo.IMAGE_TYPE
   drop constraint PK_IMAGE_TYPE
go

alter table dbo.JOB
   drop constraint PK_JOB
go

alter table dbo.JOB_EMAIL
   drop constraint PK_JOB_EMAIL
go

alter table dbo.JOB_LOG
   drop constraint PK_JOB_LOG
go

alter table dbo.LANGUAGE
   drop constraint PK_LANGUAGE
go

alter table dbo.MANUFACTURER
   drop constraint PK_MANUFACTURER
go

alter table dbo.MARKETING_CAMPAIGN
   drop constraint AK_MARKETING_PROMO_CODE
go

alter table dbo.MARKETING_CAMPAIGN
   drop constraint PK_MARKETING_CAMPAIGN
go

alter table dbo.MASTER
   drop constraint PK_MASTER
go

alter table dbo.MASTER_GROUP
   drop constraint PK_MASTER_GROUP
go

alter table dbo.MASTER_PAGEM
   drop constraint PK_MASTER_PAGEM
go

alter table dbo.MENU
   drop constraint PK_MENU
go

alter table dbo.MODULE
   drop constraint PK_MODULE
go

alter table dbo.NEWSLETTER
   drop constraint PK_NEWSLETTER
go

alter table dbo.NEWSLETTER_EMAIL
   drop constraint PK_NEWSLETTER_EMAIL
go

alter table dbo.OPINION
   drop constraint PK_OPINION
go

alter table dbo.PAGEM_MOD
   drop constraint PK_PAGEM_MOD
go

alter table dbo.PAGE_MODULE
   drop constraint PK_PAGE_MODULE
go

alter table dbo.PAYMENT
   drop constraint PK_PAYMENT
go

alter table dbo.PERMISSION
   drop constraint AK_AK_PERMISSION_PERMISSI
go

alter table dbo.PERMISSION
   drop constraint PK_PERMISSION
go

alter table dbo.POLL
   drop constraint PK_POLL
go

alter table dbo.POLL_CHOICE
   drop constraint PK_POLL_CHOICE
go

alter table dbo.POLL_IPADDRESS
   drop constraint PK_POLL_IPADDRESS
go

alter table dbo.POLL_LANG
   drop constraint PK_POLL_LANG
go

alter table dbo.PREDRACUN
   drop constraint PK_PREDRACUN
go

alter table dbo.PRODUCT
   drop constraint PK_PRODUCT
go

alter table dbo.PRODUCT_ATTRIBUTE
   drop constraint PK_PRODUCT_ATTRIBUTE
go

alter table dbo.PRODUCT_DESC
   drop constraint PK_PRODUCT_DESC
go

alter table dbo.PRODUCT_GROUP_ATTRIBUTE
   drop constraint PK_PRODUCT_GROUP_ATTRIBUTE
go

alter table dbo.PRODUCT_SMAP
   drop constraint PK_PRODUCT_SMAP
go

alter table dbo.SHOPPING_ORDER
   drop constraint PK_SHOPPING_ORDER
go

alter table dbo.SITEMAP
   drop constraint PK_SITEMAP
go

alter table dbo.SITEMAP_GALLERY
   drop constraint PK_SITEMAP_GALLERY
go

alter table dbo.SITEMAP_LANG
   drop constraint PK_SITEMAP_LANG
go

alter table dbo.SITEMAP_MODULE
   drop constraint PK_SITEMAP_MODULE
go

alter table dbo.STATUS
   drop constraint PK_STATUS
go

alter table dbo.THEME
   drop constraint PK_THEME
go

alter table dbo.TICKET
   drop constraint AK_AK_REFID_TICKET
go

alter table dbo.TICKET
   drop constraint PK_TICKET
go

alter table dbo.USER_MODULE
   drop constraint PK_USER_MODULE
go

alter table dbo.VOTE
   drop constraint PK_VOTE
go

alter table dbo.VOTE_IPADDRESS
   drop constraint PK_VOTE_IPADDRESS
go

alter table dbo.ZONE_MODULE
   drop constraint PK_ZONE_MODULE
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.ADDRESS')
            and   type = 'U')
   drop table dbo.ADDRESS
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.ARTICLE')
            and   type = 'U')
   drop table dbo.ARTICLE
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.ATTRIBUTE')
            and   type = 'U')
   drop table dbo.ATTRIBUTE
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.CART_ITEM')
            and   type = 'U')
   drop table dbo.CART_ITEM
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.CART_ITEM_ATTRIBUTE')
            and   type = 'U')
   drop table dbo.CART_ITEM_ATTRIBUTE
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.CMS_WEB_PART')
            and   type = 'U')
   drop table dbo.CMS_WEB_PART
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.COMMENTS')
            and   type = 'U')
   drop table dbo.COMMENTS
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.COMPANY')
            and   type = 'U')
   drop table dbo.COMPANY
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.CONTACT_EMAIL')
            and   type = 'U')
   drop table dbo.CONTACT_EMAIL
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.CONTENT')
            and   type = 'U')
   drop table dbo.CONTENT
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.CULTURE')
            and   type = 'U')
   drop table dbo.CULTURE
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.CUSTOMER')
            and   type = 'U')
   drop table dbo.CUSTOMER
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.CWP_ARTICLE')
            and   type = 'U')
   drop table dbo.CWP_ARTICLE
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.CWP_CONTACT_FORM')
            and   type = 'U')
   drop table dbo.CWP_CONTACT_FORM
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.CWP_GALLERY')
            and   type = 'U')
   drop table dbo.CWP_GALLERY
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.CWP_MAP')
            and   type = 'U')
   drop table dbo.CWP_MAP
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.EM_USER')
            and   type = 'U')
   drop table dbo.EM_USER
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.EM_USER_CULTURE')
            and   type = 'U')
   drop table dbo.EM_USER_CULTURE
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.ERROR_LOG')
            and   type = 'U')
   drop table dbo.ERROR_LOG
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo."FILE"')
            and   type = 'U')
   drop table dbo."FILE"
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.GALLERY')
            and   type = 'U')
   drop table dbo.GALLERY
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.GALLERY_IMAGE_TYPE')
            and   type = 'U')
   drop table dbo.GALLERY_IMAGE_TYPE
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.GALLERY_LANG')
            and   type = 'U')
   drop table dbo.GALLERY_LANG
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.GALLERY_TYPE')
            and   type = 'U')
   drop table dbo.GALLERY_TYPE
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.GALLERY_TYPE_IMAGE_TYPE')
            and   type = 'U')
   drop table dbo.GALLERY_TYPE_IMAGE_TYPE
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.GROUP_ATTRIBUTE')
            and   type = 'U')
   drop table dbo.GROUP_ATTRIBUTE
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.GROUP_ATTRIBUTE_LANG')
            and   type = 'U')
   drop table dbo.GROUP_ATTRIBUTE_LANG
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.IMAGE')
            and   type = 'U')
   drop table dbo.IMAGE
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.IMAGE_GALLERY')
            and   type = 'U')
   drop table dbo.IMAGE_GALLERY
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.IMAGE_LANG')
            and   type = 'U')
   drop table dbo.IMAGE_LANG
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.IMAGE_ORIG')
            and   type = 'U')
   drop table dbo.IMAGE_ORIG
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.IMAGE_TYPE')
            and   type = 'U')
   drop table dbo.IMAGE_TYPE
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.JOB')
            and   type = 'U')
   drop table dbo.JOB
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.JOB_EMAIL')
            and   type = 'U')
   drop table dbo.JOB_EMAIL
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.JOB_LOG')
            and   type = 'U')
   drop table dbo.JOB_LOG
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.LANGUAGE')
            and   type = 'U')
   drop table dbo.LANGUAGE
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.MANUFACTURER')
            and   type = 'U')
   drop table dbo.MANUFACTURER
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.MARKETING_CAMPAIGN')
            and   type = 'U')
   drop table dbo.MARKETING_CAMPAIGN
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.MASTER')
            and   type = 'U')
   drop table dbo.MASTER
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.MASTER_GROUP')
            and   type = 'U')
   drop table dbo.MASTER_GROUP
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.MASTER_PAGEM')
            and   type = 'U')
   drop table dbo.MASTER_PAGEM
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.MENU')
            and   type = 'U')
   drop table dbo.MENU
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.MODULE')
            and   type = 'U')
   drop table dbo.MODULE
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.NEWSLETTER')
            and   type = 'U')
   drop table dbo.NEWSLETTER
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.NEWSLETTER_EMAIL')
            and   type = 'U')
   drop table dbo.NEWSLETTER_EMAIL
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.OPINION')
            and   type = 'U')
   drop table dbo.OPINION
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.PAGEM_MOD')
            and   type = 'U')
   drop table dbo.PAGEM_MOD
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.PAGE_MODULE')
            and   type = 'U')
   drop table dbo.PAGE_MODULE
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.PAYMENT')
            and   type = 'U')
   drop table dbo.PAYMENT
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.PERMISSION')
            and   type = 'U')
   drop table dbo.PERMISSION
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.POLL')
            and   type = 'U')
   drop table dbo.POLL
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.POLL_CHOICE')
            and   type = 'U')
   drop table dbo.POLL_CHOICE
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.POLL_IPADDRESS')
            and   type = 'U')
   drop table dbo.POLL_IPADDRESS
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.POLL_LANG')
            and   type = 'U')
   drop table dbo.POLL_LANG
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.PREDRACUN')
            and   type = 'U')
   drop table dbo.PREDRACUN
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.PRODUCT')
            and   type = 'U')
   drop table dbo.PRODUCT
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.PRODUCT_ATTRIBUTE')
            and   type = 'U')
   drop table dbo.PRODUCT_ATTRIBUTE
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.PRODUCT_DESC')
            and   type = 'U')
   drop table dbo.PRODUCT_DESC
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.PRODUCT_GROUP_ATTRIBUTE')
            and   type = 'U')
   drop table dbo.PRODUCT_GROUP_ATTRIBUTE
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.PRODUCT_SMAP')
            and   type = 'U')
   drop table dbo.PRODUCT_SMAP
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.SHOPPING_ORDER')
            and   type = 'U')
   drop table dbo.SHOPPING_ORDER
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.SITEMAP')
            and   type = 'U')
   drop table dbo.SITEMAP
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.SITEMAP_GALLERY')
            and   type = 'U')
   drop table dbo.SITEMAP_GALLERY
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.SITEMAP_LANG')
            and   type = 'U')
   drop table dbo.SITEMAP_LANG
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.SITEMAP_MODULE')
            and   type = 'U')
   drop table dbo.SITEMAP_MODULE
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.STATUS')
            and   type = 'U')
   drop table dbo.STATUS
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.THEME')
            and   type = 'U')
   drop table dbo.THEME
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.TICKET')
            and   type = 'U')
   drop table dbo.TICKET
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.USER_MODULE')
            and   type = 'U')
   drop table dbo.USER_MODULE
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.VOTE')
            and   type = 'U')
   drop table dbo.VOTE
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.VOTE_IPADDRESS')
            and   type = 'U')
   drop table dbo.VOTE_IPADDRESS
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.ZONE_MODULE')
            and   type = 'U')
   drop table dbo.ZONE_MODULE
go

execute sp_revokedbaccess dbo
go

/*==============================================================*/
/* User: dbo                                                    */
/*==============================================================*/
execute sp_grantdbaccess dbo
go

/*==============================================================*/
/* Table: ADDRESS                                               */
/*==============================================================*/
create table dbo.ADDRESS (
   ADDR_ID              int                  identity,
   STREET               nvarchar(256)        not null,
   CITY                 nvarchar(256)        not null,
   POSTAL_CODE          nvarchar(50)         not null,
   STATE                nvarchar(256)        null,
   COUNTRY              nvarchar(256)        null,
   TELEPHONE            nvarchar(256)        null,
   TELEPHONE_2          nvarchar(256)        null,
   EMAIL                nvarchar(256)        null,
   FAX                  nvarchar(256)        null,
   MOBILE               nvarchar(256)        null
)
go

alter table dbo.ADDRESS
   add constraint PK_ADDRESS primary key (ADDR_ID)
go

/*==============================================================*/
/* Table: ARTICLE                                               */
/*==============================================================*/
create table dbo.ARTICLE (
   ART_ID               int                  identity,
   SMAP_ID              int                  null,
   LANG_ID              nchar(2)             not null,
   GAL_ID               int                  null,
   CWP_ID               uniqueidentifier     null,
   ART_GUID             uniqueidentifier     not null default newid(),
   ADDED_BY             nvarchar(50)         not null,
   DATE_ADDED           datetime             not null default getdate(),
   DATE_MODIFY          datetime             not null default getdate(),
   ART_TITLE            nvarchar(256)        not null,
   ART_ABSTRACT         nvarchar(4000)       null,
   ART_BODY             ntext                not null,
   RELEASE_DATE         datetime             null,
   EXPIRE_DATE          datetime             null,
   APPROVED             bit                  not null default 1,
   ACTIVE               bit                  not null default 1,
   COMMENTS_ENABLED     bit                  not null default 1,
   MEMBERS_ONLY         bit                  not null default 0,
   VIEW_COUNT           bigint               not null default 0,
   VOTES                int                  not null default 0,
   TOTAL_RATING         int                  not null default 0,
   ART_IMAGE            nvarchar(256)        null,
   EMBEDD               nvarchar(1024)       null,
   EXPOSED              bit                  not null default 0,
   SortWeight           smallint             not null default 0
)
go

alter table dbo.ARTICLE
   add constraint PK_ARTICLE primary key (ART_ID)
go

/*==============================================================*/
/* Index: XFK_ARTICLE__LANGUAGE                                 */
/*==============================================================*/
create index XFK_ARTICLE__LANGUAGE on dbo.ARTICLE (
LANG_ID ASC
)
go

/*==============================================================*/
/* Index: X_ARTICLE_1                                           */
/*==============================================================*/
create index X_ARTICLE_1 on dbo.ARTICLE (
ACTIVE DESC,
RELEASE_DATE ASC,
EXPIRE_DATE ASC
)
go

/*==============================================================*/
/* Index: FK_ARTICLE__SITEMAP                                   */
/*==============================================================*/
create index FK_ARTICLE__SITEMAP on dbo.ARTICLE (
SMAP_ID ASC
)
go

/*==============================================================*/
/* Index: X_ARTICLE_ACTIVE                                      */
/*==============================================================*/
create index X_ARTICLE_ACTIVE on dbo.ARTICLE (
ACTIVE ASC
)
go

/*==============================================================*/
/* Index: X_ARTICLE_APPROVED                                    */
/*==============================================================*/
create index X_ARTICLE_APPROVED on dbo.ARTICLE (
APPROVED ASC
)
go

/*==============================================================*/
/* Index: X_ARTICLE_RELEASE_DATE                                */
/*==============================================================*/
create index X_ARTICLE_RELEASE_DATE on dbo.ARTICLE (
RELEASE_DATE ASC
)
go

/*==============================================================*/
/* Index: X_ARTICLE_EXPIRE_DATE                                 */
/*==============================================================*/
create index X_ARTICLE_EXPIRE_DATE on dbo.ARTICLE (
EXPIRE_DATE ASC
)
go

/*==============================================================*/
/* Index: XFK_CWP                                               */
/*==============================================================*/
create index XFK_CWP on dbo.ARTICLE (
CWP_ID ASC
)
go

/*==============================================================*/
/* Index: X_ART_GUID                                            */
/*==============================================================*/
create index X_ART_GUID on dbo.ARTICLE (
ART_GUID ASC
)
go

/*==============================================================*/
/* Index: X_SORT_WEIGHT                                         */
/*==============================================================*/
create index X_SORT_WEIGHT on dbo.ARTICLE (
SortWeight ASC
)
go

/*==============================================================*/
/* Table: ATTRIBUTE                                             */
/*==============================================================*/
create table dbo.ATTRIBUTE (
   AttributeID          uniqueidentifier     not null,
   GroupAttID           uniqueidentifier     null,
   Title                nvarchar(512)        not null,
   "Desc"               nvarchar(1024)       null,
   Code                 nvarchar(256)        null,
   UrlSmall             nvarchar(512)        null,
   UrlBig               nvarchar(512)        null,
   Ordr                 int                  not null default 100
)
go

alter table dbo.ATTRIBUTE
   add constraint PK_ATTRIBUTE primary key (AttributeID)
go

/*==============================================================*/
/* Index: X_GROUP_ATTR                                          */
/*==============================================================*/
create index X_GROUP_ATTR on dbo.ATTRIBUTE (
GroupAttID ASC
)
go

/*==============================================================*/
/* Index: X_ORDER                                               */
/*==============================================================*/
create index X_ORDER on dbo.ATTRIBUTE (
Ordr ASC
)
go

/*==============================================================*/
/* Table: CART_ITEM                                             */
/*==============================================================*/
create table dbo.CART_ITEM (
   CartItemID           uniqueidentifier     not null,
   PRODUCT_ID           uniqueidentifier     not null default newid(),
   UserId               uniqueidentifier     not null,
   OrderID              uniqueidentifier     null,
   DateAdded            datetime             not null,
   Quantity             int                  not null,
   Price                money                not null default 0,
   PriceTotal           AS (Price + Tax),
   Tax                  money                not null
)
go

alter table dbo.CART_ITEM
   add constraint PK_CART_ITEM primary key (CartItemID)
go

/*==============================================================*/
/* Index: X_USER                                                */
/*==============================================================*/
create index X_USER on dbo.CART_ITEM (
UserId ASC
)
go

/*==============================================================*/
/* Index: X_ORDER                                               */
/*==============================================================*/
create index X_ORDER on dbo.CART_ITEM (
OrderID ASC
)
go

/*==============================================================*/
/* Index: X_PRODUCT_ID                                          */
/*==============================================================*/
create index X_PRODUCT_ID on dbo.CART_ITEM (
PRODUCT_ID ASC
)
go

/*==============================================================*/
/* Table: CART_ITEM_ATTRIBUTE                                   */
/*==============================================================*/
create table dbo.CART_ITEM_ATTRIBUTE (
   CartItemID           uniqueidentifier     not null,
   GroupAttID           uniqueidentifier     not null,
   AttributeID          uniqueidentifier     not null
)
go

alter table dbo.CART_ITEM_ATTRIBUTE
   add constraint PK_CART_ITEM_ATTRIBUTE primary key (CartItemID, GroupAttID)
go

/*==============================================================*/
/* Index: X_ATTRIBUTE                                           */
/*==============================================================*/
create index X_ATTRIBUTE on dbo.CART_ITEM_ATTRIBUTE (
AttributeID ASC
)
go

/*==============================================================*/
/* Table: CMS_WEB_PART                                          */
/*==============================================================*/
create table dbo.CMS_WEB_PART (
   CWP_ID               uniqueidentifier     not null,
   UserId               uniqueidentifier     null,
   ZONE_ID              nvarchar(256)        not null,
   LANG_ID              nchar(2)             null,
   REF_ID               uniqueidentifier     null,
   MOD_ID               int                  not null,
   TITLE                nvarchar(256)        null,
   "DESC"               nvarchar(1000)       null,
   ACTIVE               bit                  not null default 1,
   DELETED              bit                  not null default 0,
   "ORDER"              int                  not null,
   SHOW_TITLE           bit                  not null default 0,
   DATE_ADDED           datetime             not null,
   DATE_MODIFIED        datetime             null,
   ROLES_VIEW           nvarchar(256)        not null,
   ROLES_EDIT           nvarchar(256)        not null,
   PAGE_ENABLE          bit                  not null default 0,
   PAGE_SIZE            int                  not null default 5,
   ID_INT               int                  null,
   ID_GUID              uniqueidentifier     null,
   DISPLAY_TYPE         int                  not null default 0,
   CONTAINER_CLASS      nvarchar(256)        null
)
go

alter table dbo.CMS_WEB_PART
   add constraint PK_CMS_WEB_PART primary key (CWP_ID)
go

/*==============================================================*/
/* Index: X_SMAP_ZONE                                           */
/*==============================================================*/
create index X_SMAP_ZONE on dbo.CMS_WEB_PART (
ZONE_ID ASC
)
go

/*==============================================================*/
/* Index: X_ID_INT                                              */
/*==============================================================*/
create index X_ID_INT on dbo.CMS_WEB_PART (
ID_INT ASC
)
go

/*==============================================================*/
/* Index: X_MOD_ID                                              */
/*==============================================================*/
create index X_MOD_ID on dbo.CMS_WEB_PART (
MOD_ID ASC
)
go

/*==============================================================*/
/* Index: X_LANG_ID                                             */
/*==============================================================*/
create index X_LANG_ID on dbo.CMS_WEB_PART (
LANG_ID ASC
)
go

/*==============================================================*/
/* Index: X_ID_GUID                                             */
/*==============================================================*/
create index X_ID_GUID on dbo.CMS_WEB_PART (
ID_GUID ASC
)
go

/*==============================================================*/
/* Index: X_REF_ID                                              */
/*==============================================================*/
create index X_REF_ID on dbo.CMS_WEB_PART (
REF_ID ASC
)
go

/*==============================================================*/
/* Index: X_USER_ID                                             */
/*==============================================================*/
create index X_USER_ID on dbo.CMS_WEB_PART (
UserId ASC
)
go

/*==============================================================*/
/* Table: COMMENTS                                              */
/*==============================================================*/
create table dbo.COMMENTS (
   CommentID            uniqueidentifier     not null default newid(),
   CommentRefID         int                  identity,
   DateAdded            datetime             not null,
   RefGuid              uniqueidentifier     null,
   RefInt               int                  null,
   UserId               uniqueidentifier     null,
   UserName             nvarchar(256)        not null,
   WWW                  nvarchar(512)        null,
   Comment              nvarchar(MAX)        not null,
   Email                nvarchar(512)        null,
   Title                nvarchar(512)        null,
   IsAuthor             bit                  not null default 0,
   Active               bit                  not null default 1
)
go

alter table dbo.COMMENTS
   add constraint PK_COMMENTS primary key (CommentID)
go

alter table dbo.COMMENTS
   add constraint AK_AK_REFID_COMMENTS unique (CommentRefID)
go

/*==============================================================*/
/* Index: X_REF_GUID                                            */
/*==============================================================*/
create index X_REF_GUID on dbo.COMMENTS (
RefGuid ASC
)
go

/*==============================================================*/
/* Index: X_REF_INT                                             */
/*==============================================================*/
create index X_REF_INT on dbo.COMMENTS (
RefInt ASC
)
go

/*==============================================================*/
/* Index: X_DATE                                                */
/*==============================================================*/
create index X_DATE on dbo.COMMENTS (
DateAdded ASC
)
go

/*==============================================================*/
/* Table: COMPANY                                               */
/*==============================================================*/
create table dbo.COMPANY (
   COMPANY_ID           int                  identity,
   TAX_NUMBER           nvarchar(50)         not null,
   ADDR_ID              int                  not null,
   PageId               uniqueidentifier     null,
   NAME                 nvarchar(256)        not null,
   ACCOUNT_NO           nvarchar(50)         null,
   COMPANY_NO           nvarchar(50)         null default '0',
   WWW                  nvarchar(256)        null,
   ZAVEZANEC            bit                  not null default 0
)
go

alter table dbo.COMPANY
   add constraint PK_COMPANY primary key (COMPANY_ID)
go

/*==============================================================*/
/* Index: X_COMPANY_NAME                                        */
/*==============================================================*/
create index X_COMPANY_NAME on dbo.COMPANY (
NAME ASC
)
go

/*==============================================================*/
/* Table: CONTACT_EMAIL                                         */
/*==============================================================*/
create table dbo.CONTACT_EMAIL (
   ContID               uniqueidentifier     not null,
   Email                nvarchar(256)        not null,
   Status               smallint             not null,
   DateAdded            datetime             not null,
   DateSignedOut        datetime             null,
   UserId               uniqueidentifier     null
)
go

alter table dbo.CONTACT_EMAIL
   add constraint PK_CONTACT_EMAIL primary key (ContID)
go

/*==============================================================*/
/* Index: X_OWNER_STATUS                                        */
/*==============================================================*/
create index X_OWNER_STATUS on dbo.CONTACT_EMAIL (
Status ASC,
UserId ASC
)
go

/*==============================================================*/
/* Table: CONTENT                                               */
/*==============================================================*/
create table dbo.CONTENT (
   CON_ID               int                  identity,
   SMAP_ID              int                  null,
   LANG_ID              nchar(2)             not null,
   CWP_ID               uniqueidentifier     null,
   CONT_BODY            ntext                not null,
   DATE_ADDED           datetime             not null default getdate(),
   VIEW_COUNT           int                  not null default 0,
   ACTIVE               bit                  not null default 1,
   CON_ORDER            int                  not null default 100
)
go

alter table dbo.CONTENT
   add constraint PK_CONTENT primary key (CON_ID)
go

/*==============================================================*/
/* Index: XFK_CONTENT_SITEMAP                                   */
/*==============================================================*/
create index XFK_CONTENT_SITEMAP on dbo.CONTENT (
SMAP_ID ASC
)
go

/*==============================================================*/
/* Index: XFK_CONTENT_LANG                                      */
/*==============================================================*/
create index XFK_CONTENT_LANG on dbo.CONTENT (
LANG_ID ASC
)
go

/*==============================================================*/
/* Index: XFK_CONTENT__SMAP_LANG                                */
/*==============================================================*/
create index XFK_CONTENT__SMAP_LANG on dbo.CONTENT (
SMAP_ID ASC,
LANG_ID ASC
)
go

/*==============================================================*/
/* Index: X_CONTENT_ACTIVE                                      */
/*==============================================================*/
create index X_CONTENT_ACTIVE on dbo.CONTENT (
ACTIVE ASC
)
go

/*==============================================================*/
/* Index: X_CWP                                                 */
/*==============================================================*/
create index X_CWP on dbo.CONTENT (
CWP_ID ASC
)
go

/*==============================================================*/
/* Table: CULTURE                                               */
/*==============================================================*/
create table dbo.CULTURE (
   LCID                 int                  not null,
   LANG_ID              nchar(2)             not null,
   CULT                 nvarchar(5)          not null,
   UI_CULTURE           nvarchar(5)          not null,
   "DESC"               nvarchar(256)        null,
   IMAGE_URL            nvarchar(256)        null,
   "ORDER"              int                  not null default 0
)
go

alter table dbo.CULTURE
   add constraint PK_CULTURE primary key (LCID)
go

/*==============================================================*/
/* Index: XFK_CULTURE__LANG_ID                                  */
/*==============================================================*/
create index XFK_CULTURE__LANG_ID on dbo.CULTURE (
LANG_ID ASC
)
go

/*==============================================================*/
/* Index: X_CULTURE_ORDER                                       */
/*==============================================================*/
create index X_CULTURE_ORDER on dbo.CULTURE (
"ORDER" ASC
)
go

/*==============================================================*/
/* Index: X_CULTURE_UI_CULTURE                                  */
/*==============================================================*/
create index X_CULTURE_UI_CULTURE on dbo.CULTURE (
UI_CULTURE ASC
)
go

/*==============================================================*/
/* Table: CUSTOMER                                              */
/*==============================================================*/
create table dbo.CUSTOMER (
   CustomerID           uniqueidentifier     not null,
   USERNAME             nvarchar(50)         not null,
   COMPANY_ID           int                  null,
   PageID               uniqueidentifier     not null,
   CampaignID           uniqueidentifier     null,
   FIRST_NAME           nvarchar(50)         not null,
   LAST_NAME            nvarchar(50)         not null,
   STREET               nvarchar(128)        not null,
   CITY                 nvarchar(50)         not null,
   POSTAL_CODE          nvarchar(50)         not null,
   COUNTRY              nvarchar(50)         not null,
   TELEPHONE            nvarchar(50)         not null,
   EMAIL                nvarchar(128)        null,
   DISCOUNT_PERCENT     int                  not null default 0,
   DATE_REG             datetime             not null,
   CUSTOMER_TYPE        smallint             not null default 0,
   NEWSLETTER_ENABLED   bit                  not null default 1,
   DISCOUNT_PERCENT2    float                not null default 0
)
go

alter table dbo.CUSTOMER
   add constraint PK_CUSTOMER primary key (CustomerID)
go

/*==============================================================*/
/* Index: XFK_CUSTOMER__COMPANY                                 */
/*==============================================================*/
create index XFK_CUSTOMER__COMPANY on dbo.CUSTOMER (
COMPANY_ID ASC
)
go

/*==============================================================*/
/* Index: XFK_PAGE_ID                                           */
/*==============================================================*/
create index XFK_PAGE_ID on dbo.CUSTOMER (
PageID ASC
)
go

/*==============================================================*/
/* Index: X_USERNAME                                            */
/*==============================================================*/
create index X_USERNAME on dbo.CUSTOMER (
USERNAME ASC
)
go

/*==============================================================*/
/* Index: X_CAMPAIGN                                            */
/*==============================================================*/
create index X_CAMPAIGN on dbo.CUSTOMER (
CampaignID ASC
)
go

/*==============================================================*/
/* Table: CWP_ARTICLE                                           */
/*==============================================================*/
create table dbo.CWP_ARTICLE (
   CWP_ID               uniqueidentifier     not null,
   IMG_TYPE_THUMB       int                  null,
   SHOW_IMAGE           bit                  not null default 1,
   LIST_SHOW_ABSTRACT   bit                  not null default 1,
   LIST_SHOW_ARCHIVE    bit                  not null default 1
)
go

alter table dbo.CWP_ARTICLE
   add constraint PK_CWP_ARTICLE primary key (CWP_ID)
go

/*==============================================================*/
/* Table: CWP_CONTACT_FORM                                      */
/*==============================================================*/
create table dbo.CWP_CONTACT_FORM (
   CWP_ID               uniqueidentifier     not null,
   MAIL_TO              nvarchar(256)        null,
   SUBJECT              nvarchar(256)        null
)
go

alter table dbo.CWP_CONTACT_FORM
   add constraint PK_CWP_CONTACT_FORM primary key (CWP_ID)
go

/*==============================================================*/
/* Table: CWP_GALLERY                                           */
/*==============================================================*/
create table dbo.CWP_GALLERY (
   CWP_ID               uniqueidentifier     not null,
   THUMB_TYPE_ID        int                  null,
   BIG_TYPE_ID          int                  null
)
go

alter table dbo.CWP_GALLERY
   add constraint PK_CWP_GALLERY primary key (CWP_ID)
go

/*==============================================================*/
/* Index: X_THUMB                                               */
/*==============================================================*/
create index X_THUMB on dbo.CWP_GALLERY (
THUMB_TYPE_ID ASC
)
go

/*==============================================================*/
/* Index: X_BIG                                                 */
/*==============================================================*/
create index X_BIG on dbo.CWP_GALLERY (
BIG_TYPE_ID ASC
)
go

/*==============================================================*/
/* Table: CWP_MAP                                               */
/*==============================================================*/
create table dbo.CWP_MAP (
   CWP_ID               uniqueidentifier     not null,
   LAT                  float                not null,
   LNG                  float                not null,
   ZOOM                 int                  not null,
   MARKER_LAT           float                null,
   MARKER_LNG           float                null,
   TITLE                nvarchar(256)        null,
   ADDRESS              nvarchar(512)        null,
   SRCH_ADDRESS         nvarchar(512)        null,
   HEIGHT               int                  null,
   WIDTH                int                  null,
   SHOW_INFO            bit                  not null default 1,
   MAP_TYPE             nvarchar(128)        null
)
go

alter table dbo.CWP_MAP
   add constraint PK_CWP_MAP primary key (CWP_ID)
go

/*==============================================================*/
/* Table: EM_USER                                               */
/*==============================================================*/
create table dbo.EM_USER (
   UserId               uniqueidentifier     not null,
   USERNAME             nvarchar(256)        not null,
   LastUpdatedDate      datetime             not null,
   COMPANY_ID           int                  null,
   ADDR_ID              int                  null,
   THEME_ID             int                  null,
   MASTER_ID            int                  null,
   IMG_LOGO_ID          int                  null,
   TYPE_LOGO_ID         int                  null,
   IMG_HEADER_ID        int                  null,
   TYPE_HEADER_ID       int                  null,
   PAGE_NAME            nvarchar(256)        not null,
   PAGE_TITLE           nvarchar(256)        null,
   FIRSTNAME            nvarchar(256)        null,
   LASTNAME             nvarchar(256)        null,
   EMAIL                nvarchar(256)        not null,
   PAYED                bit                  not null default 0,
   PAYED_PRICE          money                null,
   DATE_REG             datetime             null,
   DATE_VALID           datetime             null,
   DATE_ORDER_CHANGED   datetime             null,
   DATE_PAYED           datetime             null,
   SITEMAP_COUNT        int                  not null default 1,
   ACTIVE_USER          bit                  not null default 1,
   ACTIVE_WWW           bit                  not null default 1,
   STEPS_DONE           int                  not null default 0,
   IMG_HEADER           nvarchar(256)        null,
   IMG_LOGO             nvarchar(256)        null,
   LOGO_HREF            nvarchar(256)        null,
   CONT_NAME            nvarchar(256)        null,
   CONT_STREET          nvarchar(256)        null,
   CONT_POSTAL_CODE     nvarchar(256)        null,
   CONT_CITY            nvarchar(256)        null,
   CONT_EMAIL           nvarchar(256)        null,
   CONT_PHONE           nvarchar(256)        null,
   CONT_FAX             nvarchar(256)        null,
   CONT_MOBILE          nvarchar(256)        null,
   SHOW_NAME            bit                  not null default 1,
   SHOW_ADDRESS         bit                  not null default 1,
   SHOW_EMAIL           bit                  not null default 1,
   SHOW_PHONE           bit                  not null default 1,
   SHOW_MOBILE          bit                  not null default 1,
   SHOW_FAX             bit                  not null default 1,
   SHOW_LOGO            bit                  not null default 1,
   SHOW_TEXT            bit                  not null default 0,
   IS_MULTILINGUAL      bit                  not null default 0,
   IS_DEMO              bit                  not null default 0,
   IS_EXAMPLE_TEMPLATE  bit                  not null default 0,
   CODE                 int                  identity,
   USER_TYPE            int                  not null default 1,
   PORTAL_ID            smallint             not null default 1,
   CUSTOM_DOMAIN_ACTIVATED bit                  not null default 0,
   CUSTOM_DOMAIN        nvarchar(256)        null,
   DISCOUNT_TYPE        smallint             not null default 0,
   BONUS_USER_ID        uniqueidentifier     null,
   CUSTOM_CSS_RULES     nvarchar(MAX)        null,
   GA_TRACKER           nvarchar(256)        null
)
go

alter table dbo.EM_USER
   add constraint PK_EM_USER primary key (UserId)
go

alter table dbo.EM_USER
   add constraint AK_EM_USER_PAGE_NAME unique (PAGE_NAME)
go

alter table dbo.EM_USER
   add constraint AK_EM_USER_USERNAME unique (USERNAME)
go

/*==============================================================*/
/* Index: X_USER_ID                                             */
/*==============================================================*/
create index X_USER_ID on dbo.EM_USER (
UserId ASC
)
go

/*==============================================================*/
/* Index: X_PORTAL                                              */
/*==============================================================*/
create index X_PORTAL on dbo.EM_USER (
PORTAL_ID ASC
)
go

/*==============================================================*/
/* Index: X_CUSTOM_DOMAIN                                       */
/*==============================================================*/
create index X_CUSTOM_DOMAIN on dbo.EM_USER (
CUSTOM_DOMAIN ASC
)
go

/*==============================================================*/
/* Index: X_USER_TYPE                                           */
/*==============================================================*/
create index X_USER_TYPE on dbo.EM_USER (
USER_TYPE ASC
)
go

/*==============================================================*/
/* Table: EM_USER_CULTURE                                       */
/*==============================================================*/
create table dbo.EM_USER_CULTURE (
   LCID                 int                  not null,
   UserId               uniqueidentifier     not null,
   "DEFAULT"            bit                  not null default 0,
   ACTIVE               bit                  not null default 0,
   SORT                 int                  not null default 100
)
go

alter table dbo.EM_USER_CULTURE
   add constraint PK_EM_USER_CULTURE primary key (LCID, UserId)
go

/*==============================================================*/
/* Index: X_DEFAULT                                             */
/*==============================================================*/
create index X_DEFAULT on dbo.EM_USER_CULTURE (
"DEFAULT" ASC
)
go

/*==============================================================*/
/* Index: X_ACTIVE                                              */
/*==============================================================*/
create index X_ACTIVE on dbo.EM_USER_CULTURE (
ACTIVE ASC
)
go

/*==============================================================*/
/* Index: X_SORT                                                */
/*==============================================================*/
create index X_SORT on dbo.EM_USER_CULTURE (
SORT ASC
)
go

/*==============================================================*/
/* Table: ERROR_LOG                                             */
/*==============================================================*/
create table dbo.ERROR_LOG (
   ErrorID              int                  identity,
   Message              nvarchar(1000)       not null,
   StackTrace           nvarchar(MAX)        not null,
   Username             nvarchar(256)        null,
   IP                   nvarchar(256)        null,
   DateError            datetime             not null
)
go

alter table dbo.ERROR_LOG
   add constraint PK_ERROR_LOG primary key (ErrorID)
go

/*==============================================================*/
/* Index: X_DATE                                                */
/*==============================================================*/
create index X_DATE on dbo.ERROR_LOG (
DateError ASC
)
go

/*==============================================================*/
/* Table: "FILE"                                                */
/*==============================================================*/
create table dbo."FILE" (
   FileID               uniqueidentifier     not null,
   UserId               uniqueidentifier     not null,
   RefID                uniqueidentifier     null,
   Name                 nvarchar(256)        not null,
   "Desc"               nvarchar(256)        not null,
   Type                 nvarchar(256)        not null,
   DateAdded            datetime             not null,
   RolesDownload        nvarchar(512)        not null,
   Size                 bigint               not null default 0,
   Folder               nvarchar(256)        not null,
   FileType             smallint             not null default 0
)
go

alter table dbo."FILE"
   add constraint PK_FILE primary key (FileID)
go

/*==============================================================*/
/* Index: X_DESC                                                */
/*==============================================================*/
create index X_DESC on dbo."FILE" (
"Desc" ASC
)
go

/*==============================================================*/
/* Index: X_NAME                                                */
/*==============================================================*/
create index X_NAME on dbo."FILE" (
Name ASC
)
go

/*==============================================================*/
/* Index: X_TYPE                                                */
/*==============================================================*/
create index X_TYPE on dbo."FILE" (
Type ASC
)
go

/*==============================================================*/
/* Index: X_DATE                                                */
/*==============================================================*/
create index X_DATE on dbo."FILE" (
DateAdded ASC
)
go

/*==============================================================*/
/* Index: X_REFID                                               */
/*==============================================================*/
create index X_REFID on dbo."FILE" (
RefID ASC
)
go

/*==============================================================*/
/* Table: GALLERY                                               */
/*==============================================================*/
create table dbo.GALLERY (
   GAL_ID               int                  identity,
   GAL_TYPE             int                  null,
   USERNAME             nvarchar(256)        null,
   MAIN_IMG_ID          int                  null,
   ENABLED_VOTING       bit                  not null default 0
)
go

alter table dbo.GALLERY
   add constraint PK_GALLERY primary key (GAL_ID)
go

/*==============================================================*/
/* Index: XFK_GAL_TYPE                                          */
/*==============================================================*/
create index XFK_GAL_TYPE on dbo.GALLERY (
GAL_TYPE ASC
)
go

/*==============================================================*/
/* Table: GALLERY_IMAGE_TYPE                                    */
/*==============================================================*/
create table dbo.GALLERY_IMAGE_TYPE (
   GAL_ID               int                  not null,
   TYPE_ID              int                  not null
)
go

alter table dbo.GALLERY_IMAGE_TYPE
   add constraint PK_GALLERY_IMAGE_TYPE primary key (GAL_ID, TYPE_ID)
go

/*==============================================================*/
/* Table: GALLERY_LANG                                          */
/*==============================================================*/
create table dbo.GALLERY_LANG (
   GAL_ID               int                  not null,
   LANG_ID              nchar(2)             not null,
   GAL_TITLE            nvarchar(256)        not null,
   GAL_DESC             nvarchar(1024)       null
)
go

alter table dbo.GALLERY_LANG
   add constraint PK_GALLERY_LANG primary key (GAL_ID, LANG_ID)
go

/*==============================================================*/
/* Table: GALLERY_TYPE                                          */
/*==============================================================*/
create table dbo.GALLERY_TYPE (
   GAL_TYPE             int                  identity,
   GAL_TITLE            nvarchar(256)        not null,
   GAL_DESC             nvarchar(512)        not null,
   IS_PUBLIC            bit                  not null default 0
)
go

alter table dbo.GALLERY_TYPE
   add constraint PK_GALLERY_TYPE primary key (GAL_TYPE)
go

/*==============================================================*/
/* Index: X_IS_PUBLIC                                           */
/*==============================================================*/
create index X_IS_PUBLIC on dbo.GALLERY_TYPE (
IS_PUBLIC ASC
)
go

/*==============================================================*/
/* Table: GALLERY_TYPE_IMAGE_TYPE                               */
/*==============================================================*/
create table dbo.GALLERY_TYPE_IMAGE_TYPE (
   GAL_TYPE             int                  not null,
   TYPE_ID              int                  not null
)
go

alter table dbo.GALLERY_TYPE_IMAGE_TYPE
   add constraint PK_GALLERY_TYPE_IMAGE_TYPE primary key (GAL_TYPE, TYPE_ID)
go

/*==============================================================*/
/* Table: GROUP_ATTRIBUTE                                       */
/*==============================================================*/
create table dbo.GROUP_ATTRIBUTE (
   GroupAttID           uniqueidentifier     not null,
   PageId               uniqueidentifier     null,
   Title                nvarchar(512)        not null,
   "Desc"               nvarchar(512)        null,
   OrderByField         nvarchar(256)        null
)
go

alter table dbo.GROUP_ATTRIBUTE
   add constraint PK_GROUP_ATTRIBUTE primary key (GroupAttID)
go

/*==============================================================*/
/* Index: X_PAGE_ID                                             */
/*==============================================================*/
create index X_PAGE_ID on dbo.GROUP_ATTRIBUTE (
PageId ASC
)
go

/*==============================================================*/
/* Table: GROUP_ATTRIBUTE_LANG                                  */
/*==============================================================*/
create table dbo.GROUP_ATTRIBUTE_LANG (
   LANG_ID              nchar(2)             not null,
   GroupAttID           uniqueidentifier     not null,
   Title                nvarchar(256)        not null,
   "Desc"               nvarchar(512)        null,
   Message              nvarchar(256)        not null
)
go

alter table dbo.GROUP_ATTRIBUTE_LANG
   add constraint PK_GROUP_ATTRIBUTE_LANG primary key (LANG_ID, GroupAttID)
go

/*==============================================================*/
/* Table: IMAGE                                                 */
/*==============================================================*/
create table dbo.IMAGE (
   IMG_ID               int                  not null,
   TYPE_ID              int                  not null,
   IMG_NAME             nvarchar(256)        not null,
   IMG_URL              nvarchar(512)        null,
   "PUBLIC"             bit                  not null default 1,
   DATE_ADDED           datetime             not null default getdate(),
   ROLES                nvarchar(256)        null
)
go

alter table dbo.IMAGE
   add constraint PK_IMAGE primary key (IMG_ID, TYPE_ID)
go

/*==============================================================*/
/* Table: IMAGE_GALLERY                                         */
/*==============================================================*/
create table dbo.IMAGE_GALLERY (
   GAL_ID               int                  not null,
   IMG_ID               int                  not null,
   IMG_ORDER            int                  not null default 100
)
go

alter table dbo.IMAGE_GALLERY
   add constraint PK_IMAGE_GALLERY primary key (GAL_ID, IMG_ID)
go

/*==============================================================*/
/* Index: X_IMAGE_GAL_ORDER                                     */
/*==============================================================*/
create index X_IMAGE_GAL_ORDER on dbo.IMAGE_GALLERY (
IMG_ORDER ASC
)
go

/*==============================================================*/
/* Table: IMAGE_LANG                                            */
/*==============================================================*/
create table dbo.IMAGE_LANG (
   IMG_ID               int                  not null,
   LANG_ID              nchar(2)             not null,
   IMG_TITLE            nvarchar(256)        null,
   IMG_DESC             nvarchar(1024)       null
)
go

alter table dbo.IMAGE_LANG
   add constraint PK_IMAGE_LANG primary key (IMG_ID, LANG_ID)
go

/*==============================================================*/
/* Table: IMAGE_ORIG                                            */
/*==============================================================*/
create table dbo.IMAGE_ORIG (
   IMG_ID               int                  identity,
   IMG_NAME             nvarchar(256)        not null,
   IMG_URL              nvarchar(512)        null,
   IMG_FOLDER           nvarchar(256)        null,
   USERNAME             nvarchar(256)        null,
   IMG_AUTHOR           nvarchar(256)        null,
   IMG_ORDER            int                  not null default 100,
   IMG_ACTIVE           bit                  not null default 1,
   DATE_ADDED           datetime             not null default getdate(),
   VOTES                int                  not null default 0,
   TOTAL_RATING         int                  not null default 0,
   SIZE                 bigint               not null default 0
)
go

alter table dbo.IMAGE_ORIG
   add constraint PK_IMAGE_ORIG primary key (IMG_ID)
go

/*==============================================================*/
/* Index: X_IMAGE_ORIG_ORDER                                    */
/*==============================================================*/
create index X_IMAGE_ORIG_ORDER on dbo.IMAGE_ORIG (
IMG_ORDER ASC
)
go

/*==============================================================*/
/* Index: X_IMAGE_ORIG_ACTIVE                                   */
/*==============================================================*/
create index X_IMAGE_ORIG_ACTIVE on dbo.IMAGE_ORIG (
IMG_ACTIVE ASC
)
go

/*==============================================================*/
/* Index: X_USERNAME                                            */
/*==============================================================*/
create index X_USERNAME on dbo.IMAGE_ORIG (
USERNAME ASC
)
go

/*==============================================================*/
/* Index: X_DATE_ADDED                                          */
/*==============================================================*/
create index X_DATE_ADDED on dbo.IMAGE_ORIG (
DATE_ADDED ASC
)
go

/*==============================================================*/
/* Table: IMAGE_TYPE                                            */
/*==============================================================*/
create table dbo.IMAGE_TYPE (
   TYPE_ID              int                  identity,
   TYPE_TITLE           nvarchar(256)        not null,
   "DESC"               nvarchar(512)        null,
   WIDTH                int                  not null default 0,
   HEIGHT               int                  not null default 0,
   W_MIN                int                  not null default 0,
   W_MAX                int                  not null default 0,
   H_MIN                int                  not null default 0,
   H_MAX                int                  not null default 0,
   ORIG_HEIGHT          int                  not null default 0,
   ORIG_SIZE            int                  not null default 0,
   ORIG_WIDTH           int                  not null default 0,
   SIZE_MAX             int                  not null default 0,
   CROP_POSITION        int                  not null default 0,
   FILE_TYPE            nvarchar(128)        null,
   T_TYPE               smallint             not null default 1
)
go

alter table dbo.IMAGE_TYPE
   add constraint PK_IMAGE_TYPE primary key (TYPE_ID)
go

/*==============================================================*/
/* Index: X_T_TYPE                                              */
/*==============================================================*/
create index X_T_TYPE on dbo.IMAGE_TYPE (
T_TYPE ASC
)
go

/*==============================================================*/
/* Table: JOB                                                   */
/*==============================================================*/
create table dbo.JOB (
   JobID                uniqueidentifier     not null,
   Title                nvarchar(256)        not null,
   "Desc"               nvarchar(1024)       null,
   DateNext             datetime             not null,
   PeriodDatePart       nvarchar(256)        not null,
   PeriodNumber         int                  not null,
   DateEnd              datetime             null,
   Active               bit                  not null default 1
)
go

alter table dbo.JOB
   add constraint PK_JOB primary key (JobID)
go

/*==============================================================*/
/* Index: X_DATE_END                                            */
/*==============================================================*/
create index X_DATE_END on dbo.JOB (
DateEnd ASC
)
go

/*==============================================================*/
/* Table: JOB_EMAIL                                             */
/*==============================================================*/
create table dbo.JOB_EMAIL (
   JobEmailID           int                  identity,
   JobLogID             int                  not null,
   ReferenceID          uniqueidentifier     not null,
   Email                nvarchar(256)        not null,
   DateSent             datetime             not null,
   Notes                nvarchar(512)        null
)
go

alter table dbo.JOB_EMAIL
   add constraint PK_JOB_EMAIL primary key (JobEmailID)
go

/*==============================================================*/
/* Index: X_EMAIL                                               */
/*==============================================================*/
create index X_EMAIL on dbo.JOB_EMAIL (
JobLogID ASC,
ReferenceID ASC,
Email ASC
)
go

/*==============================================================*/
/* Index: X_DATE                                                */
/*==============================================================*/
create index X_DATE on dbo.JOB_EMAIL (
ReferenceID ASC,
Email ASC,
DateSent ASC
)
go

/*==============================================================*/
/* Table: JOB_LOG                                               */
/*==============================================================*/
create table dbo.JOB_LOG (
   JobLogID             int                  identity,
   JobID                uniqueidentifier     not null,
   DateStart            datetime             not null,
   DateExecution        datetime             null,
   Notes                nvarchar(512)        null,
   Finished             bit                  not null default 0
)
go

alter table dbo.JOB_LOG
   add constraint PK_JOB_LOG primary key (JobLogID)
go

/*==============================================================*/
/* Index: XFK_JOB_ID                                            */
/*==============================================================*/
create index XFK_JOB_ID on dbo.JOB_LOG (
JobID ASC
)
go

/*==============================================================*/
/* Index: X_DATESTART                                           */
/*==============================================================*/
create index X_DATESTART on dbo.JOB_LOG (
DateStart ASC
)
go

/*==============================================================*/
/* Index: X_DATE_EXECUTION                                      */
/*==============================================================*/
create index X_DATE_EXECUTION on dbo.JOB_LOG (
DateExecution ASC
)
go

/*==============================================================*/
/* Table: LANGUAGE                                              */
/*==============================================================*/
create table dbo.LANGUAGE (
   LANG_ID              nchar(2)             not null,
   LANG_DESC            nvarchar(256)        not null,
   LANG_ORDER           int                  not null default 100
)
go

alter table dbo.LANGUAGE
   add constraint PK_LANGUAGE primary key (LANG_ID)
go

/*==============================================================*/
/* Index: X_LANG_ORDER                                          */
/*==============================================================*/
create index X_LANG_ORDER on dbo.LANGUAGE (
LANG_ORDER ASC
)
go

/*==============================================================*/
/* Table: MANUFACTURER                                          */
/*==============================================================*/
create table dbo.MANUFACTURER (
   ManufacturerID       uniqueidentifier     not null default newid(),
   PageId               uniqueidentifier     null,
   Title                nvarchar(256)        not null,
   "Desc"               nvarchar(512)        not null
)
go

alter table dbo.MANUFACTURER
   add constraint PK_MANUFACTURER primary key (ManufacturerID)
go

/*==============================================================*/
/* Index: X_TITLE                                               */
/*==============================================================*/
create index X_TITLE on dbo.MANUFACTURER (
Title ASC
)
go

/*==============================================================*/
/* Index: X_PAGE                                                */
/*==============================================================*/
create index X_PAGE on dbo.MANUFACTURER (
PageId ASC
)
go

/*==============================================================*/
/* Table: MARKETING_CAMPAIGN                                    */
/*==============================================================*/
create table dbo.MARKETING_CAMPAIGN (
   CampaignID           uniqueidentifier     not null,
   CampaignPromoCode    nvarchar(256)        not null,
   MarketingUserId      uniqueidentifier     null,
   AffilliateUserId     uniqueidentifier     null,
   MarketingPercent     float                not null,
   MarketingPercentNextYear float                not null,
   AffilliatePercent    float                not null,
   DateValidFrom        datetime             null,
   DateValidTo          datetime             null,
   DateCreated          datetime             not null,
   CreatedBy            nvarchar(256)        not null,
   Name                 nvarchar(256)        not null,
   "Desc"               nvarchar(512)        null,
   UserDiscount         float                null,
   DiscountType         smallint             null,
   Active               bit                  not null default 1,
   AffilliateType       smallint             not null default 0
)
go

alter table dbo.MARKETING_CAMPAIGN
   add constraint PK_MARKETING_CAMPAIGN primary key (CampaignID)
go

alter table dbo.MARKETING_CAMPAIGN
   add constraint AK_MARKETING_PROMO_CODE unique (CampaignPromoCode)
go

/*==============================================================*/
/* Index: XFK_AFFILIATE                                         */
/*==============================================================*/
create index XFK_AFFILIATE on dbo.MARKETING_CAMPAIGN (
AffilliateUserId ASC
)
go

/*==============================================================*/
/* Index: XFK_MARKETING                                         */
/*==============================================================*/
create index XFK_MARKETING on dbo.MARKETING_CAMPAIGN (
MarketingUserId ASC
)
go

/*==============================================================*/
/* Table: MASTER                                                */
/*==============================================================*/
create table dbo.MASTER (
   MASTER_ID            int                  identity,
   MGR_ID               int                  null,
   IMG_TYPE_ID          int                  null,
   HDR_TYPE_ID          int                  null,
   MASTER               nvarchar(256)        not null,
   MASTER_TITLE         nvarchar(256)        not null,
   MASTER_DESC          nvarchar(512)        null,
   "ORDER"              int                  not null default 100,
   ACTIVE               bit                  not null default 1,
   IS_PUBLIC            bit                  not null default 1,
   USERNAME             nvarchar(256)        null,
   HAS_HEADER_IMAGE     bit                  not null default 0,
   ALLWAYS_SHOW_TITLE   bit                  not null default 0,
   IMG_BIG              nvarchar(256)        null,
   IMG_THUMB            nvarchar(256)        null
)
go

alter table dbo.MASTER
   add constraint PK_MASTER primary key (MASTER_ID)
go

/*==============================================================*/
/* Index: X_ORDER                                               */
/*==============================================================*/
create index X_ORDER on dbo.MASTER (
"ORDER" ASC
)
go

/*==============================================================*/
/* Index: X_ACTIVE                                              */
/*==============================================================*/
create index X_ACTIVE on dbo.MASTER (
ACTIVE ASC
)
go

/*==============================================================*/
/* Index: X_IS_PUBLIC                                           */
/*==============================================================*/
create index X_IS_PUBLIC on dbo.MASTER (
IS_PUBLIC ASC
)
go

/*==============================================================*/
/* Table: MASTER_GROUP                                          */
/*==============================================================*/
create table dbo.MASTER_GROUP (
   MGR_ID               int                  identity,
   GROUP_TITLE          nvarchar(256)        not null,
   GROUP_DESC           nvarchar(512)        null,
   "ORDER"              int                  not null default 100,
   IS_PUBLIC            bit                  not null default 1
)
go

alter table dbo.MASTER_GROUP
   add constraint PK_MASTER_GROUP primary key (MGR_ID)
go

/*==============================================================*/
/* Index: X_ORDER                                               */
/*==============================================================*/
create index X_ORDER on dbo.MASTER_GROUP (
"ORDER" ASC
)
go

/*==============================================================*/
/* Table: MASTER_PAGEM                                          */
/*==============================================================*/
create table dbo.MASTER_PAGEM (
   MASTER_ID            int                  not null,
   PMOD_ID              int                  not null,
   "DESC"               nvarchar(512)        null
)
go

alter table dbo.MASTER_PAGEM
   add constraint PK_MASTER_PAGEM primary key (MASTER_ID, PMOD_ID)
go

/*==============================================================*/
/* Table: MENU                                                  */
/*==============================================================*/
create table dbo.MENU (
   MENU_ID              int                  identity,
   MENU_TITLE           nvarchar(256)        not null,
   USERNAME             nvarchar(256)        null,
   MENU_ACTIVE          bit                  not null default 1,
   MENU_CMS             bit                  not null default 0
)
go

alter table dbo.MENU
   add constraint PK_MENU primary key (MENU_ID)
go

/*==============================================================*/
/* Index: X_MENU_USERNAME                                       */
/*==============================================================*/
create index X_MENU_USERNAME on dbo.MENU (
USERNAME ASC
)
go

/*==============================================================*/
/* Index: X_MENU_ACTIVE                                         */
/*==============================================================*/
create index X_MENU_ACTIVE on dbo.MENU (
MENU_ACTIVE ASC
)
go

/*==============================================================*/
/* Index: X_MENU_CMS                                            */
/*==============================================================*/
create index X_MENU_CMS on dbo.MENU (
MENU_CMS ASC
)
go

/*==============================================================*/
/* Table: MODULE                                                */
/*==============================================================*/
create table dbo.MODULE (
   MOD_ID               int                  identity,
   MOD_TITLE            nvarchar(256)        not null,
   MOD_DESC             nvarchar(512)        null,
   CMS_ROLES            nvarchar(256)        null,
   WWW_ROLES            nvarchar(256)        null,
   MOD_SRC              nvarchar(256)        null,
   MOD_ORDER            int                  not null default 0,
   MOD_OBJ_ID           nvarchar(256)        null,
   EM_ACTIVE            bit                  not null default 1,
   EM_FREE              bit                  not null default 0,
   EM_PRICE             money                not null default 0,
   EM_BUY_TYPE          int                  not null default 0,
   EM_BUY_QUANTITY      int                  not null default 1,
   EM_TITLE             nvarchar(256)        null,
   EM_DESC              nvarchar(MAX)        null,
   EM_NAME              nvarchar(256)        null
)
go

alter table dbo.MODULE
   add constraint PK_MODULE primary key (MOD_ID)
go

/*==============================================================*/
/* Index: X_MOD_ORDER                                           */
/*==============================================================*/
create index X_MOD_ORDER on dbo.MODULE (
MOD_ORDER ASC
)
go

/*==============================================================*/
/* Index: X_MOD_OBJ_ID                                          */
/*==============================================================*/
create index X_MOD_OBJ_ID on dbo.MODULE (
MOD_OBJ_ID ASC
)
go

/*==============================================================*/
/* Index: X_ACTIVE                                              */
/*==============================================================*/
create index X_ACTIVE on dbo.MODULE (
EM_ACTIVE ASC
)
go

/*==============================================================*/
/* Table: NEWSLETTER                                            */
/*==============================================================*/
create table dbo.NEWSLETTER (
   NewsletterID         uniqueidentifier     not null,
   Title                nvarchar(256)        not null,
   "Desc"               nvarchar(512)        null,
   Body                 nvarchar(MAX)        not null,
   Subject              nvarchar(256)        not null,
   Email                nvarchar(256)        not null,
   Password             nvarchar(256)        not null,
   EmailName            nvarchar(256)        null,
   Status               smallint             not null default 0,
   DateCreated          datetime             not null,
   DateSent             datetime             null,
   CreatedBy            uniqueidentifier     not null,
   SentBy               uniqueidentifier     null,
   AttachmentFileName   nvarchar(512)        null,
   NewsletterType       smallint             not null default 1
)
go

alter table dbo.NEWSLETTER
   add constraint PK_NEWSLETTER primary key (NewsletterID)
go

/*==============================================================*/
/* Index: X_TYPE                                                */
/*==============================================================*/
create index X_TYPE on dbo.NEWSLETTER (
NewsletterType ASC
)
go

/*==============================================================*/
/* Table: NEWSLETTER_EMAIL                                      */
/*==============================================================*/
create table dbo.NEWSLETTER_EMAIL (
   EmailID              int                  identity,
   NewsletterID         uniqueidentifier     not null,
   Email                nvarchar(256)        not null,
   Status               smallint             not null default 0,
   Notes                nvarchar(1024)       null,
   UserId               uniqueidentifier     null
)
go

alter table dbo.NEWSLETTER_EMAIL
   add constraint PK_NEWSLETTER_EMAIL primary key (EmailID)
go

/*==============================================================*/
/* Index: XFK_NEWSLETTER                                        */
/*==============================================================*/
create index XFK_NEWSLETTER on dbo.NEWSLETTER_EMAIL (
NewsletterID ASC
)
go

/*==============================================================*/
/* Index: X_EMAIL                                               */
/*==============================================================*/
create index X_EMAIL on dbo.NEWSLETTER_EMAIL (
Email ASC
)
go

/*==============================================================*/
/* Table: OPINION                                               */
/*==============================================================*/
create table dbo.OPINION (
   OPINION_ID           uniqueidentifier     not null,
   PRODUCT_ID           uniqueidentifier     null default newid(),
   LANG_ID              nchar(2)             null,
   "USER"               nvarchar(50)         not null,
   TITLE                nvarchar(256)        not null,
   TEXT                 nvarchar(max)        not null,
   RATING               int                  not null default 0,
   ADDED_DATE           datetime             not null default getdate(),
   RECOMMENDS           bit                  null
)
go

alter table dbo.OPINION
   add constraint PK_OPINION primary key (OPINION_ID)
go

/*==============================================================*/
/* Index: XFK_OPINION__LANGUAGE                                 */
/*==============================================================*/
create index XFK_OPINION__LANGUAGE on dbo.OPINION (
LANG_ID ASC
)
go

/*==============================================================*/
/* Index: XFK_OPINION__PRODUCT                                  */
/*==============================================================*/
create index XFK_OPINION__PRODUCT on dbo.OPINION (
PRODUCT_ID ASC
)
go

/*==============================================================*/
/* Table: PAGEM_MOD                                             */
/*==============================================================*/
create table dbo.PAGEM_MOD (
   PMOD_ID              int                  not null,
   MOD_ID               int                  not null
)
go

alter table dbo.PAGEM_MOD
   add constraint PK_PAGEM_MOD primary key (PMOD_ID, MOD_ID)
go

/*==============================================================*/
/* Table: PAGE_MODULE                                           */
/*==============================================================*/
create table dbo.PAGE_MODULE (
   PMOD_ID              int                  identity,
   PMOD_TITLE           nvarchar(256)        not null,
   PMOD_DESC            nvarchar(512)        null,
   WWW_URL              nvarchar(256)        null,
   CMS_URL              nvarchar(256)        null,
   PMOD_ORDER           int                  not null default 100,
   THUMB_URL            nvarchar(256)        null,
   MOD_TYPE             smallint             not null default 0,
   EM_COL_NO            smallint             null
)
go

alter table dbo.PAGE_MODULE
   add constraint PK_PAGE_MODULE primary key (PMOD_ID)
go

/*==============================================================*/
/* Index: X_PMOD_ORDER                                          */
/*==============================================================*/
create index X_PMOD_ORDER on dbo.PAGE_MODULE (
PMOD_ORDER ASC
)
go

/*==============================================================*/
/* Index: X_MOD_TYPE                                            */
/*==============================================================*/
create index X_MOD_TYPE on dbo.PAGE_MODULE (
MOD_TYPE ASC
)
go

/*==============================================================*/
/* Index: X_COL_NO                                              */
/*==============================================================*/
create index X_COL_NO on dbo.PAGE_MODULE (
EM_COL_NO ASC
)
go

/*==============================================================*/
/* Table: PAYMENT                                               */
/*==============================================================*/
create table dbo.PAYMENT (
   PaymentID            int                  identity,
   UserId               uniqueidentifier     not null,
   DatePayed            datetime             not null,
   DateAdded            datetime             not null,
   Price                money                not null,
   Type                 int                  not null,
   Notes                nvarchar(512)        null,
   AddedBy              nvarchar(256)        not null,
   RefID                uniqueidentifier     null
)
go

alter table dbo.PAYMENT
   add constraint PK_PAYMENT primary key (PaymentID)
go

/*==============================================================*/
/* Index: XFK_USER                                              */
/*==============================================================*/
create index XFK_USER on dbo.PAYMENT (
UserId ASC
)
go

/*==============================================================*/
/* Index: X_DATE_ADDED                                          */
/*==============================================================*/
create index X_DATE_ADDED on dbo.PAYMENT (
DateAdded ASC
)
go

/*==============================================================*/
/* Index: X_DATE_PAYED                                          */
/*==============================================================*/
create index X_DATE_PAYED on dbo.PAYMENT (
DatePayed ASC
)
go

/*==============================================================*/
/* Index: X_REF_ID                                              */
/*==============================================================*/
create index X_REF_ID on dbo.PAYMENT (
RefID ASC
)
go

/*==============================================================*/
/* Table: PERMISSION                                            */
/*==============================================================*/
create table dbo.PERMISSION (
   PermissionID         int                  identity,
   UserId               uniqueidentifier     not null,
   Scope                uniqueidentifier     not null,
   Object               uniqueidentifier     not null,
   Action               smallint             not null,
   Type                 smallint             not null,
   ObjectTitle          nvarchar(128)        not null,
   ScopeTitle           nvarchar(128)        not null,
   DateAdded            datetime             not null,
   AddedBy              uniqueidentifier     not null
)
go

alter table dbo.PERMISSION
   add constraint PK_PERMISSION primary key nonclustered (PermissionID)
go

alter table dbo.PERMISSION
   add constraint AK_AK_PERMISSION_PERMISSI unique clustered (UserId, Scope, Object, Action)
go

/*==============================================================*/
/* Index: X_USER                                                */
/*==============================================================*/
create index X_USER on dbo.PERMISSION (
UserId ASC
)
go

/*==============================================================*/
/* Table: POLL                                                  */
/*==============================================================*/
create table dbo.POLL (
   PollID               uniqueidentifier     not null,
   CWP_ID               uniqueidentifier     null,
   UserId               uniqueidentifier     null,
   DateAdded            datetime             not null,
   Active               bit                  not null default 0,
   BlockMode            smallint             not null default 0,
   "Order"              smallint             not null default 10
)
go

alter table dbo.POLL
   add constraint PK_POLL primary key (PollID)
go

/*==============================================================*/
/* Index: XFK_CWP_ID                                            */
/*==============================================================*/
create index XFK_CWP_ID on dbo.POLL (
CWP_ID ASC
)
go

/*==============================================================*/
/* Index: X_DATE                                                */
/*==============================================================*/
create index X_DATE on dbo.POLL (
DateAdded ASC
)
go

/*==============================================================*/
/* Table: POLL_CHOICE                                           */
/*==============================================================*/
create table dbo.POLL_CHOICE (
   PollChoiceID         uniqueidentifier     not null,
   PollID               uniqueidentifier     not null,
   LANG_ID              nchar(2)             not null,
   Choice               nvarchar(512)        not null,
   "Order"              smallint             not null default 10,
   VoteCountMale        int                  not null default 0,
   VoteCountFemale      int                  not null default 0
)
go

alter table dbo.POLL_CHOICE
   add constraint PK_POLL_CHOICE primary key (PollChoiceID)
go

/*==============================================================*/
/* Index: XFK_POLL_LANG                                         */
/*==============================================================*/
create index XFK_POLL_LANG on dbo.POLL_CHOICE (
PollID ASC,
LANG_ID ASC
)
go

/*==============================================================*/
/* Table: POLL_IPADDRESS                                        */
/*==============================================================*/
create table dbo.POLL_IPADDRESS (
   IPAddress            nvarchar(256)        not null,
   PollID               uniqueidentifier     not null
)
go

alter table dbo.POLL_IPADDRESS
   add constraint PK_POLL_IPADDRESS primary key (IPAddress, PollID)
go

/*==============================================================*/
/* Table: POLL_LANG                                             */
/*==============================================================*/
create table dbo.POLL_LANG (
   PollID               uniqueidentifier     not null,
   LANG_ID              nchar(2)             not null,
   PollQuestion         nvarchar(512)        not null,
   Active               bit                  not null
)
go

alter table dbo.POLL_LANG
   add constraint PK_POLL_LANG primary key (PollID, LANG_ID)
go

/*==============================================================*/
/* Table: PREDRACUN                                             */
/*==============================================================*/
create table dbo.PREDRACUN (
   PredracunID          numeric              identity,
   UserId               uniqueidentifier     not null,
   PredracunCode        nvarchar(256)        not null,
   DateAdded            datetime             not null,
   Price                decimal              not null,
   Notes                nvarchar(512)        null
)
go

alter table dbo.PREDRACUN
   add constraint PK_PREDRACUN primary key (PredracunID)
go

/*==============================================================*/
/* Index: XFK_USER                                              */
/*==============================================================*/
create index XFK_USER on dbo.PREDRACUN (
UserId ASC
)
go

/*==============================================================*/
/* Index: X_DATE_ADDED                                          */
/*==============================================================*/
create index X_DATE_ADDED on dbo.PREDRACUN (
DateAdded ASC
)
go

/*==============================================================*/
/* Table: PRODUCT                                               */
/*==============================================================*/
create table dbo.PRODUCT (
   PRODUCT_ID           uniqueidentifier     not null default newid(),
   ID_INT               int                  identity,
   PageId               uniqueidentifier     not null,
   SMAP_ID              int                  not null,
   PMOD_ID              int                  null,
   ManufacturerID       uniqueidentifier     null,
   CODE                 nvarchar(50)         not null,
   PRICE_NO_TAX         AS (PRICE/(1+TAX_RATE)),
   PRICE                money                not null,
   DISCOUNT_PERCENT     float                not null,
   SPECIAL_DISCOUNT_PERCENT int                  not null,
   UNITS_IN_STOCK       int                  not null,
   ADDED_BY             nvarchar(50)         not null,
   ADDED_DATE           datetime             not null default getdate(),
   DELETED              bit                  not null default 0,
   ACTIVE               bit                  not null default 1,
   PRODUCT_URL          nvarchar(512)        null,
   WARRANTY             int                  not null default 0,
   TOTAL_RATING         int                  not null default 0,
   VOTES                int                  not null default 0,
   IMPORTANCE           int                  not null default 0,
   PRODUCT_TYPE         int                  not null default 0,
   CAT_ACTION           bit                  not null default 0,
   CAT_SALE             bit                  not null,
   TAX_RATE             float                not null default 0.2
)
go

alter table dbo.PRODUCT
   add constraint PK_PRODUCT primary key (PRODUCT_ID)
go

/*==============================================================*/
/* Index: X_PRODUCT_CODE                                        */
/*==============================================================*/
create index X_PRODUCT_CODE on dbo.PRODUCT (
CODE ASC
)
go

/*==============================================================*/
/* Index: X_PAGE_ID                                             */
/*==============================================================*/
create index X_PAGE_ID on dbo.PRODUCT (
PageId ASC
)
go

/*==============================================================*/
/* Index: X_SMAP_ID                                             */
/*==============================================================*/
create index X_SMAP_ID on dbo.PRODUCT (
SMAP_ID ASC
)
go

/*==============================================================*/
/* Index: X_ID_INT                                              */
/*==============================================================*/
create index X_ID_INT on dbo.PRODUCT (
ID_INT ASC
)
go

/*==============================================================*/
/* Index: X_PRICE                                               */
/*==============================================================*/
create index X_PRICE on dbo.PRODUCT (
PRICE ASC
)
go

/*==============================================================*/
/* Index: XFK_MANUFACTURER                                      */
/*==============================================================*/
create index XFK_MANUFACTURER on dbo.PRODUCT (
ManufacturerID ASC
)
go

/*==============================================================*/
/* Index: X_STOCK                                               */
/*==============================================================*/
create index X_STOCK on dbo.PRODUCT (
UNITS_IN_STOCK ASC
)
go

/*==============================================================*/
/* Table: PRODUCT_ATTRIBUTE                                     */
/*==============================================================*/
create table dbo.PRODUCT_ATTRIBUTE (
   PRODUCT_ID           uniqueidentifier     not null default newid(),
   AttributeID          uniqueidentifier     not null
)
go

alter table dbo.PRODUCT_ATTRIBUTE
   add constraint PK_PRODUCT_ATTRIBUTE primary key (PRODUCT_ID, AttributeID)
go

/*==============================================================*/
/* Table: PRODUCT_DESC                                          */
/*==============================================================*/
create table dbo.PRODUCT_DESC (
   PRODUCT_ID           uniqueidentifier     not null default newid(),
   LANG_ID              nchar(2)             not null,
   NAME                 nvarchar(256)        not null,
   DESCRIPTION_LONG     nvarchar(max)        not null,
   DESCRIPTION_SHORT    nvarchar(1000)       null,
   PDescID              int                  identity
)
go

alter table dbo.PRODUCT_DESC
   add constraint PK_PRODUCT_DESC primary key (PRODUCT_ID, LANG_ID)
go

/*==============================================================*/
/* Index: X_NAME                                                */
/*==============================================================*/
create index X_NAME on dbo.PRODUCT_DESC (
NAME ASC
)
go

/*==============================================================*/
/* Index: XAK_PDESC_ID                                          */
/*==============================================================*/
create unique index XAK_PDESC_ID on dbo.PRODUCT_DESC (
PDescID ASC
)
go

/*==============================================================*/
/* Table: PRODUCT_GROUP_ATTRIBUTE                               */
/*==============================================================*/
create table dbo.PRODUCT_GROUP_ATTRIBUTE (
   PRODUCT_ID           uniqueidentifier     not null default newid(),
   GroupAttID           uniqueidentifier     not null
)
go

alter table dbo.PRODUCT_GROUP_ATTRIBUTE
   add constraint PK_PRODUCT_GROUP_ATTRIBUTE primary key (PRODUCT_ID, GroupAttID)
go

/*==============================================================*/
/* Table: PRODUCT_SMAP                                          */
/*==============================================================*/
create table dbo.PRODUCT_SMAP (
   PRODUCT_ID           uniqueidentifier     not null default newid(),
   SMAP_ID              int                  not null,
   Ordr                 int                  not null default 0
)
go

alter table dbo.PRODUCT_SMAP
   add constraint PK_PRODUCT_SMAP primary key (PRODUCT_ID, SMAP_ID)
go

/*==============================================================*/
/* Index: X_ORDER                                               */
/*==============================================================*/
create index X_ORDER on dbo.PRODUCT_SMAP (
SMAP_ID ASC,
Ordr ASC
)
go

/*==============================================================*/
/* Table: SHOPPING_ORDER                                        */
/*==============================================================*/
create table dbo.SHOPPING_ORDER (
   OrderID              uniqueidentifier     not null,
   PredracunID          numeric              null,
   UserId               uniqueidentifier     not null,
   TicketID             uniqueidentifier     null,
   PageId               uniqueidentifier     null,
   COMPANY_ID           int                  null,
   OrderRefID           int                  identity,
   DateStart            datetime             not null,
   DatePayed            datetime             null,
   Payed                bit                  not null default 0,
   OrderStatus          smallint             not null default 0,
   ShippingType         smallint             null,
   PaymentType          smallint             null,
   Total                money                not null,
   TotalWithTax         AS (Total + Tax),
   Tax                  money                not null,
   ShippingPrice        money                null default 0,
   ShippingFirstName    nvarchar(256)        null,
   ShippingLastName     nvarchar(256)        null,
   ShippingAddress      nvarchar(256)        null,
   ShippingPostal       nvarchar(256)        null,
   ShippingCity         nvarchar(256)        null,
   ShippingCountry      nvarchar(256)        null,
   ShippedDate          datetime             null,
   IsCompany            bit                  not null default 0
)
go

alter table dbo.SHOPPING_ORDER
   add constraint PK_SHOPPING_ORDER primary key (OrderID)
go

/*==============================================================*/
/* Index: XFK_USERID                                            */
/*==============================================================*/
create index XFK_USERID on dbo.SHOPPING_ORDER (
UserId ASC
)
go

/*==============================================================*/
/* Index: X_DATE                                                */
/*==============================================================*/
create index X_DATE on dbo.SHOPPING_ORDER (
DateStart ASC
)
go

/*==============================================================*/
/* Index: X_ORDER_REF                                           */
/*==============================================================*/
create index X_ORDER_REF on dbo.SHOPPING_ORDER (
OrderRefID ASC
)
go

/*==============================================================*/
/* Index: XFK_PAGE_ID                                           */
/*==============================================================*/
create index XFK_PAGE_ID on dbo.SHOPPING_ORDER (
PageId ASC
)
go

/*==============================================================*/
/* Table: SITEMAP                                               */
/*==============================================================*/
create table dbo.SITEMAP (
   SMAP_ID              int                  identity,
   SMAP_GUID            uniqueidentifier     not null default newid(),
   MENU_ID              int                  not null,
   PMOD_ID              int                  not null,
   SMAP_TITLE           nvarchar(256)        not null,
   PARENT               int                  null,
   SMAP_ACTIVE          bit                  not null default 1,
   WWW_ROLES            nvarchar(256)        not null default '*',
   CMS_ROLES            nvarchar(256)        not null default '*',
   SMAP_ORDER           int                  not null default 100,
   SMAP_IMAGE           nvarchar(256)        null,
   WWW_VISIBLE          bit                  not null default 1,
   VIEW_COUNT           int                  not null default 0,
   THEME                nvarchar(256)        null,
   CUSTOM_CLASS         nvarchar(256)        null,
   WWW_URL              nvarchar(256)        null
)
go

alter table dbo.SITEMAP
   add constraint PK_SITEMAP primary key (SMAP_ID)
go

/*==============================================================*/
/* Index: X_SMAP_PARENT                                         */
/*==============================================================*/
create index X_SMAP_PARENT on dbo.SITEMAP (
PARENT ASC
)
go

/*==============================================================*/
/* Index: X_SMAP_ORDER                                          */
/*==============================================================*/
create index X_SMAP_ORDER on dbo.SITEMAP (
SMAP_ORDER ASC
)
go

/*==============================================================*/
/* Index: XFK_SITEMAP_MENU                                      */
/*==============================================================*/
create index XFK_SITEMAP_MENU on dbo.SITEMAP (
MENU_ID ASC
)
go

/*==============================================================*/
/* Index: XFK_SITEMAP_MODULE                                    */
/*==============================================================*/
create index XFK_SITEMAP_MODULE on dbo.SITEMAP (
PMOD_ID ASC
)
go

/*==============================================================*/
/* Index: X_SMAP_GUID                                           */
/*==============================================================*/
create index X_SMAP_GUID on dbo.SITEMAP (
SMAP_GUID ASC
)
go

/*==============================================================*/
/* Table: SITEMAP_GALLERY                                       */
/*==============================================================*/
create table dbo.SITEMAP_GALLERY (
   SMAP_ID              int                  not null,
   GAL_ID               int                  not null,
   SMG_ORDER            int                  not null default 100,
   SMG_ACTIVE           bit                  not null default 1
)
go

alter table dbo.SITEMAP_GALLERY
   add constraint PK_SITEMAP_GALLERY primary key (GAL_ID, SMAP_ID)
go

/*==============================================================*/
/* Index: X_SMG_ORDER                                           */
/*==============================================================*/
create index X_SMG_ORDER on dbo.SITEMAP_GALLERY (
SMG_ORDER ASC
)
go

/*==============================================================*/
/* Index: X_SMG_ACTIVE                                          */
/*==============================================================*/
create index X_SMG_ACTIVE on dbo.SITEMAP_GALLERY (
SMG_ACTIVE ASC
)
go

/*==============================================================*/
/* Table: SITEMAP_LANG                                          */
/*==============================================================*/
create table dbo.SITEMAP_LANG (
   SMAP_ID              int                  not null,
   LANG_ID              nchar(2)             not null,
   SML_TITLE            nvarchar(256)        not null,
   SML_DESC             nvarchar(256)        null,
   SML_ACTIVE           bit                  not null default 1,
   SML_META_DESC        nvarchar(512)        null,
   SML_KEYWORDS         nvarchar(256)        null,
   SML_ORDER            int                  not null default 100,
   DIRTY                bit                  null default 1
)
go

alter table dbo.SITEMAP_LANG
   add constraint PK_SITEMAP_LANG primary key (SMAP_ID, LANG_ID)
go

/*==============================================================*/
/* Index: X_SML_ACTIVE                                          */
/*==============================================================*/
create index X_SML_ACTIVE on dbo.SITEMAP_LANG (
SML_ACTIVE ASC
)
go

/*==============================================================*/
/* Index: X_SML_ORDER                                           */
/*==============================================================*/
create index X_SML_ORDER on dbo.SITEMAP_LANG (
SML_ORDER ASC
)
go

/*==============================================================*/
/* Table: SITEMAP_MODULE                                        */
/*==============================================================*/
create table dbo.SITEMAP_MODULE (
   SMAP_ID              int                  not null,
   MOD_ID               int                  not null
)
go

alter table dbo.SITEMAP_MODULE
   add constraint PK_SITEMAP_MODULE primary key (SMAP_ID, MOD_ID)
go

/*==============================================================*/
/* Table: STATUS                                                */
/*==============================================================*/
create table dbo.STATUS (
   StatusID             int                  identity,
   TypeID               int                  not null,
   Title                nvarchar(256)        not null,
   "Desc"               nvarchar(512)        null,
   "Order"              int                  not null default 0,
   Active               bit                  not null default 1,
   IsFinished           bit                  not null default 0,
   IsCanceled           bit                  not null default 0
)
go

alter table dbo.STATUS
   add constraint PK_STATUS primary key (StatusID)
go

/*==============================================================*/
/* Index: X_TYPE                                                */
/*==============================================================*/
create index X_TYPE on dbo.STATUS (
TypeID ASC
)
go

/*==============================================================*/
/* Table: THEME                                                 */
/*==============================================================*/
create table dbo.THEME (
   THEME_ID             int                  identity,
   MASTER_ID            int                  not null,
   THEME                nvarchar(256)        not null,
   "ORDER"              int                  not null default 100,
   THEME_DESC           nvarchar(512)        null,
   IS_PUBLIC            bit                  not null default 1,
   USERNAME             nvarchar(256)        null,
   ACTIVE               bit                  not null default 1,
   IMG_THUMB            nvarchar(256)        null,
   IMG_BIG              nvarchar(256)        null
)
go

alter table dbo.THEME
   add constraint PK_THEME primary key (THEME_ID)
go

/*==============================================================*/
/* Index: XFK_MASTER_ID                                         */
/*==============================================================*/
create index XFK_MASTER_ID on dbo.THEME (
MASTER_ID ASC
)
go

/*==============================================================*/
/* Index: X_ORDER                                               */
/*==============================================================*/
create index X_ORDER on dbo.THEME (
"ORDER" ASC
)
go

/*==============================================================*/
/* Index: X_PUBLIC                                              */
/*==============================================================*/
create index X_PUBLIC on dbo.THEME (
IS_PUBLIC ASC
)
go

/*==============================================================*/
/* Table: TICKET                                                */
/*==============================================================*/
create table dbo.TICKET (
   TicketID             uniqueidentifier     not null,
   UserId               uniqueidentifier     not null,
   TicketTypeID         smallint             not null,
   StatusTypeID         int                  not null,
   StatusID             int                  not null,
   TicketRefID          int                  identity,
   DateAdded            datetime             not null,
   DateEnd              datetime             null,
   ParentTicketID       uniqueidentifier     null,
   Priority             smallint             not null,
   Title                nvarchar(256)        not null,
   "Desc"               nvarchar(MAX)        not null,
   Active               bit                  not null default 1
)
go

alter table dbo.TICKET
   add constraint PK_TICKET primary key (TicketID)
go

alter table dbo.TICKET
   add constraint AK_AK_REFID_TICKET unique (TicketRefID)
go

/*==============================================================*/
/* Index: XFK_USERID                                            */
/*==============================================================*/
create index XFK_USERID on dbo.TICKET (
UserId ASC
)
go

/*==============================================================*/
/* Table: USER_MODULE                                           */
/*==============================================================*/
create table dbo.USER_MODULE (
   MOD_ID               int                  not null,
   UserId               uniqueidentifier     not null,
   QUANTITY             int                  not null default 1,
   EM_PRICE             money                not null default 0
)
go

alter table dbo.USER_MODULE
   add constraint PK_USER_MODULE primary key (MOD_ID, UserId)
go

/*==============================================================*/
/* Table: VOTE                                                  */
/*==============================================================*/
create table dbo.VOTE (
   VoteID               uniqueidentifier     not null,
   TotalRating          int                  not null default 0,
   Votes                int                  not null default 0,
   AvgRating            AS (
   cast(
   CASE WHEN TotalRating = 0 OR Votes = 0 THEN 
      0 ELSE 
   TotalRating/Votes
    END
   as float)
   
   
   )
)
go

execute sp_addextendedproperty 'MS_Description', 
   'must be PERSISTED field!!!',
   'user', 'dbo', 'table', 'VOTE', 'column', 'AvgRating'
go

alter table dbo.VOTE
   add constraint PK_VOTE primary key (VoteID)
go

/*==============================================================*/
/* Index: X_AVG_RATING                                          */
/*==============================================================*/
create index X_AVG_RATING on dbo.VOTE (
AvgRating ASC
)
go

/*==============================================================*/
/* Table: VOTE_IPADDRESS                                        */
/*==============================================================*/
create table dbo.VOTE_IPADDRESS (
   IPAddress            nvarchar(256)        not null,
   VoteID               uniqueidentifier     not null
)
go

alter table dbo.VOTE_IPADDRESS
   add constraint PK_VOTE_IPADDRESS primary key (IPAddress, VoteID)
go

/*==============================================================*/
/* Table: ZONE_MODULE                                           */
/*==============================================================*/
create table dbo.ZONE_MODULE (
   ZMP_ID               int                  identity,
   PMOD_ID              int                  not null,
   MOD_ID               int                  not null,
   ZONE                 nvarchar(256)        not null,
   DEFAULT_TEXT         nvarchar(MAX)        null,
   DEFAULT_TITLE        nvarchar(256)        null,
   IS_MOVABLE           bit                  not null default 0
)
go

alter table dbo.ZONE_MODULE
   add constraint PK_ZONE_MODULE primary key (ZMP_ID)
go

/*==============================================================*/
/* Index: X_PMOD_ID                                             */
/*==============================================================*/
create index X_PMOD_ID on dbo.ZONE_MODULE (
PMOD_ID ASC
)
go

/*==============================================================*/
/* Index: X_MOD_ID                                              */
/*==============================================================*/
create index X_MOD_ID on dbo.ZONE_MODULE (
MOD_ID ASC
)
go

/*==============================================================*/
/* Index: X_ZONE                                                */
/*==============================================================*/
create index X_ZONE on dbo.ZONE_MODULE (
ZONE ASC
)
go

/*==============================================================*/
/* View: vw_GalleryDesc                                         */
/*==============================================================*/
create view vw_GalleryDesc as
SELECT L.LANG_DESC, G.GAL_TITLE, G.GAL_DESC,  L.LANG_ID, G.GAL_ID 
FROM LANGUAGE L LEFT OUTER JOIN GALLERY_LANG G ON L.LANG_ID = G.LANG_ID
go

/*==============================================================*/
/* View: vw_Image                                               */
/*==============================================================*/
create view dbo.vw_Image as
SELECT I.IMG_ID
      ,I.TYPE_ID
      ,I.IMG_NAME
      ,I.IMG_URL
      ,IMG_FOLDER
      ,USERNAME
      ,IMG_AUTHOR
      ,IMG_ORDER
      ,IMG_ACTIVE
      ,O.DATE_ADDED
      ,VOTES
      ,TOTAL_RATING
 FROM IMAGE I, IMAGE_ORIG O
 WHERE I.IMG_ID = O.IMG_ID
go

/*==============================================================*/
/* View: vw_ImageDesc                                           */
/*==============================================================*/
create view dbo.vw_ImageDesc as
SELECT L.LANG_DESC, I.IMG_ID, I.IMG_TITLE, I.IMG_DESC, L.LANG_ID 
FROM LANGUAGE L LEFT OUTER JOIN IMAGE_LANG I ON L.LANG_ID = I.LANG_ID
go

/*==============================================================*/
/* View: vw_ImageLang                                           */
/*==============================================================*/
create view dbo.vw_ImageLang as
SELECT I.IMG_ID
      ,L.LANG_ID
      ,IMG_NAME
      ,IMG_URL
      ,IMG_FOLDER
      ,USERNAME
      ,IMG_AUTHOR
      ,IMG_ORDER
      ,IMG_ACTIVE
      ,DATE_ADDED
      ,VOTES
      ,TOTAL_RATING
      ,L.IMG_TITLE
      ,L.IMG_DESC 
      , -1 AS TYPE_ID
 FROM IMAGE_ORIG I LEFT OUTER JOIN IMAGE_LANG L ON I.IMG_ID = L.IMG_ID
go

/*==============================================================*/
/* View: vw_Product                                             */
/*==============================================================*/
create view dbo.vw_Product as
SELECT P.[PRODUCT_ID]
      ,P.[ID_INT]
      ,P.[PageId]
      ,P.[SMAP_ID]
      ,P.PMOD_ID
      ,P.[CODE]
      ,P.[PRICE]
      ,P.PRICE_NO_TAX
      ,P.TAX_RATE
      ,P.[DISCOUNT_PERCENT]
      ,P.[SPECIAL_DISCOUNT_PERCENT]
      ,P.[UNITS_IN_STOCK]
      ,P.[ADDED_BY]
      ,P.[ADDED_DATE]
      ,P.[ACTIVE]
      ,P.DELETED
      ,P.[PRODUCT_URL]
      ,P.[WARRANTY]
      ,P.[TOTAL_RATING]
      ,P.[VOTES]
      ,P.[IMPORTANCE]
      ,P.[PRODUCT_TYPE]
      ,P.[CAT_ACTION]
      ,P.[CAT_SALE]
      ,PD.LANG_ID
      ,PD.NAME
      ,PD.DESCRIPTION_LONG
      ,PD.DESCRIPTION_SHORT
      ,PD.PDescID
      ,I.IMG_URL
      ,I.IMG_URL as IMG_URL_BIG
      ,ID.IMG_DESC
      ,ID.IMG_TITLE
      ,I.IMG_ID
      ,I.TYPE_ID
      ,P.PRODUCT_ID as CWP_GAL
      ,v.AvgRating
      ,p.ManufacturerID
      ,m.title as ManufacturerTitle

from PRODUCT P, PRODUCT_DESC PD, IMAGE I, IMAGE_LANG ID, vote v, manufacturer m
where P.PRODUCT_ID = PD.PRODUCT_ID AND I.TYPE_ID = 1 AND ID.IMG_ID = 1 AND ID.LANG_ID = 'si'
and v.VoteID = p.product_id and m.manufacturerid = p.manufacturerid
go

/*==============================================================*/
/* View: vw_UserCulture                                         */
/*==============================================================*/
create view dbo.vw_UserCulture as
SELECT C.LCID, UserId, [DEFAULT], ACTIVE, SORT, LANG_ID, CULT, UI_CULTURE, [DESC], IMAGE_URL 
  FROM EM_USER_CULTURE U, CULTURE C
 WHERE U.LCID = C.LCID
go

alter table dbo.ARTICLE
   add constraint FK_ARTICLE__CMS_WEB_PART foreign key (CWP_ID)
      references dbo.CMS_WEB_PART (CWP_ID)
go

alter table dbo.ARTICLE
   add constraint FK_ARTICLE__GALLERY foreign key (GAL_ID)
      references dbo.GALLERY (GAL_ID)
go

alter table dbo.ARTICLE
   add constraint FK_ARTICLE__LANGUAGE foreign key (LANG_ID)
      references dbo.LANGUAGE (LANG_ID)
go

alter table dbo.ARTICLE
   add constraint FK_ARTICLE__SITEMAP foreign key (SMAP_ID)
      references dbo.SITEMAP (SMAP_ID)
go

alter table dbo.ATTRIBUTE
   add constraint FK_ATTRIBUTE__GROUP_ATTRIBUTE foreign key (GroupAttID)
      references dbo.GROUP_ATTRIBUTE (GroupAttID)
go

alter table dbo.CART_ITEM
   add constraint FK_CART_ITEM__ORDER foreign key (OrderID)
      references dbo.SHOPPING_ORDER (OrderID)
go

alter table dbo.CART_ITEM
   add constraint FK_CART_ITEM__PRODUCT foreign key (PRODUCT_ID)
      references dbo.PRODUCT (PRODUCT_ID)
go

alter table dbo.CART_ITEM_ATTRIBUTE
   add constraint FK_CART_ITEM_ATTRIBUTE__ATTRIBUTE foreign key (AttributeID)
      references dbo.ATTRIBUTE (AttributeID)
go

alter table dbo.CART_ITEM_ATTRIBUTE
   add constraint FK_CART_ITEM_ATTRIBUTE__CART_ITEM foreign key (CartItemID)
      references dbo.CART_ITEM (CartItemID)
go

alter table dbo.CART_ITEM_ATTRIBUTE
   add constraint FK_CART_ITEM_ATTRIBUTE__GROUP_ATTRIBUTE foreign key (GroupAttID)
      references dbo.GROUP_ATTRIBUTE (GroupAttID)
go

alter table dbo.CMS_WEB_PART
   add constraint FK_CMS_WEB_PART__LANGUAGE foreign key (LANG_ID)
      references dbo.LANGUAGE (LANG_ID)
go

alter table dbo.CMS_WEB_PART
   add constraint FK_CMS_WEB_PART__MODULE foreign key (MOD_ID)
      references dbo.MODULE (MOD_ID)
go

alter table dbo.CMS_WEB_PART
   add constraint FK_CMS_WEB__REFERENCE_EM_USER foreign key (UserId)
      references dbo.EM_USER (UserId)
go

alter table dbo.COMPANY
   add constraint FK_COMPANY__ADDRESS foreign key (ADDR_ID)
      references dbo.ADDRESS (ADDR_ID)
go

alter table dbo.COMPANY
   add constraint FK_COMPANY_REFERENCE_EM_USER foreign key (PageId)
      references dbo.EM_USER (UserId)
go

alter table dbo.CONTENT
   add constraint FK_CONTENT__CMS_WEB_PART foreign key (CWP_ID)
      references dbo.CMS_WEB_PART (CWP_ID)
go

alter table dbo.CONTENT
   add constraint FK_CONTENT__LANGUAGE foreign key (LANG_ID)
      references dbo.LANGUAGE (LANG_ID)
go

alter table dbo.CONTENT
   add constraint FK_CONTENT__SITEMAP foreign key (SMAP_ID)
      references dbo.SITEMAP (SMAP_ID)
go

alter table dbo.CULTURE
   add constraint FK_CULTURE__LANGUAGE foreign key (LANG_ID)
      references dbo.LANGUAGE (LANG_ID)
go

alter table dbo.CUSTOMER
   add constraint FK_CUSTOMER__COMPANY foreign key (COMPANY_ID)
      references dbo.COMPANY (COMPANY_ID)
go

alter table dbo.CUSTOMER
   add constraint FK_CUSTOMER__EM_USER foreign key (PageID)
      references dbo.EM_USER (UserId)
go

alter table dbo.CUSTOMER
   add constraint FK_CUSTOMER__MARKETING_CAMPAIGN foreign key (CampaignID)
      references dbo.MARKETING_CAMPAIGN (CampaignID)
go

alter table dbo.CWP_ARTICLE
   add constraint FK_CWP_ARTICLE__CMS_WEB_PART foreign key (CWP_ID)
      references dbo.CMS_WEB_PART (CWP_ID)
go

alter table dbo.CWP_ARTICLE
   add constraint FK_CWP_ARTICLE__IMAGE_TYPE foreign key (IMG_TYPE_THUMB)
      references dbo.IMAGE_TYPE (TYPE_ID)
go

alter table dbo.CWP_CONTACT_FORM
   add constraint FK_CWP_CONTACT_FORM__CMS_WEB_PART foreign key (CWP_ID)
      references dbo.CMS_WEB_PART (CWP_ID)
go

alter table dbo.CWP_GALLERY
   add constraint FK_CWP_GALLERY__CMS_WEB_PART foreign key (CWP_ID)
      references dbo.CMS_WEB_PART (CWP_ID)
go

alter table dbo.CWP_GALLERY
   add constraint FK_CWP_GALLERY__IMAGE_TYPE foreign key (THUMB_TYPE_ID)
      references dbo.IMAGE_TYPE (TYPE_ID)
go

alter table dbo.CWP_GALLERY
   add constraint FK_CWP_GALLERY__IMAGE_TYPE_BIG foreign key (BIG_TYPE_ID)
      references dbo.IMAGE_TYPE (TYPE_ID)
go

alter table dbo.CWP_MAP
   add constraint FK_CWP_MAP__CMS_WEB_PART foreign key (CWP_ID)
      references dbo.CMS_WEB_PART (CWP_ID)
go

alter table dbo.EM_USER
   add constraint FK_EM_USER__ADDRESS foreign key (ADDR_ID)
      references dbo.ADDRESS (ADDR_ID)
go

alter table dbo.EM_USER
   add constraint FK_EM_USER__COMPANY foreign key (COMPANY_ID)
      references dbo.COMPANY (COMPANY_ID)
go

alter table dbo.EM_USER
   add constraint FK_EM_USER__HEADER__IMAGE foreign key (IMG_LOGO_ID, TYPE_LOGO_ID)
      references dbo.IMAGE (IMG_ID, TYPE_ID)
go

alter table dbo.EM_USER
   add constraint FK_EM_USER__IMAGE foreign key (IMG_HEADER_ID, TYPE_HEADER_ID)
      references dbo.IMAGE (IMG_ID, TYPE_ID)
go

alter table dbo.EM_USER
   add constraint FK_EM_USER__MASTER foreign key (MASTER_ID)
      references dbo.MASTER (MASTER_ID)
go

alter table dbo.EM_USER
   add constraint FK_EM_USER__THEME foreign key (THEME_ID)
      references dbo.THEME (THEME_ID)
go

alter table dbo.EM_USER_CULTURE
   add constraint FK_EM_USER_CULTURE__CULTURE foreign key (LCID)
      references dbo.CULTURE (LCID)
go

alter table dbo.EM_USER_CULTURE
   add constraint FK_EM_USER_CULTURE__EM_USER foreign key (UserId)
      references dbo.EM_USER (UserId)
go

alter table dbo."FILE"
   add constraint FK_FILE__EM_USER foreign key (UserId)
      references dbo.EM_USER (UserId)
go

alter table dbo.GALLERY
   add constraint FK_GALLERY__GALLERY_TYPE foreign key (GAL_TYPE)
      references dbo.GALLERY_TYPE (GAL_TYPE)
go

alter table dbo.GALLERY_IMAGE_TYPE
   add constraint FK_GALLERY_IMAGE_TYPE__GALLERY foreign key (GAL_ID)
      references dbo.GALLERY (GAL_ID)
go

alter table dbo.GALLERY_IMAGE_TYPE
   add constraint FK_GALLERY_IMAGE_TYPE__IMAGE_TYPE foreign key (TYPE_ID)
      references dbo.IMAGE_TYPE (TYPE_ID)
go

alter table dbo.GALLERY_LANG
   add constraint FK_GALLERY_LANG__GALLERY foreign key (GAL_ID)
      references dbo.GALLERY (GAL_ID)
go

alter table dbo.GALLERY_LANG
   add constraint FK_GALLERY_LANG__LANGUAGE foreign key (LANG_ID)
      references dbo.LANGUAGE (LANG_ID)
go

alter table dbo.GALLERY_TYPE_IMAGE_TYPE
   add constraint FK_GALLERY_TYPE_IMAGE_TYPE__GALLERY_TYPE foreign key (GAL_TYPE)
      references dbo.GALLERY_TYPE (GAL_TYPE)
go

alter table dbo.GALLERY_TYPE_IMAGE_TYPE
   add constraint FK_GALLERY_TYPE_IMAGE_TYPE__IMAGE_TYPE foreign key (TYPE_ID)
      references dbo.IMAGE_TYPE (TYPE_ID)
go

alter table dbo.GROUP_ATTRIBUTE
   add constraint FK_GROUP_ATTRIBUTE__EM_USER foreign key (PageId)
      references dbo.EM_USER (UserId)
go

alter table dbo.GROUP_ATTRIBUTE_LANG
   add constraint FK_GROUP_ATTRIBUTE_LANG__GROUP_ATTRIBUTE foreign key (GroupAttID)
      references dbo.GROUP_ATTRIBUTE (GroupAttID)
go

alter table dbo.GROUP_ATTRIBUTE_LANG
   add constraint FK_GROUP_ATTRIBUTE_LANG__LANGUAGE foreign key (LANG_ID)
      references dbo.LANGUAGE (LANG_ID)
go

alter table dbo.IMAGE
   add constraint FK_IMAGE__IMAGE_ORIG foreign key (IMG_ID)
      references dbo.IMAGE_ORIG (IMG_ID)
go

alter table dbo.IMAGE
   add constraint FK_IMAGE__IMAGE_TYPE foreign key (TYPE_ID)
      references dbo.IMAGE_TYPE (TYPE_ID)
go

alter table dbo.IMAGE_GALLERY
   add constraint FK_IMAGE_GALLERY__GALLERY foreign key (GAL_ID)
      references dbo.GALLERY (GAL_ID)
go

alter table dbo.IMAGE_GALLERY
   add constraint FK_IMAGE_GALLERY__IMAGE_ORIG foreign key (IMG_ID)
      references dbo.IMAGE_ORIG (IMG_ID)
go

alter table dbo.IMAGE_LANG
   add constraint FK_IMAGE_LANG__IMAGE_ORDER foreign key (IMG_ID)
      references dbo.IMAGE_ORIG (IMG_ID)
go

alter table dbo.IMAGE_LANG
   add constraint FK_IMAGE_LANG__LANGUAGE foreign key (LANG_ID)
      references dbo.LANGUAGE (LANG_ID)
go

alter table dbo.JOB_EMAIL
   add constraint FK_JOB_EMAIL__JOB_LOG foreign key (JobLogID)
      references dbo.JOB_LOG (JobLogID)
go

alter table dbo.JOB_LOG
   add constraint FK_JOB_LOG__JOB foreign key (JobID)
      references dbo.JOB (JobID)
go

alter table dbo.MANUFACTURER
   add constraint FK_MANUFACTURER__EM_USER foreign key (PageId)
      references dbo.EM_USER (UserId)
go

alter table dbo.MASTER
   add constraint FK_MASTER__IMAGE_TYPE foreign key (IMG_TYPE_ID)
      references dbo.IMAGE_TYPE (TYPE_ID)
go

alter table dbo.MASTER
   add constraint FK_MASTER__MASTER_GROUP foreign key (MGR_ID)
      references dbo.MASTER_GROUP (MGR_ID)
go

alter table dbo.MASTER
   add constraint FK_MASTER_REFERENCE_IMAGE_TY foreign key (HDR_TYPE_ID)
      references dbo.IMAGE_TYPE (TYPE_ID)
go

alter table dbo.MASTER_PAGEM
   add constraint FK_MASTER_PAGEM__MASTER foreign key (MASTER_ID)
      references dbo.MASTER (MASTER_ID)
go

alter table dbo.MASTER_PAGEM
   add constraint FK_MASTER_PAGEM__PAGE_MODULE foreign key (PMOD_ID)
      references dbo.PAGE_MODULE (PMOD_ID)
go

alter table dbo.NEWSLETTER_EMAIL
   add constraint FK_NEWSLETTER_EMAIL__NEWSLETTER foreign key (NewsletterID)
      references dbo.NEWSLETTER (NewsletterID)
go

alter table dbo.OPINION
   add constraint FK_OPINION__LANGUAGE foreign key (LANG_ID)
      references dbo.LANGUAGE (LANG_ID)
go

alter table dbo.OPINION
   add constraint FK_OPINION__PRODUCT foreign key (PRODUCT_ID)
      references dbo.PRODUCT (PRODUCT_ID)
go

alter table dbo.PAGEM_MOD
   add constraint FK_PAGEM_MOD__MODULE foreign key (MOD_ID)
      references dbo.MODULE (MOD_ID)
go

alter table dbo.PAGEM_MOD
   add constraint FK_PAGEM_MOD__PAGE_MODULE foreign key (PMOD_ID)
      references dbo.PAGE_MODULE (PMOD_ID)
go

alter table dbo.PAYMENT
   add constraint FK_PAYMENT__EM_USER foreign key (UserId)
      references dbo.EM_USER (UserId)
go

alter table dbo.POLL
   add constraint FK_POLL__CMS_WEB_PART foreign key (CWP_ID)
      references dbo.CMS_WEB_PART (CWP_ID)
go

alter table dbo.POLL
   add constraint FK_POLL__EM_USER foreign key (UserId)
      references dbo.EM_USER (UserId)
go

alter table dbo.POLL_CHOICE
   add constraint FK_POLL_CHOICE__POLL_LANG foreign key (PollID, LANG_ID)
      references dbo.POLL_LANG (PollID, LANG_ID)
go

alter table dbo.POLL_IPADDRESS
   add constraint FK_POLL_IPADDRESS__POLL foreign key (PollID)
      references dbo.POLL (PollID)
go

alter table dbo.POLL_LANG
   add constraint FK_POLL_LANG__LANGUAGE foreign key (LANG_ID)
      references dbo.LANGUAGE (LANG_ID)
go

alter table dbo.POLL_LANG
   add constraint FK_POLL_LANG__POLL foreign key (PollID)
      references dbo.POLL (PollID)
go

alter table dbo.PREDRACUN
   add constraint FK_PREDRACUN__EM_USER foreign key (UserId)
      references dbo.EM_USER (UserId)
go

alter table dbo.PRODUCT
   add constraint FK_PRODUCT__EM_USER foreign key (PageId)
      references dbo.EM_USER (UserId)
go

alter table dbo.PRODUCT
   add constraint FK_PRODUCT__MANUFACTURER foreign key (ManufacturerID)
      references dbo.MANUFACTURER (ManufacturerID)
go

alter table dbo.PRODUCT
   add constraint FK_PRODUCT__PAGE_MODULE foreign key (PMOD_ID)
      references dbo.PAGE_MODULE (PMOD_ID)
go

alter table dbo.PRODUCT
   add constraint FK_PRODUCT__SITEMAP foreign key (SMAP_ID)
      references dbo.SITEMAP (SMAP_ID)
go

alter table dbo.PRODUCT_ATTRIBUTE
   add constraint FK_PRODUCT_ATRTIBUTE__ATTRIBUTE foreign key (AttributeID)
      references dbo.ATTRIBUTE (AttributeID)
go

alter table dbo.PRODUCT_ATTRIBUTE
   add constraint FK_PRODUCT_ATTRIBUTE__PRODUCT foreign key (PRODUCT_ID)
      references dbo.PRODUCT (PRODUCT_ID)
go

alter table dbo.PRODUCT_DESC
   add constraint FK_PRODUCT_DESC__PRODUCT foreign key (PRODUCT_ID)
      references dbo.PRODUCT (PRODUCT_ID)
go

alter table dbo.PRODUCT_DESC
   add constraint FK_PRODUCT___LANGUAGE foreign key (LANG_ID)
      references dbo.LANGUAGE (LANG_ID)
go

alter table dbo.PRODUCT_GROUP_ATTRIBUTE
   add constraint FK_PRODUCT_GROUP_ATTR__GROUP_ATTRIBUTE foreign key (GroupAttID)
      references dbo.GROUP_ATTRIBUTE (GroupAttID)
go

alter table dbo.PRODUCT_GROUP_ATTRIBUTE
   add constraint FK_PRODUCT_GROUP_ATTR___PRODUCT foreign key (PRODUCT_ID)
      references dbo.PRODUCT (PRODUCT_ID)
go

alter table dbo.PRODUCT_SMAP
   add constraint FK_PRODUCT_SMAP__PRODUCT foreign key (PRODUCT_ID)
      references dbo.PRODUCT (PRODUCT_ID)
go

alter table dbo.PRODUCT_SMAP
   add constraint FK_PRODUCT_SMAP__SITEMAP foreign key (SMAP_ID)
      references dbo.SITEMAP (SMAP_ID)
go

alter table dbo.SHOPPING_ORDER
   add constraint FK_ORDER__EM_USER foreign key (UserId)
      references dbo.EM_USER (UserId)
go

alter table dbo.SHOPPING_ORDER
   add constraint FK_ORDER__PREDRACUN foreign key (PredracunID)
      references dbo.PREDRACUN (PredracunID)
go

alter table dbo.SHOPPING_ORDER
   add constraint FK_ORDER__TICKET foreign key (TicketID)
      references dbo.TICKET (TicketID)
go

alter table dbo.SHOPPING_ORDER
   add constraint FK_SHOPPING_ORDER__EM_USER foreign key (PageId)
      references dbo.EM_USER (UserId)
go

alter table dbo.SHOPPING_ORDER
   add constraint FK_SHOPPING_ORDER___COMPANY foreign key (COMPANY_ID)
      references dbo.COMPANY (COMPANY_ID)
go

alter table dbo.SITEMAP
   add constraint FK_SITEMAP__MENU foreign key (MENU_ID)
      references dbo.MENU (MENU_ID)
go

alter table dbo.SITEMAP
   add constraint FK_SITEMAP__PAGEMODULE foreign key (PMOD_ID)
      references dbo.PAGE_MODULE (PMOD_ID)
go

alter table dbo.SITEMAP_GALLERY
   add constraint FK_SITEMAP_GALLERY__GALLERY foreign key (GAL_ID)
      references dbo.GALLERY (GAL_ID)
go

alter table dbo.SITEMAP_GALLERY
   add constraint FK_SITEMAP_GALLERY__SITEMAP foreign key (SMAP_ID)
      references dbo.SITEMAP (SMAP_ID)
go

alter table dbo.SITEMAP_LANG
   add constraint FK_SML___LANGUAGE foreign key (LANG_ID)
      references dbo.LANGUAGE (LANG_ID)
go

alter table dbo.SITEMAP_LANG
   add constraint FK_SML___SITEMAP foreign key (SMAP_ID)
      references dbo.SITEMAP (SMAP_ID)
go

alter table dbo.SITEMAP_MODULE
   add constraint FK_SITEMAP___MODULE foreign key (MOD_ID)
      references dbo.MODULE (MOD_ID)
go

alter table dbo.SITEMAP_MODULE
   add constraint FK_SITEMAP_MODULE___SITEMAP foreign key (SMAP_ID)
      references dbo.SITEMAP (SMAP_ID)
go

alter table dbo.THEME
   add constraint FK_THEME__MASTER foreign key (MASTER_ID)
      references dbo.MASTER (MASTER_ID)
go

alter table dbo.TICKET
   add constraint FK_TICKET__EM_USER foreign key (UserId)
      references dbo.EM_USER (UserId)
go

alter table dbo.TICKET
   add constraint FK_TICKET__STATUS foreign key (StatusID)
      references dbo.STATUS (StatusID)
go

alter table dbo.USER_MODULE
   add constraint FK_USER_MODULE__EM_USER foreign key (UserId)
      references dbo.EM_USER (UserId)
go

alter table dbo.USER_MODULE
   add constraint FK_USER_MODULE__MODULE foreign key (MOD_ID)
      references dbo.MODULE (MOD_ID)
go

alter table dbo.VOTE_IPADDRESS
   add constraint FK_VOTE_IPADDRESS__VOTE foreign key (VoteID)
      references dbo.VOTE (VoteID)
go

alter table dbo.ZONE_MODULE
   add constraint FK_ZONE_MODULE__MODULE foreign key (MOD_ID)
      references dbo.MODULE (MOD_ID)
go

alter table dbo.ZONE_MODULE
   add constraint FK_ZONE_MODULE__PAGE_MODULE foreign key (PMOD_ID)
      references dbo.PAGE_MODULE (PMOD_ID)
go

