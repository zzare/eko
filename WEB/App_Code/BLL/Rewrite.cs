﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using UrlRewritingNet.Web;

/// <summary>
/// Summary description for Caching
/// </summary>
namespace SM.EM
{

    public class Rewrite
    {
        public const string EMENIK_USER_CONTENT_PATH = "~/_em/content/";

        public static string ResolveUserDomain(string pname){
        
            string ret = "~";
            if (SM.BLL.Common.Emenik.Data.EnableSubDomainUsers())
                ret = "http://" + pname + "." + SM.BLL.Common.Emenik.Data.PortalHostDomain();

            return ret;        
        }

        public static string EmenikUserProductDetailsUrl(PRODUCT p, PRODUCT_DESC pdesc)
        {
//            return EmenikUserProductDetailsUrl(p.PageId, p.PRODUCT_ID, pdesc.NAME);
            return EmenikUserProductDetailsUrl(p.PageId, p.PRODUCT_ID, pdesc.NAME);
        }

        public static string EmenikUserProductDetailsUrlOld(Guid pageId, Guid  productID, string title)
        {
            string cult = LANGUAGE.GetCurrentCulture();
            cult = cult + "/";
            string url;
            ShortGuid  pid = new ShortGuid(productID);


            if (Helpers.IsDefaultDomain() && !SM.BLL.Common.Emenik.Data.EnableSubDomainUsers() && !SM.BLL.Common.Emenik.Data.CustomDomainsEnabled())
            {
                string pname = "";
                EM_USER usr = EM_USER.GetUserByID(pageId);
                if (usr != null)
                    pname = usr.PAGE_NAME;

                url = "~/" + pname + "/" + cult + "p-" + pid.ToString() + "/" + ConvertToFriendlyURL(title) + ".aspx";
            }
            else
                url = "~/" + cult + "p-" + pid.ToString() + "/" + ConvertToFriendlyURL(title) + "";
            return url;
        }

        public static string EmenikUserProductDetailsUrl(Guid pageId, Guid productID, string title)
        {
            string cult = LANGUAGE.GetCurrentCulture();
            cult = cult + "/";
            string url;
            ShortGuid pid = new ShortGuid(productID);

            if (Helpers.IsDefaultDomain() && !SM.BLL.Common.Emenik.Data.EnableSubDomainUsers() && !SM.BLL.Common.Emenik.Data.CustomDomainsEnabled())
            {
                string pname = "";
                EM_USER usr = EM_USER.GetUserByID(pageId);
                if (usr != null)
                    pname = usr.PAGE_NAME;

                url = "~/" + pname + "/" + cult + ConvertToFriendlyURL(title) + "/" + "p-" + pid.ToString();
            }
            else
                url = "~/" + cult + ConvertToFriendlyURL(title) + "/" + "p-" + pid.ToString() + "";
            return url;
        }
        public static string EmenikUserProductDetailsUrl(Guid pageId, int productID, string title)
        {
            string cult = LANGUAGE.GetCurrentCulture();
            cult = cult + "/";
            string url;

            if (Helpers.IsDefaultDomain() && !SM.BLL.Common.Emenik.Data.EnableSubDomainUsers() && !SM.BLL.Common.Emenik.Data.CustomDomainsEnabled())
            {
                string pname = "";
                EM_USER usr = EM_USER.GetUserByID(pageId);
                if (usr != null)
                    pname = usr.PAGE_NAME;

                url = "~/" + pname + "/" + cult + ConvertToFriendlyURL(title) + "/" + "p-" + productID.ToString();
            }
            else
                url = "~/" + cult + ConvertToFriendlyURL(title) + "/" + "p-" + productID.ToString() + "";
            return url;
        }

        public static string EmenikUserArticleDetailsUrl(string pname, int smapID, int pmodID, int artID, string artTitle)
        {
            return EmenikUserArticleDetailsUrl(pname, artID, artTitle);
        }
        public static string EmenikUserArticleDetailsUrl(string pname, int artID, string artTitle)
        {
            string cult = LANGUAGE.GetCurrentCulture();
            cult = cult + "/";
            string url;
            if (Helpers.IsDefaultDomain() && !SM.BLL.Common.Emenik.Data.EnableSubDomainUsers() && !SM.BLL.Common.Emenik.Data.CustomDomainsEnabled())
            {
                    url = "~/" + pname + "/" + cult + "a-" + artID.ToString() + "/" + ConvertToFriendlyURL(artTitle) + ".aspx";
            }
            else
                url = "~/" + cult + "a-" + artID.ToString() + "/" + ConvertToFriendlyURL(artTitle) ;
            return url;
        }
        public static string EmenikUserArticleArchiveUrl(string pname, int smapID, Guid cwp)
        {
            string cult = LANGUAGE.GetCurrentCulture();
            cult = cult + "/";
            string url;
            if (Helpers.IsDefaultDomain() && !SM.BLL.Common.Emenik.Data.EnableSubDomainUsers() && !SM.BLL.Common.Emenik.Data.CustomDomainsEnabled())
                //if (SM.BLL.Common.Emenik.Data.EnableSubDomainUsers())
                //    url = ResolveUserDomain(pname) + "/" + cult + "arhiv/" + smapID.ToString() + "/" + cwp.ToString().ToLower() + "";
                //else
                    url = "~/" + pname + "/" + cult + "arhiv/" + smapID.ToString() + "/" + cwp.ToString().ToLower() + ".aspx";
            else
                url = "~/" + cult + "arhiv/" + smapID.ToString() + "/" + cwp.ToString().ToLower() + "";
            return url;
        }

        public static string EmenikUserArticleArchiveUrlByMonth(string pname, int smapID, Guid cwp, int month)
        {
            return EmenikUserArticleArchiveUrlByMonth(pname, smapID, cwp, month, DateTime.Now.Year);
        }


        public static string EmenikUserArticleArchiveUrlByMonth(string pname, int smapID, Guid cwp, int month, int year)
        {
            string cult = LANGUAGE.GetCurrentCulture();
            cult = cult + "/";
            string m = "";
            string y = "";
            if (month > 0 && month <= 12)
                m = "?m=" + month.ToString();
            if (year > (DateTime.Now.Year - 5) && year <= (DateTime.Now.Year + 5))
                y = "&y=" + month.ToString();
            string url;
            if (Helpers.IsDefaultDomain() && !SM.BLL.Common.Emenik.Data.EnableSubDomainUsers() && !SM.BLL.Common.Emenik.Data.CustomDomainsEnabled())
                url = "~/" + pname + "/" + cult + "arhiv/" + smapID.ToString() + "/" + cwp.ToString().ToLower() + ".aspx" + m + y;
            else
                url = "~/" + cult + "arhiv/" + smapID.ToString() + "/" + cwp.ToString().ToLower() + "" + m + y;
            return url;
        }

        //public static string EmenikUserArticleArchiveUrlByDate(string pname, int smapID, Guid cwp, string date)
        //{
        //    string cult = LANGUAGE.GetCurrentCulture();
        //    cult = cult + "/";
        //    string url;

        //    url = EmenikUserArticleArchiveUrl(pname, smapID, cwp) + "?dt=" + HttpUtility.UrlEncode(date);


        //    return url;
        //}

        public static string EmenikUserPollArchiveUrl(string pname)
        {
            string cult = LANGUAGE.GetCurrentCulture();
            cult = cult + "/";
            string url;
            if (Helpers.IsDefaultDomain() && !SM.BLL.Common.Emenik.Data.EnableSubDomainUsers() && !SM.BLL.Common.Emenik.Data.CustomDomainsEnabled())
                url = "~/" + pname + "/" + cult + "poll.aspx";
            else
                url = "~/" + cult + "poll";
            return url;
        }
        public static string EmenikUserUrl(string pname, int smapID, string smapTitle, int pmodID)
        {
            string cult = LANGUAGE.GetCurrentCulture();
            cult = cult + "/";

            string url;
            if (Helpers.IsDefaultDomain())
                if (SM.BLL.Common.Emenik.Data.EnableSubDomainUsers())
                    url = ResolveUserDomain(pname) + "/" + cult + smapID.ToString() + "/" + ConvertToFriendlyURL(smapTitle) + "";
                else
                    url = "~/" + pname + "/" + cult + smapID.ToString() + "/" + ConvertToFriendlyURL(smapTitle) + ".aspx";
            else
                url = "~/" + cult + smapID.ToString() + "/" + ConvertToFriendlyURL(smapTitle) + "";
            return url;
        }

        public static string EmenikUserUrl(string pname, int smapID, string smapTitle, string culture)
        {
            string cult = culture ;
            if (string.IsNullOrEmpty(cult ))
                cult = LANGUAGE.GetCurrentCulture();
            cult = cult + "/";

            string url;
            if (Helpers.IsDefaultDomain() && !SM.BLL.Common.Emenik.Data.CustomDomainsEnabled())
                if (SM.BLL.Common.Emenik.Data.EnableSubDomainUsers())
                    url = ResolveUserDomain(pname) + "/" + cult + smapID.ToString() + "/" + ConvertToFriendlyURL(smapTitle) + "";
                else
                    url = "~/" + pname + "/" + cult + smapID.ToString() + "/" + ConvertToFriendlyURL(smapTitle) + ".aspx";
            else
                url = "~/" + cult + smapID.ToString() + "/" + ConvertToFriendlyURL(smapTitle) + "";
            return url;
        }
        public static string EmenikUserUrl(string pname, int smapID, string smapTitle)
        {
            return EmenikUserUrl(pname, smapID, smapTitle, LANGUAGE.GetCurrentCulture());
        }


        public static string EmenikUserUrlGetLeftPart(EM_USER usr)
        {

            string leftPart = "";



            if (SM.BLL.Common.Emenik.Data.CustomDomainsEnabled() && usr.CUSTOM_DOMAIN_ACTIVATED && !string.IsNullOrEmpty(usr.CUSTOM_DOMAIN))
                leftPart = "http://" + usr.CUSTOM_DOMAIN;
            else if (SM.BLL.Common.Emenik.Data.EnableSubDomainUsers())
                leftPart = "http://" + usr.PAGE_NAME + "." + SM.BLL.Common.Emenik.Data.PortalHostDomain();
            else {
                    leftPart = HttpContext.Current.Request.Url.GetLeftPart(UriPartial .Authority);
//                    leftPart = "http://" + SM.BLL.Common.Emenik.Data.PortalHostDomain();

                    if (HttpContext.Current.Request.IsLocal)
                    {
                        if (HttpContext.Current.Request.Url.PathAndQuery.StartsWith("/WEB"))
                            leftPart += "/WEB";
                    }

            }

            return leftPart;
        
        }

        public static string EmenikUserUrlGetRightPart(EM_USER usr, int smapID, string smapTitle)
        {

             string cult = LANGUAGE.GetCurrentCulture();
            cult = cult + "/";


            string ret = "";
            if (SM.BLL.Common.Emenik.Data.CustomDomainsEnabled() && usr.CUSTOM_DOMAIN_ACTIVATED && !string.IsNullOrEmpty(usr.CUSTOM_DOMAIN))
                ret = "/" + cult + smapID.ToString() + "/" + ConvertToFriendlyURL(smapTitle) + "";
            else if (SM.BLL.Common.Emenik.Data.EnableSubDomainUsers())
                ret = "/" + cult + smapID.ToString() + "/" + ConvertToFriendlyURL(smapTitle) + "";
            else
            {
                ret = "/" + usr.PAGE_NAME  + "/" + cult + smapID.ToString() + "/" + ConvertToFriendlyURL(smapTitle) + ".aspx"; 
            }

            return ret;

        }


        public static string EmenikUserUrl(EM_USER usr, int smapID, string smapTitle)
        {

            string url = "";

            string leftPart = EmenikUserUrlGetLeftPart(usr);

            string path = EmenikUserUrlGetRightPart(usr, smapID, smapTitle);

            url = leftPart + path;


            return url;
        }


        public static string EmenikUserUrl(string pname, int smapID)
        {
            return EmenikUserUrl(pname, smapID, "default");
        }

        public static string EmenikUserUrl(EM_USER usr)
        {
            if (!SM.BLL.Common.Emenik.Data.CustomDomainsEnabled())
                return EmenikUserUrl(usr.PAGE_NAME);

            if (!usr.CUSTOM_DOMAIN_ACTIVATED || string.IsNullOrEmpty(usr.CUSTOM_DOMAIN )  )
                return EmenikUserUrl(usr.PAGE_NAME);


            // user has own domain

            return "http://" + usr.CUSTOM_DOMAIN;


        }


        public static string EmenikUserUrl(string pname)
        {
            return EmenikUserUrl(pname, LANGUAGE.GetCurrentCulture());

        }


        public static string EmenikUserUrl(string pname, string cult)
        {
            //string cult = LANGUAGE.GetCurrentCulture();

            CULTURE defCult = CULTURE.GetDefaultUserCulture(pname);
            if (defCult == null)
                cult = "";
            else if (cult == defCult.UI_CULTURE)
                cult = "";
            else
                cult = cult + "/";


            string url;
            if (Helpers.IsDefaultDomain() && !SM.BLL.Common.Emenik.Data.CustomDomainsEnabled())
                if (SM.BLL.Common.Emenik.Data.EnableSubDomainUsers())
                {
                    if (!string.IsNullOrEmpty(cult))
                        cult += "default";
                    url = ResolveUserDomain(pname) + "/" + cult + "";
                }
                else
                    url = "~/" + pname + "/" + cult + "default.aspx";
            else
            {
                if (!string.IsNullOrEmpty(cult))
                    cult += "default";
                url = ("~/" + cult );
            }

            return url;
        }

        /* CUSTOMER LOGIN ***************************** */
        public static string EmenikCustomerRegister(Guid pageId, string ru = "")
        {
            string cult = LANGUAGE.GetCurrentCulture();
            cult = cult + "/";
            string url;

            if (Helpers.IsDefaultDomain() && !SM.BLL.Common.Emenik.Data.EnableSubDomainUsers() && !SM.BLL.Common.Emenik.Data.CustomDomainsEnabled())
            {
                string pname = "";
                EM_USER usr = EM_USER.GetUserByID(pageId);
                if (usr != null)
                    pname = usr.PAGE_NAME;

                url = "~/" + pname + "/" + cult + "register.aspx";
            }
            else
                url = "~/" + cult + "register";

            if (!string.IsNullOrEmpty(ru))
                url = url + "?" + ru;

            return url;
        }
        public static string EmenikCustomerLogin(Guid pageId, string ru = "")
        {
            string cult = LANGUAGE.GetCurrentCulture();
            cult = cult + "/";
            string url;

            if (Helpers.IsDefaultDomain() && !SM.BLL.Common.Emenik.Data.EnableSubDomainUsers() && !SM.BLL.Common.Emenik.Data.CustomDomainsEnabled())
            {
                string pname = "";
                EM_USER usr = EM_USER.GetUserByID(pageId);
                if (usr != null)
                    pname = usr.PAGE_NAME;

                url = "~/" + pname + "/" + cult + "login.aspx";
            }
            else
                url = "~/" + cult + "login";

            if (!string.IsNullOrEmpty(ru))
                url = url + "?" + ru;

            return url;
        }

        public static string EmenikCustomerForgotPass(Guid pageId)
        {
            string cult = LANGUAGE.GetCurrentCulture();
            cult = cult + "/";
            string url;

            if (Helpers.IsDefaultDomain() && !SM.BLL.Common.Emenik.Data.EnableSubDomainUsers())
            {
                string pname = "";
                EM_USER usr = EM_USER.GetUserByID(pageId);
                if (usr != null)
                    pname = usr.PAGE_NAME;

                url = "~/" + pname + "/" + cult + "password-recovery.aspx";
            }
            else
                url = "~/" + cult + "password-recovery";
            return url;
        }

        public static string EmenikCustomerEmailVerify(Guid pageId, string id)
        {
            string cult = LANGUAGE.GetCurrentCulture();
            cult = cult + "/";
            string url;

            if (Helpers.IsDefaultDomain() && !SM.BLL.Common.Emenik.Data.EnableSubDomainUsers() && !SM.BLL.Common.Emenik.Data.CustomDomainsEnabled())
            {
                string pname = "";
                EM_USER usr = EM_USER.GetUserByID(pageId);
                if (usr != null)
                    pname = usr.PAGE_NAME;

                url = "~/" + pname + "/" + cult + "verify.aspx?id=" + id;
            }
            else
                url = "~/" + cult + "verify?id=" + id;
            return url;
        }
        public static string SearchProductEmenikUrl(Guid pageId, string srch)
        {
            string cult = LANGUAGE.GetCurrentCulture();
            cult = cult + "/";
            string url;
            string sr = Microsoft.Security.Application.AntiXss.UrlEncode(srch);

            if (Helpers.IsDefaultDomain() && !SM.BLL.Common.Emenik.Data.EnableSubDomainUsers() && !SM.BLL.Common.Emenik.Data.CustomDomainsEnabled())
            {
                string pname = "";
                EM_USER usr = EM_USER.GetUserByID(pageId);
                if (usr != null)
                    pname = usr.PAGE_NAME;

                url = "~/" + pname + "/" + cult + "search" + ".aspx?srch=" + sr;
            }
            else
                url = "~/" + cult + "search/?srch=" + sr;
            return url;
        }

        /* CUSTOMER ORDER ***************************** */
        public static string OrderProductUrl(Guid pageId, Guid orderID, int step, string addQs = "")
        {
            string cult = LANGUAGE.GetCurrentCulture();
            cult = cult + "/";
            string url;
            string qs = "";
            if (step > 0)
                qs += "st=" + step.ToString();
            if (orderID != Guid.Empty) {
                if (!string.IsNullOrEmpty(qs))
                    qs += "&";
                qs += "o=" + orderID.ToString();            
            }
            if (!string.IsNullOrEmpty(qs))
                qs = "?" + qs ;

            if (Helpers.IsDefaultDomain() && !SM.BLL.Common.Emenik.Data.EnableSubDomainUsers() && !SM.BLL.Common.Emenik.Data.CustomDomainsEnabled())
            {
                string pname = "";
                EM_USER usr = EM_USER.GetUserByID(pageId);
                if (usr != null)
                    pname = usr.PAGE_NAME;

                url = "~/" + pname + "/" + cult + "order.aspx" + qs;
            }
            else
                url = "~/" + cult + "order" +qs ;

            if (!string.IsNullOrWhiteSpace(addQs))
                url = url + "&" + addQs;

            return url;
        }

        public static string PredracunDetailUrl(Guid pageId, Guid ord)
        {
            string cult = LANGUAGE.GetCurrentCulture();
            cult = cult + "/";
            string url;
            string title = "narocilo-predracun";
            string o = "?o=" + ord.ToString();

            if (ord == Guid.Empty)
                o = "";

            if (Helpers.IsDefaultDomain() && !SM.BLL.Common.Emenik.Data.EnableSubDomainUsers() && !SM.BLL.Common.Emenik.Data.CustomDomainsEnabled())
            {
                string pname = "";
                EM_USER usr = EM_USER.GetUserByID(pageId);
                if (usr != null)
                    pname = usr.PAGE_NAME;

                url = "~/" + pname + "/" + cult + title + ".aspx" + o;
            }
            else
                url = "~/" + cult + title + o ;
            return url;
        }




        public static string EmenikUserUrlLogin(string domain, string key, string returnUrl)
        {
            return "http://" + domain + "/alogin?s=" + key + "&r=" + HttpUtility.UrlEncode(returnUrl);
        }
        public static string EmenikUserUrlLoginSubdomain(string pname, string key, string returnUrl)
        {
            return ResolveUserDomain(pname) + "/alogin?s=" + key + "&r=" + HttpUtility.UrlEncode(returnUrl);
        }
        public static string EmenikUserUrlLogout(string domain, string returnUrl)
        {
            return "http://" + domain + "/alogout?r=" + HttpContext.Current.Server.UrlEncode(returnUrl);
        }
        public static string EmenikUserUrlLogoutSubdomain(string pname, string returnUrl)
        {
            return ResolveUserDomain(pname) + "/alogout?r=" + HttpContext.Current.Server.UrlEncode(returnUrl);
        }


        public static string EmenikUserSettingsUrl(string pname)
        {
            string url;

            if (Helpers.IsDefaultDomain())
                if (SM.BLL.Common.Emenik.Data.EnableSubDomainUsers())
                    url = ResolveUserDomain(pname)  + "/settings/default";
                else
                    url = "~/" + pname + "/settings/default.aspx";
            else
                url = "~/" + "settings/default";
            return url;
        }
        public static string EmenikUserSettingsUrlByStep(string pname, int step)
        {
            string url;
            if (Helpers.IsDefaultDomain())
                if (SM.BLL.Common.Emenik.Data.EnableSubDomainUsers())
                    url = ResolveUserDomain(pname) + "/settings/default?st=" + step.ToString();
                else
                    url = "~/" + pname + "/settings/default.aspx?st=" + step.ToString();
            else
                url = "~/" + "settings/default?st=" + step.ToString();

            return url;
        }

        public static string EmenikUserHelpUrlStep0(string pname)
        {
            string url;
            if (Helpers.IsDefaultDomain())
                if (SM.BLL.Common.Emenik.Data.EnableSubDomainUsers())
                    url = ResolveUserDomain(pname) + "/help/first-step";
                else
                    url = "~/" + pname + "/help/first-step.aspx";
            else
                url = "~/" + "help/first-step";

            return url;
        }

        public static string UserDefaultPageURL()
        {
            string ret = "~/";
            if (HttpContext.Current == null) return ret;

            if (!HttpContext.Current.User.Identity.IsAuthenticated) return ret;

            EM_USER usr = EM_USER.GetUserByUserName(HttpContext.Current.User.Identity.Name);
            if (usr == null)
                return "";

            return EmenikUserUrl(usr.PAGE_NAME);
        }





        public class Main
        {
            public static string UserSettingsURL()
            {
                string ret = "~/c/user/shopping-order/orders.aspx";

                return ret;
            }

            public static string GetBlogArticleURL(string blogName)
            {
                string ret = "~/si/" + blogName + "/{art}/{artTitle}.aspx";
                return ret;
            }


            public static string GetBlogArticleURL(string blogName, int  artID, string title)
            {
                string ret = "~/si/" + blogName + "/" + artID.ToString() + "/" + ConvertToFriendlyURL(title) + ".aspx";
                return ret;
            }



        }


        public static string ConvertToFriendlyURL(object title)
        {
            return ConvertToFriendlyURL(title, "");
        }
        public static string ConvertToFriendlyURL(object title, string allowedChars)
        {
            string strTitle =SM.EM.Helpers.StripHTML( title.ToString());

            #region Generate SEO Friendly URL based on Title
            //Trim Start and End Spaces.
            strTitle = strTitle.Trim();

            //Trim "-" Hyphen
            strTitle = strTitle.Trim('-');

            strTitle = strTitle.ToLower();
            char[] chars = @"$%#@!*?;:~`+=()[]{}|\'<>,/^&"".".ToCharArray();
            strTitle = strTitle.Replace("c#", "C-Sharp");
            strTitle = strTitle.Replace("vb.net", "VB-Net");
            strTitle = strTitle.Replace("asp.net", "Asp-Net");

            char[] allowChars = allowedChars.ToCharArray();


            // replace special characters
            strTitle = strTitle.Replace("č", "c");
            strTitle = strTitle.Replace("ć", "c");
            strTitle = strTitle.Replace("ž", "z");
            strTitle = strTitle.Replace("š", "s");


            //Replace . with - hyphen
            if (!allowChars.Contains(Convert.ToChar(".")))
                strTitle = strTitle.Replace(".", "-");

            //Replace Special-Characters
            for (int i = 0; i < chars.Length; i++)
            {
                string strChar = chars.GetValue(i).ToString();
                if (strTitle.Contains(strChar) && !allowChars.Contains(Convert.ToChar(strChar)))
                {
                    strTitle = strTitle.Replace(strChar, string.Empty);
                }
            }

            //Replace all spaces with one "-" hyphen
            strTitle = strTitle.Replace(" ", "-");

            //Replace multiple "-" hyphen with single "-" hyphen.
            strTitle = strTitle.Replace("--", "-");
            strTitle = strTitle.Replace("---", "-");
            strTitle = strTitle.Replace("----", "-");
            strTitle = strTitle.Replace("-----", "-");
            strTitle = strTitle.Replace("----", "-");
            strTitle = strTitle.Replace("---", "-");
            strTitle = strTitle.Replace("--", "-");

            //Replace multiple "." with single "." 
            strTitle = strTitle.Replace("..", ".");
            strTitle = strTitle.Replace("...", ".");
            strTitle = strTitle.Replace("....", ".");
            strTitle = strTitle.Replace(".....", ".");
            strTitle = strTitle.Replace("....", ".");
            strTitle = strTitle.Replace("...", ".");
            strTitle = strTitle.Replace("..", ".");

            //Run the code again...
            //Trim Start and End Spaces.
            strTitle = strTitle.Trim();

            //Trim "-" Hyphen
            strTitle = strTitle.Trim('-');
            #endregion



            // remove all non url valid chrs
            string nonValC = "";
            foreach(char c in strTitle ){


                if (!System.Text.RegularExpressions.Regex.IsMatch(c.ToString(), @"^[а-яa-zA-Z0-9\-\.\?\,\'\/\\\+&amp;%\$#_]*?$"))
                    nonValC += c.ToString();
            
            }

            // remove chars
            if (!string.IsNullOrEmpty(nonValC))
            {
                char[] list = nonValC.ToCharArray();
                foreach (char c in list) {
                    strTitle = strTitle.Replace(c.ToString(), "");                
                }
            }
            

            return strTitle;

        }


        public class Domain
        {


            public static void AddAllRewrites()
            {

                RegExRewriteRule rule;

                // add first rule -  NoWWW
                rule = new RegExRewriteRule();
                rule.VirtualUrl = "^http://www.(.*)";
                rule.DestinationUrl = "http://$1";
                rule.IgnoreCase = true;
                rule.Rewrite = RewriteOption.Domain;
                rule.Redirect = RedirectOption.Domain;
                rule.RedirectMode = RedirectModeOption.Permanent;
                rule.RewriteUrlParameter = RewriteUrlParameterOption.ExcludeFromClientQueryString;
                UrlRewriting.AddRewriteRule("NoWWW", rule);


                //// add first rule -  NoIndex
                //rule = new RegExRewriteRule();
                //rule.VirtualUrl = "^http://1dva3.si/default.aspx$";
                //rule.DestinationUrl = "http://1dva3.si/";
                //rule.IgnoreCase = true;
                //rule.Rewrite = RewriteOption.Domain;
                //rule.Redirect = RedirectOption.Domain;
                //rule.RedirectMode = RedirectModeOption.Permanent;
                //rule.RewriteUrlParameter = RewriteUrlParameterOption.ExcludeFromClientQueryString;
                //UrlRewriting.AddRewriteRule("NoIndex", rule);

                //rule = new RegExRewriteRule();
                //rule.VirtualUrl = "^http://1dva3.si/Default.aspx$";
                //rule.DestinationUrl = "http://1dva3.si/";
                //rule.IgnoreCase = true;
                //rule.Rewrite = RewriteOption.Domain;
                //rule.Redirect = RedirectOption.Domain;
                //rule.RedirectMode = RedirectModeOption.Permanent;
                //rule.RewriteUrlParameter = RewriteUrlParameterOption.ExcludeFromClientQueryString;
                //UrlRewriting.AddRewriteRule("NoIndex2", rule);


                // add blog rewrites
                //rule = new RegExRewriteRule();
                //rule.VirtualUrl = "~/si/blog/(.*)/(.*).aspx";
                //rule.DestinationUrl = "~/c/BlogDetails.aspx?art=$1&smp=34";
                //rule.IgnoreCase = true;
                //rule.Rewrite = RewriteOption.Application;
                //rule.Redirect = RedirectOption.None;
                //rule.RewriteUrlParameter = RewriteUrlParameterOption.ExcludeFromClientQueryString;
                //UrlRewriting.AddRewriteRule("MainBlog", rule);

                // add novice rewrites
                //rule = new RegExRewriteRule();
                //rule.VirtualUrl = "~/si/izdelava-spletnih-strani/novice/(.*)/(.*).aspx";
                //rule.DestinationUrl = "~/c/ArticleDetails.aspx?art=$1&smp=1427";
                //rule.IgnoreCase = true;
                //rule.Rewrite = RewriteOption.Application;
                //rule.Redirect = RedirectOption.None;
                //rule.RewriteUrlParameter = RewriteUrlParameterOption.ExcludeFromClientQueryString;
                //UrlRewriting.AddRewriteRule("MainNovice", rule);
                




                // add other rewrites
                if (SM.BLL.Common.Emenik.Data.EnableSubDomainUsers() || SM.BLL.Common.Emenik.Data.CustomDomainsEnabled())
                {
                    // animal vizitka
                    //AddAnimalFolderRewrites();
                    AddDomainRewrites();
                }
                else
                    AddFolderRewrites();

            }

            public static void AddDomainRewrites() {
                RegExRewriteRule rule;

                // auto login
                rule = new RegExRewriteRule();
                rule.VirtualUrl = "~/alogin";
                rule.DestinationUrl = "~/c/a/Login.aspx";
                rule.IgnoreCase = true;
                rule.Rewrite = RewriteOption.Application;
                rule.Redirect = RedirectOption.None;
                rule.RewriteUrlParameter = RewriteUrlParameterOption.ExcludeFromClientQueryString;
                UrlRewriting.AddRewriteRule("AutoLogin", rule);

                // auto logout
                rule = new RegExRewriteRule();
                rule.VirtualUrl = "~/alogout";
                rule.DestinationUrl = "~/c/a/Logout.aspx";
                rule.IgnoreCase = true;
                rule.Rewrite = RewriteOption.Application;
                rule.Redirect = RedirectOption.None;
                rule.RewriteUrlParameter = RewriteUrlParameterOption.ExcludeFromClientQueryString;
                UrlRewriting.AddRewriteRule("AutoLogout", rule);

                //// MAIN PAGE direct
                //rule = new RegExRewriteRule();
                //rule.VirtualUrl = "^http://" + SM.BLL.Common.Emenik.Data.PortalHostDomain() + "/$";
                //rule.DestinationUrl = "~/default.aspx";
                //rule.IgnoreCase = true;
                //rule.Rewrite = RewriteOption.Domain;
                //rule.Redirect = RedirectOption.None;
                //rule.RewriteUrlParameter = RewriteUrlParameterOption.ExcludeFromClientQueryString;
                //UrlRewriting.AddRewriteRule("MainPageDirect", rule);


                // smardt CMS 
                rule = new RegExRewriteRule();
                rule.VirtualUrl = "^http://" + SM.BLL.Common.Emenik.Data.PortalHostDomain() + "/cms/smardtCMS/(.*)/(.*).aspx";
                rule.DestinationUrl = "~/cms/ManageCMS.aspx?smp=$1";
                rule.IgnoreCase = true;
                rule.Rewrite = RewriteOption.Domain;
                rule.Redirect = RedirectOption.None;
                rule.RewriteUrlParameter = RewriteUrlParameterOption.ExcludeFromClientQueryString;
                UrlRewriting.AddRewriteRule("RewCMSSmardt", rule);
                
                // eko TMP

                // user page direct
                rule = new RegExRewriteRule();
                rule.VirtualUrl = "~/$";
                rule.DestinationUrl = "~/_em/content/default.aspx";
                rule.IgnoreCase = true;
                rule.Rewrite = RewriteOption.Application;
                rule.Redirect = RedirectOption.None;
                rule.RewriteUrlParameter = RewriteUrlParameterOption.ExcludeFromClientQueryString;
                UrlRewriting.AddRewriteRule("RewUserPageDirect", rule);

                // user page register
                rule = new RegExRewriteRule();
                rule.VirtualUrl = "~/(.*)/register";
                rule.DestinationUrl = "~/_em/content/membership/register.aspx?c=$1";
                rule.IgnoreCase = true;
                rule.Rewrite = RewriteOption.Application;
                rule.Redirect = RedirectOption.None;
                rule.RewriteUrlParameter = RewriteUrlParameterOption.ExcludeFromClientQueryString;
                UrlRewriting.AddRewriteRule("RewUserPageRegister", rule);
                // user page email verify
                rule = new RegExRewriteRule();
                rule.VirtualUrl = "~/(.*)/verify";
                rule.DestinationUrl = "~/_em/content/membership/verify.aspx?c=$1";
                rule.IgnoreCase = true;
                rule.Rewrite = RewriteOption.Application;
                rule.Redirect = RedirectOption.None;
                rule.RewriteUrlParameter = RewriteUrlParameterOption.ExcludeFromClientQueryString;
                UrlRewriting.AddRewriteRule("RewUserPageEmailVerify", rule);
                // user page login
                rule = new RegExRewriteRule();
                rule.VirtualUrl = "~/(.*)/login";
                rule.DestinationUrl = "~/_em/content/membership/login.aspx?c=$1";
                rule.IgnoreCase = true;
                rule.Rewrite = RewriteOption.Application;
                rule.Redirect = RedirectOption.None;
                rule.RewriteUrlParameter = RewriteUrlParameterOption.ExcludeFromClientQueryString;
                UrlRewriting.AddRewriteRule("RewUserPageLogin", rule);

                // user page pass recovery
                rule = new RegExRewriteRule();
                rule.VirtualUrl = "~/(.*)/password-recovery";
                rule.DestinationUrl = "~/_em/content/membership/forgot-password.aspx?c=$1";
                rule.IgnoreCase = true;
                rule.Rewrite = RewriteOption.Application;
                rule.Redirect = RedirectOption.None;
                rule.RewriteUrlParameter = RewriteUrlParameterOption.ExcludeFromClientQueryString;
                UrlRewriting.AddRewriteRule("RewUserPageForgotPass", rule);

                // search page
                rule = new RegExRewriteRule();
//                rule.VirtualUrl = "~/(.*)/search/(.*)";
                rule.VirtualUrl = "~/(.*)/search";
                rule.DestinationUrl = "~/_em/content/search.aspx?c=$1";
                rule.IgnoreCase = true;
                rule.Rewrite = RewriteOption.Application;
                rule.Redirect = RedirectOption.None;
                rule.RewriteUrlParameter = RewriteUrlParameterOption.ExcludeFromClientQueryString;
                UrlRewriting.AddRewriteRule("RewUserPageSearch", rule);
                // order page
                rule = new RegExRewriteRule();
                rule.VirtualUrl = "~/(.*)/order";
                rule.DestinationUrl = "~/_em/content/order/order.aspx?c=$1";
                rule.IgnoreCase = true;
                rule.Rewrite = RewriteOption.Application;
                rule.Redirect = RedirectOption.None;
                rule.RewriteUrlParameter = RewriteUrlParameterOption.ExcludeFromClientQueryString;
                UrlRewriting.AddRewriteRule("RewUserPageOrderProduct", rule);
                // predracun page
                rule = new RegExRewriteRule();
                rule.VirtualUrl = "~/(.*)/narocilo-predracun";
                rule.DestinationUrl = "~/_em/content/order/predracun-detail.aspx?c=$1";
                rule.IgnoreCase = true;
                rule.Rewrite = RewriteOption.Application;
                rule.Redirect = RedirectOption.None;
                rule.RewriteUrlParameter = RewriteUrlParameterOption.ExcludeFromClientQueryString;
                UrlRewriting.AddRewriteRule("RewUserPageOrderPredracunProduct", rule);

                // backend orders page
                rule = new RegExRewriteRule();
                rule.VirtualUrl = "~/ba-orders";
                rule.DestinationUrl = "~/c/user/shopping-order/orders.aspx";
                rule.IgnoreCase = true;
                rule.Rewrite = RewriteOption.Application;
                rule.Redirect = RedirectOption.None;
                rule.RewriteUrlParameter = RewriteUrlParameterOption.ExcludeFromClientQueryString;
                UrlRewriting.AddRewriteRule("RewUserPageBAOrders", rule);

                
                // article details 
                rule = new RegExRewriteRule();
                rule.VirtualUrl = "~/(.*)/a-(.*)/(.*)$";
                rule.DestinationUrl = "~/_em/content/ArticleDetails.aspx?c=$1&art=$2";
                rule.IgnoreCase = true;
                rule.Rewrite = RewriteOption.Application;
                rule.Redirect = RedirectOption.None;
                rule.RewriteUrlParameter = RewriteUrlParameterOption.ExcludeFromClientQueryString;
                UrlRewriting.AddRewriteRule("RewUserArticleDetails", rule);
                // article list
                rule = new RegExRewriteRule();
                rule.VirtualUrl = "~/(.*)/arhiv/(.*)/(.*)$";
                rule.DestinationUrl = "~/_em/content/ArticleList.aspx?c=$1&smp=$2&cwp=$3";
                rule.IgnoreCase = true;
                rule.Rewrite = RewriteOption.Application;
                rule.Redirect = RedirectOption.None;
                rule.RewriteUrlParameter = RewriteUrlParameterOption.ExcludeFromClientQueryString;
                UrlRewriting.AddRewriteRule("RewUserArticleList", rule);

                // product details 
                rule = new RegExRewriteRule();
                rule.VirtualUrl = "~/(.*)/p-(.*)/(.*)$";
                rule.DestinationUrl = "~/_em/content/product/ProductDetails.aspx?c=$1&pid=$2";
                rule.IgnoreCase = true;
                rule.Rewrite = RewriteOption.Application;
                rule.Redirect = RedirectOption.None;
                rule.RewriteUrlParameter = RewriteUrlParameterOption.ExcludeFromClientQueryString;
                UrlRewriting.AddRewriteRule("RewUserProductDetails", rule);
                // product details new
                rule = new RegExRewriteRule();
                rule.VirtualUrl = "~/(.*)/(.*)/p-(.*)$";
                rule.DestinationUrl = "~/_em/content/product/ProductDetails.aspx?c=$1&pid=$3";
                rule.IgnoreCase = true;
                rule.Rewrite = RewriteOption.Application;
                rule.Redirect = RedirectOption.None;
                rule.RewriteUrlParameter = RewriteUrlParameterOption.ExcludeFromClientQueryString;
                UrlRewriting.AddRewriteRule("RewUserProductDetailsNew", rule);


                // poll list
                rule = new RegExRewriteRule();
                rule.VirtualUrl = "~/(.*)/poll$";
                rule.DestinationUrl = "~/_em/content/PollList.aspx?c=$1";
                rule.IgnoreCase = true;
                rule.Rewrite = RewriteOption.Application;
                rule.Redirect = RedirectOption.None;
                rule.RewriteUrlParameter = RewriteUrlParameterOption.ExcludeFromClientQueryString;
                UrlRewriting.AddRewriteRule("RewUserPollList", rule);

                // settings
                rule = new RegExRewriteRule();
                rule.VirtualUrl = "~/settings(.*)$";
                rule.DestinationUrl = "~/_em/settings/default.aspx";
                rule.IgnoreCase = true;
                rule.Rewrite = RewriteOption.Application;
                rule.Redirect = RedirectOption.None;
                rule.RewriteUrlParameter = RewriteUrlParameterOption.ExcludeFromClientQueryString;
                UrlRewriting.AddRewriteRule("RewUserSettings", rule);
                // help
                rule = new RegExRewriteRule();
                rule.VirtualUrl = "~/help/first-step";
                rule.DestinationUrl = "~/_em/help/first-step.aspx";
                rule.IgnoreCase = true;
                rule.Rewrite = RewriteOption.Application;
                rule.Redirect = RedirectOption.None;
                rule.RewriteUrlParameter = RewriteUrlParameterOption.ExcludeFromClientQueryString;
                UrlRewriting.AddRewriteRule("RewUserHelp", rule);

                // page lang menu
                rule = new RegExRewriteRule();
                rule.VirtualUrl = "~/(.*)/(.*)/(.*)$";
                rule.DestinationUrl = "~/_em/content/default.aspx?c=$1&smp=$2";
                rule.IgnoreCase = true;
                rule.Rewrite = RewriteOption.Application;
                rule.Redirect = RedirectOption.None;
                rule.RewriteUrlParameter = RewriteUrlParameterOption.ExcludeFromClientQueryString;
                UrlRewriting.AddRewriteRule("RewUserPageLangMenu", rule);
                // page lang
                rule = new RegExRewriteRule();
                rule.VirtualUrl = "~/(.*)/default$";
                rule.DestinationUrl = "~/_em/content/default.aspx?c=$1";
                rule.IgnoreCase = true;
                rule.Rewrite = RewriteOption.Application;
                rule.Redirect = RedirectOption.None;
                rule.RewriteUrlParameter = RewriteUrlParameterOption.ExcludeFromClientQueryString;
                UrlRewriting.AddRewriteRule("RewUserPageLang", rule);
            
            }


            public static void AddAnimalFolderRewrites()
            {

                RegExRewriteRule rule;

                //// article details 
                //rule = new RegExRewriteRule();
                //rule.VirtualUrl = "^~/(.*)/(.*)/a-(.*)/(.*)/(.*).aspx";
                ////                rule.DestinationUrl = "~/_em/content/ArticleDetails.aspx?pname=$1&amp;smp=$3&amp;c=$2&amp;art=$5";
                //rule.DestinationUrl = "~/_em/content/ArticleDetails.aspx?pname=$1&smp=$3&c=$2&art=$5";
                //rule.IgnoreCase = true;
                //rule.Rewrite = RewriteOption.Application;
                //rule.Redirect = RedirectOption.None;
                //rule.RewriteUrlParameter = RewriteUrlParameterOption.ExcludeFromClientQueryString;
                //UrlRewriting.AddRewriteRule("RewEmenikUserPageArticleDetails", rule);
                //// article list
                //rule = new RegExRewriteRule();
                //rule.VirtualUrl = "^~/(.*)/(.*)/arhiv/(.*)/(.*).aspx";
                ////                rule.DestinationUrl = "~/_em/content/ArticleList.aspx?pname=$1&amp;c=$2&amp;smp=$3&amp;cwp=$4";
                //rule.DestinationUrl = "~/_em/content/ArticleList.aspx?pname=$1&c=$2&smp=$3&cwp=$4";
                //rule.IgnoreCase = true;
                //rule.Rewrite = RewriteOption.Application;
                //rule.Redirect = RedirectOption.None;
                //rule.RewriteUrlParameter = RewriteUrlParameterOption.ExcludeFromClientQueryString;
                //UrlRewriting.AddRewriteRule("RewEmenikUserPageArticleList", rule);

                //// poll list
                //rule = new RegExRewriteRule();
                //rule.VirtualUrl = "^~/(.*)/(.*)/poll.aspx";
                //rule.DestinationUrl = "~/_em/content/PollList.aspx?pname=$1&c=$2";
                //rule.IgnoreCase = true;
                //rule.Rewrite = RewriteOption.Application;
                //rule.Redirect = RedirectOption.None;
                //rule.RewriteUrlParameter = RewriteUrlParameterOption.ExcludeFromClientQueryString;
                //UrlRewriting.AddRewriteRule("RewEmenikUserPagePollList", rule);
                //// settings
                //rule = new RegExRewriteRule();
                //rule.VirtualUrl = "^~/(.*)/settings/(.*).aspx";
                //rule.DestinationUrl = "~/_em/settings/Default.aspx?pname=$1";
                //rule.IgnoreCase = true;
                //rule.Rewrite = RewriteOption.Application;
                //rule.Redirect = RedirectOption.None;
                //rule.RewriteUrlParameter = RewriteUrlParameterOption.ExcludeFromClientQueryString;
                //UrlRewriting.AddRewriteRule("RewEmenikUserSettings", rule);
                //// help
                //rule = new RegExRewriteRule();
                //rule.VirtualUrl = "^~/(.*)/help/first-step.aspx";
                //rule.DestinationUrl = "~/_em/help/first-step.aspx?pname=$1";
                //rule.IgnoreCase = true;
                //rule.Rewrite = RewriteOption.Application;
                //rule.Redirect = RedirectOption.None;
                //rule.RewriteUrlParameter = RewriteUrlParameterOption.ExcludeFromClientQueryString;
                //UrlRewriting.AddRewriteRule("RewEmenikUserHelp", rule);


                //// page lang menu
                //rule = new RegExRewriteRule();
                //rule.VirtualUrl = "^~/(.*)/(.*)/(.*)/(.*).aspx";
                ////                rule.DestinationUrl = "~/_em/content/Default.aspx?pname=$1&amp;smp=$3&amp;c=$2";
                //rule.DestinationUrl = "~/_em/content/Default.aspx?pname=$1&smp=$3&c=$2";
                //rule.IgnoreCase = true;
                //rule.Rewrite = RewriteOption.Application;
                //rule.Redirect = RedirectOption.None;
                //rule.RewriteUrlParameter = RewriteUrlParameterOption.ExcludeFromClientQueryString;
                //UrlRewriting.AddRewriteRule("RewEmenikUserPageMenuLang", rule);
                //// page lang
                //rule = new RegExRewriteRule();
                //rule.VirtualUrl = "^~/(.*)/(.*)/(.*).aspx";
                ////                rule.DestinationUrl = "~/_em/content/Default.aspx?pname=$1&amp;c=$2";
                //rule.DestinationUrl = "~/_em/content/Default.aspx?pname=$1&c=$2";
                //rule.IgnoreCase = true;
                //rule.Rewrite = RewriteOption.Application;
                //rule.Redirect = RedirectOption.None;
                //rule.RewriteUrlParameter = RewriteUrlParameterOption.ExcludeFromClientQueryString;
                //UrlRewriting.AddRewriteRule("RewEmenikUserPageLang", rule);
                // page
                rule = new RegExRewriteRule();
                rule.VirtualUrl = "~/(.*)/(.*).aspx";
                rule.DestinationUrl = "~/_em/content/Default.aspx?pname=$1";
                rule.IgnoreCase = true;
                rule.Rewrite = RewriteOption.Application;
                rule.Redirect = RedirectOption.None;
                rule.RewriteUrlParameter = RewriteUrlParameterOption.ExcludeFromClientQueryString;
                UrlRewriting.AddRewriteRule("RewEmenikAnimalDetailPage", rule);
            }



            public static void AddFolderRewrites()
            {

                RegExRewriteRule rule;

                // smardt CMS 
                rule = new RegExRewriteRule();
                rule.VirtualUrl = "^~/cms/smardtCMS/(.*)/(.*).aspx";
                rule.DestinationUrl = "~/cms/ManageCMS.aspx?smp=$1";
                rule.IgnoreCase = true;
                rule.Rewrite = RewriteOption.Application;
                rule.Redirect = RedirectOption.None;
                rule.RewriteUrlParameter = RewriteUrlParameterOption.ExcludeFromClientQueryString;
                UrlRewriting.AddRewriteRule("RewCMSSmardt", rule);

                // user page register
                rule = new RegExRewriteRule();
                rule.VirtualUrl = "~/(.*)/(.*)/register.aspx";
                rule.DestinationUrl = "~/_em/content/membership/register.aspx?pname=$1&c=$2";
                rule.IgnoreCase = true;
                rule.Rewrite = RewriteOption.Application;
                rule.Redirect = RedirectOption.None;
                rule.RewriteUrlParameter = RewriteUrlParameterOption.ExcludeFromClientQueryString;
                UrlRewriting.AddRewriteRule("RewEmenikUserPageRegister", rule);
                // user page verify
                rule = new RegExRewriteRule();
                rule.VirtualUrl = "~/(.*)/(.*)/verify.aspx";
                rule.DestinationUrl = "~/_em/content/membership/verify.aspx?pname=$1&c=$2";
                rule.IgnoreCase = true;
                rule.Rewrite = RewriteOption.Application;
                rule.Redirect = RedirectOption.None;
                rule.RewriteUrlParameter = RewriteUrlParameterOption.ExcludeFromClientQueryString;
                UrlRewriting.AddRewriteRule("RewEmenikUserPageEmailVerify", rule);
                // user page login
                rule = new RegExRewriteRule();
                rule.VirtualUrl = "~/(.*)/(.*)/login.aspx";
                rule.DestinationUrl = "~/_em/content/membership/login.aspx?pname=$1&c=$2";
                rule.IgnoreCase = true;
                rule.Rewrite = RewriteOption.Application;
                rule.Redirect = RedirectOption.None;
                rule.RewriteUrlParameter = RewriteUrlParameterOption.ExcludeFromClientQueryString;
                UrlRewriting.AddRewriteRule("RewEmenikUserPageLogin", rule);
                // user page pass recovery
                rule = new RegExRewriteRule();
                rule.VirtualUrl = "~/(.*)/(.*)/password-recovery.aspx";
                rule.DestinationUrl = "~/_em/content/membership/forgot-password.aspx?pname=$1&c=$2";
                rule.IgnoreCase = true;
                rule.Rewrite = RewriteOption.Application;
                rule.Redirect = RedirectOption.None;
                rule.RewriteUrlParameter = RewriteUrlParameterOption.ExcludeFromClientQueryString;
                UrlRewriting.AddRewriteRule("RewEmenikUserPageForgotPass", rule);

                // user page search
                rule = new RegExRewriteRule();
//                rule.VirtualUrl = "~/(.*)/(.*)/search/(.*).aspx";
                rule.VirtualUrl = "~/(.*)/(.*)/search.aspx";
                rule.DestinationUrl = "~/_em/content/search.aspx?pname=$1&c=$2";
                rule.IgnoreCase = true;
                rule.Rewrite = RewriteOption.Application;
                rule.Redirect = RedirectOption.None;
                rule.RewriteUrlParameter = RewriteUrlParameterOption.ExcludeFromClientQueryString;
                UrlRewriting.AddRewriteRule("RewEmenikUserPageSearch", rule);
                // user page order
                rule = new RegExRewriteRule();
                rule.VirtualUrl = "~/(.*)/(.*)/order.aspx";
                rule.DestinationUrl = "~/_em/content/order/order.aspx?pname=$1&c=$2";
                rule.IgnoreCase = true;
                rule.Rewrite = RewriteOption.Application;
                rule.Redirect = RedirectOption.None;
                rule.RewriteUrlParameter = RewriteUrlParameterOption.ExcludeFromClientQueryString;
                UrlRewriting.AddRewriteRule("RewEmenikUserOrderProduct", rule);
                // predracun page
                rule = new RegExRewriteRule();
                rule.VirtualUrl = "~/(.*)/(.*)/narocilo-predracun.aspx";
                rule.DestinationUrl = "~/_em/content/order/predracun-detail.aspx?pname=$1&c=$2";
                rule.IgnoreCase = true;
                rule.Rewrite = RewriteOption.Application;
                rule.Redirect = RedirectOption.None;
                rule.RewriteUrlParameter = RewriteUrlParameterOption.ExcludeFromClientQueryString;
                UrlRewriting.AddRewriteRule("RewEmenikUserOrderPredracunProduct", rule);

                // backend orders page
                rule = new RegExRewriteRule();
                rule.VirtualUrl = "~/(.*)/ba-orders.aspx";
                rule.DestinationUrl = "~/c/user/shopping-order/orders.aspx?pname=$1";
                rule.IgnoreCase = true;
                rule.Rewrite = RewriteOption.Application;
                rule.Redirect = RedirectOption.None;
                rule.RewriteUrlParameter = RewriteUrlParameterOption.ExcludeFromClientQueryString;
                UrlRewriting.AddRewriteRule("RewUserPageBAOrders", rule);

                // article details 
                rule = new RegExRewriteRule();
                rule.VirtualUrl = "^~/(.*)/(.*)/a-(.*)/(.*).aspx";
                //                rule.DestinationUrl = "~/_em/content/ArticleDetails.aspx?pname=$1&amp;smp=$3&amp;c=$2&amp;art=$5";
                rule.DestinationUrl = "~/_em/content/ArticleDetails.aspx?pname=$1&c=$2&art=$3";
                rule.IgnoreCase = true;
                rule.Rewrite = RewriteOption.Application;
                rule.Redirect = RedirectOption.None;
                rule.RewriteUrlParameter = RewriteUrlParameterOption.ExcludeFromClientQueryString;
                UrlRewriting.AddRewriteRule("RewEmenikUserPageArticleDetails", rule);
                // article list
                rule = new RegExRewriteRule();
                rule.VirtualUrl = "^~/(.*)/(.*)/arhiv/(.*)/(.*).aspx";
                //                rule.DestinationUrl = "~/_em/content/ArticleList.aspx?pname=$1&amp;c=$2&amp;smp=$3&amp;cwp=$4";
                rule.DestinationUrl = "~/_em/content/ArticleList.aspx?pname=$1&c=$2&smp=$3&cwp=$4";
                rule.IgnoreCase = true;
                rule.Rewrite = RewriteOption.Application;
                rule.Redirect = RedirectOption.None;
                rule.RewriteUrlParameter = RewriteUrlParameterOption.ExcludeFromClientQueryString;
                UrlRewriting.AddRewriteRule("RewEmenikUserPageArticleList", rule);

                // product details 
                rule = new RegExRewriteRule();
                rule.VirtualUrl = "~/(.*)/(.*)/p-(.*)/(.*)$";
                rule.DestinationUrl = "~/_em/content/product/ProductDetails.aspx?pname=$1&c=$2&pid=$3";
                rule.IgnoreCase = true;
                rule.Rewrite = RewriteOption.Application;
                rule.Redirect = RedirectOption.None;
                rule.RewriteUrlParameter = RewriteUrlParameterOption.ExcludeFromClientQueryString;
                UrlRewriting.AddRewriteRule("RewUserProductDetails", rule);
                // product details new
                rule = new RegExRewriteRule();
                rule.VirtualUrl = "~/(.*)/(.*)/(.*)/p-(.*)$";
                rule.DestinationUrl = "~/_em/content/product/ProductDetails.aspx?pname=$1&c=$2&pid=$4";
                rule.IgnoreCase = true;
                rule.Rewrite = RewriteOption.Application;
                rule.Redirect = RedirectOption.None;
                rule.RewriteUrlParameter = RewriteUrlParameterOption.ExcludeFromClientQueryString;
                UrlRewriting.AddRewriteRule("RewUserProductDetailsNew", rule);



                // poll list
                rule = new RegExRewriteRule();
                rule.VirtualUrl = "^~/(.*)/(.*)/poll.aspx";
                rule.DestinationUrl = "~/_em/content/PollList.aspx?pname=$1&c=$2";
                rule.IgnoreCase = true;
                rule.Rewrite = RewriteOption.Application;
                rule.Redirect = RedirectOption.None;
                rule.RewriteUrlParameter = RewriteUrlParameterOption.ExcludeFromClientQueryString;
                UrlRewriting.AddRewriteRule("RewEmenikUserPagePollList", rule);
                // settings
                rule = new RegExRewriteRule();
                rule.VirtualUrl = "^~/(.*)/settings/(.*).aspx";
                rule.DestinationUrl = "~/_em/settings/Default.aspx?pname=$1";
                rule.IgnoreCase = true;
                rule.Rewrite = RewriteOption.Application;
                rule.Redirect = RedirectOption.None;
                rule.RewriteUrlParameter = RewriteUrlParameterOption.ExcludeFromClientQueryString;
                UrlRewriting.AddRewriteRule("RewEmenikUserSettings", rule);
                // help
                rule = new RegExRewriteRule();
                rule.VirtualUrl = "^~/(.*)/help/first-step.aspx";
                rule.DestinationUrl = "~/_em/help/first-step.aspx?pname=$1";
                rule.IgnoreCase = true;
                rule.Rewrite = RewriteOption.Application;
                rule.Redirect = RedirectOption.None;
                rule.RewriteUrlParameter = RewriteUrlParameterOption.ExcludeFromClientQueryString;
                UrlRewriting.AddRewriteRule("RewEmenikUserHelp", rule);
                // page lang menu
                rule = new RegExRewriteRule();
                rule.VirtualUrl = "^~/(.*)/(.*)/(.*)/(.*).aspx";
                //                rule.DestinationUrl = "~/_em/content/Default.aspx?pname=$1&amp;smp=$3&amp;c=$2";
                rule.DestinationUrl = "~/_em/content/Default.aspx?pname=$1&smp=$3&c=$2";
                rule.IgnoreCase = true;
                rule.Rewrite = RewriteOption.Application;
                rule.Redirect = RedirectOption.None;
                rule.RewriteUrlParameter = RewriteUrlParameterOption.ExcludeFromClientQueryString;
                UrlRewriting.AddRewriteRule("RewEmenikUserPageMenuLang", rule);
                // page lang
                rule = new RegExRewriteRule();
                rule.VirtualUrl = "^~/(.*)/(.*)/(.*).aspx";
                //                rule.DestinationUrl = "~/_em/content/Default.aspx?pname=$1&amp;c=$2";
                rule.DestinationUrl = "~/_em/content/Default.aspx?pname=$1&c=$2";
                rule.IgnoreCase = true;
                rule.Rewrite = RewriteOption.Application;
                rule.Redirect = RedirectOption.None;
                rule.RewriteUrlParameter = RewriteUrlParameterOption.ExcludeFromClientQueryString;
                UrlRewriting.AddRewriteRule("RewEmenikUserPageLang", rule);
                // page
                rule = new RegExRewriteRule();
                rule.VirtualUrl = "~/(.*)/(.*).aspx";
                rule.DestinationUrl = "~/_em/content/Default.aspx?pname=$1";
                rule.IgnoreCase = true;
                rule.Rewrite = RewriteOption.Application;
                rule.Redirect = RedirectOption.None;
                rule.RewriteUrlParameter = RewriteUrlParameterOption.ExcludeFromClientQueryString;
                UrlRewriting.AddRewriteRule("RewEmenikUserPage", rule);
            }






            // remove domain rewrites
            public static void DeleteDomainRewrites(string username)
            {
                // remove all rewrite rules for domain
                string ruleNamePref = "d" + username;

                UrlRewriting.RemoveRewriteRule(ruleNamePref + "ArticleDetails");
                // article list
                UrlRewriting.RemoveRewriteRule(ruleNamePref + "ArticleList");
                // settings
                UrlRewriting.RemoveRewriteRule(ruleNamePref + "Settings");
                // page lang menu
                UrlRewriting.RemoveRewriteRule(ruleNamePref + "PageLangMenu");
                // page lang
                UrlRewriting.RemoveRewriteRule(ruleNamePref + "PageLang");
                // page
                UrlRewriting.RemoveRewriteRule(ruleNamePref + "Page");
                // main domain
                UrlRewriting.RemoveRewriteRule(ruleNamePref + "MainDomain");



            }




        }


        public class Helpers
        {

            public static bool IsDefaultDomain()
            {
                string[] defaultDomains = ConfigurationSettings.AppSettings["DefaultDomains"].ToLower().Split(",".ToCharArray());
                
                string domain = HttpContext.Current.Request.Url.Host.ToLower();


                foreach (string def in defaultDomains)
                {
                    if (domain.Contains(def.Trim()))
                    {
                        return true;
                    }
                }

                return false;
            }

            public static bool IsPortalHostDomain()
            {
                string[] defaultDomains = ConfigurationSettings.AppSettings["DefaultDomains"].ToLower().Split(",".ToCharArray());

                string domain = HttpContext.Current.Request.Url.Host.ToLower();


                foreach (string def in defaultDomains)
                {
                    if (domain == def.Trim())
                    {
                        return true;
                    }
                }

                return false;
            }

        }
    }



}
