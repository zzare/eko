﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _ctrl_module_product_edit_viewProductDescEditJhtmlArea : BaseControlView
{

    public object Data;
    public object[] Parameters;
    public Guid ProductID;
    public IQueryable<ProductRep.PRODUCT_DESC_WRAP> ProductDescs;
    public List<vw_UserCulture> UserCultures;


    protected void Page_Load(object sender, EventArgs e)
    {


        LoadData();
    }



    public void LoadData()
    {



        if (Data  == null)
            return;
        ProductDescs = (IQueryable<ProductRep.PRODUCT_DESC_WRAP>)Data;

        if (Parameters != null)
        {
            if (Parameters[0] != null)
                UserCultures = (List<vw_UserCulture>)Parameters[0];

            if (Parameters.Length > 1 && Parameters[1] != null)
                ProductID = new Guid(Parameters[1].ToString());
        }

        // language
        lvListHeader.DataSource = UserCultures;
        lvListHeader.DataBind();  

        // prodcut descs
        lvListDetail.DataSource = ProductDescs ;
        lvListDetail.DataBind();  
    }





}
