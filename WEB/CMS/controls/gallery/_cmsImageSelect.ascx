﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_cmsImageSelect.ascx.cs" Inherits="SM.UI.Sandbox.Controls._cmsImageSelect" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Namespace="SM.UI.Controls" TagPrefix="SM" %>
<%@ Register TagPrefix="CMS" TagName="ImageList" Src="~/cms/controls/gallery/_cmsImageList.ascx"  %>



<asp:HiddenField ID="hfSelect" runat="server" />

        
<asp:Panel CssClass="modCMSPopup" ID="panSelect" runat="server" style="display:none;">
    <asp:Label ID="Label1" runat="server" Text="Select images to add" CssClass="modCMSTitle"></asp:Label>
    
    <asp:UpdatePanel ID="upFilter" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
         <ContentTemplate>

    
            <CMS:ImageList ID="objImageList" runat="server" ShowCheckboxColumn="true"   />

            <br /><br />
            
            <asp:LinkButton ID="btAddSelectedImage" runat="server" Text="Add SELECTED images" onclick="btAddSelectedImage_Click" />

        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:Button ID="btCancel" runat="server" Text="Cancel" CssClass="btnRed "  />

</asp:Panel>        


    
<cc1:ModalPopupExtender RepositionMode="None"   ID="mpeSelect" runat="server" TargetControlID="hfSelect" PopupControlID="panSelect"
                        BackgroundCssClass="modCMSBcg" CancelControlID="btCancel" >
</cc1:ModalPopupExtender>