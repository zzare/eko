﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _ctrl_module_product_viewProductList : BaseControlView
{

    //public object Data;
    //public object[] Parameters;
    public string LangID;
    public bool ShowCategories = false;
    public bool ShowFirstCategoryDifferent = false;

    public bool IsModeCMS = false;

    public bool AddNewLineClasses = true;

    protected int LastSmapID = -1;

    public int CategoryItemIndex = -1;

    public int GetItemCount { get {
        return lvList.Items.Count;
    
    } }

    public string CatDescRender = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        CategoryItemIndex = -1;
        LoadList();
    }



    public void LoadList() {

        if (Data == null)
            return;

        if (Parameters != null)
        {
            if (Parameters[0] != null)
                IsModeCMS = bool.Parse(Parameters[0].ToString());
            if (Parameters.Length > 1 && Parameters[1] != null)
                LangID = Parameters[1].ToString();
        }

        lvList.DataSource = Data; 
        lvList.DataBind();

        if (lvList.Items.Count <= 0)
            phEmpty.Visible = true;

    }



    protected string RenderListItem(object d)
    {
        
        vw_Product data = (vw_Product )d;
        return ViewManager.RenderView("~/_ctrl/module/product/_viewProductListItem.ascx", data, new object[] {IsModeCMS  });
    
    }

    protected string RenderCategories(object d, bool visible, bool showDesc = true, bool showTitle = true )
    {
        string ret = "";
        CatDescRender = "";

        // do not increment allways
        if (!visible)
            return ret;


        CategoryItemIndex += 1;

        vw_Product prod = (vw_Product )d;

        if (LastSmapID != prod.SMAP_ID) {
            //ret = "SMAP_ID:" + prod.SMAP_ID;
            var sml = SITEMAP.GetSitemapLangByID(prod.SMAP_ID, LANGUAGE.GetCurrentLang());
            string edit_link = "";
            if (IsModeCMS)
                edit_link = string.Format("<a  class='btEditContwthText' href='#' onclick=\"editCatDescLoad({0}, em_g_lang); return false\" ><span>uredi</span></a>", sml.SMAP_ID);


            if (showTitle)
            {
                // add anchor
                string anc = string.Format("<a class='prod_list_anchor' id='pca{0}'></a>", sml.SMAP_ID);
                ret += "<span class='plist_sepa'></span>";
                ret += string.Format("<h2  class='prod_category font_bevan'>{0}" + sml.SML_DESC + "</h2>", anc);
            }

            // desc
            if (!string.IsNullOrWhiteSpace(sml.SML_META_DESC) && showDesc)
            {

                ret += string.Format("<p class='prod_category_desc'>{0}{1}</p>", sml.SML_META_DESC, edit_link );
            }
            CatDescRender += sml.SML_META_DESC ?? "";
            CatDescRender += edit_link;

            

            
            CategoryItemIndex = 0;
        
        }

        LastSmapID = prod.SMAP_ID;

        if (!ShowCategories)
            return "";
        
        return ret;
    
    }
 

    protected string RenderClass(object d, int ind)
    {
        vw_Product data = (vw_Product)d;
        string ret = "";

        ind = CategoryItemIndex;
        if (ind <= 0)
            ind = 0;

        // every 4 items add class
        //if (AddNewLineClasses)
        {
            if (ind % 4 == 3)
                ret += " prod_render_br";
            else
                if (ind % 4 == 0)
                    ret += " prod_render_cl";
        }


        if (data.CAT_ACTION)
            ret += " action ";

        if (!IsModeCMS)
            return ret;

        if (!data.ACTIVE)
            ret += " inactive ";
        else if (data.UNITS_IN_STOCK <= 0)
            ret += " nStock ";

        // debug
        ret += " deb" + ind;
        
        return ret;
    }

}
