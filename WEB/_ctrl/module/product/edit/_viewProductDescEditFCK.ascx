﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewProductDescEditFCK.ascx.cs" Inherits="_ctrl_module_product_edit_viewProductDescEditFCK" %>
<%@ Register Assembly="FredCK.FCKeditorV2" Namespace="FredCK.FCKeditorV2" TagPrefix="FCKeditorV2" %>

<script language = "javascript" type="text/javascript">
    function editProductDescSave(id) {

        var _items = new Array();
        $("#prodDescTabs div.item").each(function (i, val) {
            var _lan = '' + $(this).find('input.hfLang:hidden').val();
            var _title = '' + $(this).find('input:text').val();
            var _fckID = '' + $(this).find('textarea').attr('id');
            var _desc = '';
            var oEditor = FCKeditorAPI.GetInstance('ctl00_lvListDetail_ctrl' + i + '_fckPDesc');
            if (oEditor)  
                _desc= oEditor.GetXHTML(true);

            var _item = new Object();
            _item.Lang = _lan;
            if (_title == 'undefined')
                _title  = null;
            
            _item.Title = _title;
            Array.add(_items, _item);

<% if (EditLongDesc){%>
            _item.Desc = _desc;
            _item.Abstract = null;
<%}   else   { %>
            _item.Abstract = _desc;
            _item.Desc = null;
<%} %>
            //alert(_lan + ' , ' +_desc);
        });


        openModalLoading('Shranjujem...');
        WebServiceCWP.SaveProductDesc(id, em_g_lang, _items, 1, onProductEditSuccessSave, onProductSaveError, id);

    }

    
</script>


<div id="cEditProduct" >
    <div class="modPopup_header modPopup_headerEdit">
        <h4>UREDI OPIS</h4>
        <p>Uredi opise izdelka za različne jezikovne različice</p>
    </div>
    <div class="modPopup_main">
    
        <div id="prodDescTabs">
            <ul>
                <asp:ListView ID="lvListHeader" runat="server"  InsertItemPosition="None" >
                    <LayoutTemplate>
                        <asp:PlaceHolder id="itemPlaceholder" runat="server"></asp:PlaceHolder>
                    </LayoutTemplate>
                    
                    <ItemTemplate>
                         <li><a href="#tabs-<%# Container.DataItemIndex %>"><%# Eval("DESC")  %></a></li>
                    </ItemTemplate>
                </asp:ListView>	 	        
	        </ul>  
    	    
    	    
                <asp:ListView ID="lvListDetail" runat="server"  InsertItemPosition="None" >
                    <LayoutTemplate>
                        <asp:PlaceHolder id="itemPlaceholder" runat="server"></asp:PlaceHolder>
                    </LayoutTemplate>
                    
                    <ItemTemplate>
                        <div id="tabs-<%# Container.DataItemIndex %>" class="item" >
                            <input class="hfLang" type="hidden" value= '<%#  Eval("LANG_ID")%>' />
<%if (ShowTitle){%>                               
                            <span style="font-size:14px;">Naziv izdelka</span>
                            <br />
                            <input style="width:95%" class="modPopup_TBXsettings "  id="tbTitle" maxlength="256" type="text" value='<%#  Eval("NAME")%>' />
                            <br />
<%} %>
                            <span style="font-size:14px;">Opis izdelka</span> 
                            <br />
                            <FCKeditorV2:FCKeditor runat="server"  Width="95%" ID="fckPDesc" ToolbarSet="CMS_Content"  Height="600px" Value='<%# (EditLongDesc) ? Eval("DESCRIPTION_LONG") : Eval("DESCRIPTION_SHORT")%>'  ></FCKeditorV2:FCKeditor>
                            <%--<textarea id="fckDesc<%#Container.DataItemIndex %>" class="fckDesc" style="width:95%"  rows="12" cols="50"   ><%# Eval("DESCRIPTION_LONG")%></textarea>--%>
                        </div>
                    </ItemTemplate>
                </asp:ListView>	    

                
        </div>

      <%-- <a href="#" onclick="initHtmlArea(); return false">init</a>--%>


    </div>   

    <div class="modPopup_footer"> 
        <div class="buttonwrapperModal">
            <a class="lbutton lbuttonConfirm" href="#" onclick="editProductDescSave('<%= ProductID %>'); return false"><span>Shrani</span></a>
            <a class="lbutton lbuttonDelete" href="#" onclick="closeModalLoader(); return false" ><span>Prekliči</span></a>
        </div>
    </div>
</div>