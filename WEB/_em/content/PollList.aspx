﻿<%@ Page Language="C#" ValidateRequest="false" MasterPageFile="~/_em/templates/fall/fall.master" AutoEventWireup="true" CodeFile="PollList.aspx.cs" Inherits="_content_PollList" Title="Untitled Page" %>
<%@ Register TagPrefix="SM" TagName="PollList" Src="~/_ctrl/module/poll/_viewPollList.ascx"  %>
<%@ Register TagPrefix="SM" TagName="EditModulePublic" Src="~/_ctrl/module/_edit/editModulePublic.ascx"   %>

<asp:Content ID="Content2" ContentPlaceHolderID="cph" Runat="Server">
    <div class="pollListW">
        <div class="pollListT"></div>
        <div class="pollListM">
            <div class="pollListMI">
                <SM:PollList ID="objList" runat="server" />
                
                <br />
               <%-- <a href='<%= Page.ResolveUrl(SM.EM.Rewrite.EmenikUserUrl(PageName, CurrentSitemapID)) %>' >&laquo;&nbsp;<%= GetGlobalResourceObject("Modules", "BackToPage")%></a>--%>
               
               
               <SM:EditModulePublic ID="objEditPublic" runat="server" />
            </div>
        </div>    
        <div class="pollListB"></div>
    </div>
</asp:Content>
