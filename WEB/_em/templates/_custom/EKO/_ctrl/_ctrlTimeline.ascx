﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_ctrlTimeline.ascx.cs" Inherits="_em_templates__custom_EKO__ctrl_ctrlTimeline" %>




	    <div class="section" id="section_zgodba">

	    	<div class="heading"><h2>DEŽELA EKOJEDIH GURMANOV</h2></div>

 				<div class="sectionTitleBar">

	    		<div class="sectionTitle">

	    			<div class="sectionTitle_text">

	    				V BESEDI  				

	    			</div>

	    			

	    				<div class="sectionTitle_underline">

	    					<div class="sectionTitle_underline_left"></div>

	    					<div class="sectionTitle_underline_right"></div>

	    					<div class="sectionTitle_underline_center"></div>

	    				</div>	    			

	    			

	    		</div>	

	    	</div>

	    	

	    	

	   	

	    	

	    	<div class="leftright">

						<div class="goleft"><div class="goleft_arrowContainer" onmouseover="startScrollTimeline(-10)" onmouseout="stopScrollTimeline()"></div></div>

						

						<div class="leftright_scrolable" id="timeline_scrolable">

						<div class="timeline" id="timeline">

							<div class="timelinePoints">

								

													

				 				<div class="timelinePoint" id="timelinePoint_0" style="left: -470px">		 				  

				  					<div class="textContainer" id="l0t" style="z-index: 200; left: 365px; display: none; ">

				  						<div class="textContainer_title">(GENEZA) NEKJE V GORIŠKIH BRDIH</div>

				  						Ko sonce začne izgubljati svojo moč, ljudje v družbi prijateljev ob kozarcu dobrega
vina spet oživimo. Pogovor steče kar sam od sebe. Tokrat o lepoti in okusu. Spomnili
smo se na pogled lepih in sočnih rdečih paradižnikov. Tistih, ki nato razočarajo s
svojim okusom. Tako kot nas z okusom običajno razočarajo solata, meso in ostala
hrana s trgovskih polic. Družba trmastih ljudi je imela teh razočaranj dovolj. Zato
smo se odločili nekaj spremeniti ...

				  					</div> 					

										<div id="l0" class="varrow"><img src="./ekorepublika_files/timeline_arrowsempty.png"></div>				

				 				</div>													

													

				 				<div class="timelinePoint" id="timelinePoint_1" style="left: -150px">		 				  

				  					<div class="textContainer" id="l1t" style="z-index: 201; left: 430px; ">

				  						<div class="textContainer_title">(PRVI SADOVI) KORADA, GORIŠKA BRDA</div>


				  						Tri leta je trajalo, da smo idejo spremenili v dejanje. Pri tem smo na lastni koži
izkusili, kako težko je biti kmet. A nismo obupali. Zato smo ponosni, da vam danes
lahko predstavimo prve sadove našega truda: teletino goveda Limousine. Govedo se
pase v idiličnem okolju pašnikov na hribu Korada v Goriških Brdih, kjer jim družbo
delajo le tri prazne domačije. Gosta dlaka jih ščiti pred mrazom, zato veš čas, tudi

zime, preživijo na prostem, v naravi.

				  					</div> 					

										<div id="l1" class="varrow_selected"><img src="./ekorepublika_files/timeline_arrowsempty.png"></div>				

				 				</div>													

													

				 				<div class="timelinePoint" id="timelinePoint_2" style="left: -90px">		 				  

				  					<div class="textContainer" id="l2t" style="z-index: 200; left: 365px; display: none; ">

				  						<div class="textContainer_title">(UVODNA POŠILJKA) LJUBLJANA</div>

				  						Znancem in vsem, ki ste pokazali zanimanje, smo dostavili prvo pošiljko mesa
goveda Limousine. Dostavili smo ga v vakuumskem pakiranju, v katerem meso
ostane dobro za pripravo 14-20 dni. Pripravite in specite ga tako, kakor želite. Lahko
tudi po priloženem receptu.

				  					</div> 					

										<div id="l2" class="varrow"><img src="./ekorepublika_files/timeline_arrowsempty.png"></div>				

				 				</div>													

													

				 				<div class="timelinePoint" id="timelinePoint_3" style="left: 0px">		 				  

				  					<div class="textContainer" id="l3t" style="z-index: 200; left: 365px; display: none; ">

				  						<div class="textContainer_title">(OBETI) GORIŠKA BRDA</div>

				  						Ideja, s katero smo pred tremi leti začeli našo pot, je prerastla svojo zibko. Ljubezen
do dela in druženja nas vodita k novim izzivom. Z okoliškimi eko pridelovalci smo
načeli debato, v upanju, da bomo na našem spletnem mestu lahko kmalu ponudili
tudi tiste okusne sadove neokrnjene narave, ki jih pridelajo oni: sadje, zelenjavo,
vino, vodo - nasploh vse dobro, kar je vredno deliti.

				  					</div> 					

										<div id="l3" class="varrow"><img src="./ekorepublika_files/timeline_arrowsempty.png"></div>				

				 				</div>													

													

				 				<div class="timelinePoint" id="timelinePoint_4" style="left: 65px">		 				  

				  					<div class="textContainer" id="l4t" style="display:none; ">

				  						<div class="textContainer_title">(EKO REPUBLIKA) LJUBLJANA</div>

				  						Vizija sodelovanj s podobno mislečimi eko pridelovalci nas pelje preko mej Korade
in Goriških Brd, v Eko republiko, v brezmejno deželo vseh ljubiteljev zdrave, bio
hrane ter neokrnjene narave.

Dobrodošli!

				  					</div> 					

										<div id="l4" class="varrow"><img src="./ekorepublika_files/timeline_arrowsempty.png"></div>				

				 				</div>													

							
						  </div>

							<div class="timeline_line">

									

							 			<div id="timeline_bullet_0}" class="timeline_bullet" style="left: 83px"></div>												

									

							 			<div id="timeline_bullet_1}" class="timeline_bullet" style="left: 403px"></div>												

									

							 			<div id="timeline_bullet_2}" class="timeline_bullet" style="left: 463px"></div>												

									

							 			<div id="timeline_bullet_3}" class="timeline_bullet" style="left: 553px"></div>												

									

							 			<div id="timeline_bullet_4}" class="timeline_bullet" style="left: 618px"></div>												

																

							</div>

							<div class="timeline_dates">

								

							
							 	<div id="timeline_date_0}" class="timeline_date" style="left: 65px">17.Avg<br>2008</div>												

							
							 	<div id="timeline_date_1}" class="timeline_date" style="left: 385px">3.Feb<br>2011</div>												

							
							 	<div id="timeline_date_2}" class="timeline_date" style="left: 445px">10.Feb<br>2011</div>												

							
							 	<div id="timeline_date_3}" class="timeline_date" style="left: 535px">14.Apr<br>2011</div>												

							
							 	<div id="timeline_date_4}" class="timeline_date" style="left: 600px">21.Jun<br>2011</div>												

							
															

							</div>

													

						</div>

					  </div>

            <div class="goright"><div class="goright_arrowContainer" onmouseover="startScrollTimeline(10)" onmouseout="stopScrollTimeline()"></div></div>

				</div>

				

				<div id="zgodba_content">

					<div id="zgodba_image">

						<img src="./ekorepublika_files/fantazija.jpg">

					</div>

					<div id="zgodba_text_scrolable">

						<div id="zgodba_text">

							

							
							 	<div id="zgodbaScrollText_0" class="zgodbaScrollText">Ko sonce začne izgubljati svojo moč, ljudje v družbi prijateljev ob kozarcu dobrega
vina spet oživimo. Pogovor steče kar sam od sebe. Tokrat o lepoti in okusu. Spomnili
smo se na pogled lepih in sočnih rdečih paradižnikov. Tistih, ki nato razočarajo s
svojim okusom. Tako kot nas z okusom običajno razočarajo solata, meso in ostala
hrana s trgovskih polic. Družba trmastih ljudi je imela teh razočaranj dovolj. Zato
smo se odločili nekaj spremeniti ...</div>												

							
							 	<div id="zgodbaScrollText_1" class="zgodbaScrollText">Tri leta je trajalo, da smo idejo spremenili v dejanje. Pri tem smo na lastni koži
izkusili, kako težko je biti kmet. A nismo obupali. Zato smo ponosni, da vam danes
lahko predstavimo prve sadove našega truda: teletino goveda Limousine. Govedo se
pase v idiličnem okolju pašnikov na hribu Korada v Goriških Brdih, kjer jim družbo
delajo le tri prazne domačije. Gosta dlaka jih ščiti pred mrazom, zato veš čas, tudi

zime, preživijo na prostem, v naravi.</div>												

							
							 	<div id="zgodbaScrollText_2" class="zgodbaScrollText">Znancem in vsem, ki ste pokazali zanimanje, smo dostavili prvo pošiljko mesa
goveda Limousine. Dostavili smo ga v vakuumskem pakiranju, v katerem meso
ostane dobro za pripravo 14-20 dni. Pripravite in specite ga tako, kakor želite. Lahko
tudi po priloženem receptu.</div>												

							
							 	<div id="zgodbaScrollText_3" class="zgodbaScrollText">Ideja, s katero smo pred tremi leti začeli našo pot, je prerastla svojo zibko. Ljubezen
do dela in druženja nas vodita k novim izzivom. Z okoliškimi eko pridelovalci smo
načeli debato, v upanju, da bomo na našem spletnem mestu lahko kmalu ponudili
tudi tiste okusne sadove neokrnjene narave, ki jih pridelajo oni: sadje, zelenjavo,
vino, vodo - nasploh vse dobro, kar je vredno deliti.</div>												

							
							 	<div id="zgodbaScrollText_4" class="zgodbaScrollText">Vizija sodelovanj s podobno mislečimi eko pridelovalci nas pelje preko mej Korade
in Goriških Brd, v Eko republiko, v brezmejno deželo vseh ljubiteljev zdrave, bio
hrane ter neokrnjene narave.

Dobrodošli!</div>												

							
							

						</div>			

					</div>

				</div>				

				

				

			</div>
