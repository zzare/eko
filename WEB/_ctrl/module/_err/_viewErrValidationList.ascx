﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewErrValidationList.ascx.cs" Inherits="SM.EM.UI.Controls._err_viewErrList" %>
<%@ Register Namespace="SM.UI.Controls" TagPrefix="SM"  %>


<asp:ListView ID="lvList" runat="server" InsertItemPosition="None"  >
    <LayoutTemplate>
        <div class="err-message" style="color:Red;">
            <div id="itemPlaceholder" runat="server"></div>
        </div>
                
    </LayoutTemplate>
    
    <ItemTemplate>
        <span >* <%# Eval("ErrorMessage")%></span>
        <br />
    </ItemTemplate>
    
</asp:ListView>

