using System;
using System.IO;
using System.Text;
using System.Diagnostics;
using System.Globalization;
using System.ComponentModel;
using System.Security.Permissions;
using System.Web;
using System.Web.UI;
using System.Web.UI.Design;
using System.Web.UI.WebControls;


namespace SM.EM.UI
{
    public class PageChangeEventArgs : EventArgs
    {
        private int _pageNo;

        public int PageNo
        {
            [DebuggerStepThrough()]
            get
            {
                return _pageNo;
            }
        }

        public PageChangeEventArgs(int pageNo)
        {
            _pageNo = pageNo;
        }
    }

    [AspNetHostingPermission(SecurityAction.Demand, Level = AspNetHostingPermissionLevel.Minimal)]
    [AspNetHostingPermission(SecurityAction.InheritanceDemand, Level = AspNetHostingPermissionLevel.Minimal)]
    [Designer(typeof(PagerDesigner))]
    [ToolboxData("<{0}:Pager runat=\"server\"></{0}:Pager>")]
    [DefaultProperty("CurrentPageNo")]
    [DefaultEvent("PageChanged")]
    public class Pager : Control, IPostBackEventHandler
    {
        public event EventHandler<PageChangeEventArgs> PageChange;

        private const string DefaultFirstText = "<<";
        private const string DefaultPreviousText = "<";
        private const string DefaultNextText = ">";
        private const string DefaultLastText = ">>";

        private const int DefaultRowPerPage = 10;
        private const int DefaultCurrentPage = 1;
        private const int DefaultSliderSize = 8;

        [Category("Styles")]
        [DefaultValue("")]
        public string CssClass
        {
            [DebuggerStepThrough()]
            get
            {
                object obj = ViewState["cssClass"];

                return (obj == null) ? string.Empty : (string)obj;
            }
            [DebuggerStepThrough()]
            set
            {
                ViewState["cssClass"] = value;
            }
        }

        [Category("Styles")]
        [DefaultValue("")]
        public string InfoCssClass
        {
            [DebuggerStepThrough()]
            get
            {
                object obj = ViewState["infoCssClass"];

                return (obj == null) ? string.Empty : (string)obj;
            }
            [DebuggerStepThrough()]
            set
            {
                ViewState["infoCssClass"] = value;
            }
        }

        [Category("Styles")]
        [DefaultValue("")]
        public string CurrentPageCssClass
        {
            [DebuggerStepThrough()]
            get
            {
                object obj = ViewState["currentPageCssClass"];

                return (obj == null) ? string.Empty : (string)obj;
            }
            [DebuggerStepThrough()]
            set
            {
                ViewState["currentPageCssClass"] = value;
            }
        }

        [Category("Styles")]
        [DefaultValue("")]
        public string OtherPageCssClass
        {
            [DebuggerStepThrough()]
            get
            {
                object obj = ViewState["otherPageCssClass"];

                return (obj == null) ? string.Empty : (string)obj;
            }
            [DebuggerStepThrough()]
            set
            {
                ViewState["otherPageCssClass"] = value;
            }
        }

        [Category("Appearance")]
        [DefaultValue(DefaultFirstText)]
        [Localizable(true)]
        public string FirstText
        {
            [DebuggerStepThrough()]
            get
            {
                object obj = ViewState["firstText"];

                return (obj == null) ? DefaultFirstText : (string)obj;
            }
            [DebuggerStepThrough()]
            set
            {
                ViewState["firstText"] = value;
            }
        }

        [Category("Appearance")]
        [DefaultValue(DefaultPreviousText)]
        [Localizable(true)]
        public string PreviousText
        {
            [DebuggerStepThrough()]
            get
            {
                object obj = ViewState["previousText"];

                return (obj == null) ? DefaultPreviousText : (string)obj;
            }
            [DebuggerStepThrough()]
            set
            {
                ViewState["previousText"] = value;
            }
        }

        [Category("Appearance")]
        [DefaultValue(DefaultNextText)]
        [Localizable(true)]
        public string NextText
        {
            [DebuggerStepThrough()]
            get
            {
                object obj = ViewState["nextText"];

                return (obj == null) ? DefaultNextText : (string)obj;
            }
            [DebuggerStepThrough()]
            set
            {
                ViewState["nextText"] = value;
            }
        }

        [Category("Appearance")]
        [DefaultValue(DefaultLastText)]
        [Localizable(true)]
        public string LastText
        {
            [DebuggerStepThrough()]
            get
            {
                object obj = ViewState["lastText"];

                return (obj == null) ? DefaultLastText : (string)obj;
            }
            [DebuggerStepThrough()]
            set
            {
                ViewState["lastText"] = value;
            }
        }

        [Category("Appearance")]
        [DefaultValue(true)]
        public bool ShowTip
        {
            [DebuggerStepThrough()]
            get
            {
                object obj = ViewState["showTip"];

                return (obj == null) ? true : (bool)obj;
            }
            [DebuggerStepThrough()]
            set
            {
                ViewState["showTip"] = value;
            }
        }

        [Category("Appearance")]
        [DefaultValue(false)]
        public bool ShowInfo
        {
            [DebuggerStepThrough()]
            get
            {
                object obj = ViewState["showInfo"];

                return (obj == null) ? false : (bool)obj;
            }
            [DebuggerStepThrough()]
            set
            {
                ViewState["showInfo"] = value;
            }
        }

        [Category("Appearance")]
        [DefaultValue(true)]
        public bool ShowFirstAndLast
        {
            [DebuggerStepThrough()]
            get
            {
                object obj = ViewState["showFirstAndLast"];

                return (obj == null) ? true : (bool)obj;
            }
            [DebuggerStepThrough()]
            set
            {
                ViewState["showFirstAndLast"] = value;
            }
        }

        [Category("Appearance")]
        [DefaultValue(false)]
        public bool ShowPreviousAndNext
        {
            [DebuggerStepThrough()]
            get
            {
                object obj = ViewState["showPreviousAndNext"];

                return (obj == null) ? false : (bool)obj;
            }
            [DebuggerStepThrough()]
            set
            {
                ViewState["showPreviousAndNext"] = value;
            }
        }

        [Category("Appearance")]
        [DefaultValue(true)]
        public bool ShowNumbers
        {
            [DebuggerStepThrough()]
            get
            {
                object obj = ViewState["showNumbers"];

                return (obj == null) ? true : (bool)obj;
            }
            [DebuggerStepThrough()]
            set
            {
                ViewState["showNumbers"] = value;
            }
        }

        [Category("Behavior")]
        [DefaultValue(true)]
        public bool UseSlider
        {
            [DebuggerStepThrough()]
            get
            {
                object obj = ViewState["useSlider"];

                return (obj == null) ? true : (bool)obj;
            }
            [DebuggerStepThrough()]
            set
            {
                ViewState["useSlider"] = value;
            }
        }

        [Category("Behavior")]
        [DefaultValue(DefaultSliderSize)]
        public int SliderSize
        {
            [DebuggerStepThrough()]
            get
            {
                object obj = ViewState["sliderSize"];

                return (obj == null) ? DefaultSliderSize : (int)obj;
            }
            [DebuggerStepThrough()]
            set
            {
                ViewState["sliderSize"] = value;
            }
        }

        [Category("Appearance")]
        [DefaultValue(DefaultRowPerPage)]
        public int RowPerPage
        {
            [DebuggerStepThrough()]
            get
            {
                object obj = ViewState["rowPerPage"];

                return (obj == null) ? DefaultRowPerPage : (int)obj;
            }
            [DebuggerStepThrough()]
            set
            {
                ViewState["rowPerPage"] = value;
            }
        }

        [Browsable(false)]
        public int TotalRow
        {
            [DebuggerStepThrough()]
            get
            {
                object obj = ViewState["totalRow"];

                return (obj == null) ? 0 : (int)obj;
            }
            [DebuggerStepThrough()]
            set
            {
                ViewState["totalRow"] = value;
            }
        }

        [Category("Behavior")]
        [DefaultValue(DefaultCurrentPage)]
        public int CurrentPageNo
        {
            [DebuggerStepThrough()]
            get
            {
                object obj = ViewState["currentPage"];

                return (obj == null) ? DefaultCurrentPage : (int)obj;
            }
            [DebuggerStepThrough()]
            set
            {
                ViewState["currentPage"] = value;
            }
        }

        [Browsable(false)]
        public int TotalPage
        {
            [DebuggerStepThrough()]
            get
            {
                if ((TotalRow % RowPerPage) == 0)
                {
                    return (TotalRow / RowPerPage);
                }
                else
                {
                    double result = (TotalRow / RowPerPage);

                    result = Math.Ceiling(result);

                    return (Convert.ToInt32(result) + 1);
                }
            }
        }

        [Category("Behavior")]
        [DefaultValue(true)]
        public bool HideOnSinglePage
        {
            [DebuggerStepThrough()]
            get
            {
                object obj = ViewState["hideOnSinglePage"];

                return (obj == null) ? true : (bool)obj;
            }
            [DebuggerStepThrough()]
            set
            {
                ViewState["hideOnSinglePage"] = value;
            }
        }

        private static CultureInfo CurrentCulture
        {
            [DebuggerStepThrough()]
            get
            {
                return CultureInfo.CurrentCulture;
            }
        }

        public void RaisePostBackEvent(string eventArgument)
        {
            int pageNo = 0;

            if (int.TryParse(eventArgument, out pageNo))
            {
                if (pageNo > 0)
                {
                    OnPageChange(pageNo);
                }
            }
        }

        protected override void Render(HtmlTextWriter writer)
        {
            if (TotalRow > 0)
            {
                if (RowPerPage > 0)
                {
                    if (CurrentPageNo > 0)
                    {
                        if (TotalPage == 1)
                        {
                            if (!HideOnSinglePage)
                            {
                                RenderContent(writer);
                            }
                        }
                        else
                        {
                            RenderContent(writer);
                        }
                    }
                }
            }

            base.Render(writer);
        }

        private void RenderContent(HtmlTextWriter writer)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Id, this.ClientID);

            if (!string.IsNullOrEmpty(CssClass))
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, CssClass);
            }

            writer.RenderBeginTag(HtmlTextWriterTag.Div);

            if (ShowInfo)
            {
                RenderInfo(writer);
            }

            if ((ShowFirstAndLast) && (CurrentPageNo > 1))
            {
                RenderFirst(writer);
            }

            if ((ShowPreviousAndNext) && (CurrentPageNo > 1))
            {
                RenderPrevious(writer);
            }

            if (ShowNumbers)
            {
                int start = 1;
                int end = TotalPage;
                int sliderSize = SliderSize;

                if ((UseSlider) && (sliderSize > 0))
                {
                    int half = (int)Math.Floor((double)(sliderSize - 1) / 2);

                    int above = (CurrentPageNo + half + ((sliderSize - 1) % 2));
                    int below = (CurrentPageNo - half);

                    if (below < 1)
                    {
                        above += (1 - below);
                        below = 1;
                    }

                    if (above > TotalPage)
                    {
                        below -= (above - TotalPage);

                        if (below < 1)
                        {
                            below = 1;
                        }

                        above = TotalPage;
                    }

                    start = below;
                    end = above;
                }

                for (int i = start; i <= end; i++)
                {
                    if (i == CurrentPageNo)
                    {
                        RenderCurrent(writer);
                    }
                    else
                    {
                        RenderOther(writer, i);
                    }
                }
            }

            if ((ShowPreviousAndNext) && (CurrentPageNo < TotalPage))
            {
                RenderNext(writer);
            }

            if ((ShowFirstAndLast) && (CurrentPageNo < TotalPage))
            {
                RenderLast(writer);
            }

            writer.RenderEndTag();
        }

        private void RenderInfo(HtmlTextWriter writer)
        {
            if (!string.IsNullOrEmpty(InfoCssClass))
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, InfoCssClass);
            }

            writer.RenderBeginTag(HtmlTextWriterTag.Span);
            writer.Write("Page {0} of {1}", CurrentPageNo.ToString(CurrentCulture), TotalPage.ToString(CurrentCulture));
            writer.RenderEndTag();
        }

        private void RenderFirst(HtmlTextWriter writer)
        {
            if (!string.IsNullOrEmpty(OtherPageCssClass))
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, OtherPageCssClass);
            }

            string tip = "to first page";

            writer.RenderBeginTag(HtmlTextWriterTag.Span);
            RenderLink(writer, FirstText, 1, tip);
            writer.RenderEndTag();
        }

        private void RenderPrevious(HtmlTextWriter writer)
        {
            if (!string.IsNullOrEmpty(OtherPageCssClass))
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, OtherPageCssClass);
            }

            int previousPage = (CurrentPageNo - 1);

            string tip =  "to previous page";

            writer.RenderBeginTag(HtmlTextWriterTag.Span);
            RenderLink(writer, PreviousText, previousPage, tip);
            writer.RenderEndTag();
        }

        private void RenderCurrent(HtmlTextWriter writer)
        {
            if (!string.IsNullOrEmpty(CurrentPageCssClass))
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, CurrentPageCssClass);
            }

            if (ShowTip)
            {
                string tip = BuildTip(CurrentPageNo);

                writer.AddAttribute(HtmlTextWriterAttribute.Title, tip);
            }

            writer.RenderBeginTag(HtmlTextWriterTag.Span);
            writer.Write(CurrentPageNo.ToString(CurrentCulture));
            writer.RenderEndTag();
        }

        private void RenderOther(HtmlTextWriter writer, int pageNo)
        {
            if (!string.IsNullOrEmpty(OtherPageCssClass))
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, OtherPageCssClass);
            }

            string tip = BuildTip(pageNo);

            writer.RenderBeginTag(HtmlTextWriterTag.Span);
            RenderLink(writer, pageNo.ToString(), pageNo, tip);
            writer.RenderEndTag();
        }

        private void RenderNext(HtmlTextWriter writer)
        {
            if (!string.IsNullOrEmpty(OtherPageCssClass))
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, OtherPageCssClass);
            }

            int nextPage = (CurrentPageNo + 1);

            string tip = "to next page";

            writer.RenderBeginTag(HtmlTextWriterTag.Span);
            RenderLink(writer, NextText, nextPage, tip);
            writer.RenderEndTag();
        }

        private void RenderLast(HtmlTextWriter writer)
        {
            if (!string.IsNullOrEmpty(OtherPageCssClass))
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, OtherPageCssClass);
            }

            string tip = "to last page";

            writer.RenderBeginTag(HtmlTextWriterTag.Span);
            RenderLink(writer, LastText, TotalPage, tip);
            writer.RenderEndTag();
        }

        private void RenderLink(HtmlTextWriter writer, string text, int pageNo,string tip)
        {
            if (ShowTip)
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Title, tip);
            }

            string postBackEventReference = "javascript:" + this.Page.ClientScript.GetPostBackEventReference(this, pageNo.ToString(CurrentCulture));

            writer.AddAttribute(HtmlTextWriterAttribute.Href, postBackEventReference);
            writer.RenderBeginTag(HtmlTextWriterTag.A);
            writer.Write(text);
            writer.RenderEndTag();
        }

        private string BuildTip(int pageNo)
        {
            StringBuilder msg = new StringBuilder();

            msg.Append((pageNo == CurrentPageNo) ? "Showing " : "Show ");

            msg.Append((((pageNo - 1) * RowPerPage) + 1).ToString(CurrentCulture));
            msg.Append(" to ");
            msg.Append((pageNo == TotalPage ? TotalRow : pageNo * RowPerPage).ToString(CurrentCulture));
            msg.Append(" of ");
            msg.Append(TotalRow.ToString(CurrentCulture));

            return msg.ToString();
        }

        private void OnPageChange(int pageNo)
        {
            if (PageChange != null)
            {
                PageChange(this, new PageChangeEventArgs(pageNo));
            }
        }
    }

    internal sealed class PagerDesigner : ControlDesigner
    {
        private Pager _pager;

        private static CultureInfo CurrentCulture
        {
            [DebuggerStepThrough()]
            get
            {
                return CultureInfo.CurrentCulture;
            }
        }

        public PagerDesigner()
        {
        }

        public override string GetDesignTimeHtml()
        {
            const int pageCount = 10;
            const int currentPage = 1;

            StringBuilder output = new StringBuilder();
            StringWriter sw = new StringWriter(output, CurrentCulture);
            HtmlTextWriter htw = new HtmlTextWriter(sw);

            if (_pager.CssClass.Length > 0)
            {
                htw.AddAttribute(HtmlTextWriterAttribute.Class, _pager.CssClass);
            }

            htw.RenderBeginTag(HtmlTextWriterTag.Div);

            if (_pager.ShowInfo)
            {
                if (_pager.InfoCssClass.Length > 0)
                {
                    htw.AddAttribute(HtmlTextWriterAttribute.Class, _pager.InfoCssClass);
                }

                htw.RenderBeginTag(HtmlTextWriterTag.Span);
                htw.Write(string.Format(CurrentCulture, "Page {0} of {1}", currentPage, pageCount));
                htw.RenderEndTag();
            }

            if (_pager.ShowFirstAndLast)
            {
                if (_pager.OtherPageCssClass.Length > 0)
                {
                    htw.AddAttribute(HtmlTextWriterAttribute.Class, _pager.OtherPageCssClass);
                }

                htw.RenderBeginTag(HtmlTextWriterTag.Span);
                htw.AddAttribute(HtmlTextWriterAttribute.Href, "javascript:void(0)");
                htw.RenderBeginTag(HtmlTextWriterTag.A);
                htw.Write(_pager.FirstText);
                htw.RenderEndTag();
                htw.RenderEndTag();
            }

            if (_pager.ShowPreviousAndNext)
            {
                if (_pager.OtherPageCssClass.Length > 0)
                {
                    htw.AddAttribute(HtmlTextWriterAttribute.Class, _pager.OtherPageCssClass);
                }

                htw.RenderBeginTag(HtmlTextWriterTag.Span);
                htw.AddAttribute(HtmlTextWriterAttribute.Href, "javascript:void(0)");
                htw.RenderBeginTag(HtmlTextWriterTag.A);
                htw.Write(_pager.PreviousText);
                htw.RenderEndTag();
                htw.RenderEndTag();
            }

            if (_pager.ShowNumbers)
            {
                for (int i = 1; i <= pageCount; i++)
                {
                    if (i == currentPage)
                    {
                        if (_pager.CurrentPageCssClass.Length > 0)
                        {
                            htw.AddAttribute(HtmlTextWriterAttribute.Class, _pager.CurrentPageCssClass);
                        }

                        htw.RenderBeginTag(HtmlTextWriterTag.Span);
                        htw.Write(i.ToString(CultureInfo.InvariantCulture));
                        htw.RenderEndTag();
                    }
                    else
                    {
                        if (_pager.OtherPageCssClass.Length > 0)
                        {
                            htw.AddAttribute(HtmlTextWriterAttribute.Class, _pager.OtherPageCssClass);
                        }

                        htw.RenderBeginTag(HtmlTextWriterTag.Span);
                        htw.AddAttribute(HtmlTextWriterAttribute.Href, "javascript:void(0)");
                        htw.RenderBeginTag(HtmlTextWriterTag.A);
                        htw.Write(i.ToString(CultureInfo.InvariantCulture));
                        htw.RenderEndTag();
                        htw.RenderEndTag();
                    }
                }
            }

            if (_pager.ShowPreviousAndNext)
            {
                if (_pager.OtherPageCssClass.Length > 0)
                {
                    htw.AddAttribute(HtmlTextWriterAttribute.Class, _pager.OtherPageCssClass);
                }

                htw.RenderBeginTag(HtmlTextWriterTag.Span);
                htw.AddAttribute(HtmlTextWriterAttribute.Href, "javascript:void(0)");
                htw.RenderBeginTag(HtmlTextWriterTag.A);
                htw.Write(_pager.NextText);
                htw.RenderEndTag();
                htw.RenderEndTag();
            }

            if (_pager.ShowFirstAndLast)
            {
                if (_pager.OtherPageCssClass.Length > 0)
                {
                    htw.AddAttribute(HtmlTextWriterAttribute.Class, _pager.OtherPageCssClass);
                }

                htw.RenderBeginTag(HtmlTextWriterTag.Span);
                htw.AddAttribute(HtmlTextWriterAttribute.Href, "javascript:void(0)");
                htw.RenderBeginTag(HtmlTextWriterTag.A);
                htw.Write(_pager.LastText);
                htw.RenderEndTag();
                htw.RenderEndTag();
            }

            htw.RenderEndTag();
            htw.Close();

            return output.ToString();
        }

        public override void Initialize(IComponent component)
        {
            _pager = (Pager)component;
            base.Initialize(component);
        }
    }
}