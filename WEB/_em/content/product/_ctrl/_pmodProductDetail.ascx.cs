﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SM.UI.Controls;

public partial class _em_content_product__ctrl__pmodProductDetail : BaseControl_CmsWebPart  
{
    public object Data;
    protected vw_Product Prod;

    protected void Page_Load(object sender, EventArgs e)
    {
        // iniit gallery popup
        ParentPage.HasGallery = true;

        mnMain.CurrentWebPartZone = "wpz_prod_1";


        Prod = (vw_Product)Data;

        // load detail
        objProductDetail.Data = Data;

        // load gallery
        objGalleryProduct.CwpID = Prod.CWP_GAL;
//        objGalleryProduct.LoadData();


        // load related
        //ProductRep prep = new ProductRep();
        //if (Prod.ManufacturerID != null)
        //{
        //    var list = prep.GetProductDescListByManufacturer(Prod.ManufacturerID.Value, LANGUAGE.GetCurrentLang(), true, ParentPage.em_user.UserId, Prod.PRODUCT_ID );

        //    objRelated.ShowCategories = false;
        //    objRelated.Data = list.Take(4);
        //    objRelated.LoadList();
        //}

    }
}
