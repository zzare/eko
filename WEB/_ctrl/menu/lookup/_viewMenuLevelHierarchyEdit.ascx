﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewMenuLevelHierarchyEdit.ascx.cs" Inherits="SM.UI.Controls._menu_lookup_cntMenuLevelHierarchyEdit" %>

<ul  id="pme<%= ParentID %>" >
    <asp:ListView ID="lvSort" runat="server" >
        <LayoutTemplate>
            <asp:PlaceHolder ID="itemPlaceHolder" runat="server"></asp:PlaceHolder>
        </LayoutTemplate>
                             
        <ItemTemplate >
           <li id='<%# Eval("SMAP_ID") %>' >
                <a href="#" onclick="editMenuLoad(<%# Eval("SMAP_ID") %>); return false" ><span><%# Eval("SML_TITLE") %></span></a>
                <%# RenderChilds(Container.DataItem ) %>
            </li>            
        </ItemTemplate>
    
    </asp:ListView>

</ul>