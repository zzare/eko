﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_pmod_myAccount.ascx.cs" Inherits="_em_content__customer_pmod_myOrders" %>
<%@ Register Namespace="SM.EM.UI.Controls" TagPrefix="EM"  %>
<%@ Register TagPrefix="SM" TagName="OrderList" Src="~/_em/content/order/_ctrl/_ctrlOrderList.ascx"   %>
<%@ Register TagPrefix="SM" TagName="ChangePassword" Src="~/_em/content/membership/_ctrl/_cntChangePassword.ascx"   %>

<div class="myAccount">
<div class="stepHeaderW">
    <h1>Moj račun</h1>
</div>

<div class="stepShoppingCart" >
    <div class="cont_headW">
        <span class="cont_headHeadT_Text">SPREMENI GESLO</span>
            <div class="cont_headMI">
            
                <SM:ChangePassword ID="objChangePassword" runat="server" />
                
            
            </div>
    </div>
</div>
<br />
<br />
<div class="stepShoppingCart" >
    <div class="cont_headW">
        <span class="cont_headHeadT_Text">MOJI AKTIVNI NAKUPI</span>
            <div class="cont_headMI">
            
                <SM:OrderList ID="objOrderListActive" runat="server" />
            
            </div>
    </div>
</div>
</div>