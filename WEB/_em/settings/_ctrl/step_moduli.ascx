﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="step_moduli.ascx.cs" Inherits="_em_settings__ctrl_step_moduli" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Namespace="SM.UI.Controls" TagPrefix="SM"  %>

<SM:InlineScript runat="server">
    <script type="text/javascript" >
        function init_st_mod () {
        $("div.setModDesc").hide();
        $("div.settModLeftcol h2").click(function() {
        $(this).parents().eq(1).next("div.setModDesc").slideToggle("slow");
                
            });
        }
        $(document).ready(init_st_mod );

        function pageLoad(sender, args) {
            if (args.get_isPartialLoad()) {
                init_st_mod();
            }
        }


    </script>

</SM:InlineScript>


<asp:UpdatePanel ID="upModules" runat="server">
    <ContentTemplate>

        <h1>
            <asp:Literal ID="txtStanje" runat="server" Text="Trenutno virtualno stanje na tvojem računu je: "></asp:Literal>
            <asp:Label ID="txtStanjeSum" runat="server" Text=""></asp:Label>
        </h1>

        <asp:Panel ID="panCurrentState" runat="server" >
            <h2>
            <asp:Literal ID="txtValid" runat="server" Text="Vaše stran je veljavna do: "></asp:Literal>
            <asp:Label ID="txtValidDate" runat="server" Text=""></asp:Label>
            </h2>
        </asp:Panel>

        <asp:Panel ID="panExpired" runat="server" >
            <asp:Literal ID="txtExprired" runat="server" Text="Vaše stran je potekla."></asp:Literal>            
        </asp:Panel>

        <br />
        <br />

        <asp:ListView ID="lvList" runat="server" DataKeyNames="MOD_ID" OnItemDataBound="lvList_OnItemDataBound" >
            <LayoutTemplate>
                <div class="setModCont" >
                    <div runat="server" id="itemPlaceholder" ></div>
                </div>
            </LayoutTemplate>

            <ItemTemplate>
                <asp:HiddenField ID="hfModID" Value='<%# Eval("MOD_ID") %>' runat="server" />
                <asp:HiddenField ID="hfModPrice" Value='<%# Eval("EM_PRICE") %>' runat="server" />
                <asp:HiddenField ID="hfModBuyType" Value='<%# Eval("EM_BUY_TYPE") %>' runat="server" />
                <div class="setModTitle"></div>
                    <div class="settMod clearfix">
                        <div class="settModTwocols clearfix"> 
			                <div class="settModMaincol" ></div>
			                <div class="settModRightcol clearfix">
			                    <div class="settModRightcolPrice"><h2><b><%# GetPrice(Container.DataItem )%></b></h2></div> 
			                    <div class="settModRightcolCbx"><h2><asp:CheckBox ID="cbModule" runat="server" CssClass="settModRightcolCbxx" Text="&nbsp;izberi" AutoPostBack="true" OnCheckedChanged="cbModule_OnCheckChanged" /></h2>
			                    <cc1:ToggleButtonExtender ID="ToggleEx" runat="server"
                                    TargetControlID="cbModule" 
                                    ImageWidth="19" 
                                    ImageHeight="19"
                                    CheckedImageAlternateText="Odkljukaj"
                                    UncheckedImageAlternateText="Obkljukaj"
                                    UncheckedImageUrl="~/_inc/images/design/checkbox_off.gif" 
                                    CheckedImageUrl="~/_inc/images/design/checkbox_on.gif"
                                    DisabledCheckedImageUrl="~/_inc/images/design/checkbox_disabled.gif"
                                    DisabledUncheckedImageUrl="~/_inc/images/design/checkbox_disabled.gif"
                                     />
			                    </div>
			                </div>
		                </div> 
			            <div class="settModLeftcol" ><h2><%# "ELEMENT " + Container.DataItemIndex.ToString() + ": " + ((Eval("EM_NAME") != null) ? Eval("EM_NAME") + " - " : "" ) + Eval("EM_TITLE") %></h2></div>
                    </div>

                <div class="setModDesc"><%# Eval("EM_DESC") %></div>
                
                
                <div class="clearing"></div>
                
                <br />
                
            </ItemTemplate>
            
        </asp:ListView>
        
        

            <div class="settMod settModOra clearfix">
                <div class="settModTwocols settModTwocolsOra clearfix"> 
	                <div class="settModMaincol settModMaincolOra" ></div>
	                <div class="settModRightcol settModRightcolOra">
	                    <div class="settModRightcolPriceOra"><h2><b><asp:Label ID="txtSum" runat="server" Text=""></asp:Label></b></h2></div> 
	                </div>
                </div> 
	            <div class="settModLeftcol settModLeftcolOra" ><h2><asp:Literal ID="txtSumTxt" runat="server" Text="Skupaj LETNI NAJEM"></asp:Literal></h2></div>
            </div>

            <div id="divDiscount" runat="server">
                <h1>Popust: <asp:Literal ID="litDiscountGlobal" runat="server"></asp:Literal></h1>
                
                <asp:PlaceHolder ID="phPersonal" runat="server">
                    <h1>Osebni popust: <asp:Literal ID="litDiscountPersonal" runat="server"></asp:Literal></h1>
                    <h1>Skupaj popust: <asp:Literal ID="litDiscountTotal" runat="server"></asp:Literal></h1>
            
                </asp:PlaceHolder>
                <br />
                
                
                <div class="settMod settModOra clearfix">
                    <div class="settModTwocols settModTwocolsOra clearfix"> 
	                    <div class="settModMaincol settModMaincolOra" ></div>
	                    <div class="settModRightcol settModRightcolOra">
	                        <div class="settModRightcolPriceOra"><h2><b><asp:Label ID="txtSumDiscount" runat="server" Text=""></asp:Label></b></h2></div> 
	                    </div>
                    </div> 
	                <div class="settModLeftcol settModLeftcolOra" ><h2><asp:Literal ID="Literal2" runat="server" Text="Skupaj LETNI NAJEM S POPUSTOM"></asp:Literal></h2></div>
                </div>
            </div>            
            
            <h1>
                <asp:Literal ID="litMonthly" runat="server"></asp:Literal>/mesec**
            </h1>
            ** preračunano na mesec




        <br /><br />


        <asp:Panel ID="panChange" runat="server" >
            <h1>
                <asp:Literal ID="txtValidNew" runat="server" Text="Po shranitvi sprememb bo vaša stran veljavna do: "></asp:Literal>
                <asp:Label ID="txtValidDateNew" runat="server" Text=""></asp:Label>
            </h1>
            <asp:Literal ID="Literal1" runat="server" Text="* Pri dodajanju elementov ni potrebno plačati dodatnih opcij, skrajša se samo datum veljavnosti spletne strani. Ob poteku spletne strani bo izstavljen nov letni račun, ki bo vseboval vse vaše elemente."></asp:Literal>
            
            <br />
            <asp:Button ID="btCancel" runat="server" Text="Prekliči spremembe - refresh" OnClick="btCancel_OnClick" />
        </asp:Panel>

        <br />
        <br />
        
<%--        tmp: user je plačal
        <asp:TextBox ID="tbPrice" runat="server"></asp:TextBox>
        <asp:Button ID="btPayed" runat="server" Text="Plačaj" OnClick="placaj_OnClick" />

        <br />
        <br />
        tmp: clear user (plačilo in vse datume)
        <asp:Button ID="btClear" runat="server" Text="Clear" OnClick="clear_OnClick" />--%>
    
    </ContentTemplate>
</asp:UpdatePanel>
    