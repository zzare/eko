﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

public partial class c_a_Logout : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // get return url
        string returnUrl = "~/";

        if (Request.QueryString["r"] != null) {
            returnUrl = Server.UrlDecode(Request.QueryString["r"]);
        }


        // if user is not logged in... redirect
        if (!User.Identity.IsAuthenticated )
            Response.Redirect(returnUrl);


        // user is logged in -> logout
        FormsAuthentication.SignOut();

        // clear session
        SM.EM.Security.Login.LogOut();


//        SM.EM.Security.Login.LogOut();
        //Response.Cookies[FormsAuthentication.FormsCookieName].Expires = DateTime.Now.AddYears(-10);

        // redirect
        Response.Redirect(returnUrl);
    }

}
;