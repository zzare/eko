﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _ctrl_module_product_attribute_viewAttributeEdit : BaseControlView
{

    public object Data;
    public object[] Parameters;
    public Guid ProductID;
    public GROUP_ATTRIBUTE GroupAttribute;
    public List<ATTRIBUTE> AttributeList;
    public List<PRODUCT_ATTRIBUTE> ProductAttributeList;


    protected void Page_Load(object sender, EventArgs e)
    {
        LoadData();
    }



    public void LoadData()
    {



        if (Data == null)
        {
            return;
        }
        ProductID = (Guid)Data;

        if (Parameters != null)
        {
            if (Parameters.Length > 0 && Parameters[0] != null)
                IsModeCMS = bool.Parse(Parameters[0].ToString());
            if (Parameters.Length > 1 && Parameters[1] != null)
                GroupAttribute = (GROUP_ATTRIBUTE)Parameters[1];
            if (Parameters.Length > 2 && Parameters[2] != null)
                AttributeList = (List<ATTRIBUTE>)Parameters[2];
            if (Parameters.Length > 3 && Parameters[3] != null)
                ProductAttributeList = (List<PRODUCT_ATTRIBUTE >)Parameters[3];

        }
        // hide if not set
        if (GroupAttribute == null || AttributeList == null)
        {
            this.Visible = false;
            return;
        }
        // manual load
        if (ProductAttributeList == null) { 
            ProductRep prep = new ProductRep();

            ProductAttributeList = prep.GetProductAttributesByProduct(ProductID).ToList();
        }

        // bind data
        lvList.DataSource = AttributeList;
        lvList.DataBind();


    }


    protected string RenderChecked(object o)
    {
        string ret = "";

        if (ProductAttributeList == null)
            return ret;

        ATTRIBUTE ga = (ATTRIBUTE)o;

        if (ProductAttributeList.Exists(w => w.AttributeID  == ga.AttributeID))
            ret = " checked ";

        return ret;
    }


}
