﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Net;
using SM.UI.Controls;

/// <summary>
/// Summary description for BasePage
/// </summary>
namespace SM.EM.UI
{

    public class BasePage_USER_EMENIK : BasePage
    {

        // emenik user data
        public EM_USER em_user
        {

            get
            {
                if (!User.Identity.IsAuthenticated)
                    RequestLogin();
                

                return EM_USER.Current.GetCurrentPage(); 

            }
        }
        public SiteMapDataSource SubMenuSiteMapDataSource;


        public void SetStatus(string sts)
        {
            BaseMasterPage mp = (BaseMasterPage)Page.Master;
            mp.StatusText = sts;
        }
        public  void SetError(string sts)
        {
            BaseMasterPage mp = (BaseMasterPage)Page.Master;
            mp.ErrorText = sts;
        } 



        protected override void OnPreInit(EventArgs e)
        {
            //this.ForceMainPageDomain = true;
            base.OnPreInit(e);

            Theme = "CMS";
            // set user provider
            DefaultProvider = "mainUSER";
            mp.SubMenuSitemapProv = DefaultProvider;
            mp.MainSitemapProv = DefaultProvider;

            if (!User.Identity.IsAuthenticated) {
                RequestLogin();
            }
            
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            // set permissions
            CheckPermissions = true;


            // include
            Helpers.RegisterScriptIncludeAJAX("http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js", this, "jQ");
            Helpers.RegisterScriptIncludeAJAX("http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.min.js", this, "effects");
            Helpers.RegisterScriptIncludeAJAX("http://ajax.microsoft.com/ajax/jquery.validate/1.6/jquery.validate.pack.js", this, "validation");


            // scriptmanager
            AjaxControlToolkit.ToolkitScriptManager sm = AjaxControlToolkit.ToolkitScriptManager.GetCurrent(this) as AjaxControlToolkit.ToolkitScriptManager;
            // combine scripts
            sm.CombineScripts = true;

            if (Rewrite.Helpers.IsPortalHostDomain())
                sm.CombineScriptsHandlerUrl = new Uri("~/_inc/CombineScriptsHandler.ashx", UriKind.Relative);
            else
                sm.CombineScriptsHandlerUrl = new Uri("http://" + SM.BLL.Common.Emenik.Data.PortalHostDomain() + "/_inc/CombineScriptsHandler.ashx");

            sm.ScriptMode = ScriptMode.Release;
            sm.LoadScriptsBeforeUI = false;

            // composite scripts
            ServiceReference srP = new ServiceReference("~/_inc/webservice/WebServiceP.asmx");
            srP.InlineScript = true; // todo : fix caching and set to false
            sm.Services.Add(srP);
        }




        //protected  void ErrorRedirect(int errID) {
        //    Response.Redirect("~/_err/Error.aspx?e=" + errID.ToString());
        
        //}

        protected void ForceSettings()
        {
            if (em_user.USER_TYPE != EM_USER.Common.UserType.NotSet)
                return;

            // force settings before continue
            string settings = "~/c/user/settings.aspx";

//            string ru = "?ru=" + Server.UrlEncode( Request.Url.AbsoluteUri);

            Response.Redirect(Page.ResolveUrl(settings + "?" + GetReturnUrl() + "&e=1"));
        }

 




   }
}