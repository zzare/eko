﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace SM.EM.UI.CMS{

    public partial class ManageMenu : BasePage_CMS 
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack) {
            }
        }
        protected void dvMenu_ItemInserted(object sender, DetailsViewInsertedEventArgs e)
        {
            if (HandleErrors(e.Exception))
            {
                e.ExceptionHandled = true;
                e.KeepInInsertMode = true;
            }
            gvMenu.DataBind();
        }
        protected void gvMenu_RowUpdated(object sender, GridViewUpdatedEventArgs e)
        {
            if (HandleErrors(e.Exception))
            {
                e.ExceptionHandled = true;
                e.KeepInEditMode  = true;
            }
        }
}
}