﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_cmsGalleryList.ascx.cs" Inherits="SM.EM.UI.Controls.cmsGalleryList" %>


<div id="divGalleryList" runat="server" >

        <asp:ListView ID="lvList" runat="server" InsertItemPosition="None" onitemcommand="lvList_ItemCommand"  >
            <LayoutTemplate>
                    <div id="itemPlaceholder" runat="server"></div>
                
            </LayoutTemplate>
            
            <ItemTemplate>
            <div style="float:left; position:relative;">
                <span  class="galBox"  >
                    <%# RenderGalleryName(Eval("GAL_ID")) %>
                    <br />
                    <asp:LinkButton  ID="btEdit" CommandName="selectGal" CommandArgument='<%# Eval("GAL_ID") %>' runat="server"><img id="Img1" runat="server" src='<%# RenderMainImage(Eval("GAL_ID"), Eval("MAIN_IMG_ID")) %>'  /></asp:LinkButton>
                    <br />
                </span>
            </div>
            </ItemTemplate>
            
            <EmptyDataTemplate>
                
                <div class="galEmpty">
                    Ni galerij.
            
                </div>
                
            </EmptyDataTemplate>
            
        
        </asp:ListView>
        
</div>