﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace SM.UI.CMS.EM
{
    public partial class CMS_ManageTheme : SM.EM.UI.CMS.BasePage_CMS 
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //lvList.DataBind();

            
            }

        }
        protected void ldsList_OnSelecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            eMenikDataContext db = new eMenikDataContext();

            var list = from l in db.THEMEs  orderby l.ORDER  ascending select l;

            //if (_langID != "-1") contents = contents.Where(c => c.LANG_ID == _langID);

            e.Result = list;
        }


        protected void ldsList_Inserting(object sender, LinqDataSourceInsertEventArgs e)
        {
            // set GUID
            THEME c = (THEME)e.NewObject;
            c.MASTER_ID = Int32.Parse(((DropDownList)lvList.InsertItem.FindControl("ddlMaster")).SelectedValue);
        }
        protected void lvList_ItemEditing(object sender, ListViewEditEventArgs e)
        {
            // hide insert item
            lvList.InsertItemPosition = InsertItemPosition.None;
        }
        protected void lvList_ItemUpdated(object sender, ListViewUpdatedEventArgs e)
        {
            // show insert item
            lvList.InsertItemPosition = InsertItemPosition.LastItem;

            // clear cache
            SM.EM.BLL.BizObject.PurgeCacheItems(SM.EM.Caching.Key.Theme());

        }
        protected void lvList_ItemCanceling(object sender, ListViewCancelEventArgs e)
        {
            // show insert item
            lvList.InsertItemPosition = InsertItemPosition.LastItem;

        }

#region Validation
        protected void ValidateDuplicates(object sender, ServerValidateEventArgs e)
        {

            int id = -1;

            if (lvList.EditIndex >= 0)
                id =  (Int32 )lvList.DataKeys[lvList.EditIndex].Value ;
 
            string name = e.Value;
            eMenikDataContext db = new eMenikDataContext();
            var recs = (from r in db.THEMEs  where r.Theme   == name   select r );

            // if editing change SQL
            if (id != null)
                recs = recs.Where(w => w.THEME_ID  != id);

            var rec = recs.FirstOrDefault();

            if (rec==null){
                e.IsValid = true;
                return;
            }
            e.IsValid = false;

            CustomValidator val = (CustomValidator)sender;
            val.ErrorMessage = "* Theme ";
            if (rec.Theme  == e.Value)
                val.ErrorMessage += " '" + rec.Theme + "' already exists.";
        }


#endregion

        protected void lvList_ItemCreated(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType == ListViewItemType.DataItem )
            {

            }
        }
    }
}
