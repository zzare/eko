﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class editModule : BaseControl_CmsWebPart 
{
    public event EventHandler OnReload;

    protected override void OnInit(EventArgs e)
    {


        base.OnInit(e);

        ParentPage.EditArticle = objEditArticle;
//        ParentPage.EditContent = objEditContent;
        //ParentPage.EditImageNew = objImageNew;
        //ParentPage.EditImage = objImageEdit;
        //ParentPage.GallerySettings = objGallerySettings;
        //ParentPage.ArticleSettings = objArticleSettings;
        ParentPage.ContentSettings  = objContentSettings;
        ParentPage.ContactFormSettings = objContactFormSettings;
        ParentPage.MapSettings = objMapSettings;

        // show/hide
        //this.Visible = ParentPage.IsEditMode;

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        objEditArticle.OnArticleListChanged += new EventHandler(Reload);

    }

    void Reload(object sender, EventArgs e)
    {
        this.OnReload(this, e);
    }

    public override void LoadData()
    {
        
    }

    protected override void OnPreRender(EventArgs e)
    {
        // show/hide
        objGalleryPopup.Visible = ParentPage.HasGallery;    
        base.OnPreRender(e);
    }


}
