﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_ctrlUserOrderConfirmed.ascx.cs" Inherits="_mailTemplates_ctrlUserOrderConfirmed" %>
<%@ Register TagPrefix="SM" TagName="OrderSummary" Src="~/_em/content/order/_ctrl/_ctrlOrderSummary.ascx"    %>


<table width="668" cellpadding="0" cellspacing="0" border="0" style="background:#fff;">
    <tbody>
        <tr>
            <td>
                <%--<img align="right" alt="eko" src='<%= SM.BLL.Common.ResolveUrl("~/_em/templates/_custom/kolpasan/_inc/images/mailtemplate/kolpa_mailtemp_head.jpg") %>' />
                <br />    

                --%>Pozdravljeni!
                <br />    
                <br />     
                Vaše naročilo je oddano.
                <br />    
                <br />    
                <br />    

                <h1>Podatki o naročilu</h1>

                <SM:OrderSummary ID="objOrderSummary" runat="server" />

                <br />    
                <br />

                <br />    
                <br />

                Lep pozdrav,
                <%= EmPage.PAGE_TITLE %>
                <br />
                <br /><br />
            </td>
        </tr>
        <%--<tr>
            <td>
                <img alt="eko" src='<%= SM.BLL.Common.ResolveUrl("~/_em/templates/_custom/kolpasan/_inc/images/mailtemplate/kolpa_mailtemp_foot.jpg") %>' />
            </td>
        </tr>
--%>
    </tbody>
</table>




