﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewMenuEditDesc.ascx.cs" Inherits="_ctrl_module_content_viewMenuEditDesc" %>
<%@ Register Assembly="FredCK.FCKeditorV2" Namespace="FredCK.FCKeditorV2" TagPrefix="FCKeditorV2" %>
<%@ Register Namespace="SM.UI.Controls" TagPrefix="SM"  %>

<script type="text/javascript">

    function editCatSaveDesc(s, l) {
        var d = '';

        var oEditor = FCKeditorAPI.GetInstance('<%= fckBody.ClientID %>');
        if (oEditor) { d = oEditor.GetXHTML(true); }

        openModalLoading('Shranjujem...');
        WebServiceCWP.SaveMenuDesc(s, l, '', d, onCatDescSaveSuccess, onContentSaveError);
    }

    function onCatDescSaveSuccess(ret) {
        if (ret.Code != 1) {
            setModalLoaderErr(ret.Message, true);
            return;
        }
        closeModalLoader();
        closeModalLoading();
        openModalLoading('Osvežujem...');

        window.location.reload();
        //.each(function () { formatObjectEmbed(this); });
        //.find("embed").attr("wmode", "transparent").each(function() { var cl = $(this).clone(); $(this).replaceWith(cl); });

    }

</script>

<div class="">

    <div class="modPopup_header modPopup_headerEdit">
        <h4>UREDI OPIS KATEGORIJE</h4>
        <p></p>
    </div>
    <div class="modPopup_mainFCK">
        <FCKeditorV2:FCKeditor  Width="100%" ID="fckBody" ToolbarSet="CMS_Content" runat="server" Height="400px"  ></FCKeditorV2:FCKeditor>
    </div>   
    <div class="modPopup_footer"> 
        <div class="buttonwrapperModal">
            <a class="lbutton lbuttonConfirm" href="#" onclick="editCatSaveDesc(<%= Sml.SMAP_ID  %>, em_g_lang); return false"><span>Shrani</span></a>
            <a class="lbutton lbuttonDelete" href="#" onclick="closeModalLoader(); return false" ><span>Prekliči</span></a>
        </div>
    </div>
</div>
