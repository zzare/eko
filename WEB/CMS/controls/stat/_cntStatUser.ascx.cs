﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.DataVisualization.Charting;


public partial class CMS_controls_stat_cntStatUser : BaseControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        InitChart();

    }

    protected void InitChart() { 
        DateTime dateStart = DateTime.Now.AddMonths(-1);



    
        // get chart data
        eMenikDataContext db  = new eMenikDataContext ();
        // registered
        var dReg = from u in db.EM_USERs
                   where u.DATE_REG > dateStart
                   group u by new { u.DATE_REG.Value.Date } into gu
                   select new { UserCount = gu.Count(), DateReg = gu.Key.Date };
        // orders
        var dOrd = from o in db.SHOPPING_ORDERs
                   where o.DateStart > dateStart && o.OrderStatus >= SHOPPING_ORDER.Common.Status.CONFIRMED
                   group o by new { o.DateStart.Date } into gu
                   select new { OrderCount = gu.Count(), OrderSum = gu.Sum(w => w.Total), DateOrder = gu.Key.Date };
        // payments
        var dPay = from o in db.SHOPPING_ORDERs
                   where o.DatePayed > dateStart && o.OrderStatus >= SHOPPING_ORDER.Common.Status.PAYED && o.Payed == true
                   group o by new { o.DatePayed.Value.Date } into gu
                   select new { OrderCount = gu.Count(), OrderSum = gu.Sum(w => w.Total), DateOrder = gu.Key.Date };

        var dRegByMonth = from u in db.EM_USERs
                          where u.DATE_REG > dateStart
                          group u by new { u.DATE_REG.Value.Month, u.DATE_REG.Value.Year  } into gu
                   select new { UserCount = gu.Count(), DateReg =  new DateTime( gu.Key.Year, gu.Key.Month,1)  };

        var dRegCampaign = from u in db.EM_USERs
                           where u.DATE_REG > dateStart
                   group u by new { BonusID =  u.BONUS_USER_ID ,  u.DATE_REG.Value.Date } into gu
                           join mc in db.MARKETING_CAMPAIGNs on gu.Key.BonusID equals mc.CampaignID  into mcd
                           from mcdo in mcd.DefaultIfEmpty()
                   select new { UserCount = gu.Count(), DateReg = gu.Key.Date , BonusID = (gu.Key.BonusID ?? Guid.Empty), Campaign= (mcdo.Name ?? "1dva3.si")   };


        // Set series chart type
        chUsers.Series["Users"].ChartType = SeriesChartType.Column;
        chUsers.Series["Users"]["PointWidth"] = "0.8";
        chUsers.Series["Users"].IsValueShownAsLabel = true;

        chUsers.Series["Orders"].ChartType = SeriesChartType.Column;
        chUsers.Series["Orders"]["PointWidth"] = "0.8";
        chUsers.Series["Orders"].IsValueShownAsLabel = true;
        chUsers.Series["Orders"].LabelFormat = "{0: #,#.00€}";
        
        chUsers.Series["Payments"].ChartType = SeriesChartType.Column;
        chUsers.Series["Payments"]["PointWidth"] = "0.8";
        chUsers.Series["Payments"].IsValueShownAsLabel = true;
        chUsers.Series["Payments"].LabelFormat = "{0: #,#.00€}";
        //        chUsers.Series["Series1"]["BarLabelStyle"] = "Center";
//        chUsers.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;


        //chUsers.Series["Series1"]["DrawingStyle"] = "Cylinder";
//        chUsers.ChartAreas[0].AxisX.Maximum = DateTime.Now.Date.AddDays(1).ToOADate();
        chUsers.ChartAreas[0].AxisX.Interval = 1;
        chUsers.ChartAreas[0].AxisX.MajorGrid.LineColor = System.Drawing.Color.FromArgb(200, 200, 200);
        chUsers.ChartAreas[0].AxisY.MajorGrid.LineColor = System.Drawing.Color.FromArgb(200, 200, 200);


        chUsers.Series["Users"].Points.DataBind(dReg, "DateReg", "UserCount", "Tooltip=UserCount");
        chUsers.Series["Orders"].Points.DataBind(dOrd, "DateOrder", "OrderSum", "Tooltip=OrderSum");
        chUsers.Series["Payments"].Points.DataBind(dPay, "DateOrder", "OrderSum", "Tooltip=OrderSum");
        // Solution 1: Setting axis Interval  
        chUsers.Series[0].AxisLabel = "#VALX{dd.MM.}";


    

        // USERS BY CAMPAIGN
        chUsersByCampaign.ChartAreas[0].AxisX .Interval = 1;
        chUsersByCampaign.ChartAreas[0].AxisX.MajorGrid.LineColor = System.Drawing.Color.FromArgb(200, 200, 200);
        chUsersByCampaign.ChartAreas[0].AxisY.MajorGrid.LineColor = System.Drawing.Color.FromArgb(200, 200, 200);
        //chUsersByMonth.ChartAreas[0].AxisX.IntervalOffset = 0.5;

        // Show data points labels
        //chUsersByMonth.Series["Series1"].Points.DataBind(data, "DateReg", "UserCount", "Tooltip=UserCount");
        chUsersByCampaign.DataBindCrossTable(dRegCampaign, "Campaign", "DateReg", "UserCount", "Tooltip=Campaign, Label=UserCount, LegendText=Campaign");
        chUsersByCampaign.Series[0].AxisLabel = "#VALX{dd.MM.}";
        //chUsersByCampaign.ChartAreas[0].Area3DStyle.Enable3D = true;




        // CHART BY WEEK COMBINED
        chUsersWeekCombined.Series.Add(new Series("Series1"));
        chUsersWeekCombined.Series.Add(new Series("Week"));



        chUsersWeekCombined.Series["Series1"].ChartArea = "cha1";
        chUsersWeekCombined.Series["Series1"].ChartType = SeriesChartType.Column;
        chUsersWeekCombined.Series["Series1"]["PointWidth"] = "0.7";
        chUsersWeekCombined.Series["Series1"].IsValueShownAsLabel = true;
        chUsersWeekCombined.Series["Series1"].Points.DataBind(dReg, "DateReg", "UserCount", "Tooltip=UserCount");


        chUsersWeekCombined.Series["Week"].ChartArea = "cha2";
        chUsersWeekCombined.Series["Week"].ChartType = SeriesChartType.Column;
        chUsersWeekCombined.Series["Week"]["PointWidth"] = "0.7";
        chUsersWeekCombined.Series["Week"].IsValueShownAsLabel = true;
        chUsersWeekCombined.Series["Week"].AxisLabel = "#VALX{dd.MM.yy}";
        // chUsersByMonth.Series["Week"].Points.DataBind(data, "DateReg", "UserCount", "Tooltip=UserCount");

        chUsersWeekCombined.ChartAreas[0].AxisX.Interval = 1;
        chUsersWeekCombined.ChartAreas[0].Area3DStyle.Enable3D = true;
        chUsersWeekCombined.ChartAreas[0].Visible = false;


        chUsersWeekCombined.ChartAreas[1].AxisX.IntervalType = DateTimeIntervalType.Weeks;
        chUsersWeekCombined.ChartAreas[1].AxisX.LabelStyle.Angle = 90;

        //chUsersWeekCombined.ChartAreas[1].Area3DStyle.Enable3D = true;

        chUsersWeekCombined.DataManipulator.Group("SUM", 1, IntervalType.Weeks, "Series1", "Week");


        // CHART BY MONTH
        chUsersByMonth.Series.Add(new Series("Series1"));


        chUsersByMonth.Series["Series1"].ChartArea = "cha1";
        chUsersByMonth.Series["Series1"].ChartType = SeriesChartType.Column;
        chUsersByMonth.Series["Series1"]["PointWidth"] = "0.7";
        chUsersByMonth.Series["Series1"].IsValueShownAsLabel = true;
        chUsersByMonth.Series["Series1"].Points.DataBind(dRegByMonth, "DateReg", "UserCount", "Tooltip=UserCount");
        chUsersByMonth.Series["Series1"].AxisLabel = "#VALX{MMMM}";


        chUsersByMonth.ChartAreas[0].AxisX.Interval = 1;
        chUsersByMonth.ChartAreas[0].AxisX.IntervalType =  DateTimeIntervalType.Months ;
//        chUsersByMonth.ChartAreas[0].AxisX.IntervalAutoMode = IntervalAutoMode.FixedCount ;
        
        //chUsersByMonth.ChartAreas[0].Area3DStyle.Enable3D = true;
        

    }
}
