﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SM.UI.Controls;
using SM.EM.UI.Controls;


public partial class _em_content_order__ctrl__ctrlOrderDetail : System.Web.UI.UserControl  
{

    public SHOPPING_ORDER Order;
    public List<ProductRep.Data.ShoppingCartItem> CartItems;
    protected decimal _totalSum = -1;
    public decimal GetTotalSum
    {
        get
        {
            if (_totalSum > -1)
                return _totalSum;
            _totalSum = 0;
            if (CartItems == null || CartItems.Count == 0)
                return _totalSum;
            _totalSum = (from c in CartItems select c.PriceTotal).Sum();
            return _totalSum;

        }
    }


    public decimal GetShipping
    {
        get
        {
            decimal shipp = SHOPPING_ORDER.GetShippingPriceByTotal(GetTotalSum);
            if (Order != null)
            {
                shipp = Order.CalculateShippingPrice(GetTotalSum);
            }
            return shipp;
        }
    }

    public decimal GetTotalSumWithShipping
    {
        get
        {
            // add shipping
            if (ShowPriceWithShipping)
            {
                decimal shipp = GetShipping;
                return GetTotalSum + shipp;
            }

            return GetTotalSum;
        }
    }




    public bool ShowPriceWithShipping = true;

    protected void Page_Load(object sender, EventArgs e)
    {



    }


    public void LoadData() {


        // bind data
        lvList.DataSource = CartItems;
        lvList.DataBind();

        // load empty
        if (CartItems.Count == 0)
            phEmpty.Visible = true;
    
    }


    public string RenderProductAttributes(object o)
    {
        string ret = "";
        string separator = " | ";
        ProductRep.Data.ShoppingCartItem cartItem = (ProductRep.Data.ShoppingCartItem)o;

        var list = cartItem.AttrList.Where(w => w.GrType != ATTRIBUTE.Common.GrType.RBL_PRICE);

        foreach (ProductRep.Data.ItemGroupAttribute attr in list)
        {
            if (!string.IsNullOrEmpty(ret))
                ret += separator;
            ret += attr.GroupTitle + "=" + attr.Title;
            //ret += attr.Title;   
        }

        return ret;
    }

    public string RenderProductAttributesPrice(object o)
    {
        string ret = "";
        string separator = "";
        ProductRep.Data.ShoppingCartItem cartItem = (ProductRep.Data.ShoppingCartItem)o;

        var list = cartItem.AttrList.Where(w => w.GrType == ATTRIBUTE.Common.GrType.RBL_PRICE);

        foreach (ProductRep.Data.ItemGroupAttribute attr in list)
        {
            if (!string.IsNullOrEmpty(ret))
                ret += separator;

            string val = attr.PTitle;


            ret += string.Format("<br/> {0}", val);

            //ret +=  val;
            //ret += attr.Title;   
        }

        return ret;
    }


}
