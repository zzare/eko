﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SM.UI.Controls;
using SM.EM.UI.Controls;
using System.Web.Security;

public partial class _em_content_order__ctrl__stepSummary : BaseWizardStepOrder 
{
    public event EventHandler OnSaveOK;

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        ShortDesc = "Povzetek nakupa";
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        // order summary
        objOrderSummaryFormatted.PageId = ParentPageEmenik.em_user.UserId;
        objOrderSummaryFormatted.Order = Order;
        objOrderSummaryFormatted.LoadData();

        // online
        if (!IsPostBack)
        {
            
        }
    }


    public override void LoadData()
    {


        ProductRep prep = new ProductRep();

        List<ProductRep.Data.ShoppingCartItem> list = prep.GetCartItemList(SM.EM.BLL.Membership.GetCurrentUserID(), LANGUAGE.GetCurrentLang(), ParentPageEmenik.em_user.UserId, null);
        objShoppingCart.Data = list;
        objShoppingCart.Parameters = new object[] { ParentPageEmenik.em_user.UserId, false, false };
        objShoppingCart.ShowPriceWithShipping = true;
        objShoppingCart.Order = Order;
        objShoppingCart.LoadData();

        if (Order == null)
            return;
        if (Order.PaymentType == SHOPPING_ORDER.Common.PaymentType.ONLINE_e24)
        {

            btNext.Attributes["onclick"] = "return confirm('S potrditvijo boste oddali naročilo. Preusmerjeni boste na plačilno stran za izvedbo plačila.')";

        }


        // if transaction was invalid, set error msg
        // e24
        if (Order.PaymentType == SHOPPING_ORDER.Common.PaymentType.ONLINE_e24) {

            if (Order.TransIDe24 != null)
            {
                Order.TransIDe24 = null;
                Orep.Save();

                // transaction
                OnlineE24PipeRep mpr = new OnlineE24PipeRep();

                string explan = "";
                E24_TRAN  trans = mpr.GetLastTransByOrderID(Order.OrderID);
                explan =  "<br/>" + trans.StateResult;
                //ParentPageEmenik.RegisterDocumentReadyScriptEnd = "setModalLoaderErr(\"" + "<h2>Plačilo ni uspelo.</h2><br/>" + "Prosimo poskusite ponovno čez nekaj trenutkov." + explan + "\", true).dialog( 'option', 'buttons', { 'Ok': function() { $(this).dialog('close'); } } );";


            }


            // mega pos
            if (Order.TransID != null)
            {
                Order.TransID = null;
                Orep.Save();

                // transaction
                MegaPosRep mpr = new MegaPosRep();

                string explan = "";
                MP_TRAN trans = mpr.GetLastTransByOrderID(Order.OrderID);
                explan = "<br/>" + trans.StateResult;
                ParentPageEmenik.RegisterDocumentReadyScriptEnd = "setModalLoaderErr(\"" + "<h2>Plačilo ni uspelo.</h2><br/>" + "Prosimo poskusite ponovno čez nekaj trenutkov." + explan + "\", true).dialog( 'option', 'buttons', { 'Ok': function() { $(this).dialog('close'); } } );";
            }
        }


    }
    protected void btTestSend_Click(object o, EventArgs e)
    {
        // send predracun
        CustomerRep crep = new CustomerRep();
        Orep.SendMailUserOrderPredracun(EM_USER.GetUserByID(Order .PageId.Value), crep.GetCustomerByID(Order .UserId), Order  );
    }
    protected void btNext_Click(object o, EventArgs e)
    { 
        
        Page.Validate("vgConfirm");
        if (!Page.IsValid)
        {
            LoadData();
            return;
        }
        
        //Orep.AddOrder 
        if (Order == null)
            return;

        if (Order.OrderStatus != SHOPPING_ORDER.Common.Status.STEP_4)
            return;

        // check if user is confirmed
        MembershipUser user = SM.EM.BLL.Membership.GetMembershipUser(Page.User.Identity.Name );
        if (user == null)
            return;

        if (!user.IsApproved) {
            litError.Text = "Vaš uporabniški račun še ni potrjen. Preden lahko potrdite naročilo morate potrditi email, ki ste ga prejeli ob registraciji.";
            divError.Visible = true;
            LoadData();
            return;
        }

        // check stock
        ProductRep prep = new ProductRep();
        List<ProductRep.Data.ShoppingCartItem> list = prep.GetCartItemList(SM.EM.BLL.Membership.GetCurrentUserID(), LANGUAGE.GetCurrentLang(), ParentPageEmenik.em_user.UserId, null);
        bool isValidStock = true;
        string errList = "";
        foreach (var item in list) {
            PRODUCT p = prep.GetProductByID(item.ProductID);
            if (p.UNITS_IN_STOCK < item.Quantity) {
                isValidStock = false;
                errList += ", " + item.ProductTitle + " (max:" + p.UNITS_IN_STOCK.ToString() + ")";
            }        
        }

        if (!isValidStock)
        {
            litError.Text = "* Košarica vsebuje prevelike količine izdelkov, ki jih ni na zalogi. Prosimo zmanjšajte količine izdelkov: ";
            litError.Text += errList.TrimStart(", ".ToCharArray()); 
            divError.Visible = true;
            LoadData();
            return;
        }

        // check total
        var _totalSum = (from c in list select c.PriceTotal).Sum();
        if (_totalSum < SM.BLL.Custom.MinimumOrderSum)
        {
            litError.Text = string.Format("* Minimalen nakup je {0}", SM.EM.Helpers.FormatPrice(SM.BLL.Custom.MinimumOrderSum));
            LoadData();
            return;
        }


        Order.DateStart = DateTime.Now;
        
        // save
        List<RuleViolation> errlist = null;

        // online payment
        if (Order.PaymentType == SHOPPING_ORDER.Common.PaymentType.ONLINE_e24)
        {
            // start online payment
            
            errlist = Orep.SubmitOrderOnlinePaymentStart(Order);
        }
        else
        {
            Order.OrderStatus = SHOPPING_ORDER.Common.Status.CONFIRMED;
            errlist = Orep.SubmitOrder(Order);
        }
        if (errlist != null && errlist.Count > 0) {
            litError.Text = ViewManager.RenderView("~/_ctrl/module/_err/_viewErrValidationList.ascx", errlist, new object[] { });
            divError.Visible = true;

            LoadData();
            return;
        }
    

        // if all ok
        OnSaveOK(this, new EventArgs());
    
    }


}
