﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Net.Mail;
using System.Text;
using System.IO;

public partial class _ctrl_content_membership_ctrl_cntRegisterCustomerShortEko : BaseControl_EMENIK 
{
    public bool AutoLogin = true ;
    public bool SendMail = true;

    public string UsrEmail
    {
        get
        {
            return Email.Text.Trim();
    
    
    
    } }

    public event  EventHandler OnRegistrationComplete;

    protected void Page_Load(object sender, EventArgs e)
    {

        // tmp
        //Membership.DeleteUser("zzzare@gmail.com");
        //Membership.DeleteUser("zare_man@hotmail.com");





        if (!IsPostBack) { 
            // disable clientside validation if autenticated
            if (Page.User.Identity.IsAuthenticated)
            {
                valEmailPattern.EnableClientScript = false;
                valRequireEmail.EnableClientScript = false;
                //valRequirePassword.EnableClientScript = false;
//                valRequireUserName.EnableClientScript = false;
//                revUsername.EnableClientScript = false;
                valPasswordLength.EnableClientScript = false;
            
            
            }
        
        
        }
        
    }



    protected bool RegisterUser() {


        // validate input
        Page.Validate("regShort");
        if (!Page.IsValid)
            return false;

        // insert membership user
        MembershipCreateStatus status;


        // password
        string pass = Password.Text;
        string genPass = "";
        if (string.IsNullOrWhiteSpace(pass)) {
            genPass = CUSTOMER.GetRandomPassword(5);
            pass = genPass ;
        }



        MembershipUser usr = Membership.CreateUser(UsrEmail, pass, UsrEmail, "q", "a", true, out status);

        // user is successfully created
        if (status == MembershipCreateStatus.Success)
        {
            // check for bonus
            Guid? bonus = null;
            //if (this.Profile.Emenik.BonusID != null && this.Profile.Emenik.BonusID != Guid.Empty)
            //{
            //    bonus = this.Profile.Emenik.BonusID;
            //    //this.Profile.Save();
            //}


            // insert customer
            CustomerRep crep = new CustomerRep();
            CUSTOMER c = new CUSTOMER { };
            c.CustomerID = new Guid( usr.ProviderUserKey.ToString());
            c.USERNAME = UsrEmail;
            c.COMPANY_ID = null;
            c.PageID = ParentPage.em_user.UserId;
            c.NEWSLETTER_ENABLED = true;
            c.CUSTOMER_TYPE = 1;
            c.CampaignID = bonus;
            c.DISCOUNT_PERCENT = 0;
            c.DISCOUNT_PERCENT2 = 0;

            c.FIRST_NAME = tbName.Text.Trim() ;
            c.LAST_NAME = tbLastName.Text.Trim();
            c.EMAIL = UsrEmail;
            c.STREET = "";
            c.CITY = "";
            c.POSTAL_CODE  = "";
            c.COUNTRY = "";
            c.TELEPHONE = "";

            c.SentWelcomeMail = false;
            c.UserConfirmed = true;
            if (genPass == pass)
            {
             //   c.GeneratedPass = CUSTOMER.EncryptPass(pass);
                c.GeneratedPass = pass;
            }


            crep.Add(c);
            // save
            if (!crep.Save())
            {
                Membership.DeleteUser(UsrEmail);
                return false;
            }


            Response.Write(pass);
            // send confirmation mail
            //SendConfirmationMail(usr, Password.Text);

            return true;

        }
        else
        {
            switch (status)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    cvUsername.IsValid = false;
                    break;
                case MembershipCreateStatus.DuplicateEmail:
                    Response.Write("Email že obstaja");
                    break;
                case MembershipCreateStatus.InvalidUserName:
                    Response.Write("Uporabniško ime je neveljavno");
                    break;

                case MembershipCreateStatus.InvalidPassword :
                    valPasswordLength.IsValid = false;
                    break;
            }


        }

        return false;
    
    
    }

    protected void btRegister_Click(object sender, EventArgs e)
    {

        if (RegisterUser())
        // login new user
        {
            //FormsAuthentication.SetAuthCookie(UsrEmail, false);
            
            ParentPage.RedirectBackIfSpecified();

            // if not spcified, redirect to confirmation
            Response.Redirect( Page.ResolveUrl(SM.BLL.Custom.Url.RegisterCustomer  + "?ac=confirm"));
        
        }


//            SM.EM.Security.Login.LogIn(Email.Text, false, "");


        //OnRegistrationComplete(this, new EventArgs());
    }




    protected void ValidateDuplicate(object o, ServerValidateEventArgs args)
    {
        args.IsValid = true;

        // check for duplicate page name
        if (Membership.GetUser(Email.Text.Trim()) != null )
        {
            args.IsValid = false;
        
        }
        
    }


    protected void ValidateEmail(object o, ServerValidateEventArgs args)
    {
        args.IsValid = true;

        // check for duplicate page name
        //if ( !SM.EM.Helpers.IsValidEmail(Email.Text, false))
        //{
        //    args.IsValid = false;
        
        //}
    }




}
