﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_ctrlNavigation.ascx.cs" Inherits="_em_content_order__ctrl__ctrlNavigation" %>
<%@ Register Namespace="SM.EM.UI.Controls" TagPrefix="EM"  %>


<div class="stepOutW">
    <%--<div class="stepOutT"></div>--%>
    <div class="stepOutM">
        <span class="filterprodS"><%= GetGlobalResourceObject("Product", "stepNavTitle") %>:</span>
        <div class="stepOutMI">
            <a <%= RenderHref(1) %>   class='<%= RenderClass(1) %>' >
                <span class="stepOkS"></span>    
                <span class="stepT"></span>
                <span class="stepM">
                    <span class="stepMI">
                        <span class="stepTitleS">1. <%= GetGlobalResourceObject("Product", "stepNavTitleS1") %></span>
                        <span class="stepDescS">
                            <%= GetGlobalResourceObject("Product", "stepNavDescS1")%>.
                        </span>
                    </span>    
                </span>
                <span class="stepB"></span>
            </a>
            
            <a <%= RenderHref(2) %>   class='<%= RenderClass(2) %>' >
                <span class="stepOkS"></span>    
                <span class="stepT"></span>
                <span class="stepM">
                    <span class="stepMI">
                        <span class="stepTitleS">2. <%= GetGlobalResourceObject("Product", "stepNavTitleS2")%></span>
                        <span class="stepDescS">
                            <%= GetGlobalResourceObject("Product", "stepNavDescS2")%>.
                        </span>
                    </span>    
                </span>
                <span class="stepB"></span>
            </a>    
            <a <%= RenderHref(3) %>   class='<%= RenderClass(3) %>' > 
                <span class="stepOkS"></span>   
                <span class="stepT"></span>
                <span class="stepM">
                    <span class="stepMI">
                        <span class="stepTitleS">3. <%= GetGlobalResourceObject("Product", "stepNavTitleS3")%></span>
                        <span class="stepDescS">
                            <%= GetGlobalResourceObject("Product", "stepNavDescS3")%>.
                        </span>
                    </span>    
                </span>
                <span class="stepB"></span>
            </a>    
            <a <%= RenderHref(4) %>   class='<%= RenderClass(4) %>' > 
                <span class="stepOkS"></span>   
                <span class="stepT"></span>
                <span class="stepM">
                    <span class="stepMI">
                        <span class="stepTitleS">4. <%= GetGlobalResourceObject("Product", "stepNavTitleS4")%></span>
                        <span class="stepDescS">
                            <%= GetGlobalResourceObject("Product", "stepNavDescS4")%>.
                        </span>
                    </span>    
                </span>
                <span class="stepB"></span>
            </a>    
            <a <%= RenderHref(5) %>   class='<%= RenderClass(5) %>' >
                <span class="stepOkS"></span>    
                <span class="stepT"></span>
                <span class="stepM">
                    <span class="stepMI">
                        <span class="stepTitleS">5. <%= GetGlobalResourceObject("Product", "stepNavTitleS5")%></span>
                        <span class="stepDescS">
                            <%= GetGlobalResourceObject("Product", "stepNavDescS5")%>.
                        </span>
                    </span>    
                </span>
                <span class="stepB"></span>
            </a>    
        </div>    
    </div>
    <%--<div class="stepOutB"></div>--%>
</div>                        
