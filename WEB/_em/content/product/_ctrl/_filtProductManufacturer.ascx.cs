﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SM.UI.Controls;
using System.Linq.Expressions;
using LinqKit;

public partial class _em_content_product__ctrl__filtProductManufacturer : BaseControl 
{
    public string filtQS = "fm";
    public string filtQSvalues { get { return Page.Request.QueryString[filtQS]; } }
    public bool IsFilterSet { get { return (FilterValues.Count > 0); } }
    public string CurrentUrlWithout;
    public Guid PageId { get; set; }
    public int SitemapID { get; set; }
    public List<ProductRep.MANUFACTURER_COUNT> ManuList { get; set; }

    public string GetCurrentFilterQS
    {
        get
        {
            if (!IsFilterSet)
                return "";
            return filtQS + "=" + filtQSvalues;
        }
    }


    public List<Guid> FilterValues {
        get {
            if (filtQS == null)
                return null;

            List<Guid> _list = new List<Guid>();
            if (string.IsNullOrEmpty(filtQSvalues))
                return _list;
            string[] filters = filtQSvalues.Split(",".ToCharArray());
            foreach (string item in filters)
            {
                ShortGuid manID = ShortGuid.Empty;
                try
                {
                    manID = new ShortGuid(item.Trim());
                    // add filter
                    _list.Add( manID.Guid);

                }
                catch { }
            }
            return _list;
        }
    }


    public string GetFilterQS(Guid id)
    {
        //Guid id = Guid.Empty;
        //if (SM.EM.Helpers.IsGUID(val))
        //    id = new Guid(val);

        string ret = GetCurrentFilterQS;
            ShortGuid sh = new ShortGuid(id);

            if (id == Guid.Empty)
                return ret;

        // add filter
        if (!FilterValues.Contains(id))
        {
            if (!string.IsNullOrEmpty(ret))
                ret += ",";
            else
                ret = filtQS + "=";
            ret += sh.Value;
        }
        // remove filter
        else {
            ret = "";
            foreach (Guid item in FilterValues) {
                // skip current filter
                if (item == id)
                    continue;
                ShortGuid s = new ShortGuid(item);
                if (!string.IsNullOrEmpty(ret))
                    ret += ",";
                ret += s.Value ;
            }
            if (!string.IsNullOrEmpty(ret))
                ret = filtQS + "=" + ret;
        }

        return ret;
    }

    public string GetFilterFullUrl(Guid  id) {
        string qs = GetFilterQS(id);
        string ret = CurrentUrlWithout;

        if (!ret.Contains("?"))
            ret += "?";
        else
            ret += "&";
        ret += qs;
        return ret;
    }

    public Expression<Func<vw_Product, bool>> GetFilterPredicate
    {
        get
        {

        var predicate = PredicateBuilder.False<vw_Product>();
        if (!IsFilterSet )
            predicate = PredicateBuilder.True<vw_Product>();

        foreach (Guid item in FilterValues)
        {
            Guid id = item; // linq bug
                // add filter
            predicate = predicate.Or(p => p.ManufacturerID == id);

        }
        return predicate;
    } }





    protected void Page_Load(object sender, EventArgs e)
    {
        

    }


    public void LoadData() {

        ProductRep prep = new ProductRep();
        if (ManuList != null)
            lvList.DataSource = ManuList;
        else
            lvList.DataSource = prep.GetManufacturersByParentSmap(SitemapID, PageId);
        lvList.DataBind();

        if (lvList.Items.Count == 0)
            this.Visible = false;
    }


    public string RenderClass(Guid id) { 
        string ret = "";
        if (FilterValues.Contains(id))
            ret = "sel";

        return ret;
    }



}
