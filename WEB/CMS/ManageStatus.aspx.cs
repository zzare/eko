﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

namespace SM.EM.UI.CMS
{
    public partial class CMS_ManageStatus : BasePage_CMS 
    {

        protected override void OnInit(EventArgs e)
        {
            CurrentSitemapID = 786;

            base.OnInit(e);

        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                LoadData();
            }

        }

        protected void LoadData() {



            MembershipUser u = Membership.GetUser("mojka.murenc@hotmail.com", false);
            Response.Write( u.GetPassword());



        }
        

        protected void btRegisterCache_Click(object o, EventArgs e) {
            SM.BLL.ScheduleTask.RegisterCacheEntry();
        
        }
        protected void btNewJobLog_Click(object o, EventArgs e)
        {
            List<JOB_LOG> jobs = JOB.GetJobsToExecute(DateTime.Now);

            foreach (JOB_LOG job in jobs)
            {
                JOB.CreateNewJobLogIfStopped(job.JobID);
            }
        }

        protected void btCustomer_Click(object o, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(tbEmail.Text) || string.IsNullOrWhiteSpace(tbName.Text) || string.IsNullOrWhiteSpace(tbLastname.Text))
            {
                Response.Write("Vpiši vse podatke");
                return;
            }


            RegisterUser(tbName.Text, tbLastname.Text, tbEmail.Text);


        }

        protected void btTest_Click(object o, EventArgs e)
        {
            //RegisterUser("Aleš", "Vrbančič", "ales.vrbancic@luna.si");
            //RegisterUser("Helena", "Čož", "helena.coz@luna.si");
            //RegisterUser("Miha ", "Grobler ", "miha.grobler@luna.si");
            //RegisterUser("Mitja", "Milavc", "mitja.milavec@luna.si ");
            //RegisterUser("Nina", "Vidrih", "nina.vidrih@luna.si");
            //RegisterUser("Špela", "Levičnik Oblak", "spela.oblak@luna.si");

            
            return;

            //RegisterUser("Rok ", "Čeferin", "rok.ceferin@ceferin.si");
            //RegisterUser("Aleksander ", "Čeferin", "aleksander.ceferin@ceferin.si");
            //RegisterUser("Jure ", "Apih", "jure.apih@festfest.si");
            //RegisterUser("Jure ", "Pohar", "jure.pohar@bfro.uni-lj.si");
            //RegisterUser("Bojana", "Jurenec", "bojana.jurenec@vsn.si");
            //RegisterUser("Miža", "Marinšek ", "plus.bioline@siol.net");
            //RegisterUser("Branko", "Čakarmiš", "branko.cakarmis@pop-tv.si");
            //RegisterUser("Tomaž ", "Perovič", "tomaz.perovic@pop-tv.si");
            //RegisterUser("Pavle", "Vrabec", "pavel.vrabec@pop-tv.si");
            //RegisterUser("Barbara", "Smolnikar ", "bsmolnikar@gmail.com");
            //RegisterUser("Matjaž", "Murko", "m.murko@mennyacht.com");
            //RegisterUser("Lara", "Ham", "lara.ham@intercor.si");
            //RegisterUser("Jani", "Mali", "jani.mali@siol.net");
            //RegisterUser("Boštjan ", "Napotnik", "napotnik@gmail.com");
            //RegisterUser("Nataša", "Toš", "natasa.tos@siol.net");
            //RegisterUser("Marjeta ", "Žorž", "marjeta.zorz@guest.arnes.si");
            //RegisterUser("Špela ", "Žorž", "spela.zorz@grey.si");
            //RegisterUser("Arne", "Hodalič", "ardo@siol.net");
            //RegisterUser("Miha ", "Samsa", "miha@samsasped.it");
            //RegisterUser("Polona ", "Klun", "polonaklun@yahoo.com");
            //RegisterUser("Jurij", "Korenc", "jurij@studio37.si ");
            //RegisterUser("Tomaž ", "Kastelic", "t.kastelic@traffic-design.si");
            //RegisterUser("Valentina ", "Smej Novak", "valentina@totaliteta.si ");
            //RegisterUser("Peter", "Bratuša", "peter@felinafilms.si");
            //RegisterUser("Bob ", "Mušič", "bostjan.music@t-2.net");
            //RegisterUser("Roman ", "Berčon", "roman@votanleoburnett.si");
            //RegisterUser("Romana ", "Dernovšek", "romana@yahoo.com");
            //RegisterUser("Barbara", "Kranjc", "bakrajnc@gmail.com");
            //RegisterUser("Matjaž", "Škulj", "skuljmatjaz@gmail.com");
            //RegisterUser("Simona", "Dimic", "simona.dimic@dinersclub.si");
            //RegisterUser("Urška ", "Pirnat", "ursa.tusevljak@gmail.com");
            //RegisterUser("Tomaž", "Drozg", "tomaz.drozg@adriamadia");
            //RegisterUser("Matej", "Bandelj", "matejbandelj@yahoo.it");
            //RegisterUser("Klelija", "Hrovatič", "klelija.hrovatic@siol.net");
            //RegisterUser("Bojana", "Rajh", "bojana.rajh@danfoss.com");
            //RegisterUser("Marino", "Furlan", "marinof@intra.si");
            //RegisterUser("Sonja", "Mozetic", "sonja.mozetic@amis.net");
            //RegisterUser("Senad", "Hilić", "hilsdoo@gmail.com");
            //RegisterUser("Safet", "Ljubljankić   ", "ensa.safet@gmail.com");
            //RegisterUser("Boris  ", "Frank", "frankie@siol.net");
            //RegisterUser("Tatjana", "Rihar", "iztr@siol.net");
            //RegisterUser("Robi", "Hvala", "primet@siol.net");
            //RegisterUser("Nadja ", "Knez", "nadja.knez@bdo.si");
            //RegisterUser("Boris  ", "Lozej", "boris.lozej@meblojogi.si");
            //RegisterUser("Irena", "Marusic", "irena.marusic@siol.net");
            //RegisterUser("Danilo", "Jezeršek", "1825@kmetija-matic.com");
            //RegisterUser("Joško", "Sirk", "info@lasubida.it");
            //RegisterUser("Luca", "Jereb", "luca.jereb@gmail.com");
            //RegisterUser("Ladislav", "Vozelj", "cebelarstvo.vozelj@siol.net");
            //RegisterUser("Andrej", "Škibin", "andrej.skibin@vf.uni-lj.si");
            //RegisterUser("Bojan", "Kavčič", "kavcic.bojan@amis.net");
            //RegisterUser("Martina ", "Podpečan", "kmetija.podpecan@siol.net");
            //RegisterUser("Janko", "Morgan", "olje.morgan@gmail.com");
            //RegisterUser("Stojan", "Ščurek", "scurek.stojan@siol.net");
            //RegisterUser("Valerija", "Simčič", "info@simcic.si");
            //RegisterUser("Vera", "Erzetič", "vinarstvo.erzetic@siol.net");
            //RegisterUser("Martina ", "Simčič", "simcic.martina@gmail.com");
            //RegisterUser("Simona", "Klinec", "klinec@siol.net");
            //RegisterUser("Vesna", "Kristančič", "info@movia.si");
            //RegisterUser("Ajda", "Rotar", "ajda.rotar@gmail.com");
            //RegisterUser("Aleš", "Deisinger", "ales@deisinger.si");
            //RegisterUser("Aljaž", "Fajmut", "aljaz.fajmut@gmail.com");
            //RegisterUser("Janez", "Rakušček", "jrakuscek@yahoo.com");
            //RegisterUser("Jerneja", "Radovič", "jerneja.radovic@luna.si");
            //RegisterUser("Katja", "Godeša", "katja.godesa@gmail.com");
            //RegisterUser("Katja", "Petrin Dornik", "katja.petrin@gmail.com");
            //RegisterUser("Martin", "Stariha", "martin.stariha@gmail.com");
            //RegisterUser("Aleš", "Vrbančič", "ales.vrbancic@luna.si");
            //RegisterUser("Dali", "Bungič", "dali.bungic@luna.si");
            //RegisterUser("Ljubo", "Bratina", "ljubo.bratina@gmail.com");
            //RegisterUser("Martin", "Stariha", "martin.stariha@gmail.com");
            //RegisterUser("Nace", "Tomc", "nace.tomc@gmail.com");
            //RegisterUser("Nina  ", "Gabrijelčič", "gabrijelcic.nina@gmail.com");
            //RegisterUser("Nina ", "Zadnikar", "ninazadnikar@gmail.com");
            //RegisterUser("Peter", "Razpotnik", "peter.razpotnik@luna.si");
            //RegisterUser("Sara", "Mekinc", "sara.mekinc@gmail.com");
            //RegisterUser("Tina", "Stojanovski ", "tina.stojanovski@gmail.com");
            //RegisterUser("Urška", "Saletinger", "urskasaletinger@gmail.com");
            //RegisterUser("Vida", "Vida Kos ", "vida.kos@gmail.com");
            //RegisterUser("Vlasta", "Merc", "vlastamerc@gmail.com");



        }



        protected bool RegisterUser(string ime, string priimek, string email)
        {



            // insert membership user
            MembershipCreateStatus status;

            // password
            string pass = "";
            string genPass = "";
            if (string.IsNullOrWhiteSpace(pass))
            {
                genPass = CUSTOMER.GetRandomPassword(5);
                pass = genPass;
            }

            MembershipUser usr = Membership.CreateUser(email, pass, email, "q", "a", true, out status);

            // user is successfully created
            if (status == MembershipCreateStatus.Success)
            {
                // check for bonus
                Guid? bonus = null;
                //if (this.Profile.Emenik.BonusID != null && this.Profile.Emenik.BonusID != Guid.Empty)
                //{
                //    bonus = this.Profile.Emenik.BonusID;
                //    //this.Profile.Save();
                //}


                // insert customer
                CustomerRep crep = new CustomerRep();
                CUSTOMER c = new CUSTOMER { };
                c.CustomerID = new Guid(usr.ProviderUserKey.ToString());
                c.USERNAME = email;
                c.COMPANY_ID = null;
                c.PageID = SM.BLL.Custom.MainUserID ;
                c.NEWSLETTER_ENABLED = true;
                c.CUSTOMER_TYPE = 1;
                c.CampaignID = bonus;
                c.DISCOUNT_PERCENT = 0;
                c.DISCOUNT_PERCENT2 = 0;

                c.FIRST_NAME =ime;
                c.LAST_NAME = priimek;
                c.EMAIL = email;
                c.STREET = "";
                c.CITY = "";
                c.POSTAL_CODE = "";
                c.COUNTRY = "";
                c.TELEPHONE = "";

                c.SentWelcomeMail = false;
                c.UserConfirmed = true;
                if (genPass == pass)
                {
                    //   c.GeneratedPass = CUSTOMER.EncryptPass(pass);
                    c.GeneratedPass = pass;
                    Response.Write(pass);
                }


                crep.Add(c);
                // save
                if (!crep.Save())
                {
                    Membership.DeleteUser(email );
                    return false;
                }


                // send confirmation mail
                //SendConfirmationMail(usr, Password.Text);

                return true;

            }
            else
            {
                switch (status)
                {
                    case MembershipCreateStatus.DuplicateUserName:
                        Response.Write(email + "DuplicateUserName");
                        break;
                    case MembershipCreateStatus.DuplicateEmail:
                        Response.Write(email + "DuplicateEmail");
                        break;
                    case MembershipCreateStatus.InvalidUserName:
                        Response.Write(email + "InvalidUserName");
                        break;

                    case MembershipCreateStatus.InvalidPassword:
                        Response.Write(email + "InvalidPassword");
                        break;
                }


            }

            return false;


        }




}
}
