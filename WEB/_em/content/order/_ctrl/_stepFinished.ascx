﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_stepFinished.ascx.cs" Inherits="_em_content_order__ctrl__stepFinished" %>
<%@ Register Namespace="SM.EM.UI.Controls" TagPrefix="EM"  %>
<%@ Register TagPrefix="SM" TagName="EditModulePublic" Src="~/_ctrl/module/_edit/editModulePublic.ascx"   %>
<%@ Register TagPrefix="SM" TagName="ShoppingCart" Src="~/_ctrl/module/product/cart/_viewShoppingCart.ascx"   %>        
<%@ Register TagPrefix="SM" TagName="OrderSummaryFormatted" Src="~/_em/content/order/_ctrl/_ctrlOrderSummaryFormatted.ascx"    %>        

<SM:EditModulePublic ID="objEditPublic" runat="server" />

<div class="stepHeaderW">
<h1>Zaključek nakupa artiklov</h1>
<%= RenderFinishedText() %>

</div>

<div class="stepShoppingCart" >
    <div class="contentRightW">
        <span class="contentRightHeadT_Text">PODATKI NAROČILA</span>
            <div class="contentRightMI">
                <a id="A1" class="Aprint"  href="javascript:print()" style="float:right;" >Natisni</a>
                <h3>Status naročila</h3>            
                <%--Naročilo je --%><%= SHOPPING_ORDER.Common.RenderOrderStatus(Order.OrderStatus) %>
                <br /><br />

                 <SM:OrderSummaryFormatted ID="objOrderSummaryFormatted" runat="server" Visible = "true" />
                
                 <asp:PlaceHolder ID="PlaceHolder1" runat="server" Visible="false">

                <h3>Naročnik</h3>
                <%= SHOPPING_ORDER.Common.RenderShoppingOrderUserData(Order)  %>
                <br /><%--<br />--%>

                 
                <h3>Način plačila</h3>
                <%= SHOPPING_ORDER.Common.RenderSHOPPING_ORDERPayment(Order)  %>

                <h3>Način dostave</h3>
                <%= SHOPPING_ORDER.Common.RenderShoppingOrderShippingDetail (Order)  %>
                <br />
<%--                <% if (Order.IsCompany) {%>
                    <h3>Podatki za račun (podjetje)</h3>
                    <%= SHOPPING_ORDER.Common.RenderShoppingOrderCompanyDetail(Order)%>
                    <br />
                <%} %>--%>
                                
                <h3>Vsebina naročila</h3>
                <div style="padding-top:4px;">                
                    <SM:ShoppingCart ID="objShoppingCart" runat="server" />
                </div> 
                 
                 </asp:PlaceHolder>
            </div>
    </div>
</div>


