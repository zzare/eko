﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_cntUserList.ascx.cs" Inherits="SM.UI.Controls._customer_cntUserList" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Namespace="SM.UI.Controls" TagPrefix="SM" %>


<SM:InlineScript runat="server">
    <script type="text/javascript" language="javascript">
    function SetUniqueCheckbox(nameregex, current)
    {
       re = new RegExp(nameregex);
       for(i = 0; i < document.forms[0].elements.length; i++){
          elm = document.forms[0].elements[i];
          
          if (elm.type == 'checkbox')
          {
             if (re.test(elm.name))
             {
                elm.checked = false;
             }
          }
       }
       current.checked = true;
       
    Sys.Application.notifyScriptLoaded();
}
function ToggleAllUsers(all) {
    $("input:checkbox").each(function() {
        this.checked = all.checked;
    });        
}
    </script>
</SM:InlineScript>


<div class="qContList">

        <asp:LinqDataSource ID="ldsList" runat="server" OnSelecting="ldsList_OnSelecting" >
        </asp:LinqDataSource>
        
        <SM:smListView ID="lvList" runat="server" DataSourceID="ldsList" 
            DataKeyNames="CustomerID" InsertItemPosition="None" 
            onitemcommand="lvList_ItemCommand" onitemdatabound="lvList_ItemDataBound" 
            ondatabound="lvList_DataBound" >
            <LayoutTemplate>
                <table  class="tblData" cellpadding="0" cellspacing="0">
                        <thead>
                            <tr id="headerSort" runat="server">
                                <th width="50px" runat="server" id="headCB" ><input type="checkbox" onclick="ToggleAllUsers(this);" /></th>
                                <th ><asp:LinkButton ID="LinkButton2" CommandName="sort" CommandArgument="LAST_NAME" runat="server">Priimek</asp:LinkButton></th>
                                <th ><asp:LinkButton ID="LinkButton1" CommandName="sort" CommandArgument="FIRST_NAME" runat="server">Ime</asp:LinkButton></th>
                                <th ><asp:LinkButton ID="LinkButton4" CommandName="sort" CommandArgument="USERNAME" runat="server">Email</asp:LinkButton></th>
                                <th ><asp:LinkButton ID="LinkButton5" CommandName="sort" CommandArgument="GeneratedPass" runat="server">Gen. geslo</asp:LinkButton></th>
                                <th ><asp:LinkButton ID="LinkButton3" CommandName="sort" CommandArgument="DATE_REG" runat="server">Datum</asp:LinkButton></th>
                                <th width="100px" colspan="2" runat="server"  id="headManage" >Uredi</th>
                            </tr>
                        </thead>
                        <tbody id="itemPlaceholder" runat="server"></tbody>
                        <tfoot>
                            <tr>
                                <th style="text-align:right" id="footTH" runat="server"  >
                                    <asp:DataPager runat="server" ID="DataPager" >
                                        <Fields>
                                            <asp:NextPreviousPagerField  />
                                            <asp:NumericPagerField ButtonCount="5" />
                                        </Fields>
                                    </asp:DataPager>
                                </th>
                            </tr>
                        </tfoot>
                    </table>
            </LayoutTemplate>
            
            <ItemTemplate>
                <tr  class='<%#  Container.DataItemIndex % 2 == 0 ? "  " : " odd " %>'  onmouseover ="this.className='<%#  Container.DataItemIndex % 2 == 0 ? "hovRow" : "hovRowodd" %>'" onmouseout ="this.className = GetCssClass(<%#  Container.DataItemIndex %>)">
                    <td runat="server"  visible='<%# ShowCheckboxColumn %>'>
                        <asp:CheckBox ID="cbSelectUser"  runat="server" />
                        <asp:HiddenField ID="hfUser" runat="server" Value='<%# Eval("CustomerID") %>' />
                    </td>
                    <td>
                        <%# Eval("LAST_NAME")%>
                    </td>
                    <td>
                        <%# Eval("FIRST_NAME")%>
                    </td>
                    <td>
                        <%# Eval("USERNAME")%>
                    </td>
                    <td>
                        <%# Eval("GeneratedPass")%>
                    </td>
                    <td>
                        <%# SM.EM.Helpers.FormatDateLong (Eval("DATE_REG"))%>
                    </td>
                    <td width="50px" runat="server"  visible='<%# ShowManageColumn && ShowEditColumn %>'>
                        <a target='<%# OnEditTarget %>' href='<%# Page.ResolveUrl( CUSTOMER.Common.RenderBackendDetailsHref(new Guid(Eval("CustomerID").ToString()))) %>' title="edit user" >podrobno</a>
                    </td>
                </tr>  
            
            </ItemTemplate>
            
            <EmptyDataTemplate>
                
                <div class="odd">
                    Seznam je prazen.
            
                </div>
                
            </EmptyDataTemplate>
            
        
        </SM:smListView>

</div>
