

	var selectedItem = -1;
	var timelineTimer = 0;
	
	var contactStep = new Array(0,0,0);
  var autoFocus = false;
  
	var ie=false;
	if($.browser.msie) ie=true;
	//if(navigator.appName.indexOf("Explorer") > 0) ie=true;
	var conn="connector.php";
	 	 
  var langMenuTimer = null;
  var swfLoaderTimer = null;
  
  var pano = null;


  var text_ObveznoPolje ="Obvezno polje";
  var text_VeljavenEmail ="Vnesite veljaven email";
  
  var text_empty = new Array( "ime", "email", "sporocilo");
//window.onload = loadSwf;
	
$(document).ready(function(){	
	//Cufon.now();
  loadTooltips();
  
	if(ie){
		//ribbon correction
		document.getElementById("bigribbonC").style.top = 18;
		document.getElementById("smallribbonC").style.top = 18;

		//undelines
		underlines = ie_getElementsByClassName("div", "sectionTitle_underline");
		for(var i in underlines){
    	fotr = underlines[i].parentNode;
    	sirina = fotr.firstChild.offsetWidth;
    	underlines[i].style.width = sirina;
		}		
	}
	
 

  $(".varrow").mouseover(function () {
  	var indxstr = this.id.replace("l", "");
  	expandTextContainer(parseInt(indxstr));
  });
  $(".timeline_bullet").mouseover(function () {
  	var indxstr = this.id.replace("timeline_bullet_", "");
  	expandTextContainer(parseInt(indxstr));
  });  
  $(".timeline_date").mouseover(function () {
  	var indxstr = this.id.replace("timeline_date_", "");
  	expandTextContainer(parseInt(indxstr));
  }); 
  
  loadSwf();
}
);  
  
function ie_getElementsByClassName(tagname, classname){
		i = 0;
		a = document.getElementsByTagName(tagname);
		res = new Array();
		while (element = a[i++]) {
  		if (element.className == classname) {
    		res[i-1] = element; 
  		}
		}	
		return res;
}  
    
  function expandTextContainer(indx){
    //alert(indx);
    if(indx != selectedItem){
    	var textid = "l" + selectedItem + "t";
    	if(selectedItem > -1){
  	  	arr1 = "l" + selectedItem;
  	  	arrobj = document.getElementById(arr1);
  	  	    	
  	  	if(ie){
  	  		$("#"+textid).hide();
  	  		arrobj.style.height = 115;
  	  	}else{
  	  		$("#"+textid).slideUp(400);
  	  	}    	
      	
  	  	//$("#"+textid).animate({height: 0} , {duration: 300, complete:function (){textContainer.style.display="none";}});
  	  	textContainer = document.getElementById(textid); 
  	  	textContainer.style.zIndex = 200;
  	  	textContainer.style.left = 365;
      	
  	  	arrobj.className="varrow";
      }
  	  //alert(textid);
  	  
  	  selectedItem = indx;
  	  textid = "l" + indx + "t";
  	  textContainer = document.getElementById(textid); 
  	  textContainer.style.zIndex = 201;	
  	  document.getElementById("l"+indx).className="varrow_selected";

  	  	    	//returnHeight = document.getElementById("timelinePoint_"+indx).offsetHeight;
  	  //popravk vidnega polja
  	  var ofs = $("#timelinePoint_"+indx).offset();
  	  var ofspar = $("#timeline_scrolable").offset();
  	  var ofdiff = ofspar.left - ofs.left;
  	  //alert(ofdiff);
  	  if(ofdiff > 375) textContainer.style.left = ofdiff; 
  	  if(ie) corr = 380; else corr = 348;
  	  if(ofdiff < 0) textContainer.style.left = ofdiff + corr; 
  	  //alert(ofs.left);alert(ofspar.left);	    	
      //alert(document.getElementById("timeline_scrolable").scrollLeft);
      //alert(textContainer.style.left);
  	  $("#"+textid).slideDown(1000, function(){
  	  	    if(ie){
  	  	    	if( $("#"+textid).is(":visible")) document.getElementById("l"+indx).style.height = 68;
  	  	    }
  	  		}
  	  );
  	  /*
  	  textContainer.style.display="";
  	  $("#"+textid).animate({height: 101}, {duration: 500, complete:function (){
        $("#"+textid).animate({height: 148} , {duration: 200});
       }
      }); 
      */
      	
  	}
  } 
 

  
  function startScrollTimeline(step){
  	if(timelineTimer == 0){	
  		timelineTimer = 1;
  		scrollTimeline(step);
  	}
  }
  function stopScrollTimeline(){
  	timelineTimer = 0;
  }
  
  function scrollTimeline(step){
  	if(timelineTimer == 1){	
  		tlObj = document.getElementById("timeline_scrolable");
  		var ofset = parseInt(tlObj.scrollLeft);
  		
  		ofset = ofset + step;
  		tlObj.scrollLeft = ofset ;
  		
  		textObj = document.getElementById("zgodba_text_scrolable");
      
      var scrollRatio = textObj.scrollHeight / tlObj.scrollWidth;
      
      //alert(scrollRatio);		
      //alert(Math.round(scrollRatio*ofset));
  		textObj.scrollTop = Math.round(scrollRatio*ofset);
  		//alert(Math.round(scrollRatio*ofset) + "," + textObj.scrollTop);
  		
  		setTimeout("scrollTimeline("+step+")", 30);
  	}
  }
  
  function navigatePage(iddiv){
  	var offsection = $("#"+iddiv).offset();

  	$("html, body").animate(
  		{scrollTop: (offsection.top - 20 ) },
  		2000
  	);
  }

  function displayLangMenu(){
  	if($("#menu_sub_jezik").is(':hidden')) $("#menu_sub_jezik").slideDown(900);
  	clearTimeout(langMenuTimer);
  }
  function hideLangMenu(){
  	//divc = document.getElementById("menu_sub_jezik");
  	langMenuTimer = setTimeout(
  	 'if(!$("#menu_sub_jezik").is(\':hidden\')) $("#menu_sub_jezik").slideUp(600);', 3000);
  }  
  
	function isValidEmailAddress(emailAddress) {
		var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
		return pattern.test(emailAddress);
	}  
	
	function contactOnFocus(what, who){
		if(!autoFocus){
     if(contactStep[what] < 10){
     	who.className = "input_normal";
     	who.value="";
     	contactStep[what] = 10;
     }
	 }
		
	}
	
	function contactOnBlur(what, who){
		if(who.value==""){
		 contactStep[what] = 0;
		 who.className = "input_empty";
     who.value = text_empty[what];
	  }	 	
	}
		
	
	
	
	function sendContactMessage(){
		var validfields = true;;
		fname = document.getElementById("contact_name").value;
		femail = document.getElementById("contact_email").value;
		ftext = document.getElementById("contact_text").value;
		
		if(! isValidEmailAddress(femail)){
			validfields = false;
			contactStep[1] = 0;
			//autoFocus=true;
			//document.getElementById("contact_email").focus();
			document.getElementById("contact_email").className = "input_error";
			document.getElementById("contact_email").value = text_VeljavenEmail;
			
		}
		if(fname=="" || contactStep[0] == 0 ){
			contactStep[0] = 0;
			validfields = false;
			//document.getElementById("contact_name").focus();
			document.getElementById("contact_name").className = "input_error";
			document.getElementById("contact_name").value= text_ObveznoPolje;
		}	
		if(ftext=="" || contactStep[2] == 0 ){
			contactStep[2] = 0;
			validfields = false;
			//document.getElementById("contact_text").focus();
			document.getElementById("contact_text").className = "input_error";
			document.getElementById("contact_text").value= text_ObveznoPolje;
		}			
						
		if(validfields){
			$.ajax({
			   url: conn,
			   type: 'POST',
			   data: {action: "sendContactMessage",
			   				fname: fname,
			   				femail: femail,
			   				ftext: ftext
			   	     },
			   success :function(data, textStatus, jqXHR) {	
		  							document.getElementById("kontakt_forma").style.display="none";
		  							document.getElementById("kontakt_forma_hvala").style.display="";
		        			}
		        			
		   });
		 }
		 	
	 }	

  function checkStartInput(inputid){
  }

  function startInput(inputid){
  }

  
  function highlightRibbon(){
  	document.getElementById("ribbon_big").className = "ribbon_big_hover";
  	document.getElementById("ribbon_small").className = "ribbon_small_hover"; 	
  }
  function lowlightRibbon(){
  	document.getElementById("ribbon_big").className = "ribbon_big";
  	document.getElementById("ribbon_small").className = "ribbon_small"; 	  	
  }  
  
  function loadSwf(){

				var flashvars="";
				var isFlash = true;
				trgt = document.getElementById("lederbord");
				// Check to see if the version meets the requirements for playback
				//alert(swfobject.getFlashPlayerVersion().major);
				if (swfobject.getFlashPlayerVersion().major >= 9) {
					
					//pano = new SWFObject("panorama/KoradaPanorama01_out.swf", "main", "550", "400", "8", "#FFFFFF"); 
					
					swfobject.embedSWF ("panorama/KoradaPanorama01_out.swf", "lederbord" , "980", "331", "9", "",{externalinterface: "1"} , {bgcolor: "#ffeec1"},"", function callbackFn(e) {if(e.success){ pano = e.ref; checkSwfLoader()}} );
					
					
					//trgt.innerHTML ='<OBJECT classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,0,0" WIDTH="980" HEIGHT="331" id="pano" ALIGN=""><param name="wmode" value="transparent"><param name="FlashVars" value="externalinterface=1"><PARAM NAME=movie VALUE="panorama/KoradaPanorama01_out.swf"> <PARAM NAME=quality VALUE=high> <PARAM NAME=bgcolor VALUE=#ffeec1> <EMBED src="panorama/KoradaPanorama01_out.swf" quality=high bgcolor=#ffeec1 WIDTH="980" HEIGHT="331" NAME="pano" ALIGN="" TYPE="application/x-shockwave-flash" PLUGINSPAGE="http://www.macromedia.com/go/getflashplayer" VMODE="transparent" flashvars="externalinterface=1"></EMBED> </OBJECT>';
          //so.write("lederbord");
            				
				}else{
					loadGalerry();
				}
  }
  
  function checkSwfLoader(){
  	var isLoaded= false;
  	try{
  		isLoaded = pano.isComplete();
  	}catch(err){
  		//alert("error");
  	}
  	if(isLoaded){
  		//alert("checkSwfLoader");
  		loadGalerry();
  	}else{
   		swfLoaderTimer = setTimeout('checkSwfLoader()', 1000);
  	}
  }
  
  function loadGalerry(){
  	//alert("kurc");
  	//setTimeout(setGallery , 1000);
  	setGallery();
  }
  
 function setGallery(){ 
  	$('#galleryContainer').load(
  		"gallery.php",
  		function(response, status, xhr){
				//galery
				if(status == "success")
					$('#gallery').galleryView({
						panel_width: 980,
						panel_height: 430,
						panel_scale: 'nocrop',
						
						transition_speed: 800,				
						transition_interval: 15000,									
						pointer_size: 0,
						animate_pointer: false,
						frame_width: 80,
						frame_height: 60,
						frame_scale: 'crop',
						
						show_filmstrip_nav: false,
						frame_gap: 8,
						overlay_position_bottom_offset: 90,
						pause_on_hover: true,
						
						panel_animation: 'crossfade',
						
						frame_opacity: 1
				}); 
  		}  		
  	) 	
 	
 } 
  	
 function loadTooltips(){

    var style = 'easeOutExpo';

		$(document).bind('mousemove', function(e){
			  if(!e) e = window.event;
			  if(e.pageX) dst = e.pageX + 5
			  else dst = e.clientX + 5;
    		$('#tooltip').css({
       		left:  dst
    		});
		});
    
    $('.tooltipKmalu').hover(function () {
         
        //get the left pos  
        topofset = Math.round($(this).offset().top) + 30;

        $('#tooltip').css({top: topofset});  
        $('#tooltip').show(100);
    });
     
    $('.tooltipKmalu').mouseleave(function () {
        $('#tooltip').hide(200);      
    });  		
    
  		
  }
  	
