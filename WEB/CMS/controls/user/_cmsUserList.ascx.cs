﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;


namespace SM.UI.Controls
{
    public partial class _cmsUserList : BaseControl 
    {
        public delegate void OnEditEventHandler(object sender, SM.BLL.Common.Args .IDguidEventArgs e);
        public event OnEditEventHandler OnEdit;
        public event OnEditEventHandler OnDelete;
        public string OnEditTarget = "_self";
        public bool ShowCheckboxColumn { get; set; }
        public bool ShowManageColumn { get; set; }
        public bool ShowEditColumn { get; set; }
        public int PageSize = 20;
        public bool DeleteAutomatically = true;
        public bool MultipleSelect = true;

        public Guid? CategoryID { get; set; }
        public int TaskID { get; set; }
        public string SearchText { get; set; }
        public bool? Active { get; set; }
        public string RoleID { get; set; }
                public int ExcludeTaskID { get; set; }
        public Guid ExcludeUserID { get; set; }
        public bool IsAdmin { get; set; }
        public List<EM_USER> dsList;

        public List<Guid> GetSelectedItems {
            get { 
                List<Guid> list = new List<Guid>();
                if (!ShowCheckboxColumn ) return list;

                // get all selected groups
                foreach (ListViewItem item in lvList.Items ){
                    CheckBox cbSelectUser = (CheckBox)item.FindControl("cbSelectUser");
                    if (cbSelectUser == null) return list;

                    HiddenField hfUser = (HiddenField)item.FindControl("hfUser");
                    // add group to list
                    if (cbSelectUser.Checked)
                        list.Add(new Guid(hfUser.Value));
                }

                return list;
            }
        }


        
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        protected void ldsList_OnSelecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            eMenikDataContext db = new eMenikDataContext();

            List<EM_USER> list;

            if (dsList != null)
            {
                list = dsList;
            }
            else
            {

                list = (from l in db.EM_USERs where l.PORTAL_ID == (short ) SM.BLL.Common.Emenik.Data.PortalID()   select l).ToList();

                // apply filters
                //if (Active != null)
                //    list = list.Where(w => w.IsApproved == Active).ToList();

                //if (TaskID > 0)
                //    list = list.Join(db.USER_TASKs.Where(w => w.TaskID  == TaskID  ), g => g.UserId, p => p.UserId, (g, p) => g).ToList();

                //// exclude
                //if (ExcludeTaskID > 0)
                //    list = list.Where(l => !(from p in db.USER_TASKs where l.UserId == p.UserId select p.TaskID).Contains(ExcludeTaskID)).ToList();
                //if (ExcludeUserID != Guid.Empty)
                //    list = list.Where(l => l.UserId != ExcludeUserID).ToList();


                //if (!string.IsNullOrEmpty(RoleID))
                //    list = list.Where(w=>Roles.IsUserInRole(w.UserName, RoleID)).ToList();
            }

            // hide admin users for non admin users
            //if (!Page.User.IsInRole("admin"))
            //    list = list.Where(w => !Roles.IsUserInRole(w.USERNAME , "admin")).ToList();

            if (!String.IsNullOrEmpty(SearchText))
                list = list.Where(w => ((w.FIRSTNAME ?? "").Contains(SearchText) || (w.LASTNAME ?? "").Contains(SearchText) || w.EMAIL.Contains(SearchText) || w.USERNAME.Contains(SearchText) || w.PAGE_NAME.Contains(SearchText) || w.CODE.ToString("000000").Contains(SearchText))).ToList();



            // result
            e.Result = list.OrderByDescending(o => o.DATE_REG );
        }


        protected void lvList_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            if (e.CommandName == "Edit") {
                SM.BLL.Common.Args.IDguidEventArgs sbe = new SM.BLL.Common.Args.IDguidEventArgs();
                sbe.ID = new Guid(e.CommandArgument.ToString());
                // raise event
                OnEdit(this,sbe);
            }
            else if (e.CommandName == "login") {
                //MembershipUser user = Membership.GetUser(e.CommandArgument);
                Session["is_admin"] = true;
                SM.EM.Security.Login.LogIn(e.CommandArgument.ToString(), false, "", true);
                //FormsAuthentication.RedirectFromLoginPage(e.CommandArgument.ToString(), false);
        
            }

        }

        public void BindData()
        {
            lvList.DataBind();

            // init footer colspan
            HtmlTableCell footTH = (HtmlTableCell)lvList.FindControl("footTH");

            if (footTH == null) return; // no rows exist

            footTH.ColSpan = 11; //minimum cols
            if (ShowCheckboxColumn) footTH.ColSpan += 1;
            if (ShowManageColumn) footTH.ColSpan += 2;

            // init header
            HtmlTableCell headCB = (HtmlTableCell)lvList.FindControl("headCB");
            headCB.Visible = ShowCheckboxColumn;

            HtmlTableCell headManage = (HtmlTableCell)lvList.FindControl("headManage");
            headManage.Visible = ShowManageColumn;

            // init page size
            DataPager pager = (DataPager)lvList.FindControl("DataPager");
            pager.PageSize = PageSize;
        }




        protected void lvList_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem) return;

            if ( MultipleSelect || !ShowCheckboxColumn) return;

            CheckBox cbSelectUser = (CheckBox)e.Item.FindControl("cbSelectUser");
            string script = "SetUniqueCheckbox('lvList.*cbSelectUser',this)";
            cbSelectUser.Attributes.Add("onclick", script);
        }
        protected void lvList_DataBound(object sender, EventArgs e)
        {

            //// init footer colspan
            //HtmlTableCell footTH = (HtmlTableCell)lvList.FindControl("footTH");

            //if (footTH == null) return; // no rows exist

            //footTH.ColSpan = 6; //minimum cols
            //if (ShowCheckboxColumn) footTH.ColSpan += 1;
            //if (ShowManageColumn) footTH.ColSpan += 2;

            //// init header
            //HtmlTableCell headCB = (HtmlTableCell)lvList.FindControl("headCB");
            //headCB.Visible = ShowCheckboxColumn;

            //HtmlTableCell headManage = (HtmlTableCell)lvList.FindControl("headManage");
            //headManage.Visible = ShowManageColumn;

            //// init page size
            //DataPager pager = (DataPager)lvList.FindControl("DataPager");
            //pager.PageSize = PageSize;

        }
}


}