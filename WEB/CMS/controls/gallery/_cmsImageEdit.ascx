﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_cmsImageEdit.ascx.cs" Inherits="SM.EM.UI.CMS.Controls.CMS_cmsImageEdit" %>
<%@ Register Namespace="SM.UI.Controls" TagPrefix="SM" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="SM" TagName="PopupUserCrop" Src="~/_ctrl/image/popupUserCrop.ascx"  %>

<SM:InlineScript runat="server">
    <script type="text/javascript" language="javascript">
        function SetMaxImageWidth(img, w) {
            if (img.width > w)
                img.width = w;
        }
    </script>    
</SM:InlineScript>

<h3>edit image</h3>

<div >
    <asp:DropDownList ID="ddlTypes" runat="server" AutoPostBack="true" 
        onselectedindexchanged="ddlTypes_SelectedIndexChanged" >
    </asp:DropDownList>
    <br />

    <img id="imgMain" runat="server" />
    <asp:HiddenField ID="hfImgVirtualUrl" runat="server" />
    
    <div class="buttonwrappersim">
        <asp:LinkButton ID="btCrop" runat="server" CssClass="lbutton" onclick="btUserCrop_OnClick"><span>Popravi sliko</span></asp:LinkButton>
    </div>    
</div>

<div>
    <asp:CheckBox ID="cbActive" runat="server" Text="active" />
    <br />
    <br />
    author:
    <asp:TextBox ID="tbAuthor" runat="server"></asp:TextBox>
</div>

<div >
    <SM:smListView ID="lvList" DataKeyNames="IMG_ID,LANG_ID" runat="server"  InsertItemPosition="None" >
        <LayoutTemplate>
            <asp:PlaceHolder id="itemPlaceholder" runat="server"></asp:PlaceHolder>
        </LayoutTemplate>
        
        <ItemTemplate>
            <br />
            <asp:HiddenField ID="hfLang" Value='<%#  Eval("LANG_ID")%>' runat="server" />
               
            <strong> <%#  Eval("LANG_DESC")%></strong>
            <br />
            naziv: <asp:TextBox ID="tbTitle" runat="server" MaxLength="256" Text='<%#  Eval("IMG_TITLE")%>'></asp:TextBox>
            <br />
            opis: <asp:TextBox ID="tbDesc" runat="server" MaxLength="512" TextMode="MultiLine" Rows="2" Text='<%#  Eval("IMG_DESC")%>'></asp:TextBox>
        
        </ItemTemplate>
    </SM:smListView>
</div>

<br />

<SM:PopupUserCrop id="popupCrop" runat="server"></SM:PopupUserCrop>
