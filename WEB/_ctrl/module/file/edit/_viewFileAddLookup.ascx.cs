﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _ctrl_module_file_viewFileAddLookup : BaseControlView
{

    public object Data;
    public object[] Parameters;

    public Guid PageID { get; set; }
    public Guid ProductID { get; set; }
    public string LangID { get; set; }


    public int ParentID;
    public string TitleID;
    public string ValueID;


    public string OnSaveFunctionCall {get; set;}
    public string OnSaveFunctionBody = "";


    protected void Page_Load(object sender, EventArgs e)
    {


        LoadData();

       
    }


    public void LoadData()
    {



        if (Data == null)
            return;
        //Files = (IQueryable<FILE>)Data;

        if (Parameters != null)
        {

            if (Parameters.Length > 0 && Parameters[0] != null)
                PageID = (Guid)Parameters[0];
            if (Parameters.Length > 1 && Parameters[1] != null)
                ProductID = (Guid)Parameters[1];
            if (Parameters.Length > 2 && Parameters[2] != null)
                LangID = Parameters[2].ToString();

            if (Parameters.Length > 3 && Parameters[3] != null)
                OnSaveFunctionCall = Parameters[3].ToString();
        }
        // list
        lvFileAdd.DataSource = Data ;
        lvFileAdd.DataBind();


    }







}
