﻿<%@ Page Language="C#" MasterPageFile="~/CMS/CMS.master" AutoEventWireup="true" CodeFile="ManageModule_old.aspx.cs" Inherits="SM.EM.UI.CMS.ManageModule" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cph" Runat="Server">
    
    <asp:LinqDataSource ID="ldsModules" runat="server" 
        ContextTypeName="eMenikDataContext" EnableDelete="True" EnableInsert="True" 
        EnableUpdate="True" TableName="MODULEs" >
    </asp:LinqDataSource>
    
    <asp:DetailsView ID="dvModule" runat="server" AutoGenerateRows="False" 
        DataSourceID="ldsModules" DefaultMode="Insert" 
        oniteminserted="dvModule_ItemInserted" DataKeyNames="MOD_ID"         >
        <Fields>
            <asp:BoundField DataField="MOD_ID" HeaderText="MOD_ID" InsertVisible="False"
                ReadOnly="True" SortExpression="MOD_ID" />
            <asp:BoundField DataField="MOD_TITLE" HeaderText="MOD_TITLE" 
                SortExpression="MOD_TITLE" />
            <asp:BoundField DataField="MOD_DESC" HeaderText="MOD_DESC" 
                SortExpression="MOD_DESC" />
            <asp:BoundField DataField="CMS_ROLES" HeaderText="CMS_ROLES" 
                SortExpression="CMS_ROLES" />
            <asp:BoundField DataField="WWW_ROLES" HeaderText="WWW_ROLES" 
                SortExpression="WWW_ROLES" />
            <asp:BoundField DataField="MOD_SRC" HeaderText="MOD_SRC" 
                SortExpression="MOD_SRC" />
            <asp:BoundField DataField="MOD_ORDER" HeaderText="MOD_ORDER" 
                SortExpression="MOD_ORDER" />
            <asp:BoundField DataField="MOD_OBJ_ID" HeaderText="MOD_OBJ_ID" 
                SortExpression="MOD_OBJ_ID" />
            <asp:CheckBoxField DataField="EM_ACTIVE" HeaderText="EM_ACTIVE" 
                SortExpression="EM_ACTIVE" />
            <asp:CheckBoxField DataField="EM_FREE" HeaderText="EM_FREE" 
                SortExpression="EM_FREE" />
            <asp:BoundField DataField="EM_PRICE" HeaderText="EM_PRICE" 
                SortExpression="EM_PRICE" />
            <asp:BoundField DataField="EM_BUY_TYPE" HeaderText="EM_BUY_TYPE" 
                SortExpression="EM_BUY_TYPE" />
            <asp:BoundField DataField="EM_BUY_QUANTITY" HeaderText="EM_BUY_QUANTITY" 
                SortExpression="EM_BUY_QUANTITY" />
            <asp:BoundField DataField="EM_TITLE" HeaderText="EM_TITLE" 
                SortExpression="EM_TITLE" />
            <asp:BoundField DataField="EM_DESC" HeaderText="EM_DESC" 
                SortExpression="EM_DESC" />
            <asp:CommandField ShowEditButton="True" ShowInsertButton="True" />
        </Fields>
    </asp:DetailsView>
    
    <asp:GridView ID="gvModule" runat="server" AllowPaging="True" 
        AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="MOD_ID" 
        DataSourceID="ldsModules" onrowupdated="gvModule_RowUpdated" >
        <Columns>
            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
            <asp:BoundField DataField="MOD_ID" HeaderText="MOD_ID" ReadOnly="True" 
                SortExpression="MOD_ID" InsertVisible="False" />
            <asp:BoundField DataField="MOD_TITLE" HeaderText="MOD_TITLE" 
                SortExpression="MOD_TITLE" />
            <asp:BoundField DataField="MOD_DESC" HeaderText="MOD_DESC" 
                SortExpression="MOD_DESC" />
            <asp:BoundField DataField="CMS_ROLES" HeaderText="CMS_ROLES" 
                SortExpression="CMS_ROLES" />
            <asp:BoundField DataField="WWW_ROLES" HeaderText="WWW_ROLES" 
                SortExpression="WWW_ROLES" />
            <asp:BoundField DataField="MOD_SRC" HeaderText="MOD_SRC" 
                SortExpression="MOD_SRC" />
            <asp:BoundField DataField="MOD_ORDER" HeaderText="MOD_ORDER" 
                SortExpression="MOD_ORDER" />
            <asp:BoundField DataField="MOD_OBJ_ID" HeaderText="MOD_OBJ_ID" 
                SortExpression="MOD_OBJ_ID" />
            <asp:CheckBoxField DataField="EM_ACTIVE" HeaderText="EM_ACTIVE" 
                SortExpression="EM_ACTIVE" />
            <asp:CheckBoxField DataField="EM_FREE" HeaderText="EM_FREE" 
                SortExpression="EM_FREE" />
            <asp:BoundField DataField="EM_PRICE" HeaderText="EM_PRICE" 
                SortExpression="EM_PRICE" />
            <asp:BoundField DataField="EM_BUY_TYPE" HeaderText="EM_BUY_TYPE" 
                SortExpression="EM_BUY_TYPE" />
            <asp:BoundField DataField="EM_BUY_QUANTITY" HeaderText="EM_BUY_QUANTITY" 
                SortExpression="EM_BUY_QUANTITY" />
            <asp:BoundField DataField="EM_TITLE" HeaderText="EM_TITLE" 
                SortExpression="EM_TITLE" />
            <asp:BoundField  DataField="EM_DESC" HeaderText="EM_DESC" 
                SortExpression="EM_DESC" />
        </Columns>
    </asp:GridView>
</asp:Content>



