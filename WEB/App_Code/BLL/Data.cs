﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Data
/// </summary>
public class Data
{




    public class LangTitleDescActive
    {
        public string Lang { get; set; }
        public string Title { get; set; }
        public string Desc { get; set; }
        public bool Active { get; set; }
        public string MetaDesc { get; set; }
        public string Keywords { get; set; }
    }


    public class ArticleData {


        public ArticleData(ARTICLE a, string img_url, int? imgID, int? typeID, Guid? cwp_gal, string smlTitle, int catSmapID)
        {
            this.ART_ID = a.ART_ID;
            this.SMAP_ID = a.SMAP_ID;
            this.LANG_ID = a.LANG_ID;
            this.CWP_ID = a.CWP_ID;
            this.ADDED_BY = a.ADDED_BY;
            this.DATE_ADDED = a.DATE_ADDED;
            this.DATE_MODIFY = a.DATE_MODIFY;
            this.ART_TITLE = a.ART_TITLE;
            this.ART_ABSTRACT = a.ART_ABSTRACT;
            this.ART_BODY = a.ART_BODY;
            this.RELEASE_DATE = a.RELEASE_DATE;
            this.EXPIRE_DATE = a.EXPIRE_DATE;
            this.APPROVED = a.APPROVED;
            this.ACTIVE = a.ACTIVE;
            this.COMMENTS_ENABLED = a.COMMENTS_ENABLED;
            this.ART_GUID = a.ART_GUID;

            this.EXPOSED = a.EXPOSED;
            this.SortWeight = a.SortWeight;
            this.EMBEDD = a.EMBEDD;

            this.ART_IMAGE = img_url;
            this.IMG_ID = imgID;
            this.TYPE_ID = typeID;

            this.CWP_GAL = cwp_gal;

            this.CAT_SML_TITLE = smlTitle;
            this.CAT_SMAP_ID = catSmapID;


        }


        public ArticleData(ARTICLE a, string img_url, int? imgID, int? typeID, Guid? cwp_gal): this(a, img_url, imgID, typeID, cwp_gal, "", -1)
        {

        }

        public int ART_ID { get; set; }
        public System.Nullable<int> SMAP_ID { get; set; }
        public string LANG_ID { get; set; }
        public System.Nullable<System.Guid> CWP_ID { get; set; }
        public string ADDED_BY { get; set; }
        public System.DateTime DATE_ADDED { get; set; }
        public System.DateTime DATE_MODIFY { get; set; }
        public string ART_TITLE { get; set; }
        public string ART_ABSTRACT { get; set; }
        public string ART_BODY { get; set; }
        public System.Nullable<System.DateTime> RELEASE_DATE { get; set; }
        public System.Nullable<System.DateTime> EXPIRE_DATE { get; set; }
        public bool APPROVED { get; set; }
        public bool ACTIVE { get; set; }
        public bool COMMENTS_ENABLED { get; set; }
        public string ART_IMAGE { get; set; }
        public System.Guid ART_GUID { get; set; }

        public bool EXPOSED { get; set; }
        public int SortWeight { get; set; }
        
        public string EMBEDD { get; set; }
        

        public int? IMG_ID { get; set; }
        public int? TYPE_ID { get; set; }

        public Guid? CWP_GAL { get; set; }

        public string CAT_SML_TITLE { get; set; }
        public int CAT_SMAP_ID { get; set; }


        public bool IsArticleOwner()
        {
            bool ret = false;

            if (HttpContext.Current == null)
                return ret;
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
                return ret;

            if (HttpContext.Current.User.Identity.Name == this.ADDED_BY)
                return true;

            return ret;

        }

    }
}
