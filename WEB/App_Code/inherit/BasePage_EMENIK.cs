﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Net;
using SM.UI.Controls;
using System.Text;
using System.Collections.Generic;

/// <summary>
/// Summary description for BasePage
/// </summary>
namespace SM.EM.UI
{

    public class BasePage_EMENIK : BasePage 
    {
        public const string EditModeViewControlID = "ctl01$objStatusBar$btView";
        public const string EditModeCMSControlID = "ctl01$objStatusBar$btEdit";

        public string MetaDesc = "";

        Literal litErrorMessage = new Literal();
        Literal litStatus = new Literal();
        public BaseMasterPage_EMENIK mp_em;
        protected bool DisablePartialRendering = false;

        protected bool AutoRedirect = true;
        public bool IsContentPage = true;

        public bool AllwaysShowTitle = false;

        public List<int> RegSmpEmenik;
        public Dictionary<int, Emenik2SiteMapProvider> SmpEmenikList;
        protected Emenik2SiteMapProvider _currentSmpEmenik;

        public Emenik2SiteMapProvider CurrentSmpEmenik { get {
            // get sitemap with current node
            if (_currentSmpEmenik != null)
                return _currentSmpEmenik;

            if (SmpEmenikList == null)
                return null;
            if (SmpEmenikList.Count == 0)
                return null;
            foreach (KeyValuePair<int, Emenik2SiteMapProvider> item in SmpEmenikList)
            {
                if (item.Value.CurrentNode != null)
                {
                    _currentSmpEmenik = item.Value;
                    return _currentSmpEmenik;
                }
            }
            return null;
        } }

        public Emenik2SiteMapProvider SmpEmenik { get {

            // get sitemap with current node or first one from list
            if (_currentSmpEmenik != null)
                return _currentSmpEmenik;

            if (SmpEmenikList == null)
                return null;
            if (SmpEmenikList.Count == 0)
                return null;

            if (CurrentSmpEmenik != null)
                return CurrentSmpEmenik;

            // none is current, try default
            if (SmpEmenikList.ContainsKey(-1))
                return SmpEmenikList[-1];

            // return first available
            return SmpEmenikList.First().Value ;
        } }

        public bool IsParent(int smp, bool includeCurrent)
        {
            bool ret = false;

            if (CurrentSitemapNode == null)
                return ret;
            //current
            if (includeCurrent && CurrentSitemapID == smp)
                return true;

            SiteMapNode node = CurrentSitemapNode;
            int parentID = -1;
            while (node.ParentNode != null) { 
                node = node.ParentNode;

                if (!int.TryParse(node.Key, out parentID))
                    continue;
                
                // is parent
                if (parentID == smp)
                    return true;
            }


            return ret;
        }



        // emenik user data
        public EM_USER em_user
        {
            get {
                string pname = "";
                if (PageName != null) pname = PageName;
                return EM_USER.GetUserByPageName(pname );
            }
        }



        public SiteMapNode CurrentSitemapNode
        {
            get
            {

                if (CurrentSmpEmenik != null )
                    return CurrentSmpEmenik.CurrentNode;

                // retireve simtepa and create current node
                //SITEMAP_LANG sml = SITEMAP.GetSitemapLangByID(CurrentSitemapID, LANGUAGE.GetCurrentLang());
                return null ;
            }
        }

        public string CurrentPageTitle { get { 
            string ret= "default";
            if (CurrentSitemapNode != null)
                return CurrentSitemapNode.Title;

            if (CurrentSitemapID > 0){                 
                 if (CurrentSitemapLang != null)
                     return CurrentSitemapLang.SML_TITLE;
            }            
            return ret;
            } 
        }


        public string PageName{
            get{
                string _pageName = "";
                // first check for context
                if (Context.Items["pname"] != null) return Context.Items["pname"].ToString();

                if (Request.QueryString["pname"] == null) return _pageName;
                _pageName =  Request.QueryString["pname"].ToString();
                // remove duplicate entries
                _pageName = _pageName.Split(",".ToCharArray())[0];

                return _pageName;
            }
            set { EM_USER.SetPageName(value);}

        }

        public int PageModule
        {
            get
            {
                int _pMod = -1;
                // first check for context
                if (Context.Items["pmod"] != null) return int.Parse(Context.Items["pmod"].ToString());

                if (Request.QueryString["pmod"] == null) return _pMod;
                int.TryParse(Request.QueryString["pmod"].ToString(), out _pMod);

                return _pMod;
            }
            set { Context.Items["pmod"] = value; }

        }
        public string EmenikUserUrlFromMenu(string pname, int smapID)
        {
            if (CurrentSitemapNode == null || CurrentSmpEmenik == null)
                return SM.EM.Rewrite.EmenikUserUrl(pname, smapID, CurrentPageTitle );

            SiteMapNode node = CurrentSmpEmenik.FindSiteMapNodeFromKey(smapID.ToString());
            if (node == null)
                return SM.EM.Rewrite.EmenikUserUrl(pname, smapID, CurrentPageTitle );

            return SM.EM.Rewrite.EmenikUserUrl(pname, smapID, GetTitleFromSiteMap(node.Provider, node, "-", false, false));
        }



        public int SelectedStatusMenu {
            get
            {
                if (Session["SelStatMenu"] != null) return (int)(Session["SelStatMenu"]);
                if (IsEditMode) return SM.BLL.Common.EditModeStatus.CMS;
                else return SM.BLL.Common.EditModeStatus.View;
            }
            set { Session["SelStatMenu"] = value; }
        }

        public  bool IsEditMode
        {
             get
            {
                if (em_user == null) return false;
                if (em_user.IS_DEMO) return true;

                return PERMISSION.User.HasEditModePermission();



                //if (!User.Identity.IsAuthenticated) return false;
                //if (!(User.Identity.Name.Trim().ToLower() == em_user.USERNAME.ToLower() )) return false;

                //return true;
            }
        }
        public bool IsModePreview { get { return ( !IsEditMode || (SelectedStatusMenu == SM.BLL.Common.EditModeStatus.View ) ); } }
        public bool IsModeCMS { get { return IsEditMode && (SelectedStatusMenu == SM.BLL.Common.EditModeStatus.CMS);} }

        public bool CanSave
        {
            get
            {
                return PERMISSION.User.HasPermissionTypePAGE(em_user.UserId, PERMISSION.Common.Obj.PAGE, PERMISSION.Common.Action.Settings, PERMISSION.Common.Type.Allow);
                 
                //if (!User.Identity.IsAuthenticated) return false;
                //if (!(User.Identity.Name.Trim().ToLower() == em_user.USERNAME.ToLower())) return false;

                //return true;
            }
        }
        public bool HandleErrors( Exception e)
        {

            if (e != null)
            {
                if (typeof(ArgumentException).IsAssignableFrom(e.GetType()))
                {
                    SetError(e.Message);
                }
                else
                {
                    SetError(e.Message);
//                    litErrorMessage.Text = e.Message;// "An error occured. Please check data and try again.";
                }
                return true;

            }
            else
                return false;

        }

        protected void SetStatus(string sts)
        {
            BaseMasterPage mp = (BaseMasterPage)Page.Master;
            mp.StatusText = sts;
        }
        protected void SetError(string sts)
        {
            BaseMasterPage mp = (BaseMasterPage)Page.Master;
            mp.ErrorText = sts;
        }


        protected void SetPageName() {
            
            string current_host = Request.Url.Host.ToLower();
            int posSub = -1;
            posSub = current_host.IndexOf("." + SM.BLL.Common.Emenik.Data.PortalHostDomain());

            // if force
            if (!string.IsNullOrWhiteSpace(SM.BLL.Custom.ForceMainUserPageName))
            {
                PageName = SM.BLL.Custom.ForceMainUserPageName;
                return;
            }



            // first try QS
            if (!SM.BLL.Common.Emenik.Data.EnableSubDomainUsers()) {
                // get pagename from QS
                if (!string.IsNullOrEmpty(PageName))
                {
                    PageName = PageName;
                    return;
                }
                
            }


            // try subdomain
            if (SM.BLL.Common.Emenik.Data.EnableSubDomainUsers() && posSub > 0)
            {
                PageName = current_host.Substring(0, posSub);
                return;
            }

            // try custom domain
            else if (SM.BLL.Common.Emenik.Data.CustomDomainsEnabled()) {
                EM_USER _em_user = EM_USER.GetUserByCustomDomain(current_host);
                if (_em_user != null) {
                    PageName = _em_user.PAGE_NAME;
                    return;                
                }
                else if (!current_host.Contains(SM.BLL.Common.Emenik.Data.PortalHostDomain())){
                    // custom domain that user came from doesn't exist in DB
                    ErrorRedirect(81, 404);
                }           
            }
            // try QS
            else if (!SM.BLL.Common.Emenik.Data.EnableSubDomainUsers()) {
                // get pagename from QS
                if (string.IsNullOrEmpty(PageName))
                    ErrorRedirect(82, 404);
                PageName = PageName;
            }


        
        }

        protected override void OnPreInit(EventArgs e)
        {
            // init smp list
            SmpEmenikList = new Dictionary<int, Emenik2SiteMapProvider>();
            RegSmpEmenik = new List<int>();
             
            base.OnPreInit(e);
            AutoLoadMetaFromSitemap = false;

            // first get page name
            SetPageName();

            if (PageName == "") ErrorRedirect(3, 404);

            Trace.Write("OnPreInit 0");

            // check if domain is right
            //CheckDomain();


            Trace.Write("OnPreInit 1");
            // set language if not set
            if (!LANGUAGE.IsCultureSet())
            {
                CULTURE defCult = CULTURE.GetDefaultUserCulture(PageName);
                if (defCult != null)
                    LANGUAGE.SetCurrentCulture(defCult.UI_CULTURE);
            }
            else
                LANGUAGE.SetCurrentCulture(LANGUAGE.GetCurrentCulture());


            // redirect to first page if sitemapID not set 

            if (CurrentSitemapID < 1 && AutoRedirect){
                eMenikDataContext db = new eMenikDataContext();

                               

                // get default sitemapID
                var smap = (from s in db.SITEMAPs
                            from l in db.SITEMAP_LANGs 
                           from m in db.MENUs 
                           from em in db.EM_USERs 
                where em.USERNAME ==  m.USERNAME && em.PAGE_NAME  == PageName //&& pm.PMOD_ID == s.PMOD_ID
                && s.MENU_ID == m.MENU_ID 
                           && l.SMAP_ID == s.SMAP_ID && l.LANG_ID == LANGUAGE.GetCurrentLang()// && l.SML_ACTIVE == true 
                           && s.PARENT != -1
                           orderby m.MENU_ID,  l.SML_ORDER , s.SMAP_ID 
                           select new { s.SMAP_ID, s.PMOD_ID, l.SML_TITLE  }).FirstOrDefault();


                if (smap == null)
                {
                    // if page doesn't exist
                    if (EM_USER.GetUserByPageName(PageName) == null)
                        ErrorRedirect(31, 404);

                    // set error and redirect to step menu
                    if (IsEditMode && CurrentSitemapID < 1)
                        Response.Redirect(SM.EM.Rewrite.EmenikUserSettingsUrlByStep(em_user.PAGE_NAME, 3) + "&e=1");


                        // redirect to settings page
                    ForceSettings();

                }
                else {
                    // set canonical hint (instead of 301 redirect)
                    RelCanonical = SM.EM.Rewrite.EmenikUserUrl(em_user, smap.SMAP_ID, smap.SML_TITLE);

                    CurrentSitemapID = smap.SMAP_ID;
                    PageModule = smap.PMOD_ID;
                }

                
            }
            Trace.Write("OnPreInit 2");

            if (PageModule < 1 && IsContentPage )
            {
                // get page module
                PAGE_MODULE pmod = PAGE_MODULE.GetPageModuleBySitemap(CurrentSitemapID, em_user.UserId );
                // transer page to proper module if not set
                if (pmod == null)
                    ErrorRedirect(7, 404); // smap doesn't exist
                PageModule = pmod.PMOD_ID;
            }

            // add page name to context
            PageName = PageName;

            Trace.Write("OnPreInit 3");

            CheckPermissions = false;// todo: odstrani!!

            DefaultProvider = "";
            this.MasterPageFile = "~/_em/templates/fall/fall.master";
            this.Theme = "em_fall_bluelight";


            // if pagename doesn't exist - wrong path
            if (em_user == null) ErrorRedirect(1, 404);

            // if page is not yet active (the wizard is not finished), redirect to wizard
            if (!em_user.ACTIVE_USER)
                ForceSettings();

            if (!IsEditMode)
            {
                if (!HttpContext.Current.Request.IsLocal)
                {
                    if (!em_user.ACTIVE_WWW) ErrorRedirect(5, -1); // page is not active
                    if ((em_user.DATE_VALID == null && em_user.DATE_REG.Value.AddDays(SM.BLL.Common.Emenik.EMENIK_TEST_PERIOD_DAYS) < DateTime.Now.Date) || em_user.DATE_VALID < DateTime.Now)
                        ErrorRedirect(6, -1); // page is not active
                }
            }

            Trace.Write("OnPreInit 4");

            // init MASTER and THEME and TITLE
            if (em_user.MASTER_ID != null)
            {
                MASTER m = TEMPLATES.GetMasterByID(em_user.MASTER_ID.Value);
                if (m != null)
                {
                    this.MasterPageFile = m.Master;
//                    this.MasterPageFile = "~/_em/templates/fall/fall.master";
                    this.AllwaysShowTitle = m.ALLWAYS_SHOW_TITLE;
                }
            }
            Trace.Warn("master end");
            if (em_user.THEME_ID != null)
            {
                THEME t = TEMPLATES.GetThemeByID(em_user.THEME_ID.Value);
                this.Theme = t.Theme;
            }
            this.TitleRoot = em_user.PAGE_TITLE ?? PageName;

            this.mp_em = this.Master as BaseMasterPage_EMENIK;

            // add em_user to Current Context
            em_user.AddToContext();






            // init edit mode
            if (Request[PostBackEventTarget] != null)
            {
                string controlID = Request[PostBackEventTarget];

                if (controlID.Equals(EditModeViewControlID))
                {
                    SelectedStatusMenu = SM.BLL.Common.EditModeStatus.View;
                }
                else if (controlID.Equals(EditModeCMSControlID))
                    SelectedStatusMenu = SM.BLL.Common.EditModeStatus.CMS;
            }


            
        }

        protected override void OnInit(EventArgs e)
        {
            // init registered MENU SITEMAP providers
            foreach (int item in RegSmpEmenik)
            {
                if (!SmpEmenikList.ContainsKey(item))
                    SmpEmenikList[item] = new Emenik2SiteMapProvider(item);
            }
            if (SmpEmenikList == null || !SmpEmenikList.Any())
            {
                SmpEmenikList.Add(-1, new Emenik2SiteMapProvider(-1));
            }

            base.OnInit(e);

            // load proper content control
            LoadPageModControls();





            Helpers.RegisterScriptIncludeAJAX("http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js", this, "jQ");
            Helpers.RegisterScriptIncludeAJAX("http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.3/jquery-ui.min.js", this, "effects");
            Helpers.RegisterScriptIncludeAJAX("http://ajax.microsoft.com/ajax/jquery.validate/1.6/jquery.validate.pack.js", this, "validation");



            Helpers.RegisterScriptIncludeAJAX(SM.BLL.Common.Emenik.ResolveMainDomainUrl("~/JavaScript/jQuery/jquery.bgiframe.js"), this, "bgiframe");
            //Helpers.RegisterScriptIncludeAJAX(SM.BLL.Common.Emenik.ResolveMainDomainUrl("~/JavaScript/jQuery/jcarousellite_1.0.1.pack.js"), this, "carousel");
            //Helpers.RegisterScriptIncludeAJAX(ResolveUrl("~/_em/_common/javascript/jquery.pngFix.js"), this, "pngFix"); 
            // rating
            //Helpers.RegisterScriptIncludeAJAX(SM.BLL.Common.Emenik.ResolveMainDomainUrl("~/javascript/stars/ui.stars.min.js"), this, "stars", true);
            //SM.EM.Helpers.AddLinkedStyleSheet(this, SM.BLL.Common.Emenik.ResolveMainDomainUrl("~/javascript/stars/ui.stars.css"), true);
            // css for jquery UI
            SM.EM.Helpers.AddLinkedStyleSheet(this, "http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.3/themes/sunny/jquery-ui.css", false);


            // disable partial rendering for VIEWERS
            DisablePartialRendering = true;
            // tmp in eko project, move to EDIT MODE otherwise
            // upload
            SM.EM.Helpers.RegisterScriptIncludeAJAX(Page.ResolveUrl("~/javascript/ajaxupload.3.5.js"), this, "jQupload");
            
            if (IsEditMode)
            {
                DisablePartialRendering = false;

                // cropper
                Helpers.RegisterScriptIncludeAJAX(SM.BLL.Common.Emenik.ResolveMainDomainUrl("~/javascript/jQuery/jquery.Jcrop.pack.js"), this, "Crop2");
                // jHtmlarea
                Helpers.RegisterScriptIncludeAJAX(SM.BLL.Common.Emenik.ResolveMainDomainUrl("~/javascript/jHtmlArea/jHtmlArea-0.7.0.min.js"), this, "jHtmlArea", false);
                SM.EM.Helpers.AddLinkedStyleSheet(this, SM.BLL.Common.Emenik.ResolveMainDomainUrl("~/javascript/jHtmlArea/jHtmlArea.css"), false);

                // CKEDITOR
                Helpers.RegisterScriptIncludeAJAX(SM.BLL.Common.Emenik.ResolveMainDomainUrl("~/javascript/ckeditor/ckeditor.js"), this, "CKEditor", false);
                Helpers.RegisterScriptIncludeAJAX(SM.BLL.Common.Emenik.ResolveMainDomainUrl("~/javascript/ckeditor/adapters/jquery.js"), this, "CKEditorJQ", false);
                Helpers.RegisterScriptIncludeAJAX(SM.BLL.Common.Emenik.ResolveMainDomainUrl("~/javascript/ckeditor/dialog-patch.js"), this, "CKEditorDialogPatch", false);

                // FCK JQUERY
                Helpers.RegisterScriptIncludeAJAX(SM.BLL.Common.Emenik.ResolveMainDomainUrl("~/fckeditor/fckeditor.js"), this, "FCKEditor", false);
                Helpers.RegisterScriptIncludeAJAX(SM.BLL.Common.Emenik.ResolveMainDomainUrl("~/fckeditor/jquery.FCKEditor.pack.js"), this, "FCKEditorJQ", false);

                //edit
                Helpers.RegisterScriptIncludeAJAX(SM.BLL.Common.Emenik.ResolveMainDomainUrl("~/_ctrl/module/_edit/EditModule.js"), this, "3", true);

                // init edit mode
                Helpers.RegisterScriptIncludeAJAX(SM.BLL.Common.Emenik.ResolveMainDomainUrl("~/_em/_common/javascript/edit.js"), this, "edit.js", true);

                // date picker
                SM.EM.Helpers.RegisterScriptIncludeAJAX(Page.ResolveUrl("http://jquery-ui.googlecode.com/svn/trunk/ui/i18n/jquery.ui.datepicker-sl.js"), this, "jDatePickerLocale-si");
            

                // ready script for edit mode
                RegisterDocumentReadyScript += " ttip_init();";
            }




            Helpers.RegisterScriptIncludeAJAX(SM.BLL.Common.Emenik.ResolveMainDomainUrl("~/_em/_common/javascript/emenik_main.js"), this, "em_main", true);
            // ready script init MPL
            RegisterDocumentReadyScript += " mpl_init();";

            // disable partial rendering on emenik content pages
            if (DisablePartialRendering)
                ScriptManager.GetCurrent(this).EnablePartialRendering = false;

        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            // add description meta tag
            HtmlMeta description = new HtmlMeta();
            description.Name = "Description";

            string desc = MetaDesc;
            if (string.IsNullOrWhiteSpace(MetaDesc))
                desc = this.Title;

            description.Content = desc;
            Header.Controls.Add(description);
        }

        protected override void SetRelCanonical()
        {
            base.SetRelCanonical();

            // SEO rel = canonical
            if (string.IsNullOrEmpty(RelCanonical) && IsContentPage)
            {
                string can = GetDefaultCanonicalUrl();
                if (Request.Url.AbsoluteUri != can && !string.IsNullOrEmpty(can))
                    RelCanonical = can;
                //string _can = "";
                //if (CurrentSitemapNode != null)
                //    _can = CurrentSitemapNode.Url;
                //else
                //    _can = SM.EM.Rewrite.EmenikUserUrl(em_user, CurrentSitemapID, CurrentPageTitle);
                //if (Request.Url.AbsoluteUri != _can)
                //    RelCanonical = _can;

            }

        }

        public virtual string GetDefaultCanonicalUrl()
        {

                string _can = "";
                if (CurrentSitemapNode != null)
                    _can = CurrentSitemapNode.Url;
                else
                    _can = SM.EM.Rewrite.EmenikUserUrl(em_user, CurrentSitemapID, CurrentPageTitle);
            return _can;
        }



        protected override void OnPreRender(EventArgs e)
        {



            // register script
            RegisterClientScriptBlockStart  += " em_g_cult=" + Helpers.EncodeJsString(LANGUAGE.GetCurrentCulture()) + ";";
            RegisterClientScriptBlockStart += " em_g_lang=" + Helpers.EncodeJsString(LANGUAGE.GetCurrentLang()) + ";";
            RegisterClientScriptBlockStart += " em_g_pid=" + Helpers.EncodeJsString(em_user.UserId.ToString()) + ";";
            RegisterClientScriptBlockStart += " em_g_smp=" + CurrentSitemapID.ToString() + ";";
            RegisterClientScriptBlockStart += " em_g_fckbp=" +  Helpers.EncodeJsString( Page.ResolveUrl( SM.BLL.Common.Emenik.Data.FCKEditorBasePath() ))+  ";";



            // add webservices
            //ScriptManager sm = ScriptManager.GetCurrent(this);
            AjaxControlToolkit.ToolkitScriptManager sm = AjaxControlToolkit.ToolkitScriptManager.GetCurrent(this) as AjaxControlToolkit.ToolkitScriptManager;
            // combine scripts
            sm.CombineScripts = true;

            if (Rewrite.Helpers.IsPortalHostDomain())
                sm.CombineScriptsHandlerUrl = new Uri("~/_inc/CombineScriptsHandler.ashx", UriKind.Relative);
            else
                sm.CombineScriptsHandlerUrl = new Uri("http://" + SM.BLL.Common.Emenik.Data.PortalHostDomain() + "/_inc/CombineScriptsHandler.ashx");

            sm.ScriptMode = ScriptMode.Release;
            sm.LoadScriptsBeforeUI = false;






            // composite scripts
            ServiceReference srP = new ServiceReference("~/_inc/webservice/WebServiceP.asmx");
            srP.InlineScript = true; // todo : fix caching and set to false
            sm.Services.Add(srP);

            // eko:
            sm.Services.Add(new ServiceReference("~/_inc/webservice/WebServiceCWP.asmx"));

            if (IsEditMode)
            {
                sm.Services.Add(new ServiceReference("~/_inc/webservice/WebServiceEMENIK.asmx"));
                sm.Services.Add(new ServiceReference("~/_inc/webservice/WebServiceC.asmx"));
            }


            base.OnPreRender(e);


            if (em_user.CUSTOM_CSS_RULES != null && !String.IsNullOrEmpty(em_user.CUSTOM_CSS_RULES))
            {
                // add custom CSS scripts
                HtmlGenericControl css = new HtmlGenericControl();
                css.TagName = "style";
                css.Attributes["type"] = "text/css";
                css.InnerHtml = em_user.CUSTOM_CSS_RULES;
                Page.Header.Controls.Add(css);
            }





        }



        protected void CheckDomain() { 
            // test:
            //Helpers.IsDomainActive("beta.emenik.si");

            // if subdomains are enabled, user can not come by portal domain -  page doesn't exist
            if (SM.EM.Rewrite.Helpers .IsPortalHostDomain() && SM.BLL.Common.Emenik.Data.EnableSubDomainUsers () )
                ErrorRedirect(35, 404);

            if (!SM.BLL.Common.Emenik.Data.CustomDomainsEnabled())
                return;

            if (em_user == null)
                return;
            
            // get host domain
            string host = Request.Url.Host.ToLower();

            // user came from own domain - all is OK
            if (!string.IsNullOrEmpty(em_user.CUSTOM_DOMAIN) && em_user.CUSTOM_DOMAIN.Contains(host))
            {
                if (!em_user.CUSTOM_DOMAIN_ACTIVATED)
                    EM_USER.UpdateDomainActive(em_user.UserId, true);

                return;
            }

            // subdomains not enabled
            if (!SM.BLL.Common.Emenik.Data.EnableSubDomainUsers()) { 
                // if user does not have own domain, ok
                return;
            
            }


            // user came from portal domain or localhost
            if (host.Contains(SM.BLL.Common.Emenik.Data.PortalHostDomain()) || host.Contains("localhost"))
            {
                // if doesn't have own domain - OK
                if (string.IsNullOrEmpty(em_user.CUSTOM_DOMAIN))
                    return;

                    // domain is not activated  - OK
                else if (!em_user.CUSTOM_DOMAIN_ACTIVATED && !Helpers.IsDomainActive(em_user.CUSTOM_DOMAIN))
                {
                    return;
                }
                // domain is active - redirect to own domain
                else
                {
                    if (!em_user.CUSTOM_DOMAIN_ACTIVATED)
                        EM_USER.UpdateDomainActive(em_user.UserId, true);

                    // redirect to user domain
                    RedirectToOwnDomain(em_user.CUSTOM_DOMAIN);
                }
            }
            else {
                // user came from different/wrong portal - page doesn't exist
                ErrorRedirect(32, 404);
            
            }
        }

        protected void RedirectToOwnDomain(string domain){

            string url = "http://" + domain;
            string path = "";

            if (CurrentSitemapID > 0){
                path = Rewrite.EmenikUserUrl(PageName, CurrentSitemapID).TrimStart("~".ToCharArray());
            }

            Response.Redirect(url + path);
        }







        protected void ForceSettings() {
            // if user is not authenticated
            if (!IsEditMode)
                ErrorRedirect(4, 404); // page does not exist


            string tmp;
            tmp = Request.ServerVariables["PATH_INFO"];

            // redirect to settings if not on settings or help page
            if (!( tmp.ToLower().Contains ("/settings/") || tmp.ToLower().Contains ("/help/")) )

                if (em_user.STEPS_DONE < 1)
                    Response.Redirect(SM.EM.Rewrite.EmenikUserHelpUrlStep0(em_user.PAGE_NAME));
                else
                    Response.Redirect(SM.EM.Rewrite.EmenikUserSettingsUrl(em_user.PAGE_NAME ) );
        }

        public virtual  void LoadPageModControls() { 
        
             
        }

        protected override void LoadTitle()
        {


            if (CurrentSitemapNode != null)
                Page.Title = GetTitleFromSiteMap(CurrentSitemapNode);
            else
                Page.Title = CurrentPageTitle + " | " + TitleRoot;
        }

        // render page title
        public string RenderPageTitle()
        {
            if (AllwaysShowTitle)
                return RenderPageTitleLink();

            return "";
        }

        // render page title for themes with allways visible TITLE
        public string RenderPageTitleMain() {
            if (AllwaysShowTitle && em_user.SHOW_TEXT)
                return RenderPageTitleLink();
            
            return "";         
        }
        // render page title only if TITLE is NOT allways visible and LOGO is not set
        public string RenderPageTitleLogo()
        {
            if (em_user.SHOW_LOGO || AllwaysShowTitle || !em_user.SHOW_TEXT) return "";
            
            return RenderPageTitleLink();
        }
        public string RenderPageTitleLink() {
                return "<div class=\"cLogoText\"><h1><a href=\"" + Page.ResolveUrl(Rewrite.EmenikUserUrl(PageName)) + "\" >" + em_user.PAGE_TITLE + "</a></h1></div>";
        }

        public string RenderGaCode()
        {
            string ret = SM.EM.Analyitcs.CodeUser(em_user.GA_TRACKER ?? "" );
            return ret;
        }

        public string RenderEditLinkStep(int step) {

            if (!IsEditMode) return "";
            if (IsModePreview) return "";

            //if (em_user.STEPS_DONE < 8) return "";
            if (!em_user.ACTIVE_USER) return "";

            string ret = "";

            string tekst = "";
            string addClass = "";

            string href = "";
            string onclick = "";
            int settStep = step;

            switch (step  )
            {
                case 1:
                    tekst = "uredi obliko";
                    addClass = " cEditStepDesign";
                    break;
                case 2:
                    tekst = "uredi jezike";
                    addClass = " cEditStepLang";
                    break;
                case 3:
                    tekst = "uredi menije";
                    addClass = " cEditStepMenu";
                    break;
                case 4:
                    tekst = "uredi glavo";
                    addClass = " cEditStepHeader";
                    break;
                case 6:
                    tekst = "uredi nogo";
                    addClass = " cEditStepFooter";
                    break;
                case 8:
                    tekst = "uredi postavitev strani";
                    href = "#";
                    onclick = "editMenuLoad(" + CurrentSitemapID + "); return false";
                    addClass = " cEditStepMenu";
                    break;

            }

            href = Page.ResolveUrl(SM.EM.Rewrite.EmenikUserSettingsUrlByStep(PageName, settStep));

            if (!string.IsNullOrEmpty(tekst))
                ret = string.Format("<div class=\"cEditStep {2}\"><a class=\"editStep\" href=\"{0}\" onclick=\"{3}\" ><span>{1}</span></a></div>", href, tekst, addClass, onclick);


            //href = Page.ResolveUrl(SM.EM.Rewrite.EmenikUserSettingsUrlByStep(PageName, step));
//            ret = string.Format( "<div class=\"cEditStep {2}\"><a class=\"editStep\" href=\"{0}\" ><span>{1}</span></a></div>", href, tekst, addClass );

            return ret;        
        }


        public  void RequestCustomerLogin()
        {
            this.Response.Redirect(SM.EM.Rewrite.EmenikCustomerLogin(em_user.UserId)+ "?" + GetReturnUrlWithQS());
        }

        public void RequestCustomerLoginLanding()
        {
            RequestCustomerLogin(); //changed
            //this.Response.Redirect(Page.ResolveUrl(SM.BLL.Custom.Url.LoginLanding) + "?" + GetReturnUrlWithQS());
        }


        protected void OpenPopupOnStart() {
            if (IsPostBack) return;

//            string popupID = Request.Url.
        
        
        }

        public virtual string RenderBreadCrumbs(string separator) {
            return RenderBreadCrumbs(CurrentSmpEmenik, separator, true);
        }

        public virtual string RenderBreadCrumbs(SiteMapProvider provider, string separator, bool selectCurrent)
        {

            string ret = "";

            if (provider == null)
                return ret;
            if (provider.CurrentNode == null)
                return ret;


            StringBuilder sbResult = new StringBuilder();

            SiteMapNode node = provider.CurrentNode;
            string _output = "";
            string _sep = "";
            string _curclass = "";
            if (selectCurrent)
                _curclass = "class=\"sel\"";
            while (node.ParentNode != null)
            {
                _output = "<a " + _curclass + " href=\"" + Page.ResolveUrl(node.Url) + "\">" + node.Title + "</a>" + _sep + _output;

                node = node.ParentNode;
                _sep = separator;
                _curclass = "";
            }

            sbResult.Append(_output);

            ret = sbResult.ToString();
            return ret;
        }


        # region Footer
        public string RenderContactName(string suffix)
        {
            if (string.IsNullOrEmpty(em_user.CONT_NAME)) return "";
            if (!em_user.SHOW_NAME) return "";
            return Server.HtmlDecode(em_user.CONT_NAME).Replace("\r", "<br/>") + suffix;
        }

        public string RenderContactAddress(string suffix)
        {
            return RenderContactAddress(suffix, ", ");
        }

        public string RenderContactAddress(string suffix, string sep)
        {
            string ret = "";
            if (!em_user.SHOW_ADDRESS) return "";
            if (!string.IsNullOrEmpty(em_user.CONT_STREET)) ret += sep + em_user.CONT_STREET;
            if (!string.IsNullOrEmpty(em_user.CONT_POSTAL_CODE)) ret += sep + em_user.CONT_POSTAL_CODE;
            if (!string.IsNullOrEmpty(em_user.CONT_CITY)) ret += " " + em_user.CONT_CITY;

            return ret.TrimStart(sep.ToCharArray()) + suffix;
        }
        public string RenderContactEmail(string preffix, string suffix)
        {
            if (string.IsNullOrEmpty(em_user.CONT_EMAIL)) return "";
            if (!em_user.SHOW_EMAIL) return "";
            return preffix + "<a href=\"mailto:" + em_user.CONT_EMAIL + "\">" +  em_user.CONT_EMAIL + "</a>" + suffix;
        }
        public string RenderContactPhone(string preffix, string suffix)
        {
            if (string.IsNullOrEmpty(em_user.CONT_PHONE)) return "";
            if (!em_user.SHOW_PHONE) return "";
            return preffix + em_user.CONT_PHONE + suffix;
        }
        public string RenderContactFax(string preffix, string suffix)
        {
            if (string.IsNullOrEmpty(em_user.CONT_FAX)) return "";
            if (!em_user.SHOW_FAX) return "";
            return preffix + em_user.CONT_FAX + suffix;
        }
        public string RenderContactMobile(string preffix, string suffix)
        {
            if (string.IsNullOrEmpty(em_user.CONT_MOBILE)) return "";
            if (!em_user.SHOW_MOBILE) return "";
            return preffix + em_user.CONT_MOBILE + suffix;
        }

        #endregion

   }
}