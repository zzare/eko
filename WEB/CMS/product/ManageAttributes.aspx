﻿<%@ Page Language="C#" MasterPageFile="~/CMS/CMS.master" AutoEventWireup="true" CodeFile="ManageAttributes.aspx.cs" Inherits="SM.UI.CMS.EM.CMS_ManageProductAttributes" Title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Namespace="SM.UI.Controls" TagPrefix="SM" %>

<asp:Content ID="Content3" ContentPlaceHolderID="cph" Runat="Server">


<%--            <asp:LinqDataSource ID="ldsImageType" runat="server" ContextTypeName="eMenikDataContext" TableName="IMAGE_TYPEs">
            </asp:LinqDataSource>--%>

            <asp:LinqDataSource ID="ldsList" runat="server" 
                ContextTypeName="eMenikDataContext" EnableDelete="True" EnableInsert="True" 
                EnableUpdate="True" TableName="GROUP_ATTRIBUTEs" 
                OnSelecting="ldsList_OnSelecting" oninserting="ldsList_Inserting" 
                onupdating="ldsList_Updating">
            </asp:LinqDataSource>
            
            <SM:smListView ID="lvList" runat="server" DataSourceID="ldsList" 
                DataKeyNames="GroupAttID" InsertItemPosition="LastItem" 
                onitemcanceling="lvList_ItemCanceling" onitemediting="lvList_ItemEditing" 
                onitemupdated="lvList_ItemUpdated" onitemdatabound="lvList_ItemDataBound" >
                <LayoutTemplate>
                    <table  class="tblData">
                            <thead>
                                <tr id="headerSort" runat="server">
                                    <th ><asp:LinkButton ID="LinkButton1" CommandName="sort" CommandArgument="GroupAttID" runat="server">ID</asp:LinkButton></th>
                                    <th ><asp:LinkButton ID="LinkButton3" CommandName="sort" CommandArgument="EM_USER.PAGE_NAME" runat="server">Page</asp:LinkButton></th>
                                    <th ><asp:LinkButton ID="LinkButton4" CommandName="sort" CommandArgument="Title" runat="server">Title</asp:LinkButton></th>
                                    <th ><asp:LinkButton ID="LinkButton2" CommandName="sort" CommandArgument="Desc" runat="server">Desc</asp:LinkButton></th>
                                    <th width="50px"><asp:LinkButton ID="LinkButton5" CommandName="sort" CommandArgument="OrderByField" runat="server">Order field</asp:LinkButton></th>
                                    <th colspan="2">Manage</th>
                                </tr>
                            </thead>
                            <tbody id="itemPlaceholder" runat="server"></tbody>
                            <tfoot>
                                <tr>
                                    <th style="text-align:right" colspan="7">
                                        <asp:DataPager runat="server" ID="DataPager" PageSize="25">
                                            <Fields>
                                                <asp:NextPreviousPagerField  />
                                                <asp:NumericPagerField ButtonCount="5" />
                                            </Fields>
                                        </asp:DataPager>
                                    </th>
                                </tr>
                            </tfoot>
                        </table>
                </LayoutTemplate>
                
                <ItemTemplate>
                    <tr  class='<%#  Container.DataItemIndex % 2 == 0 ? " " : " odd " %>'  onmouseover ="this.className='<%#  Container.DataItemIndex % 2 == 0 ? "hovRow" : "hovRowodd" %>'" onmouseout ="this.className = GetCssClass(<%#  Container.DataItemIndex %>)">
                        <td>
                            <%# Eval("GroupAttID")%>
                        </td>
                        <td>
                            <%# Eval("EM_USER.PAGE_NAME")%>
                        </td>
                        <td>
                            <%# Eval("Title")%>
                        </td>
                        <td>
                            <%# Eval("Desc") ?? "" %>
                        </td>
                        <td >
                            <asp:LinkButton ID="lbEdit" CommandName="Edit" runat="server">edit</asp:LinkButton>
                        </td>
                        <td>
                            <asp:LinkButton ID="lbDelete" OnClientClick="javascript: return confirm('Are you sure you want to delete this item?');" CommandName="Delete" runat="server">delete</asp:LinkButton>
                        </td>                
                    </tr>        
                
                </ItemTemplate>
                
                <InsertItemTemplate>
                    <tr class="insertRow">
                        <td colspan="2">
                            <asp:TextBox MaxLength="256" Width="99%" ID="tbPageID" runat="server" Text='<%# Bind("PageID") %>'></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="256" Width="99%" ID="tbTitle" runat="server" Text='<%# Bind("Title") %>'></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="512" Width="99%" ID="tbDesc" runat="server" Text='<%# Bind("Desc") %>'></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="512" Width="99%" ID="TextBox1" runat="server" Text='<%# Bind("OrderByField") %>'></asp:TextBox>
                        </td>
                        <td colspan="2">
                            <asp:Button ID="btSave" CommandName="Insert" runat="server" Text="Add new" ValidationGroup="insert" ></asp:Button>
                        </td>
                    </tr>     
                    <tr class="errorRow">
                        <td colspan="2">
                            
                        </td>
                        <td colspan="2">
                            <asp:RequiredFieldValidator ValidationGroup="insert" ID="RequiredFieldValidator2" ControlToValidate="tbTitle" Display="Dynamic"  runat="server" ErrorMessage="* Enter title"></asp:RequiredFieldValidator>
                        </td>
                        <td colspan="3">
                        </td>
                    </tr>   
                    <%--<tr class="insertRow">
                        <td colspan="2"> 
                            <asp:DropDownList ID="ddlLogo" Width="99%" runat="server"   AppendDataBoundItems="true"  DataSourceID="ldsImageType" DataTextField="TYPE_TITLE" DataValueField="TYPE_ID">
                                <asp:ListItem Text=" - Select Logo type - " Value="" ></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td colspan="2"> 
                            <asp:DropDownList ID="ddlHeader" Width="99%" runat="server"   AppendDataBoundItems="true"  DataSourceID="ldsImageType" DataTextField="TYPE_TITLE" DataValueField="TYPE_ID">
                                <asp:ListItem Text=" - Select Header type - " Value="-1" ></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td colspan="6">
                        </td>
                    </tr>   
                    <tr class="errorRow">
                        <td colspan="2">
                            <asp:RequiredFieldValidator ValidationGroup="insert" ID="rvLogo" ControlToValidate="ddlLogo" Display="Dynamic"  runat="server" ErrorMessage="* Choose logo type"></asp:RequiredFieldValidator>
                        </td>
                        <td colspan="8">
                        </td>
                    </tr>            --%>             
                </InsertItemTemplate>
                
                <EditItemTemplate>
                <asp:HiddenField ID="hfID" runat="server" Value = '<%# Eval("GroupAttID") %>' />
                    <tr class="editRow">
                       <td colspan="2">
                            <asp:TextBox MaxLength="256" Width="99%" ID="tbPageID" runat="server" Text='<%# Bind("PageID") %>'></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="256" Width="99%" ID="tbTitle" runat="server" Text='<%# Bind("Title") %>'></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="512" Width="99%" ID="tbDesc" runat="server" Text='<%# Bind("Desc") %>'></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="512" Width="99%" ID="TextBox1" runat="server" Text='<%# Bind("OrderByField") %>'></asp:TextBox>
                        </td>
                        <td >
                            <asp:Button ID="btSave" CommandName="Update" runat="server" Text="Save" ValidationGroup="edit" ></asp:Button>
                        </td>
                        <td >
                            <asp:Button ID="btCancel" CommandName="Cancel" runat="server" Text="Cancel" ></asp:Button>
                        </td>                
                    </tr>     
                    <tr class="errorRow">
                        <td colspan="2">                            
                        </td>
                        <td colspan="2">
                            <asp:RequiredFieldValidator ValidationGroup="edit" ID="RequiredFieldValidator1" ControlToValidate="tbTitle" Display="Dynamic"  runat="server" ErrorMessage="* Enter title"></asp:RequiredFieldValidator>
                        </td>
                        <td colspan="3">
                        </td>                    
                    
                    </tr>  
                    <tr class="editRow">
                    
                    
                                
                                
                        <SM:smListView ID="lvListAttr" runat="server"  
                            DataKeyNames="AttributeID" InsertItemPosition="LastItem" 
                            onitemcanceling="lvListAttr_ItemCanceling" onitemediting="lvListAttr_ItemEditing" 
                             OnItemInserting ="lvListAttr_ItemInserting"
                            OnItemDeleting="lvListAttr_ItemDeleting" OnItemUpdating ="lvListAttr_ItemUpdating" >
                            <LayoutTemplate>
                                <table  class="tblData">
                                        <thead>
                                            <tr id="headerSort" runat="server">
                                            <th>-></th>
                                                <%--<th width="50px"><asp:LinkButton ID="LinkButton1" CommandName="sort" CommandArgument="AttributeID" runat="server">ID</asp:LinkButton></th>
                                                --%><th ><asp:LinkButton ID="LinkButton4" CommandName="sort" CommandArgument="Title" runat="server">Title</asp:LinkButton></th>
                                                <th ><asp:LinkButton ID="LinkButton2" CommandName="sort" CommandArgument="Desc" runat="server">Desc</asp:LinkButton></th>
                                                <th width="50px"><asp:LinkButton ID="LinkButton5" CommandName="sort" CommandArgument="Code" runat="server">Code</asp:LinkButton></th>
                                                <th ><asp:LinkButton ID="LinkButton6" CommandName="sort" CommandArgument="UrlSmall" runat="server">UrlSmall</asp:LinkButton></th>
                                                <th ><asp:LinkButton ID="LinkButton7" CommandName="sort" CommandArgument="UrlBig" runat="server">UrlBig</asp:LinkButton></th>
                                                <th width="50px"><asp:LinkButton ID="LinkButton8" CommandName="sort" CommandArgument="Ordr" runat="server">Order</asp:LinkButton></th>
                                                <th colspan="2">Manage</th>
                                            </tr>
                                        </thead>
                                        <tbody id="itemPlaceholder" runat="server"></tbody>

                                    </table>
                            </LayoutTemplate>
                            
                            <ItemTemplate>
                                <tr  class='<%#  Container.DataItemIndex % 2 == 0 ? " " : " odd " %>'  onmouseover ="this.className='<%#  Container.DataItemIndex % 2 == 0 ? "hovRow" : "hovRowodd" %>'" onmouseout ="this.className = GetCssClass(<%#  Container.DataItemIndex %>)">
                                <td> </td>
                                    <%--<td>
                                        <%# Eval("AttributeID")%>
                                    </td>--%>
                                    <td>
                                        <%# Eval("Title")%>
                                    </td>
                                    <td>
                                        <%# Eval("Desc") ?? "" %>
                                    </td>
                                    <td>
                                        <%# Eval("Code") ?? "" %>
                                    </td>                        
                                    <td>
                                        <%# Eval("UrlSmall") ?? ""%>
                                    </td>                        
                                    <td>
                                        <%# Eval("UrlBig") ?? ""%>
                                    </td> 
                                    <td>
                                        <%# Eval("Ordr") ?? ""%>
                                    </td>                                                            
                                    <td >
                                        <asp:LinkButton ID="lbEdit" CommandName="Edit" runat="server">edit</asp:LinkButton>
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="lbDelete" OnClientClick="javascript: return confirm('Are you sure you want to delete this item?');" CommandName="Delete" runat="server">delete</asp:LinkButton>
                                    </td>                
                                </tr>        
                            
                            </ItemTemplate>
                            
                            <InsertItemTemplate>
                                <tr class="insertRow">
                                    <td></td>
                                    <td>
                                        <asp:TextBox MaxLength="256" Width="99%" ID="tbTitle" runat="server" Text='<%# Bind("Title") %>'></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox MaxLength="512" Width="99%" ID="tbDesc" runat="server" Text='<%# Bind("Desc") %>'></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox MaxLength="512" Width="99%" ID="tbCode" runat="server" Text='<%# Bind("Code") %>'></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox MaxLength="512" Width="99%" ID="tbUrlSmall" runat="server" Text='<%# Bind("UrlSmall") %>'></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox MaxLength="512" Width="99%" ID="tbUrlBig" runat="server" Text='<%# Bind("UrlBig") %>'></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox MaxLength="512" Width="99%" ID="tbOrdr" runat="server" Text='<%# Bind("Ordr") %>'></asp:TextBox>
                                    </td>                                    
                                    <td colspan="2">
                                        <asp:Button ID="btSave" CommandName="Insert" runat="server" Text="Add new" ValidationGroup="insert" ></asp:Button>
                                    </td>
                                </tr>     
                                <tr class="errorRow">
                                    <td colspan="2">
                                        
                                    </td>
                                    <td colspan="2">
                                        <asp:RequiredFieldValidator ValidationGroup="insert" ID="RequiredFieldValidator2" ControlToValidate="tbTitle" Display="Dynamic"  runat="server" ErrorMessage="* Enter title"></asp:RequiredFieldValidator>
                                    </td>
                                    <td colspan="3">
                                    </td>
                                </tr>   
            
                            </InsertItemTemplate>  
                            
                            <EditItemTemplate>
                            
                                <tr class="editRow">
                                    <td><asp:HiddenField ID="hfID" runat="server" Value='<%# Eval("AttributeID") %>' /></td>
                                    <td>
                                        <asp:TextBox MaxLength="256" Width="99%" ID="tbTitle" runat="server" Text='<%# Bind("Title") %>'></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox MaxLength="512" Width="99%" ID="tbDesc" runat="server" Text='<%# Bind("Desc") %>'></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox MaxLength="512" Width="99%" ID="tbCode" runat="server" Text='<%# Bind("Code") %>'></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox MaxLength="512" Width="99%" ID="tbUrlSmall" runat="server" Text='<%# Bind("UrlSmall") %>'></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox MaxLength="512" Width="99%" ID="tbUrlBig" runat="server" Text='<%# Bind("UrlBig") %>'></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox MaxLength="512" Width="99%" ID="tbOrdr" runat="server" Text='<%# Bind("Ordr") %>'></asp:TextBox>
                                    </td>                                    

                                    <td >
                                        <asp:Button ID="Button1" CommandName="Update" runat="server" Text="Save" ValidationGroup="edit" ></asp:Button>
                                    </td>
                                    <td >
                                        <asp:Button ID="btCancel" CommandName="Cancel" runat="server" Text="Cancel" ></asp:Button>
                                    </td> 
                                </tr>     
                                <tr class="errorRow">
                                    <td colspan="2">
                                        
                                    </td>
                                    <td colspan="2">
                                        <asp:RequiredFieldValidator ValidationGroup="edit" ID="RequiredFieldValidator2" ControlToValidate="tbTitle" Display="Dynamic"  runat="server" ErrorMessage="* Enter title"></asp:RequiredFieldValidator>
                                    </td>
                                    <td colspan="3">
                                    </td>
                                </tr>
                            
                            
                            
                            
                            </EditItemTemplate>              
                            
                            </SM:smListView>                    
                                
                                
                    
                        
                    </tr>   
                    <%--<tr class="errorRow">
                        <td colspan="2">
                            <asp:RequiredFieldValidator ValidationGroup="edit" ID="rvLogo" ControlToValidate="ddlLogo" Display="Dynamic"  runat="server" ErrorMessage="* Choose logo type"></asp:RequiredFieldValidator>
                        </td>
                        <td colspan="8">
                        </td>
                    </tr>   --%>                    
                </EditItemTemplate>
            
            </SM:smListView>


</asp:Content>


