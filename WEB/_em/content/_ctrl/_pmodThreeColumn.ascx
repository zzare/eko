﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_pmodThreeColumn.ascx.cs" Inherits="_em_content__ctrl_pmodThreeColumn" %>
<%@ Register Namespace="SM.EM.UI.Controls" TagPrefix="EM"  %>
<%@ Register TagPrefix="EM" TagName="ModuleNew" Src="~/_ctrl/emenik/emModuleNew.ascx"  %>

    <div class="cont_wideW">
        <div class="cont_wideT"></div>
        <div class="cont_wideM">
            <div class="cont_wideMI">


    <div id="contThreeColsWrapper" class="clearfix" > 
		<div id="contThreeColsTwoCols" class="clearfix"> 
			<div id="contThreeColsMain" >            
                <EM:ModuleNew ID="mnMain" runat="server" />

                <EM:CwpZone ID="wpz_1" runat="server">
                </EM:CwpZone>

            </div>

			<div id="contThreeColsRight" >

                <EM:ModuleNew ID="mnRight" runat="server" />

			    <EM:CwpZone ID="wpz_2" runat="server">
                </EM:CwpZone>
			</div>
		</div> 
		<div id="contThreeColsLeft" >

                <EM:ModuleNew ID="mnLeft" runat="server" />

				<EM:CwpZone ID="wpz_3" runat="server">
                </EM:CwpZone>
		</div>
    </div>
            </div>    
        </div>
        <div class="cont_wideB"></div>
    </div>
