﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace SM.UI.Controls
{
    public partial class _cmsNewsletterFilter : BaseControl 
    {
        public event EventHandler OnFilterChanged;

        public string SearchText { get { return tbSearch.Text.Trim(); } }

        public short? Status {
            get {

                if (ddlStatus.SelectedValue == "")
                    return null;

                return short.Parse(ddlStatus.SelectedValue);

            }
        
        }


        protected void Page_Load(object sender, EventArgs e)
        {


            if (!IsPostBack)
                LoadDefaults();
        }

        public void LoadDefaults() {
            ddlStatus.Items.Add(new ListItem("NOT SENT", NEWSLETTER.Common.Status.NOT_SENT.ToString()));
            ddlStatus.Items.Add(new ListItem("SENT", NEWSLETTER.Common.Status.SENT.ToString()));
        
        
        }

        protected void ddlActive_SelectedIndexChanged(object sender, EventArgs e)
        {
            OnFilterChanged(this, new EventArgs());
        }


        protected void btSearch_Click(object sender, EventArgs e)
        {
            OnFilterChanged(this, new EventArgs());
        }
}
}