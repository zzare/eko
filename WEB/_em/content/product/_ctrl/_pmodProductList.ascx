﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_pmodProductList.ascx.cs" Inherits="_em_content_product__ctrl__pmodProductList" %>
<%@ Register Namespace="SM.EM.UI.Controls" TagPrefix="EM"  %>
<%@ Register TagPrefix="EM" TagName="ModuleNew" Src="~/_ctrl/emenik/emModuleNew.ascx"  %>
<%@ Register TagPrefix="SM" TagName="ProductList" Src="~/_ctrl/module/product/_viewProductList.ascx"  %>
<%@ Register TagPrefix="SM" TagName="ScriptProduct" Src="~/_ctrl/module/product/edit/_cntScript.ascx"  %>
<%@ Register TagPrefix="EM" TagName="SubMenu" Src="~/_em/templates/_custom/eko/_ctrl/_ctrlSubMenu.ascx"  %>
<%@ Register TagPrefix="EM" TagName="FilterManufacturer" Src="~/_em/content/product/_ctrl/_filtProductManufacturer.ascx"  %>

<%--

<script type= "text/javascript">

    $(document).ready(function() {
        $("input.pageJump").keypress(function(e) {
            if (e.which == 13) {
                var ru = '<%= RenderSortPageFullUrl(GetCurrentOrder(), 2, false, "") %>';
                if ($(this).val() != '') {
                    ru = ru.replace('page=2', 'page=' + $(this).val());
                    window.location.href = ru;
                }
                return false;
            }
        });
    });
    
</script>
--%>

<%--<div class="mainL">
        
    <EM:SubMenu ID="objSubMenu" runat="server" />
    <EM:FilterManufacturer ID="objFiltManufact" runat="server" />
    
    <EM:CwpZone ID="wpz_2" runat="server">
    </EM:CwpZone>    
                       
</div>
--%>
<%--<div class="mainR">
--%>
    <SM:ScriptProduct ID="objScriptProduct" runat="server" />




    <div class="productlist">
    
        <EM:ModuleNew ID="mnMain" runat="server" />

        <EM:CwpZone ID="wpz_1" runat="server">    
                  
            <asp:PlaceHolder id="placeholder" runat="server"></asp:PlaceHolder>
                            
        </EM:CwpZone>


        

    <% if (ParentPage.IsModeCMS && EnableNew ){ %>
        <div class="moduleNew" >
            <div class="moduleNewLeft">&nbsp;</div>
            <div style="float:left; ">
                <a class="btAddwthText" href="#" onclick="newProductLoad(<%= CurrentSitemapID %>); return false" title="dodaj nov izdelek"  ><span>dodaj NOV izdelek</span></a>
            
            </div>
            <%--<div style="float:left;">
                <a href="javascript:void(0)" class="btsettStatusHelp btcwpHeadHelp"><img style="margin-top:8px;" src='<%=Page.ResolveUrl("~/_inc/images/icon/help.png") %>' /></a>
                <div class="settStatusWrapp cwpHeadHelpWrapp" >
                    <div class="settStatus cwpHeadHelp">
                        - Dodaj NOV izdelek v oddelek.
                        <br />
                    </div>
                </div>
            </div>   --%> 
            <div class="moduleNewRight">&nbsp;</div>
            <div class="clearing"></div>
        </div>
               
    <%} %>
<%--        <div class="pagsortW">
            <div class="pagsortT">
                <div class="pagsortTI">
                    <div class="prodnum" style="float:left; width:242px;">&nbsp;</div>
                    <div class="paging" style="float:left;">
                        <span class="pagsort_blue">STRAN:</span>
                        <a <%= RenderPageFullUrlHREF( 1) %> class="pagesort_fbw"></a>
                        <a <%= RenderPageFullUrlHREF( GetCurrentPage()-1) %> class="pagesort_bw"></a>
                        <span style="float:left;">
                        <input class="pageJump" type="text" value='<%= GetCurrentPage()  %>'  /></span>
                        <span style="float:left; color:#333;">/ <%= TotalPages %></span>
                        <a <%= RenderPageFullUrlHREF( GetCurrentPage()+1) %> class="pagesort_fwd"></a>
                        <a <%= RenderPageFullUrlHREF( TotalPages ) %> class="pagesort_ffwd"></a>
                    </div>
                    <div class="clearing"></div>
                </div>
            </div>
            <div class="pagsortM">
            </div>
            <div class="clearing"></div>
            <div class="pagsortB"></div>
        </div>--%>

       <div class="clearing"></div>

<%--       <div class="pagesortW">
       <div class="pagesort">
        <div id ="divSorting" runat="server"  enableviewstate="false" class="sorting" style="float:left;margin-left:10px;">
            <span class="pagsort_blue">SORTIRAJ:</span>

            <a  class="sortprice <%= Pager.RenderSelClass(2) %>" href='<%= Pager.RenderSortFullUrl(2) %>' >CENA</a>
            <a class="sortname <%= Pager.RenderSelClass(1) %>" href='<%= Pager.RenderSortFullUrl(1) %>' >NAZIV</a>
        </div>
        <div class="clearing"></div>
       </div>
       </div>--%>


            

    <SM:ProductList ID="objProductList" runat="server" />
    <div class="clearing"></div>



    <div class="pagesortW">
        <div class="pagesort">
            <a <%= Pager.RenderPageFullUrlHREF( 1) %> class="pagesort_fbw">&laquo;</a>
            <a <%= Pager.RenderPageFullUrlHREF( Pager.GetCurrentPage()-1) %> class="pagesort_bw">&lsaquo;</a>
                                                
            <%= Pager.RenderPageLinks(-1) %> 
            <a <%= Pager.RenderPageFullUrlHREF( Pager.GetCurrentPage()+1) %> class="pagesort_fwd">&rsaquo;</a>
            <a <%= Pager.RenderPageFullUrlHREF( Pager.TotalPages) %> class="pagesort_ffwd">&raquo;</a>
            <div class="clearing"></div>
        </div>
    </div>  



<%--        <div class="pagsortW">
            <div class="pagsortT">
                <div class="pagsortTI">
                    <div class="prodnum" style="float:left; width:242px;">&nbsp;</div>
                    <div class="paging" style="float:left;">
                        <span class="pagsort_blue">STRAN:</span>
                        <a <%= RenderPageFullUrlHREF( 1) %> class="pagesort_fbw"></a>
                        <a <%= RenderPageFullUrlHREF( GetCurrentPage()-1) %> class="pagesort_bw"></a>
                        <span style="float:left;">
                        <input class="pageJump" type="text" value='<%= GetCurrentPage()  %>'  /></span>
                        <span style="float:left;">/ <%= TotalPages %></span>
                        <a <%= RenderPageFullUrlHREF( GetCurrentPage()+1) %> class="pagesort_fwd"></a>
                        <a <%= RenderPageFullUrlHREF( TotalPages) %> class="pagesort_ffwd"></a>
                    </div>
                    <div class="clearing"></div>
                </div>
            </div>
            <div class="pagsortM">
            </div>
            <div class="clearing"></div>
            <div class="pagsortB"></div>
        </div>--%>
<%--    </div>
--%>

</div>            
