﻿<%@ Page Language="C#" MasterPageFile="~/CMS/CMS.master" AutoEventWireup="true" CodeFile="ManageMenu.aspx.cs" Inherits="SM.EM.UI.CMS.ManageMenu" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cph" Runat="Server">

    <asp:DetailsView ID="dvMenu" runat="server" AutoGenerateRows="False" 
        DataKeyNames="MENU_ID" DataSourceID="lds_Menu" DefaultMode="Insert" 
        oniteminserted="dvMenu_ItemInserted" >
        <Fields>
            <asp:BoundField DataField="MENU_ID" HeaderText="MENU_ID" InsertVisible="False" 
                ReadOnly="True" SortExpression="MENU_ID" />
            <asp:BoundField DataField="MENU_TITLE" HeaderText="MENU_TITLE" 
                SortExpression="MENU_TITLE" />
            <asp:BoundField DataField="USERNAME" HeaderText="USERNAME" 
                SortExpression="USERNAME" />
            <asp:CheckBoxField DataField="MENU_ACTIVE" HeaderText="MENU_ACTIVE" 
                SortExpression="MENU_ACTIVE" />
            <asp:CheckBoxField DataField="MENU_CMS" HeaderText="MENU_CMS" 
                SortExpression="MENU_CMS" />
            <asp:CommandField ShowInsertButton="True" />
        </Fields>
    </asp:DetailsView>
    
    <asp:GridView ID="gvMenu" runat="server"  DataKeyNames="MENU_ID"
        AllowPaging="True" 
        AllowSorting="True" AutoGenerateColumns="False" DataSourceID="lds_Menu" 
        onrowupdated="gvMenu_RowUpdated">
        <Columns>
            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
            <asp:BoundField DataField="MENU_ID" HeaderText="MENU_ID" InsertVisible="False" 
                ReadOnly="True" SortExpression="MENU_ID" />
            <asp:BoundField DataField="MENU_TITLE" HeaderText="MENU_TITLE" 
                SortExpression="MENU_TITLE" />
            <asp:BoundField DataField="USERNAME" HeaderText="USERNAME" 
                SortExpression="USERNAME" />
            <asp:CheckBoxField DataField="MENU_ACTIVE" HeaderText="MENU_ACTIVE" 
                SortExpression="MENU_ACTIVE" />
            <asp:CheckBoxField DataField="MENU_CMS" HeaderText="MENU_CMS" 
                SortExpression="MENU_CMS" />
        </Columns>
    </asp:GridView>
    <asp:LinqDataSource ID="lds_Menu" runat="server" 
        ContextTypeName="eMenikDataContext" EnableDelete="True" EnableInsert="True" 
        EnableUpdate="True" TableName="MENUs" >
    </asp:LinqDataSource>
    
</asp:Content>

