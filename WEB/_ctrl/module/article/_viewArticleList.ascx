﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewArticleList.ascx.cs" Inherits="_ctrl_module_product_viewArticleList" %>

<%--<script type="text/javascript" >



    $(function () {
        $('.btAddBlog').click(function (e) {
            e.preventDefault();

            newBlogLoad('<%= CwpID.ToString() %>', -1);
        });


    });
</script>--%>





<div id="divArticles" runat="server" >
    <asp:ListView ID="lvList" runat="server" >
        <LayoutTemplate>
            
                <asp:PlaceHolder ID="itemPlaceholder" runat="server"/>

        </LayoutTemplate>
        <ItemTemplate>
            <div class="artBox <%# RenderArtExposedClass(Container.DataItem) %> <%# RenderArtNotApprovedClass(Container.DataItem) %>">
                <div class="artBoxI">
                    <h2 class="articleH2"><a href='<%#  RenderArtHref(Container.DataItem) %>'><%#  RenderArtTitle(Container.DataItem) %></a></h2>
                    <span class="artBox_date"><%#  RenderAdditionalInfo(Container.DataItem)%></span>
                    <div runat="server"  visible='<%# (bool)( ShowImage && HasImage(  Container.DataItem)) %>' >
<% if (IsModeCMS) {%>
                        <a style="position:absolute;" class="divGalImgThumb_btnEdit" href="#" onclick="editCustomCrop('<%# Eval("IMG_ID") %>', '<%# Eval("TYPE_ID") %>'); return false" ></a>
<%} %>
                        <a class="AartBox_img" href='<%#  RenderArtHref(Container.DataItem) %>' title='<%#  RenderArtTitle(Container.DataItem) %>'>
                            <img src='<%# SM.BLL.Common.ResolveUrl ( Eval("ART_IMAGE").ToString() )  %>' alt='<%#  RenderArtTitle(Container.DataItem) %>' />
                        </a>
                    </div>   
                    
                    <div id="Div1" class="artBox_video" runat="server"  visible='<%# (bool)( HasVideo(  Container.DataItem)) %>' >
                        <%# RenderVideo(Container.DataItem)%>
                    </div>                    
                    
                                     

<asp:PlaceHolder runat="server"  visible='<%# (bool)( IsModeCMS || IsArticleOwner(  Container.DataItem)) %>' >

<%--                    <a  class="btEditArticle" href="#" onclick="editArticleLoad('<%= CwpID.ToString() %>', <%# Eval("ART_ID") %>); return false" ></a>--%>
                    <a  class="btEditArticle" href="#" onclick="newBlogLoad('<%= CwpID.ToString() %>', <%# Eval("ART_ID") %>, <%# CurrentSitemapID %> ); return false" ></a>
</asp:PlaceHolder>

                                    
                    <div class="artBoxR">
                    <p><%#  RenderArtDesc(Container.DataItem) %></p>
                    <a class="art_more" href='<%#  RenderArtHref(Container.DataItem) %>'><%=GetGlobalResourceObject("Modules", "ReadMore")%>&nbsp;&rsaquo;</a>
                    </div>
                    <div class="clearing"></div>
                </div>
                
                
                
                <div class="clearing"></div>
<% if (ShowFooter){%>                
                <div class="artFoot" >
                    <div id="artFootWrapper" class="clearfix" > 
                        <div id="artFootTwoCols" class="clearfix"> 
                            <div id="artFootMain" >            
                                <%# RenderComments(Container.DataItem ) %>
                            </div>
                            <div id="artFootRight" >
                                <%# RenderDatePublished(Container.DataItem ) %>
                            </div>
                        </div> 
                        <div id="artFootLeft" >
                            <%# RenderAuthor(Container.DataItem ) %>
                        </div>
                    </div>
                </div>
<%} %>                
            </div>
            
        </ItemTemplate>

        <ItemSeparatorTemplate>
            <span class="art_sepa"></span>
        </ItemSeparatorTemplate>
    </asp:ListView>
    
</div>

<asp:PlaceHolder ID="phEmpty" runat="server" Visible="false">
    Seznam je prazen
</asp:PlaceHolder>



<asp:PlaceHolder ID="phPager" runat="server" >
    <div class="pagesortW">
        <div class="pagesort">
            <a <%= Pager.RenderPageFullUrlHREF( 1) %> class="pagesort_fbw">&nbsp;</a>
            <a <%= Pager.RenderPageFullUrlHREF( Pager.GetCurrentPage()-1) %> class="pagesort_bw">&nbsp;</a>
            
            <%= Pager.RenderPageLinks(-1) %> 
            <a <%= Pager.RenderPageFullUrlHREF( Pager.GetCurrentPage()+1) %> class="pagesort_fwd">&nbsp;</a>
            <a <%= Pager.RenderPageFullUrlHREF( Pager.TotalPages) %> class="pagesort_ffwd">&nbsp;</a>
            <div class="clearing"></div>
        </div>
    </div> 
</asp:PlaceHolder>


<%--<% if (ShowAddNewBlog) {%>
<span class="art_sepa"></span>
<a  onclick ="newBlogLoad('<%= CwpID.ToString() %>', -1); return false" title="dodaj" href="#" class="btn_send"><span class="font_bevan">Dodaj nov blog zapis</span></a>
<%} %>--%>