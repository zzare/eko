﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
namespace SM.UI.CMS
{
    public partial class CMS_Intranet_ManageUserToExpire : SM.EM.UI.CMS.BasePage_CMS 
    {
        protected Guid ProjectID { 
            get {
                if (Request.QueryString["p"] == null)
                    return Guid.Empty;
                if (!SM.EM.Helpers.IsGUID(Request.QueryString["p"]))
                    return Guid.Empty;

                return new Guid(Request.QueryString["p"]);
            } 
        }
        protected Guid SiteID
        {
            get
            {
                if (Request.QueryString["s"] == null)
                    return Guid.Empty;
                if (!SM.EM.Helpers.IsGUID(Request.QueryString["s"]))
                    return Guid.Empty;

                return new Guid(Request.QueryString["s"]);
            }
        }
        protected Guid GroupID
        {
            get
            {
                if (Request.QueryString["g"] == null)
                    return Guid.Empty;
                if (!SM.EM.Helpers.IsGUID(Request.QueryString["g"]))
                    return Guid.Empty;

                return new Guid(Request.QueryString["g"]);
            }
        }
        protected Guid UserID
        {
            get
            {
                if (Request.QueryString["u"] == null)
                    return Guid.Empty;
                if (!SM.EM.Helpers.IsGUID(Request.QueryString["u"]))
                    return Guid.Empty;

                return new Guid(Request.QueryString["u"]);
            }
        }

        protected bool IsAdmin { get {

            return true; // todo: remove
            if (User.Identity.IsAuthenticated)
                if (User.IsInRole("admin"))
                    return true;
            return false;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            // event handlers
            objUserEdit.OnUserChanged += new EventHandler(objUserEdit_OnUserChanged);
            objUserEdit.OnCancel += new EventHandler(objUserEdit_OnCancel);
            objFilter.OnFilterChanged += new EventHandler(objFilter_OnFilterChanged);
            objUserList.OnDelete += new SM.UI.Controls._cmsUserList.OnEditEventHandler(objUserList_OnDelete);
            objUserList.OnEdit += new SM.UI.Controls._cmsUserList.OnEditEventHandler(objUserList_OnEdit);
            objPaymentEdit.OnChanged += new EventHandler(objPaymentEdit_OnChanged);
            
            // init
            objUserList.IsAdmin = IsAdmin;

            ApplyFilters();

            if (!IsPostBack)
            {
                // if user is set as QS, show edit user
                if (UserID != Guid.Empty)
                {
                    LoadData();

                }
                    //// if project is set, insert new site
                    //else if (ProjectID != Guid.Empty)
                    //{

                    //    objUserEdit.UID = Guid.Empty;
                    //    objUserEdit.BindData();
                    //    cpeQList.Collapsed = true;
                    //    objUserList .BindData();

                    //    ShowTabs();

                    //    // hide some tabs

                    //}
                    else // normal load
                {
                    HideTabs();
                    cpeQList.Collapsed = false;
                    LoadList();
                }

            }
        }


        protected void LoadData() {

            objUserEdit.UID = UserID;
            objUserEdit.BindData();
            cpeQList.Collapsed = true;
            cpeQList.ClientState = "true";
            objPayment.UserId = objUserEdit.UID;

            LoadList();

            objPayment.BindData();


            objPaymentEdit.UserID = objUserEdit.UID;
            objPaymentEdit.BindData();

            ShowTabs();
        }

        void objUserEdit_OnCancel(object sender, EventArgs e)
        {
            HideTabs();

            cpeQList.Collapsed = false;
            cpeQList.ClientState = "false";
            
        }


        protected void ApplyFilters() {
            // list
            objUserList.SearchText = objFilter.SearchText;
            objUserList.Active = objFilter.Active;
            //objUserList.RoleID = objFilter.Role;

            objUserEdit .ProjectID = ProjectID;
            objUserEdit .SiteID  = SiteID;

            objPayment.UserId = objUserEdit.UID;
            objPaymentEdit.UserID = objUserEdit.UID;

            // item
            //objProjectEdit.CategoryID = objProjectFilter.CategoryID;
        
        }
        // filter
        void objFilter_OnFilterChanged(object sender, EventArgs e)
        {
            // filters
            ApplyFilters();

            // refresh question lists
            LoadList();


            objPayment.BindData();

        }
        //edit
        void objUserEdit_OnUserChanged(object sender, EventArgs e)
        {
            // refresh user lists
            ApplyFilters();

            // refresh user lists
            LoadList();

            // refresh edit
            objUserEdit.BindData();


        }

        //list
        void objUserList_OnEdit(object sender, SM.BLL.Common.Args.IDguidEventArgs e)
        {
            // show tabs
            ShowTabs();


            // apply filters
            ApplyFilters();


            // load selected question
            //objSiteEdit.SID = e.ClickedID;
            //objSiteEdit.BindData();

        }
        void objUserList_OnDelete(object sender, SM.BLL.Common.Args.IDguidEventArgs e)
        {
            ApplyFilters();

            // refresh group lists
            LoadList();

        }

        void objPaymentEdit_OnChanged(object sender, EventArgs e)
        {
            objPayment.BindData();

            objPaymentEdit.PaymentID = -1;
            objPaymentEdit.BindData();

        }



        protected void ShowTabs()
        {
            tabUser.Visible = true;
            tabUser.ActiveTabIndex = 0;

        }
        protected void HideTabs()
        {
            tabUser.Visible = false;

        }

        protected void LoadList() {

            objUserList.dsList = EM_USER.GetUsersToNotifyToExpire(SM.BLL.ScheduleTask.Emenik.DAYS_BEFORE_EXPIRE  );
            objUserList.BindData();
        }


    }
}