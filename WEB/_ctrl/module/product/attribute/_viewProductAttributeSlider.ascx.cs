﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _ctrl_module_product_attribute_viewProductAttributeSlider : BaseControlView
{

    public object Data;
    public object[] Parameters;
    public Guid ProductID;
    public ProductRep.Data.GroupAttributeLang GroupAttribute;
    public List<ProductRep.Data.wAttrubute> ProductAttributeList;
    public Guid? SelectedAttribute;

    public ProductRep.Data.wAttrubute Attr
    {
        get
        {
            ProductRep.Data.wAttrubute ret = new ProductRep.Data.wAttrubute { ValMin=0, ValMax = 9999, GroupAttID = GroupAttribute.GroupAttID, AttributeID = Guid.Empty  };
            if (ProductAttributeList == null)
                return ret;
            if (ProductAttributeList.Count == 0)
                return ret;
            return ProductAttributeList[0];
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        LoadData();
    }



    public void LoadData()
    {
        this.Visible = true;



        if (Data == null)
        {
            this.Visible = false;
            return;
        }
        ProductID  = (Guid )Data;

        if (Parameters != null)
        {
            if (Parameters.Length > 0 &&   Parameters[0] != null)
                IsModeCMS = bool.Parse(Parameters[0].ToString());
            if (Parameters.Length > 1 && Parameters[1] != null)
                GroupAttribute = (ProductRep.Data.GroupAttributeLang)Parameters[1];
            if (Parameters.Length > 2 && Parameters[2] != null)
                ProductAttributeList = (List<ProductRep.Data.wAttrubute>)Parameters[2];
            if (Parameters.Length > 3 && Parameters[3] != null)
                SelectedAttribute  = (Guid?)(Parameters[3]);

        }
        // hide if not set
        if (GroupAttribute == null || ProductAttributeList == null)
        {
            this.Visible = false;
            return;
        }

        // bind data
        //lvList.DataSource = ProductAttributeList;
        //lvList.DataBind();

        //if (lvList.Items.Count == 0 && !IsModeCMS )
        //{
        //    this.Visible = false;
        //    return;
        //}

        if (Attr == null && !IsModeCMS )
        {
            this.Visible = false;
            return;
        }

    }

    protected string RenderMin()
    {
        string ret = "";
        if (Attr == null )
            return ret;

        if (Attr.IsDisabled)
            ret = (Attr.GetMin - 1).ToString();
        else
            ret = Attr.GetMin.ToString();

        return "min:" + ret ;
    }
    protected string RenderMax()
    {
        string ret = "";
        if (Attr == null)
            return ret;

        if (Attr.IsDisabled)
            ret = (Attr.GetMax + 1) .ToString();
        else
            ret = Attr.GetMax.ToString();
        
        return "max:" + ret ;
    }

    protected string RenderDefault()
    {
        string ret = "";
        if (Attr == null)
            return ret;

        if (Attr.IsDisabled)// min == max
            ret = (Attr.GetMin).ToString();
        else
            ret = Attr.GetDefault.ToString();


        return "value:" + ret ;
    }



}
