﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewFileAddLookup.ascx.cs" Inherits="_ctrl_module_file_viewFileAddLookup" %>

<script language = "javascript" type="text/javascript">
    function onSaveFileLookup() {
        var _selItems = $('#lkpFileAdd input:checkbox:checked').parent();
        <%= OnSaveFunctionCall %>
        
    }


</script>



<div class="modPopup_header modPopup_headerEdit">
    <h4>Dodaj datoteko</h4>
    <p>Izberi obstoječo datoteko</p>
</div>
<div class="modPopup_main">
    <div id="lkpFileAdd" class="lkpProductSmap">
        <ul   >
            <asp:ListView ID="lvFileAdd" runat="server" >
                <LayoutTemplate>
                    <asp:PlaceHolder ID="itemPlaceHolder" runat="server"></asp:PlaceHolder>
                </LayoutTemplate>
                             
                <ItemTemplate >
                   <li id='<%# Eval("FileID") %>' >
                    <label>
                        <input type="checkbox" value='<%# Eval("FileID") %>' />
                        &nbsp;&nbsp;<span><%# Eval("Name") %></span> [<%# SM.EM.Helpers.FormatDateLong( Eval("DateAdded") )%>]
                    </label>
                    </li>            
                </ItemTemplate>
    
            </asp:ListView>

        </ul>
    </div>

</div>   

<div class="modPopup_footer"> 
    <div class="buttonwrapperModal">
        <a class="lbutton lbuttonConfirm" href="#" onclick="onSaveFileLookup(); return false"><span>Dodaj VSE IZBRANE</span></a>
        <a class="lbutton lbuttonDelete" href="#" onclick="closeModalLoader(); return false" ><span>Prekliči</span></a>
    </div>
</div>
