﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Net.Mail;

namespace SM.CMS.UI.Controls
{

    public partial class _ctrl_ContactForm : BaseControl_EMENIK 
    {

        public Guid CwpID = Guid.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void btSend_Click(object sender, EventArgs e)
        {
            Page.Validate("contForm");
            if (!Page.IsValid) return;

            CWP_CONTACT_FORM cf = CMS_WEB_PART.GetCwpContactFormByID(CwpID);
            string mailTo = ParentPage.em_user.EMAIL ;
            string subject = "Spletno povpraševanje " + SM.BLL.Common.Emenik.Data.PortalName();
            string mailFromName = ParentPage.em_user.PAGE_NAME;

            if (cf != null) {
                if (cf.MAIL_TO != null && ! string.IsNullOrEmpty(cf.MAIL_TO ))
                    mailTo = cf.MAIL_TO;
                if (cf.SUBJECT != null && !string.IsNullOrEmpty(cf.SUBJECT))
                    subject   = cf.SUBJECT;
            
            }

            // send mail
            MailMessage message = new MailMessage();
            message.From = new MailAddress(tbEmail.Text, mailFromName);
            message.ReplyTo = new MailAddress(tbEmail.Text);
            message.To.Add(new MailAddress(mailTo ));


            message.Subject = subject  + ":" + tbSubject.Text;
            message.Body = "Zadeva: " + tbSubject.Text + "<br/>" + "Ime: " + tbName.Text + "<br/>" + "Email: " + tbEmail.Text + "<br/>" + tbBody.Text;
            message.IsBodyHtml = true;

            SmtpClient client = new SmtpClient();


            try
            {
                client.EnableSsl = true;
                client.Send(message);
                mvContact.SetActiveView(vSent);
            }
            catch (Exception ex)
            {
                ERROR_LOG.LogError(ex, "ContactForm send email");
            }

        }

    }
}