﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _ctrl_language_ctrlLanguageDdl : BaseControl_EMENIK
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }


    public void LoadLanguages()
    {
        // hide if not enabled
        if (!ParentPage.em_user.IS_MULTILINGUAL)
        {
            HideLangs();
            return;
        }

        lvList.DataSource = CULTURE.GetUserCultures(ParentPage.em_user.UserId);
        lvList.DataBind();
        ShowLangs();

    }

    protected void ShowLangs()
    {
        phVisible.Visible = true;
        phHidden.Visible = false;

    }
    protected void HideLangs()
    {
        phVisible.Visible = false;
        phHidden.Visible = false;

    }

    public string RenderDllLang(object o){

        vw_UserCulture c = (vw_UserCulture)o;

        string ret = "";

        if (LANGUAGE.GetCurrentCulture() == c.UI_CULTURE)
            return ret;

        ret = string.Format("<a class='lang-{2}' href='{0}'><span>{1}</span></a>", Page.ResolveUrl(SM.EM.Rewrite.EmenikUserUrl(ParentPage.PageName, c.UI_CULTURE )), c.DESC, c.UI_CULTURE.ToLower()  );


        return ret;
    
    }

    public string RenderCurrentLanguage() {
        string ret = "";

        CULTURE c = CULTURE.GetCulture(LANGUAGE.GetCurrentCulture());
        ret = c.DESC;

        return ret;
    }
}