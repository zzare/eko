﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Linq.Expressions;
using System.Linq.Dynamic;

/// <summary>
/// Summary description for ProductRep
/// </summary>
public class ProductRep
{

    public  eMenikDataContext  db;


    public  ProductRep() {
        db = new eMenikDataContext();
        SM.HelperClasses.TraceTextWriter traceWriter = new SM.HelperClasses.TraceTextWriter();
        db.Log = traceWriter; 
    }



    //private  IEnumerable<RuleViolation> GetRuleViolations()
    //{



    //}

    //
    // Query Methods

    public IQueryable<PRODUCT > FindAllProducts()
    {
        return db.PRODUCTs ;
    }

    //public IQueryable<Dinner> FindUpcomingDinners()
    //{
    //    return from dinner in db.Dinners
    //           where dinner.EventDate > DateTime.Now
    //           orderby dinner.EventDate
    //           select dinner;
    //}

    public PRODUCT GetProductByID(Guid id)
    {
        return db.PRODUCTs.SingleOrDefault(w => w.PRODUCT_ID == id);
    }
    public List<PRODUCT> GetProductsByOrder(Guid id)
    {
        var list = from p in db.PRODUCTs
                   from ci in db.CART_ITEMs
                   where ci.OrderID == id && p.PRODUCT_ID == ci.PRODUCT_ID
                   select p;
            return list.ToList();
    }

    public  vw_Product GetProductVWByID(Guid id, string lang, Guid pageID)
    {
        var rec = from p in db.PRODUCTs
                  join pd in db.PRODUCT_DESCs on p.PRODUCT_ID equals pd.PRODUCT_ID into pdo
                  from pd in pdo.DefaultIfEmpty()
                  join r in db.VOTEs on p.PRODUCT_ID equals r.VoteID into ro
                  from r in ro.DefaultIfEmpty()

                  join cwp in (from cwpi in  db.CMS_WEB_PARTs where cwpi.ZONE_ID == "prod_main_gal" select cwpi  ) on p.PRODUCT_ID equals cwp.REF_ID into cwpo
                  from cwp in cwpo.DefaultIfEmpty()

                  join man in db.MANUFACTURERs on p.ManufacturerID  equals man.ManufacturerID into mano
                  from man in mano.DefaultIfEmpty()

                  let imgID = (from ig in db.IMAGE_GALLERies
                               from i in db.IMAGE_ORIGs 
                               where cwp.ID_INT == ig.GAL_ID && i.IMG_ID == ig.IMG_ID 
                               orderby ig.IMG_ORDER, i.DATE_ADDED descending 
                               select ig.IMG_ID).FirstOrDefault()

                  join img in
                      (from it in db.IMAGEs where it.TYPE_ID == SM.BLL.Common.ImageType.ProductDetail  select it) on imgID equals img.IMG_ID into imgo
                  from img in imgo.DefaultIfEmpty()
                  join il in
                      (from desc in db.IMAGE_LANGs where desc.LANG_ID == lang select desc) on img.IMG_ID equals il.IMG_ID into outer
                  from il in outer.DefaultIfEmpty()

                  join imgBig in
                      (from it in db.IMAGEs where it.TYPE_ID == SM.BLL.Common.ImageType.EmenikBigDefault  select it) on imgID equals imgBig.IMG_ID into imgBo
                  from imgBig in imgBo.DefaultIfEmpty()

                  where p.PageId == pageID && p.PRODUCT_ID == id && pd.LANG_ID == lang
                  select new vw_Product
                  {
                      ACTIVE = p.ACTIVE,
                      DELETED = p.DELETED,

                      ADDED_BY = p.ADDED_BY,
                      ADDED_DATE = p.ADDED_DATE,
                      CAT_ACTION = p.CAT_ACTION,
                      CAT_SALE = p.CAT_SALE,
                      CODE = p.CODE,
                      DATE_MODIFIED = p.DATE_MODIFIED,
                      DESCRIPTION_LONG = pd.DESCRIPTION_LONG,
                      DESCRIPTION_SHORT = pd.DESCRIPTION_SHORT,
                      DISCOUNT_PERCENT = p.DISCOUNT_PERCENT,
                      ID_INT = p.ID_INT,
                      IMPORTANCE = p.IMPORTANCE,
                      LANG_ID = pd.LANG_ID,
                      NAME = pd.NAME,
                      PageId = p.PageId,
                      PMOD_ID = p.PMOD_ID,
                      PRICE = p.PRICE,
                      PRODUCT_ID = p.PRODUCT_ID,
                      PRODUCT_TYPE = p.PRODUCT_TYPE,
                      PRODUCT_URL = p.PRODUCT_URL,

                      SMAP_ID = p.SMAP_ID,
                      SPECIAL_DISCOUNT_PERCENT = p.SPECIAL_DISCOUNT_PERCENT,
                      UNITS_IN_STOCK = p.UNITS_IN_STOCK,
                      WARRANTY = p.WARRANTY,
                      TAX_RATE = p.TAX_RATE,

                      IMG_URL = img.IMG_URL ?? "",
                      IMG_DESC = il.IMG_DESC ?? "",
                      IMG_TITLE = il.IMG_TITLE ?? "",
                      IMG_ID = (img != null ? img.IMG_ID : -1),
                      TYPE_ID = (img != null ? img.TYPE_ID : -1),

                      IMG_URL_BIG = imgBig.IMG_URL ?? "",

                      MainImgID = p.MainImgID ,

                      CWP_GAL = (cwp  == null ? Guid.Empty  : cwp.CWP_ID ),

                      ManufacturerID = p.ManufacturerID ,
                      ManufacturerTitle = man.Title ?? "" ,

                      TOTAL_RATING = (r == null ? 0 : r.TotalRating),
                      VOTES = (r == null ? 0 : r.Votes),
                      AvgRating = (r == null ? 0 : r.AvgRating),
                      PriceRangeMax = p.PriceRangeMax 
                  };

        return (vw_Product)rec.SingleOrDefault();
    }

    public vw_Product GetProductVWByID(int idInt, string lang, Guid pageID)
    {
        var rec = from p in db.PRODUCTs
                  join pd in db.PRODUCT_DESCs on p.PRODUCT_ID equals pd.PRODUCT_ID into pdo
                  from pd in pdo.DefaultIfEmpty()
                  join r in db.VOTEs on p.PRODUCT_ID equals r.VoteID into ro
                  from r in ro.DefaultIfEmpty()

                  join cwp in
                      (from cwpi in db.CMS_WEB_PARTs where cwpi.ZONE_ID == "prod_main_gal" select cwpi) on p.PRODUCT_ID equals cwp.REF_ID into cwpo
                  from cwp in cwpo.DefaultIfEmpty()

                  join man in db.MANUFACTURERs on p.ManufacturerID equals man.ManufacturerID into mano
                  from man in mano.DefaultIfEmpty()

                  let imgID = (from ig in db.IMAGE_GALLERies
                               from i in db.IMAGE_ORIGs
                               where cwp.ID_INT == ig.GAL_ID && i.IMG_ID == ig.IMG_ID
                               orderby ig.IMG_ORDER, i.DATE_ADDED descending
                               select ig.IMG_ID).FirstOrDefault()

                  join img in
                      (from it in db.IMAGEs where it.TYPE_ID == SM.BLL.Common.ImageType.ProductDetail select it) on imgID equals img.IMG_ID into imgo
                  from img in imgo.DefaultIfEmpty()
                  join il in
                      (from desc in db.IMAGE_LANGs where desc.LANG_ID == lang select desc) on img.IMG_ID equals il.IMG_ID into outer
                  from il in outer.DefaultIfEmpty()

                  join imgBig in
                      (from it in db.IMAGEs where it.TYPE_ID == SM.BLL.Common.ImageType.EmenikBigDefault select it) on imgID equals imgBig.IMG_ID into imgBo
                  from imgBig in imgBo.DefaultIfEmpty()

                  where p.PageId == pageID && p.ID_INT == idInt && pd.LANG_ID == lang
                  select new vw_Product
                  {
                      ACTIVE = p.ACTIVE,
                      DELETED = p.DELETED,

                      ADDED_BY = p.ADDED_BY,
                      ADDED_DATE = p.ADDED_DATE,
                      CAT_ACTION = p.CAT_ACTION,
                      CAT_SALE = p.CAT_SALE,
                      CODE = p.CODE,
                      DATE_MODIFIED = p.DATE_MODIFIED,
                      DESCRIPTION_LONG = pd.DESCRIPTION_LONG,
                      DESCRIPTION_SHORT = pd.DESCRIPTION_SHORT,
                      DISCOUNT_PERCENT = p.DISCOUNT_PERCENT,
                      ID_INT = p.ID_INT,
                      IMPORTANCE = p.IMPORTANCE,
                      LANG_ID = pd.LANG_ID,
                      NAME = pd.NAME,
                      PageId = p.PageId,
                      PMOD_ID = p.PMOD_ID,
                      PRICE = p.PRICE,
                      PRODUCT_ID = p.PRODUCT_ID,
                      PRODUCT_TYPE = p.PRODUCT_TYPE,
                      PRODUCT_URL = p.PRODUCT_URL,

                      SMAP_ID = p.SMAP_ID,
                      SPECIAL_DISCOUNT_PERCENT = p.SPECIAL_DISCOUNT_PERCENT,
                      UNITS_IN_STOCK = p.UNITS_IN_STOCK,
                      WARRANTY = p.WARRANTY,
                      TAX_RATE = p.TAX_RATE,

                      IMG_URL = img.IMG_URL ?? "",
                      IMG_DESC = il.IMG_DESC ?? "",
                      IMG_TITLE = il.IMG_TITLE ?? "",
                      IMG_ID = (img != null ? img.IMG_ID : -1),
                      TYPE_ID = (img != null ? img.TYPE_ID : -1),

                      IMG_URL_BIG = imgBig.IMG_URL ?? "",

                      MainImgID = p.MainImgID,

                      CWP_GAL = (cwp == null ? Guid.Empty : cwp.CWP_ID),

                      ManufacturerID = p.ManufacturerID,
                      ManufacturerTitle = man.Title ?? "",

                      TOTAL_RATING = (r == null ? 0 : r.TotalRating),
                      VOTES = (r == null ? 0 : r.Votes),
                      AvgRating = (r == null ? 0 : r.AvgRating),
                      PriceRangeMax = p.PriceRangeMax 
                  };

        return (vw_Product)rec.SingleOrDefault();
    }

    public IQueryable<vw_Product> GetProductDescList(string lang, bool? active, Guid pageID)
    {
        return GetProductDescList(lang, active, pageID, SM.BLL.Common.ImageType.ProductList);
    }

    public IQueryable<vw_Product> GetProductDescList(string lang, bool? active, Guid pageID, int imageType)
    {

        var rec = from p in db.PRODUCTs
                  join pd in db.PRODUCT_DESCs on p.PRODUCT_ID equals pd.PRODUCT_ID into pdo
                  from pd in pdo.DefaultIfEmpty()
                  join r in db.VOTEs on p.PRODUCT_ID equals r.VoteID into ro
                  from r in ro.DefaultIfEmpty()
                  join cwp in (from cwpi in db.CMS_WEB_PARTs where cwpi.ZONE_ID == "prod_main_gal" select cwpi) on p.PRODUCT_ID equals cwp.REF_ID into cwpo
                  from cwp in cwpo.DefaultIfEmpty()

                  join man in db.MANUFACTURERs on p.ManufacturerID equals man.ManufacturerID into mano
                  from man in mano.DefaultIfEmpty()

                  let imgID = (from ig in db.IMAGE_GALLERies
                               from i in db.IMAGE_ORIGs
                               where cwp.ID_INT == ig.GAL_ID && i.IMG_ID == ig.IMG_ID
                               orderby p.MainImgID == i.IMG_ID  descending , ig.IMG_ORDER, i.DATE_ADDED descending
                               select ig.IMG_ID).FirstOrDefault()

                  join img in
                      (from it in db.IMAGEs where it.TYPE_ID == imageType  select it) on imgID equals img.IMG_ID into imgo
                  from img in imgo.DefaultIfEmpty()
                  join il in
                      (from desc in db.IMAGE_LANGs where desc.LANG_ID == lang select desc) on img.IMG_ID equals il.IMG_ID into outer
                  from il in outer.DefaultIfEmpty()




                  where p.PageId == pageID && pd.PRODUCT_ID == p.PRODUCT_ID && pd.LANG_ID == lang && p.DELETED == false
                  select new vw_Product
                  {
                      ACTIVE = p.ACTIVE,
                      DELETED = p.DELETED ,
                      ADDED_BY = p.ADDED_BY,
                      ADDED_DATE = p.ADDED_DATE,
                      CAT_ACTION = p.CAT_ACTION,
                      CAT_SALE = p.CAT_SALE,
                      CODE = p.CODE,
                      DATE_MODIFIED = p.DATE_MODIFIED ,
                      DESCRIPTION_LONG = pd.DESCRIPTION_LONG,
                      DESCRIPTION_SHORT = pd.DESCRIPTION_SHORT,
                      DISCOUNT_PERCENT = p.DISCOUNT_PERCENT,
                      ID_INT = p.ID_INT,
                      IMPORTANCE = p.IMPORTANCE,
                      LANG_ID = pd.LANG_ID,
                      NAME = pd.NAME,
                      PageId = p.PageId,
                      PMOD_ID = p.PMOD_ID,
                      PRICE = p.PRICE,
                      PRODUCT_ID = p.PRODUCT_ID,
                      PRODUCT_TYPE = p.PRODUCT_TYPE,
                      PRODUCT_URL = p.PRODUCT_URL,
                      SMAP_ID = p.SMAP_ID,
                      SPECIAL_DISCOUNT_PERCENT = p.SPECIAL_DISCOUNT_PERCENT,
                      UNITS_IN_STOCK = p.UNITS_IN_STOCK,
                      WARRANTY = p.WARRANTY,
                      TAX_RATE = p.TAX_RATE,


                      IMG_URL = img.IMG_URL ?? "",
                      IMG_DESC = il.IMG_DESC ?? "",
                      IMG_TITLE = il.IMG_TITLE ?? "",
                      IMG_ID = (img != null ? img.IMG_ID : -1),
                      TYPE_ID = (img != null ? img.TYPE_ID : -1),

                      CWP_GAL = (cwp == null ? Guid.Empty : cwp.CWP_ID),

                      MainImgID = p.MainImgID,


                      ManufacturerID = p.ManufacturerID,
                      ManufacturerTitle = man.Title ?? "",

                      TOTAL_RATING = (r == null ? 0 : r.TotalRating),
                      VOTES = (r == null ? 0 : r.Votes),
                      AvgRating = (r == null ? 0 : r.AvgRating),
                      PDescID = pd.PDescID,
                      PriceRangeMax = p.PriceRangeMax 
                  };
        if (active != null)
            rec = rec.Where(PRODUCT.IsActiveVW());
            //rec = rec.Where(w => w.ACTIVE == active);
            

        return rec;
        
    }

    public IQueryable<vw_Product> GetProductDescList(int smap, string lang, bool? active, Guid pageID)
    {
        return GetProductDescList(smap, lang, active, pageID, SM.BLL.Common.ImageType.ProductList);
    }

    public IQueryable< vw_Product> GetProductDescList(int smap, string lang, bool? active, Guid pageID, int imageType )
    {

        var rec = GetProductDescList(lang, active, pageID, imageType );
        var list = from r in rec
                   from s in db.f_h_Sitemap(smap)
                   where r.SMAP_ID == s.SMAP_ID
                   select r;

        //return rec.Where (w=>w.SMAP_ID == smap) ;
        return list;
    }
    public IQueryable<vw_Product> GetProductDescAdditionalList(int smap, string lang, bool? active, Guid pageID)
    {

        var rec = GetProductDescList(lang, active, pageID);
        var list = from r in rec
                   from ps in db.PRODUCT_SMAPs 
                   from s in db.f_h_Sitemap(smap)
                   where ps.PRODUCT_ID == r.PRODUCT_ID && ps.SMAP_ID ==  s.SMAP_ID
                   select r;

        //return rec.Where (w=>w.SMAP_ID == smap) ;
        return list;
    }

    public IQueryable<vw_Product> GetProductDescCategoryList(int smap, string lang, bool? active, Guid pageID)
    {

        var rec = GetProductDescList(lang, active, pageID);
        var list = from r in rec
                   from sml in db.SITEMAP_LANGs 
                   from s in db.f_h_Sitemap(smap)
                   from smp  in db.SITEMAPs 
                   where r.SMAP_ID == s.SMAP_ID && smp.SMAP_ID == r.SMAP_ID 
                   && sml.SMAP_ID == r.SMAP_ID && sml.LANG_ID == lang
                   orderby  smp.SMAP_ORDER , sml.SML_ORDER , sml.SMAP_ID , r.ACTIVE descending,  r.IMPORTANCE 
                   select r;

        //return rec.Where (w=>w.SMAP_ID == smap) ;
        return list;
    }

    public IQueryable<vw_Product> GetProductDescListByManufacturer(Guid man,  string lang, bool? active, Guid pageID, Guid? exclude=null)
    {

        var rec = GetProductDescList(lang, active, pageID);
        var list = from r in rec
                   where r.ManufacturerID == man
                   //orderby r.IMPORTANCE
                   select r;
        if (exclude != null) {
            list = list.Where(w => w.PRODUCT_ID != exclude);
        }
        list = list.OrderBy(o => o.IMPORTANCE);
        //return rec.Where (w=>w.SMAP_ID == smap) ;
        return list;
    }


    public IQueryable<vw_Product> GetProductDescListSearch(string srch, string lang, bool? active, Guid pageID)
    {

        var rec = GetProductDescList(lang, active, pageID);

        return (from l in rec join s in db.f_fts_ProductDesc (srch) on l.PDescID equals s.KEY select l );
    }

    public IQueryable<PRODUCT_DESC_WRAP> GetProductDescListByPage(Guid productID, Guid pageID)
    {
        var rec = from uc in db.EM_USER_CULTUREs
                  from prod in db.PRODUCTs
                  from c in db.CULTUREs
                  join p in
                      (from p in db.PRODUCT_DESCs where p.PRODUCT_ID == productID select p) on c.LANG_ID equals p.LANG_ID into outer
                  from pd in outer.DefaultIfEmpty()
                  from l in db.LANGUAGEs
                  where prod.PRODUCT_ID == productID && prod.PageId == pageID && prod.PageId == uc.UserId && c.LCID == uc.LCID && l.LANG_ID == c.LANG_ID
                  orderby uc.DEFAULT descending, uc.SORT
                  select new PRODUCT_DESC_WRAP { LANG_ID = l.LANG_ID, PRODUCT_ID = prod.PRODUCT_ID, NAME = pd.NAME, DESCRIPTION_LONG = pd.DESCRIPTION_LONG, DESCRIPTION_SHORT = pd.DESCRIPTION_SHORT };

        return (IQueryable<PRODUCT_DESC_WRAP>)rec;
    }


    public PRODUCT_DESC GetProductDescByID(Guid prodID, string lang)
    {
        return db.PRODUCT_DESCs.SingleOrDefault(w => w.PRODUCT_ID == prodID && w.LANG_ID == lang);
    }


    public ATTRIBUTE GetAttributeByID(Guid attrID)
    {
        return db.ATTRIBUTEs.Where(w => w.AttributeID == attrID ).SingleOrDefault();
    }
    public GROUP_ATTRIBUTE  GetGroupAttributeByID(Guid attrID)
    {
        return db.GROUP_ATTRIBUTEs.Where(w => w.GroupAttID == attrID).SingleOrDefault();
    }

    public PRODUCT_ATTRIBUTE GetProductAttributeByID(Guid pid, Guid attr)
    {
        return db.PRODUCT_ATTRIBUTEs .Where(w => w.AttributeID == attr && w.PRODUCT_ID == pid ).SingleOrDefault();
    }

    public PRODUCT_ATTRIBUTE_DESC  GetProductAttributeDescByID(Guid pid, Guid attr, string lang)
    {
        return db.PRODUCT_ATTRIBUTE_DESCs.Where(w => w.AttributeID == attr && w.PRODUCT_ID== pid && w.LANG_ID == lang).SingleOrDefault();
    }


    public IQueryable<ATTRIBUTE> GetAttributes(Guid grid)
    {
        return db.ATTRIBUTEs.Where (w => w.GroupAttID  == grid );
    }

    public IQueryable<GROUP_ATTRIBUTE> GetGroupAttributesByPage(Guid pageID)
    {
        IQueryable<GROUP_ATTRIBUTE> list = from ga in db.GROUP_ATTRIBUTEs
                                           where (ga.PageId == null || ga.PageId == pageID)
                                           orderby ga.GrType, ga.Order 
                                           select ga;
        return list;
    }
    public IQueryable<ProductRep.Data.GroupAttributeLang> GetGroupAttributesByProduct(Guid prodID, string lang)
    {
        IQueryable<ProductRep.Data.GroupAttributeLang> list = from ga in db.GROUP_ATTRIBUTEs
                                                              join gal in ( from gali in  db.GROUP_ATTRIBUTE_LANGs where gali.LANG_ID == lang  select gali ) on ga.GroupAttID equals gal.GroupAttID into galo
                                                              from gal in galo.DefaultIfEmpty()
                                                              from pga in db.PRODUCT_GROUP_ATTRIBUTEs
                                                              where pga.PRODUCT_ID == prodID && ga.GroupAttID == pga.GroupAttID
                                                              orderby ga.GrType, ga.Order 
                                                              select new ProductRep.Data.GroupAttributeLang
                                                              {  GroupAttID = ga.GroupAttID ,
                                                                  PageId = ga.PageId,
                                                                  Title = gal.Title,
                                                                  Desc = gal.Desc, 
                                                                  Message = gal.Message,
                                                                  OrderByField = ga.OrderByField ,
                                                                  GrType = ga.GrType ,
                                                                  Order = ga.Order 
                                                              };
        return list;
    }
    public IQueryable<PRODUCT_GROUP_ATTRIBUTE> GetProductGroupAttributesByProduct(Guid prodID)
    {
        IQueryable<PRODUCT_GROUP_ATTRIBUTE> list = from ga in db.PRODUCT_GROUP_ATTRIBUTEs
                                           where (ga.PRODUCT_ID == prodID )
                                           select ga;
        return list;
    }
    public IQueryable<ATTRIBUTE> GetAttributesByProduct(Guid prodID)
    {
        IQueryable<ATTRIBUTE> list = from a in db.ATTRIBUTEs
                                     from pa in db.PRODUCT_ATTRIBUTEs
                                     where pa.PRODUCT_ID == prodID && a.AttributeID == pa.AttributeID
                                     select a;
        return list;
    }
    public IQueryable<PRODUCT_ATTRIBUTE> GetProductAttributesByProduct(Guid prodID)
    {
        IQueryable<PRODUCT_ATTRIBUTE> list = from pa in db.PRODUCT_ATTRIBUTEs
                                     where pa.PRODUCT_ID == prodID
                                     select pa;
        return list;
    }
    public IQueryable<PRODUCT_ATTRIBUTE> GetProductAttributesByProduct(Guid prodID, Guid groupID)
    {
        IQueryable<PRODUCT_ATTRIBUTE> list = from pa in db.PRODUCT_ATTRIBUTEs
                                             from a in db.ATTRIBUTEs 
                                             where pa.PRODUCT_ID == prodID
                                             && a.AttributeID == pa.AttributeID && a.GroupAttID == groupID
                                             select pa;
        return list;
    }

    public IQueryable<Data.wProductAttributeDesc > GetProductAttributesWithDescByProduct(Guid prodID, Guid groupID, string lang)
    {
        IQueryable<Data.wProductAttributeDesc> list = from pa in db.PRODUCT_ATTRIBUTEs
                                             from a in db.ATTRIBUTEs
                                             join desc in
                                                 (from desci in db.PRODUCT_ATTRIBUTE_DESCs where desci.LANG_ID == lang && desci.PRODUCT_ID == prodID  select desci) on pa.AttributeID  equals desc.AttributeID into desco
                                             from desc in desco.DefaultIfEmpty()
                                                              
                                             where pa.PRODUCT_ID == prodID
                                             && a.AttributeID == pa.AttributeID && a.GroupAttID == groupID
                                             select new Data.wProductAttributeDesc ( pa, desc);
        return list;
    }

    public IQueryable<ProductRep.Data.wAttrubute > GetwProductAttributesByProduct(Guid prodID)
    {
        IQueryable<ProductRep.Data.wAttrubute> list = from pa in db.PRODUCT_ATTRIBUTEs
                                                      from a in db.ATTRIBUTEs
                                                      where pa.PRODUCT_ID == prodID
                                                      && a.AttributeID == pa.AttributeID
                                                      select new Data.wAttrubute { AttributeID = a.AttributeID, GroupAttID = a.GroupAttID, Title= a.Title, Desc = a.Desc, Code =a.Code, UrlBig = a.UrlBig, UrlSmall =a.UrlSmall, Ordr = a.Ordr ,
                                                      ValMin = pa.ValMin, ValMax = pa.ValMax , ValDefault = pa.ValDefault, Price = pa.Price  };
        return list;
    }

    public IQueryable<ProductRep.Data.wProductAttributeDesc> GetwProductAttributeDescsByProduct(Guid prodID, string lang)
    {
        IQueryable<ProductRep.Data.wProductAttributeDesc> list = from pa in db.PRODUCT_ATTRIBUTEs
                                                                 from a in db.ATTRIBUTEs
                                                                 join desc in
                                                                     (from desci in db.PRODUCT_ATTRIBUTE_DESCs where desci.LANG_ID == lang && desci.PRODUCT_ID == prodID select desci) on pa.AttributeID equals desc.AttributeID into desco
                                                                 from desc in desco.DefaultIfEmpty()
                                                                 where pa.PRODUCT_ID == prodID
                                                                 && a.AttributeID == pa.AttributeID
                                                                 select new Data.wProductAttributeDesc(pa, desc, a);
        return list;
    }

    public ProductRep.Data.wProductAttributeDesc GetwProductAttributeDescByID(Guid prodID, Guid attrID, string lang)
    {
        var ret = ( from pa in db.PRODUCT_ATTRIBUTEs
                  from a in db.ATTRIBUTEs
                  join desc in
                      (from desci in db.PRODUCT_ATTRIBUTE_DESCs where desci.LANG_ID == lang && desci.PRODUCT_ID == prodID && desci.AttributeID == attrID select desci) on pa.AttributeID equals desc.AttributeID into desco
                  from desc in desco.DefaultIfEmpty()
                  where pa.PRODUCT_ID == prodID
                  && a.AttributeID == pa.AttributeID && pa.AttributeID == attrID 
                  select new Data.wProductAttributeDesc(pa, desc, a)).FirstOrDefault();
        return ret;
    }


    public IQueryable<ATTRIBUTE> GetAttributesByproductInvert(Guid grid, Guid prodid)
    {
        var list = from a in db.ATTRIBUTEs
                   where  a.GroupAttID == grid   &&
                   !(from pa in db.PRODUCT_ATTRIBUTEs where pa.AttributeID == a.AttributeID && pa.PRODUCT_ID == prodid select 1).Any()
                   select a;

        return list;
    }

    public IQueryable<SITEMAP_LANG > GetProductSitemapLangs(Guid prodID, string lang)
    {
        IQueryable<SITEMAP_LANG> list = from pa in db.PRODUCT_SMAPs 
                                        from sml in db.SITEMAP_LANGs 
                                             where  pa.PRODUCT_ID == prodID && sml.SMAP_ID == pa.SMAP_ID && sml.LANG_ID == lang
                                             select sml;
        return list;
    }

    public IQueryable<SITEMAP_LANG> GetSitemapLangsAdditionalByPM(Guid pageID, int pmod,  string lang)
    {
        IQueryable<SITEMAP_LANG> list = from sml in db.SITEMAP_LANGs
                                        from smp in db.SITEMAPs 
                                        from m in db.MENUs 
                                        from u in db.EM_USERs 
                                        where smp.MENU_ID == m.MENU_ID && u.USERNAME == m.USERNAME && u.UserId == pageID &&
                                        sml.SMAP_ID == smp.SMAP_ID  && sml.LANG_ID == lang && smp.PMOD_ID == pmod 
                                        select sml;
        return list;
    }


    public CART_ITEM  GetCartItemByID(Guid prodID, Guid userID,  List<Data.AttributeItem > attrs)
    {
        var item = from ci in db.CART_ITEMs
                   where ci.PRODUCT_ID == prodID && ci.OrderID == null && ci.UserId == userID 
                   select ci;
        foreach (Data.AttributeItem attr in attrs) {
            Guid groupAttID = attr.ID;
            Guid attributeID = attr.Value;
            int? val = attr.ValSet;
            item = item.Join(db.CART_ITEM_ATTRIBUTEs.Where(w => w.GroupAttID == groupAttID && w.AttributeID == attributeID && object.Equals(w.Value , val)), ci => ci.CartItemID, cia => cia.CartItemID, (ci, cia) => ci);
        }

        return item.FirstOrDefault();
    }
    public CART_ITEM GetCartItemByID(Guid cartID, Guid userid)
    {
        var item = from ci in db.CART_ITEMs
                   where ci.CartItemID == cartID && ci.UserId == userid 
                   select ci;
        return item.FirstOrDefault();
    }



    public Data.ShoppingCartItem GetShoppingCartItemByID(Guid cartID, Guid userID, string lang, Guid? pageID)
    {
        var item = (from ci in db.CART_ITEMs
                   from p in db.PRODUCTs
                   join pd in db.PRODUCT_DESCs on ci.PRODUCT_ID equals pd.PRODUCT_ID into pdo
                   from pd in pdo.DefaultIfEmpty()
                   let attributes = from ca in db.CART_ITEM_ATTRIBUTEs
                                    from ga in db.GROUP_ATTRIBUTEs
                                    
                                    join gal in
                                        (from gali in db.GROUP_ATTRIBUTE_LANGs where gali.LANG_ID == lang select gali) on ga.GroupAttID equals gal.GroupAttID into galo
                                    from gal in galo.DefaultIfEmpty()

                                    //join pa in (from pai in db.PRODUCT_ATTRIBUTEs  where pai.PRODUCT_ID == p.PRODUCT_ID  select pai) on a.AttributeID equals pa.AttributeID
                                    //from pa in pa.DefaultIfEmpty()

                                    join desc in
                                        (from desci in db.PRODUCT_ATTRIBUTE_DESCs where desci.LANG_ID == lang && desci.PRODUCT_ID == p.PRODUCT_ID  select desci) on ca.AttributeID equals desc.AttributeID into desco
                                    from desc in desco.DefaultIfEmpty()

                                    from a in db.ATTRIBUTEs                                    
                                    where ca.CartItemID == ci.CartItemID && ga.GroupAttID == ca.GroupAttID && a.AttributeID == ca.AttributeID
                                    select new Data.ItemGroupAttribute
                                    {

                                        GroupAttID = a.GroupAttID,
                                        AttributeID = a.AttributeID,
                                        GroupTitle = gal.Title,
                                        GroupDesc = gal.Desc,
                                        GroupMessage = gal.Message,
                                        Title = a.Title,
                                        Desc = a.Desc,
                                        Code = a.Code,
                                        UrlBig = a.UrlBig,
                                        UrlSmall = a.UrlSmall,
                                        GrType = ga.GrType ,
                                        PTitle = desc.Title,
                                        PDesc = desc.Desc,
                                        Price = ca.PriceAttribute
                                        

                                    }
                    join cwp in
                        (from cwpi in db.CMS_WEB_PARTs where cwpi.ZONE_ID == "prod_main_gal" select cwpi) on p.PRODUCT_ID equals cwp.REF_ID into cwpo
                   from cwp in cwpo.DefaultIfEmpty()

                   let img = (from ig in db.IMAGE_GALLERies
                              from io in db.IMAGE_ORIGs
                              from i in db.IMAGEs
                              join il in
                                  (from desc in db.IMAGE_LANGs where desc.LANG_ID == lang select desc) on i.IMG_ID equals il.IMG_ID into outer
                              from idesc in outer.DefaultIfEmpty()

                              where cwp.ID_INT == ig.GAL_ID && i.IMG_ID == ig.IMG_ID && io.IMG_ID == i.IMG_ID
                              && i.TYPE_ID == SM.BLL.Common.ImageType.ProductCartSmall
                              orderby ig.IMG_ORDER
                              select new { IMG_URL = i.IMG_URL, IMG_TITLE = idesc.IMG_TITLE, IMG_DESC = idesc.IMG_DESC }).FirstOrDefault()
                    where ci.CartItemID == cartID && ci.UserId == userID && ci.OrderID == null && pd.LANG_ID == lang && p.PRODUCT_ID == ci.PRODUCT_ID && p.PageId == pageID && p.ACTIVE == true && p.DELETED == false 
                   select new Data.ShoppingCartItem
                   {
                       CartItemID = ci.CartItemID,
                       ProductID = ci.PRODUCT_ID,
                       UserId = ci.UserId,
                       PageId = p.PageId,
                       Lang = lang,
                       DateAdded = ci.DateAdded,
                       Quantity = ci.Quantity,
                       Price = ci.Price,
                       PriceTotal = ci.PriceTotal ?? 0,
                       Tax = ci.Tax,
                       TaxRate = p.TAX_RATE ,
                       AttrList = attributes.ToList(),
                       ProductTitle = pd.NAME,
                       ProductUrl = (img == null ? "" : img.IMG_URL),
                       Code = ci.Code

                       //,                       Stock = ci.UnitsInStock 
                   }).SingleOrDefault();
        return item;
    }
    public List<Data.ShoppingCartItem> GetCartItemList(Guid userid, string lang, Guid? pageID, Guid? orderID)
    {
        if (SM.EM.Globals.Settings.ST.EnableCaching)
        {
            // if data in cache, return it
            if (SM.EM.BLL.BizObject .Cache[SM.EM.Caching.Key.CartItemList (userid, lang, pageID, orderID )] != null)
                return (List<Data.ShoppingCartItem>)SM.EM.BLL.BizObject.Cache[SM.EM.Caching.Key.CartItemList(userid, lang, pageID, orderID)];
        }


        Guid? ordr = null;
        var item = from ci in db.CART_ITEMs
                   from p in db.PRODUCTs 
                   join pd in db.PRODUCT_DESCs on ci.PRODUCT_ID equals pd.PRODUCT_ID into pdo
                   from pd in pdo.DefaultIfEmpty()
                   let attributes = from ca in db.CART_ITEM_ATTRIBUTEs
                                    from ga in db.GROUP_ATTRIBUTEs
                                    join gal in
                                        (from gali in db.GROUP_ATTRIBUTE_LANGs where gali.LANG_ID == lang select gali) on ga.GroupAttID equals gal.GroupAttID into galo
                                    from gal in galo.DefaultIfEmpty()
                                    
                                    join desc in
                                        (from desci in db.PRODUCT_ATTRIBUTE_DESCs where desci.LANG_ID == lang && desci.PRODUCT_ID == p.PRODUCT_ID select desci) on ca.AttributeID equals desc.AttributeID into desco
                                    from desc in desco.DefaultIfEmpty()
                          
                                    from a in db.ATTRIBUTEs 
                                    where ca.CartItemID == ci.CartItemID && ga.GroupAttID == ca.GroupAttID && a.AttributeID == ca.AttributeID 
                                    orderby ga.GrType, ga.Order 
                                    select new Data.ItemGroupAttribute { 
                                        
                                        GroupAttID = a.GroupAttID,
                                        AttributeID = a.AttributeID,
                                        GroupTitle = gal.Title,
                                        GroupDesc = gal.Desc,
                                        GroupMessage = gal.Message,
                                        Title = a.Title,
                                        Desc = a.Desc,
                                        Code = a.Code,
                                        UrlBig = a.UrlBig,
                                        UrlSmall = a.UrlSmall,
                                        Value = ca.Value,
                                        GrType = ga.GrType,
                                        PTitle = desc.Title,
                                        PDesc = desc.Desc,
                                        Price = ca.PriceAttribute
                                    }
                   join cwp in
                       (from cwpi in db.CMS_WEB_PARTs where cwpi.ZONE_ID == "prod_main_gal" select cwpi) on p.PRODUCT_ID equals cwp.REF_ID into cwpo
                   from cwp in cwpo.DefaultIfEmpty()

                   let img = (from ig in db.IMAGE_GALLERies
                              from io in db.IMAGE_ORIGs
                              from i in db.IMAGEs
                              join il in
                                  (from desc in db.IMAGE_LANGs where desc.LANG_ID == lang select desc) on i.IMG_ID equals il.IMG_ID into outer
                              from idesc in outer.DefaultIfEmpty()

                              where cwp.ID_INT == ig.GAL_ID && i.IMG_ID == ig.IMG_ID && io.IMG_ID == i.IMG_ID
                              && i.TYPE_ID == SM.BLL.Common.ImageType.ProductCartSmall 
                              orderby ig.IMG_ORDER
                              select new { IMG_URL = i.IMG_URL, IMG_TITLE = idesc.IMG_TITLE, IMG_DESC = idesc.IMG_DESC }).FirstOrDefault()
                   where ci.UserId == userid && pd.LANG_ID == lang && p.PRODUCT_ID == ci.PRODUCT_ID && p.ACTIVE == true && p.DELETED == false 
                   && ( orderID == null ?  ci.OrderID == null : ci.OrderID == orderID )
                   select new Data.ShoppingCartItem
                   {
                       CartItemID = ci.CartItemID,
                       ProductID = ci.PRODUCT_ID ,
                       UserId = ci.UserId,
                       PageId = p.PageId ,
                       Lang = lang,
                       DateAdded = ci.DateAdded,
                       Quantity = ci.Quantity,
                       Price = ci.Price,
                       PriceTotal = ci.PriceTotal ?? 0,
                       Tax = ci.Tax,
                       TaxRate = p.TAX_RATE,
                       AttrList = attributes.ToList(),
                       ProductTitle = pd.NAME ,
                       ProductUrl = (img == null ? "" : img.IMG_URL) 
                   };
        if (pageID != null)
            item = item.Where(w=>w.PageId == pageID);

        // get data
        List<Data.ShoppingCartItem> itemList = item.ToList();

        // add to cache
        SM.EM.BLL.BizObject.CacheData(SM.EM.Caching.Key.CartItemList(userid, lang, pageID, orderID),itemList);

        return itemList;
    }


    public IQueryable<MANUFACTURER > GetManufacturers(Guid pageid)
    {
        return db.MANUFACTURERs.Where(w => w.PageId == pageid ).OrderBy(o=>o.Title );
    }
    public IQueryable<MANUFACTURER_COUNT> GetManufacturersByParentSmap(int smp, Guid pageid)
    {
        var list = from s in db.f_h_Sitemap(smp)
                   from p in db.PRODUCTs
                   where p.PageId == pageid && p.SMAP_ID == s.SMAP_ID
                   && p.ACTIVE == true && p.DELETED == false
                   group p by p.ManufacturerID into pg
                   from m in db.MANUFACTURERs
                   where m.ManufacturerID == pg.Key
                   orderby m.Title ascending 
                   select new MANUFACTURER_COUNT { ManufacturerID = m.ManufacturerID, Title =  m.Title, Desc = m.Desc, Num = pg.Count() , PageId = m.PageId  };
        return list;
    }
    public IQueryable<MANUFACTURER_COUNT> GetManufacturersByParentSmapAdditional(int smp, Guid pageid)
    {
        var list = from s in db.f_h_Sitemap(smp)
                   from p in db.PRODUCTs
                   from ps in db.PRODUCT_SMAPs
                   where p.PageId == pageid && p.PRODUCT_ID == ps.PRODUCT_ID && ps.SMAP_ID ==  s.SMAP_ID
                   && p.ACTIVE == true && p.DELETED == false
                   group p by p.ManufacturerID into pg
                   from m in db.MANUFACTURERs
                   where m.ManufacturerID == pg.Key
                   orderby m.Title ascending 
                   select new MANUFACTURER_COUNT { ManufacturerID = m.ManufacturerID, Title = m.Title, Desc = m.Desc, Num = pg.Count(), PageId = m.PageId };
        return list;
    }

    public IQueryable<MANUFACTURER_COUNT> GetManufacturersByProducts(IQueryable<vw_Product > prodList )
    {
        var list = from p in prodList 
                   group p by p.ManufacturerID into pg
                   from m in db.MANUFACTURERs
                   where m.ManufacturerID == pg.Key
                   orderby m.Title ascending 
                   select new MANUFACTURER_COUNT { ManufacturerID = m.ManufacturerID, Title = m.Title, Desc = m.Desc, Num = pg.Count(), PageId = m.PageId };
        return list;
    }


    //public IQueryable<FiltShort> GetSexesByParentSmap(int smp, Guid pageid)
    //{
       
    //    var list = from s in db.f_h_Sitemap(smp)
    //               from p in db.PRODUCTs
    //               where p.PageId == pageid && p.SMAP_ID == s.SMAP_ID
    //               && p.ACTIVE == true && p.DELETED == false
    //               //&& p.SEX != 0
    //               group p by p.SEX into pg
    //               select new FiltShort { Value = pg.Key , Num = pg.Count() };
    //    return list;
    //}

    public IQueryable<FiltInt> GetTypesByParentSmap(int smp, Guid pageid)
    {

        var list = from s in db.f_h_Sitemap(smp)
                   from p in db.PRODUCTs
                   where p.PageId == pageid && p.SMAP_ID == s.SMAP_ID
                   && p.ACTIVE == true && p.DELETED == false
                   && p.PRODUCT_TYPE  != 0
                   group p by p.PRODUCT_TYPE  into pg
                   select new FiltInt { Value = pg.Key, Num = pg.Count() };
        return list;
    }


    public void DeleteAttr(ATTRIBUTE attr)
    {

        db.ATTRIBUTEs.DeleteOnSubmit (attr );
    }
    public void DeleteProductGroupAttr(Guid pid)
    {
        var list  = from pga in db.PRODUCT_GROUP_ATTRIBUTEs 
                    where pga.PRODUCT_ID == pid
                        select pga;
        db.PRODUCT_GROUP_ATTRIBUTEs.DeleteAllOnSubmit(list);
    }
    public int DeleteProductAttributesByGroup(Guid pid,  Guid grid)
    {
        var list = from pga in db.PRODUCT_ATTRIBUTEs
                   from a in db.ATTRIBUTEs 
                   where pga.PRODUCT_ID == pid && a.GroupAttID  == grid && pga.AttributeID == a.AttributeID 
                   select pga;
        db.PRODUCT_ATTRIBUTEs.DeleteAllOnSubmit(list);
        return list.Count();
    }

    public void DeleteProductSmaps(Guid pid)
    {
        var list = from pga in db.PRODUCT_SMAPs
                   where pga.PRODUCT_ID == pid
                   select pga;
        db.PRODUCT_SMAPs.DeleteAllOnSubmit(list);
    }

    public void Add(PRODUCT  product)
    {
        if (HttpContext.Current != null)
            product.ADDED_BY = HttpContext.Current.User.Identity.Name;

        if (product.PRODUCT_ID == Guid.Empty)
            product.PRODUCT_ID  = Guid.NewGuid();

        product.ADDED_DATE = DateTime.Now;
        product.DATE_MODIFIED = DateTime.Now;

        if (product.TAX_RATE <= 0)
            product.TAX_RATE = 0.2;


        db.PRODUCTs.InsertOnSubmit(product);


        // insert CWP for gallery
        CMS_WEB_PART wp = CMS_WEB_PART.CreateNewCwpGallery(SM.BLL.Common.GalleryType.DefaultProductGallery, product.PRODUCT_ID, null, MODULE.Common.GalleryModID, PRODUCT.Common.MAIN_GALLERY_ZONE, product.PageId, "Galerija izdelka", "", true, false, -1, false, false, 3, SM.BLL.Common.ImageType.EmenikGalleryThumbDefault, SM.BLL.Common.ImageType.EmenikBigDefault);


    }

    public void AddDesc(PRODUCT_DESC pdesc)
    {
        if (pdesc.DESCRIPTION_LONG == null)
            pdesc.DESCRIPTION_LONG = "";

        db.PRODUCT_DESCs.InsertOnSubmit(pdesc);
    }

    public void AddAttribute(ATTRIBUTE  att)
    {

        db.ATTRIBUTEs.InsertOnSubmit(att);
    }
    public void AddProductGroupAttribute(PRODUCT_GROUP_ATTRIBUTE  att)
    {
        db.PRODUCT_GROUP_ATTRIBUTEs .InsertOnSubmit(att);
    }
    public void AddProductAttribute(PRODUCT_ATTRIBUTE att)
    {
        db.PRODUCT_ATTRIBUTEs.InsertOnSubmit(att);
    }
    public void AddProductAttributeDesc(PRODUCT_ATTRIBUTE_DESC att)
    {
        db.PRODUCT_ATTRIBUTE_DESCs.InsertOnSubmit(att);
    }

    public void AddProductSmap(PRODUCT_SMAP psmp)
    {
        db.PRODUCT_SMAPs.InsertOnSubmit(psmp);
    }

    public void AddItemToCart(Guid pid, Guid userid, int quantity, List<Data.AttributeItem> attrList, Guid? orderID) {
        CART_ITEM item = GetCartItemByID(pid, userid, attrList);
        // get product
        PRODUCT prod = GetProductByID(pid);
        if (prod == null)
            return;

        // if not active, return
        if (!prod.ACTIVE || prod.DELETED)
            return;

        // remove if quantity = 0
        if (quantity < 0)
        {
//            RemoveItemFromCart(pid, userid, attrList);
            return;
        }

        decimal total = 0;
        decimal total_no_tax = 0;
        decimal tax = 0;

        decimal attrPrice = -1;

        // new item
        if (item == null){

            item = new CART_ITEM { PRODUCT_ID = pid, UserId = userid, Quantity = quantity, OrderID = null, CartItemID = Guid.NewGuid(), DateAdded = DateTime.Now};
            // inser item
            db.CART_ITEMs.InsertOnSubmit(item);
            // insert attributes
            attrPrice = InsertCartItemAttributes(item.CartItemID, attrList, pid);
        }
        else { 
            //update quantity and price
            item.Quantity += quantity;
            // get price
            attrPrice = GetCartItemAttributePrice(attrList, pid);
        }

        //total = prod.PRICE * item.Quantity;
        //total_no_tax = (decimal)prod.PRICE_NO_TAX * item.Quantity;
        decimal prodPrice = prod.GetPriceWithDiscount();
        decimal prodPriceNoTax = prod.GetPriceNoTaxWithDiscount();

        // force price from attribute
        if (attrPrice > -1){
            prodPrice = prod.GetPriceWithDiscount( attrPrice);
            prodPriceNoTax = prod.GetPriceNoTaxWithDiscount( attrPrice);
        }

        total = prodPrice   * item.Quantity;
        total_no_tax = prodPriceNoTax * item.Quantity;
        tax = total - total_no_tax;
        item.Price = total_no_tax;
        item.Tax = tax;
        item.OrderID = orderID;

        // remove cache
        SM.EM.Caching.Remove.CartItemList(userid);
    
    }

    public void UpdateCartItem(Guid cartID, Guid userid, int quantity, List<Data.AttributeItem> attrList) {
        if (quantity <= 0)
        {
            // remove item 
            RemoveItemFromCart(cartID, userid );
            return;
        
        }
        CART_ITEM item = GetCartItemByID(cartID, userid);
        if (item == null)
            return;

        // if new item is already in cart, add to other item 
        CART_ITEM other_item = GetCartItemByID(item.PRODUCT_ID, userid, attrList);
        if (other_item != null && other_item.CartItemID != item.CartItemID) {
            RemoveItemFromCart(cartID, userid);
            AddItemToCart(item.PRODUCT_ID, userid, quantity, attrList, null);
            return;
        
        }


        // get product
        PRODUCT prod = GetProductByID(item.PRODUCT_ID );
        if (prod == null)
        {
            // remove item 
            RemoveItemFromCart(cartID, userid );
            return;
        }
        if (!prod.ACTIVE){
            // remove item 
            RemoveItemFromCart(cartID, userid);
            return;
        }

        decimal prodPrice = prod.GetPriceWithDiscount();
        decimal prodPriceNoTax = prod.GetPriceNoTaxWithDiscount();
        decimal attrPrice = GetCartItemAttributePrice(attrList, item.PRODUCT_ID);

        // force price from attribute
        if (attrPrice > -1)
        {
            prodPrice = prod.GetPriceWithDiscount(attrPrice);
            prodPriceNoTax = prod.GetPriceNoTaxWithDiscount(attrPrice);
        }


        decimal total = 0;
        decimal total_no_tax = 0;
        decimal tax = 0;
        item.Quantity = quantity;
        //total = prod.PRICE * item.Quantity;
        //total_no_tax = (decimal)prod.PRICE_NO_TAX * item.Quantity;
        total = prodPrice * item.Quantity;
        total_no_tax = prodPriceNoTax * item.Quantity;

        tax = total - total_no_tax;
        item.Price = total_no_tax;
        item.Tax = tax;    

        //update attributes
        DeleteCartItemAttributes(cartID);
        InsertCartItemAttributes(cartID, attrList, item.PRODUCT_ID);

        // remove cache
        SM.EM.Caching.Remove.CartItemList(userid);
    
    }


    public void UpdateCartItemQuantity(Guid cartID, Guid userid, int quantity)
    {
        if (quantity <= 0)
        {
            // remove item 
            RemoveItemFromCart(cartID, userid);
            return;

        }
        CART_ITEM item = GetCartItemByID(cartID, userid);
        if (item == null)
            return;


        // get product
        PRODUCT prod = GetProductByID(item.PRODUCT_ID);
        if (prod == null)
        {
            // remove item 
            RemoveItemFromCart(cartID, userid);
            return;
        }
        if (!prod.ACTIVE)
        {
            // remove item 
            RemoveItemFromCart(cartID, userid);
            return;
        }

        decimal prodPrice = prod.GetPriceWithDiscount();
        decimal prodPriceNoTax = prod.GetPriceNoTaxWithDiscount();
        decimal attrPrice = GetCartItemAttributePrice(item.CartItemID);

        // force price from attribute
        if (attrPrice > -1)
        {
            prodPrice = prod.GetPriceWithDiscount(attrPrice);
            prodPriceNoTax = prod.GetPriceNoTaxWithDiscount(attrPrice);
        }

        decimal total = 0;
        decimal total_no_tax = 0;
        decimal tax = 0;
        item.Quantity = quantity;
        //total = prod.PRICE * item.Quantity;
        //total_no_tax = (decimal)prod.PRICE_NO_TAX * item.Quantity;
        total = prodPrice * item.Quantity;
        total_no_tax = prodPriceNoTax * item.Quantity;

        tax = total - total_no_tax;
        item.Price = total_no_tax;
        item.Tax = tax;


        // remove cache
        SM.EM.Caching.Remove.CartItemList(userid);

    }

    public List<Data.ShoppingCartItem> UpdateCartItemUser(Guid oldUser, Guid newUser, Guid? pageID, Guid? orderID)
    {
        var list = GetCartItemList(oldUser, LANGUAGE.GetDefaultLang(), pageID, null );
        // delete old list
        ProductRep prep = new ProductRep();
        foreach (Data.ShoppingCartItem item in list)
        {
            prep.RemoveItemFromCart(item.CartItemID, item.UserId);
        }
        prep.Save();

        foreach (Data.ShoppingCartItem  item in list) { 
            
            List<Data.AttributeItem> attrList = new List<Data.AttributeItem>();
            foreach (Data.ItemGroupAttribute att in item.AttrList) { 
                attrList.Add( new Data.AttributeItem{ ID= att.GroupAttID.Value , Value = att.AttributeID } );
            }
            // insert new item
            prep.AddItemToCart(item.ProductID, newUser, item.Quantity, attrList, orderID);
            // remove old item
            //RemoveItemFromCart(item.CartItemID, item.UserId);
        }
        prep.Save();

        // remove cache
        SM.EM.Caching.Remove.CartItemList(oldUser);
        SM.EM.Caching.Remove.CartItemList(newUser);


        return list;

    }

    public void UpdateCartItemOrder(Guid user, Guid? pageID, Guid? oldOrderID, Guid? newOrderID)
    {
        var list = from ci in db.CART_ITEMs
                   from p in db.PRODUCTs
                   where ci.PRODUCT_ID == p.PRODUCT_ID && p.PageId == pageID &&
                   ci.UserId == user && ci.OrderID == oldOrderID
                   select ci;
        foreach (var item in list) {
            item.OrderID = newOrderID;
        }
        db.SubmitChanges();
            
        // remove cache
        SM.EM.Caching.Remove.CartItemList(user);
    }
    

    public void RemoveItemFromCart(Guid cartid, Guid userid) {
        // remove cache
        SM.EM.Caching.Remove.CartItemList(userid);

        CART_ITEM item = GetCartItemByID(cartid, userid );
        if (item == null)
            return;

        // remove item attributes
        DeleteCartItemAttributes(item.CartItemID);
        // remove item
        db.CART_ITEMs.DeleteOnSubmit(item);


    }
    public void DeleteCartItemAttributes(Guid cartid) {
        var attrs = from a in db.CART_ITEM_ATTRIBUTEs where a.CartItemID == cartid select a;
        db.CART_ITEM_ATTRIBUTEs.DeleteAllOnSubmit(attrs);
    }
    public decimal InsertCartItemAttributes(Guid cartid, List<Data.AttributeItem> attrList, Guid? pid = null )
    {
        decimal ret = -1;
        ProductRep prep = new ProductRep();
        foreach (Data.AttributeItem ai in attrList)
        {
            var ci = new CART_ITEM_ATTRIBUTE { CartItemID = cartid, GroupAttID = ai.ID, AttributeID = ai.Value, Value = ai.ValSet };
            db.CART_ITEM_ATTRIBUTEs.InsertOnSubmit(ci);

            // update price
            if (pid != null)
            {
                var attr = prep.GetProductAttributeByID(pid.Value , ai.Value);
                if (attr != null && attr.Price != null)
                {
                    if (ret == -1)
                        ret = 0;
                    ci.PriceAttribute = attr.Price.Value;
                    ret += attr.Price.Value;
                }
            }


        }
        return ret;
    }

    public decimal GetCartItemAttributePrice(List<Data.AttributeItem> attrList, Guid pid)
    {
        decimal ret = -1;
        ProductRep prep = new ProductRep();
        foreach (Data.AttributeItem ai in attrList)
        {

            // update price
//            if (pid != null)
            {
                var attr = prep.GetProductAttributeByID(pid, ai.Value);
                if (attr != null && attr.Price != null)
                {
                    if (ret == -1)
                        ret = 0;
                    ret += attr.Price.Value;
                }
            }


        }
        return ret;
    }

    public decimal GetCartItemAttributePrice(Guid cartID)
    {
        decimal ret = -1;
        var item = (from ci in db.CART_ITEM_ATTRIBUTEs
                    where ci.CartItemID == cartID
                    && ci.PriceAttribute != null
                    select ci).Sum(w => w.PriceAttribute);
        if (item == null || item.Value <= 0)
            return ret;

        return item.Value;
    }

    public PRODUCT_DESC UpdateProductDesc(Guid prodID, string langID, string title, string descLong, string descShort)
    {

        PRODUCT_DESC pdesc = GetProductDescByID (prodID, langID );
        if (pdesc  == null)
        {
            pdesc = new PRODUCT_DESC { PRODUCT_ID = prodID, LANG_ID = langID};
            AddDesc(pdesc);
        }
        pdesc.NAME = title;
        if (descLong != null)
            pdesc.DESCRIPTION_LONG = descLong;

        if (descShort != null)
        pdesc.DESCRIPTION_SHORT = descShort;

        return pdesc;

    }

    public PRODUCT_DESC UpdateProductDescLong(Guid prodID, string langID, string descLong)
    {

        PRODUCT_DESC pdesc = GetProductDescByID(prodID, langID);
        if (pdesc == null)
        {
            pdesc = new PRODUCT_DESC { PRODUCT_ID = prodID, LANG_ID = langID };
            pdesc.NAME = "";
            AddDesc(pdesc);
        }
        
        pdesc.DESCRIPTION_LONG = descLong;

        return pdesc;

    }

    //public void Delete(Dinner dinner)
    //{
    //    db.RSVPs.DeleteAllOnSubmit(dinner.RSVPs);
    //    db.Dinners.DeleteOnSubmit(dinner);
    //}

    //
    // Persistence

    public bool  Save()
    {
        try
        {
            db.SubmitChanges();
        }
        catch 
        {

            return false;
        }
        return true;        
    }


    public static decimal  CalculateTax(decimal price, decimal rate){

        return price * 0.2m;    
    
    }


    public class PRODUCT_DESC_WRAP : PRODUCT_DESC { }



    public class MANUFACTURER_COUNT : MANUFACTURER 
    {
        public int Num { get; set; }

    }



    public class FiltFloat
    {
        public double  Value { get; set; }
        public int Num { get; set; }
        public Guid PageID { get; set; }

    }

    public class FiltShort
    {
        public short Value { get; set; }
        public int Num { get; set; }
        public Guid PageID { get; set; }

    }

    public class FiltInt
    {
        public int Value { get; set; }
        public int Num { get; set; }
        public Guid PageID { get; set; }

    }


    public class Common
    {



    }

    public class Data
    {
        public class wAttrubute : ATTRIBUTE {


            public int GetMin
            {
                get
                {
                    int r = this.ValMin ?? -1;
                    if (r < 0) r = 0;
                    return r;
                }
            }
            public int GetMax
            {
                get
                {
                    int r = this.ValMax ?? -1;
                    if (r < 0) r = 9999;
                    return r;
                }
            }
            public int GetDefault
            {
                get
                {
                    int r = this.ValDefault ?? -1;
                    if (r < 0) r = this.GetMin;
                    return r;
                }
            }

            public bool IsDisabled { get { return (GetMin == GetMax  ); } }
        }


        public class wProductAttributeDesc : PRODUCT_ATTRIBUTE
        {
            public wProductAttributeDesc()
            {                
            }

            public wProductAttributeDesc(PRODUCT_ATTRIBUTE pa, PRODUCT_ATTRIBUTE_DESC pd)
                : this(pa, pd, null)
            {

            }

            public wProductAttributeDesc( PRODUCT_ATTRIBUTE pa, PRODUCT_ATTRIBUTE_DESC pd, ATTRIBUTE a) {
                this.PRODUCT_ID = pa.PRODUCT_ID;
                this.AttributeID = pa.AttributeID;
                this.ValMin = pa.ValMin;
                this.ValMax = pa.ValMax;
                this.ValDefault = pa.ValDefault;
                this.Price = pa.Price;
                this.Code = pa.Code;
                this.UnitsInStock = pa.UnitsInStock ;

                

                if (pd != null) {
                    this.Title = pd.Title;
                    this.Desc = pd.Desc;
                }
                if (a != null) {
                    GroupAttID = a.GroupAttID;
                    this.Ordr = a.Ordr;
                }
            } 


            public string Title { get; set; }
            public string Desc { get; set; }
            public Guid?  GroupAttID { get; set; }
            public int Ordr { get; set; }

        }


        public class GroupAttributeLang {
            public Guid GroupAttID { get; set; }
            public Guid? PageId { get; set; }
            public string Title { get; set; }
            public string Desc { get; set; }
            public string Message { get; set; }
            public string OrderByField { get; set; }
            public short GrType { get; set; }
            public int Order { get; set; }

        
        }



        public class AttributeItem
        {
            public Guid ID;
            public Guid Value;
            public int? ValMin { get; set; }
            public int? ValMax { get; set; }
            public int? ValDefault { get; set; }
            public int? ValSet { get; set; }
            public decimal? Price { get; set; }
            public string  Title { get; set; }
            public string Desc { get; set; }
            public string Code { get; set; }
            public int? Stock { get; set; }
            public int? ImgID { get; set; }

        }

        public class ItemGroupAttribute
        {

            public Guid? GroupAttID { get; set; }
            public Guid AttributeID { get; set; }
            public string GroupTitle { get; set; }
            public string GroupDesc { get; set; }
            public string GroupMessage { get; set; }
            public string Title { get; set; }
            public string Desc { get; set; }
            public string Code { get; set; }
            public string UrlBig { get; set; }
            public string UrlSmall { get; set; }
            public int? Value { get; set; }
            public short GrType { get; set; }
            public string PTitle { get; set; }
            public string PDesc { get; set; }
            public decimal? Price { get; set; }
            //public int? Stock { get; set; }
            //public int? ImgID { get; set; }

        }

        public class ShoppingCartItem
        {
            public Guid CartItemID { get; set; }
            public Guid ProductID { get; set; }
            public Guid UserId { get; set; }
            public Guid PageId { get; set; }
            public string Lang { get; set; }
            public DateTime DateAdded { get; set; }
            public int Quantity { get; set; }
            public decimal Price { get; set; }
            public decimal PriceTotal { get; set; }
            public decimal Tax { get; set; }
            public double  TaxRate { get; set; }
            public List<ItemGroupAttribute> AttrList { get; set; }
            public string ProductTitle { get; set; }
            public string ProductUrl { get; set; }
            public string Code { get; set; }
            //public int? Stock { get; set; }
            public int? ImgID { get; set; }
        }


    }

}