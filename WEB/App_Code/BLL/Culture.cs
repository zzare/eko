﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

/// <summary>
/// Summary description for Language
/// </summary>
public partial class CULTURE
{

    public static string GetCurrentCulture()
    {

        return "sl-SI";
    }



    public static IEnumerable<CULTURE> GetCulturesAll()
    {
        eMenikDataContext db = new eMenikDataContext();
        IEnumerable<CULTURE> smap;

        smap = from l in db.CULTUREs
               orderby l.ORDER 
               select l ;
        return smap;
    }
    public static List<CULTURE> GetUserCulturesInvert(Guid userId)
    {
        eMenikDataContext db = new eMenikDataContext();
        
        return db.em_GetUserCulturesInvert(userId ).ToList();
    }

    public static CULTURE GetCultureByID(int lcid)
    {
        eMenikDataContext db = new eMenikDataContext();

        return db.CULTUREs.SingleOrDefault(w=>w.LCID ==  lcid);
    }
    public static CULTURE GetCulture(string culture)
    {

        // check cache
        if (SM.EM.Globals.Settings.ST.EnableCaching)
        {
            // if user in cache, return it
            if (SM.EM.BLL.BizObject.Cache[SM.EM.Caching.Key.Culture(culture )] != null)
                return (CULTURE)SM.EM.BLL.BizObject.Cache[SM.EM.Caching.Key.Culture(culture)];
        }


        eMenikDataContext db = new eMenikDataContext();

        CULTURE ret=  db.CULTUREs.SingleOrDefault(w => w.UI_CULTURE == culture );

        // cache
        if (SM.EM.Globals.Settings.ST.EnableCaching)
            SM.EM.BLL.BizObject.CacheData(SM.EM.Caching.Key.Culture(culture), ret);

        return ret;

    }

    public static List<vw_UserCulture> GetUserCultures(Guid userId) {
        eMenikDataContext db = new eMenikDataContext();

        return db.em_GetUserCultures(userId).ToList();    
    }
    


    public static void InsertUserCulture(Guid userId, int lcid, bool isDefault, bool active) {
        eMenikDataContext db = new eMenikDataContext();
        EM_USER_CULTURE c = new EM_USER_CULTURE();
        
        db.EM_USER_CULTUREs.InsertOnSubmit(c);

        c.LCID = lcid;
        c.UserId = userId;
        c.DEFAULT = isDefault;
        c.ACTIVE = active;

        db.SubmitChanges();
    }
    public static void DeleteUserCulture(Guid userId, int lcid)
    {
        eMenikDataContext db = new eMenikDataContext();
        EM_USER_CULTURE c = db.EM_USER_CULTUREs.SingleOrDefault(w=>w.UserId == userId && w.LCID == lcid );

        if (c != null)
        {
            db.EM_USER_CULTUREs.DeleteOnSubmit(c);
            db.SubmitChanges();
        }
    }
    public static void SetDefaultUserCulture(Guid userId, int lcid)
    {
        eMenikDataContext db = new eMenikDataContext();
        List<EM_USER_CULTURE> list = db.EM_USER_CULTUREs.Where(w => w.UserId == userId ).ToList();
        foreach (EM_USER_CULTURE uc in list) {
            uc.DEFAULT = false;        
        }
        EM_USER_CULTURE c = db.EM_USER_CULTUREs.SingleOrDefault(w => w.UserId == userId && w.LCID == lcid);
        c.DEFAULT = true;

        db.SubmitChanges();
    }
    public static CULTURE  GetDefaultUserCulture(Guid userId)
    {
        eMenikDataContext db = new eMenikDataContext();

        CULTURE cul = (from c in db.CULTUREs join uc in db.EM_USER_CULTUREs on c.LCID equals uc.LCID where uc.DEFAULT == true && uc.UserId == userId select c).SingleOrDefault();

        return cul;
    }
    public static CULTURE GetDefaultUserCulture(string pageName)
    {
        eMenikDataContext db = new eMenikDataContext();

        //CULTURE cul = (from c in db.CULTUREs from uc in db.EM_USER_CULTUREs from em in db.EM_USERs where em.PAGE_NAME == pageName && uc.UserId == em.UserId && c.LCID == uc.LCID && uc.DEFAULT == true select c).SingleOrDefault();

        //if (cul == null)
        //    (from c in db.CULTUREs from uc in db.EM_USER_CULTUREs from em in db.EM_USERs where em.PAGE_NAME == pageName && uc.UserId == em.UserId && c.LCID == uc.LCID select c).FirstOrDefault();
        CULTURE cul = (from c in db.CULTUREs from uc in db.EM_USER_CULTUREs from em in db.EM_USERs where em.PAGE_NAME == pageName && uc.UserId == em.UserId && c.LCID == uc.LCID  orderby uc.DEFAULT descending  select c).FirstOrDefault();

        return cul;
    }

}
