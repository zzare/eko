﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_cntLastExampleTemplateList.ascx.cs" Inherits="_ctrl_main_page_cntLastExampleTemplateList" %>


<asp:ListView ID="lvList" runat="server">

    <LayoutTemplate>
        <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
    </LayoutTemplate>

    <ItemTemplate>
        <a href='<%# Page.ResolveUrl( SM.EM.Rewrite.EmenikUserUrl(Eval("PAGE_NAME").ToString() )) %>'><%# Eval("PAGE_NAME")%></a>
            
    </ItemTemplate>
    
    <ItemSeparatorTemplate>
        <br />
    </ItemSeparatorTemplate>

</asp:ListView>