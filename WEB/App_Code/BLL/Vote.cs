﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

/// <summary>
/// Summary description for VOTE
/// </summary>
public partial class VOTE
{
    /* VOTE IP */
    public bool CanVote(string ip)
    {
//        if (this.BlockMode == Common.BlockMode.IP)
 //       {

            eMenikDataContext db = new eMenikDataContext();
            return !db.VOTE_IPADDRESSes.Where(w => w.VoteID == this.VoteID && w.IPAddress == ip).Any();
        //}
        //else if (this.BlockMode == Common.BlockMode.NONE)
        //{
        //    return true;
        //}
        return false;
    }
    public decimal Average { get {

        if (this.TotalRating == null || this.Votes == null)
            return 0;
        if (this.TotalRating == 0 || this.Votes == 0)
            return 0;

        return Convert.ToDecimal(this.TotalRating) / Convert.ToDecimal(this.Votes);

    
    } }

    public static  decimal GetAverage(int? total, int? votes)
    {
        if (total == null || votes == null)
            return 0;
        if (total == 0 || votes == 0)
            return 0;
        
        return Convert.ToDecimal(total) / Convert.ToDecimal(votes);
    }


    public static VOTE GetVoteByID(Guid  id)
    {
        eMenikDataContext db = new eMenikDataContext();

        return db.VOTEs.Where(w => w.VoteID == id).SingleOrDefault();
    }

    public static int GetCommentsCount(int refInt)
    {
        eMenikDataContext db = new eMenikDataContext();

        var list = db.COMMENTs.Where(w => w.RefInt == refInt);
        list = list.Where(w => w.Active == true);

        return list.Count();
    }


    public static VOTE CastVote(Guid id, int num, string ip) {
        eMenikDataContext db = new eMenikDataContext();
        VOTE vote = db.VOTEs.SingleOrDefault(w => w.VoteID == id);
        if (vote == null)
        {
            vote = new VOTE { VoteID = id, Votes = 0, _TotalRating = 0 };
            db.VOTEs.InsertOnSubmit(vote);
        }

        if (num > 10)
            num = 10;
        if (num < 1)
            num = 1;

        // vote if not yet
        if (vote.CanVote(ip))
        {

            vote.Votes = vote.Votes + 1;
            vote.TotalRating = vote.TotalRating + num;

            db.SubmitChanges();

            SaveVoteIP(vote.VoteID, ip);
        }
        
        return vote;    
    }

    public static void SaveVoteIP(Guid id, string ipaddress)
    {
        eMenikDataContext db = new eMenikDataContext();
        VOTE_IPADDRESS ip = db.VOTE_IPADDRESSes.SingleOrDefault(w => w.VoteID == id && w.IPAddress == ipaddress);
        if (ip != null)
            return;

        ip = new VOTE_IPADDRESS { VoteID = id, IPAddress = ipaddress };
        db.VOTE_IPADDRESSes.InsertOnSubmit(ip);
        db.SubmitChanges();
    }



    public class Common { 
    
    
    
    }

    //partial void OnValidate(System.Data.Linq.ChangeAction action)
    //{
    //}

}
