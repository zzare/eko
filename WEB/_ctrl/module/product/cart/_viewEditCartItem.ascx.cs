﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _ctrl_module_product_cart_viewEditCartItem : BaseControlView
{

    public object Data;
    public object[] Parameters;
    public ProductRep.Data.ShoppingCartItem CartItem;


    protected void Page_Load(object sender, EventArgs e)
    {
        if (CartItem == null)
            LoadData();
    }



    public void LoadData()
    {

        if (Data == null)
        {
            //this.Visible = false;
            return;
        }
        CartItem = (ProductRep.Data.ShoppingCartItem )Data;
        if (CartItem == null)
            return;

        // load attributes
        objProductAttribute.Data = CartItem.ProductID;
        objProductAttribute.IsModeCMS = false;

        ProductRep prep = new ProductRep();
        objProductAttribute.ProductGroupList = prep.GetGroupAttributesByProduct(CartItem.ProductID, CartItem.Lang ).ToList();
//        objProductAttribute.ProductAttributeList = prep.GetAttributesByProduct(CartItem.ProductID).ToList();
        objProductAttribute.ProductAttributeList = prep.GetwProductAttributeDescsByProduct(CartItem.ProductID, CartItem.Lang).ToList();
        objProductAttribute.SelectedAttributes = (from a in CartItem.AttrList select new ProductRep.Data.AttributeItem { ID = a.GroupAttID.Value, Value = a.AttributeID }).ToList();
        objProductAttribute.LoadData();

    }






}
