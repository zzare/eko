﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_stepShoppingCart.ascx.cs" Inherits="_em_content_order__ctrl__stepShoppingCart" %>
<%@ Register Namespace="SM.EM.UI.Controls" TagPrefix="EM"  %>
<%@ Register TagPrefix="SM" TagName="ShoppingCart" Src="~/_ctrl/module/product/cart/_viewShoppingCart.ascx"   %>
<%@ Register TagPrefix="SM" TagName="EditModulePublic" Src="~/_ctrl/module/_edit/editModulePublic.ascx"   %>

<SM:EditModulePublic ID="objEditPublic" runat="server" />

<div class="stepHeaderW">
<h1><%= GetGlobalResourceObject("Product", "step0Title1") %></h1>
</div>


<div class="stepShoppingCart orderStep1" >
    <div class="contentRightW">
        <span class="contentRightHeadT_Text"><%= GetGlobalResourceObject("Product", "step0Title2") %></span>
            <div class="contentRightMI">
                <div id="stepShoppingCartList">
                    <SM:ShoppingCart ID="objShoppingCart" runat="server" />
                </div>
                <asp:CustomValidator ID="cvPayment" Display="Dynamic" runat="server" OnServerValidate="ValidateCart" ValidationGroup="valCart" ErrorMessage="* Košarica je prazna"></asp:CustomValidator>

                <div class="stepnextbuttonW">
                <a id="btNext" class="Astepnext" runat="server" onserverclick="btNext_Click" ><span><%= GetGlobalResourceObject("Product", "step0Next") %> &raquo;</span></a>
                <div class="clearing"></div>
                
                </div>
                <div class="clearing"></div>    
                <div class="stepSCL_paycardslogos">
                    <img alt="" src='<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/design/card_visa_verified.jpg") %>' />
                    <img alt="" src='<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/design/card_mastercard_securecode.jpg") %>' />
                </div>

                
<% if (LANGUAGE.GetCurrentLang() == "si"){%>
                <p>Minimalen nakup <%= SM.BLL.Custom.MinimumOrderSum.ToString() %> eurov.
                </p>
                <p>* Pri naročilih nad 200 eurov je dostava brezplačna kjerkoli v Sloveniji.</p>            
                <p>Dostava v Ljubljani in njeni okolici je brezplačna
                    <br />
                    Dostava v Novi Gorici in njeni okolici je brezplačna
                    <br />
                    V ostalih delih Slovenije je za dostavo potrebno doplačati 15 eurov.
                </p>

<%}else if (LANGUAGE.GetCurrentLang() == "en"){ %>  
                <p>Minimum purchase € <%= SM.BLL.Custom.MinimumOrderSum.ToString() %>.
                </p>
                <p>* If the purchase exceeds the value of € 200, the delivery to all regions of Slovenia is free of charge.</p>            
                <p>Free delivery to Ljubljana and its surroundings.
                    <br />
                    Free delivery to Nova Gorica and its surroundings.
                    <br />
                    For the delivery to other regions of Slovenia, we charge €15.
                </p>

<%}else if (LANGUAGE.GetCurrentLang() == "it"){ %>  
                <p>Acquisto minimo <%= SM.BLL.Custom.MinimumOrderSum.ToString() %> euro.
                </p>
                <p>* Nel caso l’acquisto superi il valore di 200 euro, la consegna è gratuita in tutte le regioni della Slovenia.</p>            
                <p>La consegna è gratuita a Lubiana e dintorni.
                    <br />
                    La consegna è gratuita a Nova Gorica e dintorni.
                    <br />
                    Per la consegna nelle altre regioni della Slovenia il costo del servizio è di 15 euro.
                </p>

<%} %>



                

                <%--<p>Dostava: Vsako sredo, izjemoma tudi v četrtek ( v primeru težav v prometu).</p>--%>
            </div>
    </div>
</div>



