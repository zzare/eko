﻿<%@ Page EnableEventValidation="false" Language="C#" MasterPageFile="~/CMS/CMS.master" AutoEventWireup="true" CodeFile="ManageCMS.aspx.cs" Inherits="SM.EM.UI.CMS.ManageCMS" Title="Untitled Page" %>

<%@ Register TagPrefix="EM" TagName="cmsContent" Src="~/cms/controls/_cmsContent.ascx" %>
<%@ Register TagPrefix="EM" TagName="cmsArticle" Src="~/cms/controls/_cmsArticle.ascx" %>
<%@ Register TagPrefix="EM" TagName="cmsUpdateProgress" Src="~/cms/controls/_cmsUpdateProgress.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


  
<asp:Content ID="Content1" ContentPlaceHolderID="cphNavL" Runat="Server">
    <asp:SiteMapDataSource ID="smpdsCMSEdit" runat="server" SiteMapProvider="CMSEdit" ShowStartingNode="false" />
    <asp:TreeView ID="tvSitemap" runat="server"  DataSourceID="smpdsCMSEdit" OnPreRender="tvSitemap_OnPreRender"
        SelectedNodeStyle-CssClass="tvwOrangeSelected" CssClass="tvwOrange" ShowLines="true" NodeWrap="true" >
        <DataBindings>
            <asp:TreeNodeBinding Depth="0" SelectAction="None"  NavigateUrlField="Url" TextField="Title" ToolTipField="Description"   />
            <asp:TreeNodeBinding NavigateUrlField="Url" TextField="Title" ToolTipField="Description"   />        
        </DataBindings>
    </asp:TreeView> 
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cph" Runat="Server" >

    <asp:UpdatePanel ID="upContent" runat="server" ChildrenAsTriggers="true"  >
        <ContentTemplate>
            <asp:Label ID="lbLanguage" runat="server" Text="Language:" CssClass="artEdit"></asp:Label>
            <asp:DropDownList ID="ddlLang" runat="server" AutoPostBack="true"  CausesValidation="false"
                AppendDataBoundItems="true" 
                onselectedindexchanged="ddlLang_SelectedIndexChanged">
                <asp:ListItem Text=" - show ALL - " Value="-1" ></asp:ListItem>
            </asp:DropDownList>
            <asp:CompareValidator ID="valLang" runat="server" ControlToValidate="ddlLang" ValueToCompare="-1" Operator="NotEqual"  ValidationGroup="insert" ErrorMessage="Please select language"></asp:CompareValidator>

            <cc1:TabContainer ID="tabModule" runat="server" ActiveTabIndex="0"  >
                <cc1:TabPanel ID="tpM1" HeaderText="Content" runat="server" Visible="false" >            
                    <ContentTemplate>
                        
                        <EM:cmsContent ID="cmsContent" runat="server" />
                    </ContentTemplate>
                </cc1:TabPanel>
                
                <cc1:TabPanel ID="tpM2" HeaderText="Article" runat="server" Visible="false">            
                    <ContentTemplate>
                        <EM:cmsArticle  ID="cmsArticle" runat="server" />
                    </ContentTemplate>
                </cc1:TabPanel>

<%--                <cc1:TabPanel ID="tpM3" HeaderText="Gallery" runat="server" Visible="false">            
                    <ContentTemplate>
                        <CMS:cmsGallery id="cmsGallery" runat="server" />
                    
                        
                    </ContentTemplate>--%>
                </cc1:TabPanel>
            </cc1:TabContainer>
        

        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="uppContent" runat="server" AssociatedUpdatePanelID="upContent" DisplayAfter="1000" >
        <ProgressTemplate>
        
        <EM:cmsUpdateProgress ID="upptProgress" runat="server" />
                    
        </ProgressTemplate>    
    </asp:UpdateProgress>
</asp:Content>

