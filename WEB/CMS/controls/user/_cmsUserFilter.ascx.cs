﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace SM.UI.Controls
{
    public partial class _cmsUserFilter : BaseControl 
    {
        public event EventHandler OnFilterChanged;

        public string SearchText { get { return tbSearch.Text.Trim(); } }
        public bool? Active
        {
            get
            {
                bool? ret = null;
                if (ddlActive.SelectedValue == "") return ret;

                if (ddlActive.SelectedValue == "0") return false;

                return true;
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {



        }

        public void LoadDefaults() {
            ddlActive.SelectedIndex = 0;
        
        
        }

        protected void ddlActive_SelectedIndexChanged(object sender, EventArgs e)
        {
            OnFilterChanged(this, new EventArgs());
        }


        protected void btSearch_Click(object sender, EventArgs e)
        {
            OnFilterChanged(this, new EventArgs());
        }
}
}