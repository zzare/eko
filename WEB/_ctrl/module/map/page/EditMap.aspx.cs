﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;

public partial class _ctrl_module_map_page_EditMap : System.Web.UI.Page
{
    public Guid CwpID
    {
        get
        {
            if (Request.QueryString["id"] == null)
                return Guid.Empty;

            if (!SM.EM.Helpers.IsGUID(Request.QueryString["id"])) return Guid.Empty;

            return new Guid(Request.QueryString["id"]);
        }
    }

    public int Width
    {
        get
        {
            if (Request.QueryString["w"] == null)
                return -1;

            int res;
            if (!int.TryParse(Request.QueryString["w"], out res))
                return -1;

            return res;
        }
    }
    protected string ThemeQS
    {
        get
        {
            return Request.QueryString["t"] ?? "";
        }
    }
    protected string ReturnUrl
    {
        get
        {
            if (Request.QueryString["ru"] == null) return "";
            return Server.UrlDecode( Request.QueryString["ru"] );
        }
    }

    protected string Key
    {
        get
        {
            if (Request.QueryString["k"] == null) return "";
            return Server.UrlDecode(Request.QueryString["k"]);
        }
    }

    protected override void OnPreInit(EventArgs e)
    {
        base.OnPreInit(e);

        if (ThemeQS != string.Empty)
            Page.Theme = ThemeQS;

    }
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!User.Identity.IsAuthenticated)
        //    return;

        // load scripts
        SM.EM.Helpers.RegisterScriptIncludeAJAX("http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js", this, "jQ");
        SM.EM.Helpers.RegisterScriptIncludeAJAX("http://maps.google.com/maps?file=api&v=2&sensor=true&key=" + SM.BLL.Common.Emenik.Data.APIkey(), this, "google.maps.api");

        SM.EM.Helpers.RegisterScriptIncludeAJAX("http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.min.js", this, "effects");
        //SM.EM.Helpers.RegisterScriptIncludeAJAX(Page.ResolveUrl("~/_em/_common/javascript/map.js"), this, "map");

        SM.EM.Helpers.AddLinkedStyleSheet(this, "http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.1/themes/base/jquery-ui.css", false);
        SM.EM.Helpers.AddLinkedStyleSheet(this, "~/_em/_common/css/Template.css");
                    

        // get id
        if (CwpID != Guid.Empty)
            LoadMap();


    }

    protected void LoadMap() {
        CWP_MAP map = CMS_WEB_PART.GetCwpMapByID(CwpID);
        if (map == null) return;

        CultureInfo usc = new CultureInfo("en-US");

        string init = "";

        double lat, lng, mLat, mLng;
        int zoom, height, width;

        lat = map.LAT;
        lng = map.LNG;
        mLat = map.MARKER_LAT ?? 0;
        mLng = map.MARKER_LNG ?? 0;
        zoom = map.ZOOM;
        height = map.HEIGHT ?? -1;
        width = map.WIDTH ?? -1;

        //string returnUrl = "";
        //if (ReturnUrl != string.Empty)
        //    returnUrl = " var ru = '" + ReturnUrl + "'; alert(ru); ";

        string srchAddr = map.SRCH_ADDRESS;

        if (string.IsNullOrEmpty(srchAddr.Trim()))
            srchAddr = "Ljubljana";



        init = string.Format(" map_edit('{0}', $('#map')[0], parseFloat('{2}'), parseFloat('{3}'), {4}, parseFloat('{5}'), parseFloat('{6}'), {7}, {8}, {9}, {10}, '{11}', {12}, '{13}'); ",
             map.CWP_ID,
             map.CWP_ID,
             string.Format(usc, "{0:F16}", lat),
             string.Format(usc, "{0:F16}", lng),
             zoom,
             string.Format(usc, "{0:F16}", mLat),
             string.Format(usc, "{0:F16}", mLng),
             SM.EM.Helpers.EncodeJsString(map.TITLE),
             SM.EM.Helpers.EncodeJsString(map.ADDRESS),
             height,
             Width ,
             srchAddr,
             map.SHOW_INFO.ToString().ToLower(),
             map.MAP_TYPE
             );


        //if (map.LAT == 0 || map.LNG == 0)
        //    //init += string.Format("showAddress('{1}', '{2}');", map.CWP_ID, map.SRCH_ADDRESS, map.TITLE, map.ADDRESS);
        //    init += string.Format("editID = '{0}'; showAddress('{1}', GetMarkerHtml('{2}', '{3}') );", map.CWP_ID, map.SRCH_ADDRESS, map.TITLE, map.ADDRESS);


        this.Page.ClientScript.RegisterStartupScript(this.Page.GetType(), "readyScript", "<script type=\"text/javascript\"> $(document).ready(function(){" +  init + " });</script>");

    
    }
}
