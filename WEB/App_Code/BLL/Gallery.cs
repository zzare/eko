﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

/// <summary>
/// Summary description for  GALLERY
/// </summary>
public partial class GALLERY
{

    public static GALLERY GetGalleryBySitemap(int smapID)
    {
        eMenikDataContext db = new eMenikDataContext();

        GALLERY gal = (from g in db.GALLERies join s in db.SITEMAP_GALLERies on g.GAL_ID equals s.GAL_ID where s.SMAP_ID == smapID orderby s.SMG_ORDER select g).FirstOrDefault();

        return gal;
    }
    public static GALLERY GetGalleryByID(int galID)
    {
        eMenikDataContext db = new eMenikDataContext();
        GALLERY gal = (from g in db.GALLERies where g.GAL_ID == galID select g).SingleOrDefault  ();
        return gal;
    }
    public static GALLERY GetGalleryByID(Guid  cwp)
    {
        eMenikDataContext db = new eMenikDataContext();
        GALLERY gal = (from g in db.GALLERies from c in db.CMS_WEB_PARTs  where c.CWP_ID == cwp && g.GAL_ID == c.ID_INT.Value   select g).SingleOrDefault();
        return gal;
    }

    public static GALLERY CreateGalleryForType(int galTypeID, string username, int smapID) {
        eMenikDataContext db = new eMenikDataContext();

        GALLERY gal = new GALLERY();

        if (!string.IsNullOrEmpty(username))
            gal.USERNAME = username;
        
        gal.GAL_TYPE = galTypeID;
        db.GALLERies.InsertOnSubmit(gal);
        db.SubmitChanges();

        // if smap is set, add gallery to sitemap
        if (smapID > 0) {
            AddGalleryToSitemap(gal.GAL_ID, smapID);        
        }

        // copy image types for gallery type
        List<GALLERY_TYPE_IMAGE_TYPE> list = db.GALLERY_TYPE_IMAGE_TYPEs.Where(w => w.GAL_TYPE == galTypeID).ToList();
        foreach (GALLERY_TYPE_IMAGE_TYPE item in list) {
            GALLERY_IMAGE_TYPE type = new GALLERY_IMAGE_TYPE { GAL_ID = gal.GAL_ID, TYPE_ID = item.TYPE_ID  };
            db.GALLERY_IMAGE_TYPEs.InsertOnSubmit(type);
        }
        db.SubmitChanges();
        
        return gal;
    }

   

    public static List<vw_GalleryDesc> GetGalleryDescsByID(int galID)
    {
        eMenikDataContext db = new eMenikDataContext();

        return (from l in db.LANGUAGEs join i in (from i in db.GALLERY_LANGs where i.GAL_ID == galID select i) on l.LANG_ID equals i.LANG_ID into outer from gal in outer.DefaultIfEmpty() orderby l.LANG_ORDER select new vw_GalleryDesc { GAL_DESC = gal.GAL_DESC, GAL_ID  = gal.GAL_ID, GAL_TITLE  = gal.GAL_TITLE, LANG_DESC = l.LANG_DESC, LANG_ID = l.LANG_ID }).ToList();
    }
    public static List<vw_GalleryDesc> GetGalleryDescsByIDEmenik(int galID, string username)
    {
        eMenikDataContext db = new eMenikDataContext();

        return (from em in db.EM_USERs
                from uc in db.EM_USER_CULTUREs
                from c in db.CULTUREs 
                join i in
                    (from i in db.GALLERY_LANGs where i.GAL_ID == galID select i) on c.LANG_ID equals i.LANG_ID into outer
                from gal in outer.DefaultIfEmpty()
                from l in db.LANGUAGEs
                where em.USERNAME == username && em.UserId == uc.UserId && c.LCID == uc.LCID && l.LANG_ID == c.LANG_ID 
                orderby l.LANG_ORDER select new vw_GalleryDesc { GAL_DESC = gal.GAL_DESC, GAL_ID = gal.GAL_ID, GAL_TITLE = gal.GAL_TITLE, LANG_DESC = l.LANG_DESC, LANG_ID = l.LANG_ID }).ToList();
    }
    public static void SaveGalleryDesc(int galID, string langID, string title, string desc)
    {
        eMenikDataContext db = new eMenikDataContext();

        GALLERY_LANG  g = (from i in db.GALLERY_LANGs  where i.GAL_ID == galID && i.LANG_ID == langID select i).SingleOrDefault();
        if (g == null)
        {
            g = new GALLERY_LANG { GAL_ID  = galID, LANG_ID = langID, GAL_TITLE = title, GAL_DESC = desc };
            db.GALLERY_LANGs.InsertOnSubmit(g);
        }
        else
        {
            g.GAL_TITLE = title;
            g.GAL_DESC = desc;
        }

        db.SubmitChanges();
    }

    public static void  AddImageToGallery(int galID, int imgID)
    {
        eMenikDataContext db = new eMenikDataContext();

        IMAGE_GALLERY gal = new IMAGE_GALLERY {  GAL_ID=galID, IMG_ID = imgID, IMG_ORDER=1000 };

        db.IMAGE_GALLERies.InsertOnSubmit(gal);
        db.SubmitChanges();

    }

    public static void AddGalleryToSitemap(int galID, int smpID)
    {
        eMenikDataContext db = new eMenikDataContext();

        SITEMAP_GALLERY gal = new SITEMAP_GALLERY { GAL_ID = galID, SMAP_ID = smpID  };

        db.SITEMAP_GALLERies.InsertOnSubmit(gal);
        db.SubmitChanges();

    }

    public static void DeleteimageTypesForGalleryType(int galTypeID)
    {
        eMenikDataContext db = new eMenikDataContext();
        List<GALLERY_TYPE_IMAGE_TYPE> list = (from stc in db.GALLERY_TYPE_IMAGE_TYPEs where stc.GAL_TYPE == galTypeID  select stc).ToList();
        db.GALLERY_TYPE_IMAGE_TYPEs.DeleteAllOnSubmit(list);
        db.SubmitChanges();
    }
    public static void DeleteImageTypeForGalleryType(int galTypeID, int imgType)
    {
        eMenikDataContext db = new eMenikDataContext();
        GALLERY_TYPE_IMAGE_TYPE  g = (from stc in db.GALLERY_TYPE_IMAGE_TYPEs where stc.GAL_TYPE == galTypeID && stc.TYPE_ID == imgType  select stc).SingleOrDefault();
        if (g == null) return;
        db.GALLERY_TYPE_IMAGE_TYPEs.DeleteOnSubmit (g);
        db.SubmitChanges();
    }


    public static void InsertImageTypeForGalleryType(int imgTypeID, int galTypeID) {

        eMenikDataContext db = new eMenikDataContext();
        GALLERY_TYPE_IMAGE_TYPE gti = db.GALLERY_TYPE_IMAGE_TYPEs.SingleOrDefault(w => w.TYPE_ID == imgTypeID && w.GAL_TYPE == galTypeID);
        if (gti != null) return;

        gti = new GALLERY_TYPE_IMAGE_TYPE { TYPE_ID = imgTypeID, GAL_TYPE = galTypeID };

        db.GALLERY_TYPE_IMAGE_TYPEs.InsertOnSubmit(gti);

        db.SubmitChanges();

        //insert image type for all galleries
        List<GALLERY> list = db.GALLERies.Where(w => w.GAL_TYPE == galTypeID).ToList();
        foreach (GALLERY item in list) {
            AddImageTypeToGallery(imgTypeID, item.GAL_ID);
        }

    
    }

    public static void AddImageTypeToGallery(int imgTypeID, int galID)
    {

        eMenikDataContext db = new eMenikDataContext();
        GALLERY_IMAGE_TYPE gi = db.GALLERY_IMAGE_TYPEs.SingleOrDefault(w => w.GAL_ID == galID && w.TYPE_ID == imgTypeID);

        if (gi != null) return;

        gi = new GALLERY_IMAGE_TYPE { TYPE_ID = imgTypeID, GAL_ID = galID  };

        // insert new image type for gallery
        db.GALLERY_IMAGE_TYPEs.InsertOnSubmit(gi);
        db.SubmitChanges();

        // insert new images for new type (for existing images in gallery)
        IMAGE.Func.AutoCreateImagesForGalleryForType(galID, imgTypeID);
    }

    public static bool ExistImageTypeForGalleryType(int imgTypeID, int galTypeID) {
        eMenikDataContext db = new eMenikDataContext();
        GALLERY_TYPE_IMAGE_TYPE res = (from stc in db.GALLERY_TYPE_IMAGE_TYPEs where stc.TYPE_ID == imgTypeID && stc.GAL_TYPE  == galTypeID  select stc).SingleOrDefault();
        return (res != null);
    }

    public static int GetImageTypeFromGalleryDefault(int galID){
        eMenikDataContext db = new eMenikDataContext();

        var type = (from it in db.IMAGE_TYPEs where it.TYPE_ID == (from t in db.IMAGE_TYPEs join g in db.GALLERY_IMAGE_TYPEs on t.TYPE_ID equals g.TYPE_ID where g.GAL_ID == galID select t.ORIG_WIDTH  ).Max() select it.TYPE_ID).SingleOrDefault();

        if (type == null || type < 1 )   return  SM.BLL.Common.ImageType.CMSthumb;

        return type;
    }

    public static List<IMAGE_TYPE> GetImageTypesFromGallery(int galID)
    {

        eMenikDataContext db = new eMenikDataContext();
        List<IMAGE_TYPE> list = (from it in db.IMAGE_TYPEs from g in db.GALLERY_IMAGE_TYPEs where g.GAL_ID == galID && g.TYPE_ID == it.TYPE_ID orderby it.TYPE_TITLE select it).ToList();
        return list;
    }
    public static List<IMAGE_TYPE> GetImageTypesFromGalleryInvert(int galID)
    {

        eMenikDataContext db = new eMenikDataContext();
        List<IMAGE_TYPE> list = (from it in db.IMAGE_TYPEs where !(from g in db.GALLERY_IMAGE_TYPEs where g.GAL_ID == galID && g.TYPE_ID == it.TYPE_ID select 1).Contains(1) orderby it.TYPE_TITLE select it).ToList();
        return list;
    }
    public static IEnumerable<IMAGE_TYPE> GetImageTypesFromGalleryByType(int galID, short ttype)
    {

        eMenikDataContext db = new eMenikDataContext();
        var list = (from it in db.IMAGE_TYPEs from g in db.GALLERY_IMAGE_TYPEs where g.GAL_ID == galID && g.TYPE_ID == it.TYPE_ID && it.T_TYPE == ttype  orderby it.TYPE_TITLE select it);
        return list;
    }

    public static IQueryable <IMAGE_TYPE> GetImageTypesFromGalleryTypeByType(int galTypeID, short ttype)
    {

        eMenikDataContext db = new eMenikDataContext();
        var list = (from it in db.IMAGE_TYPEs from g in db.GALLERY_TYPE_IMAGE_TYPEs where g.GAL_TYPE == galTypeID && g.TYPE_ID == it.TYPE_ID && it.T_TYPE == ttype orderby it.TYPE_TITLE select it);
        return list;
    }



    public static List<IMAGE_ORIG> GetImageOrigsFromGallery(int galID)
    {
        eMenikDataContext db = new eMenikDataContext();
        List<IMAGE_ORIG> list = (from i in db.IMAGE_ORIGs from g in db.IMAGE_GALLERies where g.GAL_ID == galID && g.IMG_ID  == i.IMG_ID  orderby g.IMG_ORDER select i).ToList();
        return list;
    }
    public static IEnumerable <IMAGE_ORIG> GetImageOrigsFromGalleryWithoutType(int galID, int typeID)
    {
        eMenikDataContext db = new eMenikDataContext();
        var list = (from i in db.IMAGE_ORIGs 
                    from g in db.IMAGE_GALLERies 
                    where g.GAL_ID == galID && g.IMG_ID == i.IMG_ID 
                    && !(from it in db.IMAGEs where it.IMG_ID == i.IMG_ID && it.TYPE_ID == typeID select it.IMG_ID ).Contains(i.IMG_ID)
                    orderby g.IMG_ORDER select i);

        return list;
    }

    public static IQueryable <IMAGE_ORIG> GetImageOrigsFromCWPArticleWithoutType(Guid cwpID, int typeID)
    {
        eMenikDataContext db = new eMenikDataContext();
        var list = (from a in db.ARTICLEs
                    from cwpg in db.CMS_WEB_PARTs
                    from i in db.IMAGE_ORIGs
                    from g in db.IMAGE_GALLERies
                    where a.CWP_ID == cwpID && cwpg.REF_ID == a.ART_GUID && g.GAL_ID == cwpg.ID_INT && g.IMG_ID == i.IMG_ID
                    && !(from it in db.IMAGEs where it.IMG_ID == i.IMG_ID && it.TYPE_ID == typeID select it.IMG_ID).Contains(i.IMG_ID)
                    orderby g.IMG_ORDER
                    select i);

        return list;
    }

    public static List<vw_Image> GetImagesByGallery(int galID, int type)
    {
        eMenikDataContext db = new eMenikDataContext();
        List<vw_Image> list = (from i in db.vw_Images  from g in db.IMAGE_GALLERies where g.GAL_ID == galID && g.IMG_ID == i.IMG_ID && i.TYPE_ID == type orderby g.IMG_ORDER select i).ToList();
        return list;
    }


    public static IEnumerable<vw_Image > GetImageListByCwpPaged(Guid cwp, bool isEditMode, int pageNum, int pageSize)
    {
        eMenikDataContext db = new eMenikDataContext();

        IEnumerable<vw_Image> list;
        int skip = (pageNum-1) * pageSize;

        list = (from i in db.vw_Images 
                from ig in db.IMAGE_GALLERies  
                from g in db.GALLERies 
                from c in db.CWP_GALLERies 
                from cw in db.CMS_WEB_PARTs 
                where cw.CWP_ID == cwp && cw.CWP_ID == c.CWP_ID && g.GAL_ID == cw.ID_INT   && g.GAL_ID  == g.GAL_ID  && ig.GAL_ID == g.GAL_ID && i.IMG_ID == ig.IMG_ID && i.TYPE_ID == c.THUMB_TYPE_ID
                orderby ig.IMG_ORDER ascending, i.DATE_ADDED descending
                select i);

        if (!isEditMode)
            list = list.Where(w => w.IMG_ACTIVE == true);

        return list.Skip(skip).Take(pageSize);
    }

    public static IQueryable<vw_ImageLang> GetImageListByCwpPaged(Guid cwp, string lang, bool isEditMode, int pageNum, int pageSize)
    {
        return GetImageListByCwpPaged(cwp, lang, isEditMode, pageNum, pageSize, null);

    }

    public static IQueryable<vw_ImageLang> GetImageListByCwpPaged(Guid cwp, string lang, bool isEditMode, int pageNum, int pageSize, int? imgType)
    {
        eMenikDataContext db = new eMenikDataContext();

        IQueryable<vw_ImageLang> list;
        int skip = (pageNum - 1) * pageSize;

        list = (from i in db.vw_Images
                join il in db.IMAGE_LANGs on i.IMG_ID equals il.IMG_ID into oil
                from il in
                    (from s in oil where s.LANG_ID == lang select s).DefaultIfEmpty()
                from ig in db.IMAGE_GALLERies
                from c in db.CWP_GALLERies
                from cw in db.CMS_WEB_PARTs
                where cw.CWP_ID == cwp && cw.CWP_ID == c.CWP_ID && ig.GAL_ID == cw.ID_INT && i.IMG_ID == ig.IMG_ID && i.TYPE_ID == (imgType ?? c.THUMB_TYPE_ID)
                orderby ig.IMG_ORDER ascending, i.DATE_ADDED descending 
                select new vw_ImageLang
                {
                    IMG_ID = i.IMG_ID,
                    TYPE_ID = imgType ?? c.THUMB_TYPE_ID.Value,
                    LANG_ID = lang,
                    IMG_ACTIVE = i.IMG_ACTIVE,
                    IMG_AUTHOR = i.IMG_AUTHOR,
                    IMG_TITLE = il.IMG_TITLE ?? "",
                    IMG_DESC = il.IMG_DESC,
                    IMG_NAME = i.IMG_NAME,
                    IMG_FOLDER = i.IMG_FOLDER,
                    IMG_URL = i.IMG_URL,
                    IMG_ORDER = i.IMG_ORDER,
                    DATE_ADDED = i.DATE_ADDED,
                    USERNAME = i.USERNAME,
                    TOTAL_RATING = i.TOTAL_RATING,
                    VOTES = i.VOTES
                });

        if (!isEditMode)
            list = list.Where(w => w.IMG_ACTIVE == true);
        if (!isEditMode)
            list = list.Skip(skip).Take(pageSize);
        return list;
    }


    public static IQueryable<WebServiceP.GalImage> GetImageListByRefID(Guid refID, string lang, int? imgTypeSmall, int? imgTypeBig)
    {
        eMenikDataContext db = new eMenikDataContext();

        IQueryable<WebServiceP.GalImage> list;

        list = from c in db.CMS_WEB_PARTs
               from cg in db.CWP_GALLERies
               from g in db.IMAGE_GALLERies
               join il in
                   (from desc in db.IMAGE_LANGs where desc.LANG_ID == lang select desc) on g.IMG_ID equals il.IMG_ID into outer
               from idesc in outer.DefaultIfEmpty()
               from th in db.IMAGEs
               from big in db.IMAGEs
               from i in db.IMAGE_ORIGs
               where c.REF_ID == refID && cg.CWP_ID == c.CWP_ID && g.GAL_ID == c.ID_INT && i.IMG_ID == g.IMG_ID
               && th.IMG_ID == g.IMG_ID && (big.IMG_ID == g.IMG_ID) && th.TYPE_ID == (imgTypeSmall ?? cg.THUMB_TYPE_ID) && big.TYPE_ID == (imgTypeBig ?? cg.BIG_TYPE_ID)
               orderby g.IMG_ORDER ascending, i.DATE_ADDED descending 
               select new WebServiceP.GalImage { Title = idesc.IMG_TITLE, Desc = idesc.IMG_DESC, UrlBig = big.IMG_URL, UrlThumb = th.IMG_URL, ImageID = i.IMG_ID  };


        return list;
    }

    public static IQueryable<WebServiceP.GalImage> GetImageListByCwpID(Guid cwpID, string lang, int? imgTypeSmall, int? imgTypeBig)
    {
        eMenikDataContext db = new eMenikDataContext();

        IQueryable<WebServiceP.GalImage> list;

        list = from c in db.CMS_WEB_PARTs
               from cg in db.CWP_GALLERies
               from g in db.IMAGE_GALLERies
               join il in
                   (from desc in db.IMAGE_LANGs where desc.LANG_ID == lang select desc) on g.IMG_ID equals il.IMG_ID into outer
               from idesc in outer.DefaultIfEmpty()
               from th in db.IMAGEs
               from big in db.IMAGEs
               from i in db.IMAGE_ORIGs 
               where c.CWP_ID == cwpID && cg.CWP_ID == c.CWP_ID && g.GAL_ID == c.ID_INT && i.IMG_ID == g.IMG_ID 
               && th.IMG_ID == g.IMG_ID && (big.IMG_ID == g.IMG_ID) && th.TYPE_ID == (imgTypeSmall ?? cg.THUMB_TYPE_ID) && big.TYPE_ID == (imgTypeBig ?? cg.BIG_TYPE_ID)
               orderby g.IMG_ORDER ascending, i.DATE_ADDED descending 
               select new WebServiceP.GalImage { Title = idesc.IMG_TITLE, Desc = idesc.IMG_DESC, UrlBig = big.IMG_URL, UrlThumb = th.IMG_URL };


        return list;
    }


    public class Common {
        public static string RenderOpenGalleryPopup(Guid cwp, int index)
        {
            //string ret = string.Format("OpenGalleryPopup('{0}', {1})", cwp.ToString(), index.ToString());
            string ret = @"OpenGalleryPopup(&#34;" + cwp.ToString() + "&#34;, " + index.ToString() + "); return false";
            return ret;

        }

        public static string RenderOpenGalleryPopupAnimal(Guid cwp, int index, string _this)
        {
            //string ret = string.Format("OpenGalleryPopup('{0}', {1})", cwp.ToString(), index.ToString());
            string ret = @"openGalleryAnimal(&#34;" + cwp.ToString() + "&#34;, " + index.ToString() + "," + _this  + "); return false";
            return ret;

        }
    
    
    
    }


}
