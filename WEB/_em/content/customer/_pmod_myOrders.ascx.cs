﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _em_content__customer_pmod_myOrders : BaseControl_EMENIK
{


    protected override void OnInit(EventArgs e)
    {

            
        base.OnInit(e);

        // request autenticated user
        if (!Page.User.Identity.IsAuthenticated)
            ParentPage.RequestCustomerLogin();

        // init cache
        ParentPage.OutputCacheDuration = -1;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        // load order
        var orderList = SHOPPING_ORDER.GetShoppingOrdersByCustomer(SM.EM.BLL.Membership.GetCurrentUserID()).OrderByDescending(o=>o.DateStart ).ToList();



        // load orders
        var active = from l in orderList where l.OrderStatus == SHOPPING_ORDER.Common.Status.CONFIRMED  select l ;
        objOrderListActive.PageID = ParentPage.em_user.UserId ;
        objOrderListActive.OrderList = active.ToList();
        objOrderListActive.LoadData();

        var finished = from l in orderList where l.OrderStatus > SHOPPING_ORDER.Common.Status.CONFIRMED && l.OrderStatus <= SHOPPING_ORDER.Common.Status.FINISHED select l;
        objOrderListActiveFinished.PageID = ParentPage.em_user.UserId;
        objOrderListActiveFinished.OrderList = finished .ToList();
        objOrderListActiveFinished.LoadData();



    }
}
