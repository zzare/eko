﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_cntLogin.ascx.cs" Inherits="_ctrl_content_membership_ctrl_cntLogin" %>
<%@ Register Namespace="SM.UI.Controls" TagPrefix="SM"  %>
<%@ Register TagPrefix="EM" TagName="MailConfirmation" Src="~/_mailTemplates/_ctrlNewUserVerificationMail.ascx"  %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<script type="text/javascript">
    $(document).ready(function () {
        $('.tbLoginPass').watermark('watermark');
    });

</script>


<asp:LoginView ID="LoginView1" runat="server">
                <AnonymousTemplate>
                    <div id="logInBox">                        
                        <asp:Login ID="Login1" runat="server" TitleText=""  OnLoginError="LoginError" OnLoggedIn="OnLogin"  >
                            <LayoutTemplate>
                                <asp:Panel ID="panLogin" runat="server" DefaultButton="LoginButton">


                                    <%--<div class="loginboxinner">--%>
                                    <div class="">



                                        <table class="TBloginbox" cellpadding="0" cellspacing="0" >

<tr>
    <td>
        <asp:TextBox ID="UserName" runat="server" CssClass="TBXlogin TBXlogin_new"></asp:TextBox>
        <asp:RequiredFieldValidator ID="UserNameRequired" CssClass="rfvUser" runat="server" ControlToValidate="UserName" Display="Dynamic"
            ErrorMessage="* Vpiši email." ToolTip="* Vpiši email." ValidationGroup="ctl00$ctl02$Login1"></asp:RequiredFieldValidator>
         <cc1:TextBoxWatermarkExtender ID="weUsername" runat="server" WatermarkCssClass="WMmainLogin_new"  WatermarkText="vpiši e-poštni naslov" TargetControlID ="UserName"></cc1:TextBoxWatermarkExtender>
          
    </td>
</tr>
<tr>
    <td>
        <asp:TextBox ID="Password" runat="server" TextMode="Password" CssClass="TBXlogin TBXlogin_new  tbLoginPass" title="vpiši geslo" ></asp:TextBox>
        <asp:RequiredFieldValidator ID="PasswordRequired" CssClass="rfvPass" runat="server" ControlToValidate="Password" Display="Dynamic" EnableClientScript="false"
            ErrorMessage="* Vpiši geslo." ToolTip="* Vpiši geslo." ValidationGroup="ctl00$ctl02$Login1"></asp:RequiredFieldValidator>
        <cc1:TextBoxWatermarkExtender ID="wePass" runat="server" WatermarkCssClass="WMmainLogin_new"  WatermarkText="vpiši geslo" TargetControlID ="Password"></cc1:TextBoxWatermarkExtender>
    </td>
</tr>
<tr>
<td>
            <div id="cLoginErr" runat="server" enableviewstate="false" class="divloginError" visible="false">
                <div><asp:Literal ID="litLoginError" runat="server" EnableViewState="false" ></asp:Literal>
                
                    <asp:PlaceHolder ID="phResend" runat="server" Visible ="false" EnableViewState="false" >
                        &nbsp; [<a id="btResend" runat="server" onserverclick="btResend_Click" >Ponovno pošlji potrditveni email</a>]
                    </asp:PlaceHolder>
                 </div>
            </div>

</td>
</tr>
<tr>
    <td>
        <SM:LinkButtonDefault ID="LoginButton" class="Astepnext" runat="server" CommandName="Login"   ValidationGroup="ctl00$ctl02$Login1"><span>VSTOPI</span></SM:LinkButtonDefault>
    </td>
</tr>
<tr>
    <td>
          <a class="btn_forgetpass"  href='<%= Page.ResolveUrl(SM.EM.Rewrite.EmenikCustomerForgotPass(ParentPage.em_user.UserId)) %>' ><%= GetGlobalResourceObject("Modules", "LoginForgotPass")%> &rsaquo;</a>
    </td>
</tr>
<%--                                            <tr>
                                                <td>E-pošta:</td>
                                                <td>Geslo:</td>
                                                <td></td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="UserName" runat="server" CssClass="TBXlogin"></asp:TextBox><asp:RequiredFieldValidator ID="UserNameRequired" CssClass="rfvUser" runat="server" ControlToValidate="UserName" Display="Dynamic"
                                                        ErrorMessage="User Name is required." ToolTip="User Name is required." ValidationGroup="ctl00$ctl02$Login1">*</asp:RequiredFieldValidator>
                                                </td>
                                                <td>

                                                </td>
                                                <td>

                                                </td>

                                            </tr>
                                            <tr>
                                                <td class="TDsec">
                                                    <asp:CheckBox CssClass="loginCbx" ID="loginCbx" runat="server" Text="&nbsp;Ostanite prijavljeni" />
                                                </td>
                                                <td class="TDsec">
                                                    <a  href='<%= Page.ResolveUrl(SM.EM.Rewrite.EmenikCustomerForgotPass(ParentPage.em_user.UserId)) %>' >Pozabljeno geslo</a>
                                                </td>
                                                <td class="TDsec"></td>
                                            </tr>
--%>
                                        </table>
                                    </div>
                                </asp:Panel>
                            </LayoutTemplate>
                        </asp:Login>
                    </div>
                </AnonymousTemplate>
                <LoggedInTemplate>
                    <div class ="loggedInBox">
                        <div class="loggedInBoxInner">
                            <span>
                                Dobrodošli&nbsp;&nbsp;
                                <asp:LoginName CssClass="loginName" ID="LoginName2" runat="server" />
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:LoginStatus CssClass="btLogout" LogoutText="Odjava" ID="LoginStatus1" runat="server" />
                            </span>
                            
                            <br />  
                            
                            <div class="loggedInBox_bottom">
                                 
                                <% if (Page.User.IsInRole("em_user"))
                                   {%>
                                <a href='<%= Page.ResolveUrl( SM.EM.Rewrite.UserDefaultPageURL())%>'>Moja stran</a>

                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <a href='<%= Page.ResolveUrl( SM.EM.Rewrite.Main.UserSettingsURL())%>'>Servisne strani</a>
                                
                                <%} %>
                                
                                <% if (SM.EM.Security.Permissions.IsAdmin()){ %>
                                    <a href='<%= Page.ResolveUrl("~/cms/default.aspx?smp=2") %>'> ADMIN</a>
                                   
                                <%} %>
                               
                            </div>
                        </div>
                    </div>
                </LoggedInTemplate>                
            </asp:LoginView>    
    

               





            <EM:MailConfirmation ID="objMailConfirmation" runat="server" Visible="false" />