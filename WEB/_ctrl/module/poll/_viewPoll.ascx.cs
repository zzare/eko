﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _ctrl_module_poll_viewPoll : BaseControlView
{

    public object Data;
    public object[] Parameters;
    protected POLL Poll;
    protected POLL_LANG PollLang;
    public string LangID;
    public Guid  CwpID;
    public bool ShowResultsIfVoted = true;

    protected void Page_Load(object sender, EventArgs e)
    {
        LoadPoll();

        //HttpResponseSubstitutionCallback subCallback = new HttpResponseSubstitutionCallback(POLL.RenderPollUpdateCommand(HttpContext.Current, ""  ));
    }



    public void LoadPoll() {

        if (Parameters != null)
        {
            if (Parameters[0] != null)
                IsModeCMS = bool.Parse(Parameters[0].ToString());
            if (Parameters.Length > 1 && Parameters[1] != null)
                LangID = Parameters[1].ToString();
            if (Parameters.Length > 2 && Parameters[2] != null)
                CwpID = new Guid(Parameters[2].ToString());
            if (Parameters.Length > 3 && Parameters[3] != null)
                ShowResultsIfVoted  = bool.Parse(Parameters[3].ToString());
        }
        
        if (Data == null)
            return;
        Poll = (POLL)Data;


        PollLang = POLL.GetPollLangByID(Poll.PollID, LangID);
        if (PollLang == null)
            PollLang = new POLL_LANG { PollQuestion = "" };


    }

    protected string  RenderPoll() {

        if (Poll == null)
            return "";



        if (!ShowResultsIfVoted || Poll.CanVote(Context .Request.UserHostAddress))
            return ViewManager.RenderView("~/_ctrl/module/poll/_viewPollVote.ascx", POLL.GetPollChoiceList(Poll.PollID, LangID), new object[] { Poll.PollID, LangID });
        else
            return ViewManager.RenderView("~/_ctrl/module/poll/_viewPollResult.ascx", POLL.GetPollChoiceList(Poll.PollID, LangID), new object[] { Poll.PollID, LangID });
    
    }


    protected override void OnPreRender(EventArgs e)
    {
        // show/hide edit
        phPollEdit.Visible = IsModeCMS;

        if (Poll == null)
        {
            phOK.Visible = false;
            phEmpty.Visible = true;
        }
        else {
            phOK.Visible = true;
            phEmpty.Visible = false;

        }

        // show EMPTY only for mode CMS
        if (!IsModeCMS ||  CwpID == Guid.Empty  ) {
            phEmpty.Visible = false;
        }


        base.OnPreRender(e);
    }

    protected override void Render(HtmlTextWriter writer)
    {
        base.Render(writer);
    }



}
