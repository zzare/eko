﻿<%@ Page Language="C#" ValidateRequest="false" MasterPageFile="~/_em/templates/fall/fall.master" AutoEventWireup="true" CodeFile="forgot-password.aspx.cs" Inherits="_em_content_membership_forgot_password" Title="Untitled Page" %>
<%@ Register TagPrefix="SM" TagName="Register" Src="~/_em/content/membership/_ctrl/_cntRegisterCustomerShort.ascx"   %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Namespace="SM.UI.Controls" TagPrefix="SM"  %>

<asp:Content ID="Content2" ContentPlaceHolderID="cph" Runat="Server">

                <div class="registerContW" style="margin-left:350px;">
                    <h1><%= GetLocalResourceObject("Title")%></h1>
                    <asp:PasswordRecovery ID="PasswordRecovery1" runat="server" OnSendingMail="OnSendingMail"  UserNameFailureText="Email ne obstaja"   >
                        <MailDefinition BodyFileName="~/_mailTemplates/PasswordRecovery.htm" 
                                         IsBodyHtml="True" 
                                    Subject="Pozabljeno geslo" Priority="High"  >
                        </MailDefinition>
                        <UserNameTemplate >
                            <asp:Literal meta:resourcekey="H2"  runat="server" />
                            <%--<h2 ><%= GetGlobalResourceObject("Modules", "LoginForgotPass")%></h2>--%>
                            <asp:Panel ID="panLogin" runat="server" DefaultButton="LoginButton">
                            <table class="TBloginbox" border="0" cellpadding="1" cellspacing="0" style="border-collapse:collapse;">
                                <tr>
                                    <td>
                                        <table border="0" cellpadding="0">
                                            <tr>
                                                <td colspan="2">
                                                    <asp:Literal  meta:resourcekey="Desc"  runat="server" />
                                                    <%--<%$ Resources:Modules, LoginForgotPass%>Vpišite e-mail in poslali vam bomo pozabljeno geslo.--%>
                                                </td>
                                            </tr>
                                            <tr>
  
                                                <td  colspan="2">
                                                    <asp:TextBox ID="UserName" runat="server" CssClass="TBXlogin TBXlogin_new "></asp:TextBox>
                                                    <cc1:TextBoxWatermarkExtender meta:resourcekey="ForgotPass" ID="TextBoxWatermarkExtender2" runat="server" WatermarkCssClass="WMmainLogin_new"  WatermarkText="vpiši e-poštni naslov" TargetControlID ="UserName"></cc1:TextBoxWatermarkExtender>
     
                                                </td><td>
                                                    <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" 
                                                        ControlToValidate="UserName" ErrorMessage="* Vpiši uporabniško ime/email" 
                                                        ToolTip="* Vpiši uporabniško ime/email" ValidationGroup="PasswordRecovery1">*</asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" style="color:Red;">
                                                    <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>
                                                <%--<asp:Button ID="SubmitButton" runat="server" CommandName="Submit" Text="Pošlji geslo" ValidationGroup="PasswordRecovery1" />--%>
                                                     <SM:LinkButtonDefault meta:resourcekey="LoginButton" ID="LoginButton" class="Astepnext" runat="server" CommandName="Submit"   ValidationGroup="PasswordRecovery1"></SM:LinkButtonDefault>
   
                                                </td>
                                                <td>
                                                    
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                             </asp:Panel>
                        </UserNameTemplate>
                        <SuccessTemplate>
                            <h2>Geslo je bilo poslano.</h2>
                        </SuccessTemplate>
                        
                </asp:PasswordRecovery>


                       
                </div>
    
</asp:Content>
