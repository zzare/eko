﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_cmsOrderFilter.ascx.cs" Inherits="SM.UI.Controls._cmsOrderFilter" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Namespace="SM.UI.Controls" TagPrefix="SM"  %>

<div class="qFilterInner_modal">
    <table width="100%">
        <tr>
            <td width="45px;">
                Filtriraj:
            </td>
            <td>
                <asp:DropDownList ID="ddlActive" runat="server" AutoPostBack="true" AppendDataBoundItems="true" onselectedindexchanged="ddlActive_SelectedIndexChanged">
                    <asp:ListItem Text="- aktivnost : vsi -" Value="" ></asp:ListItem>
                    <asp:ListItem Text="aktivni" Selected="True" Value="2" ></asp:ListItem>
                    <asp:ListItem Text="neaktivni" Value="0" ></asp:ListItem>
                </asp:DropDownList>&nbsp;
                
            </td>
            <td>
                <asp:DropDownList ID="ddlStatus" runat="server" AutoPostBack="true" AppendDataBoundItems="true" onselectedindexchanged="ddlActive_SelectedIndexChanged">
                    <asp:ListItem Text="- status : vsi -" Value="" ></asp:ListItem>
                    <asp:ListItem Text="potrjena" Value="2" ></asp:ListItem>
                    <asp:ListItem Text="plačana" Value="3" ></asp:ListItem>
                    <asp:ListItem Text="končana" Value="4" ></asp:ListItem>
                    <asp:ListItem Text="preklicana" Value="-1" ></asp:ListItem>
                </asp:DropDownList>&nbsp;
                
            </td>            
            <td align="right">
                <asp:Panel ID="Panel1" CssClass="" runat="server" DefaultButton ="btSearch" >
                    <table>
                        <tr>
                            <td>
                                <asp:TextBox Width="140" ID="tbSearch" runat="server" CssClass="qFilter_search_tbx"></asp:TextBox>&nbsp;
                            </td>
                            <td>
                                <SM:LinkButtonDefault ID="btSearch" runat="server" OnClick="btSearch_Click" CssClass="squarebutton_mini squarebutton_mini_green"><span>Išči</span></SM:LinkButtonDefault>
                                <cc1:TextBoxWatermarkExtender ID="tbweSearch" TargetControlID="tbSearch" WatermarkText="[vpiši iskalni pojem]" runat="server"></cc1:TextBoxWatermarkExtender>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>        
            </td>
        </tr>
    </table>
</div>
