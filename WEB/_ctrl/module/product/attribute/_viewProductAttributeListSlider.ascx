﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewProductAttributeListSlider.ascx.cs" Inherits="_ctrl_module_product_attribute_viewProductAttributeListSlider" %>

<script type="text/javascript">
    $(function () {

        $("div.attrSliderW input").live("keyup", function () {
            var v = parseInt($(this).val());
            if (isNaN(v)) return;
            var s = $(this).parent().parent();
            s.slider("value", v);

            //$("#yearSlider").slider("values", 0, parseInt($(this).val()));
        }
        ).live('keypress', function (e) {
            return (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) ? false : true;
        }).live('blur', function (e) {
            var s = $(this).parent().parent();
            $(this).val(s.slider("option", "value"));
        });

    });
</script>




<div id='<%= ViewID %>' class="pd_attri_slider">

<span class="pd_attri_titlered">UREJANJE DIMENZIJ ARTIKLA</span>
<span class="pd_attri_text">Vpišite dimenzije v dovoljenih merah</span>

<asp:ListView ID="lvList" runat="server" >
    <LayoutTemplate>
        
            <asp:PlaceHolder ID="itemPlaceholder" runat="server"/>

    </LayoutTemplate>
    
    <ItemTemplate>

        <%# RenderItem(Container.DataItem ) %>
        
        <% if (IsModeCMS){%>
            <span><a class="btEditContwthText" href="#" onclick="editProductAttribute('<%# ProductID %>', '<%# Eval("GroupAttID") %>'); return false" ><span>uredi</span></a></span>
        <%} %>
    
    </ItemTemplate>
</asp:ListView>
</div>
