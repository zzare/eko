﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_pmodHome.ascx.cs" Inherits="_em_templates__custom_eko__inc_content_pmodHome" %>
<%@ Register Namespace="SM.EM.UI.Controls" TagPrefix="EM"  %>
<%@ Register TagPrefix="EM" TagName="ModuleNew" Src="~/_ctrl/emenik/emModuleNew.ascx"  %>
<%@ Register TagPrefix="SM" TagName="ProductItem" Src="~/_ctrl/module/product/_viewProductListItemBig.ascx"  %>
<%@ Register TagPrefix="SM" TagName="HomeTimeline" Src="~/_em/templates/_custom/eko/_ctrl/_ctrlTimeline.ascx"  %>
<%@ Register TagPrefix="SM" TagName="BlogList" Src="~/_ctrl/module/article/_viewArticleListBlogHome.ascx"   %>


<script type="text/javascript">

<!--
    var flashvars = {};
    var params = {};
    params.quality = "high";
    params.bgcolor = "#ffffff";
    params.allowscriptaccess = "sameDomain";
    params.allowfullscreen = "true";
    params.base = ".";
    params.wmode = "transparent";
    var attributes = {};
    attributes.id = "pano";
    attributes.name = "pano";
    attributes.align = "middle";
//    swfobject.embedSWF(
//    			"<%= Page.ResolveUrl("~/") %>_em/templates/_custom/EKO/_inc/flash/Panorama_js_4_2_out.swf", "flashContent",
//				"970", "250",
//				"9.0.0", "expressInstall.swf",
//				flashvars, params, attributes);

    swfobject.embedSWF(
    			"<%= Page.ResolveUrl("~/") %>_em/templates/_custom/EKO/_inc/flash/KoradaPanorama01_out.swf", "flashContent",
				"970", "250",
				"9.0.0", "expressInstall.swf",
				flashvars, params, attributes);

//    swfobject.embedSWF(
//    			"<%= Page.ResolveUrl("~/") %>_em/templates/_custom/EKO/_inc/flash/Panorama_js_3_out.swf", "flashContent2",
//				"970", "250", 
//				"9.0.0", "expressInstall.swf",
//				flashvars, params, attributes);

//                    swfobject.embedSWF(
//    			"<%= Page.ResolveUrl("~/") %>_em/templates/_custom/EKO/_inc/flash/Panorama_js_1_out.swf", "flashContent3",
//				"970", "250", 
//				"9.0.0", "expressInstall.swf",
//				flashvars, params, attributes);

//-->



</script>


<script type="text/javascript">


    var selectedItem = -1;
    var timelineTimer = 0;

    var contactStep = new Array(0, 0, 0);
    var autoFocus = false;

    var ie = false;
    if ($.browser.msie) ie = true;
    //if(navigator.appName.indexOf("Explorer") > 0) ie=true;
    //var conn = "connector.php";

    //var langMenuTimer = null;
    //var swfLoaderTimer = null;

    //var pano = null;
    // override main
    function navigateToImages() {
        navigatePage('.home_inpicW');
    }

    $(document).ready(function () {

        $('div.ccycle').cycle({
            timeout: 5000,
            speed: 2000,
            cleartypeNoBg: true,
            fx: 'fade',
            speedIn: 1000,
            speedOut: 900,
            delay: 0
        });

        //        $('div.ccGalView').cycle({
        //            timeout: 2000,
        //            speed: 2000,
        //            cleartypeNoBg: true,
        //            fx: 'fade',
        //            speedIn: 1000,
        //            speedOut: 900,
        //            delay: 0
        //        });



        // menu animate
//        $('.mMainGallery a').click(function (e) {
//            e.preventDefault();
//            navigatePage('.home_inpicW');
//        });

        $('.mMainContact a').click(function (e) {
            e.preventDefault();
            navigatePage('.footerW');
        });





        //    loadTooltips();

        if (ie) {
            //ribbon correction
            //            document.getElementById("bigribbonC").style.top = 18;
            //            document.getElementById("smallribbonC").style.top = 18;

            //undelines
            //            underlines = ie_getElementsByClassName("div", "sectionTitle_underline");
            //            for (var i in underlines) {
            //                fotr = underlines[i].parentNode;
            //                sirina = fotr.firstChild.offsetWidth;
            //                underlines[i].style.width = sirina;
            //            }
        }



        $(".varrow").mouseover(function () {
            var indxstr = this.id.replace("l", "");
            expandTextContainer(parseInt(indxstr));
        });
        $(".timeline_bullet").mouseover(function () {
            var indxstr = this.id.replace("timeline_bullet_", "");
            expandTextContainer(parseInt(indxstr));
        });
        $(".timeline_date").mouseover(function () {
            var indxstr = this.id.replace("timeline_date_", "");
            expandTextContainer(parseInt(indxstr));
        });

        expandTextContainer(1);
        setGallery();




        //   loadSwf();
    }
);

    function ie_getElementsByClassName(tagname, classname) {
        i = 0;
        a = document.getElementsByTagName(tagname);
        res = new Array();
        while (element = a[i++]) {
            if (element.className == classname) {
                res[i - 1] = element;
            }
        }
        return res;
    }

    function expandTextContainer(indx) {
        //alert(indx);
        if (indx != selectedItem) {
            var textid = "l" + selectedItem + "t";
            if (selectedItem > -1) {
                arr1 = "l" + selectedItem;
                arrobj = document.getElementById(arr1);

                if (ie) {
                    $("#" + textid).hide();
                    arrobj.style.height = 115 + 'px';
                } else {
                    $("#" + textid).slideUp(200);
                }


                textContainer = document.getElementById(textid);
                textContainer.style.zIndex = 200;
                textContainer.style.left = 345 + 'px';

                arrobj.className = "varrow";
            }

            selectedItem = indx;
            textid = "l" + indx + "t";
            textContainer = document.getElementById(textid);
            textContainer.style.zIndex = 201;
            document.getElementById("l" + indx).className = "varrow_selected";


            //popravk vidnega polja
            var ofs = $("#timelinePoint_" + indx).offset();
            var ofspar = $("#timeline_scrolable").offset();
            var ofdiff = ofspar.left - ofs.left;

            var cWidth = $("#timeline_scrolable").width();
            var tWidth = $("#timelinePoint_" + indx).find('.textContainer').width();
            var tLeft = $("#timelinePoint_" + indx).find('.textContainer').position().left;

            // tmp fix
            var tLeft = parseInt(textContainer.style.left, 10);
            if (tLeft > 345)
                textContainer.style.left = '345px';

            
            //alert('ofs:' + ofs.left + 'ofspar:' + ofspar.left + ', cWidth:' + cWidth + ', tWidth:' + tWidth);
//            alert('ofs:' + ofs.left + 'ofspar:' + ofspar.left + ', tLeft:' + tLeft);

            if (ofdiff > 355) textContainer.style.left = ofdiff + 'px';
//            if (ie) corr = 380; else corr = 348;
            if (ie) corr = 338; else corr = 348;
            if (ofdiff < 0) textContainer.style.left = ofdiff + corr + 'px';

            
            //alert(' textContainer.style.left:' + textContainer.style.left);
            

            $("#" + textid).slideDown(1000, function () {
                    if (ie) {
                        if ($("#" + textid).is(":visible")) document.getElementById("l" + indx).style.height = 68 + 'px';
                    }
                }
  	        );
            
        }
    }



    function startScrollTimeline(step) {
        if (timelineTimer == 0) {
            timelineTimer = 1;
            scrollTimeline(step);
        }
    }
    function stopScrollTimeline() {
        timelineTimer = 0;
    }

    function scrollTimeline(step) {
        if (timelineTimer == 1) {
            tlObj = document.getElementById("timeline_scrolable");
            var ofset = parseInt(tlObj.scrollLeft);
            ofset = ofset + step;
            // limit
            if (ofset > 250)
                return;

            tlObj.scrollLeft = ofset;

//            textObj = document.getElementById("zgodba_text_scrolable");
            textObj = $(".zgodbaR")[0];

            var scrollRatio = textObj.scrollHeight / tlObj.scrollWidth;


            textObj.scrollTop = Math.round(scrollRatio * ofset);


            setTimeout("scrollTimeline(" + step + ")", 30);
        }
    }





    var galOpts = {
            panel_width: 970,
            panel_height: 430,
            panel_scale: 'nocrop',

            transition_speed: 800,
            transition_interval: 15000,
            pointer_size: 0,
            animate_pointer: false,
            frame_width: 80,
            frame_height: 60,
            frame_scale: 'crop',

            show_filmstrip_nav: false,
            frame_gap: 8,
            overlay_position_bottom_offset: -2,
            pause_on_hover: true,

            panel_animation: 'crossfade',

            frame_opacity: 1
        };


    function setGallery() {
        $('#ekoGallery0').galleryView(galOpts);
        $('#ekoGallery1').galleryView(galOpts);
        $('#ekoGallery2').galleryView(galOpts);

        setTimeout(function () {
            setAlbum(0);

            // check if hash
            //alert(window.location.hash);
            if (window.location.hash == '#v-sliki')
                navigatePage('.home_inpicW');
        
        }, 3000);

       // $('.cGalView').not(':first').hide();


    }


    function setAlbum(indx) {
        $('.home_ip_albumsel a').removeClass('sel').eq(indx).addClass('sel');
        $('.cGalView').hide().eq(indx).show();
        //$('#ekoGallery' + indx).galleryView(galOpts);
    }

</script>

<style type="text/css">


#zgodba_content{
	height: 530;
}
#zgodba_image{
	/*float: left; */
	width: 490px; 
  margin-top: -7px;
  margin-left: 255;

}
#zgodba_text_scrolable{	
	margin-left: 535;
	margin-top: 10px;
	width: 305;
	height: 500;
	overflow: hidden;	
}
#zgodba_text{
	display: none;
	color: #593726;
	font-family: arial;
	font-size : 12;		
	TEXT-ALIGN: LEFT;
	width: 305;
	margin: 0;	
}


.zgodbaScrollText{
	margin-bottom:30px;
}

#section_galerija{
	height: 610;
}
#galleryContainer{
	margin-top: 40;
	text-align: left;
}

#ekoGallery0, #ekoGallery1, #ekoGallery2 { visibility: hidden; 
                                           /*height: 430px !important; */
                                           }
</style>

<div class="homeW">

<div class="flashContentW">
	<div id="flashContent">
		<p><a href="http://www.adobe.com/go/getflashplayer"><img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" /></a></p>
	</div>
</div>


<% if (LANGUAGE.GetCurrentLang() == "si"){%>
    <p style="padding:5px 0px 10px 0px; font-size:12px;">360&deg; panorama</p>
    <p class="p_login_text">Predstavljamo vam Terra-Gourmet, deželo gurmanov. Skupaj smo ustvarili spletni klub za sladokusce, kjer si lahko izmenjujete mnenja, recepte in misli, predvsem pa naročite izbrane vrhunske izdelke. Med njimi boste našli okusno EKO meso goveda Limousine, ki je prvovrstna EKO govedina brez konkurence. Skrbno smo izbirali in ji dodali serijo dopolnjujočih izdelkov, kot so razni siri, jabolčni sok, vino, olivno olje in vinski kis, da boste lahko svojo pojedino zaokrožili v popolno harmonijo okusov.</p>
    <p class="p_login_text_c">Vse naše izdelke vam bomo z veseljem dostavili na dom.</p>   
         
<%}else if (LANGUAGE.GetCurrentLang() == "en"){ %>  
    <p style="padding:5px 0px 10px 0px; font-size:12px;">360&deg; Panorama</p>
    <p class="p_login_text">
        We are very pleased to introduce Terra-Gourmet,
        the land of gourmets. Together we have created an
        online club, where food-lovers can share opinions,
        thoughts and recipes, and also order the products
        of the highest quality. Among the latest you will
        find our delicious organic meat of Limousin cattle,
        excellent organic beef with no competition. We
        have chosen the range of complementary products
        with care: cheese, cider, wine, olive oil and wine
        vinegar, each of them allowing you to create a
        perfect harmony of flavours when preparing your
        meals.
    </p>
    <p class="p_login_text_c">We will be happy to deliver all our products to your place.</p>   
         
<%}else if (LANGUAGE.GetCurrentLang() == "it"){ %>  
    <p style="padding:5px 0px 10px 0px; font-size:12px;">360&deg; Panorama</p>
    <p class="p_login_text">
        Permetteteci di introdurvi al mondo di Terra-
        Gourmet, il paese degli amanti di cibo. Insieme
        abbiamo creato un club online, dove tutti i
        buongustai possono condividere opinioni, ricette
        e pensieri, ma soprattutto ordinare i prodotti di
        maggiore qualità. Tra gli ultimi troverete anche la
        nostra deliziosa ECO carne dei bovini Limousine,
        ottimo manzo ecologico senza concorrenza. Noi
        l’abbiamo scelto con cura, aggiungendole una
        serie di prodotti complementari come formaggi,
        sidro, vino, olio d’oliva e aceto di vino, che vi
        permettono di elaborare i vostri pasti creando una
        perfetta armonia di sapori.
    </p>
    <p class="p_login_text_c">Saremo lieti di consegnare tutti i nostri prodotti a domicilio.</p>       
<%} %>



<span class="ph_h2_text ph_h2_text_sepa_width"><span class="ph_h2_picL"></span><span class="ph_h2_picR"></span>&nbsp;</span>
        


<div class="separator_main"></div>

<div class="home_inwordW">
<h2 class="ph_h2" style="padding-bottom:15px;"><span class="ph_h2_text"><span class="ph_h2_picL"></span><span class="ph_h2_picR"></span>
    
<% if (LANGUAGE.GetCurrentLang() == "si"){%>
V BESEDI

<%}else if (LANGUAGE.GetCurrentLang() == "en"){ %>  
IN WORDS

<%}else if (LANGUAGE.GetCurrentLang() == "it"){ %>  
IN PAROLE

<%} %>

    </span>
</h2>




<%--TIMELINE BEGIN--%>
<%--<div class="section" id="section_zgodba">--%>


<%--JEZIKI--%>
<% if (LANGUAGE.GetCurrentLang() == "si"){%>


	    	<div class="leftright">
						<div class="goleft"><div class="goleft_arrowContainer" onmouseover="startScrollTimeline(-10)" onmouseout="stopScrollTimeline()"></div></div>
						<div class="leftright_scrolable" id="timeline_scrolable">
						<div class="timeline" id="timeline">
							<div class="timelinePoints">
				 				<div class="timelinePoint" id="timelinePoint_0" style="left: -470px">		 				  
				  					<div class="textContainer" id="l0t" style="z-index: 200; left: 365px; display: none; ">
			  						<div class="textContainer_title">(GENEZA) KORADA</div>
V primežu vsakdanjika včasih pozabimo na to, da je še ne tako dolgo nazaj v naši deželi življenje utripalo v popolnem sožitju z naravo. Hrana ni imela boljšega okusa le zato, ker je zelenjava zrasla v brstečem okolju in so bile živali osvobojene zaprtih prostorov, temveč tudi zato, ker so jo skupaj, z ljubeznijo, pripravljali in jedli prijatelji in družine.
V iskanju take idile nas je pot peljala onstran Alp, preko Furlanije, mimo Soške doline. Ko se nam je s hriba Korada sredi Goriških brd odprl razgled na gore in doline, ki smo jih pustili za seboj in nas je z juga oplazila aroma jadranskega morja, smo se počutili doma – v neokrnjeni naravi dežele gurmanov.

				  					</div> 					
										<div id="l0" class="varrow"><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/img_old/") %>timeline_arrowsempty.png"></div>				
				 				</div>										
                                
				 				<div class="timelinePoint" id="timelinePoint_1" style="left: -150px">		 				  
				  					<div class="textContainer" id="l1t" style="z-index: 201; left: 430px; display: none; ">
				  						<div class="textContainer_title">(PRVI SADOVI) KORADA, GORIŠKA BRDA</div>
                                        Tri leta je trajalo, da smo idejo gurmanske dežele zdrave hrane spremenili v dejanje. Danes vam lahko predstavimo prve sadove našega truda: meso goveda Limousine. Govedo se pase v idiličnem okolju pašnikov hriba Korada, kjer jim družbo delajo le tri prazne domačije in duh svobode. Gosta dlaka jih ščiti pred mrazom, zato se vse letne čase predajajo prostrani zelenici in zadovoljstvu.

				  					</div> 					
										<div id="l1" class="varrow_selected"><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/img_old/") %>timeline_arrowsempty.png"></div>				
				 				</div>													
				 				<div class="timelinePoint" id="timelinePoint_2" style="left: -90px">		 				  
				  					<div class="textContainer" id="l2t" style="z-index: 200; left: 365px; display: none; ">
				  						<div class="textContainer_title">(UVODNA POŠILJKA) LJUBLJANA</div>
Prvo vzorčno pošiljko mesa smo na dom pripeljali strastnim gurmanom in radovednežem. Govedina je najprej tri tedne odležala, potem smo jo vakuumsko zapakirali, ker na ta način ostane sveža in voljna še nadaljnjih 14–20 dni, ter jo predali prvim gurmanom.
				  					</div> 					
										<div id="l2" class="varrow"><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/img_old/") %>timeline_arrowsempty.png"></div>				
				 				</div>													
				 				<div class="timelinePoint" id="timelinePoint_3" style="left: 0px">		 				  
				  					<div class="textContainer" id="l3t" style="z-index: 200; left: 345px; display: none; ">
				  						<div class="textContainer_title">(OBETI) GORIŠKA BRDA</div>
                                            Ideja, ki nas je pred tremi leti odpeljala po naši poti, je prerasla svojo zibko. Med druženjem s podobno mislečimi pridelovalci in nabiralci hrane smo ugotovili, da si poleg navdušenja nad slastnimi plodovi delimo tudi željo, da bi naše zaklade delili z ostalimi ljudmi. Prišli smo do zaključka, da bi bilo prekrasno, če bi lahko sadje, zelenjavo, vino, vodo – pravzaprav vse, iz česar se naredi sočno, zdravo in okusno pojedino – lahko ljudje dobili na enem mestu.
    				  					</div> 					
										<div id="l3" class="varrow"><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/img_old/") %>timeline_arrowsempty.png"></div>				
				 				</div>													
				 				<div class="timelinePoint" id="timelinePoint_4" style="left: 65px">		 				  
				  					<div class="textContainer" id="l4t" style="display:none; ">
				  						<div class="textContainer_title">(TERRA-GOURMET) LJUBLJANA</div>
                                            Izza mej Korade in Goriških brd se hranoljubci in pridelovalci srečamo v Terra-Gourmet. Počasi iščemo nove sorodne duše in širimo ponudbo.
				  					</div> 					
										<div id="l4" class="varrow"><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/img_old/") %>timeline_arrowsempty.png"></div>				
				 				</div>	
				 				<div class="timelinePoint" id="timelinePoint_5" style="left: 130px">		 				  
				  					<div class="textContainer" id="l5t" style="z-index: 200; left: 365px; display: none; ">
			  						<div class="textContainer_title">ZAČETEK SPLETNE DEŽELE</div>
                                        Začetek spletne dežele.
				  					</div> 					
										<div id="l5" class="varrow"><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/img_old/") %>timeline_arrowsempty.png"></div>				
				 				</div>
				 				<div class="timelinePoint" id="timelinePoint_6" style="left: 225px">		 				  
				  					<div class="textContainer" id="l6t" style="z-index: 200; left:465px; display: none; ">
			  						<div class="textContainer_title">EKO certifikat</div>
                                        Z največjim veseljem vam sporočamo, da smo prejeli EKO certifikat za govedino Limousine. Vse od začetka smo verjeli v nesporno kvaliteto mesa naših limuzink, sedaj pa smo prejeli tudi uradno potrditev.
				  					</div> 					
										<div id="l6" class="varrow"><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/img_old/") %>timeline_arrowsempty.png"></div>				
				 				</div>                                										
												
						  </div>
							<div class="timeline_line">
							 			<div id="timeline_bullet_0}" class="timeline_bullet" style="left: 83px"></div>												
							 			<div id="timeline_bullet_1}" class="timeline_bullet" style="left: 403px"></div>												
							 			<div id="timeline_bullet_2}" class="timeline_bullet" style="left: 463px"></div>												
							 			<div id="timeline_bullet_3}" class="timeline_bullet" style="left: 553px"></div>												
							 			<div id="timeline_bullet_4}" class="timeline_bullet" style="left: 618px"></div>
                                        <div id="timeline_bullet_5}" class="timeline_bullet" style="left: 683px"></div>												
                                        <div id="timeline_bullet_6}" class="timeline_bullet" style="left: 778px"></div>												
							</div>
							<div class="timeline_dates">
							 	<div id="timeline_date_0}" class="timeline_date" style="left: 65px">17.Avg<br>2008</div>												
							 	<div id="timeline_date_1}" class="timeline_date" style="left: 385px">3.Feb<br>2011</div>												
							 	<div id="timeline_date_2}" class="timeline_date" style="left: 445px">10.Feb<br>2011</div>												
							 	<div id="timeline_date_3}" class="timeline_date" style="left: 535px">14.Apr<br>2011</div>												
							 	<div id="timeline_date_4}" class="timeline_date" style="left: 600px">21.Jun<br>2011</div>
                                <div id="timeline_date_5}" class="timeline_date" style="left: 665px">1.Dec<br>2011</div>												
                                <div id="timeline_date_6}" class="timeline_date" style="left: 760px">Marec<br>2012</div>												
							</div>
						</div>
					  </div>
            <div class="goright"><div class="goright_arrowContainer" onmouseover="startScrollTimeline(10)" onmouseout="stopScrollTimeline()"></div></div>
		</div>








<%}else if (LANGUAGE.GetCurrentLang() == "it"){ %>  



	    	<div class="leftright">
						<div class="goleft"><div class="goleft_arrowContainer" onmouseover="startScrollTimeline(-10)" onmouseout="stopScrollTimeline()"></div></div>
						<div class="leftright_scrolable" id="timeline_scrolable">
						<div class="timeline" id="timeline">
							<div class="timelinePoints">
				 				<div class="timelinePoint" id="timelinePoint_0" style="left: -470px">		 				  
				  					<div class="textContainer" id="l0t" style="z-index: 200; left: 365px; display: none; ">
			  						<div class="textContainer_title">(GENESI) KORADA</div>
                                        <%--Nella frenesia della vita quotidiana a volte ci
                                        scordiamo che, in un passato non così remoto,
                                        la vita nel nostro Paese palpitava in perfetta
                                        armonia con la natura. Il cibo non aveva un sapore
                                        più pregiato solamente perché la verdura si
                                        coltivava nella natura genuina mentre gli animali
                                        pascolavano liberi all’aria aperta, ma anche perché
                                        gli amici e le famiglie preparavano e mangiavano
                                        i pasti insieme, con cura e con amore. La ricerca
                                        di una simile perfezione ci portò dall’altra parte
                                        delle Alpi, passando per il Friuli Venezia Giulia e
                                        per la Valle dell’Isonzo. Quando dall’alto del monte
                                        Korada, che si trova nel cuore del Collio Goriziano,
                                        ci apparve improvvisamente la vista dei monti e
                                        delle valli che lasciammo indietro, e dal sud ci
                                        avvolse la brezza del mare Adriatico, scoprimmo
                                        che avevamo trovato casa – nella natura intatta del
                                        paese dei buongustai.--%>
                                          <%--Nella frenesia della vita quotidiana a volte ci scordiamo che nel passato la vita nel nostro Paese palpitava in armonia con la natura. Il cibo aveva un sapore più pregiato perché la verdura si coltivava nella natura genuina e gli animali pascolavano all’aria aperta, mentre gli amici e le famiglie mangiavano insieme, preparando i pasti con cura e con amore. La ricerca di una simile perfezione ci portò dall’altra parte delle Alpi, passando per il Friuli Venezia Giulia e per la Valle dell’Isonzo. Quando dall’alto del monte Korada ci apparve la vista delle valli che lasciammo indietro, e dal sud ci avvolse la brezza del mare Adriatico, scoprimmo che avevamo trovato casa – nella natura intatta del paese dei buongustai. --%>
                                          Nella frenesia della vita a volte ci scordiamo che nel passato la vita nel nostro Paese palpitava in armonia con la natura. Il cibo aveva un sapore più pregiato perché la verdura si coltivava nella natura genuina e gli animali pascolavano all’aria aperta, mentre la gente preparava i pasti con cura e con amore. La ricerca di una simile perfezione ci portò dall’altra parte delle Alpi, passando per il Friuli Venezia Giulia e per la Valle dell’Isonzo. Quando dal monte Korada ci apparve la vista della terra che lasciammo indietro, e dal sud ci avvolse la brezza del mare Adriatico, scoprimmo che avevamo trovato casa – nella natura intatta del paese dei buongustai.
				  					</div> 					
										<div id="l0" class="varrow"><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/img_old/") %>timeline_arrowsempty.png"></div>				
				 				</div>										
                                
				 				<div class="timelinePoint" id="timelinePoint_1" style="left: -150px">		 				  
				  					<div class="textContainer" id="l1t" style="z-index: 201; left: 430px; display: none; ">
				  						<div class="textContainer_title">(I PRIMI FRUTTI) KORADA, IL COLLIO GORIZIANO</div>
                                        Ci sono voluti tre anni per dare vita all’idea del
                                        paese del cibo sano. Oggi vi possiamo presentare
                                        i frutti dei nostri sforzi: l’ECO carne di bovini

                                        Limousine. I bovini pascolano negli idilliaci
                                        campi del monte Korada, dove solamente tre
                                        casolari abbandonati e lo spirito di libertà gli fanno
                                        compagnia. Gli animali sono coperti da un pelo
                                        spesso che li protegge dal freddo, perciò possono
                                        godere degli immensi prati durante tutto l’anno.
				  					</div> 					
										<div id="l1" class="varrow_selected"><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/img_old/") %>timeline_arrowsempty.png"></div>				
				 				</div>													
				 				<div class="timelinePoint" id="timelinePoint_2" style="left: -90px">		 				  
				  					<div class="textContainer" id="l2t" style="z-index: 200; left: 365px; display: none; ">
				  						<div class="textContainer_title">(LA PRIMA SPEDIZIONE) LUBIANA</div>
                                        La prima spedizione di prova di carne è stata
                                        consegnata a casa dei buongustai appassionati e
                                        curiosi. Prima, la carne di manzo aveva riposato
                                        per tre settimane, dopo di che è stata confezionata
                                        sottovuoto per conservare la freschezza per altri
                                        14-20 giorni. Alla fine abbiamo consegnato la
                                        carne ai nostri buongustai.
				  					</div> 					
										<div id="l2" class="varrow"><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/img_old/") %>timeline_arrowsempty.png"></div>				
				 				</div>													
				 				<div class="timelinePoint" id="timelinePoint_3" style="left: 0px">		 				  
				  					<div class="textContainer" id="l3t" style="z-index: 200; left: 345px; display: none; ">
				  						<div class="textContainer_title">(PREVISIONI) IL COLLIO GORIZIANO</div>
                                            L’idea che tre anni fa ci ha fatto prendere il
                                            nostro cammino ora si è sviluppata, è cresciuta.
                                            Passando il tempo con i produttori e i raccoglitori di
                                            cibi che condividono la nostra mentalità, abbiamo
                                            scoperto che, oltre a condividere l’entusiasmo
                                            per i deliziosi frutti della terra, ci unisce anche
                                            il desiderio di condividere i nostri tesori con il
                                            resto del mondo. Siamo arrivati alla conclusione
                                            che sarebbe bellissimo poter far si che la gente
                                            potesse trovare tutto nello stesso posto: frutta
                                            e verdura, vino, acqua e tutti gli ingredienti che
                                            servono per preparare un pasto saporito, sano e
                                            gustoso.
    				  					</div> 					
										<div id="l3" class="varrow"><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/img_old/") %>timeline_arrowsempty.png"></div>				
				 				</div>													
				 				<div class="timelinePoint" id="timelinePoint_4" style="left: 65px">		 				  
				  					<div class="textContainer" id="l4t" style="display:none; ">
				  						<div class="textContainer_title">(TERRA-GOURMET) LUBIANA</div>
                                        Da oltre i confini del monte Korada e del Collio
                                        Goriziano, i buongustai e i produttori di cibo ci
                                        incontriamo in Terra-Gourmet. Poco a poco stiamo
                                        cercando altre anime gemelle per ampliare la
                                        nostra offerta.
				  					</div> 					
										<div id="l4" class="varrow"><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/img_old/") %>timeline_arrowsempty.png"></div>				
				 				</div>	
				 				<div class="timelinePoint" id="timelinePoint_5" style="left: 130px">		 				  
				  					<div class="textContainer" id="l5t" style="z-index: 200; left: 365px; display: none; ">
			  						<div class="textContainer_title">LA CREAZIONE DEL PAESE ONLINE</div>
                                        La creazione del paese Online.
				  					</div> 					
										<div id="l5" class="varrow"><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/img_old/") %>timeline_arrowsempty.png"></div>				
				 				</div>
				 				<div class="timelinePoint" id="timelinePoint_6" style="left: 225px">		 				  
				  					<div class="textContainer" id="l6t" style="z-index: 200; left:465px; display: none; ">
			  						<div class="textContainer_title">Certificazione Ecologica</div>
                                        È con immenso piacere che vi comunichiamo di
                                        aver ricevuto la Certificazione Ecologica per la
                                        carne dei bovini Lomousine. Abbiamo creduto nella
                                        qualità indiscussa dei nostri bovini fin dall’inizio,
                                        ora abbiamo anche la conferma ufficiale.
				  					</div> 					
										<div id="l6" class="varrow"><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/img_old/") %>timeline_arrowsempty.png"></div>				
				 				</div>                                										
												
						  </div>
							<div class="timeline_line">
							 			<div id="timeline_bullet_0}" class="timeline_bullet" style="left: 83px"></div>												
							 			<div id="timeline_bullet_1}" class="timeline_bullet" style="left: 403px"></div>												
							 			<div id="timeline_bullet_2}" class="timeline_bullet" style="left: 463px"></div>												
							 			<div id="timeline_bullet_3}" class="timeline_bullet" style="left: 553px"></div>												
							 			<div id="timeline_bullet_4}" class="timeline_bullet" style="left: 618px"></div>
                                        <div id="timeline_bullet_5}" class="timeline_bullet" style="left: 683px"></div>												
                                        <div id="timeline_bullet_6}" class="timeline_bullet" style="left: 778px"></div>												
							</div>
							<div class="timeline_dates">
															 	<div id="timeline_date_0}" class="timeline_date" style="left: 65px">17 agosto<br>2008</div>												
							 	<div id="timeline_date_1}" class="timeline_date" style="left: 375px">3 febbraio<br>2011</div>												
							 	<div id="timeline_date_2}" class="timeline_date" style="left: 445px">10 febbraio<br>2011</div>												
							 	<div id="timeline_date_3}" class="timeline_date" style="left: 535px">14 aprile<br>2011</div>												
							 	<div id="timeline_date_4}" class="timeline_date" style="left: 600px">21 giugno<br>2011</div>
                                <div id="timeline_date_5}" class="timeline_date" style="left: 665px">1 dicembre<br>2011</div>												
                                <div id="timeline_date_6}" class="timeline_date" style="left: 760px">Marzo<br>2012</div>						
							</div>
						</div>
					  </div>
            <div class="goright"><div class="goright_arrowContainer" onmouseover="startScrollTimeline(10)" onmouseout="stopScrollTimeline()"></div></div>
		</div>






<%}else if (LANGUAGE.GetCurrentLang() == "en"){ %>  



	    	<div class="leftright">
						<div class="goleft"><div class="goleft_arrowContainer" onmouseover="startScrollTimeline(-10)" onmouseout="stopScrollTimeline()"></div></div>
						<div class="leftright_scrolable" id="timeline_scrolable">
						<div class="timeline" id="timeline">
							<div class="timelinePoints">
				 				<div class="timelinePoint" id="timelinePoint_0" style="left: -470px">		 				  
				  					<div class="textContainer" id="l0t" style="z-index: 200; left: 365px; display: none; ">
			  						<div class="textContainer_title">(GENESIS) MOUNT KORADA</div>
                                        <%--In the frenetic pace of everyday life we sometimes
                                        forget that in a not too distant past, in our country
                                        we used to lead our lives in perfect harmony with
                                        nature. Not only did the food taste better because
                                        the vegetables were cultivated in a flourishing
                                        nature while the animals were free to move in the
                                        open air, but also because friends and families
                                        took their time to prepare and eat food together,
                                        with care and love. The search for such an idyll
                                        took us across the Alps, the Friuli Venezia Giulia
                                        region and past the Soča Valley. When from the
                                        top of Mount Korada, nested in the heart of the
                                        Goriška brda region, the view of the mountains
                                        and valleys we had left behind appeared, and we
                                        felt the breeze of the Adriatic Sea coming from the
                                        south, we felt like home – in the pure nature of the
                                        land of gourmets.--%>
                                          <%--In the frenetic pace of everyday life we sometimes forget that in the past we used to lead our lives in perfect harmony with nature. Not only did the food taste better because the vegetables were cultivated in a flourishing nature while the animals were free to move in the open air, but also because friends and families took their time to prepare and eat food together. The search for such an idyll took us across the Alps, the Friuli Venezia Giulia region and past the Soča Valley. When from the top of Mount Korada the view of the mountains and valleys we had left behind appeared, and we felt the breeze of the Adriatic Sea coming from the south, we felt like home – in the pure nature of the land of gourmets.--%>
                                          In the frenetic pace of everyday life we sometimes forget that in the past we used to lead our lives in harmony with nature. The food tasted better because the vegetables were cultivated in a flourishing nature while the animals were free to move in the open air, and people took their time to prepare and eat food together. The search for such an idyll took us across the Alps, the Friuli Venezia Giulia region and past the Soča Valley. When from the top of Mount Korada the view of the land we had left behind appeared, and we felt the breeze of the Adriatic Sea from the south, we felt like home – in the pure nature of the land of gourmets.
				  					</div> 					
										<div id="l0" class="varrow"><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/img_old/") %>timeline_arrowsempty.png"></div>				
				 				</div>										
                                
				 				<div class="timelinePoint" id="timelinePoint_1" style="left: -150px">		 				  
				  					<div class="textContainer" id="l1t" style="z-index: 201; left: 430px; display: none; ">
				  						<div class="textContainer_title">(THE FIRST FRUITS) MOUNT KORADA, THE GORIŠKA BRDA REGION</div>
                                            It took us three years to give life to the idea of
                                            a land of healthy food. Today we are happy to

                                            introduce you to the fruits of our efforts: the organic
                                            Limousin cattle beef. Our cattle graze on the
                                            idyllic fields of Mount Korada, where only three
                                            abandoned homesteads and the spirit of freedom
                                            keep it company. The animals are covered by a
                                            dense hair which protects them from the cold, so
                                            they can enjoy the immense meadows all year
                                            long.
				  					</div> 					
										<div id="l1" class="varrow_selected"><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/img_old/") %>timeline_arrowsempty.png"></div>				
				 				</div>													
				 				<div class="timelinePoint" id="timelinePoint_2" style="left: -90px">		 				  
				  					<div class="textContainer" id="l2t" style="z-index: 200; left: 365px; display: none; ">
				  						<div class="textContainer_title">(THE FIRST DELIVERY) LJUBLJANA</div>
                                            The first sample of meat delivery service was
                                            provided for passionate and curious foodies.
                                            Firstly, the beef had rested for three weeks, only
                                            then it had been vacuum-packed to preserve its
                                            freshness for the next 14-20 days. Finally, we
                                            delivered it to our first gourmet customers.
				  					</div> 					
										<div id="l2" class="varrow"><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/img_old/") %>timeline_arrowsempty.png"></div>				
				 				</div>													
				 				<div class="timelinePoint" id="timelinePoint_3" style="left: 0px">		 				  
				  					<div class="textContainer" id="l3t" style="z-index: 200; left: 345px; display: none; ">
				  						<div class="textContainer_title">(FORECAST) THE GORIŠKA BRDA REGION</div>
                                            The idea which three years ago made ​us start our
                                            journey has now developed, has grown. Passing
                                            the time with food producers and gatherers who
                                            share our vision, we found out that, in addition to
                                            sharing the enthusiasm for the delicious fruits of
                                            the earth, there is a common desire to share our
                                            treasures with the rest of the world. We came to
                                            the conclusion that it would be great to make sure
                                            people could find everything at one place: fruits
                                            and vegetables, wine, water and all the ingredients
                                            necessary to prepare a juicy, healthy and delicious
                                            meal.
    				  					</div> 					
										<div id="l3" class="varrow"><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/img_old/") %>timeline_arrowsempty.png"></div>				
				 				</div>													
				 				<div class="timelinePoint" id="timelinePoint_4" style="left: 65px">		 				  
				  					<div class="textContainer" id="l4t" style="display:none; ">
				  						<div class="textContainer_title">(TERRA-GOURMET) LJUBLJANA</div>
                                        From beyond the borders of Mount Korada and the
                                        Goriška brda region, gourmets and food producers
                                        all meet in Terra-Gourmet. Step by step we are
                                        looking for other kindred spirits to expand our offer.
				  					</div> 					
										<div id="l4" class="varrow"><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/img_old/") %>timeline_arrowsempty.png"></div>				
				 				</div>	
				 				<div class="timelinePoint" id="timelinePoint_5" style="left: 130px">		 				  
				  					<div class="textContainer" id="l5t" style="z-index: 200; left: 365px; display: none; ">
			  						<div class="textContainer_title">THE CREATION OF THE ONLINE LAND</div>
                                        The creation of the Online Land.
				  					</div> 					
										<div id="l5" class="varrow"><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/img_old/") %>timeline_arrowsempty.png"></div>				
				 				</div>
				 				<div class="timelinePoint" id="timelinePoint_6" style="left: 225px">		 				  
				  					<div class="textContainer" id="l6t" style="z-index: 200; left:465px; display: none; ">
			  						<div class="textContainer_title">ECO LABEL</div>
                                        We have the pleasure to inform you that our
                                        Limousin cattle beef was awarded the ECO
                                        certificate. Since the very beginning we have
                                        believed in the unquestionable quality of our beef,
                                        and now we have the official confirmation, too.
				  					</div> 					
										<div id="l6" class="varrow"><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/img_old/") %>timeline_arrowsempty.png"></div>				
				 				</div>                                										
												
						  </div>
							<div class="timeline_line">
							 			<div id="timeline_bullet_0}" class="timeline_bullet" style="left: 83px"></div>												
							 			<div id="timeline_bullet_1}" class="timeline_bullet" style="left: 403px"></div>												
							 			<div id="timeline_bullet_2}" class="timeline_bullet" style="left: 463px"></div>												
							 			<div id="timeline_bullet_3}" class="timeline_bullet" style="left: 553px"></div>												
							 			<div id="timeline_bullet_4}" class="timeline_bullet" style="left: 618px"></div>
                                        <div id="timeline_bullet_5}" class="timeline_bullet" style="left: 683px"></div>												
                                        <div id="timeline_bullet_6}" class="timeline_bullet" style="left: 778px"></div>												
							</div>
							<div class="timeline_dates">  							 	<div id="timeline_date_0}" class="timeline_date" style="left: 65px">August 17,<br>2008</div>												
							 	<div id="timeline_date_1}" class="timeline_date" style="left: 385px">Feb. 3,<br>2011</div>												
							 	<div id="timeline_date_2}" class="timeline_date" style="left: 445px">Feb. 10,<br>2011</div>												
							 	<div id="timeline_date_3}" class="timeline_date" style="left: 535px">April 14,<br>2011</div>												
							 	<div id="timeline_date_4}" class="timeline_date" style="left: 600px">June 21,<br>2011</div>
                                <div id="timeline_date_5}" class="timeline_date" style="left: 665px">December 1,<br>2011</div>												
                                <div id="timeline_date_6}" class="timeline_date" style="left: 760px">March<br>2012</div> 
										
							</div>
						</div>
					  </div>
            <div class="goright"><div class="goright_arrowContainer" onmouseover="startScrollTimeline(10)" onmouseout="stopScrollTimeline()"></div></div>
		</div>





<%} %>





<% if (LANGUAGE.GetCurrentLang() == "si"){%>
				<div id="tmp_zgodba_content" class="zgodba_content">
                    <div class="zgodbaL">
					    <div id="tmp_zgodba_image" class="zgodba_img ccycle">
						    <img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/vstopna/timeline_img_cycle_2.png") %>">
						    <img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/vstopna/timeline_img_cycle_1.png") %>">
					    </div>
                    </div>
					<div id="tmp_zgodba_text_scrolable" class="zgodbaR">
						<div id="tmp_zgodba_text" class="zgodba_text">

                            <p>
                                V primežu vsakdanjika včasih pozabimo na to, da je še ne tako dolgo nazaj v naši deželi življenje utripalo v popolnem sožitju z naravo. Hrana ni imela boljšega okusa le zato, ker je zelenjava zrasla v brstečem okolju in so bile živali osvobojene zaprtih prostorov, temveč tudi zato, ker so jo skupaj, z ljubeznijo, pripravljali in jedli prijatelji in družine.
                            </p>
                            <p style="padding:40px 0px 0px 0px;">
                                Tri leta je trajalo, da smo idejo gurmanske dežele zdrave hrane spremenili v dejanje. Danes vam lahko predstavimo prve sadove našega truda: EKO meso goveda Limousine. Govedo se pase v idiličnem okolju pašnikov hriba Korada, kjer jim družbo delajo le tri prazne domačije in duh svobode. Gosta dlaka jih ščiti pred mrazom, zato se vse letne čase predajajo prostrani zelenici in zadovoljstvu.
                            </p>
                            <p style="padding:20px 0px 0px 0px;">
                                <img alt="Bio" src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/logotipi/BIOZNAK_EU.png") %>"  />
                            </p>
						</div>			
					</div>
                    <div class="clearing"></div>
				</div>				


<%}else if (LANGUAGE.GetCurrentLang() == "en"){ %>  
				<div id="tmp_zgodba_content" class="zgodba_content">
                    <div class="zgodbaL">
					    <div id="tmp_zgodba_image" class="zgodba_img ccycle">
						    <img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/vstopna/timeline_img_cycle_2.png") %>">
						    <img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/vstopna/timeline_img_cycle_1.png") %>">
					    </div>
                    </div>
					<div id="tmp_zgodba_text_scrolable" class="zgodbaR">
						<div id="tmp_zgodba_text" class="zgodba_text">

                            <p>
                                In the frenetic pace of everyday life we sometimes
                                forget that in a not too distant past, in our country
                                we used to lead our lives in perfect harmony with
                                nature. Not only did the food taste better because
                                the vegetables were cultivated in a flourishing
                                nature while the animals were free to move in the
                                open air, but also because friends and families
                                took their time to prepare and eat food together,
                                with care and love.
                            </p>
                            <p style="padding:40px 0px 0px 0px;">
                                It took us three years to give life to the idea of
                                a land of healthy food. Today we are happy to
                                introduce you to the fruits of our efforts: the organic
                                Limousin cattle beef. Our cattle graze on the
                                idyllic fields of Mount Korada, where only three
                                abandoned homesteads and the spirit of freedom
                                keep it company. The animals are covered by a
                                dense hair which protects them from the cold, so
                                they can enjoy the immense meadows all year
                                long.
                            </p>
                            <p style="padding:20px 0px 0px 0px;">
                                <img alt="Bio" src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/logotipi/BIO_EU_ECO_ENG_small.png") %>"  />
                            </p>
						</div>			
					</div>
                    <div class="clearing"></div>
				</div>				


<%}else if (LANGUAGE.GetCurrentLang() == "it"){ %>  
				<div id="tmp_zgodba_content" class="zgodba_content">
                    <div class="zgodbaL">
					    <div id="tmp_zgodba_image" class="zgodba_img ccycle">
						    <img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/vstopna/timeline_img_cycle_2.png") %>">
						    <img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/vstopna/timeline_img_cycle_1.png") %>">
					    </div>
                    </div>
					<div id="tmp_zgodba_text_scrolable" class="zgodbaR">
						<div id="tmp_zgodba_text" class="zgodba_text">

                            <p>
                                Nella frenesia della vita quotidiana a volte ci
                                scordiamo che, in un passato non così remoto,
                                la vita nel nostro Paese palpitava in perfetta
                                armonia con la natura. Il cibo non aveva un sapore
                                più pregiato solamente perché la verdura si
                                coltivava nella natura genuina mentre gli animali
                                pascolavano liberi all’aria aperta, ma anche perché
                                gli amici e le famiglie preparavano e mangiavano i
                                pasti insieme, con cura e con amore.
                            </p>
                            <p style="padding:40px 0px 0px 0px;">
                                Ci sono voluti tre anni per dare vita all’idea del
                                paese del cibo sano. Oggi vi possiamo presentare
                                i frutti dei nostri sforzi: l’ECO carne di bovini
                                Limousine. I bovini pascolano negli idilliaci
                                campi del monte Korada, dove solamente tre
                                casolari abbandonati e lo spirito di libertà gli fanno
                                compagnia. Gli animali sono coperti da un pelo
                                spesso che li protegge dal freddo, perciò possono
                                godere degli immensi prati durante tutto l’anno.
                            </p>
                            <p style="padding:20px 0px 0px 0px;">
                                <img alt="Bio" src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/logotipi/BIO_EU_ECO_IT_small.png") %>"  />
                            </p>
						</div>			
					</div>
                    <div class="clearing"></div>
				</div>				

<%} %>







<%--</div>--%>

<%--TIMELINE END--%>
</div>

<div class="separator_main"></div>

<div class="home_blogW">
<% if (LANGUAGE.GetCurrentLang() == "si"){%>
        <h2 class="ph_h2"><span class="ph_h2_text"><span class="ph_h2_picL"></span><span class="ph_h2_picR"></span>KLEPETALNICA</span></h2>

       

<%}else if (LANGUAGE.GetCurrentLang() == "en"){ %>  
        <h2 class="ph_h2"><span class="ph_h2_text"><span class="ph_h2_picL"></span><span class="ph_h2_picR"></span>CHAT ROOM</span></h2>

<%--        <SM:BlogList ID="objBlogList" runat="server" />
--%>
<%}else if (LANGUAGE.GetCurrentLang() == "it"){ %>  
        <h2 class="ph_h2"><span class="ph_h2_text"><span class="ph_h2_picL"></span><span class="ph_h2_picR"></span>CHAT ROOM</span></h2>

<%--        <SM:BlogList ID="objBlogList" runat="server" />
--%>
<%} %>

    <SM:BlogList ID="objBlogList" runat="server" />

    <div class="clearing"></div>
</div>

<div class="separator_main"></div>


<div class="home_inpicW">
<% if (LANGUAGE.GetCurrentLang() == "si"){%>
        <h2 class="ph_h2"><span class="ph_h2_text"><span class="ph_h2_picL"></span><span class="ph_h2_picR"></span>V SLIKI</span></h2>

        <div class="ccGalView">
        <div class="cGalView">
            <ul id="ekoGallery0" >
                <li><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/galerija/nekoc/1.jpg") %>" alt="" /></li>
                <li><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/galerija/nekoc/2.jpg") %>" alt="" /></li>
                <li><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/galerija/nekoc/3.jpg") %>" alt="" /></li>
                <li><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/galerija/nekoc/4.jpg") %>" alt="" /></li>
            </ul>
        </div>
        
        <div class="cGalView"  >
            <ul id="ekoGallery1" >
                <li><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/galerija/danes/3.jpg") %>" alt="" /></li>
                <li><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/galerija/danes/4.jpg") %>" alt="" /></li>
                <li><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/galerija/danes/5.jpg") %>" alt="" /></li>
                <li><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/galerija/danes/6.jpg") %>" alt="" /></li>
                <li><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/galerija/danes/7.jpg") %>" alt="" /></li>
                <li><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/galerija/danes/8.jpg") %>" alt="" /></li>
                <li><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/galerija/danes/9.jpg") %>" alt="" /></li>
                <li><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/galerija/danes/1.jpg") %>" alt="" /></li>
                <li><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/galerija/danes/2.jpg") %>" alt="" /></li>
            </ul>
        </div>

        <div class="cGalView"  >
            <ul id="ekoGallery2" >
                <li><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/galerija/limuzinke/1.jpg") %>" alt="" /></li>
                <li><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/galerija/limuzinke/2.jpg") %>" alt="" /></li>
                <li><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/galerija/limuzinke/3.jpg") %>" alt="" /></li>
                <li><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/galerija/limuzinke/4.jpg") %>" alt="" /></li>
                <li><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/galerija/limuzinke/5.jpg") %>" alt="" /></li>
                <li><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/galerija/limuzinke/6.jpg") %>" alt="" /></li>
                <li><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/galerija/limuzinke/7.jpg") %>" alt="" /></li>
            </ul>
        </div>
        </div>
        

<%}else if (LANGUAGE.GetCurrentLang() == "en"){ %>  
        <h2 class="ph_h2"><span class="ph_h2_text"><span class="ph_h2_picL"></span><span class="ph_h2_picR"></span>IN PICTURES</span></h2>

        <div class="ccGalView">
        <div class="cGalView">
            <ul id="ekoGallery0" >
                <li><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/galerija/nekoc/1.jpg") %>" alt="" /></li>
                <li><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/galerija/nekoc/2.jpg") %>" alt="" /></li>
                <li><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/galerija/nekoc/3.jpg") %>" alt="" /></li>
                <li><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/galerija/nekoc/4.jpg") %>" alt="" /></li>
            </ul>
        </div>
        
        <div class="cGalView"  >
            <ul id="ekoGallery1" >
                <li><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/galerija/danes/3.jpg") %>" alt="" /></li>
                <li><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/galerija/danes/4.jpg") %>" alt="" /></li>
                <li><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/galerija/danes/5.jpg") %>" alt="" /></li>
                <li><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/galerija/danes/6.jpg") %>" alt="" /></li>
                <li><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/galerija/danes/7.jpg") %>" alt="" /></li>
                <li><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/galerija/danes/8.jpg") %>" alt="" /></li>
                <li><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/galerija/danes/9.jpg") %>" alt="" /></li>
                <li><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/galerija/danes/1.jpg") %>" alt="" /></li>
                <li><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/galerija/danes/2.jpg") %>" alt="" /></li>
            </ul>
        </div>

        <div class="cGalView"  >
            <ul id="ekoGallery2" >
                <li><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/galerija/limuzinke/1.jpg") %>" alt="" /></li>
                <li><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/galerija/limuzinke/2.jpg") %>" alt="" /></li>
                <li><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/galerija/limuzinke/3.jpg") %>" alt="" /></li>
                <li><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/galerija/limuzinke/4.jpg") %>" alt="" /></li>
                <li><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/galerija/limuzinke/5.jpg") %>" alt="" /></li>
                <li><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/galerija/limuzinke/6.jpg") %>" alt="" /></li>
                <li><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/galerija/limuzinke/7.jpg") %>" alt="" /></li>
            </ul>
        </div>
        </div>
        

<%}else if (LANGUAGE.GetCurrentLang() == "it"){ %>  
        <h2 class="ph_h2"><span class="ph_h2_text"><span class="ph_h2_picL"></span><span class="ph_h2_picR"></span>IN IMMAGINI</span></h2>

        <div class="ccGalView">
        <div class="cGalView">
            <ul id="ekoGallery0" >
                <li><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/galerija/nekoc/1.jpg") %>" alt="" /></li>
                <li><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/galerija/nekoc/2.jpg") %>" alt="" /></li>
                <li><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/galerija/nekoc/3.jpg") %>" alt="" /></li>
                <li><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/galerija/nekoc/4.jpg") %>" alt="" /></li>
            </ul>
        </div>
        
        <div class="cGalView"  >
            <ul id="ekoGallery1" >
                <li><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/galerija/danes/3.jpg") %>" alt="" /></li>
                <li><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/galerija/danes/4.jpg") %>" alt="" /></li>
                <li><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/galerija/danes/5.jpg") %>" alt="" /></li>
                <li><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/galerija/danes/6.jpg") %>" alt="" /></li>
                <li><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/galerija/danes/7.jpg") %>" alt="" /></li>
                <li><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/galerija/danes/8.jpg") %>" alt="" /></li>
                <li><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/galerija/danes/9.jpg") %>" alt="" /></li>
                <li><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/galerija/danes/1.jpg") %>" alt="" /></li>
                <li><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/galerija/danes/2.jpg") %>" alt="" /></li>
            </ul>
        </div>

        <div class="cGalView"  >
            <ul id="ekoGallery2" >
                <li><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/galerija/limuzinke/1.jpg") %>" alt="" /></li>
                <li><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/galerija/limuzinke/2.jpg") %>" alt="" /></li>
                <li><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/galerija/limuzinke/3.jpg") %>" alt="" /></li>
                <li><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/galerija/limuzinke/4.jpg") %>" alt="" /></li>
                <li><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/galerija/limuzinke/5.jpg") %>" alt="" /></li>
                <li><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/galerija/limuzinke/6.jpg") %>" alt="" /></li>
                <li><img src="<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/galerija/limuzinke/7.jpg") %>" alt="" /></li>
            </ul>
        </div>
        </div>
        
<%} %>

<div class="home_ip_albumselW">
<div class="home_ip_albumsel">

<% if (LANGUAGE.GetCurrentLang() == "si"){%>
        <span class="albumchoose">Poglej album:</span>
        <a href="#" onclick="setAlbum(0); return false;" ><span>Nekoč</span></a>
        <a href="#" onclick="setAlbum(1); return false;"><span>Danes</span></a>
        <a href="#" onclick="setAlbum(2); return false;"><span>Limuzinke</span></a>

<%}else if (LANGUAGE.GetCurrentLang() == "en"){ %>  
        <span class="albumchoose">Show album:</span>
        <a href="#" onclick="setAlbum(0); return false;" ><span>Once</span></a>
        <a href="#" onclick="setAlbum(1); return false;"><span>Today</span></a>
        <a href="#" onclick="setAlbum(2); return false;"><span>Limousin cattle</span></a>

<%}else if (LANGUAGE.GetCurrentLang() == "it"){ %>  
        <span class="albumchoose">Visualizza album:</span>
        <a href="#" onclick="setAlbum(0); return false;" ><span>C'era una volta</span></a>
        <a href="#" onclick="setAlbum(1); return false;"><span>Oggi</span></a>
        <a href="#" onclick="setAlbum(2); return false;"><span>Bovini Limousine</span></a>

<%} %>

    </div>
</div>

</div>


<div class="homeW_inwordW">

<%--
<h2>(GENEZA) NEKJE V GORIŠKIH BRDIH</h2>
<h4>17. Avg. 2008</h4>
<p>
    Ko sonce začne izgubljati svojo moč, ljudje v družbi prijateljev ob kozarcu dobrega
    vina spet oživimo. Pogovor steče kar sam od sebe. Tokrat o lepoti in okusu. Spomnili
    smo se na pogled lepih in sočnih rdečih paradižnikov. Tistih, ki nato razočarajo s
    svojim okusom. Tako kot nas z okusom običajno razočarajo solata, meso in ostala
    hrana s trgovskih polic. Družba trmastih ljudi je imela teh razočaranj dovolj. Zato
    smo se odločili nekaj spremeniti ...
</p>


<h2>(PRVI SADOVI) KORADA, GORIŠKA BRDA</h2>
<h4>3. Feb. 2011</h4>
<p>
    Tri leta je trajalo, da smo idejo spremenili v dejanje. Pri tem smo na lastni koži
    izkusili, kako težko je biti kmet. A nismo obupali. Zato smo ponosni, da vam danes
    lahko predstavimo prve sadove našega truda: teletino goveda Limousine. Govedo se
    pase v idiličnem okolju pašnikov na hribu Korada v Goriških Brdih, kjer jim družbo
    delajo le tri prazne domačije. Gosta dlaka jih ščiti pred mrazom, zato veš čas, tudi
    zime, preživijo na prostem, v naravi.
</p>


<h2>(UVODNA POŠILJKA) LJUBLJANA</h2>
<h4>10. Feb. 2011</h4>
<p>
    Znancem in vsem, ki ste pokazali zanimanje, smo dostavili prvo pošiljko mesa
    goveda Limousine. Dostavili smo ga v vakuumskem pakiranju, v katerem meso
    ostane dobro za pripravo 14-20 dni. Pripravite in specite ga tako, kakor želite. Lahko
    tudi po priloženem receptu.
</p>


<h2>(OBETI) GORIŠKA BRDA</h2>
<h4>14. Apr. 2011</h4>
<p>
    Ideja, s katero smo pred tremi leti začeli našo pot, je prerastla svojo zibko. Ljubezen
    do dela in druženja nas vodita k novim izzivom. Z okoliškimi eko pridelovalci smo
    načeli debato, v upanju, da bomo na našem spletnem mestu lahko kmalu ponudili
    tudi tiste okusne sadove neokrnjene narave, ki jih pridelajo oni: sadje, zelenjavo,
    vino, vodo - nasploh vse dobro, kar je vredno deliti.
</p>



<h2>(EKO REPUBLIKA) LJUBLJANA</h2>
<h4>21. Jun. 2011</h4>
<p>
    Vizija sodelovanj s podobno mislečimi eko pridelovalci nas pelje preko mej Korade
    in Goriških Brd, v Eko republiko, v brezmejno deželo vseh ljubiteljev zdrave, bio
    hrane ter neokrnjene narave.
    <br />
    Dobrodošli!
</p>

--%>
<%--<SM:HomeTimeline ID="timeline" runat="server" /> 
<div class="clearing"></div>
<h2 class="ph_h2"><span class="ph_h2_text"><span class="ph_h2_picL"></span><span class="ph_h2_picR"></span>V SLIKI</span></h2>



<h2 class="ph_h2"><span class="ph_h2_text"><span class="ph_h2_picL"></span><span class="ph_h2_picR"></span>KONTAKT</span></h2>
--%>

<%--    <div class="homeT">
        <div class="homeContent clearfix">
        <div class="homeTwocols clearfix">
        <div class="homeMaincol">
            <div class="homeBoxMW">
                    <EM:ModuleNew ID="mn2" runat="server" />            
                    <EM:CwpZone ID="wpz_2" runat="server">
                    </EM:CwpZone>
                    <SM:ProductItem ID="objProductItem2" runat="server" />


        
                <div class="clearing">&nbsp;</div>
            </div>
        </div>
        <div class="homeRightcol">
        <div class="homeBoxRW">
                    <EM:ModuleNew ID="mn3" runat="server" />            
                    <EM:CwpZone ID="wpz_3" runat="server">
                    </EM:CwpZone>
                    <SM:ProductItem ID="objProductItem3" runat="server" />


    
            <div class="clearing">&nbsp;</div>
        </div>
        </div>
        </div>
        <div class="homeLeftcol">
        <div class="homeBoxLW">
                    <EM:ModuleNew ID="mn1" runat="server" />
                    <EM:CwpZone ID="wpz_1" runat="server">
                    </EM:CwpZone>

                    <SM:ProductItem ID="objProductItem1" runat="server" />


        <div class="clearing">&nbsp;</div>
        </div>
        </div>
        </div>
        <div class="clearing">&nbsp;</div>
    </div>--%>


<%--    <div class="homeM">
        <div class="homeContent clearfix">
        <div class="homeTwocols clearfix">
        <div class="homeMaincol">
            <div class="homeBoxMW">
            <EM:ModuleNew ID="mn5" runat="server" />            
            <EM:CwpZone ID="wpz_5" runat="server">
            </EM:CwpZone>

       
                <div class="clearing">&nbsp;</div>
            </div>
        </div>
        <div class="homeRightcol">
        <div class="homeBoxRW">
            <EM:ModuleNew ID="mn6" runat="server" />            
            <EM:CwpZone ID="wpz_6" runat="server">
            </EM:CwpZone>

  
            <div class="clearing">&nbsp;</div>
        </div>
        </div>
        </div>
        <div class="homeLeftcol">
        <div class="homeBoxLW">
            <EM:ModuleNew ID="mn4" runat="server" />            
            <EM:CwpZone ID="wpz_4" runat="server">
            </EM:CwpZone>


        <div class="clearing">&nbsp;</div>
        </div>
        </div>
        </div>
        <div class="clearing">&nbsp;</div>
    </div>--%>


</div>
<div class="clearing"></div>




    </div>

    
