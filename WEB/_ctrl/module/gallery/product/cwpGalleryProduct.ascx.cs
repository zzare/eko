﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SM.EM.BLL;
using System.Collections.Generic;

namespace SM.EM.UI.Controls
{
    public partial class cwpGalleryProduct : BaseControl_CmsWebPart  
    {
        public Guid CwpID;

        public List<vw_ImageLang> ImageList;


        protected void Page_Load(object sender, EventArgs e)
        {
            this.Visible = true;

        }


        public override void LoadData()
        {

            if (CwpID == Guid.Empty)
                return ;

            InitEditMode();

            if (cwp == null)
                cwp = CMS_WEB_PART.GetWebpartByID(CwpID);


            ImageList = GALLERY.GetImageListByCwpPaged(CwpID, LANGUAGE.GetCurrentLang(), true, 1, 100, SM.BLL.Common.ImageType.EmenikGalleryThumbDefault).ToList();


            this.Visible = true;

        }

        protected override void OnPreRender(EventArgs e)
        {
            this.Visible = false;
            if (!ParentPage.IsModeCMS)
                return;


            LoadData();

            base.OnPreRender(e);
        }

        protected void InitEditMode()
        {


            // edit mode
            if (ParentPage.IsModeCMS)
            {
                divEdit.Visible = true;

            }
            else
            {
                divEdit.Visible = false;
            }

        }


        protected string RenderImageList() {
            if (!ParentPage.IsModeCMS)
                return "";

            if (CwpID == Guid.Empty )
                return "";


            
            return ViewManager.RenderView("~/_ctrl/module/gallery/_viewGalleryImageList.ascx", cwp, new object[] { true, null, ImageList });
        
        }


        public void OnNewImageClick(object sender, EventArgs e)
        {
            //CwpEdit edit = ParentPage.EditImageNew ;
            //if (edit == null)
            //    return;
            //edit.ID = -1;

            //if (cwp == null)
            //    cwp = CMS_WEB_PART.GetWebpartByID(CwpID);

            //edit.CwpID = cwp.CWP_ID;
            //edit.cwp = cwp;
            //edit.LoadData();


            objImageNew.ShowPickGallery = false;
            objImageNew.IDref = -1;

            if (cwp == null)
                cwp = CMS_WEB_PART.GetWebpartByID(CwpID);

            objImageNew.CwpID = cwp.CWP_ID;
            objImageNew.cwp = cwp;
            objImageNew.LoadData();

        }


        protected string RenderCallback() {
            string ret = "";

            if (ImageList != null && ImageList.Any())
                return ret;

            // return only if only one image
            ret = ", function(){reloadProductDetail('"+  cwp.REF_ID.ToString() +"');}";

            return  ret;
        
        
        }


    }
}