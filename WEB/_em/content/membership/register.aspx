﻿<%@ Page Language="C#" ValidateRequest="false" MasterPageFile="~/_em/templates/fall/fall.master" AutoEventWireup="true" CodeFile="register.aspx.cs" Inherits="_em_content_membership_register" Title="Untitled Page" %>
<%@ Register TagPrefix="SM" TagName="Register" Src="~/_em/content/membership/_ctrl/_cntRegisterCustomerShort.ascx"   %>


<asp:Content ID="Content2" ContentPlaceHolderID="cph" Runat="Server">

                <div class="registerContW">
                    <h1><%= GetGlobalResourceObject("Modules", "RegisterTitle")%></h1>

                    <asp:PlaceHolder ID="phRegister" runat="server">

                        <SM:Register ID="objRegister" runat="server" />

                    </asp:PlaceHolder>


                    <asp:PlaceHolder ID="phConfirmation" runat="server" Visible= "false" >
                    
                        <% if (LANGUAGE.GetCurrentLang() == "si"){%>
                        <h2>Uspešno ste registrirani v sistem</h2>
                        <p>
                        Za dokončno potrditev registracije morate potrditi svoj e-naslov. <br />
                        To naredite tako, da kliknete na potrditveno povezavo v e-sporočilu, ki ste ga prejeli na vaš e-naslov.        
                        </p>
<%}else if (LANGUAGE.GetCurrentLang() == "en"){ %>  
                        <h2>Uspešno ste registrirani v sistem</h2>
                        <p>
                        Za dokončno potrditev registracije morate potrditi svoj e-naslov. <br />
                        To naredite tako, da kliknete na potrditveno povezavo v e-sporočilu, ki ste ga prejeli na vaš e-naslov.        
                        </p>

<%}else if (LANGUAGE.GetCurrentLang() == "it"){ %>  
                        <h2>Uspešno ste registrirani v sistem</h2>
                        <p>
                        Za dokončno potrditev registracije morate potrditi svoj e-naslov. <br />
                        To naredite tako, da kliknete na potrditveno povezavo v e-sporočilu, ki ste ga prejeli na vaš e-naslov.        
                        </p>

<%} %>
                        
                    </asp:PlaceHolder>
                    
                    <asp:PlaceHolder ID="phLogin" runat="server" Visible= "false" >
                    
                        <h2>V sistem ste že registrirani.</h2>        
                    
                    </asp:PlaceHolder>  
                    <div class="clearing"></div>  
            </div>
</asp:Content>
