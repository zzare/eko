﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewLoginView.ascx.cs" Inherits="_ctrl_login_viewLoginView" %>
<%@ Register TagPrefix="SM" TagName="LoggedIn" Src="~/_ctrl/login/_viewLoggedIn.ascx"   %>
<%@ Register TagPrefix="SM" TagName="LoggedOut" Src="~/_ctrl/login/_viewLoggedOut.ascx"   %>

 <script type="text/javascript" >


     function logOut(ru, sc, fc, uc) {
         if (ru == null)
             ru = location.href;
         openModalLoading('<%= GetGlobalResourceObject("Modules", "signoutProgress")%>');

         //Sys.Services.AuthenticationService.logout(ru, sc, fc, uc);
         WebServiceP.Logout(onSuccessLogout, onError, ru);
     }


     function onSuccessLogout(ret, ru) {
         if (ret.Code != 1) {
             setModalLoaderErr('<%= GetGlobalResourceObject("Modules", "errorAjax")%>', true);
//             return;
         }

         if (ru == null || ru == undefined) {
             window.location.href = window.location.href;
         } else {
             window.location.href = ru;
         }
     }            
                
</script>                


<SM:LoggedIn ID="objLoggedIn" runat="server" Visible = "false" />
<SM:LoggedOut ID="objLoggedOut" runat="server" Visible = "false"/>
