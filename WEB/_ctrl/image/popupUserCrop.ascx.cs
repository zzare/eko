﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
namespace SM.UI.Controls
{
    public partial class popupUserCrop : BaseControl
    {
        public int ImgID { get; set; }
        public int ImgTypeID { get; set; }
        public string ImgTargetClientID { get; set; }
        public string TargetVirtualURLClientID { get; set; }
        public string CroppedImageUrl { get { return hfImageUrlVirt.Value; } set { hfImageUrlVirt.Value = value; } }
        public bool AutoSaveImage { get; set; }
        protected int WidthForce;
        protected int HeightForce;


        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);


        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }



        public void InitPopup() {
            // load big image
            if (ImgID <= 0) return;
            IMAGE_ORIG imgOrig = IMAGE.GetImageByID(ImgID);
            imgBig.Src = imgOrig.IMG_URL;

            // load init data
            hfImg.Value = ImgID.ToString();
            hfType.Value = ImgTypeID.ToString();

            // get image type data
            IMAGE_TYPE imageType = IMAGE_TYPE.GetImageTypeByID(ImgTypeID);
            if (imageType.WIDTH > 0) 
                WidthForce = imageType.WIDTH;
            else
                WidthForce = imageType.W_MAX;
            if (imageType.HEIGHT  > 0) 
                HeightForce = imageType.HEIGHT;
            else
                HeightForce = imageType.H_MAX;


            }

        public void OpenPopup() {

            InitPopup();

            // show popup
            mpeUserCrop.Show();


        }
    }
}