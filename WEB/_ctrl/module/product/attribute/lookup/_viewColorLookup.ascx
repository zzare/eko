﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewColorLookup.ascx.cs" Inherits="_ctrl_module_product_attribute_viewColorLookup" %>

<script language = "javascript" type="text/javascript">

    $(document).ready(function() {
        $('#<%= ViewID %> .item').click(function () {

            var c = $('<%= ContID %>');
            
            var code = $(this).children("input.hfCode").val();

            c.find("img.catImg").attr("src", ($(this).children("input.hfSrc").eq(0).val())).attr("alt", code);
            c.find("span.catCode").html(code);
            c.find("input.hfAttr").val($(this).children("input.hfAttr").val());

            closeModalLoader();
            return false;
        });
    });

</script>


<div id='<%= ViewID %>'>


    <div class="modPopup_header modPopup_headerEdit">
        <h4>IZBERI BARVO</h4>
        <p>Klikni na želeno barvo</p>
    </div>
    <div class="modPopup_main">
        <div  class="lkpAttributeColor">
        <asp:ListView ID="lvList" runat="server" >
            <LayoutTemplate>
                <asp:PlaceHolder ID="itemPlaceHolder" runat="server"></asp:PlaceHolder>
            </LayoutTemplate>
                             
            <ItemTemplate >
                <div class="item">

                    <img src='<%# SM.BLL.Common.ResolveUrl(Eval("UrlBig").ToString()) %>' alt='<%# Eval("Code") %>'  />
                    <span ><%# Eval("Code") %></span>

                    <input class="hfAttr" type="hidden"  value='<%# Eval("AttributeID") %>' />
                    <input class="hfSrc" type="hidden"  value='<%# SM.BLL.Common.ResolveUrl(Eval("UrlSmall").ToString()) %>' />
                    <input class="hfCode" type="hidden"  value='<%# Eval("Code") %>' />
                    <div class="clearing"></div>
                </div>          
            </ItemTemplate>
    
        </asp:ListView>
        </div>

    </div>   

    <div class="modPopup_footer"> 
        <div class="buttonwrapperModal">
    <%--        <a class="lbutton lbuttonConfirm" href="#" onclick="selectLookup(); return false"><span>Dodaj nov izdelek</span></a>--%>
            <a class="lbutton lbuttonDelete" href="#" onclick="closeModalLoader(); return false" ><span>Prekliči</span></a>
        </div>
    </div>
</div>