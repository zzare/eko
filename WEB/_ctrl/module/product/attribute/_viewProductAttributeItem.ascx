﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewProductAttributeItem.ascx.cs" Inherits="_ctrl_module_product_attribute_viewProductAttributeItem" %>

<select  name='<%= GroupAttribute.GroupAttID  %>'>
    <option value="" selected="selected" >- <%= GroupAttribute.Message  %> -</option>

    <asp:ListView ID="lvList" runat="server" >
        <LayoutTemplate>
            
                <asp:PlaceHolder ID="itemPlaceholder" runat="server"/>

        </LayoutTemplate>
        
        <ItemTemplate>
            <option value='<%# Eval("AttributeID")  %>' <%# RenderSelected(Eval("AttributeID")) %>><%# Eval("Title") %></option>
            
        </ItemTemplate>
    </asp:ListView>

</select>

