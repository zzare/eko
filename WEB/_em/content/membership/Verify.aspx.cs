﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SM.EM.UI;
using System.Text.RegularExpressions;

public partial class Verify : SM.EM.UI.BasePage_EMENIK 
{
    protected override void OnPreInit(EventArgs e)
    {
        if (PageName == "")
            PageName = "eko";

        //this.PageModule = -11;

        base.OnPreInit(e);

        //this.PageModule = -11; // set after preinit

    }

    protected override void OnInit(EventArgs e)
    {
        //this.PageModule = -11; // set after preinit

        CheckPermissions = false;
        base.OnInit(e);

    }
    protected void Page_Load(object sender, EventArgs e)
    {
        //Make sure that a valid querystring value was passed through
        string id = "";

        if (Request.QueryString["ID"] != null){
            id =  Server.UrlDecode (Request.QueryString["ID"]);
            id = SM.EM.Security.Encryption.DeHex(id);
            if (SM.EM.Helpers.IsGUID(id)){
                ConfirmUser(new Guid (id));
                return;
            }
        }

        // error parsing QS id
        litMessage.Text = "Napaka pri potrditvi email naslova.";

        //ConfirmUser(new Guid("EA2E6127-625E-4B75-AD6C-B1984C97994E"));
    }



    protected void ConfirmUser(Guid id){

       //ID exists and is kosher, see if this user is already approved    Get the ID sent in the querystring
        Guid userId = id;


       //Get information about the user
       MembershipUser userInfo  = Membership.GetUser(userId);
       if  (userInfo == null){
          //Could not find user!
          litMessage.Text = "Uporabnik ne obstaja.";
       }
       else{
          //User is valid, approve them
          userInfo.IsApproved = true;
          Membership.UpdateUser(userInfo);

          CustomerRep crep = new CustomerRep();
          var cust = crep.GetCustomerByID(new Guid(userInfo.ProviderUserKey.ToString()));
          cust.UserConfirmed = true;
          crep.Save();

          SM.EM.BLL.Membership.RemoveFromCache(userInfo.UserName);

          //Display a message
          litMessage.Text = "Vaš email je potrjen. Zdaj lahko nadaljujete in uspešno zaključite nakupovanje.";
       }
    }
}
