﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_cntChangePassword.ascx.cs" Inherits="_ctrl_content_membership_ctrl_cntChangePassword" %>
<%@ Register Namespace="SM.UI.Controls" TagPrefix="SM"  %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="EM" TagName="MailConfirmation" Src="~/_mailTemplates/_ctrlNewUserVerificationMail.ascx"  %>


<asp:Panel ID="panPassword" runat="server" DefaultButton="btChange">
        <table class="TABLE_quickregister" cellpadding="0">
            <tr>
                <td class="TD_right"><asp:Label runat="server" ID="lblPassword" AssociatedControlID="tbOldPassword" Text="Staro Geslo:" /></td>
                <td>
                    <asp:TextBox class="TBXlogin" runat="server" ID="tbOldPassword" TextMode="Password" /><br />
                </td>
            </tr>
            <tr class="tr_validation">
                <td></td>
                <td>
                    <asp:RequiredFieldValidator ID="valRequirePassword" runat="server" ControlToValidate="tbOldPassword" SetFocusOnError="true" Display="Dynamic"
                        ErrorMessage="* Vpiši želeno geslo." ToolTip="* Vpiši želeno geslo." ValidationGroup="chPass"></asp:RequiredFieldValidator>
                    <asp:CustomValidator OnServerValidate="ValidateOld" ValidationGroup="chPass" ID="CustomValidator1" runat="server" ErrorMessage="* Geslo ni pravilno"></asp:CustomValidator>
                        
                </td>
            </tr>
            <tr>
                <td class="TD_right"><asp:Label runat="server" ID="Label1" AssociatedControlID="tbNewPassword" Text="Novo Geslo:" /></td>
                <td>
                    <asp:TextBox class="TBXlogin" runat="server" ID="tbNewPassword" TextMode="Password" /><br />
                </td>
            </tr>
            <tr class="tr_validation">
                <td></td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="tbNewPassword" SetFocusOnError="true" Display="Dynamic"
                        ErrorMessage="* Vpiši želeno geslo." ToolTip="* Vpiši želeno geslo." ValidationGroup="chPass"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="tbNewPassword" SetFocusOnError="true" Display="Dynamic"
                    ValidationExpression=".{5,}" ErrorMessage="* Geslo mora biti dolgo vsaj pet (5) znakov." ToolTip="* Geslo mora biti dolgo vsaj pet (5) znakov."
                    ValidationGroup="chPass"></asp:RegularExpressionValidator>&nbsp;
                </td>
            </tr>
            <tr>
                <td class="TD_right"><asp:Label runat="server" ID="Label2" AssociatedControlID="tbConfirmPassword" Text="Potrdite novo Geslo:" /></td>
                <td>
                    <asp:TextBox class="TBXlogin" runat="server" ID="tbConfirmPassword" TextMode="Password" /><br />
                </td>
            </tr>
            <tr class="tr_validation">
                <td></td>
                <td>
                    <asp:CustomValidator OnServerValidate="ValidateCompare" ValidationGroup="chPass" ID="cvMatch" runat="server" ErrorMessage="* Potrditveno geslo se ne ujema z novim geslom"></asp:CustomValidator>
                </td>
            </tr>

           
        </table>
    <div style="padding:10px 0px 0px 0px;">
        <SM:LinkButtonDefault ID="btChange" CssClass="Astepnext" runat="server"   ValidationGroup="chPass"    onclick="btChangePass_Click"><span>ZAMENJAJ GESLO</span></SM:LinkButtonDefault>
    
    </div>
    <asp:PlaceHolder ID="phMsg" EnableViewState="false" Visible="false" runat="server">
        <asp:Literal ID="litMsg" runat="server"></asp:Literal>
    </asp:PlaceHolder>
    <div class="clearing"></div>
</asp:Panel>

