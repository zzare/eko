﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _ctrl_module_product_cart_viewShoppingCart : BaseControlView
{

    public object Data;
    public object[] Parameters;
    public List<ProductRep.Data.ShoppingCartItem> CartItems;
    protected decimal _totalSum = -1;
    public decimal GetTotalSum {
        get {
            if (_totalSum > -1)
                return _totalSum;
            _totalSum = 0;
            if (CartItems == null || !CartItems.Any())
                return _totalSum;
            _totalSum = (from c in CartItems select c.PriceTotal).Sum();

            return _totalSum;
        }
    }

    public decimal GetShipping
    {
        get
        {
            decimal shipp = SHOPPING_ORDER.GetShippingPriceByTotal(GetTotalSum);
            if (Order != null) {
                shipp = Order.CalculateShippingPrice(GetTotalSum);
            }
            return shipp;
        }
    }

    public decimal GetTotalSumWithShipping
    {
        get
        {
            // add shipping
            if (ShowPriceWithShipping)
            {
                decimal shipp = GetShipping;
                return GetTotalSum + shipp;
            }

            return GetTotalSum;
        }
    }
    protected decimal _getTotalTax = -1;
    public decimal GetTotalTax
    {
        get
        {
            if (_getTotalTax > -1)
                return _getTotalTax;
            _getTotalTax = 0;
            if (CartItems == null || !CartItems.Any())
                return _getTotalTax;
            _getTotalTax = (from c in CartItems select c.Tax ).Sum();

            if (ShowPriceWithShipping)
            {
                decimal shipp_tax = Convert.ToDecimal((GetShipping * 0.2m)); 
                _getTotalTax = _getTotalTax + shipp_tax;
            }


            return _getTotalTax;
        }
    }

    public Guid PageID;
    public bool IsPopup = true;
    public bool ShowEditDelete = true;
    public bool ShowPriceWithShipping = true;
    public bool ShowTax { get { return false; } }
    public SHOPPING_ORDER Order;



    protected void Page_Load(object sender, EventArgs e)
    {
        if (CartItems == null)
            LoadData();
    }



    public void LoadData()
    {
        if (CartItems == null)
        {
            if (Data == null)
            {
                //this.Visible = false;
                return;
            }
            CartItems = (List<ProductRep.Data.ShoppingCartItem>)Data;
            if (CartItems == null)
                return;

        }
        if (Parameters != null)
        {
            if (Parameters.Length > 0 && Parameters[0] != null)
                PageID = new Guid(Parameters[0].ToString());
            if (Parameters.Length > 1 && Parameters[1] != null)
                IsPopup = (bool)(Parameters[1]);
            if (Parameters.Length > 2 && Parameters[2] != null)
                ShowEditDelete  = (bool)(Parameters[2]);

        }

        // bind data
        lvList.DataSource = CartItems;
        lvList.DataBind();

        // load empty
        if (!CartItems.Any())
            phEmpty.Visible = true;


    }

    public string RenderLoadingVisible() {
        string ret = "";
        if (CartItems == null)
            ret = "style=\"display:block;\"";
        else
            ret = "style=\"display:none;\"";
        return ret;
    }

    public string RenderContainerVisible()
    {
        string ret = "";
//        if (CartItems != null)
            ret = "style=\"display:block;\"";
  //      else
    //        ret = "style=\"display:none;\"";
        return ret;
    }


    public string RenderProductAttributesPrice(object o)
    {
        string ret = "";
        string separator = "";
        ProductRep.Data.ShoppingCartItem cartItem = (ProductRep.Data.ShoppingCartItem)o;

        var list = cartItem.AttrList.Where (w=>w.GrType ==ATTRIBUTE.Common.GrType.RBL_PRICE);

        foreach (ProductRep.Data.ItemGroupAttribute attr in list)
        {
            if (!string.IsNullOrEmpty(ret))
                ret += separator;

            string val = attr.PTitle;


            ret += string.Format("<br/> {0}", val);

            //ret +=  val;
            //ret += attr.Title;   
        }

        return ret;
    }

    public string RenderProductAttributes(object o)
    {
        string ret = "";
        string separator = " | ";
        ProductRep.Data.ShoppingCartItem cartItem = (ProductRep.Data.ShoppingCartItem)o;

        var list = cartItem.AttrList.Where(w => w.GrType != ATTRIBUTE.Common.GrType.RBL_PRICE);

        foreach (ProductRep.Data.ItemGroupAttribute attr in list)
        {
            if (!string.IsNullOrEmpty(ret))
                ret+=separator ;

            string val = attr.Title;

            if (attr.GrType == ATTRIBUTE.Common.GrType.RANGE) {
                if (attr.Value != null)
                    val = attr.Value.Value.ToString();
            }
            else if (attr.GrType == ATTRIBUTE.Common.GrType.COLOR)
            {
                val = attr.Code;
            }
            
            ret += attr.GroupTitle + ": " + val ; 
            //ret += attr.Title;   
        }

        return ret;
    }



}
