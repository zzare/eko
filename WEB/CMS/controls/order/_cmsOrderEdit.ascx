﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_cmsOrderEdit.ascx.cs" Inherits="SM.UI.Controls._cmsOrderEdit" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="SM" TagName="OrderItemList" Src="~/c/user/_ctrl/_cntOrderItemListTicket.ascx"  %>

<div class = "qContEdit clearfix">
        
    <%--<asp:Button ValidationGroup="ordredit" ID="btSave" runat="server" Text="Save user" onclick="btSave_Click" CssClass="btnOrange " />
--%>
    <asp:Button ID="btCancel" runat="server" Text="Cancel" onclick="btCancel_Click" CssClass="btnRed"/>


    <br />

    
    <asp:Panel ID="panEdit" runat="server" Visible="false" CssClass="floatR">
        <div>
    
            Št. naročila: <asp:TextBox ID="tbOrderRefID" ReadOnly="true" runat="server"></asp:TextBox>
            <br />
            Uporabnik: <a runat="server" id="lnkUser" target="_blank" title="edit user" >user</a>
            <br />
            Znesek: <asp:Literal ID="litTotal" runat="server"></asp:Literal>
            <br />
            Način plačila: <asp:Literal ID="litPaymentType" runat="server"></asp:Literal>
            <br />
            <a target="_blank" id="lnkTicket" runat="server" title="edit ticket" >Ticket (še ne dela prav)</a>
            <br />
        
        </div>
    </asp:Panel>
    


    <h2>Status: <asp:Literal ID="litStatus" runat="server"></asp:Literal></h2>
    
    <br />   
    <h3>Spremeni status</h3>
    
    <asp:PlaceHolder ID="phSetPayed" runat="server">
    
        <asp:Button OnClick="btSetPayed_Click" ID="btSetPayed" OnClientClick="return confirm('Ste prepričani, da želite nadaljevati? Sprememba statusa nazaj ni možna');" runat="server" Text="Naročilo je plačano >>" />
        datum plačila:
        <asp:TextBox  ID="tbDatePayed" runat="server"></asp:TextBox>
        <asp:ImageButton ID="btDatePayed" ImageUrl="~/CMS/_res/images/button/calendar.png" runat="server" />&nbsp
        <cc1:CalendarExtender ID="ceDatePayed" runat="server" TargetControlID="tbDatePayed" PopupPosition="BottomLeft" PopupButtonID ="btDatePayed">
        </cc1:CalendarExtender>                            
        <cc1:MaskedEditExtender ID="meeDatePayed" runat="server" TargetControlID="tbDatePayed" Mask="99/99/9999" MessageValidatorTip="true" 
                MaskType="Date" DisplayMoney="Left" AcceptNegative="Left" ErrorTooltipEnabled="True"  CultureName="sl-SI" />                            
        <cc1:MaskedEditValidator ID="mevDatePayed" runat="server" ControlExtender="meeDatePayed" ControlToValidate="tbDatePayed"  InvalidValueMessage="* Datum je neveljaven"
            Display="Dynamic" TooltipMessage="" EmptyValueBlurredText="" InvalidValueBlurredMessage=""  ValidationGroup="payment" />   
        <asp:RequiredFieldValidator ValidationGroup="ordredit" ControlToValidate="tbDatePayed"  ID="rfvDatePayed" runat="server" ErrorMessage="* Izberi datum plačila"></asp:RequiredFieldValidator>
        <br />
    </asp:PlaceHolder>
    
    
    <asp:Button OnClick = "btSetFinished_Click" ID="btSetFinished" runat="server" Text="Naročilo je končano >>" />
    
    <asp:PlaceHolder ID="phSetPayedBack" runat="server">
    
        <asp:Button OnClick="btSetPayedBack_Click" ID="btSetPayedBack" runat="server" Text="<< Nazaj v izdelavo" />

    </asp:PlaceHolder>
    
    <asp:PlaceHolder ID="phCancel" runat="server" >
    
        <asp:Button ValidationGroup="none" OnClick="btCancelOrder_Click" OnClientClick="return confirm('Ste prepričani, da želite PREKLICATI naročilo?');" ID="btCancelOrder" runat="server" Text="<< PREKLIČI " />

    </asp:PlaceHolder>    
    
    <h2>Podrobnosti naročila</h2>
    <SM:OrderItemList ID="objOrderItemList" runat="server" />

    
</div>

