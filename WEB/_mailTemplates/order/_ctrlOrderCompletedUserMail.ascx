﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_ctrlOrderCompletedUserMail.ascx.cs" Inherits="_mailTemplates_ctrlOrderCompletedUserMail" %>

<table width="668" cellpadding="0" cellspacing="0" border="0" style="background:#fff;">
    <tbody>
        <tr>
            <td>
                <%--<img align="right" alt="eko" src='<%= SM.BLL.Common.ResolveUrl("~/_em/templates/_custom/kolpasan/_inc/images/mailtemplate/kolpa_mailtemp_head.jpg") %>' />
                <br />    
                --%>Pozdravljeni!
                <br />    
                <br />     
                Sporočamo vam, da je vaše naročilo številka <%= SHOPPING_ORDER.Common.RenderShoppingOrderNumber(Order)%> <%= SHOPPING_ORDER.Common.RenderOrderStatus(Order.OrderStatus) %>.
                <br />    
                <br />    
                <a href='<%= SM.BLL.Common.ResolveUrl(SM.EM.Rewrite.OrderProductUrl(em_user.UserId, Order.OrderID, 5, "v=1" ))  %>'>Podrobnosti naročila si lahko ogledate tukaj</a>
                <br />    
                <br />

                Lep pozdrav,
                <%= em_user.PAGE_TITLE  %>
                <br />
                <br /><br />
            </td>
        </tr>
        <%--<tr>
            <td>
                <img alt="eko" src='<%= SM.BLL.Common.ResolveUrl("~/_em/templates/_custom/kolpasan/_inc/images/mailtemplate/kolpa_mailtemp_foot.jpg") %>' />
            </td>
        </tr>--%>

    </tbody>
</table>