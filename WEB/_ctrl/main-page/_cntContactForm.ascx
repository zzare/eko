﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_cntContactForm.ascx.cs" Inherits="SM.CMS.UI.Controls._cntContactForm" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<%--<asp:UpdatePanel ID="upContact" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
    <ContentTemplate>--%>
        <asp:MultiView ID="mvContact" runat="server" ActiveViewIndex="0">
            <asp:View ID="vForm" runat="server">
                <table class="TABLEcontactform">
                <tr>
                    <td class="td_first"><asp:Literal id="lName" runat="server" Text='<%$ Resources:Name %>'></asp:Literal></td>
                    <td>
                        <asp:TextBox ValidationGroup="contForm" ID="tbName" runat="server"  CssClass="TBXcontactform"></asp:TextBox><br />
<%--                        <cc1:ValidatorCalloutExtender ID="vceName" runat="server" TargetControlID="valName">
                        </cc1:ValidatorCalloutExtender>--%>
                    </td>
                </tr>
                <tr class="tr_validation">
                    <td></td>
                    <td>
                        <asp:RequiredFieldValidator  EnableClientScript="true" ValidationGroup="contForm" id="valName" ControlToValidate="tbName" runat="server" ErrorMessage='<%$ Resources:EnterName %>' Display="Static"></asp:RequiredFieldValidator>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="td_first"><asp:Literal id="lEmail" runat="server" Text='<%$ Resources:Email %>'></asp:Literal></td>
                    <td>
                        <asp:TextBox ValidationGroup="contForm" ID="tbEmail" runat="server" CssClass="TBXcontactform"></asp:TextBox><br />
<%--                        <cc1:ValidatorCalloutExtender ID="vceEmail" runat="server" TargetControlID="valEmail">
                        </cc1:ValidatorCalloutExtender>
                        <cc1:ValidatorCalloutExtender ID="vceEmailR" runat="server" TargetControlID="valEmailR">
                        </cc1:ValidatorCalloutExtender>--%>
                    </td>
                </tr>
                <tr class="tr_validation">
                    <td></td>
                    <td>                        
                        <asp:RequiredFieldValidator Display="Dynamic" ValidationGroup="contForm" id="valEmail" ControlToValidate="tbEmail" runat="server" ErrorMessage='<%$ Resources:EnterEmail %>' ></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator Display="Dynamic" ValidationGroup="contForm" ID="valEmailR" ControlToValidate="tbEmail" runat="server" ErrorMessage='<%$ Resources:EnterValidEmail %>'  ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="td_first"><asp:Literal id="lSubject" runat="server" Text='<%$ Resources:Subject %>'></asp:Literal></td>
                    <td>
                        <asp:TextBox ValidationGroup="contForm" ID="tbSubject" runat="server" CssClass="TBXcontactform"></asp:TextBox><br />
<%--                        <cc1:ValidatorCalloutExtender ID="vceSubject" runat="server" TargetControlID="valSubject">
                        </cc1:ValidatorCalloutExtender>--%>
                    </td>
                </tr>
                <tr class="tr_validation">
                    <td></td>
                    <td>
                        <asp:RequiredFieldValidator Display="Dynamic" ValidationGroup="contForm" id="valSubject" ControlToValidate="tbSubject" runat="server" ErrorMessage='<%$ Resources:EnterSubject %>' ></asp:RequiredFieldValidator>
                        &nbsp;
                    </td>
                </tr>

                <tr>
                    <td class="td_first" style="vertical-align:top;"><asp:Literal id="lBody" runat="server" Text='<%$ Resources:Content %>'></asp:Literal></td>
                    <td>
                        <asp:TextBox ValidationGroup="contForm" ID="tbBody" runat="server" TextMode="MultiLine" Rows="8" CssClass="TBXcontactform"></asp:TextBox><br />
<%--                        <cc1:ValidatorCalloutExtender ID="vceBody" runat="server" TargetControlID="valBody">
                        </cc1:ValidatorCalloutExtender>--%>
                    </td>
                </tr>
                <tr class="tr_validation">
                    <td></td>
                    <td>
                        <asp:RequiredFieldValidator ValidationGroup="contForm" id="valBody" ControlToValidate="tbBody" runat="server" ErrorMessage='<%$ Resources:EnterContent %>' Display="Dynamic"></asp:RequiredFieldValidator>
                        &nbsp;
                    </td>
                </tr>

                <tr>
                    <td></td>
                    <td>
                        <asp:LinkButton id="btSend" CssClass="BTN_quickregister BTN_contactformsend" runat="server" Text='<%$ Resources:Send %>' OnClick="btSend_Click" ValidationGroup="contForm"/>
                    </td>
                </tr>
                </table>
                
            </asp:View>
            <asp:View ID="vSent" runat="server">
                <h2><asp:Literal runat="server" Text='<%$ Resources:SentConfirmation %>' ></asp:Literal></h2>
                
                
            </asp:View>
        
        </asp:MultiView>
<%--    </ContentTemplate>
</asp:UpdatePanel>--%>


