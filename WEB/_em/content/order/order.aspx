﻿<%@ Page Language="C#" ValidateRequest="false" MasterPageFile="~/_em/templates/Fall/Fall.master"  AutoEventWireup="true" CodeFile="order.aspx.cs" Inherits="_em_content_order_order" Title="Untitled Page" %>
<%@ Register TagPrefix="SM" TagName="StepCart" Src="~/_em/content/order/_ctrl/_stepShoppingCart.ascx"   %>
<%@ Register TagPrefix="SM" TagName="StepPayment" Src="~/_em/content/order/_ctrl/_stepPaymentType.ascx"   %>
<%@ Register TagPrefix="SM" TagName="StepShipping" Src="~/_em/content/order/_ctrl/_stepShippingEko.ascx"   %>
<%@ Register TagPrefix="SM" TagName="StepSummary" Src="~/_em/content/order/_ctrl/_stepSummary.ascx"   %>
<%@ Register TagPrefix="SM" TagName="StepFinished" Src="~/_em/content/order/_ctrl/_stepFinished.ascx"   %>
<%@ Register TagPrefix="SM" TagName="Navigation" Src="~/_em/content/order/_ctrl/_ctrlNavigation.ascx"   %>


<asp:Content ID="Content2" ContentPlaceHolderID="cph" runat="server">

<div class="order clearfix">
<div class="mainL">
        
    <SM:Navigation id="objNavigation" runat="server"></SM:Navigation>
                       
</div>

<div class="mainR">   



    <SM:StepCart ID="StepCart" runat="server" Visible="false" />
    <SM:StepPayment ID="StepPayment" runat="server" Visible="false" />
    <SM:StepShipping ID="StepAddress" runat="server" Visible="false" />
    <SM:StepSummary  ID="StepSummary" runat="server" Visible="false" />
    <SM:StepFinished ID="StepConfirmation" runat="server" Visible="false" />
            
</div>
</div>

</asp:Content>



