﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="editModulePublic.ascx.cs" Inherits="editModulePublic" %>

<script type="text/javascript">

    var cwpID = null;
    var pollID = null;
    var conID = null;
    var conBody = null;
    var imgID = null;

    var mpl = 0;
    var g_resUrl = '<%= Page.ResolveUrl("~/") %>';

    jQuery.fn.init_mpl = function() {
        return this.each(function() {
            $(this).dialog({
                autoOpen: false,
                bgiframe: true,
                //height: 600,
                zIndex: 150001,
                autoResize: true,
                closeOnEscape: false,
                draggable: false,
                resizable: false,
                modal: true
            });
        });
    };
    
    jQuery.fn.sm_animate_change = function() {
        return this.each(function() {
            $(this).effect("highlight", { color: "#FF6600" }, 2000);
        });
    };
    
    
   </script>



<div id="divModalLoading" style="display:none;vertical-align: middle;" title="Nalagam...">
    <img alt="nalagam..." style="position:relative;width:66px;height:66px;left:50%;margin-left:-33px; margin-top:20px" title="nalagam..." src='<%= Page.ResolveUrl("~/cms/_res/images/icon/ajax-loader.gif") %>' />
</div>
<div id="divModalLoader" class="divModalLoader" style="display:none;" title="Nalagam...">
</div>
<div id="divModalMsg" style="display:none;" title="1dva3">
</div>
