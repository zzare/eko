﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

[PartialCaching(100, "", "PageSize", "")]
public partial class _ctrl_main_page_cntLastPagesDetailedList : BaseControl 
{
    private int _pagesize = 5;
    public int PageSize { get { if (_pagesize < 1) return 3; return _pagesize; } set { _pagesize = value; } }

    private string  _pageName = "";
    private string _lastLanguage = "";
    private EM_USER _currentUser;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            LoadData();

    }


    protected void LoadData() {
        lvList.DataSource = EM_USER.GetLastPublishedPages(PageSize, SM.BLL.Common.Emenik.Data.PortalID() );
        lvList.DataBind();    
    
    }


    protected void lvList_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            // load themes listview for master page
            ListView lvListSub = (ListView)e.Item.FindControl("lvListSub");
            if (lvListSub == null) return;
            ListViewDataItem item = (ListViewDataItem)e.Item;
            _currentUser  = item.DataItem  as EM_USER;

            List<SITEMAP_LANG> list = SITEMAP.GetSitemapLangsForUser(_currentUser.UserId);
            _pageName = _currentUser.PAGE_NAME;
            _lastLanguage = "";

            lvListSub.DataSource = list;
            lvListSub.DataBind();
        }
    }

    protected string RenderMenulink(object item)
    {
        string ret = "";
        SITEMAP_LANG  sml = item as SITEMAP_LANG ;

        ret = string.Format("<a target=\"_blank\" style=\"font-size:13px;\" href=\"{0}\" >{1}</a>", SM.EM.Rewrite.EmenikUserUrl(_currentUser, sml.SMAP_ID, sml.SML_DESC), sml.SML_TITLE);

        return ret;
    }

    protected string RenderLanguage(object item, int index)
    {
        string ret = "";

        SITEMAP_LANG sml = item as SITEMAP_LANG;

        // render only if last one was different than current
        if (!string.IsNullOrEmpty(_lastLanguage) && _lastLanguage == sml.LANG_ID)
            return ret;

        ret =  sml.LANG_ID + " : " ;
        if (index != 0)
            ret = "<br>" + ret; 

        _lastLanguage = sml.LANG_ID;

        return ret;
    }

    protected string RenderPageMenuDetails(object item, int index)
    {
        if (index != 0)
            return "";

        SITEMAP_LANG  sml = item as SITEMAP_LANG ;

        CONTENT c = CONTENT.GetFirstContentBySitemap(sml.SMAP_ID, sml.LANG_ID, true );
        if (c == null)
            return "";

        string ret = "";
        if (sml == null)
            return ret;

        ret =  SM.EM.Helpers.CutString(SM.EM.Helpers.StripHTML(Server.HtmlDecode(c.CONT_BODY)), 180, " ...") ;
        ret = "<p>" + ret + "</p>";

        return ret;

    }


    protected string RenderPageDetails(object item) {
        EM_USER usr = item as EM_USER;


        CONTENT c = CONTENT.GetFirstContentByPageName(usr.PAGE_NAME);

        if (c == null)
            return "";

        string ret = "";
        if (usr == null)
            return ret;



        ret = SM.EM.Helpers.CutString( SM.EM.Helpers.StripHTML (  Server.HtmlDecode ( c.CONT_BODY)),180, " ..."); 

        return ret;

    }

}
