﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewPollResult.ascx.cs" Inherits="_ctrl_module_poll_viewPollResult" %>

<%--<script language="javascript" type="text/javascript">
    $(document).ready(function() {
        $("div[id$=divAnswers] img").each(function() {
            var perc = $(this).attr("val") + '%';
            $(this).css({ width: "0%" }).animate({ width: perc }, 'slow');
        });
    });
</script>
--%>    

<asp:ListView ID="lvList" runat="server">
    <LayoutTemplate>
        <asp:PlaceHolder ID="itemPlaceholder" runat="server"/>
    </LayoutTemplate>

    <ItemTemplate>
        <span class="pollChoiceResult"><%# Eval("Choice") %></span> - <%# (int) GetPercent((POLL_CHOICE)Container.DataItem, "")   %>% 
        <br />št. glasov: <%# ((POLL_CHOICE)Container.DataItem).VoteCountMale + ((POLL_CHOICE)Container.DataItem).VoteCountFemale%> | <%= GetGlobalResourceObject("Modules", "men")%>: <%# (int)GetPercent((POLL_CHOICE)Container.DataItem, "M")%>% <%= GetGlobalResourceObject("Modules", "women")%>: <%#(int)GetPercent((POLL_CHOICE)Container.DataItem, "F")%>%
        <div class="cPollChart">
            <div style="<%# RenderWidth((POLL_CHOICE)Container.DataItem, "") %>" val='<%# (int)GetPercent((POLL_CHOICE)Container.DataItem, "")   %>' 
                valM='<%# (int)GetPercent((POLL_CHOICE)Container.DataItem, "M")   %>' valF='<%# (int)GetPercent((POLL_CHOICE)Container.DataItem, "F")   %>' class="pollChart" ></div>
        </div>
        
    </ItemTemplate>

</asp:ListView>
<div class="cPollRes">
    <%= GetGlobalResourceObject("Modules", "Results")%>:&nbsp;
    <a class="sel" href="#" onclick="animateChart('<%= PollID %>', 'val', this); return false" ><%= GetGlobalResourceObject("Modules", "combined")%></a>&nbsp;
    <a href="#" onclick="animateChart('<%= PollID %>', 'valF', this); return false" ><%= GetGlobalResourceObject("Modules", "women")%></a>&nbsp;
    <a href="#" onclick="animateChart('<%= PollID %>', 'valM', this); return false" ><%= GetGlobalResourceObject("Modules", "men")%></a>
</div>

