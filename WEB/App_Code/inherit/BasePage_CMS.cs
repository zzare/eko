﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for BasePage
/// </summary>
namespace SM.EM.UI.CMS
{

    public class BasePage_CMS : BasePage 
    {
        Literal litErrorMessage = new Literal();
        Literal litStatus = new Literal();

        public bool HandleErrors( Exception e)
        {

            if (e != null)
            {
                if (typeof(ArgumentException).IsAssignableFrom(e.GetType()))
                {
                    SetError(e.Message);
                }
                else
                {
                    SetError(e.Message);
//                    litErrorMessage.Text = e.Message;// "An error occured. Please check data and try again.";
                }
                return true;

            }
            else
                return false;

        }

        protected void SetStatus(string sts)
        {
            BaseMasterPage mp = (BaseMasterPage)Page.Master;
            mp.StatusText = sts;
        }
        protected void SetError(string sts)
        {
            BaseMasterPage mp = (BaseMasterPage)Page.Master;
            mp.ErrorText = sts;
        } 

        protected void RefreshCMSSitemapProvider(string qs, string val) {
            CMSSiteMapProvider smpCms = (CMSSiteMapProvider)SiteMap.Providers["CMS"];
            smpCms.OnSitemapChanged();

            CMSEditSiteMapProvider smpECms = (CMSEditSiteMapProvider)SiteMap.Providers["CMSEdit"];
            smpECms.OnSitemapChanged();

            Response.Redirect(Helpers.GetNewQueryStringRawUrl(qs, val));
        }
        protected void RefreshCMSSitemapProvider()        {
            RefreshCMSSitemapProvider("", "");            
        }
        protected void RefreshCMSEditSitemapProvider()        {
            CMSEditSiteMapProvider smpCms = (CMSEditSiteMapProvider)SiteMap.Providers["CMSEdit"];
            smpCms.OnSitemapChanged();
            Response.Redirect(Helpers.GetNewQueryStringRawUrl("m", ""));
        }

        protected override void OnPreInit(EventArgs e)
        {
            
            base.OnPreInit(e);
            CheckPermissions = true;// todo: odstrsni!!
            DefaultProvider = "CMS";
            Theme = "CMS";

            // cropper
            Page.ClientScript.RegisterClientScriptInclude(typeof(BasePage_EMENIK), "Crop1", ResolveUrl("~/javascript/jQuery/jquery.pack.js"));
            Page.ClientScript.RegisterClientScriptInclude(typeof(BasePage_EMENIK), "Crop2", ResolveUrl("~/javascript/jQuery/jquery.Jcrop.pack.js"));
        }

        protected override void  OnPreRender(EventArgs e)
        {


            ContentPlaceHolder cph = (ContentPlaceHolder)Page.Master.FindControl("cphStatus");
            if (cph!=null) {
                
                if (!string.IsNullOrEmpty(litStatus.Text )) cph.Controls.Add(litStatus);
                if (!string.IsNullOrEmpty(litErrorMessage.Text)) cph.Controls.Add(litErrorMessage);
            }
            base.OnPreRender(e);
        }


   }
}