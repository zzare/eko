﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SM.EM.UI;
using SM.EM.UI.Controls ;

public partial class _em_help_first_step : BasePage_EMENIK 
{



    protected override void OnPreInit(EventArgs e)
    {
        AutoRedirect = false;
        IsContentPage = false;
        this.CheckPermissions = true;

        base.OnPreInit(e);

        if (!User.Identity.IsAuthenticated)
            RequestLogin();
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
       
        if (!IsPostBack) {
        }
        

    }

    protected void btStart_OnClick(object o, EventArgs e) {
        Response.Redirect( SM.EM.Rewrite.EmenikUserSettingsUrl(em_user.PAGE_NAME));
    
    
    }

    protected void btSkipWizard_OnClick(object o, EventArgs e)
    {
        // skip wizard, set default values
        EM_USER.InsertDefaultUserData(em_user);

        // redirect to first page
        Response.Redirect(SM.EM.Rewrite.EmenikUserUrl(em_user.PAGE_NAME));


    }

}
