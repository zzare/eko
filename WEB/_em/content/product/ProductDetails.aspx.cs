﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Reflection;

public partial class _em_content_product_product_details : SM.EM.UI.BasePage_CmsWebPart 
{
    public Guid  ProductID
    {
        get
        {
            Guid  _pid = Guid.Empty ;
            if (SM.EM.Helpers.GetQueryString ("pid") == null) return _pid;

            ShortGuid _spid = ShortGuid.Empty;
            try
            {
                _spid = new ShortGuid(SM.EM.Helpers.GetQueryString("pid"));
            }
            catch 
            {
                // try int
     //           ErrorRedirect(42, 404);                
            }

            return _spid.Guid;
        }
    }

    public int ProdIdInt
    {
        get
        {
            int _pid = -1;
            if (SM.EM.Helpers.GetQueryString ("pid") == null) return _pid;

            int.TryParse(SM.EM.Helpers.GetQueryString ("pid"), out _pid);

            return _pid;
        }
    }


    vw_Product  _prod;
    public vw_Product Prod
    {
        get
        {
            if (_prod != null) return _prod;

            ProductRep prep = new ProductRep();
            if (ProductID != Guid.Empty )
                _prod = prep.GetProductVWByID(ProductID, LANGUAGE.GetCurrentLang(), em_user.UserId );
            else
                _prod = prep.GetProductVWByID(ProdIdInt , LANGUAGE.GetCurrentLang(), em_user.UserId);

            // error
            if (_prod == null)
                ErrorRedirect(41, 404);

            // if not active
            //if (!_prod.ACTIVE && !IsEditMode  )
            //    ErrorRedirect(101, 404);

            return _prod;
        }
        set { _prod = value; }
    }

    public override Guid? CwpRefID
    {
        get
        {
            if (Prod != null)
                _CwpRefID = Prod.PRODUCT_ID ;

            return _CwpRefID;
        }
        set
        {
            base.CwpRefID = value;
        }
    }

    public override void LoadCwpList()
    {
        bool? active = true;
        if (IsModeCMS) active = null; // if edit mode, show all

        // get all webparts on current page by language ( not deleted ones)
        CwpList = CMS_WEB_PART.GetWebpartsByRefIDUserModules(Prod.PRODUCT_ID , LANGUAGE.GetCurrentLang(), true, false, em_user.UserId).ToList();

        //  base.LoadCwpList();
    }

    protected override void OnInit(EventArgs e)
    {
        // set sitemap id
        CurrentSitemapID = Prod.SMAP_ID;


        base.OnInit(e);
    }    

    protected void Page_Load(object sender, EventArgs e)
    {
        
        //// load js
        //SM.EM.Helpers.RegisterScriptIncludeAJAX(SM.BLL.Common.Emenik.ResolveMainDomainUrl("~/JavaScript/fancybox/jquery.fancybox-1.3.0.pack.js"), this, "fancybox");
        //SM.EM.Helpers.RegisterScriptIncludeAJAX(SM.BLL.Common.Emenik.ResolveMainDomainUrl("~/JavaScript/fancybox/jquery.easing-1.3.pack.js"), this, "easing");
        //// load css
        //SM.EM.Helpers.AddLinkedStyleSheet(this, SM.BLL.Common.Emenik.ResolveMainDomainUrl("~/javascript/fancybox/jquery.fancybox-1.3.0.css"), false);



    }

    public override void LoadPageModControls()
    {
        base.LoadPageModControls();
        // todo: replace from Db.product
        PageModule = SM.BLL.Common.PageModule.ProductDetails;

        if (Prod.PMOD_ID != null)
            PageModule = Prod.PMOD_ID.Value ;

        if (PageModule < 1)
            return;

        string ctrlSrc = "";

        switch (PageModule)
        {

            default:
                PAGE_MODULE pm = PAGE_MODULE.GetPageModuleByByID(PageModule);
                //if (pm == null)
                //    ctrlSrc = "~/_em/content/_ctrl/_pmodOneColumn.ascx";
                //else
                ctrlSrc = pm.WWW_URL;

                break;
        }


        // load content control
        UserControl uc = (UserControl)this.LoadControl(ctrlSrc);
        if (uc == null) return;

        // add data
        if (Prod != null)
        {
            Type viewControlType = uc.GetType();
            FieldInfo field = viewControlType.GetField("Data");

            if (field != null)
            {
                field.SetValue(uc, Prod);
            }
            else
            {
                throw new Exception("View file does not have a public Data property");
            }
        }

        uc.ID = "objContent";

        phContent.Controls.Add(uc);


    }

    protected override void LoadTitle()
    {
        base.LoadTitle();

        // append article to title
        Page.Title = SM.EM.Helpers.StripHTML(Prod.NAME) + " | " + Page.Title;

    }

    protected override void SetRelCanonical()
    {
        // base.SetRelCanonical();
        RelCanonical = SM.EM.Rewrite.EmenikUserUrlGetLeftPart(this.em_user) + SM.EM.Rewrite.EmenikUserProductDetailsUrl(this.em_user.UserId , Prod.ID_INT , Prod.NAME  ).TrimStart("~".ToCharArray());

    }

    public override string RenderBreadCrumbs(string separator)
    {
        string _curclass = "class=\"sel\"";
        return base.RenderBreadCrumbs(CurrentSmpEmenik, separator, false) + separator + "<a " + _curclass + " href=\"" + RelCanonical + "\">" + SM.EM.Helpers.StripHTML(Prod.NAME) + "</a>"; 
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        AddFacebookGraphMeta(); // after canonical is set
    }
    protected void AddFacebookGraphMeta()
    {

        // title
        HtmlMeta title = new HtmlMeta();
        title.Attributes["property"] = "og:title";
        title.Content = Prod.NAME;
        Header.Controls.Add(title);


        // site name
        HtmlMeta site_name = new HtmlMeta();
        site_name.Attributes["property"] = "og:site_name";
        site_name.Content = SM.BLL.Common.Emenik.Data.PortalName();
        Header.Controls.Add(site_name);

        // type
        HtmlMeta type = new HtmlMeta();
        type.Attributes["property"] = "og:type";
        type.Content = "product";
        Header.Controls.Add(type);

        // image

        string img = SM.BLL.Common.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/design/ekorepublika.png");
        //img = "http://cnj.blob.core.windows.net/zoran-img/ZJ_facebook_slovenia.jpg";
        if (!string.IsNullOrEmpty(Prod.IMG_URL))
            img = SM.BLL.Common.ResolveUrl(Prod.IMG_URL);

        HtmlMeta image = new HtmlMeta();
        image.Attributes["property"] = "og:image";
        image.Content = img;
        Header.Controls.Add(image);

        // admin        
        HtmlMeta admin = new HtmlMeta();
        admin.Attributes["property"] = "fb:admins";
        admin.Content = "759469529";
        Header.Controls.Add(admin);


        // app
        //HtmlMeta app = new HtmlMeta();
        //app.Attributes["property"] = "fb:app_id";
        //app.Content = Facebook.FacebookApplication.Current.AppId;
        //Header.Controls.Add(app);


        // url
        if (!string.IsNullOrEmpty(RelCanonical))
        {
            HtmlMeta url = new HtmlMeta();
            url.Attributes["property"] = "og:url";
            url.Content = this.RelCanonical;
            Header.Controls.Add(url);
        }

    }
}
