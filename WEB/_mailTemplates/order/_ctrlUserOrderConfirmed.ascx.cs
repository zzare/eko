﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _mailTemplates_ctrlUserOrderConfirmed : System.Web.UI.UserControl
{
    public object Data;
    public object[] Parameters;

    public string PageHref { get; set; }
    public string PageName { get; set; }
    public SHOPPING_ORDER Order { get; set; }
    public EM_USER EmPage { get; set; }
    public CUSTOMER  Customer { get; set; }

    
    protected void Page_Load(object sender, EventArgs e)
    {

        LoadData();
    }



    public void LoadData()
    {



        if (Data == null)
            return;
        Order = (SHOPPING_ORDER)Data;

        if (Parameters != null)
        {
            if (Parameters[0] != null)
                PageHref = Parameters[0].ToString();
            if (Parameters.Length > 1 && Parameters[1] != null)
                EmPage = (EM_USER)Parameters[1];
            if (Parameters.Length > 2 && Parameters[2] != null)
                Customer  = (CUSTOMER )Parameters[2];

        }


        objOrderSummary.PageId = Order.PageId.Value  ;
        objOrderSummary.Order = Order;
        objOrderSummary.LoadData();
    }
}
