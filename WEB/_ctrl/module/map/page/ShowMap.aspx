﻿<%@ Page EnableViewState="false" Language="C#" AutoEventWireup="true" CodeFile="ShowMap.aspx.cs" Inherits="_ctrl_module_map_page_ShowMap" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    
        <style type="text/css"  >
        h3 {font-size:15px;padding:5px 0;}
        h1, h2, h3 {font-family:Tahoma;font-weight:normal;}
        * {
            margin:0; 
            padding:0; 
        }
        p{color:#666666;font-family:Tahoma;font-size:12px;}
    </style>
</head>
<body>
    
    <form id="form1" runat="server">
    <div>
        <div id="map" ></div>
    </div>
    </form>
</body>
</html>
