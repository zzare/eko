﻿<%@ Page Language="C#" MasterPageFile="~/CMS/CMS.master" AutoEventWireup="true" CodeFile="ManageSitemap.aspx.cs" Inherits="SM.EM.UI.CMS.ManageSiteMap" Title="Untitled Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="EM" TagName="cmsUpdateProgress" Src="~/cms/controls/_cmsUpdateProgress.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphNavL"  Runat="Server">
    SELECT MENU
    <asp:DropDownList ID="ddlMenu" runat="server" AutoPostBack="True" onselectedindexchanged="ddlMenu_SelectedIndexChanged"></asp:DropDownList>
    
    <asp:TreeView ID="tvSitemap" runat="server" onselectednodechanged="tvSitemap_SelectedNodeChanged" SelectedNodeStyle-BackColor=""
    SelectedNodeStyle-CssClass="tvwOrangeSelected" CssClass="tvwOrange" ShowLines="true" NodeWrap="true">
    </asp:TreeView>    

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph" Runat="Server">
    <script type="text/javascript" >
    var lookup;
    function SetRolesCms(){
        lookup = document.getElementById('<%= tbCmsRoles.ClientID %>');
        var mpe = $find("mpeRoles");
        if (mpe)
            mpe.show();
    }
    function SetRolesWww(){
        lookup = document.getElementById('<%= tbWwwRoles.ClientID %>');
        var mpe = $find("mpeRoles");
        if (mpe)
            mpe.show();
    }
    function SetRoles(){
        
        var cbl = document.getElementById('divRoles').getElementsByTagName('input');
        
        lookup.value = '';

        for (var i=0; i<cbl.length; i++){
            var cbx = cbl[i];
            if (cbx.type != 'checkbox')
                continue;
            
            if (cbx.checked){
                lookup.value += "," + cbx.nextSibling.innerHTML;
            }
        }
        if (lookup.value.length > 0)
            lookup.value = lookup.value.substring(1);
    }
//    function SetParent(){
//        
//        var name = ctl00_cph_tvParent_Data.selectedNodeID.value;
//        var node = document.getElementById(name);
//        
//        alert(node);
//        
//    }
//    


    </script>

    <asp:HiddenField ID="hfParentID" runat="server" Value="-1" />
    

    <asp:LinqDataSource ID="ldsSiteMaps" runat="server" 
        ContextTypeName="eMenikDataContext" EnableDelete="True" EnableInsert="True" 
        EnableUpdate="True" TableName="SITEMAPs" 
        Where="MENU_ID == @MENU_ID " >
        <WhereParameters>
            <asp:ControlParameter ControlID="ctl00$cphNavL$ddlMenu" DefaultValue="0" Name="MENU_ID" 
                PropertyName="SelectedValue" Type="Int32" />
        </WhereParameters>
    </asp:LinqDataSource>
    
    <asp:UpdatePanel ID="upContent" runat="server">
        <ContentTemplate>
    
        
        <asp:Panel ID="panInsert" CssClass="floatL" runat="server" DefaultButton="btSave">
            <div style="text-align:left;">
            <asp:Button ID="btSave" ValidationGroup="newSmap" runat="server" Text="Save" onclick="btSave_Click" />
            <asp:Button ID="btNew" runat="server" Text="New" CausesValidation="false" onclick="btNew_Click" />
            <asp:Button ID="btDelete" runat="server" Text="Delete" CausesValidation="false" onclick="btDelete_Click" />
            <asp:Button ID="btRefresh" runat="server" Text="Refresh" CausesValidation="false" onclick="btRefresh_Click" />
            </div>
        
            <table class="TABLEmanagesitemap" cellpadding="0" cellspacing="0" >
    <%--            <tr>
                    <th>MENU</th>
                    <td><asp:DropDownList ID="ddlMenu" runat="server" AutoPostBack="True" 
                            onselectedindexchanged="ddlMenu_SelectedIndexChanged"></asp:DropDownList></td>
                </tr>--%>
                <tr>
                    <th>ID</th>
                    <td><asp:Literal ID="lSitemapID" runat="server"></asp:Literal></td>
                </tr>
                <tr>
                    <th>TITLE</th>
                    <td>
                        <asp:TextBox class="tbx" ID="tbTitle" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ValidationGroup="newSmap" ID="reqvTitle" ControlToValidate="tbTitle" runat="server" ErrorMessage="TITLE must be set."></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <th>PARENT</th>
                    <td>
                        <asp:Literal ID="lParent" runat="server" Text="ROOT"></asp:Literal>
                        <asp:ImageButton ID="btSetParent" runat="server" ImageUrl="~/_inc/images/button/lookup.png" />
                        <asp:CustomValidator ID="cvParent"  runat="server" ErrorMessage="Choose parent" OnServerValidate="ValidateParent" ValidationGroup="newSmap"></asp:CustomValidator>
                    </td>
                </tr>
                <tr>
                    <th>PAGE MODULE</th>
                    <td><asp:DropDownList ID="ddlModule" runat="server" OnSelectedIndexChanged="ddlModule_OnSelectedIndexChanged" AutoPostBack="true"></asp:DropDownList></td>
                </tr>
                <tr>
                    <th>WWW_URL *overrides PAGE MODULE</th>
                    <td>
                        <asp:TextBox ID="tbWWW_URL" class="tbx" runat="server"></asp:TextBox>                        
                    </td>
                </tr>                
                <tr>
                    <th>WWW ROLES</th>
                    <td>
                        <asp:TextBox ID="tbWwwRoles" class="tbx" runat="server"></asp:TextBox><asp:ImageButton ID="btSetRolesWww" runat="server"  ImageUrl="~/_inc/images/button/lookup.png" OnClientClick="SetRolesWww();" CausesValidation="false" />
                        <asp:RequiredFieldValidator ValidationGroup="newSmap" ID="reqvWwwRoles" ControlToValidate="tbWwwRoles" runat="server" ErrorMessage="WWW Roles must be set."></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <th>CMS ROLES</th>
                    <td>
                        <asp:TextBox ID="tbCmsRoles" class="tbx" runat="server"></asp:TextBox><asp:ImageButton ID="btSetRolesCms" runat="server"  ImageUrl="~/_inc/images/button/lookup.png" OnClientClick="SetRolesCms();return false;" CausesValidation="false" />
                        <asp:RequiredFieldValidator ValidationGroup="newSmap" ID="reqCmsRoles" ControlToValidate="tbCmsRoles" runat="server" ErrorMessage="CMS Roles must be set."></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <th>ORDER</th>
                    <td>
                        <asp:TextBox ID="tbOrder" class="tbx" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <th>ACTIVE</th>
                    <td>
                        <asp:CheckBox ID="cbActive" runat="server"></asp:CheckBox>
                    </td>
                </tr>
                <tr>
                    <th>WWW VISIBLE</th>
                    <td>
                        <asp:CheckBox ID="cbWwwVisible" runat="server" Text="Www visible" />
                    </td>
                </tr>
                <tr>
                    <th>IMAGE</th>
                    <td>
                        <asp:TextBox ID="tbImage" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <th>CUSTOM CLASS</th>
                    <td>
                        <asp:TextBox ID="tbClass" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <th>THEME</th>
                    <td>
                        <asp:TextBox ID="tbTheme" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <th>Modules</th>
                    <td>
                        <asp:CheckBoxList ID="cblModules" runat="server" DataSourceID="ldsModules" 
                                            DataTextField="MOD_TITLE" DataValueField="MOD_ID">
                        </asp:CheckBoxList>
                        <asp:LinqDataSource ID="ldsModules" runat="server" 
                            ContextTypeName="eMenikDataContext" EnableDelete="True" EnableInsert="True" 
                            EnableUpdate="True" TableName="MODULEs"  >
                        </asp:LinqDataSource>                         
                    </td>
                </tr>                
            
            </table>
            
        
          <%--  modal popups --%>
            <asp:Panel CssClass="modCMSPopup" ID="panParent" runat="server" style="display:none;">
                <asp:TreeView ID="tvParent" runat="server" SelectedNodeStyle-BackColor="" OnSelectedNodeChanged="tvParent_SelectedNodeChanged"
                SelectedNodeStyle-CssClass="tvwOrangeSelected" CssClass="tvwOrange" ShowLines="true" NodeWrap="true"  >
                </asp:TreeView>
                <asp:Button ID="btCancel" runat="server" Text="Cancel" />
                
            </asp:Panel>
            
            <asp:Panel CssClass="modCMSPopup" ID="panRoles" runat="server" style="display:none;">
                <div id="divRoles">
                    <asp:CheckBoxList ID="cblRoles" runat="server" AppendDataBoundItems="true">
                        <asp:ListItem Text="*" Value="*"></asp:ListItem>
                        <asp:ListItem Text="?" Value="?"></asp:ListItem>
                    </asp:CheckBoxList>
                </div>
                <asp:Button ID="btOkRoles" runat="server" Text="Save" CausesValidation="false"  />
                <asp:Button ID="btCancelRoles" runat="server" Text="Cancel"  CausesValidation="false"/>
            </asp:Panel>
            
            <cc1:ModalPopupExtender RepositionMode="RepositionOnWindowResizeAndScroll" Y ="50" ID="mpeParent" runat="server" TargetControlID="btSetParent" PopupControlID="panParent"
                                    BackgroundCssClass="modCMSBcg" CancelControlID="btCancel" >
            </cc1:ModalPopupExtender>  

            <cc1:ModalPopupExtender RepositionMode="RepositionOnWindowResizeAndScroll" BehaviorID="mpeRoles"  ID="mpeRoles" runat="server" TargetControlID="btSetRolesWww" PopupControlID="panRoles"
                                    BackgroundCssClass="modCMSBcg" CancelControlID="btCancelRoles" OkControlID="btOkRoles" OnOkScript="SetRoles()" >
            </cc1:ModalPopupExtender> 
                    
        </asp:Panel>
        
    
        <div class="floatL">
        
        <%--  SITEMAP LANGUAGE --%>
        <asp:Panel ID="panSmapLangHD" runat="server" CssClass="cpeHeader" >
           LANGUAGES : <asp:Label ID="lSmapLangHeader" runat="server" Text=""></asp:Label>    
        </asp:Panel>
        <asp:Panel ID="panSmapLang" runat="server">
        
            
            <asp:LinqDataSource ID="ldsSmapLang" runat="server" 
                ContextTypeName="eMenikDataContext" EnableDelete="True" EnableInsert="True" 
                EnableUpdate="True" TableName="SITEMAP_LANGs" Where="SMAP_ID == @SMAP_ID" 
                onselecting="ldsSmapLang_Selecting" >
            </asp:LinqDataSource>
            
            <asp:DetailsView ID="dvSmapLang" runat="server"  
                DataSourceID="ldsSmapLang" DefaultMode="Insert" AutoGenerateRows="False" 
                DataKeyNames="SMAP_ID,LANG_ID" OnDataBound="dvSmapLang_DataBound" 
                OnItemInserted="dvSitemapLang_ItemInserted" 
                oniteminserting="dvSmapLang_ItemInserting" >
                <Fields>
                    <asp:TemplateField HeaderText="SMAP_ID" SortExpression="SMAP_ID" >
                        <InsertItemTemplate>
                            <asp:Literal ID="lSmapId" runat="server" Text='<%# Bind("SMAP_ID") %>'></asp:Literal>
                        </InsertItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="LANG_ID" SortExpression="LANG_ID" >
                        <InsertItemTemplate>
                            <asp:DropDownList ID="ddlLanguage" runat="server" AppendDataBoundItems="True" >
                                <asp:ListItem Text=" - Select Language - " Value = "-1"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:CompareValidator ID="CompareValidator1" ValidationGroup="vgSmlUpdate" ValueToCompare="-1" Operator="NotEqual" ControlToValidate="ddlLanguage" runat="server" ErrorMessage="Select Language"></asp:CompareValidator>
                        </InsertItemTemplate>
                    </asp:TemplateField>            
                    <asp:TemplateField HeaderText="SML_TITLE" SortExpression="SML_TITLE">
                        <InsertItemTemplate>
                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("SML_TITLE") %>'></asp:TextBox>
                            <asp:RequiredFieldValidator ValidationGroup="vgSmlUpdate" ID="RequiredFieldValidator1" ControlToValidate="TextBox1" runat="server" ErrorMessage="Set Title"></asp:RequiredFieldValidator>
                        </InsertItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="SML_DESC" HeaderText="SML_DESC" 
                        SortExpression="SML_DESC" />
                    <asp:TemplateField HeaderText="SML_ACTIVE" SortExpression="SML_ACTIVE">
                        <InsertItemTemplate>
                            <asp:CheckBox ID="cbActive" runat="server" Checked='<%# Bind("SML_ACTIVE") %>' />
                        </InsertItemTemplate>
                    </asp:TemplateField>
            <%--        <asp:CheckBoxField DataField="SML_ACTIVE" HeaderText="SML_ACTIVE" 
                        SortExpression="SML_ACTIVE" />--%>
                    <asp:BoundField DataField="SML_KEYWORDS" HeaderText="SML_KEYWORDS" 
                        SortExpression="SML_KEYWORDS" />
                    <asp:BoundField DataField="SML_META_DESC" HeaderText="SML_META_DESC" SortExpression="SML_META_DESC" />
                    <asp:BoundField DataField="SML_ORDER" HeaderText="SML_ORDER" 
                        SortExpression="SML_ORDER" />
                    <asp:CommandField ShowInsertButton="True" ValidationGroup="vgSmlUpdate" />
                </Fields>
            </asp:DetailsView>

            
            <asp:GridView ID="gvSmapLang" runat="server" AllowSorting="True"  
                AutoGenerateColumns="False" DataKeyNames="SMAP_ID,LANG_ID" 
                DataSourceID="ldsSmapLang" onrowdeleted="gvSmapLang_RowDeleted" 
                onrowupdated="gvSmapLang_RowUpdated">
                <Columns>
                    <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
                    <asp:BoundField DataField="SMAP_ID" HeaderText="ID" ReadOnly="True" 
                        SortExpression="SMAP_ID" />
                    <asp:BoundField DataField="LANG_ID" HeaderText="LANG" ReadOnly="True" 
                        SortExpression="LANG_ID" />
                    <asp:BoundField DataField="SML_TITLE" HeaderText="TITLE" 
                        SortExpression="SML_TITLE" />
                    <asp:BoundField DataField="SML_DESC" HeaderText="DESC" 
                        SortExpression="SML_DESC" />
                    <asp:CheckBoxField DataField="SML_ACTIVE" HeaderText="ACT" 
                        SortExpression="SML_ACTIVE" />
                    <asp:BoundField DataField="SML_KEYWORDS" HeaderText="KEYWORDS" SortExpression="SML_KEYWORDS" />
                    <asp:BoundField DataField="SML_META_DESC" HeaderText="SML_META_DESC" SortExpression="SML_META_DESC" />
                    <asp:BoundField DataField="SML_ORDER" HeaderText="ORDER" 
                        SortExpression="SML_ORDER" />
                </Columns>
            </asp:GridView>
        </asp:Panel>
        <cc1:CollapsiblePanelExtender ID="cpeSmapLang" runat="server" Collapsed="false" TextLabelID="lSmapLangHeader" CollapseControlID="panSmapLangHD" ExpandControlID="panSmapLangHD" TargetControlID="panSmapLang" ExpandDirection="Vertical" CollapsedText ="Show" ExpandedText="Hide">
        </cc1:CollapsiblePanelExtender>

    </div>        

    <div class="clearing"></div>
    <br />

    <asp:Panel ID="panSmapListHD" runat="server" CssClass="cpeHeader" >
       SITEMAP LIST : <asp:Label ID="lSmapListTxt" runat="server" Text="Label"></asp:Label>    
    </asp:Panel>
    
    <asp:Panel ID="panSmapList" runat="server">
        <asp:GridView ID="gvSiteMap" runat="server" AllowPaging="True" 
            AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="SMAP_ID" 
            DataSourceID="ldsSiteMaps" onrowupdated="gvSiteMap_RowUpdated" 
            onselectedindexchanged="gvSiteMap_SelectedIndexChanged"  >
            <Columns>
                <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" 
                    ShowSelectButton="True" />
                <asp:BoundField DataField="SMAP_ID" HeaderText="ID" ReadOnly="True" 
                    SortExpression="SMAP_ID" InsertVisible="False" />
                <asp:BoundField DataField="MENU_ID" HeaderText="MENU" 
                    SortExpression="MENU_ID" />
                <asp:BoundField DataField="PMOD_ID" HeaderText="PAGE MODULE" 
                    SortExpression="PMOD_ID" />
                <asp:BoundField DataField="SMAP_TITLE" HeaderText="TITLE" 
                    SortExpression="SMAP_TITLE" />
                <asp:BoundField DataField="PARENT" HeaderText="PARENT" 
                    SortExpression="PARENT" />
                <asp:CheckBoxField DataField="SMAP_ACTIVE" HeaderText="ACT" 
                    SortExpression="SMAP_ACTIVE" />
                <asp:BoundField DataField="WWW_ROLES" HeaderText="WWW ROL" 
                    SortExpression="WWW_ROLES" />
                <asp:BoundField DataField="CMS_ROLES" HeaderText="CMS ROL" 
                    SortExpression="CMS_ROLES" />
                <asp:BoundField DataField="SMAP_ORDER" HeaderText="ORD" 
                    SortExpression="SMAP_ORDER" />
                <asp:BoundField DataField="SMAP_IMAGE" HeaderText="IMG" 
                    SortExpression="SMAP_IMAGE" />
                <asp:CheckBoxField DataField="WWW_VISIBLE" HeaderText="VIS" 
                    SortExpression="WWW_VISIBLE" />
            </Columns>
        </asp:GridView>
    </asp:Panel>
    
    <cc1:CollapsiblePanelExtender ID="cpeSmapList" runat="server" Collapsed="true" TextLabelID="lSmapListTxt" CollapseControlID="panSmapListHD" ExpandControlID="panSmapListHD" TargetControlID="panSmapList" ExpandDirection="Vertical" CollapsedText ="Show" ExpandedText="Hide">
    </cc1:CollapsiblePanelExtender>    
        
        </ContentTemplate>
    </asp:UpdatePanel>    
    <asp:UpdateProgress ID="uppContent" runat="server" AssociatedUpdatePanelID="upContent" DisplayAfter="1000" >
        <ProgressTemplate>
        
        <EM:cmsUpdateProgress ID="upptProgress" runat="server" />
                    
        </ProgressTemplate>    
    </asp:UpdateProgress>
    
      
</asp:Content>

