﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;


namespace SM.UI.Controls
{
    public partial class _cmsUserSelectNoUP : BaseControl 
    {
        public event EventHandler OnUserSelect;
        public List<Guid> GetSelectedItems { get { return objUserSelectList.GetSelectedItems; } }
        public Guid ExcludeProjectID { get; set; }
        public Guid ExcludeSiteID { get; set; }
        public int ExcludeTaskID { get; set; }
        public Guid ExcludeUserID { get; set; }
        public int PageSize = 20;
        public bool MultipleSelect = true;

        public bool IsOpen { get {
            if (ViewState["IsOpen"] == null)
                return false;
        
            if (ViewState["IsOpen"] != null)
            return bool.Parse(ViewState["IsOpen"].ToString());

            return false;
        }
            set { ViewState["IsOpen"] = value; }
        } 


        protected void Page_Load(object sender, EventArgs e)
        {
            objFilter.OnFilterChanged += new EventHandler(objFilter_OnFilterChanged);

            objUserSelectList.ExcludeTaskID = ExcludeTaskID;
            //objUserSelectList.ExcludeSiteID = ExcludeSiteID;
            //objUserSelectList.ExcludeGroupID = ExcludeGroupID;
            objUserSelectList.MultipleSelect = MultipleSelect;
            objUserSelectList.ShowCheckboxColumn=true;
            objUserSelectList.ShowManageColumn = false;

            // init
            objUserSelectList.PageSize = PageSize;

            if (IsOpen)
                mpeSort.Show(); 

             

        }
        public void OpenPopup() {
            // init data
            objUserSelectList.ExcludeTaskID = ExcludeTaskID;
            //objUserSelectList.ExcludeSiteID = ExcludeSiteID;
            //objUserSelectList.ExcludeGroupID = ExcludeGroupID;
            objUserSelectList.ExcludeUserID = ExcludeUserID;

            objUserSelectList.PageSize = PageSize;
            objUserSelectList.BindData();
            objFilter.LoadDefaults();

            IsOpen = true;
            mpeSort.Show();        
        }

        protected void btAddSelectedUser_Click(object sender, EventArgs e)
        {
            OnUserSelect(this, new EventArgs());
            IsOpen = false;

            mpeSort.Hide();
        }

        // filter
        void objFilter_OnFilterChanged(object sender, EventArgs e)
        {
            // filters
            objUserSelectList.Active = objFilter.Active;
            objUserSelectList.SearchText = objFilter.SearchText;

            // refresh question lists
            objUserSelectList.BindData();

            mpeSort.Show();

        }


    }

}