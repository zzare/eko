﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SM.UI.Controls ;

/// <summary>
/// Summary description for BaseMasterPage
/// </summary>
namespace SM.EM.UI
{

    public abstract class BaseMasterPage_EMENIK : BaseMasterPage
    {
        public BasePage_EMENIK ppem;
         

        public BaseMasterPage_EMENIK() {
      //      wpManager = new DynamicWebPartManager();
    //        wpManager.Personalization.InitialScope= PersonalizationScope.Shared ;
//            this.Controls.AddAt(0,wpManager);        
        }
         
        protected override void OnInit(EventArgs e)
        {




            ppem = (BasePage_EMENIK)Page;
            base.OnInit(e);
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (!IsPostBack)
                LoadData();
        }

        public virtual  void LoadData() { 
        }

    
    }
}