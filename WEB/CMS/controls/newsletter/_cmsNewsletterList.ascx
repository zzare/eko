﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_cmsNewsletterList.ascx.cs" Inherits="SM.UI.Controls._cmsNewsletterList" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Namespace="SM.UI.Controls" TagPrefix="SM" %>


<div class="qContList">

        <asp:LinqDataSource ID="ldsList" runat="server" OnSelecting="ldsList_OnSelecting" >
        </asp:LinqDataSource>
        
        <SM:smListView ID="lvList" runat="server" DataSourceID="ldsList" InsertItemPosition="None"  >
            <LayoutTemplate>
                <table  class="tblData" cellpadding="0" cellspacing="0">
                        <thead>
                            <tr id="headerSort" runat="server">
                                <th ><asp:LinkButton ID="LinkButton5" CommandName="sort" CommandArgument="DateCreated" runat="server">Date Created</asp:LinkButton></th>
                                <th ><asp:LinkButton ID="LinkButton1" CommandName="sort" CommandArgument="DateSent" runat="server">Date Sent</asp:LinkButton></th>
                                <th ><asp:LinkButton ID="LinkButton2" CommandName="sort" CommandArgument="Title" runat="server">Title</asp:LinkButton></th>
                                <th ><asp:LinkButton ID="LinkButton3" CommandName="sort" CommandArgument="Status" runat="server">Status</asp:LinkButton></th>
                                <th width="100px" colspan="2" runat="server"  id="headManage" >Uredi</th>
                            </tr>
                        </thead>
                        <tbody id="itemPlaceholder" runat="server"></tbody>
                        <tfoot>
                            <tr>
                                <th style="text-align:right" colspan="8" >
                                    <asp:DataPager runat="server" ID="DataPager" PageSize="30" >
                                        <Fields>
                                            <asp:NextPreviousPagerField  />
                                            <asp:NumericPagerField ButtonCount="5" />
                                        </Fields>
                                    </asp:DataPager>
                                </th>
                            </tr>
                        </tfoot>
                    </table>
            </LayoutTemplate>
            
            <ItemTemplate>
                <tr  class='<%#  Container.DataItemIndex % 2 == 0 ? "  " : " odd " %>'  onmouseover ="this.className='<%#  Container.DataItemIndex % 2 == 0 ? "hovRow" : "hovRowodd" %>'" onmouseout ="this.className = GetCssClass(<%#  Container.DataItemIndex %>)">
                    <td>
                        <%# SM.EM.Helpers.FormatDateLong(Eval("DateCreated"))%>
                    </td>
                    <td>
                        <%# SM.EM.Helpers.FormatDateLong(Eval("DateSent"))%>
                    </td>                    
                    <td>
                        <%# Eval("Title")%>
                    </td>
                    <td>
                        <%# NEWSLETTER.Common.RenderStatus(Eval("Status"))%>
                    </td>
                    <td width="50px" runat="server"  >
                        <a target='<%# OnEditTarget %>' href='<%# Page.ResolveUrl( NEWSLETTER.Common.GetNewsletterDetailsCMSHref(new Guid(Eval("NewsletterID").ToString()))) %>' title="edit order" >uredi</a>
                    </td>
                </tr>  
            
            </ItemTemplate>
            
            <EmptyDataTemplate>
                
                <div class="odd">
                    Seznam je prazen.
            
                </div>
                
            </EmptyDataTemplate>
            
        
        </SM:smListView>

</div>
