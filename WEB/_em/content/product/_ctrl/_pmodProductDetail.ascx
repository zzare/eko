﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_pmodProductDetail.ascx.cs" Inherits="_em_content_product__ctrl__pmodProductDetail" %>
<%@ Register Namespace="SM.EM.UI.Controls" TagPrefix="EM"  %>
<%@ Register TagPrefix="EM" TagName="ModuleNew" Src="~/_ctrl/emenik/emModuleNew.ascx"  %>
<%@ Register TagPrefix="SM" TagName="ProductDetail" Src="~/_ctrl/module/product/_viewProductDetail.ascx"  %>
<%@ Register TagPrefix="SM" TagName="ScriptProduct" Src="~/_ctrl/module/product/edit/_cntScript.ascx"  %>
<%@ Register TagPrefix="SM" TagName="ProductGallery" Src="~/_ctrl/module/gallery/product/cwpGalleryProduct.ascx"  %>
<%@ Register TagPrefix="EM" TagName="SubMenu" Src="~/_em/templates/_custom/eko/_ctrl/_ctrlSubMenu.ascx"  %>
<%@ Register TagPrefix="SM" TagName="RelatedProducts" Src="~/_ctrl/module/product/_viewProductListRelated.ascx"    %>


<%--<div class="mainL">
        
    <EM:SubMenu ID="objSubMenu" runat="server" />
                       
</div>
--%>

<%--<div class="mainR">
--%>
<div class="productdetailWrapp">
    <SM:ScriptProduct ID="objScriptProduct" runat="server" />

    <% if(ParentPage.IsModeCMS ) { %>
    <div class="moduleNew" >
        <div class="moduleNewLeft">&nbsp;</div>
        <div style="float:left; ">
            <a class="btEditwthText" href="#" onclick="editProduct('<%= Prod.PRODUCT_ID %>'); return false" title="uredi lastnosti izdelka"  ><span>uredi lastnosti izdelka</span></a>
<% if (SM.EM.Security.Permissions.IsAdmin()) { %>            
            <a class="btEditwthText" href="#" onclick="editProductDesc('<%= Prod.PRODUCT_ID %>'); return false" title="uredi naziv in opis"  ><span>uredi naziv in opis</span></a>
<%} %>
            <a class="btEditwthText" href="#" onclick="editProductDescFCKshort('<%= Prod.PRODUCT_ID %>'); return false" title="uredi naziv in opis"  ><span>uredi naziv in opis</span></a>
        
        </div>
        <%--<div style="float:left;">
            <a href="javascript:void(0)" class="btsettStatusHelp btcwpHeadHelp"><img style="margin-top:8px;" src='<%=Page.ResolveUrl("~/_inc/images/icon/help.png") %>' /></a>
            <div class="settStatusWrapp cwpHeadHelpWrapp" >
                <div class="settStatus cwpHeadHelp">
                    - Dodaj NOV izdelek v oddelek.
                    <br />
                </div>
            </div>
        </div>   --%> 
        <div class="moduleNewRight">&nbsp;</div>
        <div class="clearing"></div>
    </div>
    <%} %>
    
    <SM:ProductDetail ID="objProductDetail" runat="server" />

    <SM:ProductGallery  ID="objGalleryProduct" runat="server" />
               
    <EM:ModuleNew ID="mnMain" runat="server" />

    <EM:CwpZone ID="wpz_prod_1" runat="server">
    </EM:CwpZone>


    

   <%-- <SM:RelatedProducts ID="objRelated" runat="server" />--%>

<%--</div>
--%>
</div>    
<div class="clearing"></div>
