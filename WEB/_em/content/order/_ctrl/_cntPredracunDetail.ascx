﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_cntPredracunDetail.ascx.cs" Inherits="SM.UI.Controls.c_user_ctrl_cntPredracunDetail" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Namespace="SM.UI.Controls" TagPrefix="SM" %>
<%@ Register TagPrefix="SM" TagName="OrderDetail" Src="~/_em/content/order/_ctrl/_ctrlOrderDetail.ascx"    %>
<%@ Register TagPrefix="SM" TagName="OrderSummaryFormatted" Src="~/_em/content/order/_ctrl/_ctrlOrderSummaryFormatted.ascx"    %>  



 <SM:OrderSummaryFormatted ID="objOrderSummaryFormatted" runat="server" />














<asp:PlaceHolder runat="server" Visible="false">
<h1>Predračun</h1>
Št. predračuna: <%= SHOPPING_ORDER.Common.Predracun.RenderPredracunID(Order.OrderRefID)  %>
<br />
Datum: <%= SM.EM.Helpers.FormatDateMedium(Order.DateStart) %>
<br />
<br />

<h2>Naročnik</h2>
<%= SHOPPING_ORDER.Common.RenderShoppingOrderUserData(Order)  %>


<br />
<br />

<h2>Podatki za plačilo</h2>
<%=  SM.BLL.Common.Emenik.Data.SMARDT_TITLE  %>
<br />
<%=  SM.BLL.Common.Emenik.Data.SMARDT_ADDRESS  %>
<br />
<%=  SM.BLL.Common.Emenik.Data.SMARDT_CITY  %>
<br />
<%=  SM.BLL.Common.Emenik.Data.SMARDT_PHONE  %>
<br />
<%=  SM.BLL.Common.Emenik.Data.SMARDT_URL  %>
<br />
<br />
Št. računa: <%= SM.BLL.Common.Emenik.Data.TRR() %>
<br />
<strong>Referenca (sklic): <%= SHOPPING_ORDER.Common.RenderShoppingOrderNumber  (Order)%></strong>
<br />
ID za DDV: <%= SM.BLL.Common.Emenik.Data.TaxNo() %>
<br />
<br />
Zapadlost: <%= SM.EM.Helpers.FormatDate( Order.GetDatumZapadlosti() ) %>
<br />
Znesek skupaj: <%= SM.EM.Helpers.FormatPrice(Order.TotalWithTax)%>
<br />
<br />



<h2>Podatki naročila</h2>
Št. naročila: <%= SHOPPING_ORDER.Common.RenderShoppingOrderNumber(Order)%>
<br />
<strong>Status naročila: <%= SHOPPING_ORDER.Common.RenderOrderStatus(Order.OrderStatus)%></strong>
<br />
<br />
<h2>Vsebina naročila</h2>
<SM:OrderDetail ID="objOrderDetail" runat="server" />

</asp:PlaceHolder>


<%--Znesek skupaj: <%= SM.EM.Helpers.FormatPrice(Order.Total)%> + ddv (<%= SM.EM.Helpers.FormatPrice(Order.Tax)%>)  = <%= SM.EM.Helpers.FormatPrice(Order.TotalWithTax)%>
--%>



