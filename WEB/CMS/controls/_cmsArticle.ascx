﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_cmsArticle.ascx.cs" Inherits="SM.EM.UI.CMS.Controls.CMS_cmsArticle" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="FredCK.FCKeditorV2" Namespace="FredCK.FCKeditorV2" TagPrefix="FCKeditorV2" %>
<%@ Register TagPrefix="EM" Namespace="SM.EM.UI" %>
<%@ Register Namespace="SM.UI.Controls" TagPrefix="SM" %>

  <script type="Text/javascript">
    var fckBody;
    function ValidateBody(source, args)
    {
      args.IsValid = fckBody.GetXHTML(true) != "";
    }
    function FCKeditor_OnComplete(fckInstance)
    {  
      fckBody = fckInstance;
    }
  </script>

<SM:smListView ID="listContent" runat="server" DataSourceID="ldsList"  
    DataKeyNames="ART_ID" 
    OnItemEditing="listContent_ItemEditing" 
    OnItemInserted="listContent_ItemInserted" 
    onitemupdated="listContent_ItemUpdated"
    OnItemCanceling="listContent_ItemCanceling" 
    onitemcreated="listContent_ItemCreated" >
    <LayoutTemplate>
        
        <table cellpadding="0" cellspacing="0" width="100%" class="tblData">
            <thead>
                <tr>
                    <th><asp:LinkButton ID="lbSort0" CommandName="sort" CommandArgument="ART_ID" runat="server">ID</asp:LinkButton></th>
                    <th><asp:LinkButton ID="lbSortLan" CommandName="sort" CommandArgument="LANG_ID" runat="server">LANG</asp:LinkButton></th>
                    <th>Title</th>
                    <th><asp:LinkButton ID="LinkButton1" CommandName="sort" CommandArgument="RELEASE_DATE" runat="server">Release date</asp:LinkButton></th>
                    <th><asp:LinkButton ID="LinkButton2" CommandName="sort" CommandArgument="EXPIRE_DATE" runat="server">Expire date</asp:LinkButton></th>
                    <th><asp:LinkButton ID="lbSort1" CommandName="sort" CommandArgument="ACTIVE" runat="server">Active</asp:LinkButton></th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <asp:PlaceHolder ID="itemPlaceholder" runat="server"/>
            <tfoot>
                <tr>
                    <th style="text-align:left">
                        <asp:Button ID="btNew" CommandName="addnew" runat="server" onclick="btNew_Click" Text="Add New" ValidationGroup="insert"></asp:Button>
                    </th>
                    <th colspan="8" style="text-align:right">
                        <asp:DataPager ID="pagContent" runat="server" PagedControlID="listContent" PageSize="15">
                            <Fields>
                                <asp:NumericPagerField />
                                <asp:NextPreviousPagerField />
                                
                            </Fields>
                        </asp:DataPager>
                    </th>
                </tr> 
            </tfoot>           
        </table>
    </LayoutTemplate>

    <ItemTemplate>
        <tr class='<%#  Container.DataItemIndex % 2 == 0 ? " " : " odd " %>'  onmouseover ="this.className='<%#  Container.DataItemIndex % 2 == 0 ? "hovRow" : "hovRowodd" %>'" onmouseout ="this.className = GetCssClass(<%#  Container.DataItemIndex %>)">
            <td><%#Eval("ART_ID") %></td>
            <td><%#Eval("LANG_ID") %></td>
            <td><%# SM.EM.Helpers.CutString( SM.EM.Helpers.StripHTML(Eval("ART_TITLE").ToString()), 25, " ...") %></td>
            <td><%# SM.EM.Helpers.FormatDate(Eval("RELEASE_DATE"))  %></td>
            <td><%# SM.EM.Helpers.FormatDate(Eval("EXPIRE_DATE"))  %></td>
            <td><asp:CheckBox ID="cbActive" runat="server" Checked ='<%#Eval("ACTIVE") %>' Enabled="false" /> </td>
            <td><asp:LinkButton ID="lbEdit" CommandName="Edit" runat="server">Edit</asp:LinkButton>
                &nbsp;<asp:LinkButton ID="lbDelete" CommandName="Delete" runat="server">Delete</asp:LinkButton></td>
        </tr>
    </ItemTemplate>
    <EditItemTemplate>
        <tr>
            <td colspan="7">  
                <div style="border-top:#000000 solid 2px; width:100%; height:1px;"></div>
            </td> 
        </tr>
         <tr>
            <td colspan="7">
                <div class="artEdit">
                    <asp:Label ID="lbTitle" runat="server" Text="Title:" ></asp:Label>
                    <asp:RequiredFieldValidator ControlToValidate="tbTitle" ID="reqTitle" runat="server" ErrorMessage="Enter Title"  Display="None"  ValidationGroup="edit"></asp:RequiredFieldValidator>
                    <br />                     
                    <asp:TextBox ID="tbTitle" runat="server" Text = '<%#Bind("ART_TITLE") %>' Width="90%"></asp:TextBox>
                    <cc1:ValidatorCalloutExtender ID="vceTitle" runat="server" TargetControlID="reqTitle">
                    </cc1:ValidatorCalloutExtender>
                </div>
                <div class="floatL">
                    <asp:Label ID="lbRelDate" runat="server" Text="Release date:" CssClass="artEdit" ></asp:Label>
                    <asp:TextBox ID="tbReleaseDate"  runat="server" Text = '<%#Bind("RELEASE_DATE", "{0:dd.MM.yyyy}") %>'></asp:TextBox>
                    <asp:ImageButton ID="btReleaseDate" ImageUrl="~/CMS/_res/images/button/calendar.png" runat="server" />&nbsp
                    <cc1:CalendarExtender ID="ceReleaseDate" runat="server" TargetControlID="tbReleaseDate" PopupPosition="BottomLeft" PopupButtonID ="btReleaseDate">
                    </cc1:CalendarExtender>

                    <asp:Label ID="lbExpDate" runat="server" Text="Expire date:"  CssClass="artEdit"></asp:Label>
                    <asp:TextBox ID="tbExpireDate"  runat="server" Text = '<%#Bind("EXPIRE_DATE", "{0:dd.MM.yyyy}") %>'></asp:TextBox>
                    <asp:ImageButton ID="btExpireDate" ImageUrl="~/CMS/_res/images/button/calendar.png" runat="server" />
                    <cc1:CalendarExtender ID="ceExpireDate" runat="server" TargetControlID="tbExpireDate" PopupPosition="BottomLeft" PopupButtonID="btExpireDate"  >
                    </cc1:CalendarExtender>
                </div>
                <div class="floatR artEdit">

                    <asp:CheckBox ID="cbActive" runat="server" Checked='<%#Bind("ACTIVE") %>' Text=" Active" />&nbsp&nbsp
                    <asp:CheckBox ID="cbApproved" runat="server" Checked='<%#Bind("APPROVED") %>' Text=" Approved" />&nbsp&nbsp
                    <asp:CheckBox ID="cbComments" runat="server" Checked='<%#Bind("COMMENTS_ENABLED") %>' Text=" Comments enabled" />&nbsp&nbsp
                    <asp:CheckBox ID="cbMembers" runat="server" Checked='<%#Bind("MEMBERS_ONLY") %>' Text=" Members only" />
                </div>
                <div class="clearing"></div>
            </td>
        </tr>
        <tr>
            <th class="artEdit">
                <asp:Label ID="lbAbstract" runat="server" Text="Abstract:"  CssClass="artEdit"></asp:Label>
                <asp:RequiredFieldValidator  ControlToValidate="tbAbstract" ID="reqAbstract" runat="server" ErrorMessage="Enter Abstract" Display="Dynamic" ValidationGroup="edit"></asp:RequiredFieldValidator>
                <%--<asp:CustomValidator runat="server" ID="cvAbstract" ControlToValidate="fckAbstract" SetFocusOnError="true" Display="none" ErrorMessage="Enter Abstract" Text="*" OnServerValidate="ValFCK" ClientValidationFunction="ValidateAbstract" ValidateEmptyText="true" ValidationGroup="edit"></asp:CustomValidator>--%>


   <%--                 <cc1:ValidatorCalloutExtender ID="vceAbstract" runat="server" TargetControlID="cvAbstract">
                    </cc1:ValidatorCalloutExtender>--%>
                    </th>
        </tr>
        <tr>
            <td colspan="7">
                <asp:TextBox ID="tbAbstract" Width="100%" TextMode="MultiLine" Rows="4" runat="server" Text='<%#Bind("ART_ABSTRACT") %>'></asp:TextBox>
                <%--<FCKeditorV2:FCKeditor ID="fckAbstract" runat="server" Height="150px" Value='<%#Bind("ART_ABSTRACT") %>'></FCKeditorV2:FCKeditor>--%>
            </td>
        </tr>
        <tr>
            <th class="artEdit">
                <asp:Label ID="lbBody" runat="server" Text="Body:"  CssClass="artEdit"></asp:Label>
                <asp:RequiredFieldValidator EnableClientScript="false" ControlToValidate="fckBody" ID="reqBody" runat="server" ErrorMessage="Enter Body"  Display="Dynamic" ValidationGroup="edit"></asp:RequiredFieldValidator>
            </th>
        </tr>
        <tr>
            <td colspan="7">
                <FCKeditorV2:FCKeditor ID="fckBody" runat="server" Height="300px" Width="100%" Value='<%#Bind("ART_BODY") %>'></FCKeditorV2:FCKeditor>
            </td>
        </tr>
        <tr>
            <td colspan="1">
                    <asp:Button ID="btSave" CommandName="Update" runat="server" Text="Save" ValidationGroup="edit" ></asp:Button>
            </td>
            <td colspan="6">
                <asp:Button ID="btCancel" CommandName="Cancel" runat="server" Text="Cancel" ></asp:Button>
            </td>
        </tr>
        <tr>
            <td colspan="7">  
                <div style="border-top:#000000 solid 2px; width:100%; height:1px;"></div>
            </td> 
        </tr>
    </EditItemTemplate>
    <InsertItemTemplate>
        <tr>
            <td colspan="7">  
                <div style="border-top:#000000 solid 2px; width:100%; height:1px;"></div>
            </td> 
        </tr>
        <tr>
            <td colspan="7">
                <div class="artEdit">
                    <asp:Label ID="lbTitle" runat="server" Text="Title:" ></asp:Label>                     
                    <asp:RequiredFieldValidator ControlToValidate="tbTitle" ID="reqTitle" runat="server" ErrorMessage="Enter Title" Text="*" Display="None"  ValidationGroup="insert"></asp:RequiredFieldValidator>
                    <br />
                    <asp:TextBox ID="tbTitle" runat="server" Text = '<%#Bind("ART_TITLE") %>' Width="90%"></asp:TextBox>
                    <cc1:ValidatorCalloutExtender ID="vceTitle" runat="server" TargetControlID="reqTitle">
                    </cc1:ValidatorCalloutExtender>
                </div>
                <div class="floatL">
                    <asp:Label ID="lbRelDate" runat="server" Text="Release date:" CssClass="artEdit" ></asp:Label>
                    <asp:TextBox ID="tbReleaseDate"  runat="server" Text = '<%#Bind("RELEASE_DATE", "{0:dd.MM.yyyy}") %>'></asp:TextBox>
                    <asp:ImageButton ID="btReleaseDate" ImageUrl="~/CMS/_res/images/button/calendar.png" runat="server" />&nbsp
                    <cc1:CalendarExtender ID="ceReleaseDate" runat="server" TargetControlID="tbReleaseDate" PopupPosition="BottomLeft" PopupButtonID ="btReleaseDate">
                    </cc1:CalendarExtender>

                    <asp:Label ID="lbExpDate" runat="server" Text="Release date:" CssClass="artEdit" ></asp:Label>
                    <asp:TextBox ID="tbExpireDate"  runat="server" Text = '<%#Bind("EXPIRE_DATE", "{0:dd.MM.yyyy}") %>'></asp:TextBox>
                    <asp:ImageButton ID="btExpireDate" ImageUrl="~/CMS/_res/images/button/calendar.png" runat="server" />
                    <cc1:CalendarExtender ID="ceExpireDate" runat="server" TargetControlID="tbExpireDate" PopupPosition="BottomLeft" PopupButtonID="btExpireDate"  >
                    </cc1:CalendarExtender>
                </div>
                <div class="floatR artEdit">

                    <asp:CheckBox ID="cbActive" runat="server" Checked='<%#Bind("ACTIVE") %>' Text="Active" />&nbsp&nbsp
                    <asp:CheckBox ID="cbApproved" runat="server" Checked='<%#Bind("APPROVED") %>' Text="Approved" />&nbsp&nbsp
                    <asp:CheckBox ID="cbComments" runat="server" Checked='<%#Bind("COMMENTS_ENABLED") %>' Text="Comments enabled" />&nbsp&nbsp
                    <asp:CheckBox ID="cbMembers" runat="server" Checked='<%#Bind("MEMBERS_ONLY") %>' Text="Members only" />
                </div>
                <div class="clearing"></div>
            </td>
        </tr>
        <tr>
            <th class="artEdit">
                <asp:Label ID="lbAbstract" runat="server" Text="Abstract:" CssClass="artEdit"></asp:Label>                     
                <asp:RequiredFieldValidator   ControlToValidate="tbAbstract" ID="reqAbstract" runat="server" ErrorMessage="Enter Abstract"  Display="Dynamic" ValidationGroup="insert"></asp:RequiredFieldValidator>
        </tr>
        <tr>
            <td colspan="7">
                <asp:TextBox ID="tbAbstract" Width="100%"  TextMode="MultiLine" Rows="4" runat="server" Text='<%#Bind("ART_ABSTRACT") %>'></asp:TextBox>
                <%--<FCKeditorV2:FCKeditor ID="fckAbstract" runat="server" Height="150px" Value='<%#Bind("ART_ABSTRACT") %>'></FCKeditorV2:FCKeditor>--%>
            </td>
        </tr>
        <tr>
            <th class="artEdit">
                <asp:Label ID="lbBody" runat="server" Text="Body:" CssClass="artEdit"></asp:Label>                     
                <%--<asp:RequiredFieldValidator EnableClientScript="false"  ControlToValidate="fckBody" ID="reqBody" runat="server" ErrorMessage="Enter Body" Display="Dynamic" ValidationGroup="insert"></asp:RequiredFieldValidator>--%>
                <asp:CustomValidator runat="server" ID="cvBody" ControlToValidate="fckBody" SetFocusOnError="true" Display="dynamic" Text="The Body is required" ClientValidationFunction="ValidateBody" ValidateEmptyText="true" ValidationGroup="insert"></asp:CustomValidator>                
            </th>
        </tr>
        <tr>
            <td colspan="7">
                <FCKeditorV2:FCKeditor ID="fckBody" runat="server" Height="300px" Width="100%" Value='<%#Bind("ART_BODY") %>'></FCKeditorV2:FCKeditor>
            </td>
        </tr>
        <tr>
            <td colspan="1">
                    <asp:Button ID="btSave" CommandName="Insert" runat="server" Text="Save"  ValidationGroup="insert"></asp:Button>
            </td>
            <td colspan="1">
                <asp:Button ID="btCancel" CommandName="Cancel" runat="server" Text="Cancel" ></asp:Button>
            </td>
        </tr>
        <tr>
            <td colspan="7">  
                <div style="border-top:#000000 solid 2px; width:100%; height:1px;"></div>
            </td> 
        </tr>
    </InsertItemTemplate>   
    <EmptyDataTemplate>
    no data yet.
        <asp:Button ID="btNewED" runat="server" CommandName="addnew" onclick="btNew_Click" Text="Add New" ValidationGroup="insert"></asp:Button>
    </EmptyDataTemplate>

</SM:smListView>

<asp:LinqDataSource ID="ldsList" runat="server" 
    ContextTypeName="eMenikDataContext" EnableDelete="True" 
    EnableUpdate="True" EnableInsert="True"
    onselecting="ldsContent_Selecting" OnInserting="ldsContent_Inserting" 
    TableName="ARTICLEs" >
</asp:LinqDataSource>
<br />
