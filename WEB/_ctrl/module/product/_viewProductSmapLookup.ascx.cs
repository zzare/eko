﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _ctrl_module_product_viewProductSmapLookup : BaseControlView
{

    public object Data;
    public object[] Parameters;

    public int ParentID;
    public string TitleID;
    public string ValueID;


    protected void Page_Load(object sender, EventArgs e)
    {


        if (Parameters != null)
        {

            if (Parameters.Length > 1 && Parameters[1] != null)
                TitleID = Parameters[1].ToString();
            if (Parameters.Length > 2 && Parameters[2] != null)
                ValueID = Parameters[2].ToString();
        }
       
    }



    protected string RenderItems()
    {

        if (Data == null)
            return "";
        string ret = "";

        ret = ViewManager.RenderView("~/_ctrl/menu/lookup/_viewMenuLevelHierarchy.ascx", Data, Parameters);

        return ret;

    }





}
