﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

/// <summary>
/// Summary description for COMMENT
/// </summary>
public partial class CUSTOMER
{

    public string GetName { get { return this.FIRST_NAME + " " + this.LAST_NAME; } }

    public static CUSTOMER GetCurrentCustomer()
    {
        return CUSTOMER.GetCustomerByID(SM.EM.BLL.Membership.GetCurrentUserID());
    }

    public static  CUSTOMER GetCustomerByID(Guid id)
    {
        eMenikDataContext db = new eMenikDataContext();

        return db.CUSTOMERs.SingleOrDefault(w => w.CustomerID == id);
    }


    public static string GetRandomPassword(int length)
    {
        char[] chars = "1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();
        string password = string.Empty;
        Random random = new Random();

        for (int i = 0; i < length; i++)
        {
            int x = random.Next(1, chars.Length);
            //Don't Allow Repetation of Characters
            if (!password.Contains(chars.GetValue(x).ToString()))
                password += chars.GetValue(x);
            else
                i--;
        }
        return password;
    }
    public static string EncKey = "!5664a#KN";
    
    public static string EncryptPass(string pass)
    {
        string key = EncKey;

        Utils.Cryptography.Encryption enc = new Utils.Cryptography.Encryption();
        enc.Password = key;

        return enc.Encrypt(pass);
    }
    public static string DecryptPass(string pass)
    {
        string key = EncKey;
        Utils.Cryptography.Encryption enc = new Utils.Cryptography.Encryption();
        enc.Password = key;

        return enc.Decrypt(pass);
    }




   public class Common
    {
        public static string RenderBackendDetailsHref(Guid user)
        {
            return "~/c/user/customer/manage-customer.aspx?u=" + user.ToString() ;
        }    
    
    
    }

    //partial void OnValidate(System.Data.Linq.ChangeAction action)
    //{
    //}

}
