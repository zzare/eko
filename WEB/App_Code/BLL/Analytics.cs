﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using UrlRewritingNet.Web;

/// <summary>
/// Summary description for Analyitcs
/// </summary>
namespace SM.EM
{

    public class Analyitcs
    {

        public static string CodeMain()
        {
            string code = "<script type=\"text/javascript\">var gaJsHost = ((\"https:\" == document.location.protocol) ? \"https://ssl.\" : \"http://www.\");document.write(unescape(\"%3Cscript src='\" + gaJsHost + \"google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E\"));</script>" +
                        "<script type=\"text/javascript\">try {var pageTracker = _gat._getTracker(\"UA-9390672-1\");pageTracker._trackPageview();} catch(err) {}</script>";


            if (HttpContext.Current != null)
                if (HttpContext.Current.Request.IsLocal)
                    return "";

            return code;
        }
        
        public static string CodeUser(string userTracker) { 
        

            string trackerUser = "var pageTrackerU = _gat._getTracker(\"UA-9393272-1\");pageTrackerU._trackPageview();";
            string trackerAll = "var pageTracker = _gat._getTracker(\"UA-9390672-1\");pageTracker._setDomainName(\"none\");pageTracker._trackPageview();";

            string trackerSingleUser = "";
            
            
            if (!string.IsNullOrEmpty(userTracker ))
                trackerSingleUser = "var pageTrackerSU = _gat._getTracker(\"" + userTracker + "\");pageTrackerSU._trackPageview();";


            string code = "<script type=\"text/javascript\">var gaJsHost = ((\"https:\" == document.location.protocol) ? \"https://ssl.\" : \"http://www.\");document.write(unescape(\"%3Cscript src='\" + gaJsHost + \"google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E\"));</script>" +
                        "<script type=\"text/javascript\">try {" + trackerUser  + trackerAll  +  trackerSingleUser  + "} catch(err) {}</script>";

            if (HttpContext.Current != null)
                if (HttpContext.Current.Request.IsLocal)
                    return "";

            return code;        
        }

    }

}
