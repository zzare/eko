﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_cmsUserSelectNoUP.ascx.cs" Inherits="SM.UI.Controls._cmsUserSelectNoUP" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="SB" TagName="UserList" Src="~/CMS/controls/user/_cmsUserList.ascx"   %>
<%@ Register TagPrefix="SB" TagName="UserFilter" Src="~/CMS/controls/user/_cmsUserFilter.ascx"   %>


<asp:HiddenField ID="hfUserSelect" runat="server" />

        
<asp:Panel CssClass="modCMSPopup" ID="panSelect" runat="server" style="display:none;">
    <asp:Label ID="Label1" runat="server" Text="Izberi uporabnike" CssClass="modCMSTitle"></asp:Label>
    

            <div class="qFilter">
                    <SB:UserFilter ID="objFilter" runat="server" />
            </div>

    
            <SB:UserList ID="objUserSelectList" runat="server" ShowCheckboxColumn="true" ShowManageColumn = "false" />

            <br />
            
            <asp:LinkButton ID="btAddSelectedUser" runat="server" onclick="btAddSelectedUser_Click" CssClass="squarebutton"><span>Dodaj IZBRANE uporabnike</span></asp:LinkButton>


    <asp:LinkButton ID="btCancel" runat="server" CssClass="squarebutton "><span>Prekliči</span></asp:LinkButton>

</asp:Panel>        


    
<cc1:ModalPopupExtender RepositionMode="None"   ID="mpeSort" runat="server" TargetControlID="hfUserSelect" PopupControlID="panSelect"
                        BackgroundCssClass="modCMSBcg" CancelControlID="btCancel" >
</cc1:ModalPopupExtender>
