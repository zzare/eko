﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="step_design.ascx.cs" Inherits="_em_settings__ctrl_step_design" %>

<script type="text/javascript">

    function thumbPrev() {

        xOffset = 200;
        yOffset = 25;
        posX = 0;
        dir = "left";

        $("img.imgThumb").hover(function(e) {
            if ($(window).width() / 2 < e.pageX) {
                dir = "right";
                posX = ($(window).width() - e.pageX) + yOffset;
            }
            else {
                dir = "left";
                posX = e.pageX + yOffset;
            }


            var a = $(this).parent()[0];
                        $("body").append("<div id='previewTheme' style='background:transparent;position:absolute;display:none;'><img style='visibility:visible;' src='" + a.href + "' />" + "</div>");

            $("#previewTheme").show().css("top", (e.pageY - xOffset) + "px").css(dir, posX + "px").fadeIn("fast");


        }, function() {
            $("#previewTheme").remove();
//            $(this).next("img").hide();
        })

        $("img.imgThumb").mousemove(function(e) {
            if ($(window).width() / 2 < e.pageX) {
                dir = "right";
                posX = ($(window).width() - e.pageX) + yOffset;
            }
            else {
                dir = "left";
                posX = e.pageX + yOffset;
            }

            $("#previewTheme").show().css("top", (e.pageY - xOffset) + "px").css(dir, posX + "px");

        });


        $("a.desPrev").click(function() {
//        var bt = $(this).parent().parent().parent().find("div.desTCont").find("a");
        var bt = $(this).parent().find("div.desTCont a");
        window.location.href = bt[0].href;
            return false;

        });
            
    }

    $(document).ready(thumbPrev);

</script>

<asp:LinqDataSource ID="ldsList" runat="server" 
    ContextTypeName="eMenikDataContext" 
    onselecting="ldsList_Selecting"
    TableName="ARTICLEs">
</asp:LinqDataSource>
<h1>Izberi spletno predlogo
    <a href="javascript:void(0)" class="btsettStatusHelp"><img src='<%=Page.ResolveUrl("~/_inc/images/icon/help.png") %>' /></a>
    <div class="settStatusWrapp" >
    <div class="settStatus">
    Izberi obliko in barvno kombinacijo spletne strani.To narediš tako, da klikneš na barvno kombinacijo želene oblikovne predloge. Ne skrbi, temo ali barvo lahko zamenjaš kadarkoli s klikom na gumb "Nastavitve" in nato na gumb "Oblika".
    <br />
    Namig: Če se z miško zapelješ nad sliko oblikovne kombinacije in se tam zaustaviš, se izbrana slika poveča.
    </div></div>
</h1>
<br />
<asp:ListView EnableViewState="false"  ID="lvList" runat="server" DataSourceID="ldsList" 
    onitemdatabound="lvList_ItemDataBound" >
    <LayoutTemplate>
        <div class="designCont" >
            <div runat="server" id="itemPlaceholder" ></div>
            
        </div>
        
    </LayoutTemplate>

    <ItemTemplate>
        <div class="settDes" style="float:left; margin-right:15px; width:205px;">
            <div style="height:500px;">
                <h1 style="padding-bottom:10px;"><%# Eval("MASTER_TITLE") %></h1>
                <a class="desPrev" href='<%# Page.ResolveUrl((Eval("IMG_BIG") ?? "").ToString() ) %>'   ><img class="imgThumb" src='<%# Page.ResolveUrl((Eval("IMG_THUMB") ?? "").ToString() ) %>' alt='<%# Eval("MASTER_TITLE") %>' title='<%# Eval("MASTER_TITLE") %>' /></a>
                <br />
                <div>
                    <asp:ListView ID="lvTheme" runat="server" >
                        <LayoutTemplate>
                            <div class="desTCont" >
                                <div runat="server" id="itemPlaceholder" ></div>
                            </div>
                        </LayoutTemplate>

                        <ItemTemplate>
                            <div class="desTheme <%#  IsSelectedTheme(Eval("THEME_ID")," lbt_wizz_seltheme_sel") %>">

                                <asp:LinkButton EnableViewState="false" ID="btSelect" CommandArgument='<%# Eval("THEME_ID") %>' OnClick="btSelect_OnClick" runat="server">
                                    <img width="16" height="16"  src='<%# Page.ResolveUrl((Eval("IMG_THUMB") ?? "").ToString() ) %>' alt='<%# Eval("THEME_DESC") %>' title='<%# Eval("THEME_DESC") %>' /><%# Eval("THEME_DESC")%>
                                </asp:LinkButton>
                            </div>
                        
                        </ItemTemplate>
                    </asp:ListView>
                
                </div>
            </div>

        
<%--        
            <div class="desMTitle"><h1>Spletna predloga - <%= SM.BLL.Common.Emenik.Data.PortalName()  %> <%# Eval("MASTER_TITLE") %></h1></div>
              <div style="width:99%;">    
                        <div id="mastcontentwrapper">
                        <div id="mastcontentcolumn">
                        <p><%# Eval("MASTER_DESC") %></p>

                        </div>
                        </div>
                        <div id="mastleftcolumn">                                        

                        </div>
                    
                    </div>
                    
                    
                    
                    <div class="clearing"></div>
                    
                    
            <h2>Barvne palete grafične predloge <%= SM.BLL.Common.Emenik.Data.PortalName()  %> <%# Eval("MASTER_TITLE") %>:</h2>
--%>
        </div>
  
    </ItemTemplate>
</asp:ListView>
<div class="clearing"></div>
