﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using SM.EM.BLL;
namespace SM.EM.UI.Controls
{
    public partial class _viewGalleryImageList : BaseControlView
    {

        public object Data;
        public object[] Parameters;

        public CMS_WEB_PART Cwp;
        public CWP_GALLERY CwpGallery;
        protected IEnumerable <vw_ImageLang > ImageLangList;
        protected int ImgTypeID=-1;

        public string Separator = "";

        public bool ShowEmptyTemplate = true;

        public bool ShowGalleryPopup = false;


        protected void Page_Load(object sender, EventArgs e)
        {

            BindData();
        }
        // bind data
        public void BindData()
        {

            if (Data == null && Cwp == null)
                return;

            // data
            if (Cwp == null)
                Cwp = (CMS_WEB_PART)Data;
            if (Cwp == null)
                return;
            if (Parameters != null)
            {
                if (Parameters.Length > 0 && Parameters[0] != null)
                    IsModeCMS = bool.Parse(Parameters[0].ToString());
                if (Parameters.Length > 1 && Parameters[1] != null)
                    CwpGallery = (CWP_GALLERY)Parameters[1];
                if (Parameters.Length > 2 && Parameters[2] != null)
                    ImageLangList = (IEnumerable<vw_ImageLang>)Parameters[2];
                if (Parameters.Length > 3 && Parameters[3] != null)
                    Separator = Parameters[3].ToString();
            }

            // load title
            litTitle.Text = Cwp.TITLE;
            divTitle.Visible = Cwp.SHOW_TITLE;

            int pSize = 1000;
            if (!IsModeCMS)
                pSize = Cwp.PAGE_SIZE ;

            if (ImageLangList == null)
                ImageLangList = GALLERY.GetImageListByCwpPaged(Cwp.CWP_ID, LANGUAGE.GetCurrentLang(), IsModeCMS, 1, pSize);

            //  get image type
            List<vw_ImageLang> listimgLang = ImageLangList.ToList();
            if (listimgLang != null && listimgLang.Count > 0)
                ImgTypeID = listimgLang[0].TYPE_ID;


            lvList.DataSource = ImageLangList;
            lvList.DataBind();

            if (lvList.Items.Count < 1 && !IsModeCMS) { this.Visible = false; return; }
            else 
                this.Visible = true;



        }

        public string RenderOpenGalleryPopup()
        {
            if (lvList.Items.Count == 0) return "";
            return GALLERY.Common.RenderOpenGalleryPopupAnimal(Cwp.CWP_ID , 0, "this");
        }



        void objArticleEdit_OnArticleListChanged(object sender, EventArgs e)
        {
            this.BindData();
        }


        protected string RenderCustomClass() {

            string ret = "";
            if (ImgTypeID <= 0)
                return ret;
            if (ImgTypeID  == SM.BLL.Common.ImageType.EmenikGalleryThumbCarousel)
                ret = " thSmall";

            return ret;
        
        
        }

        //protected void lvList_ItemCommand(object sender, ListViewCommandEventArgs e)
        //{
        //    if (e.CommandName == "editImage")
        //    {
        //        OnEditClick(this, new SM.BLL.Common.ClickIDEventArgs { ID = Int32.Parse(e.CommandArgument.ToString()) });
        //    }
        //}




    }
}
