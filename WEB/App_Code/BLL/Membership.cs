﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using SM.EM.BLL;


namespace SM.EM.BLL
{
    /// <summary>
    /// Summary description for Membership
    /// </summary>
    public partial class Membership
    {

        public static Guid GetCurrentUserID()
        {
            Guid ret = Guid.Empty ;
            if (HttpContext.Current == null)
                return ret;
            if (!HttpContext.Current.User.Identity.IsAuthenticated )
                return new Guid( HttpContext.Current.Request.AnonymousID );

            return GetUserID(HttpContext.Current.User.Identity.Name);

        }

        public static Guid GetUserID(string username)
        {
            MembershipUser usr = GetMembershipUser(username);
            if (usr == null)
                return Guid.Empty;
            return new Guid(usr.ProviderUserKey.ToString());
        }
        public static MembershipUser GetMembershipUser(string username)
        {

            // check cache
            if (SM.EM.Globals.Settings.ST.EnableCaching)
            {
                // if user in cache, return it
                if (BizObject.Cache[SM.EM.Caching.Key.MembershipUser(username)] != null)
                    return (MembershipUser)BizObject.Cache[SM.EM.Caching.Key.MembershipUser(username)];
            }

            MembershipUser ret = System.Web.Security.Membership.GetUser(username);

            // cache
            BizObject.CacheData(SM.EM.Caching.Key.MembershipUser(username), ret);

            return ret;

        }

        public static void RemoveFromCache(string username)
        {
            SM.EM.BLL.BizObject.Cache.Remove(SM.EM.Caching.Key.MembershipUser(username));
        }



        public class Common
        {

            public static string RenderCMSHref(Guid user)
            {
                return "~/cms/ManageUser-membership.aspx?u=" + user.ToString() ;
            }


        }

        //partial void OnValidate(System.Data.Linq.ChangeAction action)
        //{
        //}

    }
}