﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_cmsUserEdit.ascx.cs" Inherits="SM.UI.Controls._cmsUserEdit" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<div class = "qContEdit clearfix">
        
    <asp:Button ValidationGroup="usredit" ID="btSave" runat="server" Text="Save user" onclick="btSave_Click" CssClass="btnOrange " />

    <asp:Button ID="btCancel" runat="server" Text="Cancel" onclick="btCancel_Click" CssClass="btnRed"/>

    <asp:Button Visible="false" ID="btDeleteUser" runat="server" Text="Delete user" OnClientClick="javascript: return confirm('Ali res želiš zbrisati uporabnika????')" onclick="btDelete_Click" CssClass="btnRed"/>

    <br />

    
    <asp:Panel ID="panEdit" runat="server" Visible="false" CssClass="floatR">
        <div style="line-height:150%;">
    
        <asp:CheckBox ID="cbActive" runat="server" Text="Aktiven (ali je šel skozi vse nujno potrebne začetne nastavitve)" TextAlign="Left" />
        <br />

        <asp:CheckBox ID="cbActiveWWW" runat="server" Text="Stran aktivirana (javno dostopna)" TextAlign="Left" />
        <br />

        <asp:CheckBox ID="cbDemo" runat="server" Text="DEMO" TextAlign="Left" />
        <br />

        <asp:CheckBox ID="cbExampleTemplate" runat="server" Text="EXAMPLE template" TextAlign="Left" />
        <br />
        
        UserName: <h1><asp:Literal ID="tbUserName" runat="server"></asp:Literal></h1>
        <br />
        Email: <asp:TextBox ID="tbEmail" Enabled="true"  runat="server"></asp:TextBox>
        <br />
        PageName: <asp:TextBox ID="tbPageName" Enabled="true"  runat="server"></asp:TextBox>
        <br />
        Custom domain: <asp:TextBox ID="tbCustomDomain" Enabled="true"  runat="server"></asp:TextBox>
        <br />
        Naročilo domene:
        <asp:DropDownList ID="ddlOrderDomainStatus" runat="server">
            <asp:ListItem Text="NI NAROČENA" Value="0" ></asp:ListItem>
            <asp:ListItem Text="JE NAROČENA" Value="1" ></asp:ListItem>
            <asp:ListItem Text="PLAČANA" Value="2" ></asp:ListItem>
            <asp:ListItem Text="NAROČILO DOMENE NI DOVOLJENO" Value="3" ></asp:ListItem>
        </asp:DropDownList>   
        <br />
        Želena domena: <asp:TextBox ID="tbOrderedDomain" Enabled="true"  runat="server"></asp:TextBox>
        <br />
        Promocija: <asp:Literal ID="litBonus" runat="server"></asp:Literal>
                
        <br />
        MENU limit:
        <asp:TextBox ID="tbMenuCount" Enabled="true"  runat="server"></asp:TextBox>
        
        
        <br /><br />
        PortalID: 
        <asp:DropDownList ID="ddlPortalID" runat="server">
            <asp:ListItem Text="1dva3" Value="1" ></asp:ListItem>
            <asp:ListItem Text="cms.smardt.si" Value="2" ></asp:ListItem>
        </asp:DropDownList>
        <br />
        <asp:Button ID="btClearCache" runat="server" Text="CLEAR USERs CACHE" onclick="btClearCache_Click" />
        
        <asp:CheckBoxList ID="cblRoles" runat="server">
        </asp:CheckBoxList>
        </div>
        
        
        
    </asp:Panel>
    


    <h2>Podatki</h2>
    <br />
    REFERENCA: <asp:Literal ID="litCode" runat="server"></asp:Literal>
    <br />   
    POPUST[%]: 
    <asp:TextBox ID="tbDiscountPercent" runat="server"></asp:TextBox>
    <asp:RadioButtonList  RepeatLayout= "Flow" RepeatDirection  ="Horizontal"  ID="rblDiscount" runat="server"></asp:RadioButtonList>
    
    <h3>Custom CSS</h3>
    <asp:TextBox Width="50%" ID="tbCustomCSS" TextMode="MultiLine" Rows="30" runat="server"></asp:TextBox>
    <h3>Dodatno</h3>
    Zamenjaj dizajn:
    <asp:DropDownList ID="ddlTheme" runat="server"></asp:DropDownList>
    <asp:Button ID="btTheme" runat="server" Text="Zamenjaj" onclick="btTheme_Click" CssClass="btnRed"/>
    Google analytics tracker
    <asp:TextBox ID="tbGA" runat="server"></asp:TextBox>
    
    
</div>

