﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SM.EM.BLL;

namespace SM.EM.UI.Controls
{
    public partial class cwpPoll : BaseControl_CmsWebPart  
    {

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

        }
        protected void Page_Load(object sender, EventArgs e)
        {

            //    // edit mode
            //if (ParentPage.IsModeCMS)
            //{
            //    //objArticleList.OnEditArticleClick += new cmsArticleList.OnEditEventHandler(objArticleList_OnEditArticleClick);

            //}
        }

        public override void LoadData()
        {
            InitEditMode();
            LoadList();
        }

        protected void InitEditMode()
        {


            // edit mode
            if (ParentPage.IsModeCMS)
            {
                divEdit.Visible = true;

                // button actions
                //btEdit.HRef = "javascript:onEdit('" + mpeEdit.BehaviorID + "');";
                btShow.HRef = "javascript:onToggleCwpActive('" + btShow.ClientID + "','" + btHide.ClientID + "', '" + cwp.CWP_ID.ToString() + "' ,true);";
                btHide.HRef = "javascript:onToggleCwpActive('" + btShow.ClientID + "','" + btHide.ClientID + "','" + cwp.CWP_ID.ToString() + "', false);";

                // initial hiding of buttons
                if (cwp.ACTIVE)
                {
                    btShow.Attributes["style"] = "display:none";
                    btHide.Attributes["style"] = "display:";
                    //divEdit.Attributes["class"] = divEdit.Attributes["class"].Replace("cwpHidden", "");
                }
                else
                {
                    btShow.Attributes["style"] = "display:";
                    btHide.Attributes["style"] = "display:none";
                    //divEdit.Attributes["class"] += " cwpHidden";
                }

            }
            else
            {
                divEdit.Visible = false;
            }

            //// title
            //litTitle.Text = cwp.TITLE;
            //divTitle.Visible = cwp.SHOW_TITLE;


        }

        protected void LoadList() {

            if (cwp == null)
                return;

            //if (cwp.ID_GUID == null)
            //    return;

            objPollCWP.Data = cwp;
            objPollCWP.Parameters = new object[] { ParentPage.IsModeCMS };
            objPollCWP.LoadPoll();

            if (cwp.ID_GUID != null)
            {
                objPoll.Data = POLL.GetPollByID(cwp.ID_GUID.Value );
            }
            objPoll.ShowResultsIfVoted = (SM.BLL.Common.Emenik.Data.OutputCacheDuration() <= 0);
            objPoll.Parameters = new object[] { ParentPage.IsModeCMS, LANGUAGE.GetCurrentLang(), cwp.CWP_ID };
            objPoll.LoadPoll();
        }

        protected void btSettings_Click(object sender, EventArgs e)
        {
            CwpEdit edit = ParentPage.ArticleSettings;
            if (edit == null) return;

            edit.cwp = cwp;
            edit.CwpID = cwp.CWP_ID;
            edit.LoadData();
            edit.Zone = cwp.ZONE_ID;
        }

        public string RenderArhivHREF()
        {

            return Page.ResolveUrl(SM.EM.Rewrite.EmenikUserPollArchiveUrl(ParentPage.em_user.PAGE_NAME));
        }
        
    }
}