﻿<%@ Page ValidateRequest="false" Language="C#" MaintainScrollPositionOnPostback="true" MasterPageFile="~/_em/templates/fall/fall.master" AutoEventWireup="true" CodeFile="first-step.aspx.cs" Inherits="_em_help_first_step" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cph" runat="server">

<h1>Pozdravljeni v sistemu <%= SM.BLL.Common.Emenik.Data.PortalHostDomain()  %> za izdelavo spletnih strani</h1>
<p>Izbiraš lahko med dvema pristopoma izdelave strani, s takojšnjim urejanjem strani ali pričneš pri samem grajenju spletne strani. Če ne želiš sam do spletne strani ti omogočamo, da postavitev spletne strani naročiš, pri čemer mi poskrbimo za vnos vsebin in postavitev spletne strani.</p>

<br />
            <div id="firststepTwoCols" class="clearfix">
                <h2>Sam do spletne strani s pomočjo urejevalnika</h2>
                <div id="firststepLeft" >
                    <div class="firststepLeftInner">
                        <h3>Takojšnje urejanje vsebine</h3>
                        <img style="float:left;" src='<%=Page.ResolveUrl("~/_inc/images/design/1dva3/1dva3_startcontent.png") %>' />
                        <p>Spletna stran je sestavljena po prednastavljeni predlogi, katero lahko kasneje poljubno zamenjaš. Sistem 1dva3 te bo vodil na vstopno stran tvoje spletne strani, kjer pričneš z vnašanjem vsebin.
                            <br /><br />
                        </p>
                        <div class="clearing"></div>
                        <br />
                        <asp:LinkButton CssClass="BTN_firststepregister" style="float:left;" ID="btSkipWizard" runat="server" OnClick="btSkipWizard_OnClick">Začni z urejanjem vsebin</asp:LinkButton>
                        <div class="clearing"></div>
                    </div>
                </div>
                <div id="firststepMain" >
                    <div class="firststepMainInner">
                        <h3>Izdelava strani s pomočjo 1dva3 čarovnika</h3>
                        <img style="float:left;" src='<%=Page.ResolveUrl("~/_inc/images/design/1dva3/1dva3_wizzard.png") %>' />
                        <p>1dva3 čarovnik te vodi po logičnih korakih za samostojno izdelavo spletne strani. Na vsakem koraku ti pomagajo ostale komponente pomoči, ki so hitro dostopne.
                            <br /><br />
                        </p>
                        <div class="clearing"></div>
                        <br />
                        <asp:LinkButton CssClass="BTN_firststepregister" style="float:left;" ID="btStart" runat="server" OnClick="btStart_OnClick">Uporabi 1dva3 čarovnika</asp:LinkButton>
                        <div class="clearing"></div>
                    </div> 
                </div>
            </div>
            <div id="firststepRight" > 
               
            </div>
<br />
</asp:Content>

