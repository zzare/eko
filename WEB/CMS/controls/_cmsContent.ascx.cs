﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SM.EM.UI.CMS;
using FredCK.FCKeditorV2;

namespace SM.EM.UI.CMS.Controls
{
    public partial class CMS_cmsContent : BaseControl
    {
        public int _smapID = -1;
        public string _langID = "-1";

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public void LoadTab() {
            listContent.DataBind();
        }


        protected void ldsContent_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {

            if (_smapID == -1) return;

            eMenikDataContext db = new eMenikDataContext();

            var contents = from c in db.CONTENTs  where c.SMAP_ID == _smapID   select c;
            if (_langID != "-1") contents = contents.Where(c => c.LANG_ID == _langID);

            e.Result = contents ;
        }
        protected void listContent_ItemEditing(object sender, ListViewEditEventArgs e)
        {
            CloseInsert();
            ((Button)listContent.FindControl("btNew")).Visible = false;
            //listContent.EditIndex = e.NewEditIndex;
        }
        private void CloseInsert()
        {
            listContent.InsertItemPosition = InsertItemPosition.None;
            ((Button)listContent.FindControl("btNew")).Visible = true;
        }
        protected void btNew_Click(object sender, EventArgs e)
        {
            listContent.EditIndex = -1;
            listContent.InsertItemPosition = InsertItemPosition.LastItem ;
            ((Button)sender).Visible = false;
            
        }
        protected void listContent_ItemCanceling(object sender, ListViewCancelEventArgs e)
        {
            if (e.CancelMode == ListViewCancelMode.CancelingInsert || e.CancelMode == ListViewCancelMode.CancelingEdit )
            {
                CloseInsert();
            }
        }

        protected void listContent_ItemInserted(object sender, ListViewInsertedEventArgs e)
        {
            CloseInsert();
        }
        protected void ldsContent_Inserting(object sender, LinqDataSourceInsertEventArgs e)
        {
            CONTENT cont = (CONTENT)e.NewObject;
            cont.LANG_ID = _langID  ;
            cont.DATE_ADDED = DateTime.Now;
            cont.SMAP_ID = _smapID;
        }

        protected void listContent_ItemUpdated(object sender, ListViewUpdatedEventArgs e)
        {
            CloseInsert();
        }

        protected void listContent_ItemCreated(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType == ListViewItemType.InsertItem)
            {
                CheckBox cb = (CheckBox)e.Item.FindControl("cbActive");
                if (cb != null)
                    cb.Checked = true;
                Button btNew = ((Button)listContent.FindControl("btNew"));
                if (btNew != null) btNew.Visible = false;

            }
        }

}
}