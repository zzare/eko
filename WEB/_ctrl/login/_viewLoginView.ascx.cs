﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _ctrl_login_viewLoginView : BaseControlView
{

    //public object Data;
    //public object[] Parameters;
    protected Guid PageId;


    protected void Page_Load(object sender, EventArgs e)
    {
        if (PageId == Guid.Empty )
            LoadData();
    }



    public void LoadData()
    {

        if (Data == null)
        {
            //this.Visible = false;
            return;
        }
        PageId  = (Guid )Data;
        if (PageId == null || PageId == Guid.Empty )
            return;

        // load data
        if (Page.User.Identity.IsAuthenticated)
        {
            objLoggedIn.Visible = true;
            objLoggedIn.Data = Data;
            objLoggedIn.LoadData();
        }
        else
        {
            objLoggedOut.Visible = true;
            objLoggedOut.Data = Data;
            objLoggedOut.LoadData();
        }




    }






}
