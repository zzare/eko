﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_cmsPermissionList.ascx.cs" Inherits="SM.UI.Controls._ctrl_permission_cmsPermissionList" %>
<%@ Register Namespace="SM.UI.Controls" TagPrefix="SM" %>


<div class="qContList">

         <SM:smListView ID="lvList" runat="server"   InsertItemPosition="None" onitemcommand="lvList_ItemCommand"  >
            <LayoutTemplate>
                <table  class="tblData" cellpadding="0" cellspacing="0">
                        <thead>
                            <tr id="headerSort" runat="server">
                                <th ><asp:LinkButton ID="LinkButton4" CommandName="sort" CommandArgument="Scope" runat="server">Scope</asp:LinkButton></th>
                                <th ><asp:LinkButton ID="LinkButton10" CommandName="sort" CommandArgument="Object" runat="server">Object</asp:LinkButton></th>
                                <th ><asp:LinkButton ID="LinkButton11" CommandName="sort" CommandArgument="Action" runat="server">Action</asp:LinkButton></th>
                                <th ><asp:LinkButton ID="LinkButton1" CommandName="sort" CommandArgument="Type" runat="server">Type</asp:LinkButton></th>
                                <th width="100px" colspan="2" runat="server"  id="headManage" >Uredi</th>
                            </tr>
                        </thead>
                        <tbody id="itemPlaceholder" runat="server"></tbody>
                        <tfoot>
                            <tr>
                                <th style="text-align:right" colspan="5" id="footTH" runat="server"  >
                                    <asp:DataPager runat="server" ID="DataPager"  >
                                        <Fields>
                                            <asp:NextPreviousPagerField  />
                                            <asp:NumericPagerField ButtonCount="5" />
                                        </Fields>
                                    </asp:DataPager>
                                </th>
                            </tr>
                        </tfoot>
                    </table>
            </LayoutTemplate>
            
            <ItemTemplate>
                <tr  class='<%#  Container.DataItemIndex % 2 == 0 ? "  " : " odd " %>'  onmouseover ="this.className='<%#  Container.DataItemIndex % 2 == 0 ? "hovRow" : "hovRowodd" %>'" onmouseout ="this.className = GetCssClass(<%#  Container.DataItemIndex %>)">
                    <td>
                        <%# Eval("ScopeTitle")%>
                    </td>
                    <td>
                        <%# Eval("ObjectTitle")%>
                    </td>
                    <td>
                        <%# Enum.Parse(typeof(PERMISSION.Common.Action), Eval("Action").ToString()).ToString() %>
                    </td>                    
                    <td>
                        <%# Enum.Parse(typeof(PERMISSION.Common.Type), Eval("Type").ToString()).ToString()%>
                    </td>                    

                    <td width="50px" runat="server"  >
                        <asp:LinkButton ID="lbLogin" CommandArgument='<%# Eval("PermissionID") %>' CommandName="del" runat="server">delete</asp:LinkButton>
                    </td>                
                </tr>  
            
            </ItemTemplate>
            
            <EmptyDataTemplate>
                
                <div class="odd">
                    Seznam je prazen.
            
                </div>
                
            </EmptyDataTemplate>
            
        
        </SM:smListView>

</div>
