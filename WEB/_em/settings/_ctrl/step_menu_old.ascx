﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="step_menu_old.ascx.cs" Inherits="_em_settings__ctrl_step_menu_old" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Namespace="SM.UI.Controls" TagPrefix="SM" %>
<%@ Register TagPrefix="CMS" TagName="MenuSort" Src="~/_ctrl/menu/_menuSort.ascx"   %>




<SM:InlineScript ID="InlineScript1"  runat="server">



<script type="text/javascript">
function SetUniqueRadioButton(nameregex, current)
{
   re = new RegExp(nameregex);
   for(i = 0; i < document.forms[0].elements.length; i++){
      elm = document.forms[0].elements[i]
      if (elm.type == 'radio')
      {
         if (re.test(elm.name))
         {
            elm.checked = false;
         }
      }
   }
   current.checked = true;


}


function int_mSort() {
//    $("ul.sortCont").sortable({
//    cursor: "move",
////        appendTo:"body",
////        handle: 'li.sortItem',
//        revert: true,
//        items: "li",
//        opacity: 0.7,
//        //tolerance: "intersect",
//        //connectWith: [".sortCont ul"],
//        placeholder: "cwp_placeholder",
//        //forcePlaceholderSize: false,
////        dropOnEmpty: true,
//        zIndex: 1000000,
//        update: function(evn, ui) {
//            var index = $(ui.item).parent().children().index(ui.item);
//            var prnt = $(ui.item).parent()[0].id;
//            //$(evn.target).parent().children().index(evn.target);

//            if (index >= 0) {

//                alert('id:: ' + ui.item[0].id + ' ; pos :: ' + index + ' ; parent:: ' + prnt );
////                WebServiceCWP.MoveMenu(ui.item[0].id, index, prnt, Refresh);
//                WebServiceCWP.MoveMenuParent(ui.item[0].id, index, prnt);
//            }
//        }
//    });






//function int_mSort() {
//    $("#menuSort").sortable({
//        cursor: "move",
//        //handle: 'li.sortItem',
//        revert: true,
//        opacity: 1.0,
//        tolerance: "pointer",
//        placeholder: "cwp_placeholder",
//        dropOnEmpty: true,
//        zIndex: 1000,
//        update: function(evn, ui) {
//            var index = $(this).children().index(ui.item);
//            if (index >= 0) {
//                //alert(ui.item[0].id);
//                //WebServiceCWP.MoveMenu(ui.item[0].id, index, Refresh);
//                WebServiceCWP.MoveMenu(ui.item[0].id, index);
//            }
//        }
//    });

}

$(document).ready(function() { int_mSort(); });

</script>
</SM:InlineScript>


<h1>Uredi menijsko strukturo spletne strani
    <a href="javascript:void(0)" class="btsettStatusHelp"><img src='<%=Page.ResolveUrl("~/_inc/images/icon/help.png") %>' /></a>
    <div class="settStatusWrapp" >
    <div class="settStatus">
        Uredi navigacijo na spletni strani. To pomeni, da boš poljubno uredil in dodal podstrani kot so o nas, blog, galerija...
    </div></div>
</h1>
<div class="wizzNav clearfix">
    <div class="wizzNav_Twocols clearfix""> 
	    <div class="wizzNav_Maincol" >
	        <h2>Dodajanje nove podstrani:
	            <a href="javascript:void(0)" class="btsettStatusHelp"><img src='<%=Page.ResolveUrl("~/_inc/images/icon/help.png") %>' /></a>
                <div class="settStatusWrapp" >
                <div class="settStatus">
                Novo podstran dodaš tako da klikneš na gumb "Dodaj podstran" in vpišeš ime nove podstrani ter klikneš na gumb "Dodaj".                
                </div></div>
	        </h2>
	        <p>* Na voljo imaš <%= SM.BLL.Common.Emenik.SITEMAP_COUNT_DEFAULT  %> podstrani</p>
	        <div class="buttonwrapper">
                <a href="javascript:void(0);" ID="btNew" class="lbutton lbuttonAdd"  runat="server" ><span>Dodaj podstran</span></a>
            </div>
            <asp:Panel ID="panNew" runat="server" >
                <br />
                <asp:TextBox ID="tbTitle" class="wizzNav_TBX" runat="server" MaxLength="256" Width="200px" ></asp:TextBox>
                <cc1:TextBoxWatermarkExtender ID="tbwexTitle" runat="server"  WatermarkText="Tukaj vpiši naziv podstrani" TargetControlID ="tbTitle" WatermarkCssClass="wizzNav_Watermark"></cc1:TextBoxWatermarkExtender>
                <div class="buttonwrapper">
                    <asp:LinkButton ID="btAdd" runat="server" CssClass="lbutton lbuttonAdd" OnClick="btAdd_OnClick"><span>Dodaj</span></asp:LinkButton>
                    <asp:CustomValidator ID="cvSitemapCount" runat="server" ErrorMessage=""></asp:CustomValidator>
                </div>
            </asp:Panel>
            <cc1:CollapsiblePanelExtender ID="cpeNew" runat="server" Collapsed="true" TextLabelID="lSmapLangHeader" CollapseControlID="btNew" ExpandControlID="btNew" TargetControlID="panNew" ExpandDirection="Vertical" >
            </cc1:CollapsiblePanelExtender>
	    </div>
	    <div class="wizzNav_Rightcol" >
	        <h2>Vrstni red podstrani:
	            <a href="javascript:void(0)" class="btsettStatusHelp"><img src='<%=Page.ResolveUrl("~/_inc/images/icon/help.png") %>' /></a>
                <div class="settStatusWrapp" >
                <div class="settStatus">
                    Za urejanje vrstnega reda podstrani klikni na gumb "Uredi vrstni red" in potem s sistemom povleci & spusti uredi vrstni red podstrani (urejanje vrstnega reda spreminja vrstni red podstrani tudi na spletni strani).
                </div>
                </div>
	        </h2>
	        <p>* Poljubno uredi vrstni red podstrani</p>
	        <div class="buttonwrapper">
	            <asp:LinkButton ID="btSort" runat="server" CssClass="lbutton lbuttonEdit" onclick="btSort_Click"><span>Uredi vrstni red</span></asp:LinkButton>
            </div>
            <asp:HiddenField ID="hfSort" runat="server" />
	    </div>
    </div> 
	<div class="wizzNav_Leftcol" >
        <h2>Menijska struktura in tip postavitve:
   	        <a href="javascript:void(0)" class="btsettStatusHelp"><img src='<%=Page.ResolveUrl("~/_inc/images/icon/help.png") %>' /></a>
            <div class="settStatusWrapp" >
            <div class="settStatus">
            Za urejanje že ustvarjenih podstrani klikni na podstran, ki jo želiš urediti. Urediš lahko ime podstrani, njen prevod v tuj jezik in postavitev strani (določiš koliko stolpcev za vsebine želiš na podstrani).
            </div></div>
        </h2>
        <p>* Klikni na želeno podstran za urejanje imena za vse izbrane jezike ter za določitev tipa strani.</p>
        <asp:TreeView  ID="tvMenu" runat="server" CssClass="wizzNav_Tv" SelectedNodeStyle-CssClass="wizzNav_TvSel" 
        PathSeparator=":"         OnSelectedNodeChanged="tvMenu_SelectedNodeChanged"
                 ShowLines="false" NodeIndent="0" NodeWrap="true" SkipLinkText="" LeafNodeStyle-ImageUrl="~/_inc/images/webpart/btn_white_edit_on.jpg" SelectedNodeStyle-ImageUrl="~/_inc/images/webpart/btn_white_edit_on.jpg">
        </asp:TreeView>
	</div>
</div>

<div class="wizzNav_Separator"></div>



<asp:Panel ID="panEdit" runat="server">

    <h1>Urejanje menija:</h1>
    
    <SM:smListView ID="lvList" runat="server" DataKeyNames="SMAP_ID, LANG_ID" InsertItemPosition="None" >
        <LayoutTemplate>
            <table  class="wizzNav_TBum">
                    <thead>
                        <tr id="headerSort" runat="server">
                            <th class="wizzNav_TBum_th1">Jezik</th>
                            <th >
                                Naziv
   	                            <a href="javascript:void(0)" class="btsettStatusHelp"><img src='<%=Page.ResolveUrl("~/_inc/images/icon/help.png") %>' /></a>
                                <div class="settStatusWrapp" >
                                <div class="settStatus">
                                Uredi tekst ki bo prikazan v meniju in v naslovu strani.
                                </div></div>
                            </th>
                            <th >
                                Prikazovanje
    	                        <a href="javascript:void(0)" class="btsettStatusHelp"><img src='<%=Page.ResolveUrl("~/_inc/images/icon/help.png") %>' /></a>
                                <div class="settStatusWrapp" >
                                <div class="settStatus">
                                Z obkljukanjem možnosti poveš v katerih jezikih želiš to podstran prikazovati v meniju.
                                </div></div>
                            </th>
                            <th ></th>
                            <th >
                                <%= (SM.EM.Security.Permissions.IsAdvancedCms()) ? "Opis" :"" %> 
                            </th>
                            
                        </tr>
                    </thead>
                    <tbody id="itemPlaceholder" runat="server"></tbody>
                </table>
        </LayoutTemplate>
        
        <ItemTemplate>
            <tr  class='<%#  Container.DataItemIndex % 2 == 0 ? " " : " odd " %>'  onmouseover ="this.className='<%#  Container.DataItemIndex % 2 == 0 ? "hovRow" : "hovRowodd" %>'" onmouseout ="this.className = GetCssClass(<%#  Container.DataItemIndex %>)">
                <td>
                    <%#  Eval("LANGUAGE.LANG_DESC")%>
                </td>
                <td class="wizzNav_TBum_td1">
                    <asp:TextBox ID="tbTitle" class="wizzNav_TBX" runat="server" MaxLength="40" Width="160px" Text='<%#  Eval("SML_TITLE")%>'></asp:TextBox>
                </td>
                <td>
                    <asp:CheckBox ID="cbActive" runat="server" Checked='<%# Eval("SML_ACTIVE") %>' Text="&nbsp;&nbsp;&nbsp;Prikaži v meniju&nbsp;"  CssClass="wizzFootCbx" />
                    <cc1:ToggleButtonExtender ID="ToggleActive" runat="server"
                        TargetControlID="cbActive" 
                        ImageWidth="19" 
                        ImageHeight="19"
                        CheckedImageAlternateText="Odkljukaj"
                        UncheckedImageAlternateText="Obkljukaj"
                        UncheckedImageUrl="~/_inc/images/design/checkbox_off.gif" 
                        CheckedImageUrl="~/_inc/images/design/checkbox_on.gif"
                        DisabledCheckedImageUrl="~/_inc/images/design/checkbox_disabled.gif"
                        DisabledUncheckedImageUrl="~/_inc/images/design/checkbox_disabled.gif" />
                </td>
                <td>&nbsp;&nbsp;
                    <a href='<%# RenderEditHref(Container.DataItem) %>' >uredi vsebino</a>
                </td>
            <% if (SM.EM.Security.Permissions.IsAdvancedCms()) {%>
                <td colspan="3" class="wizzNav_TBum_td1">
                    <asp:TextBox ID="tbDesc" class="wizzNav_TBX" runat="server" MaxLength="100" Width="260px" Text='<%#  Eval("SML_DESC")%>'></asp:TextBox>
                </td>
            <%} %>
            </tr>  
            
        
        </ItemTemplate>
    </SM:smListView>
    
    <asp:PlaceHolder ID="phCustom" runat="server">
        <br />
        Custom CLASS:
        <asp:TextBox ID="tbCustomClass" class="wizzNav_TBX" runat="server" MaxLength="50" Width="160px" ></asp:TextBox>    
    </asp:PlaceHolder>
    
</asp:Panel>


<asp:Panel CssClass="modPopupDrag" ID="panSort" runat="server" style="display:none;">
        <div class="modPopup_header modPopup_headerSettings">
        <h4>UREDI VRSTNI RED V MENIJU</h4>
        <p>Z miško primi podstran in jo odvleci na želeno mesto.</p>
    </div>
    <div class="modPopupDrag_main">
    <span class="modPopup_mainTitle" >Z miško primi podstran in jo odvleci na želeno mesto</span>
    <br />
    
<%--    <asp:Label ID="Label1" runat="server" Text="Uredi vrstni red" CssClass="modTitle"></asp:Label>
--%>    
    
        <CMS:MenuSort ID="objSort" runat="server" ShowSaveButton="false"/>
    </div>
    <br />
    <div class="modPopup_footer">
        <div class="buttonwrapperModal">
            <%--<asp:LinkButton ID="btCancelSort" runat="server" CssClass="lbutton lbuttonDelete"><span>Prekliči</span></asp:LinkButton>--%>
            <asp:LinkButton ID="btCloseSort" OnClick="btCloseSort_Click" runat="server" CssClass="lbutton lbuttonConfirm"><span>Zapri</span></asp:LinkButton>
        </div>
    </div>
</asp:Panel>        

<cc1:ModalPopupExtender RepositionMode="None" BehaviorID="mpeSort"  ID="mpeSort" runat="server" TargetControlID="hfSort" PopupControlID="panSort"
                        BackgroundCssClass="modBcg" >
</cc1:ModalPopupExtender>

<br />
<div id="divPageMod" runat="server" class="divPageMod">
    <h1>Tip postavitve na spletni predstavitvi:
        <a href="javascript:void(0)" class="btsettStatusHelp"><img src='<%=Page.ResolveUrl("~/_inc/images/icon/help.png") %>' /></a>
        <div class="settStatusWrapp" >
        <div class="settStatus">
        Izberi koliko stolpcev za vsebine želiš na podstrani.
        </div></div>
    </h1>
    
    <SM:smListView ID="lvPageMod" runat="server" DataKeyNames="PMOD_ID" 
        InsertItemPosition="None" onitemdatabound="lvPageMod_ItemDataBound" >
        <LayoutTemplate>
            <div class="wizzNav_lvPageMod">
                <ul id="itemPlaceholder" runat="server">
                </ul>
            </div>                
        </LayoutTemplate>
        
        <ItemTemplate>
            <li>
                
                <asp:RadioButton ID="rbPageMod" runat="server" GroupName="pageMod" Text= '<%#  Eval("PMOD_TITLE")%>'/>
                <br />
                <img src='<%# Page.ResolveUrl(Eval("THUMB_URL").ToString())%>' alt='<%#  Eval("PMOD_TITLE")%>' />
                <br />
                
                <%#  Eval("PMOD_DESC")%>
                
            </li>
        
        </ItemTemplate>
    </SM:smListView>
    
    <div class="clearing"></div>
    <br />

    <div class="buttonwrapper">
        <asp:LinkButton ID="btSave" runat="server" CssClass="lbutton lbuttonConfirm" onclick="btSave_Click" ><span>SHRANI SPREMEMBE</span></asp:LinkButton>
        <asp:LinkButton ID="btDelete" runat="server" CssClass="lbutton lbuttonDelete" onclick="btDelete_Click" OnClientClick="javascript: return confirm('Ali res želite zbrisati izbrano podstran? Vsi morebitni vnosi bodo izgubljeni za vse jezike');"><span>ZBRIŠI IZBRANO PODSTRAN</span></asp:LinkButton>
    </div>    
</div>

