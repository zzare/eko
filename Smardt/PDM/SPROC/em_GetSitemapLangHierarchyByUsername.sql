IF EXISTS (SELECT * FROM sysobjects WHERE name='em_GetSitemapLangHierarchyByUsername' and type='P')
DROP Procedure em_GetSitemapLangHierarchyByUsername

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		zare
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].em_GetSitemapLangHierarchyByUsername
	-- Add the parameters for the stored procedure here

	@USERNAME         nvarchar(256)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	
WITH Hierarchy(SMAP_ID, LANG_ID, PARENT, SML_TITLE, SML_DESC, SML_ACTIVE, SMAP_ORDER, HLevel, ValuePath )
AS
(

	SELECT S.SMAP_ID, cast('si' as nchar(2)) AS LANG_ID, S.PARENT, cast('root' as nvarchar(256)) AS SML_TITLE, cast('root' as nvarchar(256)) AS SML_DESC, cast(0 as bit) AS SML_ACTIVE, 0 AS SMAP_ORDER, -1 as Hlevel, cast('' as nvarchar(max))
	  FROM SITEMAP S, MENU M
	 WHERE S.MENU_ID = M.MENU_ID AND
		   M.USERNAME = @USERNAME AND 
		   S.PARENT = -1 
	UNION ALL
	SELECT L.SMAP_ID, L.LANG_ID, S.PARENT, L.SML_TITLE, L.SML_DESC, L.SML_ACTIVE, S.SMAP_ORDER, H.HLevel + 1, H.ValuePath + ':' + cast(L.SMAP_ID as nvarchar(max))
	  FROM SITEMAP S, SITEMAP_LANG L, MENU M, EM_USER_CULTURE U, CULTURE C, Hierarchy H, EM_USER EM
	 WHERE S.SMAP_ID = L.SMAP_ID AND
		   S.MENU_ID = M.MENU_ID AND
		   M.USERNAME = @USERNAME AND
           H.SMAP_ID = S.PARENT AND
	       EM.USERNAME = M.USERNAME AND
           U.UserId = EM.UserId AND
           U.[DEFAULT] = 1 AND
           U.LCID = C.LCID AND
           L.LANG_ID = C.LANG_ID


)
SELECT * FROM Hierarchy ORDER BY HLevel asc ,SMAP_ORDER asc


END
GO
