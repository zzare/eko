﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class c_user__ctrl_settings_cntUserSettings : BaseControl  
{
    public EM_USER em_user;

    //public event EventHandler OnSaveSuccess;
    //public event EventHandler OnSaveError;

    protected void Page_Load(object sender, EventArgs e)
    {

    }



    public  void LoadData()
    {
        if (!Page.User.Identity.IsAuthenticated) return;

        // set error if exists
        //SetError();


        // get address
        ADDRESS a = em_user.ADDRESS;


        string type = "F";
        // get company 
        if (em_user.COMPANY_ID != null)
        {
            COMPANY c = em_user.COMPANY;

            txtCTax.Text = c.TAX_NUMBER;
            txtCName.Text = c.NAME;
            a = c.ADDRESS;
        }

        txtFirstName.Text = em_user.FIRSTNAME;
        txtLastName.Text = em_user.LASTNAME;
        txtEmail.Text = em_user.EMAIL;

        if (a != null)
        {
            txtStreet.Text = a.STREET;
            txtPostalCode.Text = a.POSTAL_CODE;
            txtCity.Text = a.CITY;
            txtPhone.Text = a.TELEPHONE;
            txtFax.Text = a.FAX;
        }


        if (em_user.USER_TYPE == EM_USER.Common.UserType.Pravna)
            type = "P";

        rblCustomerType.SelectedValue = type;

        // set view
        SetView();



    }

    public  bool SaveData()
    {
        if (!Page.User.Identity.IsAuthenticated)
            return false;

        Page.Validate("editUsrSett");
        if (!Page.IsValid)
            return false;

        eMenikDataContext db = new eMenikDataContext();

        //EM_USER user = EM_USER.GetUserByUserName(User.Identity.Name );
        int company = em_user.COMPANY_ID ?? -1;
        int address = em_user.ADDR_ID ?? -1;

        ADDRESS a;
        // if company
        if (rblCustomerType.SelectedValue == "P")
        {
            // update company
            COMPANY c;

            if (em_user.COMPANY_ID != null)
            {
                c = db.COMPANies.SingleOrDefault(w => w.COMPANY_ID == em_user.COMPANY_ID);
                a = db.ADDRESSes.SingleOrDefault(w => w.ADDR_ID == c.ADDR_ID);
                // address
                a.STREET = txtStreet.Text;
                a.POSTAL_CODE = txtPostalCode.Text;
                a.CITY = txtCity.Text;
                a.TELEPHONE = txtPhone.Text;
                a.FAX = txtFax.Text;
                db.SubmitChanges();
            }
            else
            {
                a = new ADDRESS();
                db.ADDRESSes.InsertOnSubmit(a);
                // address
                a.STREET = txtStreet.Text;
                a.POSTAL_CODE = txtPostalCode.Text;
                a.CITY = txtCity.Text;
                a.TELEPHONE = txtPhone.Text;
                a.FAX = txtFax.Text;
                db.SubmitChanges();

                c = new COMPANY();
                db.COMPANies.InsertOnSubmit(c);
            }

            // save company
            c.TAX_NUMBER = txtCTax.Text;
            c.NAME = txtCName.Text;
            c.ADDR_ID = a.ADDR_ID;
            db.SubmitChanges();

            company = c.COMPANY_ID;
        }
        else
        {
            // update address
            a = em_user.ADDRESS;

            if (em_user.ADDR_ID != null)
            {
                a = db.ADDRESSes.SingleOrDefault(w => w.ADDR_ID == em_user.ADDR_ID);

            }
            else
            {
                a = new ADDRESS();
                db.ADDRESSes.InsertOnSubmit(a);
            }

            // address
            a.STREET = txtStreet.Text;
            a.POSTAL_CODE = txtPostalCode.Text;
            a.CITY = txtCity.Text;
            a.TELEPHONE = txtPhone.Text;
            a.FAX = txtFax.Text;
            db.SubmitChanges();
            address = a.ADDR_ID;

            EM_USER.UpdateUserFistName(em_user.UserId, txtFirstName.Text, txtLastName.Text, txtEmail.Text);

        }


        // update em_user
        EM_USER.UpdateUserAddressCompany(em_user.UserId, company, address, EM_USER.Common.UserType.GetUserType(rblCustomerType.SelectedValue));

        // if RU specified, redirect back
        //RedirectBackIfSpecified();

        return true;

    }


    protected void SetView()
    {
        switch (rblCustomerType.SelectedValue)
        {
            case "F":
                mvType.SetActiveView(vFizicna);

                break;
            case "P":
                mvType.SetActiveView(vPravna);
                break;
        }
    }
    protected void rblCustomerType_SelectedIndexChanged(object sender, EventArgs e)
    {
        SetView();

    }

    protected void btSaveEdit_Click(object o, EventArgs e)
    {

        SaveData();
    }


}
