﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using SM.EM.BLL;
namespace SM.EM.UI.Controls
{
    public partial class _viewGalleryImageListGalSimple : BaseControlView
    {

        //public object Data;
        //public object[] Parameters;

        public string ItemClass = "";
        public string ItemRel = "";
        public string DefaultTitle = "";

        public Guid GalCwpID;
        public int GalleryOffset = 0;

        public IEnumerable <vw_ImageLang> ImageList;



        protected void Page_Load(object sender, EventArgs e)
        {

            BindData();
        }
        // bind data
        public void BindData()
        {


            // data
            if (ImageList == null)
                ImageList = (IEnumerable<vw_ImageLang>)Data;
            if (ImageList == null)
                return;
            if (Parameters != null)
            {
                if (Parameters.Length > 0 && Parameters[0] != null)
                    ItemClass = Parameters[0].ToString();
                if (Parameters.Length > 1 && Parameters[1] != null)
                    ItemRel = Parameters[1].ToString();
                if (Parameters.Length > 2 && Parameters[2] != null)
                    DefaultTitle = Parameters[2].ToString();
            }


            //  get image type

            lvList.DataSource = ImageList;
            lvList.DataBind();

            if (lvList.Items.Count < 1 && !IsModeCMS) { this.Visible = false; return; }
            else 
                this.Visible = true;



        }

        protected string RenderImageTitle(object o)
        {
            string ret = "";
            vw_ImageLang img = (vw_ImageLang)o;

            if (string.IsNullOrEmpty(img.IMG_TITLE))
                ret = DefaultTitle;
            else
                ret = img.IMG_TITLE;

            return ret;
        }


    }
}
