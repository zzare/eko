﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _ctrl_ad_adMainFooter : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }


    protected string RenderAdTitle(int id, int tip)
    {
        string ret = "izdelava spletnih strani";
        string url = "http://smardt.si";

        int sitemap = 0;

        if (Page is SM.EM.UI.BasePage)
        {
            SM.EM.UI.BasePage bp = (SM.EM.UI.BasePage)Page;
            sitemap = bp.CurrentSitemapID;
        }


        // logo
        if (id == 1)
        {
            if (LANGUAGE.GetCurrentLang() == "si")
            {
                ret = "izdelava spletnih strani";
                url = "http://smardt.si";
            }
            else if (LANGUAGE.GetCurrentLang() == "en")
            {
                ret = "web site development";
                url = "http://smardt.si";
            }
            else if (LANGUAGE.GetCurrentLang() == "ru")
            {
                ret = "разработка и создание интернет магазина";
                url = "http://smardt.si";
            }
        }// text
        else if (id == 2)
        {

            int type = sitemap % 4;

            if (type == 0 || type == 2)
            {
                if (LANGUAGE.GetCurrentLang() == "si")
                {
                    ret = "izdelava spletne trgovine";
                    url = "http://smardt.si/sl-SI/469/izdelava-spletnih-trgovin.aspx";
                }
                else if (LANGUAGE.GetCurrentLang() == "en")
                {
                    ret = "e-commerce";
                    url = "http://smardt.si/sl-SI/469/izdelava-spletnih-trgovin.aspx";
                }
                else if (LANGUAGE.GetCurrentLang() == "ru")
                {
                    ret = "разработка и создание интернет магазина";
                    url = "http://smardt.si/sl-SI/469/izdelava-spletnih-trgovin.aspx";
                }
            }
            else if (type == 1 )
            {

                if (LANGUAGE.GetCurrentLang() == "si")
                {
                    ret = "spletne trgovine";
                    url = "http://smardt.si/sl-SI/469/izdelava-spletnih-trgovin.aspx";
                }
                else if (LANGUAGE.GetCurrentLang() == "en")
                {
                    ret = "e-commerce";
                    url = "http://smardt.si/sl-SI/469/izdelava-spletnih-trgovin.aspx";
                }
                else if (LANGUAGE.GetCurrentLang() == "ru")
                {
                    ret = "разработка и создание интернет магазина";
                    url = "http://smardt.si/sl-SI/469/izdelava-spletnih-trgovin.aspx";
                }
            }

            else if (type == 3)
            {

                if (LANGUAGE.GetCurrentLang() == "si")
                {
                    ret = "izdelava spletnih strani";
                    url = "http://smardt.si";
                }
            }



        }



        // title
        if (tip == 1)
            return ret;
        else
            return url;


        //return ret;
    }
}
