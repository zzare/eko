﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SM.EM.BLL;
namespace SM.EM.UI.Controls
{
    public partial class popupImageNew : CwpEdit
    {
        //public ARTICLE Art;
//        public event EventHandler OnImageAdded;

        public bool ShowPickGallery = false;
        


        protected void Page_Load(object sender, EventArgs e)
        {
            objImageNew.OnImageAdded += new EventHandler(objImageNew_OnImageAdded);
            objImageNew.OnImageAddCanceled += new EventHandler(objImageNew_OnImageAddCanceled);
            objGalleryList.OnEditClick += new cmsGalleryList.OnEditEventHandler(objGalleryList_OnEditClick);



            // init data
            InitData();

        }


        protected void InitData() {


            // init add new image
            if (IDref == -1)
            {
                GALLERY g = GALLERY.GetGalleryByID(CwpID);
                if (g != null)
                    IDref = g.GAL_ID;
            }

            objImageNew.GalleryID = IDref;
            objImageNew.UploadFolder = IMAGE.Common.GetUserImageFolder();
            objImageNew.CwpID = CwpID;

            // init gallery select
            objGalleryList.UserID = ParentPage.em_user.UserId;
            objGalleryList.ExcludeID = IDref;
            objGalleryList.TypeID = SM.BLL.Common.ImageType.EmenikThumbDefault;

        }

        public override void LoadData()
        {
            Zone = cwp.ZONE_ID;

            InitData();

            objGalleryList.BindData();
            tpGallery.Visible = !objGalleryList.IsEmpty && ShowPickGallery;


            Show();
        }


        void objImageNew_OnImageAddCanceled(object sender, EventArgs e)
        {
            Hide();

        }
        void objImageNew_OnImageAdded(object sender, EventArgs e)
        {
            //litStatus.Text = "Image(s) has been added.";

            // hide add image
            Hide();

            // reload zone
            if (ParentPage.Zones.Keys.Contains(Zone))
                ParentPage.ReLoadZone(ParentPage.Zones[Zone], true);
        }


        void objGalleryList_OnEditClick(object sender, SM.BLL.Common.ClickIDEventArgs e)
        {

            if (!ParentPage.CanSave) return;

            // change gallery
            CMS_WEB_PART.UpdateID(CwpID, e.ID);

            // reload zone
            if (ParentPage.Zones.Keys.Contains(Zone))
                ParentPage.ReLoadZone(ParentPage.Zones[Zone], true);
        }


        //protected void btCancelEdit_Click(object sender, EventArgs e)
        //{
        //    mpeEdit.Hide();

        //}
        protected void btSaveEdit_Click(object sender, EventArgs e)
        {



            SaveData();

        }

        public void SaveData()
        {
            if (!ParentPage.CanSave) return;

            objImageNew.Save();

        }


        public void Show() {
            mpeEditImageNew.Show();
            this.Visible = true;

        }
        public void Hide()
        {
            mpeEditImageNew.Hide();
            this.Visible = false;

        }
}
}
