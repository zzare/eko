﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for BaseControlView
/// </summary>
public abstract class BaseControlView : System.Web.UI.UserControl
{
    public bool IsModeCMS = false;
    public object Data;
    public object[] Parameters; 
    public Guid ViewID;

    protected override void OnInit(EventArgs e)
    {
        this.EnableViewState = false;
        base.OnInit(e);
        ViewID = Guid.NewGuid();
    }


    protected override void OnLoad(EventArgs e)
    {
        if (Page is SM.EM.UI.BasePage_CmsWebPart) {
            IsModeCMS = ((SM.EM.UI.BasePage_CmsWebPart)Page).IsModeCMS;
        
        }


        base.OnLoad(e);
    }


}
