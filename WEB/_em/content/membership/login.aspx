﻿<%@ Page Language="C#" ValidateRequest="false" MasterPageFile="~/_em/templates/fall/fall.master" AutoEventWireup="true" CodeFile="login.aspx.cs" Inherits="_em_content_membership_login" Title="Untitled Page" %>
<%@ Register TagPrefix="SM" TagName="Login" Src="~/_em/content/membership/_ctrl/_cntLogin.ascx"   %>

<asp:Content ID="Content2" ContentPlaceHolderID="cph" Runat="Server">
    
                <div class="loginContW">
                    <h1><%= GetGlobalResourceObject("Modules", "LoginLogin")%></h1>
                    
                    <SM:Login ID="objLogin" runat="server" />
                    <br />
                    
                   <%-- Še nimate računa?
                    <br />
                    
                    <a class="Astepnext" style="margin-top:7px;" href='<%=  PreserveReturnUrl( Page.ResolveUrl(SM.EM.Rewrite.EmenikCustomerRegister (em_user.UserId) )) %>'><span>REGISTRIRAJTE SE</span></a>--%>
                    <div class="clearing"></div>
                </div>

    
    
    
</asp:Content>
