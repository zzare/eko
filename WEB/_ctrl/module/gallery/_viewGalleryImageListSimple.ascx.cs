﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using SM.EM.BLL;
namespace SM.EM.UI.Controls
{
    public partial class _viewGalleryImageListSimple : BaseControlView
    {

        //public object Data;
        //public object[] Parameters;

        public string ItemClass = "";
        public string ItemRel = "";
        public string DefaultTitle = "";
        public string ItemClassImage = ""; 
        public int ImageTypeID = -1;

        public  IQueryable<WebServiceP.GalImage> ImageList;



        protected void Page_Load(object sender, EventArgs e)
        {

            BindData();
        }
        // bind data
        public void BindData()
        {


            // data
            if (ImageList == null)
                ImageList = (IQueryable<WebServiceP.GalImage>)Data;
            if (ImageList == null)
                return;
            if (Parameters != null)
            {
                if (Parameters.Length > 0 && Parameters[0] != null)
                    ItemClass = Parameters[0].ToString();
                if (Parameters.Length > 1 && Parameters[1] != null)
                    ItemRel = Parameters[1].ToString();
                if (Parameters.Length > 2 && Parameters[2] != null)
                    DefaultTitle = Parameters[2].ToString();
            }


            //  get image type

            lvList.DataSource = ImageList;
            lvList.DataBind();

            if (lvList.Items.Count < 1 && !IsModeCMS) { this.Visible = false; return; }
            else 
                this.Visible = true;



        }

        protected string RenderImageTitle(object o)
        {
            string ret = "";
            WebServiceP.GalImage img = (WebServiceP.GalImage)o;

            if (string.IsNullOrEmpty(img.Title ))
                ret = DefaultTitle;
            else
                ret = img.Title;

            return ret;
        }


    }
}
