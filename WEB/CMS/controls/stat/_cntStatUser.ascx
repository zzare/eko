﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_cntStatUser.ascx.cs" Inherits="CMS_controls_stat_cntStatUser" %>
<%@ Register Assembly="System.Web.DataVisualization, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>
    
    


<asp:Chart id="chUsers" runat="server" Width="900px" Palette="BrightPastel"   >
    <Titles><asp:Title Text="Skupna statistika uporabnikov"></asp:Title></Titles>
    <Legends><asp:Legend Docking="Top"  ></asp:Legend></Legends>
    <series><asp:Series Name="Users"></asp:Series></series>
    <series><asp:Series Name="Orders"></asp:Series></series>
    <series><asp:Series Name="Payments"></asp:Series></series>
    <chartareas><asp:ChartArea Name="ChartArea1"></asp:ChartArea></chartareas>
</asp:Chart>



<asp:Chart id="chUsersByCampaign" runat="server" Width="900px"  >
    <Titles><asp:Title Text="Registrirani uporabniki po dnevih in po kampanjah "></asp:Title></Titles>
    <Legends><asp:Legend Docking="Top"  ></asp:Legend></Legends>
    <chartareas><asp:ChartArea Name="ChartArea1"></asp:ChartArea></chartareas>
</asp:Chart>


<asp:Chart id="chUsersWeekCombined" runat="server" Width="900px"   >
    <Titles><asp:Title Text="Registrirani uporabniki po tednih "></asp:Title></Titles>
    
    <chartareas><asp:ChartArea Name="cha1"></asp:ChartArea></chartareas>
    <chartareas><asp:ChartArea Name="cha2" ></asp:ChartArea></chartareas>
</asp:Chart>

<asp:Chart id="chUsersByMonth" runat="server" Width="900px"  >
    <Titles><asp:Title Text="Registrirani uporabniki po mesecih"></asp:Title></Titles>
    <chartareas><asp:ChartArea Name="cha1"></asp:ChartArea></chartareas>
</asp:Chart>


