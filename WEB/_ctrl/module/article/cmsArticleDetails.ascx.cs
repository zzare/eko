﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SM.EM.BLL;
namespace SM.EM.UI.Controls
{
    public partial class cmsArticleDetails : BaseControl_EMENIK
    {
#region Property


        private string containerCssClass = "contArticles";
        public string ContainerCssClass
        {
            get { return this.containerCssClass; }
            set { this.containerCssClass = value; }
        }
        private string appendToTitle = "&nbsp;";
        public string AppendToTitle
        {
            get { return this.appendToTitle; }
            set { this.appendToTitle = value; }
        }

        private bool showImage = false;
        public bool ShowImage
        {
            get { return this.showImage; }
            set { this.showImage = value; }
        }
        private bool showDesc = true;
        public bool ShowDesc
        {
            get { return this.showDesc; }
            set { this.showDesc = value; }
        }
        private int titleLength = -1;
        public int TitleLength
        {
            get { return this.titleLength; }
            set { this.titleLength = value; }
        }
        private int descLength = -1;
        public int DescLength
        {
            get { return this.descLength; }
            set { this.descLength = value; }
        }


        public int ArtID
        {
            get
            {
                int _artID = -1;
                if (Request.QueryString["art"] == null) return _artID;
                if (!Int32.TryParse(SM.EM.Helpers.GetQueryString("art"), out _artID)) return _artID;
                return _artID;
            }

        }

#endregion

        Data.ArticleData _art;
        public Data.ArticleData art
        {
            get
            {
                //if (_art != null) return _art;

                //return ARTICLE.GetArticleByIDSitemap(ArtID, CurrentSitemapID);
                return _art;
            }
            set { _art = value; }
        }
        protected IMAGE_TYPE ImgType { get; set; }



        protected void InitData() {
            divArticles.Attributes["class"] = ContainerCssClass + " ";
        }

        protected void Page_Load(object sender, EventArgs e)
        {

                LoadData();
        }

        public void LoadData() {

            if (ArtID < 1) { this.Visible = false; return; }
            //art = ARTICLE.GetArticleByID(ArtID, ParentPage.em_user.UserId );

            if (art == null) { this.Visible = false; return; }

            // init
            if (!IsPostBack) InitData();

            // init gallery
            if (art.CWP_GAL != null && art.CWP_GAL != Guid.Empty)
            {
                objGalleryArticle.CwpID = art.CWP_GAL.Value;
                //objGalleryArticle.LoadData();
            }

            int imgTypeID = SM.BLL.Common.ImageType.ArticleDetailsThumbDefault ;
            if (art.TYPE_ID != null)
            if (art.TYPE_ID.Value  > 0 )
                imgTypeID = art.TYPE_ID.Value ;

            ImgType  =  IMAGE_TYPE.GetImageTypeByID(imgTypeID);

        
        }

        protected bool HasImage(object o)
        {
            Data.ArticleData art = (Data.ArticleData)o;
            if (art.IMG_ID == null)
                return false;

            if (ARTICLE.HasVideo(art.EMBEDD))
                return false;

            return true;


        }

        protected string RenderGalleryStyle()
        {
            string ret = "style='display:none;'";

            if (art == null) return "";
            if (ImgType == null) return ret;

            if (!HasImage(art))
                return ret;

            int padd = 20;

            // set fixed width
            
            if (ImgType.WIDTH > 0)
            {
                ret = (padd + ImgType.WIDTH).ToString();

                ret = " style=\"float:left; width:" + ret + "px\" ";
            }

            return ret;

        }


        protected string RenderArtTitle()
        {
            if (art == null) return "";
            return Helpers.CutString(art.ART_TITLE, TitleLength - AppendToTitle.Length) + AppendToTitle;

        }
        protected string RenderArtDesc()
        {
            if (!ShowDesc) return "";

            if (art == null) return "";

            
            string desc = art.ART_ABSTRACT;
            if (string.IsNullOrEmpty(desc))
                desc = art.ART_ABSTRACT;

            //return Server.HtmlDecode(desc);
            return Helpers.CutString(desc, DescLength ) ;

        }

        protected string RenderArtBody()
        {
            if (art == null) return "";

            return  Server.HtmlDecode(art.ART_BODY);
        }

        protected string RenderDatePublished()
        {

            DateTime date = art.DATE_ADDED;
            if (art.RELEASE_DATE != null && art.RELEASE_DATE.Value.Date > art.DATE_ADDED.Date)
                date = art.RELEASE_DATE.Value;

            if (date == date.Date) // if time is not set, show only date part
                return string.Format("{0:dd.MM.yyyy} ", date);
            else
                return string.Format("{0:dd.MM.yyyy} ", date);
        }

        protected string RenderAdditionalInfo()
        {
            Data.ArticleData a = art;
            string ret = "";
            //if (!ShowCategory && !ShowDate)
            //    return ret;
            //if (ShowCategory)
                ret = ARTICLE.Render.RenderCategoryHref(a, "artBox_artCategory", ParentPage.em_user.PAGE_NAME);

            if (!string.IsNullOrEmpty(ret) )
                ret = "&nbsp;|&nbsp;" + ret;
            //if (ShowDate)
                ret = ARTICLE.Render.RenderDatePublished(a) + ret;

            return ret;
        }


        protected string RenderVideo()
        {
            string ret = "";


            ret = ARTICLE.Render.RenderVideoEmbed(art, 440, 258, "float:left;");
            
            return ret;
        }

    }
}
