﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewPollAdd.ascx.cs" Inherits="_ctrl_module_poll_viewPollAdd" %>

<script language = "javascript" type="text/javascript">
    function addPollSave() {
        WebServiceCWP.UpdateID(cwpID, $('div.cPollAddList input:checked').val(), onPollAddSuccessSave, onPollSaveError);
    }
</script>



<div class="">

    <div class="modPopup_header modPopup_headerEdit">
        <h4>PRIKAŽI OBSTOJEČO ANKETO</h4>
        <p>Prikaži že obstoječo anketo</p>
    </div>
    <div class="modPopup_main">
        
        <h1>Izberi anketo, ki jo želiš prikazati.</h1>
        
        <div class="cPollAddList" > 
            <asp:ListView ID="lvList" runat="server">
                <LayoutTemplate>
                        <asp:PlaceHolder ID="itemPlaceholder" runat="server"/>
                </LayoutTemplate>

                <ItemTemplate>
                    <label>
                        <input <%# RenderChecked(Container.DataItem) %> type="radio" name="pollAdd" value='<%# Eval("PollID") %>' />
                        <%# Eval("PollQuestion")%>
                    </label>
                    <br />
                
                </ItemTemplate>
            </asp:ListView>
         </div>

    </div>   

    <div class="modPopup_footer"> 
        <div class="buttonwrapperModal">
            <a class="lbutton lbuttonConfirm" href="#" onclick="addPollSave(); return false"><span>Shrani</span></a>
            <a class="lbutton lbuttonDelete" href="#" onclick="closeModalLoader(); return false" ><span>Prekliči</span></a>
        </div>
    </div>
</div>

