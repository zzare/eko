﻿<%@ Page Language="C#" ValidateRequest="false" MasterPageFile="~/_em/templates/fall/fall.master" AutoEventWireup="true" CodeFile="ProductDetails.aspx.cs" Inherits="_em_content_product_product_details" Title="Untitled Page" %>
<%@ Register TagPrefix="EM" TagName="EditModule" Src="~/_ctrl/module/_edit/editModule.ascx"  %>


<asp:Content ID="Content2" ContentPlaceHolderID="cph" Runat="Server">

    <EM:EditModule ID="objEditModule" runat="server" />

    
    <asp:PlaceHolder ID="phContent" runat="server"></asp:PlaceHolder>
    

</asp:Content>
