﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace SM.UI.Controls
{
    public partial class _cmsOrderFilter : BaseControl 
    {
        public event EventHandler OnFilterChanged;

        public string SearchText { get { return tbSearch.Text.Trim(); } }
        public bool? Active
        {
            get
            {
                bool? ret = null;
                if (ddlActive.SelectedValue == "") return ret;

                if (ddlActive.SelectedValue == "0") return false;

                if (ddlActive.SelectedValue == "2") return true;

                return true;
            }
        }
        public short? Status {
            get {
                if (ddlStatus.SelectedValue == "-1")
                    return SHOPPING_ORDER.Common.Status.CANCELED;

                if (ddlStatus.SelectedValue == "2")
                    return SHOPPING_ORDER.Common.Status.CONFIRMED;
                if (ddlStatus.SelectedValue == "3")
                    return SHOPPING_ORDER.Common.Status.PAYED;
                if (ddlStatus.SelectedValue == "4")
                    return SHOPPING_ORDER.Common.Status.FINISHED;

                return null;
            }
        
        }


        protected void Page_Load(object sender, EventArgs e)
        {



        }

        public void LoadDefaults() {
            ddlActive.SelectedIndex = 0;
        
        
        }

        protected void ddlActive_SelectedIndexChanged(object sender, EventArgs e)
        {
            OnFilterChanged(this, new EventArgs());
        }


        protected void btSearch_Click(object sender, EventArgs e)
        {
            OnFilterChanged(this, new EventArgs());
        }
}
}