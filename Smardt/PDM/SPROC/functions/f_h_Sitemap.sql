
IF EXISTS (SELECT * FROM sysobjects WHERE name='f_h_Sitemap' and type='FN')
DROP Function [dbo].f_h_Sitemap
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].f_h_Sitemap (@parent int)
RETURNS TABLE
AS
RETURN (
	WITH Hierarchy(SMAP_ID, PARENT, HLevel)
	AS
	(
		SELECT S.SMAP_ID, S.PARENT, -1 as Hlevel
		  FROM SITEMAP S
		 WHERE S.SMAP_ID = @parent 
		UNION ALL
		SELECT S.SMAP_ID, S.PARENT, H.HLevel + 1
		  FROM SITEMAP S, Hierarchy H
		 WHERE H.SMAP_ID = S.PARENT 
		       
	)
	SELECT * FROM Hierarchy 
   )

GO
