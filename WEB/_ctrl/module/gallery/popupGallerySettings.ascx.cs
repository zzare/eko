﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SM.EM.BLL;
namespace SM.EM.UI.Controls
{
    public partial class popupGallerySettings : CwpEdit
    {



        protected void Page_Load(object sender, EventArgs e)
        {
            objGalleryEdit.OnGalleryEdited += new EventHandler(objGalleryEdit_OnGalleryEdited);

            // init edit gallery
            objGalleryEdit.UserName = ParentPage.em_user.USERNAME;
        }

        void objGalleryEdit_OnGalleryEdited(object sender, EventArgs e)
        {
            
        }


        protected void InitData() {

        }

        public override void LoadData()
        {
            Show();
            objSettings.cwp = cwp;
            objSettings.CwpID = CwpID;
            objSettings.LoadData();

            objGalleryEdit.GalleryID = IDref ;
            objGalleryEdit.BindData();
        }


        protected void btSaveEdit_Click(object sender, EventArgs e)
        {
            SaveData();

        }

        public void SaveData()
        {
            if (!ParentPage.CanSave) return;

           if (! objSettings.SaveData()) return;

           objGalleryEdit.Save();

           // reload zone
           ParentPage.ReLoadZone(ParentPage.Zones[Zone], true);

        }


        public void Show() {
            mpeGallerySettings.Show();
            this.Visible = true;

        }
        public void Hide()
        {
            mpeGallerySettings.Hide();
            this.Visible = false;

        }
    }
}
