﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_stepShipping2.ascx.cs" Inherits="_em_content_order__ctrl__stepShipping2" %>
<%@ Register Namespace="SM.EM.UI.Controls" TagPrefix="EM"  %>
<%@ Register TagPrefix="SM" TagName="EditModulePublic" Src="~/_ctrl/module/_edit/editModulePublic.ascx"   %>








 
<script type="text/javascript" >

    $(document).ready(function () {
                if ($('#<%= rbShipping.ClientID %>').is(':checked'))
                    $('#divShipping').show();
                else
                    $('#divShipping').hide();

                if ($('#<%= cbShipping.ClientID %>').is(':checked'))
                    $('#cShipping').show();
                else
                    $('#cShipping').hide();

        //        $('#cShipping').click(function() {
        //            selectCB('<%= rbShipping.ClientID %>');
        //        });
        //        $('#divShipping').click(function () {
        //            selectCB('<%= rbShipping.ClientID %>');
        //        });


        $('#<%= cbShipping.ClientID %>').change(function () {
            $('#cShipping').slideToggle();
        });
        $('#<%= rbShipping.ClientID %>').click(function () {
            if ($(this).is(':checked'))
                $('#divShipping').slideDown('fast');
//            alert('change');
            //$('#divShipping').toggle();
        });
        $('#<%= rbPersonal.ClientID %>').click(function () {
            if ($(this).is(':checked'))
                $('#divShipping').slideUp('fast');
            //$('#divShipping').toggle();
        });

    });


    
    function selectCB(cb) {
        $('#' + cb ).attr('checked', true);
    }


    function copyAddr(t) {
        //        alert($(t).parent('span.title').html());
        $('#<%= tbName.ClientID %>').val($(t).parent().find('span.title').html());
        $('#<%= tbAddress.ClientID %>').val($(t).parent().find('span.addr').html());
        $('#<%= tbCity.ClientID %>').val($(t).parent().find('span.city').html());
        $('#<%= tbPhone.ClientID %>').val($(t).parent().find('span.phone').html());

    }
    function copyAddrC(t) {
        $('#<%= tbCFirstname.ClientID %>').val($(t).parent().find('span.firstname').html());
        $('#<%= tbCLastname.ClientID %>').val($(t).parent().find('span.lastname').html());
        $('#<%= tbCAddress.ClientID %>').val($(t).parent().find('span.addr').html());
        $('#<%= tbCCity.ClientID %>').val($(t).parent().find('span.city').html());
        $('#<%= tbCTititle.ClientID %>').val($(t).parent().find('span.title').html());
        $('#<%= tbCTax.ClientID %>').val($(t).parent().find('span.tax').html());
        $('#<%= tbCPhone.ClientID %>').val($(t).parent().find('span.phone').html());

    }


</script>



<SM:EditModulePublic ID="objEditPublic" runat="server" />

<div class="stepHeaderW">
<h1>Vpišite podatke za račun in dostavo</h1>
</div>

<div class="stepShoppingCart" >
    <div class="contentRightW">
        <span class="contentRightHeadT_Text">PODATKI ZA RAČUN</span>
            <div class="contentRightMI">


                <div id="divCompany">                    
                    <div class="step3L">
                    <table class="tableOrder_step3_company" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                Ime*:    
                            </td>
                            <td>
                                <asp:TextBox ID="tbCFirstname" MaxLength="256"  runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator EnableClientScript="false" ValidationGroup="vgCompany" ControlToValidate="tbCFirstname" ID="RequiredFieldValidator8" runat="server" ErrorMessage="* Vpišite ime"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Priimek*:    
                            </td>
                            <td>
                                <asp:TextBox ID="tbCLastname" MaxLength="256"  runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator EnableClientScript="false" ValidationGroup="vgCompany" ControlToValidate="tbCLastname" ID="RequiredFieldValidator9" runat="server" ErrorMessage="* Vpišite priimek"></asp:RequiredFieldValidator>
                            </td>
                        </tr>


                                             
                        <tr>
                            <td>
                                Naslov*:
                            </td>
                            <td>
                                <asp:TextBox ID="tbCAddress" MaxLength="256"  runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator EnableClientScript="false" ValidationGroup="vgCompany" ControlToValidate="tbCAddress" ID="RequiredFieldValidator5" runat="server" ErrorMessage="* Vpišite naslov"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Poštna št. in kraj*:
                            </td>
                            <td>
                                <asp:TextBox ID="tbCCity" MaxLength="256"  runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator EnableClientScript="false" ValidationGroup="vgCompany" ControlToValidate="tbCCity" ID="RequiredFieldValidator6" runat="server" ErrorMessage="* Vpišite pošto in kraj"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Telefon*:
                            </td>
                            <td>
                                <asp:TextBox ID="tbCPhone" MaxLength="256"  runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator EnableClientScript="false" ValidationGroup="vgCompany" ControlToValidate="tbCPhone" ID="RequiredFieldValidator4" runat="server" ErrorMessage="* Vpišite telefon"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Podjetje:    
                            </td>
                            <td>
                                <asp:TextBox ID="tbCTititle" MaxLength="256"  runat="server"></asp:TextBox>
<%--                                <asp:RequiredFieldValidator EnableClientScript="false" ValidationGroup="vgCompany" ControlToValidate="tbCTititle" ID="RequiredFieldValidator4" runat="server" ErrorMessage="* Vpišite naziv"></asp:RequiredFieldValidator>--%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Id za DDV:    
                            </td>
                            <td>
                                <asp:TextBox ID="tbCTax" MaxLength="256"  runat="server"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator EnableClientScript="false" ValidationGroup="vgCompany" ControlToValidate="tbCTax" ID="RequiredFieldValidator7" runat="server" ErrorMessage="* Vpišite davčno št."></asp:RequiredFieldValidator>--%>
                            </td>
                        </tr>   
<%--                        <tr>
                            <td>DDV zavezanec:</td>
                            <td>
                                
                                <asp:CheckBox class="cbZavezanec" ID="cbZavezanec" runat="server" Text = "&nbsp;Sem zavezanec za DDV" />
                            </td>
                        </tr> --%>

                    </table> 
                    </div>
                    <div class="step3R">
                    <asp:PlaceHolder ID="phCopyC" runat="server">
                        <div class="ord_prevdataW">
                            <div class="ord_prevdataT"></div>
                            <div class="ord_prevdataM">
                                <div class="ord_prevdataMI">
                                    UPORABI MOJE PODATKE
                                    <br />
                                    <asp:ListView ID="lvListCAddr" runat="server" >
                                        <LayoutTemplate>
                                                <asp:PlaceHolder ID="itemPlaceholder" runat="server"/>
                                            
                                        </LayoutTemplate>
                                        
                                        <ItemTemplate>
                                            <div >
                                                <a href="#" onclick="copyAddrC(this); return false" >&laquo;&nbsp;<%# RenderDisplayName(Container.DataItem ) %></a>
                                                <span style="display:none" class="firstname" ><%# Eval("FirstName")%></span>
                                                <span style="display:none" class="lastname" ><%# Eval("LastName")%></span>
                                                <span style="display:none" class="addr" ><%# Eval("Address")%></span>
                                                <span style="display:none" class="city"><%# Eval("City")%></span>
                                                <span style="display:none" class="title"><%# Eval("Company")%></span>
                                                <span style="display:none" class="tax"><%# Eval("TaxNo")%></span>
                                                <span style="display:none" class="phone"><%# Eval("Phone")%></span>
<%--                                                <span style="display:none" class="zav"><%# Eval("Zavezanec").ToString().ToLower()%></span>--%>
                                            </div >
                                        
                                        
                                        </ItemTemplate>
                                        
                                    </asp:ListView>
                                </div>    
                            </div>
                            <div class="ord_prevdataB"></div>
                        </div>
                    </asp:PlaceHolder>                                       
                </div>
                <div class="clearing"></div>
                </div>  





                <br />
                <span class="contentRightHeadT_Text">NAČIN DOSTAVE</span>
            
            
                <asp:RadioButton class="RBTorder" GroupName="rbgShipping" ID="rbShipping" runat="server" Text="&nbsp;DOSTAVA NA NASLOV" />
                <br />
                <div id="divShipping">
                    <asp:CheckBox class="order_cbCompany" ID="cbShipping" runat="server" Text="&nbsp;Blago želim prejeti na naslov, ki je drugačen od zgornjih podatkov za račun" />
                    <div id="cShipping"  >
                        <div class="step3L">
                            Blago naj bo dostavljeno na spodnji naslov:
                            <br />

                            <table class="tableOrder_step3" border="0" cellpadding="0" cellspacing="0">
                                <tr >
                                    <td>
                                        Ime in Priimek:    
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tbName" MaxLength="256"  runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator EnableClientScript="false" ValidationGroup="vgShipping" ControlToValidate="tbName" ID="RequiredFieldValidator1" runat="server" ErrorMessage="* Vpišite ime in priimek"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Naslov:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tbAddress" MaxLength="256"  runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator EnableClientScript="false" ValidationGroup="vgShipping" ControlToValidate="tbAddress" ID="RequiredFieldValidator2" runat="server" ErrorMessage="* Vpišite naslov"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Poštna št. in kraj:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tbCity" MaxLength="256"  runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator EnableClientScript="false" ValidationGroup="vgShipping" ControlToValidate="tbCity" ID="RequiredFieldValidator3" runat="server" ErrorMessage="* Vpišite pošto in kraj"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Telefon:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tbPhone" MaxLength="256"  runat="server"></asp:TextBox>
                                    </td>
                                </tr>

                            </table>
                        </div>
                        <div class="step3R">
                        <asp:PlaceHolder ID="phCopyAddress" runat="server">
                            <div class="ord_prevdataW">
                                <div class="ord_prevdataT"></div>
                                <div class="ord_prevdataM">
                                    <div class="ord_prevdataMI">
                                        UPORABI MOJ NASLOV
                                        <br />
                                        <asp:ListView ID="lvListCopy" runat="server" >
                                            <LayoutTemplate>
                                                    <asp:PlaceHolder ID="itemPlaceholder" runat="server"/>
                                            
                                            </LayoutTemplate>
                                        
                                            <ItemTemplate>
                                                <div >
                                                    <a href="#" onclick="copyAddr(this); return false" >&laquo;&nbsp;<%# Eval("Title")%>, <%# Eval("Address")%></a>
                                                    <span style="display:none" class="title" ><%# Eval("Title")%></span>
                                                    <span style="display:none" class="addr" ><%# Eval("Address")%></span>
                                                    <span style="display:none" class="city"><%# Eval("City")%></span>
                                                    <span style="display:none" class="phone"><%# Eval("Phone")%></span>
                                                </div >
                                            </ItemTemplate>
                                        </asp:ListView>
                                    </div>    
                                </div>
                                <div class="ord_prevdataB"></div>
                            </div>
                        </asp:PlaceHolder>
                        </div>
                        <div class="clearing"></div>
                    </div>
                </div>
                
                
                
                <br />                
                <asp:RadioButton class="RBTorder" GroupName="rbgShipping" ID="rbPersonal" runat="server" Text="&nbsp;OSEBNI PREVZEM" />
                <br />
                Osebni prevzem na sedežu podjetja
                <br />
                

                <br />
                
                <%--<asp:CheckBox class="order_cbCompany" ID="cbCompany" runat="server" Text="&nbsp;Račun želim prejeti na podjetje" />--%>
              
                <br />                                
                <br />
                <asp:CustomValidator ID="cvShipping" runat="server" OnServerValidate="ValidateShipping" ValidationGroup="valShipping" ></asp:CustomValidator>
                <div class="stepnextbuttonW">
                <a class="Astepnext" runat="server" onserverclick="btNext_Click" ><span>NAPREJ K POVZETKU &raquo;</span></a>
                </div>
                <div class="clearing"></div>
            </div>
    </div>



</div>



