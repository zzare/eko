﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using SM.EM.BLL;

/// <summary>
/// Summary description for OnlineE24Payment
/// </summary>
public partial class OnlineE24Payment
{

    public static string GetTxId(int id) {


        return "sm" + id.ToString() + "ks";
    }

    public static string CertificatePath(){

        return ConfigurationManager.AppSettings["OnlineCert"];

        //return @"D:\_test\certifikati_za_trgovino_test_2\test\test.cer";
    }
    public static string Store()
    {
        return ConfigurationManager.AppSettings["OnlineStore"];
    }
    public static bool TestMode()
    {
        return !(ConfigurationManager.AppSettings["OnlineTestMode"] == "0");
    }
    public static string Action()
    {
        return Common.Action.AUTHORIZATION ;
    }

    

    public static string WebAddress()
    {
        return ConfigurationManager.AppSettings["OnlineWebAddress"];
    }
    public static string Port()
    {
        return ConfigurationManager.AppSettings["OnlinePort"];
    }
    public static string Context()
    {
        return ConfigurationManager.AppSettings["OnlineContext"];
    }
    public static string ID()
    {
        return ConfigurationManager.AppSettings["OnlineID"];
    }
    public static string Pass()
    {
        return ConfigurationManager.AppSettings["OnlinePass"];
    }

    public static string UpdateURL()
    {
        return "~/_inc/online_e24_update.ashx";
    }

    public static int StatusRefreshInterval()
    {
        return 8;
    }
    


   public class Common
    {



        public static string RenderUpdateHref(int txId)
        {
            return UpdateURL() + "?txId=" + txId.ToString();
        }
        public static string RenderStatusHref(Guid ordrID)
        {
            return SM.BLL.Common.ResolveUrlFull(SM.EM.Rewrite.OrderProductUrl(SM.BLL.Custom.MainUserID , ordrID, 5));
            //return "~/_inc/mp_update.aspx?txId=" + txId.ToString();
        }

        public static string RenderErrorHref(Guid ordrID)
        {
            return SM.BLL.Common.ResolveUrlFull(SM.EM.Rewrite.OrderProductUrl(SM.BLL.Custom.MainUserID, ordrID, 5) + "&e=1");
            //return "~/_inc/mp_update.aspx?txId=" + txId.ToString();
        }


        public class Status {
            public static string NOT_SET = "";
            public static string APPROVED = "APPROVED"; // Uspešna avtorizacija 
            public static string NOT_APPROVED = "NOT APPROVED"; //Neuspešna avtorizacija
            public static string CAPTURED = "CAPTURED"; // Uspešna avtorizacija in poravnava
            public static string NOT_CAPTURED = "NOT CAPTURED"; // Neuspešna avtorizacija
            public static string DENIED_BY_RISK = "DENIED BY RISK"; // Blokirana transakcija zaradi varnostnih parametrov nastavljenih s strani banke.
            public static string HOST_TIMEOUT = "HOST TIMEOUT"; // Neprocesirana transakcija zaradi tehničnih oziroma komunikacijskih težav pri povezovanju z avtorizacijskim sistemom.

            public static string VOIDED = "VOIDED"; // Stornirana avtorizacija
            public static string REVERSED = "REVERSED"; // Stornirana avtorizacija

            public static string FAILED = "FAILED"; // error - custom status
            public static string INITIALIZED = "INITIALIZED"; // initialized - custom status

        }

        public class Action
        {
            public static string PURCHASE = "1";
            public static string AUTHORIZATION = "4";
            public static string CAPTURE = "5";
            public static string REVERSAL = "3";
            public static string VOID = "9";
            
        }

        //public static  string  GetStatusFromTrans(string transStat) {
        //    string ret = Status.NOT_SET;

        //    switch (transStat)
        //    {
        //        case "INITIALIZING":
        //            ret = Status.INITIALIZING;
        //            break;
        //        case "INITIALIZED":
        //            ret = Status.INITIALIZED;
        //            break;
        //        case "PROCESSED":
        //            ret = Status.PROCESSED;
        //            break;
        //        case "FAILED":
        //            ret = Status.FAILED;
        //            break;
        //        case "CANCELLED":
        //            ret = Status.CANCELLED;
        //            break;
        //        default:
        //            ret = Status.FAILED;
        //            break;
        //    }


        //    return ret;
        
        //}
    
    
    }

    //partial void OnValidate(System.Data.Linq.ChangeAction action)
    //{
    //}




}

//    /// <summary>
///// Summary description for MP_TRANS
//    /// </summary>
//public partial class MP_TRANS
//{

//    public string GetTxId()
//    {
//        return "";
//    }


//}
