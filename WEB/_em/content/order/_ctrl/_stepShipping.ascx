﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_stepShipping.ascx.cs" Inherits="_em_content_order__ctrl__stepShipping" %>
<%@ Register Namespace="SM.EM.UI.Controls" TagPrefix="EM"  %>
<%@ Register TagPrefix="SM" TagName="EditModulePublic" Src="~/_ctrl/module/_edit/editModulePublic.ascx"   %>

<script type="text/javascript" >

    $(document).ready(function() {
        if ($('#<%= cbCompany.ClientID %>').is(':checked'))
            $('#divCompany').show();
        else
            $('#divCompany').hide();

        $('#cShipping').click(function() {
            selectCB('<%= rbShipping.ClientID %>');
        });
        $('#divCompany').click(function() {
            selectCB('<%= cbCompany.ClientID %>');
        });            

        $('#<%= cbCompany.ClientID %>').change(function() {
            $('#divCompany').toggle();

        });

    });

    function copyAddr(t) {
//        alert($(t).parent('span.title').html());
        $('#<%= tbName.ClientID %>').val($(t).parent().find('span.title').html());
        $('#<%= tbAddress.ClientID %>').val($(t).parent().find('span.addr').html());
        $('#<%= tbCity.ClientID %>').val($(t).parent().find('span.city').html());
        $('#<%= tbPhone.ClientID %>').val($(t).parent().find('span.phone').html());

    }
    function copyAddrC(t) {
        $('#<%= tbCTititle.ClientID %>').val($(t).parent().find('span.title').html());
        $('#<%= tbCAddress.ClientID %>').val($(t).parent().find('span.addr').html());
        $('#<%= tbCCity.ClientID %>').val($(t).parent().find('span.city').html());
        $('#<%= tbCTax.ClientID %>').val($(t).parent().find('span.tax').html());
        if ($(t).parent().find('span.zav').html() == 'true')
            $('#<%= cbZavezanec.ClientID %>').attr('checked', true);
        else
            $('#<%= cbZavezanec.ClientID %>').removeAttr('checked');
    
    }
    
    function selectCB(cb) {
        $('#' + cb ).attr('checked', true);    
    }


</script>



<SM:EditModulePublic ID="objEditPublic" runat="server" />

<div class="stepHeaderW">
<h1>Izberite naslov za dostavo artiklov</h1>
</div>

<div class="stepShoppingCart" >
    <div class="contentRightW">
        <span class="contentRightHeadT_Text">NASLOV ZA DOSTAVO</span>
            <div class="contentRightMI">
            
            
                <asp:RadioButton class="RBTorder" GroupName="rbgShipping" ID="rbShipping" runat="server" Text="&nbsp;DOSTAVA NA NASLOV" />
                <br />
                <div id="cShipping"  >
                    <div class="step3L">
                                    Blago bo dostavljeno na spodnji naslov:
                    <br />

                    <table class="tableOrder_step3" border="0" cellpadding="0" cellspacing="0">
                        <tr >
                            <td>
                                Ime in Priimek:    
                            </td>
                            <td>
                                <asp:TextBox ID="tbName" MaxLength="256"  runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator EnableClientScript="false" ValidationGroup="vgShipping" ControlToValidate="tbName" ID="RequiredFieldValidator1" runat="server" ErrorMessage="* Vpišite ime in priimek"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Naslov:
                            </td>
                            <td>
                                <asp:TextBox ID="tbAddress" MaxLength="256"  runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator EnableClientScript="false" ValidationGroup="vgShipping" ControlToValidate="tbAddress" ID="RequiredFieldValidator2" runat="server" ErrorMessage="* Vpišite naslov"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Poštna št. in kraj:
                            </td>
                            <td>
                                <asp:TextBox ID="tbCity" MaxLength="256"  runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator EnableClientScript="false" ValidationGroup="vgShipping" ControlToValidate="tbCity" ID="RequiredFieldValidator3" runat="server" ErrorMessage="* Vpišite pošto in kraj"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Telefon:
                            </td>
                            <td>
                                <asp:TextBox ID="tbPhone" MaxLength="256"  runat="server"></asp:TextBox>
                            </td>
                        </tr>

                    </table>
                    </div>
                    <div class="step3R">
                    <asp:PlaceHolder ID="phCopyAddress" runat="server">
                        <div class="ord_prevdataW">
                            <div class="ord_prevdataT"></div>
                            <div class="ord_prevdataM">
                                <div class="ord_prevdataMI">
                                    UPORABI MOJ NASLOV
                                    <br />
                                    <asp:ListView ID="lvListCopy" runat="server" >
                                        <LayoutTemplate>
                                                <asp:PlaceHolder ID="itemPlaceholder" runat="server"/>
                                            
                                        </LayoutTemplate>
                                        
                                        <ItemTemplate>
                                            <div >
                                                <a href="#" onclick="copyAddr(this); return false" ><%# Eval("Title")%>, <%# Eval("Address")%></a>
                                                <span style="display:none" class="title" ><%# Eval("Title")%></span>
                                                <span style="display:none" class="addr" ><%# Eval("Address")%></span>
                                                <span style="display:none" class="city"><%# Eval("City")%></span>
                                                <span style="display:none" class="phone"><%# Eval("Phone")%></span>
                                            </div >
                                        </ItemTemplate>
                                    </asp:ListView>
                                </div>    
                            </div>
                            <div class="ord_prevdataB"></div>
                        </div>
                    </asp:PlaceHolder>
                    </div>
                    <div class="clearing"></div>
                </div>

                
                
                <br />                
                <asp:RadioButton class="RBTorder" GroupName="rbgShipping" ID="rbPersonal" runat="server" Text="&nbsp;OSEBNI PREVZEM" />
                <br />
                Osebni prevzem na sedežu podjetja
                <br />
                

                <br />
                
                <asp:CheckBox class="order_cbCompany" ID="cbCompany" runat="server" Text="&nbsp;Račun želim prejeti na podjetje" />
                <div id="divCompany">                    
                    <div class="step3L">
                    <table class="tableOrder_step3_company" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                Naziv:    
                            </td>
                            <td>
                                <asp:TextBox ID="tbCTititle" MaxLength="256"  runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator EnableClientScript="false" ValidationGroup="vgCompany" ControlToValidate="tbCTititle" ID="RequiredFieldValidator4" runat="server" ErrorMessage="* Vpišite naziv"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Davčna št.:    
                            </td>
                            <td>
                                <asp:TextBox ID="tbCTax" MaxLength="256"  runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator EnableClientScript="false" ValidationGroup="vgCompany" ControlToValidate="tbCTax" ID="RequiredFieldValidator7" runat="server" ErrorMessage="* Vpišite davčno št."></asp:RequiredFieldValidator>
                            </td>
                        </tr>   
                        <tr>
                            <td>DDV zavezanec:</td>
                            <td>
                                
                                <asp:CheckBox class="cbZavezanec" ID="cbZavezanec" runat="server" Text = "&nbsp;Sem zavezanec za DDV" />
                            </td>
                        </tr>                                              
                        <tr>
                            <td>
                                Naslov:
                            </td>
                            <td>
                                <asp:TextBox ID="tbCAddress" MaxLength="256"  runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator EnableClientScript="false" ValidationGroup="vgCompany" ControlToValidate="tbCAddress" ID="RequiredFieldValidator5" runat="server" ErrorMessage="* Vpišite naslov"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Poštna št. in kraj:
                            </td>
                            <td>
                                <asp:TextBox ID="tbCCity" MaxLength="256"  runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator EnableClientScript="false" ValidationGroup="vgCompany" ControlToValidate="tbCCity" ID="RequiredFieldValidator6" runat="server" ErrorMessage="* Vpišite pošto in kraj"></asp:RequiredFieldValidator>
                            </td>
                        </tr>

                    </table> 
                    </div>
                    <div class="step3R">
                    <asp:PlaceHolder ID="phCopyC" runat="server">
                        <div class="ord_prevdataW">
                            <div class="ord_prevdataT"></div>
                            <div class="ord_prevdataM">
                                <div class="ord_prevdataMI">
                                    UPORABI MOJE PODJETJE
                                    <br />
                                    <asp:ListView ID="lvListCAddr" runat="server" >
                                        <LayoutTemplate>
                                                <asp:PlaceHolder ID="itemPlaceholder" runat="server"/>
                                            
                                        </LayoutTemplate>
                                        
                                        <ItemTemplate>
                                            <div >
                                                <a href="#" onclick="copyAddrC(this); return false" ><%# Eval("Title")%>, <%# Eval("Address")%></a>
                                                <span style="display:none" class="title" ><%# Eval("Title")%></span>
                                                <span style="display:none" class="addr" ><%# Eval("Address")%></span>
                                                <span style="display:none" class="city"><%# Eval("City")%></span>
                                                <span style="display:none" class="tax"><%# Eval("Tax")%></span>
                                                <span style="display:none" class="zav"><%# Eval("Zavezanec").ToString().ToLower()%></span>
                                            </div >
                                        
                                        
                                        </ItemTemplate>
                                        
                                    </asp:ListView>
                                </div>    
                            </div>
                            <div class="ord_prevdataB"></div>
                        </div>
                    </asp:PlaceHolder>                                       
                </div>
                <div class="clearing"></div>
                </div>                
                <br />                                
                <br />
                <asp:CustomValidator ID="cvShipping" runat="server" OnServerValidate="ValidateShipping" ValidationGroup="valShipping" ></asp:CustomValidator>
                <div class="stepnextbuttonW">
                <a class="Astepnext" runat="server" onserverclick="btNext_Click" ><span>NAPREJ K POVZETKU &raquo;</span></a>
                </div>
                <div class="clearing"></div>
            </div>
    </div>



</div>



