﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class _err_Login : SM.EM.UI.BasePage 
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        CheckPermissions = false;

    }
    protected void Page_Load(object sender, EventArgs e)
    {
        SetTitle("Login");

        if (!Context.User.Identity.IsAuthenticated)
        {
            phLogin.Visible = true;
            phForbidden.Visible = false;
        
        }
        else
        {
            phLogin.Visible = false;
            phForbidden.Visible = true;

        }

    }

}
