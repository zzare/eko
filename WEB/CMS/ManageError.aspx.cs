﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
namespace SM.UI.CMS
{
    public partial class CMS_Intranet_ManageError : SM.EM.UI.CMS.BasePage_CMS 
    {

        protected int ErrorID { get { 
            return int.Parse(Request.QueryString["id"] ?? "-1"); 
        }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //throw new Exception("test");
            
            if (!IsPostBack)
            {
                if (ErrorID > 0)
                {
                    ERROR_LOG err = ERROR_LOG.GetErrorByID(ErrorID);
                    // load error details
                    litDetail.Text = "<h1>" + err.Message + "</h1>" + err.StackTrace.Replace("\n", "<br/>");
                    phList.Visible = false;
                
                }
                else
                {
                    phList.Visible = true;
                    cpeQList.Collapsed = false;
                }

            }
        }


    }
}