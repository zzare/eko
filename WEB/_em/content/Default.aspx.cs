﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class _em_content_Default : SM.EM.UI.BasePage_CmsWebPart 
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //cmsContent.CurrentSitemapID = CurrentSitemapID;
        //cmsArticle.CurrentSitemapID = CurrentSitemapID;
    }

    public override void LoadPageModControls()
    {
        base.LoadPageModControls();

        if (PageModule < 1)
            return;

        //objOneColumn.Visible = false;
        //objTwoColumn.Visible = false;


        string ctrlSrc = "";



        
        switch (PageModule)
        {
            case SM.BLL.Common.PageModule.OneColumn:
                //objOneColumn.Visible = true;
                ctrlSrc = "~/_em/content/_ctrl/_pmodOneColumn.ascx";
                break;
            case SM.BLL.Common.PageModule.TwoColumn:
                ctrlSrc = "~/_em/content/_ctrl/_pmodTwoColumn.ascx";
                break;
            case SM.BLL.Common.PageModule.ThreeColumn:
                ctrlSrc = "~/_em/content/_ctrl/_pmodThreeColumn.ascx";
                break;
            case SM.BLL.Common.PageModule.TwoColumnExL :
                ctrlSrc = "~/_em/content/_ctrl/_pmodTwoColumnExL.ascx";
                break;
            case SM.BLL.Common.PageModule.ThreeColumnExL:
                ctrlSrc = "~/_em/content/_ctrl/_pmodThreeColumnExL.ascx";
                break;

            case SM.BLL.Common.PageModule.Col2_50_50:
                ctrlSrc = "~/_em/content/_ctrl/_pmod_2col_50_50.ascx";
                break;
            case SM.BLL.Common.PageModule.Col2_33_66:
                ctrlSrc = "~/_em/content/_ctrl/_pmod_2col_33_66.ascx";
                break;
            case SM.BLL.Common.PageModule.Comb_3_1_3_1:
                ctrlSrc = "~/_em/content/_ctrl/_pmod_comb_3_1_3_1_.ascx";
                break;

            default:
                PAGE_MODULE pm = PAGE_MODULE.GetPageModuleByByID(PageModule);
                if (pm == null)
                    ctrlSrc = "~/_em/content/_ctrl/_pmodOneColumn.ascx";
                else
                    ctrlSrc = pm.WWW_URL;

                break;
        }


        // load content control
        UserControl uc = (UserControl)this.LoadControl(ctrlSrc);
        if (uc == null) return;

        uc.ID = "objContent";

        // add webpart to zone
        //zone.Controls.Add(uc);


        phContent.Controls.Add(uc);


    }
}
