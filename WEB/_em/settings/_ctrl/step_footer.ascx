﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="step_footer.ascx.cs" Inherits="_em_settings__ctrl_step_footer" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>



<h1 style="padding-bottom:10px; ">
<asp:Literal ID="litFootData" runat="server" Text = "Vnesi podatke za nogo spletne predstavitve"></asp:Literal>
<a href="javascript:void(0)" class="btsettStatusHelp"><img src='<%=Page.ResolveUrl("~/_inc/images/icon/help.png") %>' /></a>
<div class="settStatusWrapp" >
    <div class="settStatus">
        Vpiši svoje podatke in obkljukaj katere od teh želiš imeti objavljene na svoji spletni strani. Ti podatki so objavljeni v "nogi" tvoje spletne strani, levo spodaj.
        <br />
        Ko boš končal pritisni gumb "Shrani spremembe".
        <br /><br />
        Podatki se lahko razlikujejo od podatkov podjetja kamor se pošlje račun za spletno stran.

<%--        <ul>
            <li>
                Vpiši svoje podatke in obkljukaj katere od teh želiš imeti objavljene na svoji spletni strani. Ti podatki so objavljeni v "nogi" tvoje spletne strani, levo spodaj.
            </li>
            <li>
                Podatki se lahko razlikujejo od podatkov podjetja kamor se pošlje račun za spletno stran.
            </li>
        </ul>
--%>    
    </div>
</div>
</h1>
<table class="wizzFootTABLE">
    <tr>
        <td class="td1"><asp:Literal ID="litName" runat="server" Text="Naziv:"></asp:Literal></td>
        <td class="td2"><asp:TextBox ID="tbName" TextMode="MultiLine" Rows="2" CssClass="wizzNav_TBX" runat="server"></asp:TextBox></td>
        <td class="td3">
            <asp:CheckBox ID="cbName" CssClass="wizzFootCbx" runat="server" Text="&nbsp;&nbsp;&nbsp;Prikaži na strani" Enabled="false" Checked="true" />
            <cc1:ToggleButtonExtender ID="ToggleName" runat="server"
                TargetControlID="cbName" 
                ImageWidth="19" 
                ImageHeight="19"
                CheckedImageAlternateText="Odkljukaj"
                UncheckedImageAlternateText="Obkljukaj"
                UncheckedImageUrl="~/_inc/images/design/checkbox_off.gif" 
                CheckedImageUrl="~/_inc/images/design/checkbox_on.gif"
                DisabledCheckedImageUrl="~/_inc/images/design/checkbox_disabled.gif"
                DisabledUncheckedImageUrl="~/_inc/images/design/checkbox_disabled.gif" />
        </td>
        <td><asp:RequiredFieldValidator ValidationGroup="footer" ID="rfvName" ControlToValidate="tbName" runat="server" ErrorMessage="* Vpiši naziv"></asp:RequiredFieldValidator></td>
    </tr>
    <tr>
        <td class="td1">
            <h2 style="padding-top:15px; padding-bottom:10px; "><asp:Literal ID="Literal1" runat="server" Text = "Naslov"></asp:Literal></h2>
        </td>
        <td class="td2"> </td>
        <td class="td3">
            <asp:CheckBox ID="cbAddress" CssClass="wizzFootCbx" runat="server" Text="&nbsp;&nbsp;&nbsp;Prikaži na strani" />
            <cc1:ToggleButtonExtender ID="ToggleAddress" runat="server"
                TargetControlID="cbAddress" 
                ImageWidth="19" 
                ImageHeight="19"
                CheckedImageAlternateText="Odkljukaj"
                UncheckedImageAlternateText="Obkljukaj"
                UncheckedImageUrl="~/_inc/images/design/checkbox_off.gif" 
                CheckedImageUrl="~/_inc/images/design/checkbox_on.gif"
                DisabledCheckedImageUrl="~/_inc/images/design/checkbox_disabled.gif"
                DisabledUncheckedImageUrl="~/_inc/images/design/checkbox_disabled.gif" />
        </td>   
    
    </tr>
    <tr>
        <td class="td1"><asp:Literal ID="litAddress" runat="server" Text = "Ulica:"></asp:Literal></td>
        <td class="td2">
            <asp:TextBox ID="tbStreet" CssClass="wizzNav_TBX" runat="server"></asp:TextBox>
            <cc1:TextBoxWatermarkExtender  WatermarkCssClass="modPopup_mainWatermark"  ID="tbweStreet" runat="server" TargetControlID="tbStreet" WatermarkText="Tukaj vpiši ulico" ></cc1:TextBoxWatermarkExtender>
        </td>
        <td class="td3">
            
        </td>
        <td><asp:RequiredFieldValidator ValidationGroup="footer" ID="rfvStreet" ControlToValidate="tbStreet" runat="server" ErrorMessage="* Vpiši naslov"></asp:RequiredFieldValidator></td>
    </tr>
    <tr>
        <td class="td1"><asp:Literal ID="litPostCode" runat="server" Text = "Poštna številka:"></asp:Literal></td>
        <td class="td2">
            <asp:TextBox ID="tbPostalCode" CssClass="wizzNav_TBX" runat="server"></asp:TextBox>
            <cc1:TextBoxWatermarkExtender  WatermarkCssClass="modPopup_mainWatermark"  ID="tbwePostalCode" runat="server" TargetControlID="tbPostalCode" WatermarkText="Tukaj vpiši pošto" ></cc1:TextBoxWatermarkExtender>
        </td>
        <td class="td3"></td>
        <td></td>
    </tr>
    <tr>
        <td class="td1"><asp:Literal ID="litCity" runat="server" Text = "Mesto:"></asp:Literal></td>
        <td class="td2">
            <asp:TextBox ID="tbCity" CssClass="wizzNav_TBX" runat="server"></asp:TextBox>
            <cc1:TextBoxWatermarkExtender WatermarkCssClass="modPopup_mainWatermark"   ID="tbweCity" runat="server" TargetControlID="tbCity" WatermarkText="Tukaj vpiši mesto" ></cc1:TextBoxWatermarkExtender>
        </td>
        <td class="td3"></td>
        <td></td>
    </tr>
</table>    


<h2 style="padding-top:15px; padding-bottom:10px; "><asp:Literal ID="litContact" runat="server" Text = "Kontaktni podatki"></asp:Literal></h2>

    
<table class="wizzFootTABLE">
    <tr>
        <td class="td1"><asp:Literal ID="litConEmail" runat="server" Text = "Email:"></asp:Literal></td>
        <td class="td2"><asp:TextBox ID="tbConEmail" CssClass="wizzNav_TBX" runat="server"></asp:TextBox></td>
        <td class="td3">
            <asp:CheckBox ID="cbEmail" CssClass="wizzFootCbx" runat="server" Text="&nbsp;&nbsp;&nbsp;Prikaži na strani" />
            <cc1:ToggleButtonExtender ID="ToggleEmail" runat="server"
                TargetControlID="cbEmail" 
                ImageWidth="19" 
                ImageHeight="19"
                CheckedImageAlternateText="Odkljukaj"
                UncheckedImageAlternateText="Obkljukaj"
                UncheckedImageUrl="~/_inc/images/design/checkbox_off.gif" 
                CheckedImageUrl="~/_inc/images/design/checkbox_on.gif"
                DisabledCheckedImageUrl="~/_inc/images/design/checkbox_disabled.gif"
                DisabledUncheckedImageUrl="~/_inc/images/design/checkbox_disabled.gif" />
        </td>
        <td><asp:RegularExpressionValidator  ControlToValidate="tbConEmail"   ID="revEmail" runat="server"     ErrorMessage="* Vpiši veljaven email"     ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator></td>
    </tr>
    <tr>
        <td class="td1"><asp:Literal ID="litConPhone" runat="server" Text = "Telefon:"></asp:Literal></td>
        <td class="td2"><asp:TextBox ID="tbConPhone" CssClass="wizzNav_TBX" runat="server"></asp:TextBox></td>
        <td class="td3">
            <asp:CheckBox ID="cbPhone" CssClass="wizzFootCbx" runat="server" Text="&nbsp;&nbsp;&nbsp;Prikaži na strani" />
            <cc1:ToggleButtonExtender ID="TogglePhone" runat="server"
                TargetControlID="cbPhone" 
                ImageWidth="19" 
                ImageHeight="19"
                CheckedImageAlternateText="Odkljukaj"
                UncheckedImageAlternateText="Obkljukaj"
                UncheckedImageUrl="~/_inc/images/design/checkbox_off.gif" 
                CheckedImageUrl="~/_inc/images/design/checkbox_on.gif"
                DisabledCheckedImageUrl="~/_inc/images/design/checkbox_disabled.gif"
                DisabledUncheckedImageUrl="~/_inc/images/design/checkbox_disabled.gif" />
        </td>
        <td></td>
    </tr> 
    <tr>
        <td class="td1"><asp:Literal ID="litConMobile" runat="server" Text = "GSM:"></asp:Literal></td>
        <td class="td2"><asp:TextBox ID="tbConMobile" CssClass="wizzNav_TBX" runat="server"></asp:TextBox></td>
        <td class="td3">
            <asp:CheckBox ID="cbMobile" CssClass="wizzFootCbx" runat="server" Text="&nbsp;&nbsp;&nbsp;Prikaži na strani" />
            <cc1:ToggleButtonExtender ID="ToggleMobile" runat="server"
                TargetControlID="cbMobile" 
                ImageWidth="19" 
                ImageHeight="19"
                CheckedImageAlternateText="Odkljukaj"
                UncheckedImageAlternateText="Obkljukaj"
                UncheckedImageUrl="~/_inc/images/design/checkbox_off.gif" 
                CheckedImageUrl="~/_inc/images/design/checkbox_on.gif"
                DisabledCheckedImageUrl="~/_inc/images/design/checkbox_disabled.gif"
                DisabledUncheckedImageUrl="~/_inc/images/design/checkbox_disabled.gif" />
        </td>
        <td></td>
    </tr>    
    <tr>
        <td class="td1"><asp:Literal ID="litConFax" runat="server" Text = "Fax:"></asp:Literal></td>
        <td class="td2"><asp:TextBox ID="tbConFax" CssClass="wizzNav_TBX" runat="server"></asp:TextBox></td>
        <td class="td3">
            <asp:CheckBox ID="cbFax" CssClass="wizzFootCbx" runat="server" Text="&nbsp;&nbsp;&nbsp;Prikaži na strani" />
            <cc1:ToggleButtonExtender ID="ToggleFax" runat="server"
                TargetControlID="cbFax" 
                ImageWidth="19" 
                ImageHeight="19"
                CheckedImageAlternateText="Odkljukaj"
                UncheckedImageAlternateText="Obkljukaj"
                UncheckedImageUrl="~/_inc/images/design/checkbox_off.gif" 
                CheckedImageUrl="~/_inc/images/design/checkbox_on.gif"
                DisabledCheckedImageUrl="~/_inc/images/design/checkbox_disabled.gif"
                DisabledUncheckedImageUrl="~/_inc/images/design/checkbox_disabled.gif" />
        </td>
        <td></td>
    </tr>    
</table>    
<br />
