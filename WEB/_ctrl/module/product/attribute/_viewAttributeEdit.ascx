﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewAttributeEdit.ascx.cs" Inherits="_ctrl_module_product_attribute_viewAttributeEdit" %>

<script language = "javascript" type="text/javascript">

    function editProductAttributeSave(id, grid) {
        var _items = new Array();
        $("#prodAttributes input:checkbox:checked").not('#cbAttrSelAll').each(function(i, val) {
            Array.add(_items, $(this).val());
            //alert(_lan + ' , ' +_desc);
        });

        openModalLoading('Shranjujem...');
        WebServiceCWP.SaveProductAttributes(id, grid, _items, em_g_lang, onProductEditSuccessSave, onProductSaveError, id);

    }
    $(document).ready(function() {
        $('#cbAttrSelAll').click(function() {
            $("#prodAttributes :checkbox").attr('checked', $(this).is(':checked'));
        });

    });
       
</script>



<div class="modPopup_header modPopup_headerEdit">
    <h4>UREDI DODATNE ATRIBUTE</h4>
    <p>Uredi dodatne atribute za ta artikel</p>
</div>
<div class="modPopup_main">
    
    <span class="modPopup_mainTitle"><%= GroupAttribute.Title  %></span> 
    
    
    <asp:ListView ID="lvList" runat="server" >
        <LayoutTemplate>
            <div id="prodAttributes">
                <label>
                    <input id="cbAttrSelAll" type="checkbox"  />
                    &nbsp;Označi vse/nobenega
                </label>
                <br />
                <asp:PlaceHolder ID="itemPlaceholder" runat="server"/>
            </div>
        </LayoutTemplate>
        
        <ItemTemplate>
            <label>
                <input <%# RenderChecked(Container.DataItem) %> type="checkbox" value='<%# Eval("AttributeID") %>' />
                &nbsp;<%# Eval("Title")%>
            </label>
            <br />
            
        </ItemTemplate>
    </asp:ListView>    
    
    

</div>   

<div class="modPopup_footer"> 
    <div class="buttonwrapperModal">
        <a class="lbutton lbuttonConfirm" href="#" onclick="editProductAttributeSave('<%= ProductID %>', '<%= GroupAttribute.GroupAttID %>'); return false"><span>Shrani</span></a>
        <a class="lbutton lbuttonDelete" href="#" onclick="closeModalLoader(); return false" ><span>Prekliči</span></a>
    </div>
</div>
