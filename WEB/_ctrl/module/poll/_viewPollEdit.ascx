﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewPollEdit.ascx.cs" Inherits="_ctrl_module_poll_cntPollEdit" %>

<script language = "javascript" type="text/javascript">
    function editPollSave() {
        var q = $("#<%= tbQuestion.ClientID %>").val();
        //alert('q:' + q + ';;lang:' + em_g_lang + ';;cwp:: ' + cwpID + ';; pollID::' + pollID);
        var _items = new Object();
        $('#<%= ViewID.ToString() %>').find("#tbChoices tbody :text").each(function(i, val) {
            var _chid = '' + $(this).parent().parent()[0].id;
            if (_chid == null || _chid == '')
                _chid = 'n_' + i;
            _items[_chid] = '' + this.value;
        });
        openModalLoading('Shranjujem...');
        if (cwpID == null)
            WebServiceCWP.SavePoll(pollID, cwpID, em_g_lang, $("#<%= cbActivePoll.ClientID %>").attr('checked'), q, _items, onPollSaveSuccess, onPollSaveError);
        else
            WebServiceCWP.SavePoll(pollID, cwpID, em_g_lang, $("#<%= cbActivePoll.ClientID %>").attr('checked'), q, _items, onPollAddSuccessSave, onPollSaveError);
                
    }

    function addNewChoice() {
        var tr = "<tr ><td width='100' >Možnost " + ($('#<%= ViewID.ToString() %>').find("#tbChoices tbody :text").length + 1) + "</td><td><input class='modPopup_TBXsettings' type='text'> <a href='#' onclick='removeChoice(this, false); return false;'>Zbriši</a></td></tr>";
        $('#<%= ViewID.ToString() %>').find("#tbChoices tbody").append(tr);

    }
    function removeChoice(a, c) {
        if (c)
            if (!confirm('Ali res želite zbrisati to možnost?'))
                return; 
        $(a).parent().parent().remove();
    }
    
</script>



<div  style="">

    <div class="modPopup_header modPopup_headerEdit">
        <h4>DODAJ/UREDI ANKETO</h4>
        <p>Uredi anketo</p>
    </div>
    <div id='<%= ViewID.ToString() %>' class="modPopup_main">
        
        <span class="modPopup_mainTitle">Anketno vprašanje:</span> 
        <input class="modPopup_TBXsettings" style="width:95%" id="tbQuestion" maxlength="256" runat="server" type="text" />
        <br />
        <label  for="<%= cbActivePoll.ClientID %>"> <input  id="cbActivePoll" runat="server" name ="cbActivePoll" type="checkbox" />&nbsp;&nbsp;Anketa naj bo vidna obiskovalcem strani</label>

        <span class="modPopup_mainTitle">Seznam možnih odgovorov</span>

        <table id="tbChoices" width="95%" cellpadding="0" cellspacing="0" >
            <tbody>
        <asp:ListView ID="lvList" runat="server">
            <LayoutTemplate>
                
                    <asp:PlaceHolder ID="itemPlaceholder" runat="server"/>
            </LayoutTemplate>

            <ItemTemplate>
                <tr id="<%# Eval("PollChoiceID").ToString() == Guid.Empty.ToString() ? "n_" + Container.DataItemIndex.ToString() : Eval("PollChoiceID") %>">
                    <td width="100">
                        Možnost <%# Container.DataItemIndex + 1 %>
                    </td>
                    <td >
                        <input class="modPopup_TBXsettings" maxlength="256" type="text" value="<%# Eval("Choice") %>" />
                        <a onclick="removeChoice(this, true); return false;" href="#"  class="poll_AHREF">Zbriši</a>
                    </td>
                </tr>
                
            </ItemTemplate>
        </asp:ListView>
            </tbody>
        </table>
        

        <a href="#" onclick="addNewChoice();return false" class="poll_AHREF">Dodaj novo možnost</a>
        

    </div>   

    <div class="modPopup_footer"> 
        <div class="buttonwrapperModal">
            <a class="lbutton lbuttonConfirm" href="#" onclick="editPollSave(); return false"><span>Shrani</span></a>
            <a class="lbutton lbuttonDelete" href="#" onclick="closeModalLoader(); return false" ><span>Prekliči</span></a>
            <asp:PlaceHolder ID="phAdd" runat="server">
                <a class="lbutton lbuttonConfirm" href="#" onclick="addPollLoad(cwpID); return false"><span>Prikaži obstoječo anketo</span></a>
            </asp:PlaceHolder>
        </div>
    </div>
</div>






