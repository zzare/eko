﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SM.EM.BLL;
using SM.EM.UI.Controls;

public partial class _ctrl_module_product_viewArticleList2Paged : BaseControlView
{

    protected CustomPager Pager;

    public object Data;
    public object[] Parameters;
    public string LangID;

    public int CurrentSitemapID{get; set;}
    public EM_USER  em_user { get; set; }
    public Guid CwpID { get; set; }
    public CWP_ARTICLE  CwpArticle{ get; set; }



    public bool ShowFooter { get; set; }
    public int? ImgTypeID { get; set; }

    public bool EnablePager { get; set; }


    #region Property


    //private string containerCssClass = "contArticles";
    //public string ContainerCssClass
    //{
    //    get { return this.containerCssClass; }
    //    set { this.containerCssClass = value; }
    //}
    private string appendToTitle = "&nbsp;&raquo;";
    public string AppendToTitle
    {
        get { return this.appendToTitle; }
        set { this.appendToTitle = value; }
    }

    private bool showImage = false;
    public bool ShowImage
    {
        get { return this.showImage; }
        set { this.showImage = value; }
    }
    private bool showDesc = true;
    public bool ShowDesc
    {
        get { return this.showDesc; }
        set { this.showDesc = value; }
    }
    private int titleLength = -1;
    public int TitleLength
    {
        get { return this.titleLength; }
        set { this.titleLength = value; }
    }
    private int descLength = -1;
    public int DescLength
    {
        get { return this.descLength; }
        set { this.descLength = value; }
    }

    private string articleDetailsPath = "~/c/ArticleDetails.aspx";
    public string ArticleDetailsPath
    {
        get { return this.articleDetailsPath; }
        set { this.articleDetailsPath = value; }
    }
    //private bool isEditMode = false;
    //public bool IsEditMode
    //{
    //    get { return this.isEditMode; }
    //    set { this.isEditMode = value; }
    //}
    //private bool autoLoad = true;
    //public bool AutoLoad
    //{
    //    get { return this.autoLoad; }
    //    set { this.autoLoad = value; }
    //}

    public int PageNum    {        get ;        set;    }
    public int PageSize    {        get ;        set;    }


    #endregion


    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);


        PageSize = 10;

        // init pager
        Pager = new CustomPager();
        //Pager.OrderColumns = "";
        
        //Pager.SelectedClass = "pagesel";
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        //phPager.Visible = false;

        BindData();
    }



    public void BindData()
    {

        if (Data == null)
            return;

        //if (lvList.Items.Count > 0)
        //    return;

        if (Parameters != null)
        {
            if (Parameters.Length > 0 && Parameters[0] != null)
                CwpID  = (Guid )Parameters[0];
            if (Parameters.Length > 1 && Parameters[1] != null)
                em_user = (EM_USER)Parameters[1];
            if (Parameters.Length > 2 && Parameters[2] != null)
                LangID = Parameters[2].ToString();
            if (Parameters.Length > 3 && Parameters[3] != null)
                CurrentSitemapID = (int)(Parameters[3]);
            if (Parameters.Length > 4 && Parameters[4] != null)
                IsModeCMS = bool.Parse(Parameters[4].ToString());
            if (Parameters.Length > 5 && Parameters[5] != null)
                CwpArticle  = (CWP_ARTICLE ) Parameters[5];
        }

        IQueryable<Data.ArticleData> list = (IQueryable<Data.ArticleData>)Data;

        // set pager TOTAL COUNT
        //Pager.TotalCount = list.Count();
        //Pager.PageSize = PageSize;


        // list
        if (Pager.GetCurrentPage() == 1) {
            objArticleList2.CwpID = CwpID;
            objArticleList2.IsModeCMS = IsModeCMS;
            objArticleList2.PageNum = 1;
            objArticleList2.PageSize = PageSize ;
            objArticleList2.EnablePager  = EnablePager ;
            objArticleList2.CurrentSitemapID = CurrentSitemapID;
            objArticleList2.em_user = em_user;

            objArticleList2.ShowDesc = showDesc ;
            objArticleList2.ShowImage = showImage ;
            objArticleList2.ImgTypeID = ImgTypeID ;

            objArticleList2.Data = list;//.Skip(Pager.PageSize * (Pager.GetCurrentPage() - 1)).Take(Pager.PageSize);
            objArticleList2.BindData();



        
        }
        else if (Pager.GetCurrentPage() >= 1)
        {
            objArticleList.CwpID = CwpID;
            objArticleList.IsModeCMS = IsModeCMS;
            objArticleList.PageNum = 1;
            objArticleList.PageSize = PageSize;
            objArticleList.EnablePager = EnablePager;

            objArticleList.CurrentSitemapID = CurrentSitemapID;
            objArticleList.em_user = em_user;

            objArticleList.ShowDesc = showDesc;
            objArticleList.ShowImage = showImage;
            objArticleList.ImgTypeID = ImgTypeID;


            objArticleList.Data = list;//.Skip(Pager.PageSize * (Pager.GetCurrentPage() - 1)).Take(Pager.PageSize);
            objArticleList.BindData();
        
        }

        //if (Pager.TotalPages > 1)
        //    phPager.Visible = true;



        // set relcanonical (if default SORT and DIRECTION and not first page)
        if (Pager.GetCurrentOrder() == 1 && Pager.GetCurrentDirection() == Pager.GetDefaultDirectionByID(1) && Pager.GetCurrentPage() > 1)
        {
            if (Page is SM.EM.UI.BasePage_EMENIK)
            {
                SM.EM.UI.BasePage_EMENIK pg = (SM.EM.UI.BasePage_EMENIK)Page;
                pg.RelCanonical = Pager.RenderSortPageFullUrl(Pager.GetCurrentOrder(), Pager.GetCurrentPage(), false, "");
            }

            
        }


        //lvList.DataSource = Data; 
        //lvList.DataBind();

        //if (lvList.Items.Count <= 0)
        //{
        //    divArticles.Visible = false;
        //    phEmpty.Visible = true;
        //}

    }





}
