﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
namespace SM.UI.CMS
{
    public partial class CMS_backend_user_Manage_customer : SM.EM.UI.BasePage_USER_EMENIK 
    {
        protected Guid ProjectID { 
            get {
                if (Request.QueryString["p"] == null)
                    return Guid.Empty;
                if (!SM.EM.Helpers.IsGUID(Request.QueryString["p"]))
                    return Guid.Empty;

                return new Guid(Request.QueryString["p"]);
            } 
        }
        protected Guid SiteID
        {
            get
            {
                if (Request.QueryString["s"] == null)
                    return Guid.Empty;
                if (!SM.EM.Helpers.IsGUID(Request.QueryString["s"]))
                    return Guid.Empty;

                return new Guid(Request.QueryString["s"]);
            }
        }
        protected Guid GroupID
        {
            get
            {
                if (Request.QueryString["g"] == null)
                    return Guid.Empty;
                if (!SM.EM.Helpers.IsGUID(Request.QueryString["g"]))
                    return Guid.Empty;

                return new Guid(Request.QueryString["g"]);
            }
        }
        protected Guid UserID
        {
            get
            {
                if (Request.QueryString["u"] == null)
                    return Guid.Empty;
                if (!SM.EM.Helpers.IsGUID(Request.QueryString["u"]))
                    return Guid.Empty;

                return new Guid(Request.QueryString["u"]);
            }
        }

        protected bool IsAdmin { get {

            return true; // todo: remove
            if (User.Identity.IsAuthenticated)
                if (User.IsInRole("admin"))
                    return true;
            return false;
            }
        }



        protected override void OnInit(EventArgs e)
        {
            CurrentSitemapID = 1447;

            base.OnInit(e);

        }

        protected Guid PageId { get { return SM.BLL.Custom.MainUserID; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            // event handlers
            objUserEdit.OnUserChanged += new EventHandler(objUserEdit_OnUserChanged);
            objUserEdit.OnCancel += new EventHandler(objUserEdit_OnCancel);
            objFilter.OnFilterChanged += new EventHandler(objFilter_OnFilterChanged);
            objUserList.OnDelete += new SM.UI.Controls._customer_cntUserList .OnEditEventHandler(objUserList_OnDelete);
            objUserList.OnEdit += new SM.UI.Controls._customer_cntUserList.OnEditEventHandler(objUserList_OnEdit);
//            objPaymentEdit.OnChanged += new EventHandler(objPaymentEdit_OnChanged);
            
            // init
            objUserList.IsAdmin = IsAdmin;
            objUserEdit.PageID = PageId;
            objUserList.PageID = PageId;
            objOrderList.PageID = PageId;
            objUserList.PageSize = 40;


            if (UserID != Guid.Empty)
            {
                //EM_USER em_user = EM_USER.GetUserByID(UserID);
                //if (em_user != null)
                //    objUserSettings.em_user = em_user;
            }


            ApplyFilters();

            if (!IsPostBack)
            {
                // if user is set as QS, show edit user
                if (UserID != Guid.Empty)
                {
                    LoadData();

                }

                    else // normal load
                {
                    HideTabs();
                    cpeQList.Collapsed = false;
                    objUserList .BindData();
                }

            }
        }


        protected void LoadData() {

            objUserEdit.UID = UserID;
            objUserEdit.BindData();
            cpeQList.Collapsed = true;
            cpeQList.ClientState = "true";
            objOrderList.UserId = objUserEdit.UID;
            objUserList.BindData();

            objOrderList.BindData();


            //objPaymentEdit.UserID = objUserEdit.UID;
            //objPaymentEdit.BindData();

            //// load data
            //objUserSettings.LoadData();


            ShowTabs();
        }

        void objUserEdit_OnCancel(object sender, EventArgs e)
        {
            HideTabs();

            cpeQList.Collapsed = false;
            cpeQList.ClientState = "false";
            
        }


        protected void ApplyFilters() {
            // list
            objUserList.SearchText = objFilter.SearchText;
            objUserList.Active = objFilter.Active;
            objOrderList.Active = true;

            objUserEdit .ProjectID = ProjectID;
            objUserEdit .SiteID  = SiteID;

            objOrderList.UserId = objUserEdit.UID;
            //objPaymentEdit.UserID = objUserEdit.UID;


        
        }
        // filter
        void objFilter_OnFilterChanged(object sender, EventArgs e)
        {
            // filters
            ApplyFilters();

            // refresh question lists
            objUserList.BindData();

            objOrderList.BindData();

        }
        //edit
        void objUserEdit_OnUserChanged(object sender, EventArgs e)
        {
            // refresh user lists
            ApplyFilters();

            // refresh user lists
            objUserList.BindData();

            // refresh edit
            objUserEdit.BindData();


        }

        //list
        void objUserList_OnEdit(object sender, SM.BLL.Common.Args.IDguidEventArgs e)
        {
            // show tabs
            ShowTabs();


            // apply filters
            ApplyFilters();


            // load selected question
            //objSiteEdit.SID = e.ClickedID;
            //objSiteEdit.BindData();

        }
        void objUserList_OnDelete(object sender, SM.BLL.Common.Args.IDguidEventArgs e)
        {
            ApplyFilters();

            // refresh group lists
            objUserList.BindData();

        }





        protected void ShowTabs()
        {
            tabUser.Visible = true;
            tabUser.ActiveTabIndex = 0;

        }
        protected void HideTabs()
        {
            tabUser.Visible = false;

        }


    }
}