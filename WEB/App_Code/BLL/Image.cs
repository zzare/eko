﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;

    /// <summary>
    /// Summary description for Menu
    /// </summary>
    public partial class IMAGE
    {

        public static IMAGE_ORIG GetImageByID(int imgID)
        {

            eMenikDataContext db = new eMenikDataContext();

            return (from r in db.IMAGE_ORIGs where r.IMG_ID == imgID select r).SingleOrDefault();

        }
        public static IMAGE_ORIG GetImageByID(int imgID, string username)
        {

            eMenikDataContext db = new eMenikDataContext();

            return (from r in db.IMAGE_ORIGs where r.IMG_ID == imgID && r.USERNAME == username  select r).SingleOrDefault();

        }

        public static IMAGE GetImageByID(int imgID, int typeID)
        {
            eMenikDataContext db = new eMenikDataContext();

            return (from r in db.IMAGEs where r.IMG_ID == imgID && r.TYPE_ID == typeID  select r).SingleOrDefault();
        }
        public static vw_Image  GetvwImageByID(int imgID, int typeID)
        {
            eMenikDataContext db = new eMenikDataContext();

            return (from r in db.vw_Images where r.IMG_ID == imgID && r.TYPE_ID == typeID select r).SingleOrDefault();
        }
        public static vw_Image GetvwImageByID(int imgID, int typeID, string username)
        {
            eMenikDataContext db = new eMenikDataContext();

            return (from r in db.vw_Images where r.IMG_ID == imgID && r.TYPE_ID == typeID && r.USERNAME == username  select r).SingleOrDefault();
        }
        public static List<IMAGE> GetImagesByID(int imgID)
        {
            eMenikDataContext db = new eMenikDataContext();

            return (from r in db.IMAGEs where r.IMG_ID == imgID select r).ToList();
        }

        public static List<IMAGE_TYPE > GetImageTypesByImage(int imgID)
        {
            eMenikDataContext db = new eMenikDataContext();

            return (from r in db.IMAGE_TYPEs join i in db.IMAGEs on r.TYPE_ID equals i.TYPE_ID  where i.IMG_ID == imgID select r).ToList();
        }

        public static List<vw_ImageDesc> GetImageDescsByID(int imgID)
        {
            eMenikDataContext db = new eMenikDataContext();

            return (from l in db.LANGUAGEs join  i in (from i in db.IMAGE_LANGs where i.IMG_ID ==  imgID select i )  on  l.LANG_ID equals i.LANG_ID    into outer from img in outer.DefaultIfEmpty() orderby l.LANG_ORDER    select new vw_ImageDesc { IMG_DESC = img.IMG_DESC, IMG_ID = img.IMG_ID, IMG_TITLE=img.IMG_TITLE, LANG_DESC=l.LANG_DESC, LANG_ID = l.LANG_ID   }).ToList();
        }

        public static List<vw_ImageDesc> GetImageDescsByIDEmenik(int imgID, string username)
        {
            eMenikDataContext db = new eMenikDataContext();

            return (from em in db.EM_USERs
                    from uc in db.EM_USER_CULTUREs
                    from c in db.CULTUREs
                    join i in
                        (from i in db.IMAGE_LANGs where i.IMG_ID  == imgID select i) on c.LANG_ID equals i.LANG_ID into outer
                    from img in outer.DefaultIfEmpty()
                    from l in db.LANGUAGEs
                    where em.USERNAME == username && em.UserId == uc.UserId && c.LCID == uc.LCID && l.LANG_ID == c.LANG_ID
                    orderby uc.DEFAULT descending , l.LANG_ORDER
                    select new vw_ImageDesc { IMG_DESC = img.IMG_DESC , IMG_ID  = img.IMG_ID, IMG_TITLE = img.IMG_TITLE, LANG_DESC = l.LANG_DESC, LANG_ID = l.LANG_ID }).ToList();
        }

        public static void SaveImageDesc(int imgID, string langID, string title, string desc) {
            eMenikDataContext db = new eMenikDataContext();

            IMAGE_LANG img = (from i in db.IMAGE_LANGs where i.IMG_ID == imgID && i.LANG_ID == langID select i).SingleOrDefault() ;
            if (img == null)
            {
                img = new IMAGE_LANG { IMG_ID = imgID, LANG_ID = langID, IMG_TITLE = title, IMG_DESC = desc };
                db.IMAGE_LANGs.InsertOnSubmit(img);
            }
            else
            {
                img.IMG_TITLE = title;
                img.IMG_DESC = desc;
            }

            db.SubmitChanges();
        }
        public static IMAGE_ORIG UpdateImage(int imgID, string author, bool active, string username)
        {
            eMenikDataContext db = new eMenikDataContext();

            IMAGE_ORIG o = (from im in db.IMAGE_ORIGs where im.IMG_ID == imgID && im.USERNAME == username  select im).SingleOrDefault();
            if (o == null)
                return o;
            o.IMG_ACTIVE = active ;
            o.IMG_AUTHOR = author ;
            db.SubmitChanges();

            return o;
        }

        public static void DeleteImageFromGallery(int imgID, int galID)
        {
            eMenikDataContext db = new eMenikDataContext();

            // delete image from gallery
            IMAGE_GALLERY gal = (from g in db.IMAGE_GALLERies where g.IMG_ID == imgID && g.GAL_ID == galID select g).SingleOrDefault();
            if (gal != null)
                db.IMAGE_GALLERies.DeleteOnSubmit(gal);

            db.SubmitChanges();

            // check if image can be deleted
            gal = (from g in db.IMAGE_GALLERies where g.IMG_ID == imgID select g).FirstOrDefault();
            if (gal == null)
                DeleteImage(imgID);

        }
        

        public static bool DeleteImage(int imgID)
        {
            eMenikDataContext db = new eMenikDataContext();

            try
            {
                // delete IMAGE_LANG
                List<IMAGE_LANG> lang = (from l in db.IMAGE_LANGs where l.IMG_ID == imgID select l).ToList();
                db.IMAGE_LANGs.DeleteAllOnSubmit(lang);
//                db.SubmitChanges();

                // delete images
                var img = (from i in db.IMAGEs where i.IMG_ID == imgID select i).ToList();
                db.IMAGEs.DeleteAllOnSubmit(img);
  //              db.SubmitChanges();
                // delete files from disk
                foreach (IMAGE i in img)
                {
                    string file = HttpContext.Current.Server.MapPath(i.IMG_URL);
                    if (System.IO.File.Exists(file))
                        System.IO.File.Delete(file);
                }

                // delete image from gallery
                var gal = db.IMAGE_GALLERies.Where(w => w.IMG_ID == imgID);
                db.IMAGE_GALLERies.DeleteAllOnSubmit(gal);
                db.SubmitChanges();


                // delete original
                IMAGE_ORIG imgO = (from i in db.IMAGE_ORIGs where i.IMG_ID == imgID select i).SingleOrDefault();
                db.IMAGE_ORIGs.DeleteOnSubmit(imgO);
                db.SubmitChanges();

                // delete original file
                string fileO = HttpContext.Current.Server.MapPath(imgO.IMG_URL);
                if (System.IO.File.Exists(fileO))
                    System.IO.File.Delete(fileO);                      

                return true;
            }
            catch (Exception ex)
            {
                ERROR_LOG.LogError(ex, "DeleteImage"); 
                return false;
            }
        }

        public static void UpdateOrder(int galID, int imgID, int order)
        {
            eMenikDataContext db = new eMenikDataContext();

            IMAGE_GALLERY ig = db.IMAGE_GALLERies.SingleOrDefault(w => w.GAL_ID == galID && w.IMG_ID == imgID);
            ig.IMG_ORDER  = order < 0 ? 0 : order;
            db.SubmitChanges();
        }


        public static class Common {
            public const int MAX_IMAGE_UPLOAD_SIZE = 4194304;// 4MB
            public const int MAX_IMAGE_ORIGINAL_SIZE = 2048;
            public const int MAX_IMAGE_ORIGINAL_HEIGHT = 1024;
            public const int MAX_IMAGE_ORIGINAL_WIDTH = 1024;
            public const string DEFAULT_IMAGE_FORMAT = ".jpg";

            public static string GetUserImageFolder() { 
                string usrFolder = "annonymous";
                if (HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    EM_USER em_user = EM_USER.GetFromContext();
                    if (em_user == null) em_user = EM_USER.Current.GetCurrentPage ();
                    if (em_user == null) em_user = EM_USER.GetUserByUserName(HttpContext.Current.User.Identity.Name);
                    if (em_user != null)
                    {
                        usrFolder = em_user.USERNAME;//+ "_" + em_user.UserId;
                    }
                }
                else {
                    EM_USER usr = EM_USER.GetUserByPageName(HttpContext.Current.Items["pname"].ToString());
                    if (usr != null)
                        usrFolder = usr.USERNAME;
                }

                return "~/userfiles/" + usrFolder + "/";
            
            }
            public static string GetUserImageFolder(Guid pageID)
            {
                string usrFolder = "annonymous";
                EM_USER em_user = EM_USER.GetUserByID(pageID);
                if (em_user != null)
                {
                    usrFolder = em_user.USERNAME;
                }

                return "~/userfiles/" + usrFolder + "/";

            }

            public static string GetNewImageName()
            {
                return GetNewImageName(".jpg");

            }

            public static string GetNewImageName(string suffix) {
                Guid id = Guid.NewGuid();
                return id.ToString() + suffix ;
            }

            public static bool IsFormatValid(string format) {
                format = format.ToLower();

                if (String.IsNullOrEmpty(format)) return false;

                if (!(format == ".jpg" || format== ".png" || format== ".gif")) return false;


                return true;
            }

            public static string GetFormat( IMAGE_TYPE type, string origFormat) {

                // if forced is valid, force format
                if (IsFormatValid(type.FILE_TYPE)) return type.FILE_TYPE;

                // return original format
                if (IsFormatValid(origFormat)) return origFormat;

                // force default format
                return DEFAULT_IMAGE_FORMAT;
                }
            

            public static ImageFormat GetImageFormatFromString(string format){
                format = format.ToLower();
			        switch (format) {
			        case ".bmp":
				        return ImageFormat.Bmp;
			        case ".emf":
				        return ImageFormat.Emf;
			        case ".exif":
				        return ImageFormat.Exif;
			        case ".gif":
				        return ImageFormat.Gif;
			        case ".icon":
				        return ImageFormat.Icon;
			        case ".jpeg":
				        return ImageFormat.Jpeg;
			        case ".memorybmp":
				        return ImageFormat.MemoryBmp;
			        case ".png":
				        return ImageFormat.Png;
			        case ".tiff":
				        return ImageFormat.Tiff;
			        case ".wmf":
				        return ImageFormat.Wmf;
			        }

                return ImageFormat.Jpeg;        
            }

        
        }

        // static utility functions
        public static class Func
        {


            /// <summary>
            /// Changes image url on image for specified type
            /// </summary>
            public static void ChangeImage(int imgID, int imgTypeID, string imageURL)
            {
                eMenikDataContext db = new eMenikDataContext();
                IMAGE img = db.IMAGEs.SingleOrDefault(w => w.IMG_ID == imgID && w.TYPE_ID == imgTypeID);
                img.IMG_URL = imageURL;
                db.SubmitChanges();
            }
            /// <summary>
            /// Changes image url on image for specified type
            /// </summary>
            public static void ChangeImageSecure(int imgID, int imgTypeID, string imageURL, string usr)
            {
                eMenikDataContext db = new eMenikDataContext();
                IMAGE img = (from i in db.IMAGEs
                             from o in db.IMAGE_ORIGs 
                             where i.IMG_ID == imgID && i.TYPE_ID == imgTypeID  && i.IMG_ID == o.IMG_ID  &&o.USERNAME == usr
                             select i).SingleOrDefault();
                    
                img.IMG_URL = imageURL;
                db.SubmitChanges();

            }

            /// <summary>
            /// Adds new image. original image and resized image to image_type
            /// </summary>
            public static IMAGE AddNewImage(System.Drawing.Image imgIn, int imgTypeID, bool deleteOld, string origFormat, string folder)
            {
                if (imgTypeID < 1) throw new System.Exception("Image type must be set !");
                IMAGE_TYPE type = IMAGE_TYPE.GetImageTypeByID(imgTypeID);
                return AddNewImage(imgIn, type, new IMAGE_ORIG { IMG_ACTIVE = true }, deleteOld, origFormat, folder );

            }

            public static IMAGE AddNewImage(System.Drawing.Image imgIn, IMAGE_TYPE imgType, IMAGE_ORIG imgData, bool deleteOld, string origFormat, string uploadFolder)
            {
                // resize original to maximum size
                int tmpWidth = imgType.ORIG_WIDTH;
                int tmpHeight = imgType.ORIG_HEIGHT;
                string tmpFormat = origFormat;
                if (tmpWidth < 1) tmpWidth = IMAGE.Common.MAX_IMAGE_ORIGINAL_WIDTH;
                 if (tmpHeight < 1) tmpHeight = IMAGE.Common.MAX_IMAGE_ORIGINAL_HEIGHT;

                // if format is forced, make new format
                tmpFormat = IMAGE.Common.GetFormat(imgType, origFormat);

                System.Drawing.Image imgOrig = Utils.ResizeImage(imgIn, tmpWidth, tmpHeight);
                // save original image to disk
                if (String.IsNullOrEmpty(imgData.IMG_NAME))
                    imgData.IMG_NAME = IMAGE.Common.GetNewImageName(tmpFormat);
                if (String.IsNullOrEmpty(imgData.IMG_FOLDER))
                {
                    if (!String.IsNullOrEmpty(uploadFolder))
                        imgData.IMG_FOLDER = uploadFolder;
                    else 
                        imgData.IMG_FOLDER = IMAGE.Common.GetUserImageFolder();
                }
                if (String.IsNullOrEmpty(imgData.IMG_URL))
                    imgData.IMG_URL = imgData.IMG_FOLDER + imgData.IMG_NAME;
                if (String.IsNullOrEmpty(imgData.USERNAME))
                    imgData.USERNAME = HttpContext.Current.User.Identity.Name.ToLower();
                imgData.DATE_ADDED = DateTime.Now;

                // if folder doesn't exist create one
                string folder = HttpContext.Current.Server.MapPath(imgData.IMG_FOLDER);
                if (!System.IO.Directory.Exists(folder))
                    System.IO.Directory.CreateDirectory(folder);

                // save image
                imgOrig.Save(HttpContext.Current.Server.MapPath(imgData.IMG_URL), IMAGE.Common.GetImageFormatFromString(tmpFormat));
                
                // get image size
                long  size = (new System.IO.FileInfo(HttpContext.Current.Server.MapPath(imgData.IMG_URL))).Length;
                imgData.SIZE = size;

                // save original image to db
                eMenikDataContext db = new eMenikDataContext();
                db.IMAGE_ORIGs.InsertOnSubmit(imgData);
                db.SubmitChanges();

                int imgID = imgData.IMG_ID;

                
                // dispose
                db.Dispose();


                // if type is not in mandatory types, add image for mandatory type
                foreach (int type in SM.BLL.Common.ImageType.MandatoryTypes)
                    if ( type != imgType.TYPE_ID)
                        SetNewImageForType(imgIn, imgID, type, deleteOld, tmpFormat);                    


                // save new image for type
                return SetNewImageForType(imgIn, imgID, imgType.TYPE_ID, deleteOld, tmpFormat);

            }

            /// <summary>
            /// Adds new image for image type, if it doesn't exist
            /// </summary>
            public static IMAGE AutoCreateImageForType(int imgID, int typeID, bool deleteOld)
            {
                IMAGE_ORIG imgOrig = IMAGE.GetImageByID(imgID);
                if (imgOrig == null) return null;
                IMAGE imgNew = IMAGE.GetImageByID(imgID, typeID);
                if (imgNew != null) return imgNew;

                // check if file exists
                if (!System.IO.File.Exists(HttpContext.Current.Server.MapPath(imgOrig.IMG_URL)))
                    return null;

                // auto create new image for image and type
                System.Drawing.Image imgIn = System.Drawing.Image.FromFile(HttpContext.Current.Server.MapPath(imgOrig.IMG_URL));

                if (imgIn == null) return null;

                return SetNewImageForType(imgIn, imgID, typeID, deleteOld, System.IO.Path.GetExtension(HttpContext.Current.Server.MapPath(imgOrig.IMG_URL)));
            }
            /// <summary>
            /// Adds new images for image type, for all images in gallery
            /// </summary>
            public static void AutoCreateImagesForGalleryForType(int galID, int typeID)
            {
                //// get all images in gallery
                //List<IMAGE_ORIG> origs = GALLERY.GetImageOrigsFromGallery(galID);
                IEnumerable<IMAGE_ORIG> origs = GALLERY.GetImageOrigsFromGalleryWithoutType(galID, typeID);
                foreach (IMAGE_ORIG img in origs)
                {
                    AutoCreateImageForType(img.IMG_ID, typeID, true);
                }

            }

            /// <summary>
            /// Adds new images for image type, for all images in all gallery on all CWPs with REF_ID
            /// </summary>
            public static void AutoCreateImagesForImageList(IQueryable <IMAGE_ORIG> imgList, int typeID)
            {
                //// get all images in gallery
                foreach (IMAGE_ORIG img in imgList)
                {
                    AutoCreateImageForType(img.IMG_ID, typeID, true);
                }

            }

            /// <summary>
            /// Adds new image for image type
            /// </summary>
            public static IMAGE SetNewImageForType(System.Drawing.Image imgIn, int imgID, int typeID, bool deleteOld, string format)
            {
                // smart resize of new image
                System.Drawing.Image imgNew = Utils.SmartResizeImage(imgIn, typeID);


                eMenikDataContext db = new eMenikDataContext();

                IMAGE img = db.IMAGEs.SingleOrDefault(w => w.IMG_ID == imgID && w.TYPE_ID == typeID);
                // if image doesn't exist, create it
                if (img == null)
                {
                    img = new IMAGE();
                    img.IMG_ID = imgID;
                    img.TYPE_ID = typeID;

                    db.IMAGEs.InsertOnSubmit(img);
                }
                else if (deleteOld ) { 
                    // delete old image from server
                    if (System.IO.File.Exists(HttpContext.Current.Server.MapPath(img.IMG_URL)))
                        System.IO.File.Delete(HttpContext.Current.Server.MapPath(img.IMG_URL));
                
                }
                img.IMG_NAME = IMAGE.Common.GetNewImageName(format);
                img.IMG_URL = IMAGE.Common.GetUserImageFolder() + img.IMG_NAME;
                img.DATE_ADDED = DateTime.Now;
                img.PUBLIC = true;

                string fileFolder = HttpContext.Current.Server.MapPath(IMAGE.Common.GetUserImageFolder());
                if (!System.IO.Directory.Exists(fileFolder))
                    System.IO.Directory.CreateDirectory(fileFolder);

                // save new image
                imgNew.Save( HttpContext.Current.Server.MapPath(img.IMG_URL), IMAGE.Common.GetImageFormatFromString(format));



                // save new image to db
                db.SubmitChanges();

                // dispose img
                imgNew.Dispose();
//                imgIn.Dispose();

                return img;            
            }


            public static  string CustomCropImage(int imgID, int typeID, int x1, int y1, int x2, int y2, string folder)
            { 
                // get original image
                IMAGE_ORIG imOrig = IMAGE.GetImageByID(imgID);
                System.Drawing.Image imageOrig = System.Drawing.Image.FromFile(HttpContext.Current.Server.MapPath(imOrig.IMG_URL ));

                // get image type
                IMAGE_TYPE imgType = IMAGE_TYPE.GetImageTypeByID(typeID);                

                // get user's croped image
                System.Drawing.Image imageUser = IMAGE.Utils.CustomCropImage(imageOrig, x1, y1, x2, y2);

                // repair image if necessary
                System.Drawing.Image imageNew = IMAGE.Utils.SmartResizeImage(imageUser, imgType.WIDTH, imgType.HEIGHT, imgType.W_MIN, imgType.W_MAX, imgType.H_MIN, imgType.H_MAX, imgType.SIZE_MAX, imgType.CROP_POSITION);


                // save new image
                if (string.IsNullOrEmpty(folder.Trim())) folder = Common.GetUserImageFolder();

                string format = System.IO.Path.GetExtension(HttpContext.Current.Server.MapPath(imOrig.IMG_URL));
                string imageUrl = folder + Common.GetNewImageName(format);

                string fileFolder = HttpContext.Current.Server.MapPath(folder);
                if (!System.IO.Directory.Exists(fileFolder))
                    System.IO.Directory.CreateDirectory(fileFolder);

                imageNew.Save(HttpContext.Current.Server.MapPath(imageUrl), IMAGE.Common.GetImageFormatFromString(format));

                //dispose
                imageNew.Dispose();
                imageOrig.Dispose();
                imageUser.Dispose();


                // return new image url
                return imageUrl;            
            }
        }


        // static utility functions
        public static class Utils
        {
            /// <summary>
            /// Crops current image with custom parameters
            /// </summary>
            public static  System.Drawing.Image CustomCropImage(System.Drawing.Image imgIn, int x1, int y1, int x2, int y2)
            {
                int width = (int)Math.Abs(x1-x2) ;
                int height = (int)Math.Abs(y1 - y2);


                PixelFormat Format = imgIn.PixelFormat;
                if (Format.ToString().Contains("Indexed"))
                    Format = PixelFormat.Format24bppRgb;

                Bitmap bmPhoto = new Bitmap(width, height, Format);
                bmPhoto.SetResolution(imgIn.HorizontalResolution, imgIn.VerticalResolution);

                Graphics grPhoto = Graphics.FromImage(bmPhoto);
                grPhoto.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                grPhoto.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                grPhoto.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
                grPhoto.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;

                try
                {
                    grPhoto.DrawImage(imgIn, new Rectangle(0, 0, width, height),
                                        x1, y1, width , height , GraphicsUnit.Pixel);
                }
                catch (Exception e)
                {
                    throw e;
                }
                finally
                {
//                    imgIn.Dispose();
                    grPhoto.Dispose();
                }
                return bmPhoto ;
            
            }





            public static System.Drawing.Image SmartResizeImage(System.Drawing.Image imgIn, int typeID)
            {
                return SmartResizeImage(imgIn, IMAGE_TYPE.GetImageTypeByID(typeID));

            }

            public static System.Drawing.Image SmartResizeImage(System.Drawing.Image imgIn,IMAGE_TYPE imgType){
                return SmartResizeImage(imgIn, imgType.WIDTH, imgType.HEIGHT, imgType.W_MIN, imgType.W_MAX, imgType.H_MIN, imgType.H_MAX, imgType.SIZE_MAX, imgType.CROP_POSITION);
            
            }

            /// <summary>
            /// Automatically Resizes/Crops image
            /// </summary>
            public static System.Drawing.Image SmartResizeImage(System.Drawing.Image imgIn, int width, int height, int minWidth, int maxWidth, int minHeight, int maxHeight, int sizeMax, int cropPosition)
            {
                float ratioNew = (float)height / (float)width;
                float ratioIn = (float)imgIn.Height  / (float)imgIn.Width;

                int tmpWidth;
                int tmpHeight;


                // todo: if some min/max are set and some fixed w/h are set : combine


                // if fixed H & W
                if (width > 0 && height > 0)
                { // check if new ratio is OK
                    double diff = (double) (ratioIn   / ratioNew );
                    // if calculated ratio is different from original for less then 1 %, just resize it
                    if (diff < 1.001 && diff > 0.999)
                        return ResizeImage(imgIn, width, height);
                    else
                    {


                        return CropImage(imgIn, width, height, cropPosition);

                    }
                    
                }

                // Resize image
                if (width > 0 || height > 0)
                    return ResizeImage(imgIn, width, height);


                // if we have range of width/height set
                if (maxWidth < 1 || minWidth < 1 || maxHeight < 1 || minHeight < 1)
                    throw  new System.Exception(" Image range is not properly set. All parameters must be > 0. ");


                // calculate ratios
                float ratioMin = (float)minHeight / (float)maxWidth;
                float ratioMax = (float)maxHeight / (float)minWidth;
                ratioNew = (float)maxHeight / (float)maxWidth;


                // if image is in boundaries, do nothing
                if (imgIn.Height <= maxHeight && imgIn.Height >= minHeight && imgIn.Width <= maxWidth && imgIn.Width >= minWidth)
                    return imgIn;

                // if ratio is in boundaries, calculate maximum new resized image
                if (ratioIn >= ratioMin && ratioIn <= ratioMax)
                {
                    //calulate new maximum dimensions

                    if (ratioNew > ratioIn)
                    {
                        tmpWidth = maxWidth;
                        tmpHeight = (int)Math.Round((float)maxWidth * ratioIn);
                    }
                    else
                    {
                        tmpWidth = (int)Math.Round((float)maxHeight / ratioIn);
                        tmpHeight = maxHeight;
                    }

                    // resize image to new dimensions
                    return ResizeImage(imgIn, tmpWidth, tmpHeight);
                }
                // ratio is not in boundaries, image will be cropped to best ratio
                else {
                    if (ratioIn < ratioMin)
                    {
                        tmpWidth = maxWidth;
                        tmpHeight = minHeight;
                    }
                    else {
                        tmpWidth = minWidth ;
                        tmpHeight = maxHeight;                    
                    }
                    return CropImage(imgIn, tmpWidth, tmpHeight, cropPosition);
                }

            }



            /// <summary>
            /// Resizes image to set dimensions. if Width or Height is > 0 then it calculates it to preserve ratio
            /// </summary>
            public static System.Drawing.Image ResizeImage(System.Drawing.Image imgPhoto, int Width, int Height)
            {
                int sourceWidth = imgPhoto.Width;
                int sourceHeight = imgPhoto.Height;

                // if image is smaller, return image
                if (sourceHeight <= Height && sourceWidth <= Width) return imgPhoto;

                // if only one parameter is set, calculate other one
                if (Width < 1 && Height < 1) return null;
                if (Width < 1)
                {
                    Width = (int) Math.Round((float)Height * ((float)sourceWidth / (float)sourceHeight)); 
                }
                else {
                    Height = (int)Math.Round((float)Width * ((float)sourceHeight / (float)sourceWidth));
                }

                
                PixelFormat Format = imgPhoto.PixelFormat;
                if (Format.ToString().Contains("Indexed"))
                    Format = PixelFormat.Format24bppRgb;

                Bitmap bmPhoto = new Bitmap(Width, Height, Format);
                bmPhoto.SetResolution(imgPhoto.HorizontalResolution, imgPhoto.VerticalResolution);

                // to fix bug with gray borders
                System.Drawing.Imaging.ImageAttributes attr = new System.Drawing.Imaging.ImageAttributes();
                attr.SetWrapMode(System.Drawing.Drawing2D.WrapMode.TileFlipXY);

                Graphics grPhoto = Graphics.FromImage(bmPhoto);
//                grPhoto.Clear(Color.Red);
                grPhoto.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                grPhoto.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                grPhoto.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
                grPhoto.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;


                //grPhoto.DrawImage(imgPhoto,
                //    new Rectangle(0, 0, Width, Height),
                //    new Rectangle(0, 0, sourceWidth, sourceHeight),
                //    GraphicsUnit.Pixel);


                grPhoto.DrawImage(imgPhoto,
                    new Rectangle(0, 0, Width, Height),
                    0, 0, sourceWidth, sourceHeight,
                    GraphicsUnit.Pixel, attr);

                grPhoto.Dispose();
                attr.Dispose();
                //imgPhoto.Dispose();
                return bmPhoto;
            }


            /// <summary>
            /// Crops image to set dimensions. Use CropPosition to set initial cropping position
            /// </summary>
            public static System.Drawing.Image CropImage(System.Drawing.Image imgPhoto, int Width, int Height, int CropPosition)
            {
                int sourceWidth = imgPhoto.Width;
                int sourceHeight = imgPhoto.Height;
                int sourceX = 0;
                int sourceY = 0;
                int destX = 0;
                int destY = 0;

                double nPercent = 0;
                double nPercentW = 0;
                double nPercentH = 0;

                nPercentW = ((double)Width / (double)sourceWidth);
                nPercentH = ((double)Height / (double)sourceHeight);

                if (nPercentH < nPercentW)
                {
                    nPercent = nPercentW;
                    switch (CropPosition)
                    {
                        case 1: // top
                            destY = 0;
                            break;
                        case 2: // bottom
                            destY = (int)Math.Round(Height - (sourceHeight * nPercent));
                            break;
                        default: // center
                            destY = (int)Math.Round((Height - (sourceHeight * nPercent)) / 2);
                            break;
                    }
                }
                else
                {
                    nPercent = nPercentH;
                    switch (CropPosition)
                    {
                        case 3:// left
                            destX = 0;
                            break;
                        case 4://right
                            destX = (int)Math.Round(Width - (sourceWidth * nPercent));
                            break;
                        default://center
                            destX = (int)Math.Round((Width - (sourceWidth * nPercent)) / 2);
                            break;
                    }
                }

                int destWidth = (int)Math.Round(sourceWidth * nPercent);
                int destHeight = (int)Math.Round(sourceHeight * nPercent);

                // if image is inside dimensions, return
                if (sourceWidth == Width && sourceHeight == Height)
                    return imgPhoto;

                
                //Bitmap bmPhoto = new Bitmap(Width, Height, PixelFormat.Format24bppRgb);
                PixelFormat Format = imgPhoto.PixelFormat;
                if (Format.ToString().Contains("Indexed"))
                    Format = PixelFormat.Format24bppRgb;

//                Bitmap bmPhoto = new Bitmap(destWidth, destHeight, Format);
                Bitmap bmPhoto = new Bitmap(Width, Height, Format);
                bmPhoto.SetResolution(imgPhoto.HorizontalResolution, imgPhoto.VerticalResolution);

                Graphics grPhoto = Graphics.FromImage(bmPhoto);
                //grPhoto.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

                // to fix bug with gray borders
                System.Drawing.Imaging.ImageAttributes attr = new System.Drawing.Imaging.ImageAttributes();
                attr.SetWrapMode(System.Drawing.Drawing2D.WrapMode.TileFlipXY);


                grPhoto.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality ;
                
                grPhoto.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                grPhoto.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
                grPhoto.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                grPhoto.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceCopy ;


                //grPhoto.DrawImage(imgPhoto,
                //    new RectangleF((float)(destX - 0.5), (float)(destY - 0.5), (float)(destWidth + 1), (float)destHeight + 1), // to fix bug with gray borders
                //    new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
                //    GraphicsUnit.Pixel);
                grPhoto.DrawImage(imgPhoto,
                    new Rectangle(destX , destY , destWidth , destHeight ), 
                    sourceX, sourceY, sourceWidth, sourceHeight,
                    GraphicsUnit.Pixel, attr);

                grPhoto.Dispose();
                attr.Dispose();
//                imgPhoto.Dispose();
                return bmPhoto;
            }        



        }


        //public static IMAGE_TYPE GetTypeByID(int typeID) {
        //    eMenikDataContext db = new eMenikDataContext();
        //    return (IMAGE_TYPE )db.IMAGE_TYPEs.SingleOrDefault(w => w.TYPE_ID == typeID);        
        //}

        public class Move {


            public static bool MoveImageToPosition(Guid cwpID, int id,  int pos, string user)
            {
                CMS_WEB_PART cwp = CMS_WEB_PART.GetWebpartByID(cwpID , user);

                if (cwp == null) return false;

                MoveImageToPosition(cwp.ID_INT.Value , id, pos);

                return true;

            }

            public static void MoveImageToPosition(int galID, int id, int pos)
            {

                int step = 5;
                int newOrder = step;// order for first item in list

                eMenikDataContext db = new eMenikDataContext();

                // get all webparts in zone
                var list = (from i in db.IMAGE_ORIGs from g in db.IMAGE_GALLERies where g.GAL_ID == galID && g.IMG_ID == i.IMG_ID orderby g.IMG_ORDER select new {g.IMG_ID , g.IMG_ORDER }).ToList();
                // ...without current one
                list = list.Where(w => w.IMG_ID  != id).ToList();

                // item is only one
                if (list == null)
                {
                    UpdateOrder(galID, id, newOrder);
                    return;
                }

                // item is last one
                if ( list.Count <= pos)
                {
                    newOrder = list[list.Count - 1].IMG_ORDER  + step;
                    UpdateOrder(galID,  id, newOrder);
                    return;
                }

                int ord = step;
                // reorder entire list
                for (int i = 0; i < list.Count(); i++)
                {

                    if (i == pos || (pos < 0 && i == 0))
                    {
                        newOrder = ord;
                        ord += step;
                    }
                    UpdateOrder(galID,  list[i].IMG_ID , ord);

                    ord += step;
                }

                UpdateOrder(galID,  id, newOrder);
            }
        
        
        
        
        }

    }
