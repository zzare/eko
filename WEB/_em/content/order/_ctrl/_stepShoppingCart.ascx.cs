﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SM.UI.Controls;
using SM.EM.UI.Controls;


public partial class _em_content_order__ctrl__stepShoppingCart : BaseWizardStepOrder 
{
    public event EventHandler OnSaveOK;


    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        ShortDesc = "Pregled naročila";
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        cvPayment.ErrorMessage = Resources.Product.msgEmptyBasket;
     

    }


    public override void LoadData()
    {

        ProductRep prep = new ProductRep();

        List<ProductRep.Data.ShoppingCartItem> list = prep.GetCartItemList(SM.EM.BLL.Membership.GetCurrentUserID(), LANGUAGE.GetCurrentLang(), ParentPageEmenik.em_user.UserId, null);
        objShoppingCart.Data = list;
        objShoppingCart.Parameters = new object[] { ParentPageEmenik.em_user.UserId, false  };
        objShoppingCart.LoadData();
        if (list.Count <= 0)
            btNext.Visible = false; 


    }

    protected void ValidateCart(object o, ServerValidateEventArgs args)
    {
        args.IsValid = true;

        ProductRep prep = new ProductRep();

        List<ProductRep.Data.ShoppingCartItem> list = prep.GetCartItemList(SM.EM.BLL.Membership.GetCurrentUserID(), LANGUAGE.GetCurrentLang(), ParentPageEmenik.em_user.UserId, null);
        if (list.Count <= 0)
        {
            args.IsValid = false;
            CustomValidator cv = (CustomValidator)o;
            cv.ErrorMessage = "* " + Resources.Product.msgEmptyBasket;
            return;


        }
        // check total
        var _totalSum = (from c in list select c.PriceTotal).Sum();
        if (_totalSum < SM.BLL.Custom.MinimumOrderSum) {
            args.IsValid = false;
            CustomValidator cv = (CustomValidator)o;
            cv.ErrorMessage = string.Format(Resources.Product.step0MsgMinimum , SM.EM.Helpers.FormatPrice(SM.BLL.Custom.MinimumOrderSum));

        }

        LoadData();

    }

    protected void btNext_Click(object o, EventArgs e)
    {
        Page.Validate("valCart");
        if (!Page.IsValid)
            return;

        // if all ok
        OnSaveOK(this, new EventArgs());
    
    }


}
