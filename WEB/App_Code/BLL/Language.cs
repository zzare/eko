﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Threading;

/// <summary>
/// Summary description for Language
/// </summary>
public partial class LANGUAGE
{
    public static string GetDefaultLang()
    {

        return "si";
        //return "en";
    }
    
    public static string GetCurrentLang() {

        CULTURE c = CULTURE.GetCulture(GetCurrentCulture());
        if (c != null)
            return c.LANG_ID;

        return "si"; 
        //return "en";
    }
    public static string GetDefaultCulture()
    {

        return "sl-SI";
        //return "en";
    }
    public static string GetCurrentCulture()
    {
        string cult = GetDefaultCulture();
        if (HttpContext.Current == null) return cult;

        // try CONTEXT
        if (HttpContext.Current.Items["c"] != null)
            cult =  HttpContext.Current.Items["c"].ToString(); 

        // try QS
        else if (HttpContext.Current.Request.QueryString["c"] != null)
            cult = HttpContext.Current.Request.QueryString["c"];
            
        // try session
        else if (HttpContext.Current.Session != null)
            if (HttpContext.Current.Session["MyCulture"] != null)
                cult =  HttpContext.Current.Session["MyCulture"].ToString();


        if (cult.IndexOf(",") >= 0)
            cult = cult.Remove(cult.IndexOf(","));

        return ValidateCulture(cult);
    }

    public static string ValidateCulture(string culture) {
        string ret = GetDefaultCulture();

        if (culture.IndexOf("-") < 0)
            return ret;

        if (culture.Length != 5)
            return ret;

        string cult = culture.Substring(0, 2);
        string country = culture.Substring(3, 2);

        return cult.ToLower() + "-" + country.ToUpper();
    }

    public static string GetLangFromCulture(string culture)
    {
        string ret = culture;

        string cult = culture.Substring(0, 2);
        return cult.ToLower();
    }


    public static void SetCurrentCulture(string uiCulture)
    {
        if (HttpContext.Current == null) return;

        uiCulture = ValidateCulture(uiCulture);

        // add to CONTEXT
        HttpContext.Current.Items["c"] = uiCulture;

        //HttpContext.Current.Session["MyCulture"] = uiCulture;
    }
    public static bool IsCultureSet()
    {
        bool cult = false;
        if (HttpContext.Current == null) return cult;

        // try CONTEXT
        if (HttpContext.Current.Items["c"] != null)
            return true;

        // try QS
        if (HttpContext.Current.Request.QueryString["c"] != null)
            return true;

        // try session
        if (HttpContext.Current.Session != null)
            if (HttpContext.Current.Session["MyCulture"] != null)
                return true;
        return cult;
    }

    public static int GetCurrentCultureLCID()
    {
        if (HttpContext.Current.Session != null && HttpContext.Current.Session["MyCultureLCID"] != null)
            return int.Parse(HttpContext.Current.Session["MyCultureLCID"].ToString());
        return 24; //si
    }

    public static IEnumerable<LANGUAGE> GetSmapLangsNew(int id)
    {
        eMenikDataContext db = new eMenikDataContext();
        IEnumerable<LANGUAGE> smap;

        smap = from l in db.LANGUAGEs
               where !(from sml in db.SITEMAP_LANGs where sml.SMAP_ID == id select sml.LANG_ID).Contains(l.LANG_ID)
               select l;
        return smap;
    }

    public static IEnumerable<LANGUAGE> GetEmUsersLanguages(Guid userid)
    {
        eMenikDataContext db = new eMenikDataContext();
        IEnumerable<LANGUAGE> smap;

        smap = (from uc in db.EM_USER_CULTUREs join c in db.CULTUREs on uc.LCID equals c.LCID join l in db.LANGUAGEs on c.LANG_ID equals l.LANG_ID 
               where uc.UserId == userid
               select l).Distinct();
        return smap;
    }

    public static IEnumerable<LANGUAGE> GetLanguagesAll()
    {
        eMenikDataContext db = new eMenikDataContext();
        IEnumerable<LANGUAGE> smap;

        smap = from l in db.LANGUAGEs orderby l.LANG_ORDER 
               select l ;
        return smap;
    }

    partial void OnValidate(System.Data.Linq.ChangeAction action)
    {
        if (this.LANG_ID.Length != 2)
            throw new ArgumentException("Length of LANG_ID must be 2 chars.");    
    }

}
