﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


/// <summary>
/// Summary description for PermissionRep
/// </summary>
public class PermissionRep
{

    private eMenikDataContext  db = new eMenikDataContext ();

   
    // Query Methods
    //public ARTICLE GetArticleByID(int artID, Guid userid)
    //{
        
    //    ARTICLE art;

    //    art = (from a in db.ARTICLEs
    //           from s in db.SITEMAPs
    //           from m in db.MENUs
    //           from u in db.EM_USERs
    //           where a.ART_ID == artID && s.SMAP_ID == a.SMAP_ID && m.MENU_ID == s.MENU_ID && u.USERNAME == m.USERNAME && u.UserId == userid
    //           select a).SingleOrDefault();

    //    return art;
    //}

    public PERMISSION GetPermissionByID(int id)
    {

        return db.PERMISSIONs.Where(w => w.PermissionID == id).SingleOrDefault();
    }

    public PERMISSION GetPermissionByID(Guid user, Guid scope, Guid obj, PERMISSION.Common.Action action)
    {
        
        return db.PERMISSIONs.Where( w=>w.UserId == user && w.Scope == scope && w.Object == obj && w.Action == (short) action  ).SingleOrDefault();
    }

    public IQueryable< PERMISSION> GetPermissionByUser(Guid user)
    {

        return db.PERMISSIONs.Where(w => w.UserId == user);
    }









    // add

    public void Add(PERMISSION p)
    {
        if (HttpContext.Current != null)
            p.AddedBy  = SM.EM.BLL.Membership.GetCurrentUserID ();

        p.DateAdded  = DateTime.Now;

        db.PERMISSIONs.InsertOnSubmit(p);
    }


    // delete
    public void Delete(int id)
    {
        Delete(GetPermissionByID(id));
    }
    public void Delete(PERMISSION p)
    {
        db.PERMISSIONs.DeleteOnSubmit(p);
    }

    
    // Persistence

    public bool  Save()
    {
        try
        {
            db.SubmitChanges();
        }
        catch (Exception ex)
        {
            ERROR_LOG.LogError( ex, "Save permissions");

            return false;
        }
        return true;        
    }








}