﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _ctrl_module_product_viewProductListItemBig : BaseControlView
{

    public object Data;
    public object[] Parameters;
    public vw_Product Prod;



    protected void Page_Load(object sender, EventArgs e)
    {
        LoadList();
    }



    public void LoadList() {

        if (Data == null)
            return;
        Prod = Data as vw_Product;

        if (Parameters != null)
        {
            if (Parameters[0] != null)
                IsModeCMS = bool.Parse(Parameters[0].ToString());
            //if (Parameters.Length > 1 && Parameters[1] != null)
            //    LangID = Parameters[1].ToString();
        }

        //objRating.AvgRating = (float)Prod.AvgRating;
        //objRating.Number = 5;


    }

    protected string RenderClass(object d)
    {
        vw_Product data = (vw_Product)d;
        string ret = "";

        if (data.CAT_ACTION)
            ret += " action ";

        if (!IsModeCMS)
            return ret;

        if (!data.ACTIVE)
            ret = " inactive ";

        return ret;
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        if (Prod == null)
            this.Visible = false;
    }




}
