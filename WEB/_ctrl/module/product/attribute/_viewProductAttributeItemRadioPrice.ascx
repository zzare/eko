﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewProductAttributeItemRadioPrice.ascx.cs" Inherits="_ctrl_module_product_attribute_viewProductAttributeItemRadioPrice" %>

<script type="text/javascript">

    $(function () {
        <%= SelectFirstAttribute() %>
    });


    <% if(IsModeCMS ) {%>
function editProductAttributeDesc(p, a, l) {

    setModalLoaderLoading();
    openModalLoading('Odpiram...');
    var _items = new Array();
    Array.add(_items, p);
    Array.add(_items, a);
    Array.add(_items, l);
    WebServiceCWP.RenderViewAut(_items, 19, onContentLoadSuccess, onError);
}
<%} %>

</script>

<div class="cAttrPrice">
<span class="ap_heading"><%= GetGlobalResourceObject("Product", "choosePackage") %>:</span>
<asp:ListView ID="lvList" runat="server" >
    <LayoutTemplate>
        
            <asp:PlaceHolder ID="itemPlaceholder" runat="server"/>
            
    </LayoutTemplate>
        
    <ItemTemplate>

        <label for='pap<%# Eval("AttributeID") %>' class="itmProdAttrPrice" id='<%# Eval("AttributeID") %>' data-price='<%#SM.EM.Helpers.FormatPrice(  Eval("Price")) %>' >
            <input id='pap<%# Eval("AttributeID") %>'  type="radio" name="<%= GroupAttribute.GroupAttID  %>" <%# RenderSelected(Eval("AttributeID")) %> value='<%# Eval("AttributeID") %>'  />
            <span class="ap_rbt_title"><%# Eval("Title") %><%# RenderPrice(Container.DataItem )  %></span>
            
            <span class="ap_rbt_desc"><%# Eval("Desc") %></span>
            <% if (IsModeCMS){%>
            <span><a class="btEditContwthText" href="#" onclick="editProductAttributeDesc('<%# ProductID %>', '<%# Eval("AttributeID") %>', em_g_lang); return false" ><span>uredi opis paketa</span></a></span>
            <%} %>
        
        </label>
            
    </ItemTemplate>
</asp:ListView>
    <input id="hf<%= GroupAttribute.GroupAttID %>" class="hfAttr" type="hidden"  />
    <input class="hfGrAttr" type="hidden" value='<%= GroupAttribute.GroupAttID  %>' />
    
</div>