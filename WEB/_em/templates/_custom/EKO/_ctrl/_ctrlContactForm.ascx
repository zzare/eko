﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_ctrlContactForm.ascx.cs" Inherits="_em_templates__custom_SD__inc__ctrl_ctrlContactForm" %>


<script type="text/javascript">


    function notWatermark(value, element) {
        return (value != $(element).attr('title'));
    }

    $.validator.addMethod("notWatermark", notWatermark, "");

    $(document).ready(function () {

        $('#<%= ViewID %>').find('.wm').watermark('watermark');

        $("#aspnetForm").validate({
            onsubmit: false,
            rules: {
                CoFoName: { required: true, notWatermark: true },
                CoFoText: { required: true, notWatermark: true },
                CoFoEmail: { required: true, email: true, notWatermark: true }

            },
            messages: {
                CoFoName: { required: "", notWatermark: "" },
                CoFoText: { required: "", notWatermark: "" },
                CoFoEmail: {
                    required: "",
                    email: ""
                    , notWatermark: ""
                }
            }
        });

    });
    

    function contactFormSendMail(v, id) {

        var $group = $('#' + v ).find('.validationGroup');
        var isValid = true;

        $group.find(':input').each(function (i, item) {
            if (!$(item).valid())
                isValid = false;
        });


        //alert(isValid);
        if (isValid) {
            var add = null;
            openModalLoading('<%= GetGlobalResourceObject("Modules", "MessageTitleSubmitting")%>');
            <% if (Cust != null && Cust.CustomerID != Guid.Empty){%>
            var _name = '<%= Cust.FIRST_NAME + " " + Cust.LAST_NAME %>';
            var _email = '<%= Cust.EMAIL %>';
            <%} else { %>
            var _name = $('#' + v).find('.CoFoName').val();
            var _email = $('#' + v).find('.CoFoEmail').val();
             <%} %>
            WebServiceP.SubmitContactFormShort(id, _name, $('#' + v).find('.CoFoText').val(), _email, em_g_cult, em_g_lang, add, '', '',
            //WebServiceP.SubmitContactFormShort(id, $('#' + v).find('.CoFoName').val(), $('#' + v).find('.CoFoText').val(), $('#' + v).find('.CoFoEmail').val(), em_g_cult, em_g_lang, add, '', '',
            function (res) {
                closeModalLoading();
                if (res.Code == 1) {
                    $('#' + v).find('.CoFoText').val('').watermark('watermark');
                    setModalMsg('<%= GetGlobalResourceObject("Modules", "MessageContactFormSubmitted")%>', res.Data, true);
                }
                else {
                    setModalMsg('<%= GetGlobalResourceObject("Modules", "MessageErrorTitle")%>', res.Data, true);
                }


            }, onError);
        }
    
    }



</script>


<div id="<%= ViewID %>" >


            <div class="contact_formW">
            <table class="tbl_contact_form validationGroup" border="0" cellpadding="0" cellspacing="0">
                <tbody>
                    <% if (Page.User.Identity.IsAuthenticated){%>
                    <tr>
                        <%--<td class="td_text td_veralign"><span>Moj predlog*:</span></td>--%>
                        <td colspan="2" class="td_input td_nopadd"><textarea title="<%= GetGlobalResourceObject("Modules", "ContactFormTextTitle")%>." rows="4" class="val CoFoText wm"   name="CoFoText"></textarea></td>
                        <td class="td_error"></td>
                    </tr>
                    <%}else{ %>
                    <tr>
                        <td class="td_input td_nopadd"><input title="<%= GetGlobalResourceObject("Modules", "ContactFormNameTitle")%>*" class="val CoFoName wm"   name="CoFoName" type="text" maxlength="256"  /></td>
                        <td class="td_input td_input_sec td_nopadd"><input title="E-mail*" class="val CoFoEmail wm"    name="CoFoEmail" type="text" maxlength="64"  /></td>

                        <td class="td_error"></td>
                    </tr>
                    <tr>
                        <%--<td class="td_text td_veralign"><span>Moj predlog*:</span></td>--%>
                        <td colspan="2" class="td_input td_nopadd"><textarea title="<%= GetGlobalResourceObject("Modules", "ContactFormTextTitle")%>." rows="3" class="val CoFoText wm short"   name="CoFoText"></textarea></td>
                        <td class="td_error"></td>
                    </tr>
                    <%} %>




                </tbody>
            </table>

        </div>
        <div class="contact_form_btnW"><a class="btn_send" href="#" onclick="contactFormSendMail('<%= ViewID %>', '<%= ViewID %>'); return false;"><span class="font_bevan"><%= GetGlobalResourceObject("Modules", "Send")%></span></a></div>
        <div class="clearing"></div>


</div>
