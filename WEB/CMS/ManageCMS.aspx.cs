﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

namespace SM.EM.UI.CMS
{
    public partial class ManageCMS : BasePage_CMS 
    {
        protected  int SitemapID{
            get{
                // if menu is postback:
                //if (tvSitemap.SelectedNode == null) return -1;
                //return int.Parse(tvSitemap.SelectedValue); 
                int lOut = -1;
                if (Request.QueryString["smp"] != null) {
                    if (int.TryParse(Request.QueryString["smp"], out lOut))
                        return lOut;                
                }
                return lOut;
            }
        }

        protected override void OnPreInit(EventArgs e)
        {
           
                base.OnPreInit(e);

                this.CheckPermissions = true;

                if (SitemapID != 14)
                    DefaultProvider = "CMSEdit";
        }

        protected void Page_Load(object sender, EventArgs e){

            this.Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "AjaxFCKHack", "for ( var i = 0; i < parent.frames.length; ++i ) if ( parent.frames[i].FCK ) parent.frames[i].FCK.UpdateLinkedField();");


            // init control data
            cmsContent._smapID = SitemapID;
            cmsContent._langID = ddlLang.SelectedValue;
            cmsArticle._smapID = SitemapID;
            cmsArticle._langID = ddlLang.SelectedValue;
            //cmsGallery._smapID = SitemapID;
            //cmsGallery._langID = ddlLang.SelectedValue;

            if (!IsPostBack) {

                if (ddlLang != null){
                    ddlLang.DataSource = LANGUAGE.GetLanguagesAll();
                    ddlLang.DataTextField = "LANG_DESC";
                    ddlLang.DataValueField = "LANG_ID";
                    ddlLang.DataBind();
                }
                // pre-select language
                if (Session["cmsLang"] != null){
                    ddlLang.SelectedValue = Session["cmsLang"].ToString();
                }

                // enable/moduls
                IEnumerable<SITEMAP_MODULE> smapModuls = SITEMAP.GetModulsForSitemapModuls(SitemapID );
                foreach (SITEMAP_MODULE smap in smapModuls) {

                    foreach (AjaxControlToolkit.TabPanel t in tabModule.Tabs) {
                        if (t.ID == "tpM" + smap.MOD_ID.ToString()) {
                            t.Visible = true;
                        }
                    }                
                }

                // load controls
                LoadContent();
            }


       }

        protected void tvSitemap_SelectedNodeChanged(object sender, EventArgs e)        {
            if (tvSitemap.SelectedNode == null) return;
            
            SetStatus(tvSitemap.SelectedNode.Text);
        }


        protected void tvSitemap_OnPreRender(object sender, EventArgs e){
            // hide content if not selected
            if (tvSitemap.SelectedNode == null){
                
                upContent.Visible = false;
                SetStatus("Select Menu");
            }
            else{
                upContent.Visible = true;
            }
        }

        protected void ddlLang_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["cmsLang"] = ddlLang.SelectedValue;

        }

        protected void LoadContent() {

            if (tpM1.Visible)
                cmsContent.LoadTab();
            if (tpM2.Visible)
                cmsArticle.LoadTab();
            //if (tpM3.Visible)
            //    cmsGallery.LoadTab();
        
        }
}
}