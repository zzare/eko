﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Reflection;
using SM.EM.UI;
using SM.EM.UI.Controls;

public partial class _em_content_order_order : SM.EM.UI.BasePage_EMENIK
{
    protected int MaxSteps = 5;


    Guid _orderID;
    public Guid OrderID { get {
        Guid ret = Guid.Empty;
        // if querystring is set, use it
        if (Request.QueryString["o"] != null)
        {
            if (SM.EM.Helpers.IsGUID(Request.QueryString["o"]))
                ret = new Guid(Request.QueryString["o"]);
        }
        if (ret != Guid.Empty)
            return ret;

        return ret;
    } }

    OrderRep orep ;

    protected SHOPPING_ORDER _order;
    protected SHOPPING_ORDER Order { get {
        if (_order != null)
            return _order;

        int step = -1;
        // if querystring is set, use it
        if (Request.QueryString["st"] != null)
        {
            Int32.TryParse(Request.QueryString["st"], out step);
        }        
        if (step  < SHOPPING_ORDER.Common.Status.STEP_2 && OrderID == Guid.Empty )
            return null;
        if (!User.Identity.IsAuthenticated)
            RequestCustomerLogin() ;

        //  get order by ID        

        if (OrderID != Guid.Empty)
        {
            _order = orep.GetOrderByID(OrderID);
            if (_order != null) // if user viewving is not owner and not admin
                if (_order.UserId != SM.EM.BLL.Membership.GetCurrentUserID() && !SM.EM.Security.Permissions.IsCMSeditor())
                    ErrorRedirect(151, 404, false);
        }
        if (_order != null)
            return _order;
        
        // ensure order
        _order = orep.EnsureOpenOrderByUser(SM.EM.BLL.Membership.GetCurrentUserID(), em_user.UserId );
        return _order;
    } }

    protected int CurrentStepID
    {
        get
        {
            int step = -1;

            //// get step form viewstate
            //if (step == -1 && ViewState["CurrentStepID"] != null)
            //{
            //    Int32.TryParse(ViewState["CurrentStepID"].ToString(), out step);
            //}

            // if querystring is set, use it
            if (step == -1 && Request.QueryString["st"] != null)
            {
                Int32.TryParse(Request.QueryString["st"], out step);
            }



            // if step is not allowed, go to max step
//            if (step > em_user.STEPS_DONE + 1) step = em_user.STEPS_DONE + 1;
            if (step >= MaxSteps) step = MaxSteps ;

            // check order
            if ( step > 0  &&  Order != null) {
                if (Order.OrderStatus >= SHOPPING_ORDER.Common.Status.STEP_CONFIRMED )
                    step = SHOPPING_ORDER.Common.Status.STEP_CONFIRMED;
                else if (Order.OrderStatus < step)
                    step = Order.OrderStatus;
            
            }
            if (step < 1) step = 1;


            return step;
        }

        set
        {
            ViewState["CurrentStepID"] = value;
        }
    }

    protected BaseWizardStepOrder  CurrentStep
    {
        get
        {
            switch (CurrentStepID)
            {
                case 1:
                    return StepCart as BaseWizardStepOrder;
                case 2:
                    return StepPayment as BaseWizardStepOrder;
                case 3:
                    return StepAddress as BaseWizardStepOrder;
                case 4:
                    return StepSummary as BaseWizardStepOrder;
                case 5:
                    return StepConfirmation as BaseWizardStepOrder;

                default:
                    return StepCart as BaseWizardStepOrder;

            }
        }
    }




    protected override void OnInit(EventArgs e)
    {
        // set sitemap id
        CurrentSitemapID = -1;

        base.OnInit(e);

        orep = new OrderRep();

        // init caching
        this.OutputCacheDuration = -1;
    }

    //protected override void OnInitComplete(EventArgs e)
    //{
    //    base.OnInitComplete(e);
    //    ProductRep prep = new ProductRep();
    //    var list  = prep.GetProductDescListSearch(SearchString, LANGUAGE.GetCurrentLang(), true, em_user.UserId);
    //    objList.ProductList = list;
    //    objList.PageSize = 50;
    //    objList.EnableSorting = false;
    //}
    protected void Page_Load(object sender, EventArgs e)
    {

        // events
        StepCart.OnSaveOK += new EventHandler(OnSaveOK);
        StepPayment.OnSaveOK += new EventHandler(OnSaveOK);
        StepAddress.OnSaveOK += new EventHandler(OnSaveOK);
        StepSummary.OnSaveOK += new EventHandler(OnSaveOK);
        StepConfirmation.OnSaveOK += new EventHandler(OnSaveOK);



        // init
        CurrentStep.Orep = orep;
        CurrentStep.Order = Order;

        //// tmp init
        //StepSummary.Orep = orep;
        //StepSummary.Order = Order;


        // show current step (also if not postback)
        StepCart.Visible = false;
        StepPayment.Visible = false;
        StepAddress.Visible = false;
        StepSummary.Visible = false;
        StepConfirmation.Visible = false;

        CurrentStep.Visible = true;
        CurrentStep.EnableViewState = true;

        if (!IsPostBack)
        {
            LoadStep();
        }

        // init navigation
        objNavigation.CurrentStepID = CurrentStepID;
        int stepsdone = 2;
        if (Order != null)
            stepsdone = Order.OrderStatus ;
        objNavigation.StepsDone = stepsdone;
    }


    protected void LoadStep()
    {



        // show current step
        //CurrentStep.Visible = true;
        //CurrentStep.EnableViewState = true;

        CurrentStep.LoadData();



    }





    void OnSaveOK(object sender, EventArgs e)
    {
        int newstep = CurrentStepID + 1;
        // save step
        if (newstep > 2)
        {
            if (Order == null)
            {
                _order = orep.EnsureOpenOrderByUser(SM.EM.BLL.Membership.GetCurrentUserID(), em_user.UserId );
            }

            if (newstep < SHOPPING_ORDER.Common.Status.STEP_CONFIRMED)
            {
                Order.OrderStatus = (short)newstep;
                orep.Save();
            }
        }

        Guid orderid = OrderID;
        if (orderid == Guid.Empty && Order != null)
            orderid = Order.OrderID;

        // redirect next
        Response.Redirect(Page.ResolveUrl(SM.EM.Rewrite.OrderProductUrl(em_user.UserId, orderid, newstep))); 
        

    }



    protected override void LoadTitle()
    {
        base.LoadTitle();

        // append article to title
        Page.Title = CurrentStepID.ToString() + " - " + CurrentStep.ShortDesc +  " | Naročilo " ;

    }



}
