﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SM.EM.BLL;
namespace SM.EM.UI.Controls
{
    public partial class cmsArticleEdit : CwpEdit
    {
        //public ARTICLE Art;
        public event EventHandler OnArticleListChanged;

        //public int GroupID { get; set; }
        //public string BehaviorID { get { return mpeEdit.BehaviorID; } set { mpeEdit.BehaviorID = value; } }



        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack) { 
                // init
                btCancelEdit.OnClientClick = "closePopup('" + mpeEditArticle.BehaviorID + "'); return false;";

            }

            if (!IsPostBack)
                fckBody.Config["FloatingPanelsZIndex"] = "200000";

            // init validation
            //btSaveEdit.ValidationGroup = BehaviorID;
            //reqTitle.ValidationGroup = BehaviorID;
            //reqAbstract.ValidationGroup = BehaviorID;
            //reqBody.ValidationGroup = BehaviorID; 

        }

        public override void LoadData()
        {
            if (cwp != null)
                Zone = cwp.ZONE_ID;

            BindData();

            Show();
        }


        // bind data
        public void BindData()
        {

            if (IDref <= 0)
            {
                // init data
                tbTitle.Text = "";
                tbReleaseDate.Text = Helpers.FormatDate(DateTime.Now);
                tbExpireDate.Text = "";
                cbActive.Checked = true;
                tbAbstract.Text = "";
                fckBody.Value = "";
                cbComments.Checked = false;

                // hide delete button
                btDelete.Visible = false;
            }
            else
            {
                // manualy load data
                ARTICLE art = ARTICLE.GetArticleByID(IDref);

                tbTitle.Text = Server.HtmlDecode( art.ART_TITLE);
                tbReleaseDate.Text = Helpers.FormatDate(art.RELEASE_DATE);
                
                if (art.EXPIRE_DATE == null )
                    tbExpireDate.Text = "";
                else
                    tbExpireDate.Text = Helpers.FormatDate(art.EXPIRE_DATE);

                cbActive.Checked = art.ACTIVE;
                tbAbstract.Text = Server.HtmlDecode( art.ART_ABSTRACT);
                fckBody.Value = Server.HtmlDecode( art.ART_BODY);
                cbComments.Checked = art.COMMENTS_ENABLED;

                if (SM.EM.Security.Permissions.IsAdvancedCms())
                    btDelete.Visible = true;
            }




        }

        //protected void btCancelEdit_Click(object sender, EventArgs e)
        //{
        //    mpeEdit.Hide();

        //}
        protected void btSaveEdit_Click(object sender, EventArgs e)
        {
            SaveData();

        }

        public void SaveData()
        {

            if (!ParentPage.CanSave) return ;

            Page.Validate("editArt");
            if (!Page.IsValid)
            {
                mpeEditArticle.Show();
                fckBody.Visible = true;

                return;
            } 

            eMenikDataContext db = new eMenikDataContext();

            ARTICLE art = new ARTICLE();

            if (IDref  == -1)// insert new article
            {
                db.ARTICLEs.InsertOnSubmit(art);
                art.ADDED_BY = String.IsNullOrEmpty(Page.User.Identity.Name) ? "anonnymous" : Page.User.Identity.Name;
                art.DATE_ADDED = DateTime.Now;
                art.LANG_ID = LANGUAGE.GetCurrentLang();
                art.SMAP_ID = ParentPage.CurrentSitemapID;
                art.ART_GUID = Guid.NewGuid();
                art.CWP_ID = CwpID;
                art.APPROVED = true;

            }
            else
            { // update old article
                art = db.ARTICLEs.Single(a => a.ART_ID == IDref);
            }

            art.ART_TITLE = Server.HtmlEncode(tbTitle.Text);


            // validate date
            DateTime releaseDate = DateTime.MinValue ;
            if (!string.IsNullOrEmpty(tbReleaseDate.Text.Trim()))
                DateTime.TryParse(tbReleaseDate.Text, out releaseDate);
            
            DateTime expireDate = DateTime.MinValue;
            if (!string.IsNullOrEmpty(tbExpireDate.Text.Trim()))
                DateTime.TryParse(tbExpireDate.Text, out expireDate);

            releaseDate = SM.EM.Helpers.Date.ReValidateSqlDateTime(releaseDate);
            expireDate  = SM.EM.Helpers.Date.ReValidateSqlDateTime(expireDate );


            art.RELEASE_DATE = releaseDate == System.Data.SqlTypes.SqlDateTime.MinValue ? (DateTime?)null : releaseDate;
            art.EXPIRE_DATE = expireDate == System.Data.SqlTypes.SqlDateTime.MinValue ? (DateTime?)null : expireDate;
            art.DATE_MODIFY = DateTime.Now;
            art.ACTIVE = cbActive.Checked;
            art.ART_ABSTRACT = Server.HtmlEncode(tbAbstract.Text);
            art.ART_BODY =  Server.HtmlEncode( fckBody.Value);
            art.COMMENTS_ENABLED = cbComments.Checked;
            //art.GROUP_ID = GroupID;

            db.SubmitChanges();

            // raise changed event
//            this.OnArticleListChanged(this, new EventArgs());
            mpeEditArticle.Hide();
            fckBody.Visible = false;


            // reload zone
            if (Zone != "")
                ParentPage.ReLoadZone(ParentPage.Zones[Zone], true);
            else
                OnArticleListChanged(this, new EventArgs());

            Hide();
        }

        protected void btDelete_Click(object sender, EventArgs e)
        {
            ARTICLE.DeleteArticleByID(IDref);

            // reload zone
            if (Zone != "")
                ParentPage.ReLoadZone(ParentPage.Zones[Zone], true);
            else
                OnArticleListChanged(this, new EventArgs());

        }



        public void Show() {
            mpeEditArticle.Show();
            fckBody.Visible = true;
            this.Visible = true;

        }
        public void Hide() {
            mpeEditArticle.Hide();
            this.Visible = false;
        }
}
}
