﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewPoll.ascx.cs" Inherits="_ctrl_module_poll_viewPoll" %>

<asp:PlaceHolder ID="phOK" runat="server">

    <asp:PlaceHolder ID="phPollEdit" runat="server">
        <a  class="btEditArticle" href="#" onclick="editPollLoad('<%= Poll.PollID  %>'); return false" ></a>
        <br />
    </asp:PlaceHolder>


    <h3><%= PollLang.PollQuestion %></h3>

        <% Response.WriteSubstitution(   POLL.RenderPollUpdateCommand);%>
        

    <div id="cPoll<%= Poll.PollID  %>">

        <input class="hdPollID" type="hidden" value='<%= Poll.PollID  %>'  />


        <%= RenderPoll() %>
    
        <span class="err" style="display:none;">* <%= GetGlobalResourceObject("Modules", "VoteErrorReq")%>!</span>
    </div>
</asp:PlaceHolder>


<asp:PlaceHolder ID="phEmpty" runat="server">
    Dodane ni nobene ankete. <a href="#" onclick="newPollLoad('<%= Guid.Empty %>', '<%= CwpID.ToString() %>'); return false" >Dodaj anketo</a>
</asp:PlaceHolder>