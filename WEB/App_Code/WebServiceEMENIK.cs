﻿using System;
using System.Collections;
using System.Collections.Generic ;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Linq;
using System.Reflection;
/// <summary>
/// Summary description for WebServiceEMENIK
/// </summary>
[WebService(Namespace = "http://www.1dva3.si/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
 [System.Web.Script.Services.ScriptService]
public class WebServiceEMENIK : System.Web.Services.WebService {

    public WebServiceEMENIK () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }



    [WebMethod]
    public string SetPageActive()
    {
        if (Context == null) return "";

        if (!Context.User.Identity.IsAuthenticated) return "";

        EM_USER user = EM_USER.GetUserByUserName(Context.User.Identity.Name);
        if (user == null) return "";

        user =  EM_USER.UpdateActiveWWW (user.UserId, true);
        return EM_USER.GetPageStatus(user);
    }


    [WebMethod]
    public bool UpdateCwpMap(Guid id, double lat, double lng, int zoom, double mLat, double mLng, string title, string addr, string srch, int height, int width, bool showInfo, string mapType)
    {
        if (!HttpContext.Current.User.Identity.IsAuthenticated)
            return false;

        return  CMS_WEB_PART.UpdateCwpMap(HttpContext.Current.User.Identity.Name, id, lat, lng, zoom, mLat, mLng, title, addr, srch, height, width, showInfo, mapType );
    }

    [WebMethod]
    public bool UpdateCwpMapEnc(string key, Guid id, double lat, double lng, int zoom, double mLat, double mLng, string title, string addr, string srch, int height, int width, bool showInfo, string mapType)
    {
        string username = SM.EM.Security.Encryption.DeHex(key);

        // todo: enhance security check
        if (//!System.Web.Security.Roles.IsUserInRole(username, "is_editor_all")
            
            !SM.EM.Security.Permissions.IsCMSeditor()

            )
        {
            return false;
        }
        
        // get username
        eMenikDataContext db = new eMenikDataContext();
        var uname = (from c in db.CMS_WEB_PARTs 
                    from u in db.EM_USERs 
                    where c.CWP_ID == id && u.UserId == c.UserId 
                    select u.USERNAME ).SingleOrDefault();

        return CMS_WEB_PART.UpdateCwpMap(uname, id, lat, lng, zoom, mLat, mLng, title, addr, srch, height, width, showInfo, mapType);

//        return CMS_WEB_PART.UpdateCwpMap(username, id, lat, lng, zoom, mLat, mLng, title, addr, srch, height, width, showInfo, mapType);
    }





    [WebMethod]
    public string[] CustomCropImage(int imgID, int typeID, int x1, int y1, int x2, int y2, string folder, bool save)
    {
        // cache resulf if user is viewer
        TimeSpan cacheDuration = TimeSpan.FromMinutes(1);

        if (!Context.User.Identity.IsAuthenticated)
        {
            cacheDuration = TimeSpan.FromMinutes(1);
        }
        else
        {
            cacheDuration = TimeSpan.FromMinutes(0);
        }


        FieldInfo maxAge = Context.Response.Cache.GetType().GetField("_maxAge", BindingFlags.Instance | BindingFlags.NonPublic);
        maxAge.SetValue(Context.Response.Cache, cacheDuration);

        Context.Response.Cache.SetCacheability(HttpCacheability.Public);
        Context.Response.Cache.SetExpires(DateTime.Now.Add(cacheDuration));
        Context.Response.Cache.AppendCacheExtension("must-revalidate, proxy-revalidate");
        


        string imgUrl = "";
        string[] res = new string[2];
        // enable webservice for authenticated users only
        if (Context == null) return res;
        
        if (Context.User.Identity.IsAuthenticated)
        {
            imgUrl = IMAGE.Func.CustomCropImage(imgID, typeID, x1, y1, x2, y2, folder);
            res[0] = imgUrl;
            res[1] = VirtualPathUtility.ToAbsolute(imgUrl);

            if (save)
            {


                // get current page
                EM_USER usr = EM_USER.Current.GetCurrentPage();
                if (usr == null)
                {
                    return res;
                }
                // check permissions
                if (!PERMISSION.User.HasPermissionTypeMODULE(usr.UserId, PERMISSION.Common.Obj.MOD_GALLERY, PERMISSION.Common.Action.Edit, PERMISSION.Common.Type.Allow))
                {
                    return res;
                }

                IMAGE.Func.ChangeImage(imgID, typeID, imgUrl);

                
                //if (Context.User.IsInRole("admin"))
                //    IMAGE.Func.ChangeImage(imgID, typeID, imgUrl);
                //else
                //    IMAGE.Func.ChangeImageSecure(imgID, typeID, imgUrl, Context.User.Identity.Name);
            }

        }
        return res;
    }

    [WebMethod]
    public SM.EM.Ajax.ReturnData SaveMenuNew(string name, int smp, string lang, string action)
    {
        SM.EM.Ajax.ReturnData ret = new SM.EM.Ajax.ReturnData { Code = SM.EM.Ajax.ReturnCode.Failure, Message = " no go! " };

        if (Context == null)
        {
            ret.Message = "no go ;)";
            return ret;
        }

        if (!Context.User.Identity.IsAuthenticated)
        {
            ret.Message = "Tvoja seja je potekla. Ponovno se moraš prijaviti v sistem.";
            return ret;
        }

        // get user
        EM_USER usr = EM_USER.Current.GetCurrentPage();
        if (usr == null)
        {
            ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
            return ret;
        }

        // check permissions
        if (!PERMISSION.User.HasPermissionTypePAGE(usr.UserId, PERMISSION.Common.Obj.MENU, PERMISSION.Common.Action.New, PERMISSION.Common.Type.Allow))
        {
            ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
            return ret;
        }

        // save MENU

        List<RuleViolation> errlist = SITEMAP.AddSitemap(usr, name, lang, smp);

        if (errlist != null && errlist.Count > 0) {
            ret.Message = ViewManager.RenderView("~/_ctrl/module/_err/_viewErrValidationListPopup.ascx", errlist, new object[] { });
            return ret;
        
        }   

        ret.Message = "success";
        ret.Code = SM.EM.Ajax.ReturnCode.OK;

        if (action == "soft_r")
        {
            SITEMAP root = SITEMAP.GetRootByAnySitemap(smp);
            if (root != null)
            {
                ret.Data = MENU.Render.MenuEditHierarchy(root.SMAP_ID, lang);
                ret.ID = root.SMAP_ID;
                return ret;
            }
        }


        return ret;
    }

    [WebMethod]
    public SM.EM.Ajax.ReturnData SaveMenuEditAll(int smpID, string lang, List<Data.LangTitleDescActive > items, string custom_class, int pmod,  string action)
    {
        SM.EM.Ajax.ReturnData ret = new SM.EM.Ajax.ReturnData { Code = SM.EM.Ajax.ReturnCode.Failure, Message = " no go! " };

        if (Context == null)
        {
            ret.Message = "no go ;)";
            return ret;
        }

        if (!Context.User.Identity.IsAuthenticated)
        {
            ret.Message = "Tvoja seja je potekla. Ponovno se moraš prijaviti v sistem.";
            return ret;
        }

        // get user
        EM_USER usr = EM_USER.Current.GetCurrentPage();
        if (usr == null)
        {
            ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
            return ret;
        }

        // check permissions
        if (!PERMISSION.User.HasPermissionTypePAGE(usr.UserId, PERMISSION.Common.Obj.MENU, PERMISSION.Common.Action.New, PERMISSION.Common.Type.Allow))
        {
            ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
            return ret;
        }


        // save MENU
        SitemapRep srep = new SitemapRep();
        SITEMAP smap = srep.GetSiteMapByID(smpID, usr.UserId);
        if (smpID == null) {
            ret.Message = "Nimaš pravic ";
            return ret;
        }

        // save lang data
        var def = CULTURE.GetDefaultUserCulture(usr.UserId);
        string mainTitle = "";
        foreach (Data.LangTitleDescActive  item in items )
        {

            string desc = "";
            if (item.Desc != null)
                desc = item.Desc;
            // update sitemap lang
            //if (item.Desc  != null && SM.EM.Security.Permissions.IsAdvancedCms())
            if (SM.EM.Security.Permissions.IsAdmin())
            {
                MENU.ChangeSitemapLang(smpID, item.Title, desc, item.MetaDesc, item.Keywords , item.Lang, item.Active, true);
            }
            else
                MENU.ChangeSitemapLang(smpID, item.Title, item.Lang, item.Active);

            // set default
            if (def.LANG_ID == item.Lang)
                mainTitle = item.Title;

        }

        // save sitemap data
        if (custom_class == null)
            custom_class = "";
        if (SM.EM.Security.Permissions.IsAdvancedCms())
            smap.CUSTOM_CLASS = custom_class;

        if (pmod > 0)
            smap.PMOD_ID = pmod;



        // save
        if (!srep.Save())
        {
            ret.Message = "Napaka pri shranjevanju";
            return ret;
        }

        // check if new SMLs need to be added
        // get users languages
        List<LANGUAGE> langs = LANGUAGE.GetEmUsersLanguages(usr.UserId).ToList();
        
        // add new item for each language if not exists
        foreach (LANGUAGE lan in langs)
        {
            var exists = items.Select(w => w.Lang).Contains(lan.LANG_ID );            
            if (!exists)
                MENU.ChangeSitemapLang(smap.SMAP_ID, mainTitle, lan.LANG_ID, false);
        }

        // remove menu cache for current user
        SM.EM.Caching.RemoveEmenikUserMenuCacheKey(usr.PAGE_NAME);
        SM.EM.BLL.BizObject.PurgeCacheItems(SM.EM.Caching.Key.PageModule(smpID));

        // all ok
        ret.Message = "success";
        ret.Code = SM.EM.Ajax.ReturnCode.OK;

        if (action == "soft_r") {
            SITEMAP root = SITEMAP.GetRootByMenu(smap.MENU_ID);
            if (root != null)
            {
                ret.Data = MENU.Render.MenuEditHierarchy(root.SMAP_ID, lang);
                ret.ID = root.SMAP_ID;
                return ret;
            }
        }

        //if (action == "edit")
        //{
        //    //return redirect url
        //    ret.Data = SM.BLL.Common.ResolveUrl(SM.EM.Rewrite.EmenikUserProductDetailsUrl(prod, pdesc));
        //}
        //else// refresh list
        //{   
        //    var data = productRep.GetProductDescList(smp, lang, null, prod.PageId );
        //    ret.Data = ViewManager.RenderView("~/_ctrl/module/product/_viewProductList.ascx", data, new object[]{} );

        //}


        return ret;
    }

    [WebMethod]
    public SM.EM.Ajax.ReturnData DeleteMenu(int smp)
    {
        SM.EM.Ajax.ReturnData ret = new SM.EM.Ajax.ReturnData { Code = SM.EM.Ajax.ReturnCode.Failure, Message = " no go! " };

        if (Context == null)
        {
            ret.Message = "no go ;)";
            return ret;
        }

        if (!Context.User.Identity.IsAuthenticated)
        {
            ret.Message = "Tvoja seja je potekla. Ponovno se moraš prijaviti v sistem.";
            return ret;
        }

        // get user
        EM_USER usr = EM_USER.Current.GetCurrentPage();
        if (usr == null)
        {
            ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
            return ret;
        }

        // check permissions
        if (!PERMISSION.User.HasPermissionTypePAGE(usr.UserId, PERMISSION.Common.Obj.MENU, PERMISSION.Common.Action.Delete, PERMISSION.Common.Type.Allow))
        {
            ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
            return ret;
        }



        // check permissions
        SITEMAP smap = SITEMAP.GetSiteMapByID(smp, usr.UserId);
        if (smap  == null)
        {
            ret.Message = "Napaka pri brisanju. ";
            return ret;
        }

        // delete MENU
        SITEMAP.DeleteSiteMapByID(smp );
        // remove menu cache for current user
        SM.EM.Caching.RemoveEmenikUserMenuCacheKey(usr.PAGE_NAME );


        ret.Message = "success";
        ret.Code = SM.EM.Ajax.ReturnCode.OK;

        return ret;
    }


}

