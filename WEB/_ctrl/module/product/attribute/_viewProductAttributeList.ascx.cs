﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Linq.Dynamic;

public partial class _ctrl_module_product_attribute_viewProductAttributeList : BaseControlView
{

    public object Data;
    public object[] Parameters;
    public Guid ProductID;
    public List<ProductRep.Data.GroupAttributeLang> ProductGroupList;
//    public List<ProductRep.Data.wAttrubute> ProductAttributeList;
    public List<ProductRep.Data.wProductAttributeDesc> ProductAttributeList;
    public List<ProductRep.Data.AttributeItem> SelectedAttributes;


    protected void Page_Load(object sender, EventArgs e)
    {
        LoadData();
    }



    public void LoadData()
    {
        this.Visible = true;



        if (Data == null)
        {
            this.Visible = false;
            return;
        }
        ProductID  = (Guid )Data;

        if (Parameters != null)
        {
            if (Parameters.Length > 0 &&   Parameters[0] != null)
                IsModeCMS = bool.Parse(Parameters[0].ToString());
            if (Parameters.Length > 1 && Parameters[1] != null)
                ProductGroupList = (List<ProductRep.Data.GroupAttributeLang>)Parameters[1];
            if (Parameters.Length > 2 && Parameters[2] != null)
                ProductAttributeList = (List<ProductRep.Data.wProductAttributeDesc>)Parameters[2];
            if (Parameters.Length > 3 && Parameters[3] != null)
                SelectedAttributes = (List<ProductRep.Data.AttributeItem>)(Parameters[3]);

        }
        // hide if not set
        if (ProductGroupList == null || ProductAttributeList == null )
        {
            this.Visible = false;
            return;
        }

        //// bind data
        //lvList.DataSource = ProductGroupList;
        //lvList.DataBind();


        // slider
        //objColorList.Data = Data;
        //objColorList.IsModeCMS = IsModeCMS;
        //objColorList.ProductGroupList = ProductGroupList.Where(w => w.GrType == ATTRIBUTE.Common.GrType.COLOR).ToList();
        //objColorList.ProductAttributeList = ProductAttributeList;
        //objColorList.SelectedAttributes = SelectedAttributes;
        //objColorList.LoadData();

        //// slider
        //objSliderList.Data = Data;
        //objSliderList.IsModeCMS = IsModeCMS;
        //objSliderList.ProductGroupList = ProductGroupList.Where(w => w.GrType == ATTRIBUTE.Common.GrType.RANGE).OrderBy(o => o.Order).ToList();
        //objSliderList.ProductAttributeList = ProductAttributeList;
        //objSliderList.SelectedAttributes = SelectedAttributes;
        //objSliderList.LoadData();

        // DDL
        objDDLList.Data = Data;
        objDDLList.IsModeCMS = IsModeCMS;
        objDDLList.ProductGroupList = ProductGroupList.Where(w => w.GrType == ATTRIBUTE.Common.GrType.DDL ).ToList();
        objDDLList.ProductAttributeList = ProductAttributeList;
        objDDLList.SelectedAttributes = SelectedAttributes;
        objDDLList.LoadData();

        // RBL
        objRBLList.Data = Data;
        objRBLList.IsModeCMS = IsModeCMS;
        objRBLList.ProductGroupList = ProductGroupList.Where(w => w.GrType == ATTRIBUTE.Common.GrType.RBL_PRICE).ToList();
        objRBLList.ProductAttributeList = ProductAttributeList;
        objRBLList.SelectedAttributes = SelectedAttributes;
        objRBLList.LoadData();


        // hide control if empty
        if (ProductGroupList.Count == 0)
            this.Visible = false;


    }

    //public string RenderItem( object o) {
    //    ProductRep.Data.GroupAttributeLang ga = (ProductRep.Data.GroupAttributeLang)o;
    //    var pa = (from a in ProductAttributeList
    //                          where a.GroupAttID == ga.GroupAttID
    //                          select a);

    //    if (!string.IsNullOrEmpty(ga.OrderByField))
    //        pa = pa.AsQueryable().OrderBy(ga.OrderByField);
    //    else
    //        pa = pa.OrderBy(w => w.Ordr);

    //    Guid? sel = null;
    //    if (SelectedAttributes != null)
    //        sel = (from s in SelectedAttributes where s.ID == ga.GroupAttID select s.Value).SingleOrDefault();

    //    string view = "~/_ctrl/module/product/attribute/_viewProductAttribute.ascx";
    //    if (ga.GrType == ATTRIBUTE.Common.GrType.RANGE )
    //        view = "~/_ctrl/module/product/attribute/_viewProductAttributeSlider.ascx";

    //    return ViewManager.RenderView(view, Data, new object[] { IsModeCMS, ga, pa.ToList(), sel });
    
    //}





}
