﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewShoppingCartPopup.ascx.cs" Inherits="_ctrl_module_product_cart_viewShoppingCartPopup" %>
<%@ Register TagPrefix="SM" TagName="ShoppingCart" Src="~/_ctrl/module/product/cart/_viewShoppingCart.ascx"   %>


 <script type="text/javascript" >

     $(document).ready(function() {

         $("#cartPopup").hoverIntent({
             sensitivity: 7,
             interval: 10,
             over: function() {
                 $('.cartW').stop().fadeInShow(200);
             },
             timeout: 400,
             out: function() {
                 closeHoverCart();
             }
         });

         if ($.browser.msie && $.browser.version.substr(0, 1) < 7) {
             $('.cartW').bgiframe();
         }
     });

     function closeHoverCart() {
         $('.cartW').fadeOutHide(400);
     }

     function changeQuantity(v, t) {
         $q = $(t).parent().find('.ci_quantityS');

         var _curQ = $q.html();

         _curQ = _curQ.slice(0, - 1);


         var c = parseInt(_curQ);
         if (isNaN(c))
             c = 1;
         else
             c = c + v;
         if (c < 1)
             c = 1;

         $q.html(c + 'x');

         var _ci = $q.parent().find('.hfCartItem').val();
         
         WebServiceP.UpdateCartItemQuantity(_ci, c, em_g_cult, em_g_lang, em_g_pid, onCartUpdateSuccess, onCartUpdateError, t); 
     }


                
</script>                

    <a class="Acart" href='<%= SM.BLL.Common.ResolveUrl( SM.EM.Rewrite.OrderProductUrl(PageID,Guid.Empty, 1)) %>'><span><%= GetGlobalResourceObject("Product", "shoppingCart")%> <%--<%= SM.EM.Helpers.FormatPrice (GetTotalSum) %>--%>(<%= CartItems.Count  %>)</span></a>
    <div class="clearing"></div>
    <div  class="cartW">
        <%--<div  class="cHov cartT" ></div>--%>
        <div  class="cHov cartM" >
            <div class="cartMI">
                
                <div id="cartContainer" <%= RenderContainerVisible() %>>
                    <span class="cartMITopS"><%= GetGlobalResourceObject("Product", "shoppingCartContent")%></span><a onclick="closeHoverCart(); return false" class="AcarthovClose" href="#"></a> 
                    <div class="clearing"></div>
                        <div class="cartItemList">
                            <SM:ShoppingCart ID="objShoppingCart" runat="server" />
                        </div>
                        

                                    
                </div>
            </div>    

        </div>
        <%--<div class="cHov cartB"></div>--%>
    </div>


