﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for UrlRSqlPersonalizationProvider
/// </summary>
public class UrlRSqlPersonalizationProvider : System.Web.UI.WebControls.WebParts.SqlPersonalizationProvider
{
    protected override void LoadPersonalizationBlobs(WebPartManager webPartManager, string path, string userName, ref byte[] sharedDataBlob, ref byte[] userDataBlob)
    {
        base.LoadPersonalizationBlobs(webPartManager, this.GetActualPath(), userName, ref sharedDataBlob, ref userDataBlob);
    }

    protected override void ResetPersonalizationBlob(WebPartManager webPartManager, string path, string userName)
    {
        base.ResetPersonalizationBlob(webPartManager, this.GetActualPath(), userName);
    }

    public override int ResetState(PersonalizationScope scope, string[] paths, string[] usernames)
    {
        return base.ResetState(scope, paths, usernames);
    }

    public override int ResetUserState(string path, DateTime userInactiveSinceDate)
    {
        return base.ResetUserState(this.GetActualPath(), userInactiveSinceDate);
    }

    protected override void SavePersonalizationBlob(WebPartManager webPartManager, string path, string userName, byte[] dataBlob)
    {
        base.SavePersonalizationBlob(webPartManager, this.GetActualPath(), userName, dataBlob);
    }

    protected string GetActualPath()
    {

        HttpApplication app = HttpContext.Current.ApplicationInstance;
        string p = app.Request.RawUrl;
        
        return app.Request.QueryString.ToString();

        //// Convert Absolute to Relative
        //if (p.ToLowerInvariant().StartsWith(VirtualPathUtility.ToAbsolute("~/").ToLowerInvariant()))
        //{
        //    p = "~/" + p.Substring(VirtualPathUtility.ToAbsolute("~/").Length);
        //}

        //// Remove QueryString Parameters
        //if (p.Contains("?"))
        //    p = p.Substring(0, p.IndexOf("?"));

        //return p;


        
    }    
}
