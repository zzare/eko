﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _ctrl_module_poll_cntPollSettings : BaseControlView
{

    public object Data;
    public object[] Parameters;

    protected CMS_WEB_PART  Cwp;
    protected string  LangID;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        LoadPoll();
    }



    protected void LoadPoll() {

        if (Data == null)
            return;

        // data
        Cwp = (CMS_WEB_PART )Data;

        // load poll data
        tbTitle.Value = Cwp.TITLE ;
        cbActive.Checked = Cwp.SHOW_TITLE;
    }
}
