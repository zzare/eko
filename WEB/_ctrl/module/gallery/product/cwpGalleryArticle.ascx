﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="cwpGalleryArticle.ascx.cs" Inherits="SM.EM.UI.Controls.cwpGalleryArticle" %>
<%@ Register TagPrefix="SM" TagName="ImageList" Src="~/_ctrl/module/gallery/_viewGalleryImageList.ascx"   %>
<%@ Register TagPrefix="SM" TagName="ImageNew" Src="~/_ctrl/module/gallery/popupImageNew.ascx"   %>



<div id='<%= CwpID %>' class='<%= "cwp cwpArtGal" + (ParentPage.IsModeCMS ? " cwpEdit " : " ") + CMS_WEB_PART.Common.RenderHiddenClass(cwp) %>'>
    <div id="divEdit" runat="server" class="cwpHeader " visible="false" >
        <div class="cwpHeaderRight"> &nbsp;</div>

        <div style="float:right; ">
            <%--<a id="btNew" onserverclick ="OnNewImageClick" title="dodaj" runat="server" class="btNew"></a>--%>
            <a id="btNew" href="#" onclick="addImageLoad('<%= cwp.CWP_ID.ToString() %>', true <%= RenderCallback() %>); return false"  title="dodaj"  class="btNew"></a>
            <a id="btSettings" href="#" onclick="settingsLoad('<%= cwp.CWP_ID.ToString() %>'); return false" title="nastavitve" class="btSettings"  ></a>
            
            
            <%--<a id="btSettings" href="#" onclick="settingsGalleryLoad('<%= cwp.CWP_ID.ToString() %>'); return false" title="nastavitve" class="btSettings"  ></a>--%>
        </div>

        <div class="cwpHeaderLeft"> &nbsp;</div>
        
        <div class="cwpHeaderTitle">
            Galerija
<%--            <a href="javascript:void(0)" class="btsettStatusHelp btcwpHeadHelp"><img src='<%=Page.ResolveUrl("~/_inc/images/icon/help.png") %>' /></a>
            <div class="settStatusWrapp cwpHeadHelpWrapp" >
                <div class="settStatus cwpHeadHelp">
                    Uredi galerijo za članek
                    <br />
                    Slika pove več kot 1000 besed.
                </div>
            </div>--%>
        </div>
    </div>
    <div class="clearing"></div>
    
    <div class='<%= "cGal " + ( ParentPage.IsModeCMS ? "cwpContainer" : "" ) %>'>
    
        <%= RenderImageList() %>
      

    </div>    
</div>

<%if (ParentPage.IsModeCMS) {%>
    <SM:ImageNew ID="objImageNew" runat="server" Visible="false"/> 
<%} %>

