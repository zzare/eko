﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for BasePage
/// </summary>
namespace SM.EM.UI
{

    public class BasePage_Main : BasePage 
    {

        protected override void OnPreInit(EventArgs e)
        {
            this.EnableViewState = false;
            this.ForceMainPageDomain = true;
            
            base.OnPreInit(e);
            
        }




        public override void OutputCaching()
        {

            // do not cache
            return;

            // cache page 
            TimeSpan freshness = new TimeSpan(0, 0, 0, 30);
            Response.Cache.SetCacheability(HttpCacheability.Server);
            Response.Cache.SetExpires(DateTime.Now.Add(freshness));
            Response.Cache.SetMaxAge(freshness);
            Response.Cache.SetValidUntilExpires(true);
            //            Response.Cache.SetLastModified(this.Context.Timestamp);
            Response.Cache.VaryByParams.IgnoreParams = false;
            Response.Cache.VaryByParams["*"] = true;


        }


   }
}