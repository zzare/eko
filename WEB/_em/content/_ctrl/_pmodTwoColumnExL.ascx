﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_pmodTwoColumnExL.ascx.cs" Inherits="_em_content__ctrl_pmodTwoColumnExL" %>
<%@ Register Namespace="SM.EM.UI.Controls" TagPrefix="EM"  %>
<%@ Register TagPrefix="EM" TagName="ModuleNew" Src="~/_ctrl/emenik/emModuleNew.ascx"  %>

    <div class="cont_wideW">
        <div class="cont_wideT"></div>
        <div class="cont_wideM">
            <div class="cont_wideMI">

    <div id="contTwoColsMain">

        <EM:ModuleNew ID="mnMain" runat="server" />

        <EM:CwpZone ID="wpz_1" runat="server">
        </EM:CwpZone>
    </div>
    
            </div>    
        </div>
        <div class="cont_wideB"></div>
    </div>
