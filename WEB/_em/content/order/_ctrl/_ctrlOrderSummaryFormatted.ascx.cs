﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SM.UI.Controls;
using SM.EM.UI.Controls;


public partial class _em_content_order__ctrl__ctrlOrderSummaryFormatted : System.Web.UI.UserControl  
{

    public SHOPPING_ORDER Order;
    public Guid PageId;

    public bool IsPredracun = false;


    public List<ProductRep.Data.ShoppingCartItem> CartItems;
    protected decimal _totalSum = -1;
    public decimal GetTotalSum
    {
        get
        {
            if (_totalSum > -1)
                return _totalSum;
            _totalSum = 0;
            if (CartItems == null || !CartItems.Any())
                return _totalSum;
            _totalSum = (from c in CartItems select c.PriceTotal).Sum();

            return _totalSum;
        }
    }

    public decimal GetShipping
    {
        get
        {
            decimal shipp = SHOPPING_ORDER.GetShippingPriceByTotal(GetTotalSum);
            if (Order != null)
            {
                shipp = Order.CalculateShippingPrice(GetTotalSum);
            }
            return shipp;
        }
    }

    public decimal GetTotalSumWithShipping
    {
        get
        {
            // add shipping
            if (ShowPriceWithShipping)
            {
                decimal shipp = GetShipping;
                return GetTotalSum + shipp;
            }

            return GetTotalSum;
        }
    }
    protected decimal _getTotalTax = -1;
    public decimal GetTotalTax
    {
        get
        {
            if (_getTotalTax > -1)
                return _getTotalTax;
            _getTotalTax = 0;
            if (CartItems == null || !CartItems.Any())
                return _getTotalTax;
            _getTotalTax = (from c in CartItems select c.Tax).Sum();

            if (ShowPriceWithShipping)
            {
                decimal shipp_tax = Convert.ToDecimal((GetShipping * 0.2m));
                _getTotalTax = _getTotalTax + shipp_tax;
            }


            return _getTotalTax;
        }
    }

    protected decimal _getTotalTax_8 = -1;
    public decimal GetTotalTax_8
    {
        get
        {
            if (_getTotalTax_8 > -1)
                return _getTotalTax_8;
            _getTotalTax_8 = 0;
            if (CartItems == null || !CartItems.Any())
                return _getTotalTax_8;
            _getTotalTax_8 = (from c in CartItems where c.TaxRate == (double )0.085m select c.Tax).Sum();

            //if (ShowPriceWithShipping)
            //{
            //    decimal shipp_tax = Convert.ToDecimal((GetShipping * 0.2m));
            //    _getTotalTax_8 = _getTotalTax_8 + shipp_tax;
            //}


            return _getTotalTax_8;
        }
    }

    protected decimal _getTotalTax_20 = -1;
    public decimal GetTotalTax_20
    {
        get
        {
            if (_getTotalTax_20 > -1)
                return _getTotalTax_20;
            _getTotalTax_20 = 0;
            if (CartItems == null || !CartItems.Any())
                return _getTotalTax_20;
            _getTotalTax_20 = (from c in CartItems where c.TaxRate == (double)0.2m select c.Tax).Sum();

            if (ShowPriceWithShipping)
            {
                decimal shipp_tax = Convert.ToDecimal((GetShipping * 0.2m));
                _getTotalTax_20 = _getTotalTax_20 + shipp_tax;
            }


            return _getTotalTax_20;
        }
    }


    public bool ShowPriceWithShipping = true;


    protected void Page_Load(object sender, EventArgs e)
    { 




    }


    public void LoadData() {

        if (Order == null)
            return;

        ProductRep prep = new ProductRep();
        Guid? orderid = Order.OrderID ;
        if (Order.OrderStatus > SHOPPING_ORDER.Common.Status.NEW && Order.OrderStatus < SHOPPING_ORDER.Common.Status.STEP_CONFIRMED){
            //phStatus.Visible = false;
            orderid = null;
        }

        //objOrderDetail.Order = Order;
        //objOrderDetail.CartItems = prep.GetCartItemList(Order.UserId , LANGUAGE.GetCurrentLang(), PageId, orderid );
        //objOrderDetail.LoadData();

        CartItems= prep.GetCartItemList(Order.UserId, LANGUAGE.GetCurrentLang(), PageId, orderid);
        lvList.DataSource = CartItems;
        lvList.DataBind();
    
    }


    public string RenderPayment() {
        string ret = "";

        if (Order == null)
            return ret;

        ret = SHOPPING_ORDER.Common.RenderSHOPPING_ORDERPayment(Order) + "<br/>";
        return ret;
    }

    public string RenderShipping()
    {
        string ret = "";

        if (Order == null)
            return ret;

        ret = SHOPPING_ORDER.Common.RenderShoppingOrderShipping(Order.ShippingType) ;

        if (Order.ShippingType == SHOPPING_ORDER.Common.ShippingType.SHIPPING)
        {


            ret += "";
            ret += "Ime in priimek: " + Order.GetShippingName() + "<br/>";
            ret += "Naslov: " + Order.GetShippingAddress() + "<br/>";
            ret += "Kraj: " + Order.GetShippingPostalAndCity() + "<br/>";
            //ret += "Tel: " + Order.GetShippingPhone() + "<br/>";
        }
        ret += "<br/>";

        return ret;
    }

    public string RenderProductAttributes(object o)
    {
        string ret = "";
        string separator = " | ";
        ProductRep.Data.ShoppingCartItem cartItem = (ProductRep.Data.ShoppingCartItem)o;

        var list = cartItem.AttrList.Where(w => w.GrType != ATTRIBUTE.Common.GrType.RBL_PRICE);

        foreach (ProductRep.Data.ItemGroupAttribute attr in list)
        {
            if (!string.IsNullOrEmpty(ret))
                ret += separator;
            ret += attr.GroupTitle + "=" + attr.Title;
            //ret += attr.Title;   
        }

        return ret;
    }

    public string RenderProductAttributesPrice(object o)
    {
        string ret = "";
        string separator = "";
        ProductRep.Data.ShoppingCartItem cartItem = (ProductRep.Data.ShoppingCartItem)o;

        var list = cartItem.AttrList.Where(w => w.GrType == ATTRIBUTE.Common.GrType.RBL_PRICE);

        foreach (ProductRep.Data.ItemGroupAttribute attr in list)
        {
            if (!string.IsNullOrEmpty(ret))
                ret += separator;

            string val = attr.PTitle;


            ret += string.Format(", {0}", val);

            //ret +=  val;
            //ret += attr.Title;   
        }

        return ret;
    }
}
