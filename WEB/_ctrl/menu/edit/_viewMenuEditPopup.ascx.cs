﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _ctrl_menu_edit_viewMenuEditPopup : BaseControlView
{

    public object Data;
    public object[] Parameters;
    public SITEMAP Smap;


    protected void Page_Load(object sender, EventArgs e)
    {
        LoadData();
    }



    public void LoadData()
    {



        if (Data  == null)
            return;

        Smap = (SITEMAP)Data;

        if (Parameters != null)
        {
            //if (Parameters[0] != null)
            //    IsModeCMS = bool.Parse(Parameters[0].ToString());

            //if (Parameters.Length > 1 && Parameters[1] != null)
            //    LangID = Parameters[1].ToString();
        }


        // load edit
        objMenuEdit.Data = Data;
        objMenuEdit.Parameters = new object[] { Parameters[0], Parameters[1] };
        objMenuEdit.LoadData();


        objMenuPageModuleEdit.Data = Data;
        objMenuPageModuleEdit.Parameters = new object[] { Parameters[0], Parameters[2] };
        objMenuPageModuleEdit.LoadData();


    }






}
