﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewGalleryImageList.ascx.cs" Inherits="SM.EM.UI.Controls._viewGalleryImageList" %>
<%@ Register Namespace="SM.UI.Controls" TagPrefix="SM"  %>

<div id="divTitle"  class="cwpH1" runat="server">
    <h1><asp:Literal ID="litTitle" runat="server" ></asp:Literal></h1>
</div>


<div id="divGalleryList" class="contGal" runat="server" style="padding-top:5px;" >

        <div class="galImgList <%= RenderCustomClass() %>" >
        <asp:ListView ID="lvList" runat="server" InsertItemPosition="None"  >
            <LayoutTemplate>
                <div id="itemPlaceholder" runat="server"></div>
                    
            </LayoutTemplate>
            
            <ItemTemplate>
                <%--<div class="divGalImgThumb" style="width:108px; height:88px;float:left; margin:0px 8px 8px 0px;">--%>
                <div class="divGalImgThumb " >
                    <asp:PlaceHolder ID="phPreview" Visible='<%# !IsModeCMS %>' runat="server">
                        <a href="javascript:void(0)" class="divGalImgThumb_thumb" title='<%# Eval("IMG_TITLE")  %>' onclick='<%# GALLERY.Common.RenderOpenGalleryPopupAnimal(Cwp.CWP_ID, Container.DataItemIndex , "this") %>' ><img alt='<%# Eval("IMG_TITLE") + " | " + Eval("IMG_DESC") %>' id="Img2" src='<%# SM.BLL.Common.ResolveUrl (Eval("IMG_URL").ToString()) %>'  /></a>
                    </asp:PlaceHolder>

                    <asp:PlaceHolder ID="phEdit" Visible='<%# IsModeCMS %>' runat="server" >
                            <%--<asp:LinkButton style="position:absolute;" CssClass="divGalImgThumb_btnEdit" ToolTip="Uredi" ID="btEdit" Enabled='<%# IsModeCMS %>' CommandName="editImage" CommandArgument='<%# Eval("IMG_ID") %>' runat="server"></asp:LinkButton>--%>
                            <a href="#" onclick="editImageLoad('<%# Eval("IMG_ID") %>', '<%# Eval("TYPE_ID") %>', '<%# Cwp.CWP_ID %>'); return false" style="position:absolute;" class="divGalImgThumb_btnEdit" title="Uredi" ></a>
                            <img class="imgMove" alt='<%# Eval("IMG_TITLE") + " | " + Eval("IMG_DESC") %>'  id='<%# Eval("IMG_ID") %>'  src='<%# SM.BLL.Common.ResolveUrl( Eval("IMG_URL").ToString()) %>'  />
                            
                    </asp:PlaceHolder>                    
                </div>
            </ItemTemplate>
            
            <EmptyDataTemplate>
                <% if (ShowEmptyTemplate){%>
                <div class="galEmpty">
                    Vstavi slike.
            
                </div>
                <%} %>
                
            </EmptyDataTemplate>
            
            <ItemSeparatorTemplate>
                <%= Separator %>
            
            </ItemSeparatorTemplate>
            
        
        </asp:ListView>
    </div>


    <br />
    <div class="clearing"></div>

<% if (ShowGalleryPopup){%>
    <div class="galOpen">
        <a  href="#" onclick='<%= RenderOpenGalleryPopup() %>' ><%= GetGlobalResourceObject("Modules", "OpenGallery")%></a>
    </div>    
<%} %>
</div>