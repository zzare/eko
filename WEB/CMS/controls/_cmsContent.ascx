﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_cmsContent.ascx.cs" Inherits="SM.EM.UI.CMS.Controls.CMS_cmsContent" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="FredCK.FCKeditorV2" Namespace="FredCK.FCKeditorV2" TagPrefix="FCKeditorV2" %>
<%@ Register TagPrefix="EM" Namespace="SM.EM.UI" %>
<%@ Register Namespace="SM.UI.Controls" TagPrefix="SM" %>


<SM:smListView ID="listContent" runat="server" DataSourceID="ldsContent" 
    DataKeyNames="CON_ID" OnItemEditing="listContent_ItemEditing" 
    OnItemInserted="listContent_ItemInserted" 
    onitemupdated="listContent_ItemUpdated"
    OnItemCanceling="listContent_ItemCanceling" 
    onitemcreated="listContent_ItemCreated" >
    <LayoutTemplate>
        <table cellpadding="0" cellspacing="0" width="100%" class="tblData">
            <thead>
                <tr>
                    <th><asp:LinkButton ID="lbSort0" CommandName="sort" CommandArgument="CON_ID" runat="server">ID</asp:LinkButton></th>
                    <th><asp:LinkButton ID="lbSortLan" CommandName="sort" CommandArgument="LANG_ID" runat="server">LANG</asp:LinkButton></th>
                    <th>Body...</th>
                    <th><asp:LinkButton ID="lbSort1" CommandName="sort" CommandArgument="Active" runat="server">Active</asp:LinkButton></th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <asp:PlaceHolder ID="itemPlaceholder" runat="server"/>
            <tfoot>
                <tr>
                    <th style="text-align:left">
                        <asp:Button ID="btNew" runat="server" onclick="btNew_Click" Text="Add New" ValidationGroup="insert"></asp:Button>
                    </th>
                    <th colspan="5" style="text-align:right">
                        <%--<EM:Pager ID="dpContent" RowPerPage="3"  runat="server" SliderSize="5" ShowFirstAndLast="false" ShowPreviousAndNext="true" PreviousText="<" NextText=">"/>--%>
                        <asp:DataPager ID="pagContent" runat="server" PagedControlID="listContent" PageSize="15">
                            <Fields>
                                <asp:NumericPagerField />
                                <asp:NextPreviousPagerField />
                                
                            </Fields>
                        </asp:DataPager>
                    </th>
                </tr>
            </tfoot>            
        </table>
    </LayoutTemplate>

    <ItemTemplate>
        <tr  class='<%#  Container.DataItemIndex % 2 == 0 ? " " : " odd " %>'  onmouseover ="this.className='<%#  Container.DataItemIndex % 2 == 0 ? "hovRow" : "hovRowodd" %>'" onmouseout ="this.className = GetCssClass(<%#  Container.DataItemIndex %>)">
            <td><%#Eval("CON_ID") %></td>
            <td><%#Eval("LANG_ID") %></td>
            <td><%# SM.EM.Helpers.CutString( SM.EM.Helpers.StripHTML(Eval("CONT_BODY").ToString()), 25, " ...") %></td>
            <td><asp:CheckBox ID="cbActive" runat="server" Checked ='<%#Eval("ACTIVE") %>' Enabled="false" /> </td>
            <td><asp:LinkButton ID="lbEdit" CommandName="Edit" runat="server">Edit</asp:LinkButton>
                &nbsp;<asp:LinkButton ID="lbDelete" CommandName="Delete" runat="server">Delete</asp:LinkButton></td>
        </tr>
    </ItemTemplate>
    <EditItemTemplate>
        <tr>
            <td colspan="6">  
                <div style="border-top:#000000 solid 2px; width:100%; height:1px;"></div>
            </td> 
        </tr>
        <tr>
            <th colspan="5"></th>
            <th colspan="1" class="artEdit">
                <asp:CheckBox CssClass="floatR" ID="CheckBox1" runat="server" Checked='<%#Bind("ACTIVE") %>' Text=" Active" />
            </th>
        </tr>

        <tr>
            <td colspan="6">
                <FCKeditorV2:FCKeditor Width="100%" ID="fckBody" runat="server" Height="400px" Value='<%#Bind("CONT_BODY") %>'></FCKeditorV2:FCKeditor>
            </td>
        </tr>
        <tr>
            <td colspan="1">
                    <asp:Button ID="btSave" CommandName="Update" runat="server" Text="Save" ></asp:Button>
            </td>
            <td colspan="1">
                <asp:Button ID="btCancel" CommandName="Cancel" runat="server" Text="Cancel" ></asp:Button>
            </td>
        </tr>
        <tr>
            <td colspan="6">  
                <div style="border-bottom:#000000 solid 2px; width:100%; height:1px;"></div>
            </td> 
        </tr>
    </EditItemTemplate>
    <InsertItemTemplate>
        <tr>
            <td colspan="6">  
                <div style="border-top:#000000 solid 2px; width:100%; height:1px;"></div>
            </td> 
        </tr>
        <tr>
            <th colspan="5"></th>
            <th colspan="1" class="artEdit">
                <asp:CheckBox CssClass="floatR" ID="cbActive" runat="server" Checked='<%#Bind("ACTIVE") %>' Text=" Active" />
            </th>
        </tr>

        <tr>
            <td colspan="6">
                <%--<asp:TextBox ID="tbBody" runat="server" TextMode="MultiLine" Width="500" Rows="10" Text='<%#Bind("CONT_BODY") %>' ></asp:TextBox>--%>
                <FCKeditorV2:FCKeditor Width="100%" ID="fckBody" runat="server" Height="400px" Value='<%#Bind("CONT_BODY") %>'></FCKeditorV2:FCKeditor>
            </td>
        </tr>
        <tr>
            <td colspan="1">
                    <asp:Button ID="btSave" CommandName="Insert" runat="server" Text="Save" ></asp:Button>
            </td>
            <td colspan="1">
                <asp:Button ID="btCancel" CommandName="Cancel" runat="server" Text="Cancel" ></asp:Button>
            </td>
        </tr>
        <tr>
            <td colspan="6">  
                <div style="border-bottom:#000000 solid 2px; width:100%; height:1px;"></div>
            </td> 
        </tr>
    </InsertItemTemplate>   
    <EmptyDataTemplate>
    no data yet.
        <asp:Button ID="btNew" runat="server" onclick="btNew_Click" Text="Add New" ValidationGroup="insert"></asp:Button>
    </EmptyDataTemplate>

</SM:smListView>

<asp:LinqDataSource ID="ldsContent" runat="server" 
    ContextTypeName="eMenikDataContext" EnableDelete="True" 
    EnableUpdate="True" EnableInsert="True"
    onselecting="ldsContent_Selecting" OnInserting="ldsContent_Inserting" 
    TableName="CONTENTs" >
</asp:LinqDataSource>
<br />
