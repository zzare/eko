﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _ctrl_module_product_attribute_viewAttributeDescEdit : BaseControlView 
{
    //public object Data;
    public ProductRep.Data.wProductAttributeDesc ProdAttr;

    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!IsPostBack)
            fckBody.Config["FloatingPanelsZIndex"] = "200000";
            fckBody.Config["EnterMode"] = "br";

            if (Data == null)
                return;
            ProdAttr = (ProductRep.Data.wProductAttributeDesc)Data;

            fckBody.Value = ProdAttr.Desc ?? "";

    }




}
