﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace SM.UI.CMS.EM
{
    public partial class CMS_ManageModule : SM.EM.UI.CMS.BasePage_CMS 
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void ldsList_OnSelecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            eMenikDataContext db = new eMenikDataContext();

            var list = from l in db.MODULEs  orderby l.MOD_TITLE   ascending select l;

            //if (_langID != "-1") contents = contents.Where(c => c.LANG_ID == _langID);

            e.Result = list;
        }


        protected void ldsList_Inserting(object sender, LinqDataSourceInsertEventArgs e)
        {
            // set logo and header type
            MODULE c = (MODULE)e.NewObject;

            c.MOD_ORDER = 100;

            if (c.EM_BUY_TYPE == null)
                c.EM_BUY_TYPE = 1;
            if (c.EM_BUY_QUANTITY == null)
                c.EM_BUY_QUANTITY = 1;


        }
        protected void lvList_ItemEditing(object sender, ListViewEditEventArgs e)
        {
            // hide insert item
            lvList.InsertItemPosition = InsertItemPosition.None;
        }
        protected void lvList_ItemUpdated(object sender, ListViewUpdatedEventArgs e)
        {
            // show insert item
            lvList.InsertItemPosition = InsertItemPosition.LastItem;

        }
        protected void lvList_ItemCanceling(object sender, ListViewCancelEventArgs e)
        {
            // show insert item
            lvList.InsertItemPosition = InsertItemPosition.LastItem;

        }
#region Validation
        protected void ValidateDuplicates(object sender, ServerValidateEventArgs e)
        {


                e.IsValid = true;
        }


#endregion

        protected void ldsList_Updating(object sender, LinqDataSourceUpdateEventArgs e)
        {
            // set header type
            

            //MASTER c = (MASTER)e.NewObject;
            //int imgHeader = Int32.Parse(((DropDownList)lvList.Items[lvList.EditIndex].FindControl("ddlHeader")).SelectedValue);
            //if (imgHeader > 0)
            //{
            //    c.HDR_TYPE_ID = imgHeader;
            //    c.HAS_HEADER_IMAGE = true;
            //}
            //else
            //    c.HAS_HEADER_IMAGE = false;

        }
        protected void lvList_ItemDataBound(object sender, ListViewItemEventArgs e)
        {

        }
}
}
