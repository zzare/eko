﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewAttributeDescEdit.ascx.cs" Inherits="_ctrl_module_product_attribute_viewAttributeDescEdit" %>
<%@ Register Assembly="FredCK.FCKeditorV2" Namespace="FredCK.FCKeditorV2" TagPrefix="FCKeditorV2" %>
<%@ Register Namespace="SM.UI.Controls" TagPrefix="SM"  %>

<script type="text/javascript">

    function editProductAttributeSaveDesc(p, a, l) {
        var d = '';

        var oEditor = FCKeditorAPI.GetInstance('<%= fckBody.ClientID %>');
        if (oEditor) { d = oEditor.GetXHTML(true); }

        var t = $('#prodAttrDescEditTitle').val();

        openModalLoading('Shranjujem...');
        WebServiceCWP.SaveProductAttributeDesc(p, a, l, t, d, onProductAttributeDescSaveSuccess, onContentSaveError);
    }

    function onProductAttributeDescSaveSuccess(ret) {
        if (ret.Code != 1) {
            setModalLoaderErr(ret.Message, true);
            return;
        }
        closeModalLoader();
        closeModalLoading();
        //openModalLoading('Osvežujem...');

        $('#' + ret.Data.AttributeID).find('span.ap_rbt_desc').html(ret.Data.Desc)
        $('#' + ret.Data.AttributeID).find('span.ap_rbt_title').html(ret.Data.Title);
        //window.location.reload();
        //.each(function () { formatObjectEmbed(this); });
        //.find("embed").attr("wmode", "transparent").each(function() { var cl = $(this).clone(); $(this).replaceWith(cl); });

    }

</script>

<div class="">

    <div class="modPopup_header modPopup_headerEdit">
        <h4>UREDI OPIS ATRIBUTA</h4>
        <p></p>
    </div>

<%--    <div class="modPopup_main">
              

    </div>--%>

    <div class="modPopup_mainFCK">

    <span class="modPopup_mainTitle">Uredi NAZIV</span> 
    
        <input id="prodAttrDescEditTitle" class="modPopup_TBXsettings" style="width:95%;"  type="text" value='<%= ProdAttr.Title  %>' />
          
     <span class="modPopup_mainTitle">Uredi OPIS</span> 
    
        <FCKeditorV2:FCKeditor  Width="100%" ID="fckBody" ToolbarSet="CMS_Content" runat="server" Height="400px"  ></FCKeditorV2:FCKeditor>
    </div>   
    <div class="modPopup_footer"> 
        <div class="buttonwrapperModal">
            <a class="lbutton lbuttonConfirm" href="#" onclick="editProductAttributeSaveDesc('<%= ProdAttr.PRODUCT_ID  %>', '<%= ProdAttr.AttributeID  %>', em_g_lang); return false"><span>Shrani</span></a>
            <a class="lbutton lbuttonDelete" href="#" onclick="closeModalLoader(); return false" ><span>Prekliči</span></a>
        </div>
    </div>
</div>
