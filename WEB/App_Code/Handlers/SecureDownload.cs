﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
/// <summary>
/// Summary description for SecureDownloadHandler
/// </summary>
public class SecureDownloadHandler : IHttpHandler
{


    public bool IsReusable
    {
        get { return true; }
    }


    public void ProcessRequest(HttpContext context)
    {
            // We need to get the file name that the user is requesting.
            string requestedFile = context.Request.Url.Segments[context.Request.Url.Segments.Length - 1];
            // If the user has permission, send the file, else throw an exception
                // Get the file path
                Guid id = new Guid(requestedFile.Substring(0, requestedFile.LastIndexOf(".")));

                FILE f = FILE.GetFileByID(id);
                string file = f.Folder  + requestedFile;


                if (f == null)
                    throw new Exception("Datoteka ne obstaja");

                if (HasPermission(context, f.RolesDownload, f ))
                {

                }
                else
                {
                    context.Response.Redirect("~/Prepovedano.aspx");
                }


                // Set headers
                context.Response.ContentType = f.Type ;
                context.Response.AddHeader("Content-Disposition", "inline; filename=\"" + f.Name + "\"");

                // Send the file using transmit, this way it's not read into memory. We can send up to a 2 gig file.
                context.Response.TransmitFile(file);
                // Increment the file counter
                //IncrementFileCounter(requestedFile, context.User.Identity.Name);
    }


    private bool HasPermission(HttpContext context, string roles, FILE f)
    {
        // annnimous
        if (roles.Contains("?"))
            return true;

        if (!context.User.Identity.IsAuthenticated)
            return false;


        if (context.User.Identity.IsAuthenticated && roles.Contains("*")) return true;

        if (context.User.IsInRole("admin") ) return true;
        

        foreach (string role in roles.Split(",".ToCharArray()))
        {
            if (context.User.IsInRole(role)) return true;
        }

        // check if user is owner
        Guid  userID = SM.EM.BLL.Membership.GetCurrentUserID();
        if (userID != null)
        {
            if (f.UserId == userID)
                return true;
        }

        return false;

    }



}

