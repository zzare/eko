﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SM.UI.Controls;
using System.Linq.Dynamic;
using LinqKit;

public partial class _em_content_product__ctrl__pmodProductListNoFilter : BaseControl_Pager 
{
    public ProductRep Prep;
    public bool EnableFilters = true;

    public string GetCurrentFilterQS {
        get { return ""; }
    
    }

    //public override string RenderCurrentFilterQSWithout(string fid)
    //{
    //    string ret = "";

    //    if (fid != objFiltManufact.filtQS)
    //        ret += objFiltManufact.GetCurrentFilterQS;

    //    return ret;
    //}
    
    

    public override string GetOrderColumnByID(int id) {
        string ret = "NAME";

        if (id == 1)
            ret = "NAME";
        else if (id == 2)
            ret = "PRICE";
        else if (id == 3)
            ret = "AvgRating";
        else if (id == 4)
            ret = "DISCOUNT_PERCENT";
        return ret;
    }


    public override  bool GetDefaultDirectionByID(int id)
    {
        // true = ascending
        bool ret = true;

        if (id == 1)
            ret = true;
        else if (id == 2)
            ret = true;
        else if (id == 3)
            ret = false;
        else if (id == 4)
            ret = false;

        return ret;
    }

    public bool EnableSorting = true;

    protected IQueryable<vw_Product> _productList;
    public   IQueryable<vw_Product> ProductList {
        get {

            if (_productList != null)
                return _productList;

//            ProductRep prep = new ProductRep();
            bool? active = true;
            if (ParentPage.IsModeCMS)
                active = null;

            // tmp
            if (ParentPage.PageModule == PAGE_MODULE.Common.PRODUCT_ADDITIONAL )
                _productList = Prep.GetProductDescAdditionalList(CurrentSitemapID, LANGUAGE.GetCurrentLang(), active, ParentPage.em_user.UserId);
            else
                _productList = Prep.GetProductDescList(CurrentSitemapID, LANGUAGE.GetCurrentLang(), active, ParentPage.em_user.UserId);
            return _productList;
        
        }

        set { _productList = value; }
    }



    protected override void OnInit(EventArgs e)
    {
        PageSize = PRODUCT.Common.PageSizeDefault();
        Prep = new ProductRep();

        base.OnInit(e);

    }


    protected void Page_Load(object sender, EventArgs e)
    {

        mnMain.CurrentWebPartZone = "wpz_1";

        //set total count
        TotalCount = ProductList.Count();

        // load list
        objProductList.Data = ProductList.OrderBy(RenderCurrentSortSQL()).Skip( PageSize*(GetCurrentPage() -1)  ).Take(PageSize );
        objProductList.LoadList();


        // set relcanonical (if default SORT and DIRECTION and not first page)
        if (GetCurrentOrder() == 1 &&  GetCurrentDirection() == GetDefaultDirectionByID(1) && GetCurrentPage() > 1 ) {
            ParentPage.RelCanonical = RenderSortPageFullUrl(GetCurrentOrder(), GetCurrentPage(), false, "");
        
        }


    }

    protected override void OnPreRender(EventArgs e)
    {
        divSorting.Visible = EnableSorting;

        base.OnPreRender(e);
    }





}
