﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class cwpGallerySettings : SM.EM.UI.Controls.CwpEdit  
{
    public CWP_GALLERY CwpGallery;


    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public override void LoadData()
    {
        // load data
        tbTitle.Text  = cwp.TITLE;
        tbPageSize.Text = cwp.PAGE_SIZE.ToString();
        cbShowTitle.Checked = cwp.SHOW_TITLE;


        CwpGallery = CMS_WEB_PART.GetCwpGalleryByID(cwp.CWP_ID);

        // bind types
        if (cwp.ID_INT != null)
        {

            lvType.DataSource = GALLERY.GetImageTypesFromGalleryByType(cwp.ID_INT.Value , IMAGE_TYPE.Common.Type.THUMB_PUBLIC);
            lvType.DataBind();
        }
 


    }

    public bool SaveData() {

        eMenikDataContext db = new eMenikDataContext();
        CMS_WEB_PART c = db.CMS_WEB_PARTs.SingleOrDefault(w => w.CWP_ID == CwpID);

//        c.TITLE = Server.HtmlEncode(tbTitle.Text);
        c.TITLE = tbTitle.Text;
        int page = 1;
        if (int.TryParse(tbPageSize.Text, out page))
        {
            if (page < 1) page = 1;
            c.PAGE_SIZE = page;
        }
        c.SHOW_TITLE = cbShowTitle.Checked;


        //CWP_GALLERY cwpg = db.CWP_GALLERies.SingleOrDefault(w=>w.CWP_ID == CwpID );
        //if (cwpg != null){
        //    foreach (ListViewDataItem item in lvType.Items)
        //    {
        //        HtmlInput rbType = item.FindControl("rbType");
        //        if (rbType.Checked)
        //            cwpg.THUMB_TYPE_ID = new Guid(rbType.Value);

        //    }
        //}



        db.SubmitChanges();

        return true;
    }

    protected void btSaveEdit_Click(object sender, EventArgs e)
    {
        SaveData();
    }



    protected string RenderChecked(object d)
    {
        string ret = "";
        if (cwp == null)
            return ret;
        if (CwpGallery  == null)
            return ret;

        IMAGE_TYPE  item = (IMAGE_TYPE )d;

        if (item.TYPE_ID   == CwpGallery.THUMB_TYPE_ID )
            return "checked";

        return ret;

    }


}
