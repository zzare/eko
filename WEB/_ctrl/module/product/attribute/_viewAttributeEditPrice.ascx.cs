﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _ctrl_module_product_attribute_viewAttributeEditPrice : BaseControlView
{

    public object Data;
    public object[] Parameters;
    public Guid ProductID;
    public GROUP_ATTRIBUTE GroupAttribute;
    public List<ATTRIBUTE> AttributeList;
//    public List<PRODUCT_ATTRIBUTE> ProductAttributeList;
    public List<ProductRep.Data.wProductAttributeDesc > ProductAttributeList;
    public string Lang;


    protected void Page_Load(object sender, EventArgs e)
    {
        LoadData();
    }



    public void LoadData()
    {
        //ProductRep arep = new ProductRep();


        ////tmp
        //eMenikDataContext db = new eMenikDataContext();
        //var list = from p in db.PRODUCTs
        //           where !(from cwp in db.CMS_WEB_PARTs where cwp.REF_ID == p.PRODUCT_ID select 1).Any()

        //           select p;
        //foreach (var item in list)
        //{
        //    CMS_WEB_PART wp = CMS_WEB_PART.CreateNewCwpGallery(SM.BLL.Common.GalleryType.DefaultProductGallery, item.PRODUCT_ID, null, MODULE.Common.GalleryModID, PRODUCT.Common.MAIN_GALLERY_ZONE, item.PageId, "Galerija izdelka", "", true, false, -1, false, false, 3, SM.BLL.Common.ImageType.EmenikGalleryThumbDefault, SM.BLL.Common.ImageType.EmenikBigDefault);
        //}









        if (Data == null)
        {
            return;
        }
        ProductID = (Guid)Data;

        if (Parameters != null)
        {
            if (Parameters.Length > 0 && Parameters[0] != null)
                IsModeCMS = bool.Parse(Parameters[0].ToString());
            if (Parameters.Length > 1 && Parameters[1] != null)
                GroupAttribute = (GROUP_ATTRIBUTE)Parameters[1];
            if (Parameters.Length > 2 && Parameters[2] != null)
                AttributeList = (List<ATTRIBUTE>)Parameters[2];
            if (Parameters.Length > 3 && Parameters[3] != null)
                ProductAttributeList = (List<ProductRep.Data.wProductAttributeDesc>)Parameters[3];

        }
        // hide if not set
        if (GroupAttribute == null || AttributeList == null)
        {
            this.Visible = false;
            return;
        }

        // tmp
        Lang = LANGUAGE.GetCurrentLang();

        // manual load
        if (ProductAttributeList == null) { 
            ProductRep prep = new ProductRep();

            //ProductAttributeList = prep.GetProductAttributesByProduct(ProductID, GroupAttribute.GroupAttID).ToList();
            ProductAttributeList = prep.GetProductAttributesWithDescByProduct(ProductID, GroupAttribute.GroupAttID, Lang).ToList();
        }

        // bind data
        lvList.DataSource = AttributeList;
        //lvList.DataSource = ProductAttributeList ;
        lvList.DataBind();


    }


    protected string RenderChecked(object o)
    {
        string ret = "";

        if (ProductAttributeList == null)
            return ret;

        ATTRIBUTE ga = (ATTRIBUTE)o;

        if (ProductAttributeList.Exists(w => w.AttributeID  == ga.AttributeID))
            ret = " checked ";

        return ret;
    }

    protected PRODUCT_ATTRIBUTE GetAttribute(object o)
    {
        PRODUCT_ATTRIBUTE ret = new PRODUCT_ATTRIBUTE { };

        if (ProductAttributeList == null)
            return ret;
        ATTRIBUTE ga = (ATTRIBUTE)o;

        var a = ProductAttributeList.Where(w => w.AttributeID == ga.AttributeID).SingleOrDefault() ?? ret;
        
        return a;
    }



    protected ProductRep.Data.wProductAttributeDesc  GetAttributeDesc(object o)
    {
        ProductRep.Data.wProductAttributeDesc ret = new ProductRep.Data.wProductAttributeDesc ();

        if (ProductAttributeList == null)
            return ret;
        ATTRIBUTE ga = (ATTRIBUTE)o;

        var a = ProductAttributeList.Where(w => w.AttributeID == ga.AttributeID).SingleOrDefault() ?? ret;

        return a;
    }

}
