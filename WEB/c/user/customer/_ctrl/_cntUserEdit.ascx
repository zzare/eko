﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_cntUserEdit.ascx.cs" Inherits="SM.UI.Controls._customer_cntUserEdit" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<div class = "qContEdit clearfix">
        
    <asp:Button ValidationGroup="usredit" ID="btSave" runat="server" Text="Shrani" onclick="btSave_Click" CssClass="btnOrange " />

    <asp:Button ID="btCancel" runat="server" Text="Zapri" onclick="btCancel_Click" CssClass="btnRed"/>

<%--    <asp:Button Visible="false" ID="btDeleteUser" runat="server" Text="Delete user" OnClientClick="javascript: return confirm('Ali res želiš zbrisati uporabnika????')" onclick="btDelete_Click" CssClass="btnRed"/>
--%>
    <br />

    
    <asp:Panel ID="panEdit" runat="server" Visible="false" CssClass="floatR">
        <div style="line-height:150%;">
    
        <asp:CheckBox Enabled="false" ID="cbActive" runat="server" Text="Aktiven (ali je potrdil svoj email naslov)" TextAlign="Left" />
        <br />


<%--        PageName: <asp:TextBox ID="tbPageName" Enabled="true"  runat="server"></asp:TextBox>
        <br />--%>



     <%--   Promocija: <asp:Literal ID="litBonus" runat="server"></asp:Literal>--%>
                
   
<%--        <asp:CheckBoxList ID="cblRoles" runat="server">
        </asp:CheckBoxList>--%>
        </div>
        
        
        
    </asp:Panel>
    


    
    Uporabnik: <h1><asp:Literal ID="tbUserName" runat="server"></asp:Literal></h1>
    <br />
    Ime: <asp:TextBox  ID="tbName"   runat="server"></asp:TextBox>
    <br />    
    Priimek: <asp:TextBox  ID="tbLastName" runat="server"></asp:TextBox>
    <br />    
    Email: <asp:TextBox  ID="tbEmail" ReadOnly="true"  runat="server"></asp:TextBox>
    <br />    
    
    <div id="divChangePassword" runat="server">
    Spremeni geslo:
        <asp:TextBox ID="tbChangePassWord" runat="server"></asp:TextBox>
         <asp:RegularExpressionValidator ID="valPasswordLength" runat="server" ControlToValidate="tbChangePassWord" SetFocusOnError="true" Display="Dynamic"
            ValidationExpression=".{5,}" ErrorMessage="* Dolžina gesla mora biti najmanj 5 znakov." ToolTip="Dolžina gesla mora biti najmanj 5 znakov."
            ValidationGroup="usredit"></asp:RegularExpressionValidator>
    
    </div>

    
</div>

