﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography;
using System.Globalization;

namespace SM.UI.Controls
{
    public partial class shopping_order_cntOrderEdit : BaseControl 
    {
        public Guid PageID { get; set; }
        protected SHOPPING_ORDER Order;


        public event EventHandler OnUserChanged;
        public event EventHandler OnCancel;

        public Guid OrderID {
            get
            {
                Guid res = Guid.Empty;
                if (ViewState["OrderID"] == null) return res;
                if (!SM.EM.Helpers.IsGUID(ViewState["OrderID"].ToString())) return res;

                return new Guid(ViewState["OrderID"].ToString());
            }
            set{
                ViewState["OrderID"] = value;
            }
        }


        protected short NewStatus;

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {

            }

        }

        protected override void OnPreRender(EventArgs e)
        {
            if (Order == null)
                Order  = SHOPPING_ORDER.GetShoppingOrderByIDByPage(OrderID, PageID);

            base.OnPreRender(e);
        }

        // bind data
        public void BindData() {
            this.Visible = true;


            if (OrderID == Guid.Empty)
            {
                //LoadDefaultValues();
            }
            else {
                LoadData();
            
            }
        }

        // LOAD Question
        protected void LoadData() {

            SHOPPING_ORDER ord = SHOPPING_ORDER.GetShoppingOrderByIDByPage(OrderID, PageID );
            if (ord == null)
                return;
            Order = ord;
            tbOrderRefID.Text = SHOPPING_ORDER.Common.RenderShoppingOrderNumber(ord);
            litTotal.Text =  SM.EM.Helpers.FormatPrice(ord.TotalWithTax) + " ( " +   SM.EM.Helpers.FormatPrice(ord.Total) + " +ddv )";
            litDateStart.Text = SM.EM.Helpers.FormatDateMedium(ord.DateStart);
            //litPaymentType.Text = SHOPPING_ORDER.Common.RenderSHOPPING_ORDERPayment(ord.PaymentType.Value );

            lnkUser.HRef = Page.ResolveUrl(CUSTOMER.Common.RenderBackendDetailsHref (ord.UserId ));
            CustomerRep crep = new CustomerRep();
            CUSTOMER cus = crep.GetCustomerByID(ord.UserId);
            if (cus != null)
            {
                lnkUser.InnerHtml = cus.USERNAME;
            }

//            lnkTicket.HRef = TICKET.Common.GetTicketDetailsCMSHref (ord.TicketID.Value );

            // status
            litStatus.Text = SHOPPING_ORDER.Common.RenderOrderStatus(ord.OrderStatus);

            // order detail
            ProductRep prep = new ProductRep();
            List<ProductRep.Data.ShoppingCartItem> list = prep.GetCartItemList(ord.UserId , LANGUAGE.GetCurrentLang(), ord.PageId , ord.OrderID);
            objOrderDetail.Order = Order;
            objOrderDetail.CartItems = list;
            objOrderDetail.LoadData();


            phCancel.Visible = true;
            phSetPayed.Visible = false;
            phSetFinished.Visible = false;
            phSetPayedOnline.Visible = false;
            phSetPayedBack.Visible = false;
            phDeliverySend.Visible = false;
            phDelivery.Visible = false;

            rfvDatePayed.Enabled = false;
            //rfvDatePayedOnline.Enabled = false;

            if (ord.OrderStatus == SHOPPING_ORDER.Common.Status.CONFIRMED)
            {

                // post-payed
                if (ord.PaymentType == SHOPPING_ORDER.Common.PaymentType.DELIVERY)
                {
                    phDeliverySend.Visible = true;
                }
                else
                {
                    // pre-payed
                    phSetPayed.Visible = true;
                    rfvDatePayed.Enabled = true;
                }

                // enable cancel
                phCancel.Visible = true;
            }
            else if (ord.OrderStatus == SHOPPING_ORDER.Common.Status.ONLINE_PAYMENT_RESERVED )
            {
              //  tbDatePayedOnline.Text = DateTime.Now.Date.ToShortDateString();


                phSetPayedOnline.Visible = true;
                //rfvDatePayedOnline.Enabled = true;
                phCancel.Visible = true;
            }
            else if (ord.OrderStatus == SHOPPING_ORDER.Common.Status.SENT_DELIVERY )
            {
                phDelivery.Visible = true;
                //rfvDatePayedOnline.Enabled = true;
                phCancel.Visible = true;
            }

            else if (ord.OrderStatus == SHOPPING_ORDER.Common.Status.PAYED)
            {
//                phSetPayed.Visible = false;
//                rfvDatePayed.Enabled = false;
                phSetFinished.Visible = true;
                //btSetPayedBack.Visible = false;
                phCancel.Visible = false;
            }
            else if (ord.OrderStatus == SHOPPING_ORDER.Common.Status.FINISHED)
            {
//                phSetPayed.Visible = false;
//                rfvDatePayed.Enabled = false;
//                btSetPayedBack.Visible = true;

                // payed by delivery
                if (ord.PaymentType == SHOPPING_ORDER.Common.PaymentType.DELIVERY)
                { 
                }
                else
                // pre payed
                {
                    phSetPayedBack.Visible = true;
                }

                
                phCancel.Visible = false;

            }
            else if (ord.OrderStatus == SHOPPING_ORDER.Common.Status.CANCELED)
            {
//                phSetPayed.Visible = false;
//                rfvDatePayed.Enabled = false;
//                btSetPayedBack.Visible = false;
                phCancel.Visible = false;

            }


            panEdit.Visible = true;
        }

        public void Save() {
            


            
            eMenikDataContext db = new eMenikDataContext();

            SHOPPING_ORDER ord = db.SHOPPING_ORDERs.SingleOrDefault(w => w.OrderID == OrderID);


            // status
            // user has payed
            if (ord.OrderStatus == SHOPPING_ORDER.Common.Status.CONFIRMED && NewStatus == SHOPPING_ORDER.Common.Status.PAYED)
            { 
                ord.DatePayed = DateTime.Parse( tbDatePayed.Text);
                ord.Payed = true;


            } // shippment is sent
            else if (ord.OrderStatus == SHOPPING_ORDER.Common.Status.CONFIRMED && NewStatus == SHOPPING_ORDER.Common.Status.SENT_DELIVERY )
            {
                // send mail to user
                OrderRep orep = new OrderRep();
                CustomerRep crep = new CustomerRep();

                orep.SendMailUserOrderCompleted(EM_USER.GetUserByID(ord.PageId.Value), crep.GetCustomerByID(ord.UserId), ord);
                ord.ShippedDate = DateTime.Now;
            }
                // back: order is not finished
            else if (ord.OrderStatus == SHOPPING_ORDER.Common.Status.FINISHED && NewStatus == SHOPPING_ORDER.Common.Status.PAYED)
            { 
                // back to 
                ord.DateFinished = null;

            } // order is finished
            else if (ord.OrderStatus == SHOPPING_ORDER.Common.Status.PAYED && NewStatus == SHOPPING_ORDER.Common.Status.FINISHED)
            {
                // send mail to user
                OrderRep orep = new OrderRep();
                CustomerRep crep = new CustomerRep();

                orep.SendMailUserOrderCompleted(EM_USER.GetUserByID(ord.PageId.Value), crep.GetCustomerByID(ord.UserId), ord);

                ord.DateFinished = DateTime.Parse(tbDateFinished.Text);
                ord.ShippedDate = DateTime.Now;

            }// order sent by DELIVERY is finished
            else if (ord.OrderStatus == SHOPPING_ORDER.Common.Status.SENT_DELIVERY  && NewStatus == SHOPPING_ORDER.Common.Status.FINISHED)
            {
                // set payed
                ord.DatePayed = DateTime.Parse(tbDateDeliveryFinished.Text);
                ord.Payed = true;

                ord.DateFinished = DateTime.Now;

            }// CANCEL: order was placed, but is canceled
            else if (ord.OrderStatus == SHOPPING_ORDER.Common.Status.CONFIRMED && NewStatus == SHOPPING_ORDER.Common.Status.CANCELED)
            {
                // reupdate stock
                ProductRep prep = new ProductRep();
                // get items
                var items = prep.GetCartItemList(ord.UserId, LANGUAGE.GetDefaultLang(), ord.PageId, ord.OrderID );
                // update stock
                var products = prep.GetProductsByOrder(ord.OrderID).Distinct();
                foreach (var item in products)
                {
                    var quantity = items.Where(p => p.ProductID == item.PRODUCT_ID).Sum(s => s.Quantity);
                    item.UNITS_IN_STOCK += quantity;
                }
                // save new stock
                prep.Save();

            
            }// CANCEL: order was ONLINE payed, but is CANCELED
            else if (ord.OrderStatus == SHOPPING_ORDER.Common.Status.ONLINE_PAYMENT_RESERVED  && NewStatus == SHOPPING_ORDER.Common.Status.CANCELED)
            {

                if (ord.PaymentType == SHOPPING_ORDER.Common.PaymentType.ONLINE_e24)
                {
                    OnlineE24PipeRep olr = new OnlineE24PipeRep();
                    // get trans
                    E24_TRAN trans = olr.GetTransByID(ord.TransIDe24.Value);

                    if (trans == null)
                    {
                        // todo: set error text
                        if (Page is SM.EM.UI.BasePage_USER_EMENIK)
                        {
                            SM.EM.UI.BasePage_USER_EMENIK ppuem = (SM.EM.UI.BasePage_USER_EMENIK)Page;
                            ppuem.SetStatus("Napaka. Transakcija ne obstaja.");
                        }

                        return;
                    }


                    e24PaymentPipeLib.e24PaymentPipeCtlClass pipe = new e24PaymentPipeLib.e24PaymentPipeCtlClass();
                    pipe.Action = OnlineE24Payment.Common.Action.VOID ;
                    pipe.Currency = trans.Currency; // Euro
                    pipe.Amt = String.Format(CultureInfo.GetCultureInfo("en-US").NumberFormat, "{0:f2}", trans.Amount);
                    pipe.Language = "SLO";
                    pipe.ResponseUrl = trans.UpdateURL;  //String.Format("http://{0}/TransactionNotification.aspx?result=Success", Request.Url.Authority);
                    pipe.ErrorUrl = trans.ErrorURL; //String.Format("http://{0}/TransactionNotification.aspx?result=Cancel", Request.Url.Authority);

                    // store
                    pipe.WebAddress = OnlineE24Payment.WebAddress();// "test4.constriv.com";
                    pipe.PortStr = OnlineE24Payment.Port();// "443";
                    pipe.Context = OnlineE24Payment.Context(); //"/cg"; //"/cg301";
                    pipe.ID = OnlineE24Payment.ID();// "89025555";
                    pipe.Password = OnlineE24Payment.Pass();// "test";

                    // id
                    pipe.PaymentId = trans.PaymentID;
                    pipe.TrackId = trans.TrackID;
                    pipe.TransId = trans.TranID;
                    pipe.Udf1 = trans.TransID.ToString();
                    pipe.Udf2 = trans.RefID.ToString();

                    Int16 result = pipe.PerformTransaction();
                    var RawResponse = pipe.RawResponse;
                    var ErrorMsg = pipe.ErrorMsg;

                    if (result != 0)
                    { // todo: set error text
                        if (Page is SM.EM.UI.BasePage_USER_EMENIK)
                        {
                            SM.EM.UI.BasePage_USER_EMENIK ppuem = (SM.EM.UI.BasePage_USER_EMENIK)Page;
                            ppuem.SetStatus("Napaka. Napaka pri procesiranju, stornacija ni uspela. " + ErrorMsg);
                        }

                        return;
                    }
                    else
                    {
                        if (Page is SM.EM.UI.BasePage_USER_EMENIK)
                        {
                            SM.EM.UI.BasePage_USER_EMENIK ppuem = (SM.EM.UI.BasePage_USER_EMENIK)Page;
                            ppuem.SetStatus("Stornacija je uspela!");
                        }
                    }

                }
                else
                {
                    // mega pos service
                    si.megapos.service.MegaposProcessorService service = new si.megapos.service.MegaposProcessorService();

                    X509Certificate cert = X509Certificate.CreateFromCertFile(MegaPos.CertificatePath());
                    service.ClientCertificates.Add(cert);

                    // transaction
                    MegaPosRep mpr = new MegaPosRep();

                    if (ord.TransID != null)
                    {
                        MP_TRAN trans = mpr.GetTransByID(ord.TransID.Value);

                        // cancel
                        si.megapos.service.TxOperationResult initTxResult = service.cancelTx(MegaPos.Store(), trans.TxId);

                        if (initTxResult.success != true)
                        {
                            // todo: set error text
                            if (Page is SM.EM.UI.BasePage_USER_EMENIK)
                            {
                                SM.EM.UI.BasePage_USER_EMENIK ppuem = (SM.EM.UI.BasePage_USER_EMENIK)Page;
                                ppuem.SetStatus("Napaka pri preklicu");
                            }

                            return;
                        }


                    }
                }

                // reupdate stock
                ProductRep prep = new ProductRep();
                // get items
                var items = prep.GetCartItemList(ord.UserId, LANGUAGE.GetDefaultLang(), ord.PageId, ord.OrderID );
                // update stock
                var products = prep.GetProductsByOrder(ord.OrderID).Distinct();
                foreach (var item in products)
                {
                    var quantity = items.Where(p => p.ProductID == item.PRODUCT_ID).Sum(s => s.Quantity);
                    item.UNITS_IN_STOCK += quantity;
                }
                // save new stock
                prep.Save();

            }
            else // user has payed  -ONLINE -> transfer money
                if (ord.OrderStatus == SHOPPING_ORDER.Common.Status.ONLINE_PAYMENT_RESERVED && NewStatus == SHOPPING_ORDER.Common.Status.PAYED)
                {
                    ord.DatePayed = DateTime.Now ;
                    ord.Payed = true;

                    if (ord.PaymentType == SHOPPING_ORDER.Common.PaymentType.ONLINE_e24)
                    {
                        OnlineE24PipeRep olr = new OnlineE24PipeRep();
                        // get trans
                        E24_TRAN trans = olr.GetTransByID(ord.TransIDe24.Value);

                        if (trans == null)
                        {
                            // todo: set error text
                            if (Page is SM.EM.UI.BasePage_USER_EMENIK)
                            {
                                SM.EM.UI.BasePage_USER_EMENIK ppuem = (SM.EM.UI.BasePage_USER_EMENIK)Page;
                                ppuem.SetStatus("Napaka. Transakcija ne obstaja.");
                            }

                            return;
                        }


                        e24PaymentPipeLib.e24PaymentPipeCtlClass pipe = new e24PaymentPipeLib.e24PaymentPipeCtlClass();
                        pipe.Action = OnlineE24Payment.Common.Action.CAPTURE ;
                        pipe.Currency = trans.Currency; // Euro
                        pipe.Amt = String.Format(CultureInfo.GetCultureInfo("en-US").NumberFormat, "{0:f2}", trans.Amount);
                        pipe.Language = "SLO";
                        pipe.ResponseUrl = trans.UpdateURL;  //String.Format("http://{0}/TransactionNotification.aspx?result=Success", Request.Url.Authority);
                        pipe.ErrorUrl = trans.ErrorURL; //String.Format("http://{0}/TransactionNotification.aspx?result=Cancel", Request.Url.Authority);

                        // store
                        pipe.WebAddress = OnlineE24Payment.WebAddress();// "test4.constriv.com";
                        pipe.PortStr = OnlineE24Payment.Port();// "443";
                        pipe.Context = OnlineE24Payment.Context(); //"/cg"; //"/cg301";
                        pipe.ID = OnlineE24Payment.ID();// "89025555";
                        pipe.Password = OnlineE24Payment.Pass();// "test";

                        // id
                        pipe.PaymentId = trans.PaymentID ;
                        pipe.TrackId = trans.TrackID;
                        pipe.TransId = trans.TranID;
                        pipe.Udf1 = trans.TransID.ToString();
                        pipe.Udf2 = trans.RefID.ToString();

                        Int16 result = pipe.PerformTransaction();
                        var RawResponse = pipe.RawResponse;
                        var ErrorMsg = pipe.ErrorMsg;

                        if (result != 0)
                        { // todo: set error text
                            if (Page is SM.EM.UI.BasePage_USER_EMENIK)
                            {
                                SM.EM.UI.BasePage_USER_EMENIK ppuem = (SM.EM.UI.BasePage_USER_EMENIK)Page;
                                ppuem.SetStatus("Napaka. Napaka pri procesiranju, plačilo ni uspelo. " + ErrorMsg);
                            }

                            return;
                        }
                        else {
                            if (Page is SM.EM.UI.BasePage_USER_EMENIK)
                            {
                                SM.EM.UI.BasePage_USER_EMENIK ppuem = (SM.EM.UI.BasePage_USER_EMENIK)Page;
                                ppuem.SetStatus("Plačilo je uspelo!");
                            }
                        }

                    }
                    else {
                        // transfer money!!
                        // mega pos service
                        si.megapos.service.MegaposProcessorService service = new si.megapos.service.MegaposProcessorService();

                        X509Certificate cert = X509Certificate.CreateFromCertFile(MegaPos.CertificatePath());
                        service.ClientCertificates.Add(cert);

                        // transaction
                        MegaPosRep mpr = new MegaPosRep();

                        if (ord.TransID != null)
                        {
                            MP_TRAN trans = mpr.GetTransByID(ord.TransID.Value);

                            // cancel
                            si.megapos.service.TxOperationResult initTxResult = service.processOrder(MegaPos.Store(), trans.TxId, null);

                            if (initTxResult.success != true)
                            {
                                // todo: set error text
                                if (Page is SM.EM.UI.BasePage_USER_EMENIK)
                                {
                                    SM.EM.UI.BasePage_USER_EMENIK ppuem = (SM.EM.UI.BasePage_USER_EMENIK)Page;
                                    ppuem.SetStatus("Napaka pri prenašanju");
                                }

                                return;
                            }

                        }
                    }





                    // set user payed, calculate new date valid
                    //EM_USER.UpdateUserPayment(ord.UserId, ord.TotalWithTax.Value , PAYMENT.Common.PaymentType.Payment, "", Page.User.Identity.Name, ord.OrderID);

                    // set tickets are active
                    //SHOPPING_ORDER.UpdateOrderTicketsActive(OrderID, true);
                }

            ord.OrderStatus = NewStatus;


            // save
            db.SubmitChanges();

            


            // raise changed event
            OnUserChanged (this, new EventArgs());

        }

        // GROUP
        //protected void btSave_Click(object sender, EventArgs e)
        //{
        //    Save();
        //}
        protected void btCancel_Click(object sender, EventArgs e)
        {
            //this.Visible = false;
            OnCancel(this, new EventArgs());
            // remove status text
            ParentPage.mp.StatusText = "";

            
        }
        protected void btSetPayed_Click(object sender, EventArgs e)
        {
            NewStatus = SHOPPING_ORDER.Common.Status.PAYED;
            // validation
            Page.Validate("ordredit");
            if (!Page.IsValid) return;
            Save();
        }

        protected void btSetPayedOnline_Click(object sender, EventArgs e)
        {
            NewStatus = SHOPPING_ORDER.Common.Status.PAYED;
            // validation
            Page.Validate("ordredit");
            if (!Page.IsValid) return;
            Save();
        }
        protected void btDeliverySend_Click(object sender, EventArgs e)
        {
            NewStatus = SHOPPING_ORDER.Common.Status.SENT_DELIVERY ;
            // validation
            Page.Validate("ordredit");
            if (!Page.IsValid) return;
            Save();
        }
        
        
        protected void btSetFinished_Click(object sender, EventArgs e)
        {
            NewStatus = SHOPPING_ORDER.Common.Status.FINISHED;

            // validation
            Page.Validate("ordreditFIN");
            if (!Page.IsValid) return;

            Save();

        }
        protected void btDeliveryFinished_Click(object sender, EventArgs e)
        {
            NewStatus = SHOPPING_ORDER.Common.Status.FINISHED;

            // validation
            Page.Validate("ordreditDEl_FIN");
            if (!Page.IsValid) return;

            Save();

        }
        protected void btSetPayedBack_Click(object sender, EventArgs e)
        {
            NewStatus = SHOPPING_ORDER.Common.Status.PAYED;

            // validation
            Page.Validate("ordredit");
            if (!Page.IsValid) return;

            Save();
        }
        protected void btCancelOrder_Click(object sender, EventArgs e)
        {
            NewStatus = SHOPPING_ORDER.Common.Status.CANCELED;


            Save();
        }


    }
}