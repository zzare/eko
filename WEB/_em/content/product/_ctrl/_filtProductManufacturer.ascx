﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_filtProductManufacturer.ascx.cs" Inherits="_em_content_product__ctrl__filtProductManufacturer" %>

<div class="mainFilW">
    <div class="mainFilT"></div>
    <div class="mainFilM">
        <span class="filterprodS">Filtriranje artiklov:</span>
        <div class="mainFilMI">
            <div class="mainFilInT"></div>
            <div class="mainFilInM">
                <div class="mainFilInMI">
                    <span class="filterBlueS">PROIZVAJALEC</span>
                    <span class="filterGreyS">
                    
                    
                    <asp:ListView ID="lvList" runat="server" >
                        <LayoutTemplate>
                            <asp:PlaceHolder ID="itemPlaceHolder" runat="server"></asp:PlaceHolder>
                        </LayoutTemplate>
                                             
                        <ItemTemplate >
        
                            <a class='Amaker <%# RenderClass(new Guid(Eval("ManufacturerID").ToString())) %>' href='<%# GetFilterFullUrl(new Guid(Eval("ManufacturerID").ToString()))  %>'>
                            <span class="filterMakerOut">
                            <span class="filterMaker" ><%# Eval("Title") %></span>
                            <span class="filterImg" ></span>
                            <span class="filterQuant" >(<%# Eval("Num") %>)</span>
                            </span>
                            </a>
                            <div class="clearing"></div>

                        </ItemTemplate>
                    
                    </asp:ListView>                    
                    
                    </span>
                </div>    
            </div>
            <div class="mainFilInB"></div>
        </div>    
    </div>
    <div class="mainFilB"></div>
</div> 