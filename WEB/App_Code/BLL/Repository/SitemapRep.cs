﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ProductRep
/// </summary>
public class SitemapRep
{

    private eMenikDataContext  db = new eMenikDataContext ();

   
    // Query Methods



    public SITEMAP GetSiteMapByID(int id, Guid pageid)
    {
        var smap = (from sm in db.SITEMAPs
                from m in db.MENUs
                from u in db.EM_USERs
                where sm.SMAP_ID == id && m.MENU_ID == sm.MENU_ID && u.USERNAME == m.USERNAME && u.UserId == pageid
                select sm).SingleOrDefault();
        return smap;
    }



    //public void Add(CUSTOMER customer)
    //{
    //    if (customer.CustomerID == Guid.Empty)
    //        customer.CustomerID = Guid.NewGuid();

    //    customer.DATE_REG = DateTime.Now;

    //    db.CUSTOMERs.InsertOnSubmit(customer );

    //}


    //
    // Persistence

    public bool  Save()
    {
        try
        {
            db.SubmitChanges();
        }
        catch (Exception ex)
        {
            ERROR_LOG.LogError( ex, "Save sitemap");

            return false;
        }
        return true;        
    }







}