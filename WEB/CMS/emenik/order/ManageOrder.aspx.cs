﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
namespace SM.UI.CMS
{
    public partial class CMS_Intranet_ManageOrder: SM.EM.UI.CMS.BasePage_CMS 
    {

        protected Guid OrderID
        {
            get
            {
                if (Request.QueryString["o"] == null)
                    return Guid.Empty;
                if (!SM.EM.Helpers.IsGUID(Request.QueryString["o"]))
                    return Guid.Empty;

                return new Guid(Request.QueryString["o"]);
            }
        }

        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);
            CurrentSitemapID = 801;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            // event handlers
            objOrderEdit.OnUserChanged += new EventHandler(objOrderEdit_OnOrderChanged);
            objOrderEdit.OnCancel += new EventHandler(objOrderEdit_OnCancel);
            objFilter.OnFilterChanged += new EventHandler(objFilter_OnFilterChanged);
            objOrderList.OnDelete += new SM.UI.Controls._cmsOrderList.OnEditEventHandler(objOrderList_OnDelete);
            objOrderList.OnEdit += new SM.UI.Controls._cmsOrderList.OnEditEventHandler(objOrderList_OnEdit);
            
            // init

            ApplyFilters();

            if (!IsPostBack)
            {
                // if Order is set as QS, show edit Order
                if (OrderID != Guid.Empty)
                {
                    LoadData();

                }

                    else // normal load
                {
                    HideTabs();
                    cpeQList.Collapsed = false;
                    objOrderList .BindData();
                }

            }
        }


        protected void LoadData() {

            objOrderEdit.OrderID = OrderID;
            objOrderEdit.BindData();
            cpeQList.Collapsed = true;
            cpeQList.ClientState = "true";

            objOrderList.BindData();



            ShowTabs();
        }

        void objOrderEdit_OnCancel(object sender, EventArgs e)
        {
            HideTabs();

            cpeQList.Collapsed = false;
            cpeQList.ClientState = "false";
            
        }


        protected void ApplyFilters() {
            // list
            objOrderList.SearchText = objFilter.SearchText;
            objOrderList.Active = objFilter.Active;
            objOrderList.Status = objFilter.Status;
            //objOrderList.RoleID = objFilter.Role;

            //objOrderEdit.ProjectID = ProjectID;
            //objOrderEdit.SiteID = SiteID;


        
        }
        // filter
        void objFilter_OnFilterChanged(object sender, EventArgs e)
        {
            // filters
            ApplyFilters();

            // refresh question lists
            objOrderList.BindData();



        }
        //edit
        void objOrderEdit_OnOrderChanged(object sender, EventArgs e)
        {
            // refresh Order lists
            ApplyFilters();

            // refresh Order lists
            objOrderList.BindData();

            // refresh edit
            objOrderEdit.BindData();


        }

        //list
        void objOrderList_OnEdit(object sender, SM.BLL.Common.Args.IDguidEventArgs e)
        {
            // show tabs
            ShowTabs();


            // apply filters
            ApplyFilters();



        }
        void objOrderList_OnDelete(object sender, SM.BLL.Common.Args.IDguidEventArgs e)
        {
            ApplyFilters();

            // refresh group lists
            objOrderList.BindData();

        }




        protected void ShowTabs()
        {
            tabOrder.Visible = true;
            tabOrder.ActiveTabIndex = 0;

        }
        protected void HideTabs()
        {
            tabOrder.Visible = false;

        }


    }
}