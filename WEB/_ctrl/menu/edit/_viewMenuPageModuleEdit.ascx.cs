﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _ctrl_menu_edit_viewMenuPageModuleEdit : BaseControlView
{

    public object Data;
    public object[] Parameters;
    public SITEMAP Smap;
    protected IEnumerable<PAGE_MODULE > PmodList;
    protected EM_USER em_user;


    protected void Page_Load(object sender, EventArgs e)
    {
        LoadData();
    }



    public void LoadData()
    {



        if (Data  == null)
            return;
        Smap = (SITEMAP)Data;

        if (Parameters != null)
        {

            if (Parameters.Length >  0 && Parameters[0] != null)
                em_user = (EM_USER)Parameters[0];
            if (Parameters.Length > 1 && Parameters[1] != null)
                PmodList = (IEnumerable<PAGE_MODULE>)Parameters[1];
        }

        if (PmodList != null)
        {
            lvPageMod.DataSource = PmodList;
            lvPageMod.DataBind();
        }



    }

    protected string RenderEditHref(object o)
    {
        SITEMAP_LANG item = o as SITEMAP_LANG;
        string ret = "";
        string cult = "";
        if (item == null)
            return ret;

        // get cultury by lang
        vw_UserCulture culture = CULTURE.GetUserCultures(em_user.UserId).FirstOrDefault(w => w.LANG_ID == item.LANG_ID);
        if (culture == null)
            return CULTURE.GetDefaultUserCulture(em_user.UserId).UI_CULTURE;

        cult = culture.UI_CULTURE;

        ret = SM.EM.Rewrite.EmenikUserUrl(em_user.PAGE_NAME , item.SMAP_ID, item.SML_TITLE, cult);
        return SM.BLL.Common.ResolveUrl(ret);

    }


    protected string RenderChecked(object d)
    {
        string ret = "";

        PAGE_MODULE item = (PAGE_MODULE)d;

        if (item.PMOD_ID  == Smap.PMOD_ID)
            return "checked";

        return ret;
    }





}
