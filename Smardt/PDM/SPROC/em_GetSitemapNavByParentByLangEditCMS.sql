IF EXISTS (SELECT * FROM sysobjects WHERE name='em_GetSitemapNavByParentByLangEditCMS' and type='P')
DROP Procedure em_GetSitemapNavByParentByLangEditCMS

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		zare
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].em_GetSitemapNavByParentByLangEditCMS
	-- Add the parameters for the stored procedure here

	@PARENT         int,
	@LANG_ID         nchar(2)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON
	
	
SELECT SM.[SMAP_ID]
      ,SM.[MENU_ID]
      ,SM.[PMOD_ID]
      ,SM.[SMAP_TITLE]
      ,SM.[PARENT]
      ,SM.[SMAP_ACTIVE]
      ,SM.[WWW_ROLES]
      ,SM.[CMS_ROLES]
      ,SM.[SMAP_ORDER]
      ,SM.[SMAP_IMAGE]
      ,SM.[WWW_VISIBLE]
      ,SML.SML_TITLE
      ,SML.SML_DESC
	  ,PM.WWW_URL
      ,PM.CMS_URL

  FROM [SITEMAP] SM, SITEMAP_LANG  SML, PAGE_MODULE PM 
 WHERE SM.SMAP_ID = SML.SMAP_ID 
		AND PM.PMOD_ID = SM.PMOD_ID
		AND SM.SMAP_ACTIVE = 1 
		AND	SML.SML_ACTIVE = 1
		AND SM.PARENT = @PARENT 
		AND SML.LANG_ID = @LANG_ID
ORDER BY SML_ORDER ASC, SMAP_ORDER ASC


END
GO
