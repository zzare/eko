﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_stepSummary.ascx.cs" Inherits="_em_content_order__ctrl__stepSummary" %>
<%@ Register Namespace="SM.EM.UI.Controls" TagPrefix="EM"  %>
<%@ Register TagPrefix="SM" TagName="EditModulePublic" Src="~/_ctrl/module/_edit/editModulePublic.ascx"   %>
<%@ Register TagPrefix="SM" TagName="ShoppingCart" Src="~/_ctrl/module/product/cart/_viewShoppingCart.ascx"   %>        
<%@ Register TagPrefix="SM" TagName="OrderSummaryFormatted" Src="~/_em/content/order/_ctrl/_ctrlOrderSummaryFormatted.ascx"    %>        

<SM:EditModulePublic ID="objEditPublic" runat="server" />

<div class="stepHeaderW">
<h1>Povzetek nakupa</h1>
</div>

<div class="stepShoppingCart" >
    <div class="contentRightW">
       <%-- <span class="contentRightHeadT_Text">POVZETEK NAKUPA</span>--%>
            <div class="contentRightMI">
            
                <%--<SM:OrderSummary ID="objOrderSummary" runat="server" />--%>
                <a id="A1" class="Aprint"  href="javascript:print()" style="float:right;" >Natisni</a>
                <div class="clearing"></div>



                <SM:OrderSummaryFormatted ID="objOrderSummaryFormatted" runat="server" Visible = "false" />

                <% if (Request.IsLocal){%>
                <asp:Button ID="btTestSend" runat="server" Text="Pošlji na mejl" OnClick="btTestSend_Click" />
                <%} %>




                <asp:PlaceHolder runat="server" Visible="true">


<% if (LANGUAGE.GetCurrentLang() == "si"){%>
                <h3>Naročnik</h3>
                <%= SHOPPING_ORDER.Common.RenderShoppingOrderUserData(Order)  %>
                <br />

                <h3>Način plačila</h3>
                <%= SHOPPING_ORDER.Common.RenderSHOPPING_ORDERPayment(Order)  %>

                <h3>Način dostave</h3>
                <%= SHOPPING_ORDER.Common.RenderShoppingOrderShippingDetail (Order)  %>
                <br />
                <h3>Vsebina naročila</h3>
<%}else if (LANGUAGE.GetCurrentLang() == "en"){ %>  
                <h3>Naročnik</h3>
                <%= SHOPPING_ORDER.Common.RenderShoppingOrderUserData(Order)  %>
                <br />

                <h3>Način plačila</h3>
                <%= SHOPPING_ORDER.Common.RenderSHOPPING_ORDERPayment(Order)  %>

                <h3>Način dostave</h3>
                <%= SHOPPING_ORDER.Common.RenderShoppingOrderShippingDetail (Order)  %>
                <br />
                <h3>Vsebina naročila</h3>

<%}else if (LANGUAGE.GetCurrentLang() == "it"){ %>  
                <h3>Cliente</h3>
                <%= SHOPPING_ORDER.Common.RenderShoppingOrderUserData(Order)  %>
                <br />

                <h3>Modalità di pagamento</h3>
                <%= SHOPPING_ORDER.Common.RenderSHOPPING_ORDERPayment(Order)  %>

                <h3>Modalità di consegna</h3>
                <%= SHOPPING_ORDER.Common.RenderShoppingOrderShippingDetail (Order)  %>
                <br />
                <h3>Contenuto del carrello</h3>

<%} %>
                

                <div style="padding-top:4px;">
                    <div id="stepShoppingCartList">
                        <SM:ShoppingCart ID="objShoppingCart" runat="server" />
                    </div>
                </div>
                
                </asp:PlaceHolder>
                

                <div ID="divError" EnableViewState="false" Visible="false" runat="server" style="color:Red ;" >
                    <asp:Label ID="litError" EnableViewState="false"  runat="server"></asp:Label>
                </div>    
                  
                <div class="stepnextbuttonW">
                <a id="btNext" class="Astepnext" onclick="return confirm('S potrditvijo boste oddali naročilo')" runat="server" onserverclick="btNext_Click" ><span><%= GetGlobalResourceObject("Product", "step3Next") %> &raquo;</span></a>
                </div>
                <div class="clearing"></div>
            </div>
    </div>
</div>



