﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

namespace SM.UI.Controls
{
    public partial class _cmsOrderEdit : BaseControl 
    {
        public event EventHandler OnUserChanged;
        public event EventHandler OnCancel;

        public Guid OrderID {
            get
            {
                Guid res = Guid.Empty;
                if (ViewState["OrderID"] == null) return res;
                if (!SM.EM.Helpers.IsGUID(ViewState["OrderID"].ToString())) return res;

                return new Guid(ViewState["OrderID"].ToString());
            }
            set{
                ViewState["OrderID"] = value;
            }
        }


        protected short NewStatus;

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {

            }

        }

        // bind data
        public void BindData() {
            this.Visible = true;


            if (OrderID == Guid.Empty)
            {
                //LoadDefaultValues();
            }
            else {
                LoadData();
            
            }
        }

        // LOAD Question
        protected void LoadData() {

            SHOPPING_ORDER ord = SHOPPING_ORDER.GetSHOPPING_ORDERByID(OrderID);
            tbOrderRefID.Text = SHOPPING_ORDER.Common.RenderShoppingOrderNumber(ord);
            litTotal.Text =  SM.EM.Helpers.FormatPrice(ord.TotalWithTax) + " ( " +   SM.EM.Helpers.FormatPrice(ord.Total) + " +ddv )";
            litPaymentType.Text = SHOPPING_ORDER.Common.RenderSHOPPING_ORDERPayment(ord);

            lnkUser.HRef = Page.ResolveUrl(EM_USER.Common.RenderCMSHref(ord.UserId ));
            lnkUser.InnerHtml = EM_USER.GetUserByID(ord.UserId).USERNAME ;

            lnkTicket.HRef = TICKET.Common.GetTicketDetailsCMSHref (ord.TicketID.Value );

            // status
            litStatus.Text = SHOPPING_ORDER.Common.RenderOrderStatus(ord.OrderStatus);

            // order detail
            objOrderItemList.OrderID = OrderID;
            objOrderItemList.UserId = ord.UserId ;
            objOrderItemList.BindData();


            phCancel.Visible = true;

            if (ord.OrderStatus == SHOPPING_ORDER.Common.Status.CONFIRMED)
            {
                phSetPayed.Visible = true;
                rfvDatePayed.Enabled = true;
                btSetFinished.Visible = false;
                btSetPayedBack .Visible = false;
                phCancel.Visible = true;
            }
            else if (ord.OrderStatus == SHOPPING_ORDER.Common.Status.PAYED)
            {
                phSetPayed.Visible = false;
                rfvDatePayed.Enabled = false;
                btSetFinished.Visible = true;
                btSetPayedBack.Visible = false;
                phCancel.Visible = false;
            }
            else if (ord.OrderStatus == SHOPPING_ORDER.Common.Status.FINISHED)
            {
                phSetPayed.Visible = false;
                rfvDatePayed.Enabled = false;
                btSetFinished.Visible = false;
                btSetPayedBack.Visible = true;
                phCancel.Visible = false;

            }
            else if (ord.OrderStatus == SHOPPING_ORDER.Common.Status.CANCELED)
            {
                phSetPayed.Visible = false;
                rfvDatePayed.Enabled = false;
                btSetFinished.Visible = false;
                btSetPayedBack.Visible = false;
                phCancel.Visible = false;

            }


            panEdit.Visible = true;
        }

        public void Save() {
            


            
            eMenikDataContext db = new eMenikDataContext();

            SHOPPING_ORDER ord = db.SHOPPING_ORDERs.SingleOrDefault(w => w.OrderID == OrderID);


            // status
            // user has payed
            if (ord.OrderStatus == SHOPPING_ORDER.Common.Status.CONFIRMED && NewStatus == SHOPPING_ORDER.Common.Status.PAYED)
            { 
                ord.DatePayed = DateTime.Parse( tbDatePayed.Text);
                ord.Payed = true;
                // set user payed, calculate new date valid
                EM_USER.UpdateUserPayment(ord.UserId, ord.TotalWithTax.Value , PAYMENT.Common.PaymentType.Payment, "", Page.User.Identity.Name, ord.OrderID);

                // set tickets are active
                //SHOPPING_ORDER.UpdateOrderTicketsActive(OrderID, true);



            
            }// order is finished
            else if (ord.OrderStatus == SHOPPING_ORDER.Common.Status.FINISHED && NewStatus == SHOPPING_ORDER.Common.Status.PAYED)
            { 
                // just change status
            
            } // back: order is not finished
            else if (ord.OrderStatus == SHOPPING_ORDER.Common.Status.PAYED && NewStatus == SHOPPING_ORDER.Common.Status.FINISHED)
            {
                // just change status

            }


            ord.OrderStatus = NewStatus;


            // save
            db.SubmitChanges();

            


            // raise changed event
            OnUserChanged (this, new EventArgs());

        }

        // GROUP
        //protected void btSave_Click(object sender, EventArgs e)
        //{
        //    Save();
        //}
        protected void btCancel_Click(object sender, EventArgs e)
        {
            //this.Visible = false;
            OnCancel(this, new EventArgs());
            // remove status text
            ParentPage.mp.StatusText = "";

            
        }
        protected void btSetPayed_Click(object sender, EventArgs e)
        {
            NewStatus = SHOPPING_ORDER.Common.Status.PAYED;

            // validation
            Page.Validate("ordredit");
            if (!Page.IsValid) return;

            Save();


        }
        protected void btSetFinished_Click(object sender, EventArgs e)
        {
            NewStatus = SHOPPING_ORDER.Common.Status.FINISHED;

            // validation
            Page.Validate("ordredit");
            if (!Page.IsValid) return;

            Save();

        }
        protected void btSetPayedBack_Click(object sender, EventArgs e)
        {
            NewStatus = SHOPPING_ORDER.Common.Status.PAYED;

            // validation
            Page.Validate("ordredit");
            if (!Page.IsValid) return;

            Save();
        }
        protected void btCancelOrder_Click(object sender, EventArgs e)
        {
            NewStatus = SHOPPING_ORDER.Common.Status.CANCELED;


            Save();
        }


    }
}