﻿function toggleButton(show, hide, state){
    var btS = document.getElementById(show);
    var btH = document.getElementById(hide);
    
    if (state){
        btS.style.display = 'none';
        btH.style.display = '';
        $('#' + state).removeClass("cwpHidden");
    }
    else{
        btS.style.display = '';
        btH.style.display = 'none';
        $('#' + state).addClass("cwpHidden");
    }
    
}

function onToggleCwpActive(show, hide, cwpID, active) {
    WebServiceCWP.UpdateCwpActive(cwpID, active);

    toggleButton(show, hide, active);
    if (active) {
        $('#' + cwpID).removeClass("cwpHidden");
    }
    else {
        $('#' + cwpID).addClass("cwpHidden");
    }
}

function closePopup(object)
{
    var mpe = $find(object);
    if (!mpe)
        alert(object);
    mpe.hide();

}
/* numeric localization */
function locale(decimalPoint, thousandSep, fracDigits) {
  this.decimalPoint = new String(decimalPoint);
  this.thousandSep = new String(thousandSep);
  this.fracDigits = fracDigits;
}
function roundFloat(num, fracDigits) {
  var factor = Math.pow(10, fracDigits);
  return(Math.round(num*factor)/factor);
}     
function toLcString(num, lc) {
  var str = new String(num);
  var aParts = str.split(".");
  return(aParts.join(lc.decimalPoint));
}
function formatNum(num, lc) {
    var sNum = new String(roundFloat(num, lc.fracDigits));
    if (lc.fracDigits > 0) {
        if (sNum.indexOf(".") < 0)
            sNum = sNum + ".";
        while (sNum.length < 1 + sNum.indexOf(".") + lc.fracDigits)
            sNum = sNum + "0";
    }
    return (toLcString(sNum, lc));
}
function parseLcNum(str, lc) {
    var sNum = new String(str);
    var aParts = sNum.split(lc.thousandSep);
    sNum = aParts.join("");
    aParts = sNum.split(lc.decimalPoint);
    return (parseFloat(aParts.join(".")));
}




/* CONTENT edit */
function editContentLoad(id) {
    conID = id;
    setModalLoaderLoading();
    openModalLoading('Nalagam vsebino');
    WebServiceCWP.GetContent(id, onContentLoadSuccess, onContentLoadError);
}
function onContentLoadSuccess(ret) {
    //$("#divModalLoader").dialog('option', 'height', 600);
    if (ret.Code == 1) {

        setModalLoader(ret.Data);
    }
    else if (ret.Code == 0) {
        setModalLoaderErr(ret.Message, true);
        return;
    }
    $("#divModalLoader" + mpl).init_mpl().dialog('open').dialog('option', 'width', 810).dialog('option', 'position', 'center').parent().find("div.ui-dialog-titlebar").hide();
    //$("#divModalLoader").dialog('open').dialog('option', 'width', 810).dialog('option', 'height', 600).dialog('option', 'position', 'center').parent().find("div.ui-dialog-titlebar").hide();
}
function onContentSaveSuccess(ret) {
    if (ret.Code != 1) {
        setModalLoaderErr(ret.Message, true);
        return;
    }
    closeModalLoader();
    closeModalLoading();

    $("#" + conID.toLowerCase()).find("div.conBody").html(conBody)
        .effect("highlight", { color: "#FF6600" }, 2000)
        .each(function() { formatObjectEmbed(this); });
    //.find("embed").attr("wmode", "transparent").each(function() { var cl = $(this).clone(); $(this).replaceWith(cl); });

}

function onContentLoadError(err) {
    logError(err);
    setModalLoaderErr("Prišlo je do napake. Prosim poskusite ponovno.", true);
}
function onContentSaveError(err) {
    logError(err);
    setModalLoaderErr("Prišlo je do napake. Prosim poskusite ponovno.", false);
}

function deleteEL(id) {
    if (confirm('Ali res želite zbrisati ta element?') == false)
        return;
    WebServiceCWP.DeleteElement(id, onDeleteSuccess, onDeleteError);
}
function onDeleteSuccess(ret) {
    if (ret.Code != 1) {
        setModalLoaderErr(ret.Message, true);
        return;
    }
    $("#" + ret.Data.toLowerCase()).slideUp("slow");
}
function onDeleteError(err) {
    logError(err);
    setModalLoaderErr("Prišlo je do napake. Prosim poskusite ponovno.", true);
}


/* POLL edit */
function editPollLoad(id) {
    pollID = id;
    cwpID = null;
    setModalLoaderLoading();
    openModalLoading('Nalagam anketo');
    WebServiceCWP.GetPoll(id, em_g_lang, onPollEditSuccessLoad, onPollEditErrorLoad);
}
function newPollLoad(id, cwp) {
    pollID = id;
    cwpID = cwp;
    setModalLoaderLoading();
    openModalLoading('Nalagam anketo');
    WebServiceCWP.GetPoll(id, em_g_lang, onPollEditSuccessLoad, onPollEditErrorLoad);
}

function onPollEditSuccessLoad(ret) {
    if (ret.Code == 1) {
        setModalLoader(ret.Data);
        closeModalLoading();
    }
    else if (ret.Code == 0) {
        setModalLoaderErr(ret.Message, true);
        return;
    }
    $("#divModalLoader" + mpl).init_mpl().dialog('open').dialog('option', 'width', 810).dialog('option', 'position', 'center').dialog('option', 'width', 810).parent().find("div.ui-dialog-titlebar").hide();
}
function onPollEditErrorLoad(err) {
    logError(err);
    setModalLoaderErr("Prišlo je do napake. Prosim poskusite ponovno.", true);
}

function onPollSaveSuccess(ret) {
    if (ret.Code == 0) {
        setModalLoaderErrValidation('', ret.Message);
        return;
    }
    else if (ret.Code != 1) {
        setModalLoaderErr(ret.Message, true);
        return;
    }
    closeModalLoading();
    closeModalLoader();

    $('#cPoll' + pollID.toLowerCase()).parent().html(ret.Data).effect("highlight", { color: "#FF6600" }, 2000);
}
function onPollSaveError(err) {
    logError(err);
    setModalLoaderErr("Prišlo je do napake. Prosim poskusite ponovno.", true);
}

//function settingsPollLoad(id) {
//    cwpID = id;
//    setModalLoaderLoading();
//    openModalLoading('Nalagam nastavitve');
//    WebServiceCWP.GetSettings(id, onCWPSettingsSuccessLoad, onCWPSettingsErrorLoad);
//}
function settingsLoad(id) {
    cwpID = id;
    setModalLoaderLoading();
    openModalLoading('Nalagam nastavitve');
    WebServiceCWP.GetSettings(id, onCWPSettingsSuccessLoad, onCWPSettingsErrorLoad);
}

function onCWPSettingsSuccessLoad(ret) {
    if (ret.Code == 1) {
        setModalLoader(ret.Data);
    }
    else if (ret.Code == 0) {
        setModalLoaderErr(ret.Message, true);
        return;
    }

    $("#divModalLoader" + mpl).init_mpl().dialog('open').dialog('option', 'width', 810).dialog('option', 'position', 'center').parent().find("div.ui-dialog-titlebar").hide();
}
function onCWPSettingsErrorLoad(err) {
    logError(err);
    setModalLoaderErr("Prišlo je do napake. Prosim poskusite ponovno.", true);
}
function onPollSettingsSuccessSave(ret) {
    if (ret.Code != 1) {
        setModalLoaderErr(ret.Message, true);
        return;
    }
    closeModalLoader();

    $("#" + ret.ID).find('div.cPollCWP').html(ret.Data).parent().effect("highlight", { color: "#FF6600" }, 2000);
}
function onCWPSettingsErrorSave(err) {
    logError(err);
    setModalLoaderErr("Prišlo je do napake. Prosim poskusite ponovno.", true);
}

function addPollLoad(cwp) {
    cwpID = cwp;
    setModalLoaderLoading();
    openModalLoading('Nalagam...');
    WebServiceCWP.GetPollAdd(cwp, onPollAddSuccessLoad, onCWPSettingsErrorLoad);
}
function onPollAddSuccessLoad(ret) {
    if (ret.Code == 1) {
        setModalLoader(ret.Data);
    }
    else if (ret.Code == 0) {
        setModalLoaderErr(ret.Message, true);
        return;
    }
    $("#divModalLoader" + mpl).init_mpl().dialog('open').dialog('option', 'position', 'center').parent().find("div.ui-dialog-titlebar").hide();
}
function onPollAddSuccessSave(ret) {
    if (ret.Code != 1) {
        setModalLoaderErr(ret.Message, true);
        return;
    }
    closeModalLoading();
    closeModalLoaderAll();

    $('#' + ret.ID).find('div.cPollItem').html(ret.Data).effect("highlight", { color: "#FF6600" }, 2000);
}

/* GALLERY edit */
//function settingsGalleryLoad(id) {
//    cwpID = id;
//    setModalLoaderLoading();
//    openModalLoading('Nalagam nastavitve');
//    WebServiceCWP.GetSettings(id, onCWPSettingsSuccessLoad, onCWPSettingsErrorLoad);
//}
function onGallerySettingsSuccessSave(ret) {
    if (ret.Code != 1) {
        setModalLoaderErr(ret.Message, true);
        return;
    }
    closeModalLoader();
    closeModalLoading();

    $("#" + ret.ID + " div.cGal").html(ret.Data).effect("highlight", { color: "#FF6600" }, 2000);
    $("#" + ret.ID + " div.galImgList").init_gal_sort();
}

function editImageLoad(id, tid, cwp) {
    imgID = id;
    cwpID = cwp;
    setModalLoaderLoading();
    openModalLoading('Nalagam sliko');
    WebServiceCWP.GetImageEdit(id, tid, onImageEditSuccessLoad, onError);
}
function onImageEditSuccessLoad(ret) {

    if (ret.Code == 1) {
        setModalLoader(ret.Data);
    }
    else if (ret.Code == 0) {
        setModalLoaderErr(ret.Message, true);
        return;
    }

    $("#divModalLoader" + mpl).init_mpl().show().dialog('open').dialog('option', 'width', 810).dialog('option', 'position', 'center').parent().find("div.ui-dialog-titlebar").hide();
}
function onImageEditSuccessSave(ret, _id) {
    if (ret.Code == 2) {
        //document.location.reload(true);
        window.location.href = window.location.href;
        return;
    }
    if (ret.Code != 1) {
        setModalLoaderErr(ret.Message, true);
        return;
    }
    closeModalLoader();
    closeModalLoading();

    var id = _id;
    if (id == null)
        id = ret.ID;

    $("#" + id + " div.cGal").html(ret.Data).effect("highlight", { color: "#FF6600" }, 2000);
    $("#" + id + " div.galImgList").init_gal_sort();
}
function deleteImage(id) {
    var c = confirm('Ali res želiš zbrisati to sliko?');
    if (!c)
        return;

    setModalLoaderLoading();
    openModalLoading('Brišem sliko...');
    WebServiceCWP.DeleteImage(id, cwpID, onImageEditSuccessSave, onError);
}

function editCustomCrop(id, tid) {
    imgID = id;
    setModalLoaderLoading();
    openModalLoading('Nalagam sliko');
    WebServiceCWP.GetImageCustomCrop(id, tid, onImageCropSuccessLoad, onError);
}
function onImageCropSuccessLoad(ret) {

    if (ret.Code == 1) {
        setModalLoader(ret.Data);
    }
    else if (ret.Code == 0) {
        setModalLoaderErr(ret.Message, true);
        return;
    }
    $("#divModalLoader" + mpl).init_mpl().dialog('open').dialog('option', 'width', 1060).dialog('option', 'resizable', true).dialog('option', 'position', 'center').parent().find("div.ui-dialog-titlebar").hide();
    ready_customCrop();
}

/* IMAGE ADD */
var imgAddPopClbck;
function addImageLoad(cwp, r, clb) {

    setModalLoaderLoading();
    openModalLoading('Odpiram...');
    var _items = new Array();
    Array.add(_items, cwp);
    if (clb)
        imgAddPopClbck = clb;
    WebServiceCWP.RenderViewAut(_items, 10, onImageNewSuccessLoad, onError, cwp);
}

function onImageNewSuccessLoad(ret, c, clb) {
    if (ret.Code == 1) {
        setModalLoader(ret.Data);
        closeModalLoading();
    }
    else if (ret.Code == 0) {
        setModalLoaderErr(ret.Message, true);
        return;
    }
    $("#divModalLoader" + mpl).init_mpl().dialog('open').dialog('option', 'width', 810).dialog('option', 'position', 'center').parent().find("div.ui-dialog-titlebar").hide();
    init_fu_image('fu_img_upload', c);
}

/* ARTICLE */
function editArticleLoad(cwp, a) {

    setModalLoaderLoading();
    openModalLoading('Odpiram...');
    var _items = new Array();
    Array.add(_items, cwp);
    Array.add(_items, a);
    WebServiceCWP.RenderViewAut(_items, 13, onPopupSuccessLoad, onError);
}

function onArticleSettingsSuccessSave(ret, cwp) {
    if (ret.Code == 0) {
        setModalLoaderErrValidation('', ret.Message);
        return;
    }
    else if (ret.Code == 2) {
        // refresh
        closeModalLoader();
        openModalLoading('Osvežujem...');
        //document.location.reload(false);
        window.location.href = window.location.href;
        return;
    }
    else if (ret.Code != 1) {
        setModalLoaderErr(ret.Message, true);
        return;
    }

    $("#" + cwp).find('div.cArtList').html(ret.Data).parent().sm_animate_change();
}
/* MENU */

function newMenuLoad(smp) {
    setModalLoaderLoading();
    openModalLoading('Nalagam');
    var _items = new Array();
    Array.add(_items, smp);
    Array.add(_items, em_g_lang);
    Array.add(_items, '1. nivo');
    WebServiceCWP.RenderViewAut(_items, 8, onPopupSuccessLoad, onError);
}
function editMenuLoad(smp) {
    setModalLoaderLoading();
    openModalLoading('Nalagam');
    var _items = new Array();
    Array.add(_items, smp);
    Array.add(_items, em_g_lang);
    WebServiceCWP.RenderViewAut(_items, 9, onPopupSuccessLoad, onError);
}
