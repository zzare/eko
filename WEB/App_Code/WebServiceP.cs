﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Globalization;
using System.Web.Security;
using System.Net.Mail;


/// <summary>
/// Summary description for WebServiceP
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]
public class WebServiceP : System.Web.Services.WebService {

    public WebServiceP () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    public class GalImage
    {
        public string UrlBig { get; set; }
        public string UrlThumb { get; set; }
        public string Title { get; set; }
        public string Desc { get; set; }
        public int ImageID { get; set; }
    }

    public class ImageData
    {
        public List<GalImage> Data;
        public string GalleryTitle;
        public string GalleryDesc;

    }




    [WebMethod]
    public ImageData GetImagesLang(Guid cwp, int index, string culture)
    {
        string cult = LANGUAGE.ValidateCulture(culture);
        LANGUAGE.SetCurrentCulture(cult);
        return GetImages(cwp, index);

    }
    [WebMethod]
    public ImageData GetImages(Guid cwp, int index)
    {
        // security: prevent DOS
        if (!SM.EM.Security.IsValidRequest()) return new ImageData { GalleryTitle = "no go ;)" };


        eMenikDataContext db = new eMenikDataContext();
        //int defBig = SM.BLL.Common.ImageType.EmenikBigDefault;
        int defThumb = SM.BLL.Common.ImageType.EmenikGalleryThumbCarousel;

        var list = from c in db.CMS_WEB_PARTs
                   from cg in db.CWP_GALLERies
                   from g in db.IMAGE_GALLERies
                   join il in
                       (from desc in db.IMAGE_LANGs where desc.LANG_ID == LANGUAGE.GetCurrentLang() select desc) on g.IMG_ID equals il.IMG_ID into outer
                   join gl in
                       (from desc in db.GALLERY_LANGs where desc.LANG_ID == LANGUAGE.GetCurrentLang() select desc) on g.GAL_ID equals gl.GAL_ID into outerG
                   from idesc in outer.DefaultIfEmpty()
                   from gdesc in outerG.DefaultIfEmpty()
                   from th in db.IMAGEs
                   from big in db.IMAGEs
                   where c.CWP_ID == cwp && cg.CWP_ID == c.CWP_ID && g.GAL_ID == c.ID_INT && th.IMG_ID == g.IMG_ID && (big.IMG_ID == g.IMG_ID) && th.TYPE_ID == defThumb && big.TYPE_ID == cg.BIG_TYPE_ID
                   orderby g.IMG_ORDER
                   select new { idesc.IMG_TITLE, idesc.IMG_DESC, BIG_URL = big.IMG_URL, THUMB_URL = th.IMG_URL, GAL_TITLE = gdesc.GAL_TITLE, GAL_DESC = gdesc.GAL_DESC };

        List<GalImage> images = new List<GalImage>();
        foreach (var item in list)
        {
            GalImage img = new GalImage();
            img.Title = item.IMG_TITLE ?? "";
            img.Desc = item.IMG_DESC ?? "";
            img.UrlBig = VirtualPathUtility.ToAbsolute(item.BIG_URL);
            img.UrlThumb = VirtualPathUtility.ToAbsolute(item.THUMB_URL); ;
            images.Add(img);
        }

        ImageData data = new ImageData();
        data.Data = images;
        data.GalleryTitle = list.First().GAL_TITLE ?? "";
        data.GalleryDesc = list.First().GAL_DESC ?? "";
        return data;
    }

    [WebMethod]
    public string IsDomainTaken(string domainName)
    {
        //return SM.EM.Helpers.WhoIs(domainName);
        string hostName = domainName.Trim();
        hostName = domainName.Replace("www.", "");

        bool isTaken = SM.EM.Helpers.IsDomainTaken(hostName);
        string ret = "prosta";
        if (isTaken)
            ret = "zasedena";

        ret = "<strong>" + ret + "</strong>";

        return "domena " + hostName + " je " + ret;

    }

    [WebMethod]
    public SM.EM.Ajax.ReturnData VotePoll(Guid pollChoice, string cult, string type)
    {
        SM.EM.Ajax.ReturnData ret = new SM.EM.Ajax.ReturnData { Code = SM.EM.Ajax.ReturnCode.Failure, Message = " no go! " };

        if (Context == null)
        {
            ret.Message = "no go ;)";
            return ret;
        }

        string culture = LANGUAGE.ValidateCulture(cult);        
        System.Threading. Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
        System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo(culture);


        // cast vote
        POLL_CHOICE p = POLL.VotePollChoice(pollChoice, type, Context.Request.UserHostAddress);
        if (p == null)
        {
            ret.Message = "Napaka pri glasovanju. Prosim, poskusite ponovno.";
            return ret;
        }

        // return poll results
        if (p.PollID != Guid.Empty)
        {
            object[] param = new object[] { p.PollID, p.LANG_ID };

            ret.Message = "success";
            ret.Code = SM.EM.Ajax.ReturnCode.OK;
            ret.Data = ViewManager.RenderView("~/_ctrl/module/poll/_viewPollResult.ascx", POLL.GetPollChoiceList(p.PollID, p.LANG_ID), param);
        }

        return ret;
    }

    [WebMethod]
    public SM.EM.Ajax.ReturnData GetPollResultsIfVoted(Guid pollID, string cult, string lang)
    {
        SM.EM.Ajax.ReturnData ret = new SM.EM.Ajax.ReturnData { Code = SM.EM.Ajax.ReturnCode.Failure, Message = " no go! " };

        if (Context == null)
        {
            ret.Message = "no go ;)";
            return ret;
        }

        string culture = LANGUAGE.ValidateCulture(cult);
        System.Threading.Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
        System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo(culture);



        // cast vote
        POLL p = POLL.GetPollByID(pollID);
        if (p == null)
        {
            ret.Message = "Anketa ne obstaja";
            return ret;
        }

        if (p.CanVote(Context.Request.UserHostAddress))
        {
            ret.Message = "success";
            ret.Code = SM.EM.Ajax.ReturnCode.Failure;
            return ret;
        }

        // return poll results
        if (p.PollID != Guid.Empty)
        {
            object[] param = new object[] { p.PollID, lang };

            ret.Message = "success";
            ret.Code = SM.EM.Ajax.ReturnCode.OK;
            ret.Data = ViewManager.RenderView("~/_ctrl/module/poll/_viewPollResult.ascx", POLL.GetPollChoiceList(p.PollID, lang), param);
        }

        return ret;
    }



    [WebMethod]
    public SM.EM.Ajax.ReturnData CastVote(Guid id, string cult, int num)
    {
        SM.EM.Ajax.ReturnData ret = new SM.EM.Ajax.ReturnData { Code = SM.EM.Ajax.ReturnCode.Failure, Message = " no go! " };

        if (Context == null)
        {
            ret.Message = "no go ;)";
            return ret;
        }

        string culture = LANGUAGE.ValidateCulture(cult);
        System.Threading.Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
        System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo(culture);


        // cast vote
        VOTE v = VOTE.CastVote(id, num , Context.Request.UserHostAddress);
        if (v == null)
        {
            ret.Message = "Napaka pri ocenjevanju. Prosim, poskusite ponovno.";
            return ret;
        }

        // return poll results
        ret.Message = "success";
        ret.Code = SM.EM.Ajax.ReturnCode.OK;
        ret.Data = SM.EM.Helpers.FormatPriceEdit(v.Average) ;

        return ret;
    }


    [WebMethod]
    public SM.EM.Ajax.ReturnData AddToCart(Guid pid, int quantity, List<ProductRep.Data.AttributeItem> items, string cult, string lang, Guid pgid)
    {
        SM.EM.Ajax.ReturnData ret = new SM.EM.Ajax.ReturnData { Code = SM.EM.Ajax.ReturnCode.Failure, Message = " no go! " };

        if (Context == null)
        {
            ret.Message = "no go ;)";
            return ret;
        }

        string culture = LANGUAGE.ValidateCulture(cult);
        System.Threading.Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
        System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo(culture);

        // get user id
        Guid userid = Guid.Empty;

        userid = SM.EM.BLL.Membership.GetCurrentUserID();
        if (userid == Guid.Empty )
        {
            ret.Message = "Napaka. Prosim poskusite ponovno.";
            return ret;
        }

        // add item to cart
        ProductRep prep = new ProductRep();

        prep.AddItemToCart(pid, userid, quantity, items, null);
        // save
        if (!prep.Save())
        {
            ret.Message = "Napaka pri dodajanju v košarico";
            return ret;
        }



        ret.Message = "success";
        ret.Code = SM.EM.Ajax.ReturnCode.OK;

        // return cart
        ret.Data = ViewManager.RenderView("~/_ctrl/module/product/cart/_viewShoppingCartPopup.ascx", prep.GetCartItemList(SM.EM.BLL.Membership.GetCurrentUserID(), lang, pgid, null), new object[] { pgid });
        
        return ret;
    }

    [WebMethod]
    public SM.EM.Ajax.ReturnData UpdateCartItem(Guid cid, int quantity, List<ProductRep.Data.AttributeItem> items, string cult, string lang, Guid pgid)
    {
        SM.EM.Ajax.ReturnData ret = new SM.EM.Ajax.ReturnData { Code = SM.EM.Ajax.ReturnCode.Failure, Message = " no go! " };

        if (Context == null)
        {
            ret.Message = "no go ;)";
            return ret;
        }

        string culture = LANGUAGE.ValidateCulture(cult);
        System.Threading.Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
        System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo(culture);

        // get user id
        Guid userid = Guid.Empty;

        userid = SM.EM.BLL.Membership.GetCurrentUserID();
        if (userid == Guid.Empty)
        {
            ret.Message = "Napaka. Prosim poskusite ponovno.";
            return ret;
        }

        // add item to cart
        ProductRep prep = new ProductRep();

        // check stock
        CART_ITEM item = prep.GetCartItemByID(cid, userid);
        if (item == null){
            ret.Message = "Napaka pri dodajanju v košarico";
            return ret;
        }
        PRODUCT  p = prep.GetProductByID(item.PRODUCT_ID );
        if (p.UNITS_IN_STOCK < quantity) {
            ret.Message = "Prevelika količina. Število izdelkov na zalogi: " + p.UNITS_IN_STOCK ;
            return ret;
        
        }

        prep.UpdateCartItem(cid, userid, quantity, items);
        // save
        if (!prep.Save())
        {
            ret.Message = "Napaka pri dodajanju v košarico";
            return ret;
        }


        ret.Message = "success";
        ret.Code = SM.EM.Ajax.ReturnCode.OK;

        // return cart
        ret.Data = ViewManager.RenderView("~/_ctrl/module/product/cart/_viewShoppingCartPopup.ascx", prep.GetCartItemList(SM.EM.BLL.Membership.GetCurrentUserID(), lang, pgid, null), new object[] { pgid });

        return ret;
    }

    [WebMethod]
    public SM.EM.Ajax.ReturnData UpdateCartItemQuantity(Guid cid, int quantity, string cult, string lang, Guid pgid)
    {
        SM.EM.Ajax.ReturnData ret = new SM.EM.Ajax.ReturnData { Code = SM.EM.Ajax.ReturnCode.Failure, Message = " no go! " };

        if (Context == null)
        {
            ret.Message = "no go ;)";
            return ret;
        }

        string culture = LANGUAGE.ValidateCulture(cult);
        System.Threading.Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
        System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo(culture);

        // get user id
        Guid userid = Guid.Empty;

        userid = SM.EM.BLL.Membership.GetCurrentUserID();
        if (userid == Guid.Empty)
        {
            ret.Message = "Napaka. Prosim poskusite ponovno.";
            return ret;
        }

        // add item to cart
        ProductRep prep = new ProductRep();

        // check stock
        CART_ITEM item = prep.GetCartItemByID(cid, userid);
        if (item == null)
        {
            ret.Message = "Napaka pri dodajanju v košarico";
            return ret;
        }
        PRODUCT p = prep.GetProductByID(item.PRODUCT_ID);
        if (p.UNITS_IN_STOCK < quantity)
        {
            ret.Message = "Prevelika količina. Število izdelkov na zalogi: " + p.UNITS_IN_STOCK;
            return ret;

        }

        prep.UpdateCartItemQuantity(cid, userid, quantity);
        // save
        if (!prep.Save())
        {
            ret.Message = "Napaka pri spreminjanju košarice";
            return ret;
        }


        ret.Message = "success";
        ret.Code = SM.EM.Ajax.ReturnCode.OK;

        // return cart
        ret.Data = ViewManager.RenderView("~/_ctrl/module/product/cart/_viewShoppingCartPopup.ascx", prep.GetCartItemList(SM.EM.BLL.Membership.GetCurrentUserID(), lang, pgid, null), new object[] { pgid });

        return ret;
    }


    [WebMethod]
    public SM.EM.Ajax.ReturnData DeleteCartItem(Guid cid, string cult, string lang, Guid pgid)
    {
        SM.EM.Ajax.ReturnData ret = new SM.EM.Ajax.ReturnData { Code = SM.EM.Ajax.ReturnCode.Failure, Message = " no go! " };

        if (Context == null)
        {
            ret.Message = "no go ;)";
            return ret;
        }

        string culture = LANGUAGE.ValidateCulture(cult);
        System.Threading.Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
        System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo(culture);

        // get user id
        Guid userid = Guid.Empty;

        userid = SM.EM.BLL.Membership.GetCurrentUserID();
        if (userid == Guid.Empty)
        {
            ret.Message = "Napaka. Prosim poskusite ponovno.";
            return ret;
        }

        // add item to cart
        ProductRep prep = new ProductRep();

        prep.RemoveItemFromCart(cid, userid);
        // save
        if (!prep.Save())
        {
            ret.Message = "Napaka pri dodajanju v košarico";
            return ret;
        }



        ret.Message = "success";
        ret.Code = SM.EM.Ajax.ReturnCode.OK;

        // return cart
        ret.Data = ViewManager.RenderView("~/_ctrl/module/product/cart/_viewShoppingCartPopup.ascx", prep.GetCartItemList(SM.EM.BLL.Membership.GetCurrentUserID(), lang, pgid, null), new object[] { pgid });

        return ret;
    }

    [WebMethod]
    public SM.EM.Ajax.ReturnData RenderView(object[] args, int viewID)
    {
        SM.EM.Ajax.ReturnData ret = new SM.EM.Ajax.ReturnData { Code = SM.EM.Ajax.ReturnCode.Failure, Message = " no go! " };
        if (Context == null)
        {
            ret.Message = "no go ;)";
            return ret;
        }

        string view = "";
        object data = new object();
        object[] parameters = new object[] { };


        string culture = LANGUAGE.ValidateCulture(args[0].ToString());
        System.Threading.Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
        System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo(culture);



        // get view data
        if (viewID == 1)
        {
            Guid id = new Guid(args[1].ToString());
            string lang = args[2].ToString();
            Guid pageid = new Guid  (args[3].ToString());
            ProductRep prep = new ProductRep();
            data = prep.GetShoppingCartItemByID(id, SM.EM.BLL.Membership.GetCurrentUserID(), lang, pageid );
            view = "~/_ctrl/module/product/cart/_viewEditCartItem.ascx";
        }
        else if (viewID == 2)
        {
            Guid id = new Guid(args[1].ToString());
            string lang = args[2].ToString();
            int index = (int) args[3];
            
            // get current page
            EM_USER usr = EM_USER.Current.GetCurrentPage();
            if (usr == null)
            {
                ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
                return ret;
            }

            data = GALLERY.GetImageListByCwpID(id, lang, SM.BLL.Common.ImageType.NewGalleryCarousel , SM.BLL.Common.ImageType.EmenikBigDefault);
            parameters = new object[] { false, index };
            view = "~/_ctrl/module/gallery/popup/_viewGalleryShow.ascx";
        }
        else if (viewID == 3)
        {// newBlogLoad
            Guid cwp = new Guid(args[0].ToString());
            //lang = args[1].ToString();
            int artID = int.Parse(args[1].ToString());

            view = "~/_ctrl/module/article/_viewBlogEdit.ascx";
            //ProductRep prep = new ProductRep();
            //data = ARTICLE.GetArticleByID(artID, usr.UserId);

            int sml = (int)args[2];
            string lang = args[3].ToString();
            var smapLang = SITEMAP.GetSitemapLangByID(sml, lang);


            if (artID > 0)
            {
                var art = ARTICLE.GetArticleByID(artID);
                data = art;

                // get art CWP_GAL
                var arep = new ArticleRep();
                var vwArt = arep.GetArticleByID(artID);
                ret.ID = vwArt.CWP_GAL.ToString();
                //ret.ID = art.ART_GUID.ToString();
            }
            else
            {
                data = new ARTICLE { ART_ID = -1, ART_TITLE = "", ART_BODY = "", ACTIVE = true, CWP_ID = cwp, RELEASE_DATE = DateTime.Now.Date };
                ret.ID = Guid.NewGuid().ToString();
            }
            parameters = new object[] { smapLang };

            
        }

        if (data == null)
        {
            ret.Message = "Napaka pri nalaganju";
            return ret;
        }

        ret.Message = "success";
        ret.Code = SM.EM.Ajax.ReturnCode.OK;
        ret.Data = ViewManager.RenderView(view, data, parameters);
        return ret;
    }


    [WebMethod]
    public void LogError(string err)
    {
        string user = "annonymous";
        string ip = SM.EM.BLL.BizObject.CurrentUserIP;
        if (Context.User.Identity.IsAuthenticated)
        {
            user = Context.User.Identity.Name;
        }
        ERROR_LOG.LogError("AJAX: <br/>", err, user, ip);
    }
 [WebMethod]
    public SM.EM.Ajax.ReturnData Logout()
    {
        SM.EM.Ajax.ReturnData ret = new SM.EM.Ajax.ReturnData { Code = SM.EM.Ajax.ReturnCode.Failure, Message = " no go! " };
        

        // if user is not logged in... redirect
        if (!User.Identity.IsAuthenticated)
            return ret;


        // user is logged in -> logout
        FormsAuthentication.SignOut();

        // clear session
        SM.EM.Security.Login.LogOut();

        ret.Message = "success";
        ret.Code = SM.EM.Ajax.ReturnCode.OK;
        return ret;
    } 



[WebMethod]
    public SM.EM.Ajax.ReturnData SubmitContactFormShort(Guid id, string name, string text, string email, string cult, string lang, List<string> additional, string notes, string sub)
    {
        SM.EM.Ajax.ReturnData ret = new SM.EM.Ajax.ReturnData { Code = SM.EM.Ajax.ReturnCode.Failure, Message = " no go! " };

        if (Context == null)
        {
            ret.Message = "no go ;)";
            return ret;
        }

        string culture = LANGUAGE.ValidateCulture(cult);
        System.Threading.Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
        System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo(culture);



        //EM_USER usr = EM_USER.Current.GetCurrentPage();
        //if (usr == null)
        //{
        //    ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
        //    return ret;
        //}


        // server side validation

        List<RuleViolation> errlist = new List<RuleViolation>();
        bool valid = true;

        if (string.IsNullOrWhiteSpace(name))
            errlist.Add(new RuleViolation(Resources.Modules.RegisterName , "Name"));
        if (string.IsNullOrWhiteSpace(text))
            errlist.Add(new RuleViolation(Resources.Modules.ContactFormTextTitle, "Phone"));
        if (string.IsNullOrWhiteSpace(email))
            errlist.Add(new RuleViolation(Resources.Modules.LoginEmail, "Email"));

        if (!valid || errlist.Count > 0)
        {
            ret.Data = ViewManager.RenderView("~/_ctrl/module/_err/_viewErrValidationListPopup.ascx", errlist, new object[] { });
            return ret;
        }



        CWP_CONTACT_FORM cf = CMS_WEB_PART.GetCwpContactFormByID(id);
        //string mailTo = "perfekt.sd@gmail.com"; //"zzzare@gmail.com";// SM.EM.Mail.Account.Info.Email;
        string mailTo = SM.EM.Mail.Account.Info.Email;

        if (SM.BLL.Common.Emenik.Data.IsTestMode() && Context.Request.IsLocal )
            mailTo = "zzzare@gmail.com";
        

//        string mailTo = "zzzare@gmail.com"; //"zzzare@gmail.com";// SM.EM.Mail.Account.Info.Email;
        string subject = "Vloga za kontaktiranje - " + name + " (" + email + ")" +  " - " + lang ;
        string mailFromName = SM.BLL.Common.Emenik.Data.PortalName();

        if (cf != null)
        {
            if (cf.MAIL_TO != null && !string.IsNullOrEmpty(cf.MAIL_TO))
                mailTo = cf.MAIL_TO;
            if (cf.SUBJECT != null && !string.IsNullOrEmpty(cf.SUBJECT))
                subject = cf.SUBJECT;

        }

        if (!string.IsNullOrWhiteSpace(sub))
            subject = sub + " - " + name + " (" + email + ")";

        // send mail
        MailMessage message = new MailMessage();
        message.From = new MailAddress(SM.EM.Mail.Account.Info.Email, mailFromName);
        message.ReplyToList.Add( new MailAddress(email ));
        if (HttpContext.Current.Request.IsLocal) {
            message.Bcc.Add(new MailAddress("zzzare@gmail.com"));
        }
        else
            message.To.Add(new MailAddress(mailTo));
        message.Bcc.Add(new MailAddress("zzzare@gmail.com"));

    // NOT LOCAL
        if (!HttpContext.Current.Request.IsLocal) 
            message.CC.Add(new MailAddress("narocila.terragourmet@gmail.com"));
        //message.To.Add(new MailAddress("zzzare@gmail.com"));


        message.Subject = subject ;
        message.Body = "Ime: " + name + "<br/>" + "Email: " + email + "<br/>" + "Vsebina: " + text  + "<br/>" + "Jezik: " + lang + "<br/>";

        // additional info
        if (additional != null && additional.Count > 0){
            message.Body += "<br/>Zanima nas tudi:<br/>";
            foreach (var item in additional ){
                message.Body += " - " + item + "<br/>";
            }
        }

        if (!string.IsNullOrEmpty(notes.Trim())) {
            message.Body += "<br/>Opombe:<br/>" + notes;            
        }

        message.IsBodyHtml = true;
        message.BodyEncoding = System.Text.Encoding.UTF8;
        message.SubjectEncoding = System.Text.Encoding.UTF8;




        SmtpClient client = new SmtpClient();


        try
        {
            //client.EnableSsl = true;


            // if local, use gmail, else use server SMTP
            //if (HttpContext.Current != null && HttpContext.Current.Request.IsLocal)
            //{
            //    client.Credentials = new System.Net.NetworkCredential(SM.EM.Mail.Account.Info.Email, SM.EM.Mail.Account.Info.Pass);
            //    client.Port = 587;//or use 587            
            //    client.Host = "smtp.gmail.com";

            //    client.EnableSsl = true;
            //}
            //else
            {
                //smtpClient.DeliveryMethod = SmtpDeliveryMethod.PickupDirectoryFromIis;

                // network delivery for propely setting SENDER header in mail
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                //client.Host = "localhost";
                client.Host = "1dva3.si";
            }

            if (!HttpContext.Current.Request.IsLocal )
                client.Send(message);
        }
        catch (Exception ex)
        {
            ret.Message = "Error. Please, try again.";
            if (lang == "si")
                ret.Message = "Napaka pri pošiljanju obrazca. Prosimo, poskusite ponovno.";
            else if (lang == "en")
                ret.Message = "Error. Please, try again.";
            else if (lang == "it")
                ret.Message = "Errore. Si prega di riprovare.";
            return ret;
        }


        // return results
        ret.Message = "success";
        ret.Code = SM.EM.Ajax.ReturnCode.OK;
        ret.Data = "Thank you. We will answer you shortly.";
        if (lang == "si")
            ret.Data = "Vaše povpraševanje je bilo poslano. Odgovor boste prejeli v najkrajšem možnem času.";
        else if (lang == "en")
            ret.Data = "Thank you. We will answer you shortly.";
        else if (lang == "it")
            ret.Data = "Grazie. Vi risponderà in breve tempo";
        
        return ret;
    }


[WebMethod(EnableSession = true)]
public SM.EM.Ajax.ReturnData SaveBlogEdit(Guid  artGuid, int artID, Guid cwpID, string title, string dateStart, string dateExpire, string abstr, string body, bool comments, bool approved, bool exposed, short sw, string lang, int smp, string emb, List<int> smapAdd)
{
    SM.EM.Ajax.ReturnData ret = new SM.EM.Ajax.ReturnData { Code = SM.EM.Ajax.ReturnCode.Failure, Message = " no go! " };

    if (Context == null)
    {
        ret.Message = "no go ;)";
        return ret;
    }

    if (!Context.User.Identity.IsAuthenticated)
    {
        ret.Message = "Tvoja seja je potekla. Ponovno se moraš prijaviti v sistem.";
        return ret;
    }

    // get current page
    EM_USER usr = EM_USER.Current.GetCurrentPage();
    if (usr == null)
    {
        ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
        return ret;
    }
    // check permissions
    //if (!PERMISSION.User.HasPermissionTypeMODULE(usr.UserId, null, cwpID, PERMISSION.Common.Obj.MOD_ARTICLE, PERMISSION.Common.Action.Edit, PERMISSION.Common.Type.Allow))
    //{
    //    ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
    //    return ret;
    //}

    // get CWP
    //CMS_WEB_PART cwp = CMS_WEB_PART.GetWebpartByID(cwpID, usr.USERNAME);
    //if (cwp == null)
    //{
    //    ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
    //    return ret;
    //}

    // get main smp
    // if not set, select first
    // if none is set, select main

    // get first cwp for main smp
    var cwp = CMS_WEB_PART.GetWebpartsByZoneSitemapLangUserModules(smp, lang, "wpz_1", true, false, usr.UserId).OrderBy(o => o.ORDER).FirstOrDefault();
    if (cwp == null)
    {
        ret.Message = "Težava pri dodajanju bloga. Prosimo poskusite ponovno.";
        return ret;
    }


    // save ARTICLE
    ArticleRep arep = new ArticleRep();

    ARTICLE art;
    if (artID <= 0)
    {// new article
        art = new ARTICLE();
        art.LANG_ID = lang;
        art.SMAP_ID = smp;
        art.CWP_ID = cwp.CWP_ID;

        arep.Add(art);
        art.ART_GUID = artGuid;

        // insert CWP for gallery
        // allready inserted
        //CMS_WEB_PART wp = CMS_WEB_PART.CreateNewCwpGallery(SM.BLL.Common.GalleryType.DefaultArticle, art.ART_GUID, null, MODULE.Common.GalleryModID, ARTICLE.Common.MAIN_GALLERY_ZONE, usr.UserId, "Galerija", "", true, false, -1, false, false, 3, SM.BLL.Common.ImageType.ArticleDetailsThumbDefault, SM.BLL.Common.ImageType.EmenikBigDefault);
    }
    else
    {
        //            art = arep.GetArticleByID(artID, cwp.UserId.Value);
        art = arep.GetArticleByID(artID, cwp.UserId.Value);
        if (art == null)
        {
            ret.Message = "Zapis ne obstaja";
            return ret;
        }
        art.SMAP_ID = smp;
        art.CWP_ID = cwp.CWP_ID;
    }



    // validate date
    DateTime releaseDate = DateTime.MinValue;
    if (!string.IsNullOrEmpty(dateStart.Trim()))
        DateTime.TryParse(dateStart.Trim(), out releaseDate);

    DateTime expireDate = DateTime.MinValue;
    if (!string.IsNullOrEmpty(dateExpire.Trim()))
        DateTime.TryParse(dateExpire.Trim(), out expireDate);

    releaseDate = SM.EM.Helpers.Date.ReValidateSqlDateTime(releaseDate);
    expireDate = SM.EM.Helpers.Date.ReValidateSqlDateTime(expireDate);

    // data
    art.ART_TITLE = title;
    art.RELEASE_DATE = releaseDate == System.Data.SqlTypes.SqlDateTime.MinValue ? (DateTime?)null : releaseDate;
    art.EXPIRE_DATE = expireDate == System.Data.SqlTypes.SqlDateTime.MinValue ? (DateTime?)null : expireDate;
    art.DATE_MODIFY = DateTime.Now;
    art.ART_ABSTRACT = abstr;
    art.ART_BODY = Server.HtmlEncode(body);
    art.COMMENTS_ENABLED = comments;

    art.APPROVED = approved;
    art.EXPOSED = exposed;
    art.SortWeight = sw;

    
    // reformat video url
    art.EMBEDD = emb;
    art.EMBEDD = art.EMBEDD.Replace("youtu.be/", "youtube.com/watch?v=");

    if ( !string.IsNullOrEmpty(art.EMBEDD) && !art.EMBEDD.StartsWith("http://"))
        art.EMBEDD = "http://" + art.EMBEDD;
    

    // validate
    List<RuleViolation> errlist = new List<RuleViolation>();
    bool valid = true;
    if (!art.IsValid)
    {
        errlist = art.GetRuleViolations().ToList();
        valid = false;

    }
    if (!valid)
    {
        ret.Message = ViewManager.RenderView("~/_ctrl/module/_err/_viewErrValidationListPopup.ascx", errlist, new object[] { });
        return ret;
    }


    // all ok, save
    if (!arep.Save())
    {
        ret.Message = "Napaka pri shranjevanju";
        return ret;
    }
    else { 
        // send mail to admin
        if (artID <= 0) {
            string mailTo = SM.EM.Mail.Account.Info.Email;// "info@terra-gourmet.com";
            if (SM.BLL.Common.Emenik.Data.IsTestMode() && Context.Request.IsLocal)
                mailTo = "zzzare@gmail.com";
            // send mail
            MailMessage message = new MailMessage();
            message.From = new MailAddress(SM.EM.Mail.Account.Info.Email);
            message.To.Add(new MailAddress(mailTo));


            message.Subject = "Nov blog zapis dodan" + ":" + art.ART_TITLE ;
            message.Body = "URL: " + SM.BLL.Common.ResolveUrlFull( SM.EM.Rewrite.EmenikUserArticleDetailsUrl( usr.PAGE_NAME, art.ART_ID, art.ART_TITLE  ) ) ;
            message.Body += "<br/>" + art.ART_ABSTRACT;
            message.Body += "<br/>" + art.ART_BODY;
            message.IsBodyHtml = true;

            SmtpClient client = new SmtpClient();


            try
            {

                if (HttpContext.Current != null && HttpContext.Current.Request.IsLocal)
                {
                    client.Credentials = new System.Net.NetworkCredential(SM.EM.Mail.Account.SendGmail.Email, SM.EM.Mail.Account.SendGmail.Pass);
                    client.Port = 587;//or use 587            
                    client.Host = "smtp.gmail.com";
                    client.EnableSsl = true;
                }
                else
                {
                    //                client.EnableSsl = true;
                    client.DeliveryMethod = SmtpDeliveryMethod.Network;
                    client.Host = "1dva3.si";
                    
                }
                client.Send(message);
            }
            catch (Exception ex)
            {
                ERROR_LOG.LogError(ex, "SaveBlogEdit send email");
            }
        
        }
    
    }


    // delete from all additional smp 
    // add to all additional smp except main
    arep.DeleteArticleSmaps(art.ART_ID);
    foreach (int item in smapAdd)
    {
        ARTICLE_SMAP pga = new ARTICLE_SMAP { ART_ID = art.ART_ID, SMAP_ID = item, LANG_ID = lang };
        arep.AddArticleSmap(pga);
    }
    arep.Save();

    


    //if (action == "ajax-list-cwp")
    //{
    //    // return new image list
    //    var cwp_gal = CMS_WEB_PART.GetCwpGalleryByID(data.CWP_ID);
    //    var img_list = GALLERY.GetImageListByCwpPaged(data.CWP_ID, LANGUAGE.GetCurrentLang(), true, 1, pSize);

    //    ret.Code = SM.EM.Ajax.ReturnCode.OK;
    //    ret.Data = ViewManager.RenderView("~/_ctrl/module/article/_viewArticleList.ascx", data, new object[] { true, cwp_gal, img_list });
    //    ret.ID = cwpID.ToString();
    //}
    //else {
    ret.Code = SM.EM.Ajax.ReturnCode.OK_refresh;
    //}

    ret.Message = "success";

    return ret;
}


}