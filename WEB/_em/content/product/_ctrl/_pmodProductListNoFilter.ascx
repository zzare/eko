﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_pmodProductListNoFilter.ascx.cs" Inherits="_em_content_product__ctrl__pmodProductListNoFilter" %>
<%@ Register Namespace="SM.EM.UI.Controls" TagPrefix="EM"  %>
<%@ Register TagPrefix="EM" TagName="ModuleNew" Src="~/_ctrl/emenik/emModuleNew.ascx"  %>
<%@ Register TagPrefix="SM" TagName="ProductList" Src="~/_ctrl/module/product/_viewProductList.ascx"  %>
<%@ Register TagPrefix="SM" TagName="ScriptProduct" Src="~/_ctrl/module/product/edit/_cntScript.ascx"  %>



<script type= "text/javascript">

    $(document).ready(function() {
        $("input.pageJump").keypress(function(e) {
            if (e.which == 13) {
                var ru = '<%= RenderSortPageFullUrl(GetCurrentOrder(), 2, false, "") %>';
                if ($(this).val() != '') {
                    ru = ru.replace('page=2', 'page=' + $(this).val());
                    window.location.href = ru;
                }
                return false;
            }
        });
    });
    
</script>



<SM:ScriptProduct ID="objScriptProduct" runat="server" />

<div class="productlist">
    <EM:ModuleNew ID="mnMain" runat="server" />

    <EM:CwpZone ID="wpz_1" runat="server">
    </EM:CwpZone>

        <div class="pagsortW">
            <div class="pagsortT">
                <div class="pagsortTI">
                    <div class="prodnum" style="float:left; width:242px;">&nbsp;</div>
                    <div class="paging" style="float:left;">
                        <span class="pagsort_blue">STRAN:</span>
                        <a <%= RenderPageFullUrlHREF( 1) %> class="pagesort_fbw"></a>
                        <a <%= RenderPageFullUrlHREF( GetCurrentPage()-1) %> class="pagesort_bw"></a>
                        <span style="float:left;">
                        <input class="pageJump" type="text" value='<%= GetCurrentPage()  %>'  /></span>
                        <span style="float:left; color:#333;">/ <%= TotalPages %></span>
                        <a <%= RenderPageFullUrlHREF( GetCurrentPage()+1) %> class="pagesort_fwd"></a>
                        <a <%= RenderPageFullUrlHREF( TotalPages ) %> class="pagesort_ffwd"></a>
                    </div>
                    <div id ="divSorting" runat="server"  enableviewstate="false" class="sorting" style="float:left;margin-left:20px;">
                        <span class="pagsort_blue">SORTIRAJ:</span>

                        <a  class="sortprice <%=RenderSelClass(2) %>" href='<%= RenderSortFullUrl(2) %>' >cena</a>
                        <a class="sortname <%=RenderSelClass(1) %>" href='<%= RenderSortFullUrl(1) %>' >naziv</a>
                        <a  class="sortprice <%=RenderSelClass(3) %>" href='<%= RenderSortFullUrl(3) %>' >ocena</a>
                        <a class="sortprice <%=RenderSelClass(4) %>" href='<%= RenderSortFullUrl(4) %>' >popust</a>
                    </div>
                    <div class="clearing"></div>
                </div>
            </div>
            <div class="pagsortM">
            </div>
            <div class="clearing"></div>
            <div class="pagsortB"></div>
        </div>

       <div class="clearing"></div>
            

    <SM:ProductList ID="objProductList" runat="server" />
    <div class="clearing"></div>

    <div class="pagsortW">
        <div class="pagsortT">
            <div class="pagsortTI">
                <div class="prodnum" style="float:left; width:242px;">&nbsp;</div>
                <div class="paging" style="float:left;">
                    <span class="pagsort_blue">STRAN:</span>
                    <a <%= RenderPageFullUrlHREF( 1) %> class="pagesort_fbw"></a>
                    <a <%= RenderPageFullUrlHREF( GetCurrentPage()-1) %> class="pagesort_bw"></a>
                    <span style="float:left;">
                    <input class="pageJump" type="text" value='<%= GetCurrentPage()  %>'  /></span>
                    <span style="float:left;">/ <%= TotalPages %></span>
                    <a <%= RenderPageFullUrlHREF( GetCurrentPage()+1) %> class="pagesort_fwd"></a>
                    <a <%= RenderPageFullUrlHREF( TotalPages) %> class="pagesort_ffwd"></a>
                </div>
                <div class="clearing"></div>
            </div>
        </div>
        <div class="pagsortM">
        </div>
        <div class="clearing"></div>
        <div class="pagsortB"></div>
    </div>
</div>


