﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Utils.Extensions
{
    public static class Cryptography
    {
        public static string GetMD5HashCode(this string str)
        {
            return Utils.Cryptography.Hashing.Hash(str);
        }
    }
}
