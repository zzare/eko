﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using SM.EM.BLL;
namespace SM.EM.UI.Controls
{
    public partial class _viewImageAddNew : BaseControlView
    {

        public object Data;
        public object[] Parameters;

        public Guid CwpID;

        public bool ForceDummyGallery = false;

        public string ForceGallery { get {

            string ret = "";

            if (!ForceDummyGallery)
                return ret;

            ret = "&force=1";
            
            return ret;
        
        } }


        protected void Page_Load(object sender, EventArgs e)
        {

            BindData();
        }
        // bind data
        public void BindData()
        {

            if (CwpID == Guid.Empty && Data == null)
                return;


            if (CwpID == Guid.Empty)
                CwpID = (Guid )Data;

            if (CwpID == Guid.Empty)
                return;

            // data


            if (Parameters != null)
            {
                //if (Parameters.Length > 0 && Parameters[0] != null)
                //    IsModeCMS = bool.Parse(Parameters[0].ToString());
                //if (Parameters.Length > 1 && Parameters[1] != null)
                //    CwpGallery = (CWP_GALLERY)Parameters[1];
                //if (Parameters.Length > 2 && Parameters[2] != null)
                //    ImageLangList = (IEnumerable<vw_ImageLang>)Parameters[2];
            }




        }


    }
}
