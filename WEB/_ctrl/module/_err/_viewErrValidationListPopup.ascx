﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewErrValidationListPopup.ascx.cs" Inherits="SM.EM.UI.Controls._err_viewErrListPopup" %>
<%@ Register Namespace="SM.UI.Controls" TagPrefix="SM"  %>
<%@ Register TagPrefix="SM" TagName="ErrorList" Src="~/_ctrl/module/_err/_viewErrValidationList.ascx"   %>

<h2>Podatki so napačni</h2>

<SM:ErrorList id="objErrorList" runat="server"></SM:ErrorList>

<div class="modPopup_footer"> 
    <div class="buttonwrapperModal">
        <a class="lbutton lbuttonConfirm" href="#" onclick="closeModalMsg(); return false"><span>Zapri</span></a>
    </div>
</div>