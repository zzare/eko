using System;
using System.Web;
using System.Web.UI;
using System.IO;
using System.Reflection;

public class AdvViewManager<T> where T : Control
{

    #region Properties

    private T _control = default(T);

    /// <summary>
    /// Gives you access to the control you are rendering allows
    /// you to set custom properties etc.
    /// </summary>
    public T Control
    {
        get
        {
            return _control;
        }
    }

    // Used as a placeholder page to render the control on.
    private Page _holder = null;

    #endregion

    #region Constructor

    /// <summary>
    /// Default constructor for this view manager, pass in the path for the control
    /// that this view manager is render.
    /// </summary>
    /// <param name="inPath"></param>
    public AdvViewManager(string inPath)
    {
        //Init the holder page
        _holder = new Page();

        
        // Create an instance of our control
        //_control = (T)_holder.LoadControl(inPath);

        //// Add it to our holder page.
        //_holder.Controls.Add(_control);
    }

    #endregion

    #region Rendering

    /// <summary>
    /// Renders the current control.
    /// </summary>
    /// <returns></returns>
    public string Render()
    {
        StringWriter sw = new StringWriter();

        // Execute the page capturing the output in the stringwriter.
        HttpContext.Current.Server.Execute(_holder, sw, false);

        // Return the output.
        return sw.ToString();
    }

    #endregion

}
