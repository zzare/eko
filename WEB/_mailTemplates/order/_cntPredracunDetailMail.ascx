﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_cntPredracunDetailMail.ascx.cs" Inherits="SM.UI.Controls.c_user_ctrl_cntPredracunDetailMail" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Namespace="SM.UI.Controls" TagPrefix="SM" %>
<%@ Register TagPrefix="SM" TagName="PredracunDetail" Src="~/_em/content/order/_ctrl/_cntPredracunDetail.ascx"  %>




<table width="668" cellpadding="0" cellspacing="0" border="0" style="background:#fff;">
    <tbody>
        <tr>
            <td>
                <%--<img align="right" alt="eko" src='<%= SM.BLL.Common.ResolveUrl("~/_em/templates/_custom/kolpasan/_inc/images/mailtemplate/kolpa_mailtemp_head.jpg") %>' />
                <br />    
               --%> 
               <%--Pozdravljeni!
                <br />    
                <br />     
                Pošiljamo Vam predračun za plačilo naročila št. <%= SHOPPING_ORDER.Common.RenderSHOPPING_ORDERRef(Order.OrderRefID) %>.
                <br />    
                <br />    
                <br />    --%>

                <SM:PredracunDetail ID="objPredracunDetail" runat="server" />

                <br /><br />


                <br />
                Predračun lahko kadarkoli pogledate in natisnete tudi na tej povezavi:
                <br />
                <a href='<%= SM.BLL.Common.ResolveUrl (SHOPPING_ORDER.Common.Predracun .GetPredracunHref(Order.OrderID, Order.PageId.Value) ) %>'><%=  SM.BLL.Common.ResolveUrl(SHOPPING_ORDER.Common.Predracun.GetPredracunHref(Order.OrderID, Order.PageId.Value))%></a>
                <br />
                <br /><br />
            </td>
        </tr>
        <%--<tr>
            <td>
                <img alt="eko" src='<%= SM.BLL.Common.ResolveUrl("~/_em/templates/_custom/kolpasan/_inc/images/mailtemplate/kolpa_mailtemp_foot.jpg") %>' />
            </td>
        </tr>--%>

    </tbody>
</table>