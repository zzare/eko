﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewProductAttributeSlider.ascx.cs" Inherits="_ctrl_module_product_attribute_viewProductAttributeSlider" %>


<script type="text/javascript">
    $(function () {
        //alert('test');
        $("#<%= ViewID %> .attrSlider").slider({
            <%= RenderDefault() %>,
            <%= RenderMin() %>,
            <%= RenderMax() %>,
            //			step: 50,
            disabled: <%= Attr.IsDisabled.ToString().ToLower() %>,
            slide: function (event, ui) {
                $(this).parent().find(".valSlider").html(ui.value);
                //$(ui.handle).parent().find("input").remove();
                var inp = $(ui.handle).find("input");
                if (inp.length == 0){
                    var i = '<input value="' + ui.value + '" />';
                    $(ui.handle).append(i);

                } 
                else
                    inp.eq(0).val(ui.value);

            }
        });
        $("#<%= ViewID %> .attrSlider").each(function(){
            var inp = $(this).find("input");
            if (inp.length == 0){
                $(this).find("a").append('<input value="' + $(this).slider("option","value") + '" />');
            } 
        
        });
        


    });
</script>

<div id="<%= ViewID  %>" name="<%= Attr.GroupAttID   %>" class="cAttrSlider"  >


 <div class="attr_slider_attrtitle"> <%= GroupAttribute.Title.ToUpper()  %></div>


    <span class="valSlider"></span>
    <div class="attrSliderW">
        <div name="<%= Attr.AttributeID  %>" class="attrSlider" ></div>
        <div class="attr_slider_min"><%= Attr.GetMin %></div>
        <div class="attr_slider_max"><%= Attr.GetMax %></div>
        <div class="clearing"></div>
    </div>
    <div class="clearing"></div>
</div>
<div class="clearing"></div>



