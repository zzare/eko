﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_ctrlLanguage.ascx.cs" Inherits="SM.UI.Controls._ctrlLanguage" %>

<div id="cLang">

    <asp:PlaceHolder ID="phVisible" runat="server">

        <% if (SM.EM.Security.Permissions.IsAdvancedCms()) ParentPage.RenderEditLinkStep(2); %>
        <asp:ListView ID="lvList" runat="server" onitemcommand="lvList_ItemCommand">
            <LayoutTemplate>
                
                
                <div id="itemPlaceholder" runat="server"></div>
            </LayoutTemplate>
            
            <ItemTemplate>
                <%--<asp:LinkButton ID="lbCult" CssClass='<%# RenderSelClass(Eval("UI_CULTURE").ToString()) %>' CommandArgument='<%# Eval("UI_CULTURE") %>' CommandName="setLang" runat="server"><%# Eval("DESC").ToString().ToLower() %></asp:LinkButton>--%>
                <a title="<%# SM.BLL.Common.Emenik.Data.PortalName() + " " + Eval("DESC") %>"  href='<%# Page.ResolveUrl( SM.EM.Rewrite.EmenikUserUrl(ParentPage.PageName, Eval("UI_CULTURE").ToString() ))  %>' class='<%# RenderSelClass(Eval("UI_CULTURE").ToString()) %>' ><%# Eval("DESC").ToString().ToLower() %></a>
            </ItemTemplate>
            
            <ItemSeparatorTemplate>
            <%# Separator%> 
            </ItemSeparatorTemplate>

        </asp:ListView>

    </asp:PlaceHolder>
    

    <asp:PlaceHolder ID="phHidden" Visible="false" runat="server">
    &nbsp;    
    </asp:PlaceHolder>
    
</div>