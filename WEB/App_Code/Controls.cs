﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Data.Linq;


/// <summary>
/// Summary description for Controls
/// </summary>
/// 
namespace SM.UI.Controls {


    public class CheckBoxID : CheckBox
    {
        public int IDint { 
            get{return  (int)( ViewState["IDint"] ??  -1);}
            set { ViewState["IDint"] = value; }
        }
        public string IDstring
        {
            get { return (string)(ViewState["IDstring"] ?? ""); }
            set { ViewState["IDstring"] = value; }
        }

    }
    


    public class LinkButtonDefault : LinkButton
    {
        protected override void OnLoad(System.EventArgs e)
        {
            Page.ClientScript.RegisterStartupScript(GetType(), "addClickFunctionScript",
                _addClickFunctionScript, true);

            string script = String.Format(_addClickScript, ClientID);
            Page.ClientScript.RegisterStartupScript(GetType(), "click_" + ClientID,
                script, true);
            base.OnLoad(e);
        }

        private const string _addClickScript = "addClickFunction('{0}');";

        private const string _addClickFunctionScript =
            @"  function addClickFunction(id) {{
            var b = document.getElementById(id);
            if (b && typeof(b.click) == 'undefined') b.click = function() {{
                var result = true; if (b.onclick) result = b.onclick();
                if (typeof(result) == 'undefined' || result) {{ eval(b.getAttribute('href')); }}
            }}}};";
    }
     
    /// <summary>
    /// Menu control
    /// </summary>
    /// 
    public class smMenu : Menu {

        public event  EventHandler OnMenuDataBound;

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }
        protected override void OnDataBound(EventArgs e)
        {
            base.OnDataBound(e);
            // select selected menu item
            SiteMapDataSource  ds = (SiteMapDataSource)Page.Master.FindControl(this.DataSourceID);
            SelectCurrentMenuItem(this, SiteMap.Providers[ds.SiteMapProvider ]);


            OnMenuDataBound(this, new EventArgs());
        }

        protected override void OnMenuItemDataBound(MenuEventArgs e)
        {
            base.OnMenuItemDataBound(e);
            SiteMapNode node = e.Item.DataItem as SiteMapNode;
            if (node != null)
            {
                if (!string.IsNullOrEmpty(node["class"]))
                    e.Item.ToolTip += ";" + node["class"];
            }

            

        }
        public void SelectCurrentMenuItem(Menu m, SiteMapProvider prov){
            foreach (MenuItem item in m.Items){
                SelectCurrentMenuItem(item, m, prov);
            }

        }
        protected void SelectCurrentMenuItem(MenuItem mi, Menu m, SiteMapProvider provider){

            if (m.SelectedItem != null) return;

            if (provider.CurrentNode != null && mi.DataPath == provider.CurrentNode.Key){
                mi.Selected = true;
                return;
            }
            if (mi.ChildItems == null)
                return;

            foreach (MenuItem item in mi.ChildItems){
                SelectCurrentMenuItem(item, m, provider);
            }
        } 
  

    }

    /// <summary>
    /// List View control with icons in sortable header. header must be runat=server and id="headerSort" !!
    /// </summary>
    /// 
    public class smListView : ListView {

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            Page.ClientScript.RegisterStartupScript(this.GetType(), "smListView", "function GetCssClass(index){ if (index % 2 == 0) return ''; else return 'odd';}", true);
        }

        protected override void OnDataBound(EventArgs e)
        {
            base.OnDataBound(e);

            //  if there is no sort expression, don't bother
            if (this.SortExpression.Length > 0)
            {
                //  if this is the first time ItemDataBound has fired, figure out what column
                //  is being sorted by
                //if (!this._sortCellIndex.HasValue)
                //{
                HtmlTableRow header = ((HtmlTableRow)this.FindControl("headerSort"));

                if (header == null) return;

                //  loop through the cells and find the one that contains the linkbutton
                //  with the commandargument of the ListView's current SortExpression
                for (int i = 0; i < header.Cells.Count; i++)
                {
                    HtmlTableCell th = header.Cells[i];
                    //  find the LinkButton control
                    foreach (Control c in th.Controls)
                    {
                        LinkButton linkButton = c as LinkButton;
                        if (linkButton != null)
                            if (linkButton.CommandArgument == this.SortExpression)
                            {
                                //  keep track of the cell index
                                //this._sortCellIndex = i;

                                // remove sort classes
                                string originalHeaderStyle = th.Attributes["class"];
                                if (!string.IsNullOrEmpty(originalHeaderStyle))
                                    originalHeaderStyle = originalHeaderStyle.Replace("sortasc", "").Replace("sortdesc", "");

                                //  add the sort class to this item                        
                                th.Attributes["class"] = string.Format("{0} {1}", originalHeaderStyle, this.SortDirection == SortDirection.Ascending ? "sortasc" : "sortdesc").Trim();
                                break;
                            }
                            else {
                                // remove sort classes
                                string originalHeaderStyle = th.Attributes["class"];
                                if (!string.IsNullOrEmpty(originalHeaderStyle))
                                    th.Attributes["class"] = originalHeaderStyle.Replace("sortasc", "").Replace("sortdesc", "");
                            }
                    }
                    //}
                }
            }     
        }
    }

    public class DynamicWebPartManager : WebPartManager
    {
        
        public void AddDynamicWebPart(WebPart webPart, WebPartZone zone)
        {
            Internals.AddWebPart(webPart);
            Internals.SetZoneID(webPart, zone.ID);
        }
        public void SetDirty()
        {
            SetPersonalizationDirty();
        }

    }

    /// <summary>
    /// inline script control which works in updatepanel
    /// </summary>
    /// 
    public class InlineScript : Control
    {

        protected override void Render(HtmlTextWriter writer)
        {
            ScriptManager sm = ScriptManager.GetCurrent(Page);

            if (sm.IsInAsyncPostBack)
            {

                StringBuilder sb = new StringBuilder();
                base.Render(new HtmlTextWriter(new StringWriter(sb)));
                string script = sb.ToString();
                ScriptManager.RegisterStartupScript(this, typeof(InlineScript), UniqueID, script, false);
            }
            else
            {
                base.Render(writer);
            }
        }
    }


}

namespace SM.EM.UI.Controls {
    public class EmenikWebPartZone : WebPartZone {
        public EmenikWebPartZone() {
            // init
            base.DeleteVerb.Visible = false;
            base.CloseVerb.Visible = false;
            base.Width = new Unit( "100%");
            base.PartChromeType = PartChromeType.None;
            base.MinimizeVerb.Visible = false;
            base.RestoreVerb.Visible = false;
            base.EmptyZoneText = "Sem lahko premaknete objekt.";
            base.LayoutOrientation = Orientation.Vertical;
            
        }    
    }

    public class BreadCrubs : System.Web.UI.WebControls.WebControl {


        public string Separator {get; set;}
        public string RootName = "Home Page";
        public SiteMapProvider Provider;

        protected override void Render(HtmlTextWriter writer)
        {
            if (Provider == null)
                return;
            if (Provider.CurrentNode == null)
                return;


            StringBuilder sbResult = new StringBuilder();

            SiteMapNode node = Provider.CurrentNode;
            string _output = "";
            string _sep = "";
            while (node.ParentNode != null)
            {
                 _output = "<a href=\"" + Page.ResolveUrl( node.Url) + "\">" + node.Title + "</a>" + _sep  + _output;

                node = node.ParentNode;
                _sep = Separator;
            }

            sbResult.Append(_output);
            
            writer.Write(sbResult.ToString());



        }

    
    
    
    }



    public class CwpZone : Control
    {
        //public override ControlCollection Controls
        //{
        //    get
        //    {
        //        UpdatePanel _up = FindControl("up" + this.ID) as UpdatePanel ;
        //        if (_up != null) return _up.ContentTemplateContainer.Controls ;
        //        return base.Controls;
        //    }
        //}
        //public void Update() {
        //    UpdatePanel _up = FindControl("up" + this.ID) as UpdatePanel;
        //    if (_up != null) _up.Update();
        
        //}
        private bool _hasPlaceholder = false;
        private PlaceHolder _placeHolder;
        public ControlCollection ContentPlaceholder
        {
            get
            {
                if (_hasPlaceholder && _placeHolder != null)
                    return _placeHolder.Controls ;

                PlaceHolder ph = (PlaceHolder) SM.EM.Helpers.FindControlRecursive(this , "placeholder");
                if (ph != null)
                {
                    _hasPlaceholder = true;
                    _placeHolder = ph;
                    return ph.Controls;
                }
                
                return base.Controls;
            }
        }


        protected override void OnInit(EventArgs e)
        {
            this.EnableViewState = false;

            // register control on page
            base.OnInit(e);

            //Type type = Type.GetType("SM.EM.UI.BasePage_CmsWebPart", false, false);
            //Type pageType = this.Page.GetType();


            //Control c = this.Page as Control;
            if (this.Page is BasePage_CmsWebPart)
            {
                BasePage_CmsWebPart parent = Page as BasePage_CmsWebPart;
                if (!parent.Zones.ContainsKey(this.ID ))
                    parent.Zones.Add(this.ID, this);

            }

        }


        public override void RenderControl(HtmlTextWriter writer)
        {
            if (this.ContentPlaceholder.Count == 0)
            {
                if (this.Page is BasePage_CmsWebPart)
                {
                    // hide if empty and PREVIEW
                    BasePage_CmsWebPart parent = Page as BasePage_CmsWebPart;
                    if (!parent.IsModeCMS)
                        this.Visible = false;
                }
            }

            base.RenderControl(writer);
        }




        protected override void Render(HtmlTextWriter writer)
        {

            // render without placeholder
            if (!this._hasPlaceholder)
            {
                CustomRenderBeginTag(writer);
                base.Render(writer);
                writer.RenderEndTag();
            }
            else {
                // Render Children
                foreach (Control c in Controls)
                {
                    if (c == _placeHolder)
                        CustomRenderBeginTag(writer);
                    
                    c.RenderControl(writer);
                    
                    if (c == _placeHolder)
                        writer.RenderEndTag();
                }
            }



            
            //string cssClass = "cwp_zone";
            // add another class for edit mode
            //if (this.Page is BasePage_CmsWebPart)
            //{
            //    BasePage_CmsWebPart parent = Page as BasePage_CmsWebPart;
            //    if (parent.IsEditMode && parent.IsModeCMS)
            //        ;
            //    //                    cssClass += " cwp_zone_edit";
            //    else // hide if empty
            //        if (this.ContentPlaceholder.Count == 0)
            //        {
            //            this.Visible = false;
            //            return;
            //        }

            //}


            //// render div with control id aroud all content
            //writer.AddAttribute(HtmlTextWriterAttribute.Id, this.ID);
            //writer.AddAttribute(HtmlTextWriterAttribute.Class, cssClass );
            
            
            //writer.RenderBeginTag(HtmlTextWriterTag.Div);

            //base.Render(writer);
            
            //writer.RenderEndTag();

        }

        protected void CustomRenderBeginTag(HtmlTextWriter writer)
        {
            string cssClass = "cwp_zone";
            // add another class for edit mode
            if (this.Page is BasePage_CmsWebPart)
            {
                BasePage_CmsWebPart parent = Page as BasePage_CmsWebPart;
                if (parent.IsEditMode && parent.IsModeCMS)
                    cssClass += " cwp_zone_edit";
                //else // add class if empty
                //    if (this.ContentPlaceholder.Count == 0)
                //    {
                //        //cssClass += " cwp_empty";
                //        this.Visible = false;
                //        return;
                //    }

            }


            // render div with control id aroud all content
            writer.AddAttribute(HtmlTextWriterAttribute.Id, this.ID);
            writer.AddAttribute(HtmlTextWriterAttribute.Class, cssClass);


            writer.RenderBeginTag(HtmlTextWriterTag.Div);


            //            writer.RenderEndTag();
        }

    }

    public class CwpEdit : BaseControl_CmsWebPart {
        public int IDref
        {
            get
            { return (int)(ParentPage.GetViewStateValue(this, "IDref") ?? -1); }
            set { ParentPage.SetViewStateValue(this, "IDref", value); }
        }
        public Guid CwpID
        {
            get { return (ViewState["CwpID"] == null ? Guid.Empty : new Guid(ViewState["CwpID"].ToString())); }
            set { ViewState["CwpID"] = value; }
        }
        public string Zone
        {
            get { return (string)(ParentPage.GetViewStateValue(this, "Zone") ?? ""); }
            set { ParentPage.SetViewStateValue(this, "Zone", value); }
        }
    }


    public class BaseWizardStep : BaseControl {

        public BasePage_EMENIK ParentPageEmenik;
        public string Description;
        public string ShortDesc;
        public string ErrorText;
        public int StepID;
        public bool AllwaysShowSaveButton = false;
        public bool AllwaysHideSaveButton = false;
        public bool ShowTopNavigation = false;

        //public delegate void OnErrorEventHandler(object sender, SM.BLL.Common.StringEventArgs e);
        public event EventHandler  OnErrorSet;


        public virtual void LoadData() { }

        public virtual bool SaveData(int step) { return true; }

        protected override void OnInit(EventArgs e)
        {
            ParentPageEmenik = (BasePage_EMENIK)Page;
            base.OnInit(e);
        }

        public virtual bool Validate() {  return true; }

        protected void UpdateUserStep(int step)
        {
            EM_USER.UpdateUserStep(ParentPageEmenik.em_user.UserId, step);

        }

        protected bool CheckCanSave() {

            // if is demo, can't save
            if (!ParentPageEmenik.CanSave)
            {
                ErrorText = "Nimate pravic za spreminjanje podatkov";
                return false;
            }

            return true;
        }
    }


    public class BaseWizardStepOrder : BaseControl
    {

        public BasePage_EMENIK ParentPageEmenik;
        public string Description;
        public string ShortDesc;
        public string ErrorText;
        public int StepID;
        public SHOPPING_ORDER Order;
        public OrderRep Orep;

        //public delegate void OnErrorEventHandler(object sender, SM.BLL.Common.StringEventArgs e);
        public event EventHandler OnErrorSet;
        //public event EventHandler OnSaveOK;


        public virtual void LoadData() { }

        public virtual bool SaveData(int step) { return true; }

        protected override void OnInit(EventArgs e)
        {
            ParentPageEmenik = (BasePage_EMENIK)Page;
            base.OnInit(e);
        }

        public virtual bool Validate() { return true; }



    }




    public class CustomPager : System.Web.UI.Control {
        public string QSKeySort = "ord";
        public string QSKeyDirection = "dir";
        public string QSKeyPage = "page";

        public string StartingQueryString = "";

        public string SelectedClass = "sel";
        public string PageLinkTemplate = "<a {0} class=\"pagenum {2}\">{1}</a>";

        public string BaseAbsoluteUrl = "";


        public string OrderColumns = "";
        public string OrderDirections = "";


        //public abstract string GetOrderColumnByID(int id);
        //public abstract bool GetDefaultDirectionByID(int id);


    public string GetOrderColumnByID(int id)
    {
        string ret = "";

        if (string.IsNullOrEmpty(OrderColumns))
            return ret;

        string[] orderCol = OrderColumns.Split(",".ToCharArray());

        if (orderCol.Length > id - 1 && orderCol[id - 1] != null)
        {
            ret = orderCol[id - 1].Trim();
        }
        return ret;
    }

    public  bool GetDefaultDirectionByID(int id)
    {
        // true = ascending
        bool ret = true;

        if (string.IsNullOrEmpty(OrderDirections))
            return ret;
        string[] orderDir = OrderDirections.Split(",".ToCharArray());

        if (orderDir .Length > id - 1 && orderDir[id - 1] != null)
        {
            if (orderDir[id - 1].Trim() != "asc")
                ret = false ;
        }


        return ret;
    }




        public virtual string RenderOrderDirectionByID(bool dir)
        {
            string ret = "1";
            if (dir == true)
                ret = "1";
            else if (dir == false)
                ret = "0";

            return ret;
        }

        public virtual string RenderSqlOrderDirectionByID(bool dir)
        {
            string ret = "asc";
            if (dir == true)
                ret = "asc";
            else
                ret = "desc";

            return ret;
        }


        /* CURRENT */
        public virtual int GetCurrentOrder()
        {
            int ret = 1;
            if (HttpContext.Current == null)
                return ret;
            if (HttpContext.Current.Request.QueryString[QSKeySort] == null)
                return ret;

            int.TryParse(HttpContext.Current.Request.QueryString[QSKeySort], out ret);

            return ret;
        }
        public virtual bool GetCurrentDirection()
        {
            bool ret = GetDefaultDirectionByID(GetCurrentOrder());
            if (HttpContext.Current == null)
                return ret;
            if (HttpContext.Current.Request.QueryString[QSKeyDirection] == null)
                return ret;

            int dir = 0;
            int.TryParse(HttpContext.Current.Request.QueryString[QSKeyDirection], out dir);
            if (dir == 1)
                ret = true;
            else
                ret = false;

            return ret;
        }
        public virtual int GetCurrentPage()
        {
            int ret = 1;
            if (HttpContext.Current == null)
                return ret;
            if (HttpContext.Current.Request.QueryString[QSKeyPage] == null)
                return ret;

            int.TryParse(HttpContext.Current.Request.QueryString[QSKeyPage], out ret);

            if (ret > TotalPages)
                ret = TotalPages;

            if (ret < 1)
                ret = 1;

            return ret;
        }

        public virtual string RenderCurrentSortSQL()
        {
            string ret = "";

            // render QS
            ret = GetOrderColumnByID(GetCurrentOrder());
            ret += " " + RenderSqlOrderDirectionByID(GetCurrentDirection());

            return ret;
        }


        /* RENDER */

        public virtual string RenderSortQS(int id, bool changeDir)
        {
            string ret = "";
            bool dir = GetDefaultDirectionByID(id);

            // if current is selected, change direction
            if (changeDir && id == GetCurrentOrder() && GetDefaultDirectionByID(id) == GetCurrentDirection())
            {
                dir = !GetCurrentDirection();
            }
            else if (!changeDir && id == GetCurrentOrder())
            {
                dir = GetCurrentDirection();
            }

            // render QS
            ret = QSKeySort + "=" + id.ToString();
            ret += "&" + QSKeyDirection + "=" + RenderOrderDirectionByID(dir);

            return ret;
        }

        public virtual string RenderPageQS(int id)
        {
            string ret = "";
            if (id <= 1)
                return ret;

            // render QS
            ret = QSKeyPage + "=" + id.ToString();
            return ret;
        }

        public virtual string RenderCurrentFilterQSWithout(string fid)
        {
            string ret = "";
            // render QS
            //ret = "page=" + id.ToString();
            return ret;
        }



        public virtual string RenderSortFullUrl(int sortId)
        {
            return RenderSortPageFullUrl(sortId, 1, true, "");
        }
        public virtual string RenderPageFullUrlHREF(int pageId)
        {

            if (pageId < 1 || pageId == GetCurrentPage() || pageId > TotalPages)
                return "";

            return "href=\"" + RenderSortPageFullUrl(GetCurrentOrder(), pageId, false, "") + "\"";
        }

        public virtual string GetStartingAbsolutePath()
        {
            return HttpContext.Current.Request.Url.AbsolutePath;
        }

        public virtual string RenderSortPageFullUrl(int sortId, int pageId, bool changeSortDir, string withoutFilt)
        {
            string ret = "";

            //ret = Utils.Web.QueryString.Current.Remove(QSKeySort).Remove(QSKeyPage).Remove(QSKeyDirection).ToString();

            ret = GetStartingAbsolutePath();
            string qs = StartingQueryString;

            // sort
            if (!string.IsNullOrEmpty(RenderSortQS(sortId, changeSortDir)))
            {
                if (!string.IsNullOrEmpty(qs))
                    qs += "&";
                qs += RenderSortQS(sortId, changeSortDir);
            }

            // page
            if (!string.IsNullOrEmpty(RenderPageQS(pageId)))
            {
                if (!string.IsNullOrEmpty(qs))
                    qs += "&";
                qs += RenderPageQS(pageId);
            }

            //filter
            if (!string.IsNullOrEmpty(RenderCurrentFilterQSWithout(withoutFilt)))
            {
                if (!string.IsNullOrEmpty(qs))
                    qs += "&";
                qs += RenderCurrentFilterQSWithout(withoutFilt);
            }

            // append to url
            if (!string.IsNullOrEmpty(qs))
                qs = "?" + qs;

            ret += qs;

            return SM.BLL.Common.ResolveUrl(ret);
        }

        public virtual string RenderSelClass(int sortId)
        {
            string ret = "";

            if (sortId != GetCurrentOrder())
                return ret;
            ret = " sel";
            ret += " " + RenderSqlOrderDirectionByID(GetCurrentDirection());

            return ret;
        }


        /* PAGING */
        protected int _pageSize = 10;
        public virtual int PageSize
        {
            get
            {
                return _pageSize;
            }
            set
            {
                _pageSize = value;
            }
        }
        protected int _TotalCount = -1;
        public  virtual int TotalCount
        {
            get
            {
                return _TotalCount;
            }
            set { _TotalCount = value; }
        }

        public  virtual int TotalPages
        {
            get
            {
                if ((TotalCount % PageSize) == 0)
                {
                    return (TotalCount / PageSize);
                }
                else
                {
                    double result = (TotalCount / PageSize);
                    result = Math.Ceiling(result);
                    return (Convert.ToInt32(result) + 1);
                }
            }
        }

        public virtual string RenderSelPageClass(int page)
        {
            string ret = "";

            if (page != GetCurrentPage())
                return ret;
            ret = " " + SelectedClass;

            return ret;
        }


        public string RenderPageLinks(int limit)
        {
            string ret = "";

            int count = TotalPages;

            int min = 1;
            int max = TotalPages;
            int cur = GetCurrentPage();
            int half_limit = (int)Math.Round((decimal)(limit - 1) / 2);

            if (limit > 0 && count > limit)
            {
                // limit


                // min
                if (max < limit || (cur <= half_limit + 1))
                    max = min + limit - 1;
                // max
                else if (cur >= max - half_limit - 1)
                    min = max - limit + 1;
                else
                {
                    min = cur - half_limit;
                    max = min + limit - 1;
                }

                count = limit;
            }


            for (int i = min; i <= max; i++)
            {
                ret += string.Format(PageLinkTemplate, RenderPageFullUrlHREF(i), i, RenderSelPageClass(i));
            }


            return ret;
        }
    
    
    
    
    }



}
