﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.Linq;
using System.Data.Linq.Mapping ;
using System.Reflection ;
using System.Collections.Generic;
using System.Linq.Expressions;

/// <summary>
/// Summary description for SiteMap
/// </summary>
public partial class SITEMAP
{

    public static SITEMAP GetSiteMapByID(int id)
    {
        eMenikDataContext db = new eMenikDataContext();
        SITEMAP smap;

        smap = (from sm in db.SITEMAPs  where sm.SMAP_ID == id select sm).SingleOrDefault();
        return smap;
    }

    public static SITEMAP GetSiteMapByID(int id, Guid pageid)
    {
        eMenikDataContext db = new eMenikDataContext();
        SITEMAP smap;

        smap = (from sm in db.SITEMAPs 
                from m in db.MENUs
                from u in db.EM_USERs 
                where sm.SMAP_ID == id && m.MENU_ID == sm.MENU_ID && u.USERNAME == m.USERNAME && u.UserId == pageid                 
                select sm).SingleOrDefault();
        return smap;
    }

    public static IEnumerable<SITEMAP> GetSiteMapChildsByID(int id)
    {
        eMenikDataContext db = new eMenikDataContext();
        IEnumerable<SITEMAP> smap;

        smap = from sm in db.SITEMAPs where sm.PARENT == id select sm;

        return smap;
    }


    public static IEnumerable<SITEMAP> GetSiteMapsByUserName(string username)
    {
        eMenikDataContext db = new eMenikDataContext();
        IEnumerable<SITEMAP> smap;

        smap = from sm in db.SITEMAPs join m in db.MENUs on sm.MENU_ID equals m.MENU_ID where m.USERNAME == username.ToLower() select sm;

        return smap;
    }

    public static int GetSiteMapCountByUserName(string username)
    {
        eMenikDataContext db = new eMenikDataContext();
        int count;

        count = (from sm in db.SITEMAPs join m in db.MENUs on sm.MENU_ID equals m.MENU_ID where m.USERNAME == username.ToLower() select 1).Count();

        return count -1;
    }
    public static IEnumerable<em_GetSitemapNavByParentByLangResult> GetSiteMapNavByParentByLang(int parent, string culID)
    {
        eMenikDataContext db = new eMenikDataContext();
        IEnumerable<em_GetSitemapNavByParentByLangResult> smap;

        smap = db.em_GetSitemapNavByParentByLang(parent, culID);
        
        return smap;
    }
    public static IEnumerable<em_GetSitemapNavByParentByLangEditCMSResult> GetSiteMapNavByParentByLangEditCMS(int parent, string culID)
    {
        eMenikDataContext db = new eMenikDataContext();
        IEnumerable<em_GetSitemapNavByParentByLangEditCMSResult> smap;

        smap = db.em_GetSitemapNavByParentByLangEditCMS(parent, culID);

        return smap;
    }

    public static IEnumerable<em_GetSitemapNavByParentByLangWWWResult> GetSiteMapNavByParentByLangWWW(int parent)
    {
        eMenikDataContext db = new eMenikDataContext();
        IEnumerable<em_GetSitemapNavByParentByLangWWWResult> smap;

        smap = db.em_GetSitemapNavByParentByLangWWW(parent, LANGUAGE.GetCurrentCulture());

        return smap;
    }
    public static IEnumerable<em_GetSitemapNavByParentByLangWWWResult> GetSiteMapNavByParentByLangWWWByCulture(int parent, string culture )
    {
        eMenikDataContext db = new eMenikDataContext();
        IEnumerable<em_GetSitemapNavByParentByLangWWWResult> smap;

        smap = db.em_GetSitemapNavByParentByLangWWW(parent, culture );

        return smap;
    }

    public static List<em_GetEmenikSitemapLang_HierarchyResult> GetSiteMapHierarchyByLang(string  pageName, string culture)
    {
        eMenikDataContext db = new eMenikDataContext();
        return db.em_GetEmenikSitemapLang_Hierarchy(pageName, culture).ToList();
    }

    public static IEnumerable<em_GetEmenikSitemapLang_HierarchyResult> GetSiteMapHierarchyByLangByMenu(int menuID, string culture)
    {
        eMenikDataContext db = new eMenikDataContext();
        return db.em_GetEmenikSitemapLang_byMenuID_Hierarchy2(menuID, culture);
    }

    public static List<em_GetEmenikSitemapLangEx_HierarchyResult> GetSiteMapHierarchyByLangEx(string pageName, string culture)
    {
        eMenikDataContext db = new eMenikDataContext();
        return db.em_GetEmenikSitemapLangEx_Hierarchy(pageName, culture).ToList();
    }

    public static IEnumerable<em_GetEmenikSitemapLangEx_HierarchyResult> GetSiteMapHierarchyByLangExByMenu(int menuID, string culture)
    {
        eMenikDataContext db = new eMenikDataContext();
        return db.em_GetEmenikSitemapLangEx_byMenuID_Hierarchy2(menuID, culture);
    }


    public static IEnumerable<SITEMAP_LANG> GetSiteMapChildsByIDByLang(int id, string langID)
    {
        eMenikDataContext db = new eMenikDataContext();
        IEnumerable<SITEMAP_LANG> smap;

        smap = from sm in db.SITEMAP_LANGs from s in db.SITEMAPs  where s.SMAP_ID == sm.SMAP_ID && sm.SITEMAP.PARENT == id && sm.LANG_ID==langID orderby s.SMAP_ORDER    select sm;
        return smap;
    }
    public static IEnumerable<SITEMAP_LANG> GetSitemapLangsBySitemap(int smapID)
    {
        eMenikDataContext db = new eMenikDataContext();

        IEnumerable<SITEMAP_LANG> list = (from l in db.SITEMAP_LANGs from c in db.CULTUREs from u in db.EM_USER_CULTUREs from s in db.SITEMAPs from m in db.MENUs from emu in db.EM_USERs  
                                          where l.SMAP_ID == smapID && l.LANG_ID == c.LANG_ID && c.LCID == u.LCID && u.UserId == emu.UserId && emu.USERNAME == m.USERNAME && m.MENU_ID == s.MENU_ID && s.SMAP_ID == l.SMAP_ID 
                                          select l);
        return list;
    }

    public static SITEMAP_LANG GetSitemapLangByID(int smapID, string langID)
    {
        eMenikDataContext db = new eMenikDataContext();
        SITEMAP_LANG sl = db.SITEMAP_LANGs.SingleOrDefault(w => w.LANG_ID == langID && w.SMAP_ID == smapID);

        return sl;
    }
    public static IEnumerable<SITEMAP> GetSiteMapChildsByID(int id, int menuID)
    {
        eMenikDataContext db = new eMenikDataContext();
        IEnumerable<SITEMAP> smap;

        smap = from sm in db.SITEMAPs where sm.PARENT == id && sm.MENU_ID == menuID orderby sm.SMAP_ORDER  select sm;
        return smap;
    }

    public static SITEMAP GetRootByMenu(int menuID)
    {
        eMenikDataContext db = new eMenikDataContext();
        SITEMAP smap;

        smap =(SITEMAP ) (from sm in db.SITEMAPs where sm.PARENT == -1 && sm.MENU_ID == menuID select sm).FirstOrDefault();
        return smap;
    }

    public static SITEMAP GetRootByPageName(string pageName)
    {
        eMenikDataContext db = new eMenikDataContext();
        SITEMAP smap;

        smap = (SITEMAP)(from sm in db.SITEMAPs join m in db.MENUs on sm.MENU_ID equals m.MENU_ID
                         from em in db.EM_USERs 
                         where sm.PARENT == -1 &&  m.USERNAME == em.USERNAME && em.PAGE_NAME  == pageName 
                         select sm).FirstOrDefault();
        return smap;
    }
    public static SITEMAP GetRootByAnySitemap(int smpID)
    {
        eMenikDataContext db = new eMenikDataContext();
        var  smap = ( from sm in db.SITEMAPs
                      let menuID = (from s in db.SITEMAPs where s.SMAP_ID == smpID select s.MENU_ID ).FirstOrDefault()
                     where sm.PARENT == -1 && sm.MENU_ID == menuID select sm);
        return smap.FirstOrDefault();
    }

    public static string GetWWWurlBySitemapID(int sitemapID) {
        SITEMAP smp = GetSiteMapByID(sitemapID);
        if (smp == null) return "";

        if (smp.WWW_URL != null && !string.IsNullOrEmpty(smp.WWW_URL))
            return smp.WWW_URL ;


        return smp.PAGE_MODULE.WWW_URL.Replace("{}", sitemapID.ToString());
    }


    //public static IQueryable<Data.SitemapLang > GetSitemapLangs(int smapID, string langID, Expression expr)
    //{
    //    eMenikDataContext db = new eMenikDataContext();
    //    var list = from s in db.SITEMAPs
    //               from sl in db.SITEMAP_LANGs
    //               where s.SMAP_ID == smapID && sl.SMAP_ID == s.SMAP_ID && sl.LANG_ID == langID 
    //               select new Data.SitemapLang
    //               {
    //                   SMAP_ID = s.SMAP_ID,
    //                   LANG_ID = sl.LANG_ID,
    //                   SMAP_GUID = s.SMAP_GUID,
    //                   MENU_ID = s.MENU_ID,
    //                   PARENT = s.PARENT,
    //                   PMOD_ID = s.PMOD_ID,
    //                   SML_ACTIVE = sl.SML_ACTIVE,
    //                   SML_DESC = sl.SML_DESC,
    //                   SML_KEYWORDS = sl.SML_KEYWORDS,
    //                   SML_META_DESC = sl.SML_META_DESC,
    //                   SML_ORDER = sl.SML_ORDER,
    //                   SML_TITLE = sl.SML_TITLE
    //               };
            

    //    return list;
    //}


    public static  List<RuleViolation> AddSitemap(EM_USER em_user, string title, string lang, int parent)
    {
     
        List<RuleViolation> errlist = new List<RuleViolation>();

        // check if user has  menus left
        if (em_user.SITEMAP_COUNT <= SITEMAP.GetSiteMapCountByUserName(em_user.USERNAME))
        {

            errlist.Add(new RuleViolation("* Trenutno imaš največje število dovoljenih menijev (" + em_user.SITEMAP_COUNT.ToString() + "). Če jih želiš imeti več, nas prosim kontaktiraj.", "SITEMAP_COUNT"));
            return errlist;
        }

        title = title.Trim();

        if (string.IsNullOrEmpty(title))
            title = "(nov meni)";


        eMenikDataContext db = new eMenikDataContext();

        // insert new sitemap 
        SITEMAP s = new SITEMAP();
        SITEMAP p = SITEMAP.GetSiteMapByID (parent, em_user.UserId  );
        if (p == null) {
            errlist.Add(new RuleViolation("* Napaka pri dodajanju.", "PageID"));
            return errlist;
        }

        s.MENU_ID = p.MENU_ID;
        s.PARENT = p.SMAP_ID;
        s.SMAP_GUID = Guid.NewGuid();
        s.PMOD_ID = SM.BLL.Common.PageModule.TwoColumn;  // two column
        s.SMAP_TITLE = title;
        s.SMAP_ACTIVE = true;
        s.WWW_ROLES = "?,*";
        s.CMS_ROLES = "admin, em_cms";
        s.SMAP_ORDER = 100;
        s.WWW_VISIBLE = true;

        db.SITEMAPs.InsertOnSubmit(s);
        db.SubmitChanges();

        // get users languages
        List<LANGUAGE> langs = LANGUAGE.GetEmUsersLanguages(em_user.UserId).ToList();
        // add new item for each language 
        foreach (LANGUAGE lan in langs){
            MENU.ChangeSitemapLang(s.SMAP_ID, s.SMAP_TITLE, lan.LANG_ID, false);
        }

        // remove menu cache for current user
        SM.EM.Caching.RemoveEmenikUserMenuCacheKey(em_user.PAGE_NAME );

        return errlist ;
    }


    partial void OnValidate(System.Data.Linq.ChangeAction action)
    {
        if (action == System.Data.Linq.ChangeAction.Delete)
            return;

        if (string.IsNullOrEmpty(this.SMAP_TITLE))
            throw new ArgumentException("Sitemap title must be set!");
        
        //if (this.PARENT <0)
        //    throw new ArgumentException("PARENT title must be set!");
    }
    public static void  DeleteSiteMapByID(int id)
    {
        eMenikDataContext db = new eMenikDataContext();
        SITEMAP smap;


        // delete all module content
        CONTENT.DeleteContentBySitemapID(id);
        ARTICLE.DeleteContentBySitemapID(id);

        // delete all CWPs
        CMS_WEB_PART.DeleteCWPsBySitemap(id); 
        
        // delete simteap modules
        DeleteModulsForSitemapModule(id);

        // delete groups
        //MODULE_GROUP.DeleteGroupBySitemapID(id);

        // delete sitemap langs
        DeleteSiteMapLangsBySitemapID(id);


        // delete sitemap
        smap = (from sm in db.SITEMAPs where sm.SMAP_ID == id select sm).SingleOrDefault();
        db.SITEMAPs.DeleteOnSubmit(smap);
        db.SubmitChanges();
    }
    public static void DeleteSiteMapLangsBySitemapID(int id)
    {
        eMenikDataContext db = new eMenikDataContext();
        List<SITEMAP_LANG> smap;

        smap = (from sm in db.SITEMAP_LANGs where sm.SMAP_ID == id select sm).ToList() ;
        db.SITEMAP_LANGs.DeleteAllOnSubmit(smap);
        db.SubmitChanges();
    
    }



    public static List<SITEMAP_LANG>  GetSitemapLangsForUser(Guid uid)
    {
        eMenikDataContext db = new eMenikDataContext();

        var cont = (from u in db.EM_USERs
                    from uc in db.EM_USER_CULTUREs
                    from c in db.CULTUREs
                    from m in db.MENUs
                    from sml in db.SITEMAP_LANGs
                    from s in db.SITEMAPs
                    where u.UserId == uid && uc.UserId == u.UserId && uc.DEFAULT == true && uc.ACTIVE == true && c.LCID == uc.LCID
                    && m.USERNAME == u.USERNAME & s.MENU_ID == m.MENU_ID && s.SMAP_ACTIVE == true && sml.SMAP_ID == s.SMAP_ID && sml.LANG_ID == c.LANG_ID 
                    && sml.SML_ACTIVE == true
                    orderby uc.DEFAULT descending ,  c.ORDER, s.SMAP_ORDER
                    select sml );

        return cont.ToList();
    }





    // sitemap modules!!!

    public static IEnumerable<SITEMAP_MODULE> GetModulsForSitemapModuls(int smapID)
    {
        eMenikDataContext db = new eMenikDataContext();

        return db.SITEMAP_MODULEs.Where(pm => pm.SMAP_ID == smapID);
    }



    public static void DeleteModulsForSitemapModule(int smapID)
    {
        eMenikDataContext db = new eMenikDataContext();
        var smapMods = db.SITEMAP_MODULEs.Where(pm => pm.SMAP_ID == smapID);

        db.SITEMAP_MODULEs.DeleteAllOnSubmit(smapMods);
        db.SubmitChanges();

    }

    public static void UpdatePageModule(int smapID, int pageModule)
    {
        eMenikDataContext db = new eMenikDataContext();
        SITEMAP smap = db.SITEMAPs.SingleOrDefault(pm => pm.SMAP_ID == smapID);

        if (smap == null) return;


        smap.PMOD_ID = pageModule;
        db.SubmitChanges();

        // delete from cache
        SM.EM.BLL.BizObject.PurgeCacheItems(SM.EM.Caching.Key.PageModule(smapID));

    }

    public static void UpdateCustomClass(int smapID, string customclass)
    {
        eMenikDataContext db = new eMenikDataContext();
        SITEMAP smap = db.SITEMAPs.SingleOrDefault(pm => pm.SMAP_ID == smapID);

        if (smap == null) return;

        smap.CUSTOM_CLASS = customclass ;
        db.SubmitChanges();
    }



    public static void InsertSitemapLang(int smapID, string lang, string title, string desc, bool active, string metadesc, string keywords, int order)
    {
        eMenikDataContext db = new eMenikDataContext();
        SITEMAP_LANG r = db.SITEMAP_LANGs.SingleOrDefault(pm => pm.SMAP_ID == smapID && pm.LANG_ID == lang );
        if (r != null) return ;

        r = new SITEMAP_LANG ();

        r.SMAP_ID = smapID;
        r.LANG_ID = lang;
        r.SML_TITLE = title;
        r.SML_DESC= desc;
        r.SML_ACTIVE = active;
        r.SML_META_DESC = metadesc;
        r.SML_KEYWORDS = keywords;
        r.SML_ORDER = order;
        db.SITEMAP_LANGs.InsertOnSubmit(r);

        db.SubmitChanges();
    }


    public class Data {

        public class SitemapLang {
            public int SMAP_ID { get; set; }
            public string LANG_ID { get; set; }
            public string SML_TITLE { get; set; }
            public string SML_DESC { get; set; }
            public string SML_META_DESC { get; set; }
            public string SML_KEYWORDS { get; set; }
            public int SML_ORDER { get; set; }
            public bool SML_ACTIVE { get; set; }

            public int MENU_ID { get; set; }
            public Guid SMAP_GUID { get; set; }
            public int PMOD_ID { get; set; }
            public int? PARENT { get; set; }
        
        
        }
    
    }

}
