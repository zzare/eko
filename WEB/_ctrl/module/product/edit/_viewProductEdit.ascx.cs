﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _ctrl_module_product_edit_viewProductEdit : BaseControlView
{

    public object Data;
    public object[] Parameters;
    public vw_Product  Prod;
    public SITEMAP_LANG SitemapLang;

    protected List<PRODUCT_GROUP_ATTRIBUTE> ProductGroupAttList;
    protected List<SITEMAP_LANG > ProductSmapList;

    public bool ShowMainImgID { get { return SM.EM.Security.Permissions.IsPortalAdmin(); } }


    protected void Page_Load(object sender, EventArgs e)
    {
        LoadData();
    }



    public void LoadData()
    {



        if (Data  == null)
            return;
        Prod = (vw_Product)Data;

        if (Parameters != null)
        {
            if (Parameters[0] != null)
                SitemapLang = (SITEMAP_LANG)Parameters[0];

            //if (Parameters.Length > 1 && Parameters[1] != null)
            //    LangID = Parameters[1].ToString();
        }

        // load attributes
        ProductRep prep = new ProductRep();
        ProductGroupAttList = prep.GetProductGroupAttributesByProduct(Prod.PRODUCT_ID).ToList();
        
        lvListAttr.DataSource = prep.GetGroupAttributesByPage(Prod.PageId);
        lvListAttr.DataBind();

        // load additional categories
        ProductSmapList = prep.GetProductSitemapLangs(Prod.PRODUCT_ID, SitemapLang.LANG_ID).ToList();
        lvProductSmap.DataSource = prep.GetSitemapLangsAdditionalByPM(Prod.PageId, PAGE_MODULE.Common.PRODUCT_ADDITIONAL , SitemapLang.LANG_ID);
        lvProductSmap.DataBind();



    }

    protected string RenderChecked( object o) {
        string ret = "";

        if (ProductGroupAttList == null)
            return ret;

        GROUP_ATTRIBUTE ga = (GROUP_ATTRIBUTE )o;

        if (ProductGroupAttList.Exists(w => w.GroupAttID == ga.GroupAttID))
            ret = " checked ";

        return ret;    
    
    }


    protected string RenderCheckedProdSmap(object o)
    {
        string ret = "";
        if (ProductSmapList == null)
            return ret;

        SITEMAP_LANG ga = (SITEMAP_LANG)o;

        if (ProductSmapList.Exists(w => w.SMAP_ID  == ga.SMAP_ID ))
            ret = " checked ";

        return ret;
    }

    protected string RenderCheckedDDV(double d)
    {
        if (d == Prod.TAX_RATE)
            return "selected";

        return "";
    }

}
