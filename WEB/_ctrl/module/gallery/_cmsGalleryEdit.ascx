﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_cmsGalleryEdit.ascx.cs" Inherits="SM.EM.UI.Controls._cmsGalleryEdit" %>
<%@ Register Namespace="SM.UI.Controls" TagPrefix="SM" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="CMS" TagName="ImageSelect" Src="~/cms/controls/gallery/_cmsImageSelect.ascx"  %>

<span class="modPopup_mainTitle" >Uredi naziv in opis galerije za različne jezike</span>
<div runat="server" visible = "false" >
    Main image:
    <br />
    <img id="imgMain" runat="server" />
    <asp:HiddenField ID="hfImgID" runat="server" />
    
    <asp:LinkButton ID="btSelectImage" OnClick="btSelectImage_OnClick" runat="server">select image</asp:LinkButton>
    <CMS:ImageSelect ID="objImageSelect" runat="server" />
    
</div>

<div >
    <SM:smListView ID="lvList" DataKeyNames="GAL_ID,LANG_ID" runat="server"  InsertItemPosition="None" >
        <LayoutTemplate>
            <asp:PlaceHolder id="itemPlaceholder" runat="server"></asp:PlaceHolder>
        </LayoutTemplate>
        
        <ItemTemplate>
            <div style="float:left; width:260px;">
                <asp:HiddenField ID="hfLang" Value='<%#  Eval("LANG_ID")%>' runat="server" />
                   
                <div style="color:#659700; font-size:15px; margin-bottom:4px;"> <%#  Eval("LANG_DESC")%></div>
                <span style="font-size:14px;">Naziv galerije</span>
                <br />
                <asp:TextBox ID="tbTitle" CssClass="modPopup_TBXsettings modPopup_TBXsettingsGalLang" runat="server" MaxLength="256" Text='<%#  Eval("GAL_TITLE")%>'></asp:TextBox>
                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1"  WatermarkText="Tukaj vpiši naziv galerije" TargetControlID ="tbTitle" WatermarkCssClass="modPopup_mainWatermark modPopup_mainWatermarkGalLang" runat="server" ></cc1:TextBoxWatermarkExtender>
                <br />
                <span style="font-size:14px;">Opis galerije</span> 
                <br />
                <asp:TextBox ID="tbDesc" CssClass="modPopup_TBXsettings modPopup_TBXsettingsGalLang" runat="server" MaxLength="512" TextMode="MultiLine" Rows="3" Text='<%#  Eval("GAL_DESC")%>'></asp:TextBox>
                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2"  WatermarkText="Tukaj vpiši opis galerije" TargetControlID ="tbDesc" WatermarkCssClass="modPopup_mainWatermark modPopup_mainWatermarkGalLang" runat="server" ></cc1:TextBoxWatermarkExtender>
                <br />
            </div>
        </ItemTemplate>
    </SM:smListView>
</div>

<br />

<div runat="server" visible = "false">
    edit image types:
    <br />
    
    <SM:smListView ID="lvImageType" runat="server"  InsertItemPosition="None" >
        <LayoutTemplate>
            <asp:PlaceHolder id="itemPlaceholder" runat="server"></asp:PlaceHolder>
        </LayoutTemplate>
        
        <ItemTemplate>
            <div style="display:inline;">
                <%# Eval("TYPE_TITLE") %> [ <%# RenderDimensions(Eval("TYPE_ID"))%> ]
                <br />
            
            </div>
        
        </ItemTemplate>
    </SM:smListView>
    
    <br />
    dodaj nov tip:
    <asp:DropDownList ID="ddlType" runat="server" AppendDataBoundItems="true">
        
    </asp:DropDownList>
    <asp:LinkButton OnClick="btAddType_Click" ID="btAddType" runat="server" class="">dodaj NOV TIP</asp:LinkButton>
    
    
    

</div>
