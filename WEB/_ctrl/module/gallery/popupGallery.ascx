﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="popupGallery.ascx.cs" Inherits="SM.EM.UI.Controls.popupGallery" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Namespace="SM.UI.Controls" TagPrefix="SM"  %>


<SM:InlineScript ID="InlineScript2"  runat="server">
<script type="text/javascript">
    var currIndex = 0;
    var min = 0;
    var max = 1;
    var thumbCount ;
    var data;
    var loadCount = 0;
    function OpenGalleryPopup(cwp, index) {
        currIndex = index;
        scroll(0, 0);
        $find("mpeGalleryPopup").show();
        $("#cPopupGal").hide();
        $("#sProgress").show();
        WebServiceP.GetImagesLang(cwp, index, em_g_cult, LoadGallery);

    }
    function CloseGalleryPopup() {
        $find("mpeGalleryPopup").hide();
    }    

    function LoadGallery(d) {

        $('#mpeGalleryPopup_backgroundElement').click(function() { CloseGalleryPopup(); });

        data = d.Data; 
        LoadThumbs();
        $("#galTitle").html(d.GalleryTitle);
        $("#galDesc").html(d.GalleryDesc);
        LoadMainImage();
    }

    function LoadMainImage() {
        
        var imgB = $("#imgGalBig")[0];
        if (data[currIndex]) {
            var pr = $("#cProgress");
            var cont = $("#cImgContent");

            
            
            pr.height(cont.height());
            cont.hide().css('display', 'none');
            pr.show().css('display', 'block');

            if (imgB.complete)
                OnImageLoaded(imgB);
            else
                $(imgB).load(function() { OnImageLoaded(imgB); });
            imgB.src = data[currIndex].UrlBig;
            imgB.alt = data[currIndex].Title;
            $("#txtTitle").html(data[currIndex].Title);
            $("#txtDesc").html(data[currIndex].Desc);

            if (currIndex == 0) $("a.aPrev").hide();
            else $("a.aPrev").show();

            if (currIndex < data.length - 1) $("a.aNext").show();
            else $("a.aNext").hide();

        }
    }

    function LoadThumbs() {
        $("#cGalThumb").empty();

        loadCount = 0;
        for (var i = 0; i < data.length; i++) {
            var li = $("<li></li>");
            var a = $("<a ></a>").attr({ id: 'aC' + i, href: "javascript:SelectImage(" + i + ");" });
            var img = $("<img  />").css('visibility', 'visible');
            img.addClass('galImgThumb');
            //if (i == currIndex || i == 0) // add to first also and remove later - hack
            img.addClass('selThumb');
            //else
            //  img.removeClass('selThumb');

            $(img).load(function() { loadCount++; loadCarousel(); });
            img.attr({ src: data[i].UrlThumb, alt: data[i].Title });
            $(a).append(img);
            $(li).append(a);
            $("#cGalThumb").append(li);

        }
        loadCarousel();
    }
    function ScrollThumbs(pos) {
        var scrlIndex  = pos;
        if (scrlIndex < 0) scrlIndex = 0;
        $("#" + scrlIndex).click();

        $("#cGalThumb img.galImgThumb").removeClass('selThumb').eq(currIndex).addClass('selThumb');
    }
    function loadCarousel() {

        if (loadCount < data.length) return;

        var btgo = new Array();
        for (var i = 0; i < data.length; i++) {
            btgo[i] = "#" + i;
            var a = $("<a ></a>").attr({ id: i + '' }).hide();
            $("#cGalImgBig").append(a);
        }
    
        var strt = currIndex - 4;
        if (strt < 0) strt = 0; 

        $("#cPopupGal").show();
        $("#sProgress").hide();

        $(".jCarouselGal").jCarouselLite({
            start: strt,
            scroll: 1,
            btnNext: "#cNext",
            btnPrev: "#cPrev",
            speed: 500,
            easing: "easeOutQuint", 
            circular: false,
            visible: 8,
            btnGo: btgo//,
//            beforeStart: function(a) { canMove = false; },
//            afterEnd: function(a) { canMove = true; }
        });

        $("#cGalThumb img.galImgThumb").removeClass('selThumb').eq(currIndex).addClass('selThumb');

        // reposition hover - first time load
        OnImageLoaded($("#imgGalBig")[0]);

    }

    function OnImageLoaded(img) {

        if (img) {
            $("#cProgress").hide().css('display','none');
            $("#cImgContent").show().css('display', 'block');

            var hov = $('#galHovNav')[0];
            hov.style.height = img.height + 'px';
            hov.style.top = img.offsetTop + 'px';
            hov.style.left = '26px';
            hov.style.width = '768px';            

        }
    }

    function SelectImage(index) {
        currIndex  = index ;
        LoadMainImage();

        ScrollThumbs(currIndex-4);
    }
    function LoadImage(step) {
        currIndex += step;
        if (currIndex > data.length - 1)
            currIndex = data.length - 1;
        if (currIndex < 0)
            currIndex = 0;

        ScrollThumbs(currIndex-4);

        LoadMainImage();

        return false;
    }

    $(document).ready(function() {
        thumbCount = $("#cGalThumb a").each(function(i) {
            $(this).click(function() { SelectImage(i); })
        }).length;
    });

    $(document).keydown(function(e) {
        if (e.which == 27) { CloseGalleryPopup(); }
    });
    

</script>
</SM:InlineScript>


 




<asp:HiddenField ID="hfPopup" runat="server" />
<asp:Panel CssClass="modPopupGal"  ID="panSettings" runat="server" style="display:none;">

<div id="cPopupGal">    
    <div class="galData">
        <span id="galTitle"></span>    <br />
        <span id="galDesc"></span>
    </div>
    
    <div class="jCarousel">
        <div class="jCarouselLeft"><a id="cPrev" href="#" class="jCarouselPrevious"></a></div>
        <div class="jCarouselCenter">
            <div  class="jCarouselGal"> 
                <ul id="cGalThumb" >
                    <%-- tukej se not nafilajo slike  --%> 
                </ul>
            </div> 
        </div>
        <div class="jCarouselRight"><a id="cNext" href="#" class="jCarouselNext" ></a></div>
        <div class="clearing"></div>
    </div>
    <br />
    <div class="imagePN">
        <div class="imagePNleft">
            <a class="pPrev aPrev" onclick="LoadImage(-1); return false" href="#" ><%= GetGlobalResourceObject("Modules", "PreviousPhoto")%></a>
        </div>
        <div class="imagePNright">
             <a class="pNext aNext" onclick="LoadImage(1); return false" href="#" ><%= GetGlobalResourceObject("Modules", "NextPhoto")%></a>
        </div>
        <div class="clearing"></div>
    </div>
    <br />
    
    <div id="cImgContent">
        <%-- big image  --%> 
        <div id="cGalImgBig">
            <div class="cGalImgBigImg" >
                <img id="imgGalBig" />
<%--                <img onload="OnImageLoaded(this)" id="img1" />--%>
                
            </div>
        </div>
        <%-- image hover buttons  --%> 
        <div id="galHovNav" >
            <a id="hovPrev" class="aPrev" title="&laquo; <%= GetGlobalResourceObject("Modules", "PreviousPhoto") %>" onclick="LoadImage(-1); return false" href="#"  ><span>&nbsp;</span></a>
            <a id="hovNext" class="aNext" title="<%= GetGlobalResourceObject("Modules", "NextPhoto") %> &raquo;"  onclick="LoadImage(1); return false" href="#" ><span>&nbsp;</span></a>
        </div>
        
        <div class="txtData">
            <span id="txtTitle"></span><br />
            <span id="txtDesc"></span>
        </div>                            
        
        <div class="btnwrappModalGallery">
            <asp:LinkButton ID="btCancelEdit" runat="server" CssClass="btClose"></asp:LinkButton>
        </div>
    </div>
    
    <div id="cProgress">
        <img id="imgProgress" style="margin-top:200px" src='<%= Page.ResolveUrl("~/cms/_res/images/icon/ajax-loader.gif") %>' />
        
    </div>
    
</div>
<div id="sProgress">
    <img  src='<%= Page.ResolveUrl("~/cms/_res/images/icon/ajax-loader.gif") %>' />
    <div class="btnwrappModalGallery">
        <a class="btClose" href="javascript:CloseGalleryPopup()" ></a>
    </div>
</div>        
</asp:Panel>

           
<cc1:ModalPopupExtender RepositionMode="RepositionOnWindowResize" Y ="10"  BehaviorID="mpeGalleryPopup"  ID="mpeGalleryPopup" runat="server" TargetControlID="hfPopup" PopupControlID="panSettings"
                        BackgroundCssClass="modBcgGal" CancelControlID="btCancelEdit" OnCancelScript="CloseGalleryPopup" >
</cc1:ModalPopupExtender> 

