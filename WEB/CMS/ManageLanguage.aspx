﻿<%@ Page Language="C#" MasterPageFile="~/CMS/CMS.master" AutoEventWireup="true" CodeFile="ManageLanguage.aspx.cs" Inherits="SM.EM.UI.CMS.ManageLanguage" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cph" Runat="Server">
    
    <asp:LinqDataSource ID="ldsLanguages" runat="server" 
        ContextTypeName="eMenikDataContext" EnableDelete="True" EnableInsert="True" 
        EnableUpdate="True" TableName="LANGUAGEs" >
    </asp:LinqDataSource>
    
    <asp:DetailsView ID="dvLanguage" runat="server" AutoGenerateRows="False" 
        DataKeyNames="LANG_ID" DataSourceID="ldsLanguages" DefaultMode="Insert" 
        oniteminserted="dvLanguage_ItemInserted"         >
        <Fields>
            <asp:BoundField DataField="LANG_ID" HeaderText="LANG_ID" ReadOnly="True" 
                SortExpression="LANG_ID" />
            <asp:BoundField DataField="LANG_DESC" HeaderText="LANG_DESC" 
                SortExpression="LANG_DESC" />
            <asp:BoundField DataField="LANG_ORDER" HeaderText="LANG_ORDER" 
                SortExpression="LANG_ORDER" />
            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" 
                ShowInsertButton="True" />
        </Fields>
    </asp:DetailsView>
    
    <asp:GridView ID="gvLanguage" runat="server" AllowPaging="True" 
        AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="LANG_ID" 
        DataSourceID="ldsLanguages" onrowupdated="gvLanguage_RowUpdated" >
        <Columns>
            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" 
                ShowSelectButton="False" />
            <asp:BoundField DataField="LANG_ID" HeaderText="LANG_ID" ReadOnly="True" 
                SortExpression="LANG_ID" />
            <asp:BoundField DataField="LANG_DESC" HeaderText="LANG_DESC" 
                SortExpression="LANG_DESC" />
            <asp:BoundField DataField="LANG_ORDER" HeaderText="LANG_ORDER" 
                SortExpression="LANG_ORDER" />
        </Columns>
    </asp:GridView>
</asp:Content>

