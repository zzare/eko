﻿<%@ Page Language="C#" MasterPageFile="~/CMS/CMS.master" AutoEventWireup="true" CodeFile="ManageRoles.aspx.cs" Inherits="SM.EM.UI.CMS.CMS_ManageRoles" Title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Namespace="SM.UI.Controls" TagPrefix="SM" %>

<asp:Content ID="Content3" ContentPlaceHolderID="cph" Runat="Server">

    <asp:UpdatePanel ID="upList" runat="server" ChildrenAsTriggers="true">
        <ContentTemplate>

            <SM:smListView ID="lvList" runat="server" 
                 InsertItemPosition="LastItem" 
                onitemcanceling="lvList_ItemCanceling" onitemediting="lvList_ItemEditing" 
                onitemupdated="lvList_ItemUpdated" 
                oniteminserting="lvList_ItemInserting"  OnItemDeleting="lvList_ItemDeleteing" >
                <LayoutTemplate>
                    <table  class="tblData">
                            <thead>
                                <tr id="headerSort" runat="server">
                                    <th ><asp:LinkButton ID="LinkButton1" CommandName="sort" CommandArgument="Container.DataItem" runat="server">Role</asp:LinkButton></th>
                                    <th colspan="1">Manage</th>
                                </tr>
                            </thead>
                            <tbody id="itemPlaceholder" runat="server"></tbody>
                            <tfoot>
                                <tr>
                                    <th style="text-align:right" colspan="2">
                                        <asp:DataPager runat="server" ID="DataPager" PageSize="100">
                                            <Fields>
                                                <asp:NextPreviousPagerField  />
                                                <asp:NumericPagerField ButtonCount="5" />
                                            </Fields>
                                        </asp:DataPager>
                                    </th>
                                </tr>
                            </tfoot>
                        </table>
                </LayoutTemplate>
                
                <ItemTemplate>
                    <tr  class='<%#  Container.DataItemIndex % 2 == 0 ? " " : " odd " %>'  onmouseover ="this.className='<%#  Container.DataItemIndex % 2 == 0 ? "hovRow" : "hovRowodd" %>'" onmouseout ="this.className = GetCssClass(<%#  Container.DataItemIndex %>)">
                        <td>
                            <asp:Literal ID="litRole" runat="server" Text='<%# Container.DataItem%>'></asp:Literal>
                        </td>
                        <td>
                            <asp:LinkButton ID="lbDelete" OnClientClick="javascript: return confirm('Are you sure you want to delete this item?');" CommandName="Delete" runat="server">delete</asp:LinkButton>
                        </td>                
                    </tr>        
                
                </ItemTemplate>
                
                <InsertItemTemplate>
                    <tr class="insertRow">
                        <td>
                            <asp:TextBox MaxLength="256" Width="99%" ID="tbRole" runat="server" Text='<%# Bind("Role") %>'></asp:TextBox>
                        </td>
                        <td colspan="1">
                            <asp:Button ID="btSave" CommandName="Insert" runat="server" Text="Add new" ValidationGroup="insert" ></asp:Button>
                        </td>
                    </tr>     
                    <tr class="errorRow">
                        <td colspan="2">
                            <asp:RequiredFieldValidator ValidationGroup="insert" ID="reqState" ControlToValidate="tbRole" Display="Dynamic"  runat="server" ErrorMessage="* Enter role name"></asp:RequiredFieldValidator>
                            <asp:CustomValidator ValidationGroup="insert" ID="cvState" ControlToValidate="tbRole" Display="Dynamic" runat="server" ErrorMessage="* Role already exists" OnServerValidate="ValidateDuplicates"></asp:CustomValidator>
                        </td>
                    </tr>   
                </InsertItemTemplate>
                
            </SM:smListView>

        </ContentTemplate>
    </asp:UpdatePanel>


</asp:Content>


