﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_cntCommentEdit.ascx.cs" Inherits="SM.EM.UI.Controls._cntCommentEdit" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>




        
<asp:PlaceHolder ID="phConfirmation" runat="server" EnableViewState="false" Visible = "false">
    <h2>Vaš komentar je bil poslan.</h2>
</asp:PlaceHolder>

<asp:PlaceHolder ID="phLogin" runat="server" >
    <h3>Pred komentiranjem se prosim prijavite. Še niste registrirani? Registrirajte se <a href='<%= Page.ResolveUrl("~/") %>'>tukaj brezplačno</a></h3>
    
    <br />
</asp:PlaceHolder>

<asp:PlaceHolder ID="phComment" runat="server" >


<div class="comment_cfW">
 <% if (SM.EM.Security .Permissions.CanAddBlog()) {%>
            <table class="tbl_contact_form validationGroup" border="0" cellpadding="0" cellspacing="0">
                <tbody>
                    <%--<tr>
                        <td class="td_input td_nopadd"><input title="ime in priimek" class="val CoFoName wm"   name="CoFoName" type="text" maxlength="256"  /></td>
                        <td class="td_error"></td>
                    </tr>
                    <tr>
                        <td class="td_input td_nopadd"><input title="elektronski naslov" class="val CoFoEmail wm"    name="CoFoEmail" type="text" maxlength="64"  /></td>
                        <td class="td_error"></td>
                    </tr>--%>

                    <tr id="trName" runat="server">
                        <td class="">Ime:</td>
                        <td>
                            <asp:TextBox ValidationGroup="vgComm" ID="tbName" runat="server"  CssClass="TBXcontactform"></asp:TextBox><br />
                        </td>
                    </tr>
             <%--       <tr class="tr_validation">
                        <td></td>
                        <td>
                            <asp:RequiredFieldValidator EnableClientScript="true" ValidationGroup="vgComm" id="valName" ControlToValidate="tbName" runat="server" ErrorMessage="Vpiši ime " Display="Dynamic"></asp:RequiredFieldValidator>
                            &nbsp;
                        </td>
                    </tr>--%>
       
        
                    <tr id="trEmail" runat="server" onmousedown="return trEmail_onmousedown()">
                        <td class="">Email:</td>
                        <td>
                            <asp:TextBox ID="tbEmail" runat="server" CssClass="TBXcontactform"></asp:TextBox>
                        </td>
                    </tr>

                    <tr id="trWWW" runat="server">
                        <td class=""  >Spletna stran:</td>
                        <td>
                            <asp:TextBox  ID="tbWWW" runat="server" CssClass="TBXcontactform"></asp:TextBox>
                        </td>
                    </tr>


                    <tr id="trTitle" runat="server">
                        <td class=""  >Naslov:</td>
                        <td>
                            <asp:TextBox  ID="tbSubject" runat="server" CssClass="TBXcontactform"></asp:TextBox><br />
                            <%--<asp:RequiredFieldValidator  ValidationGroup="vgComm" id="valSubject" ControlToValidate="tbSubject" runat="server" ErrorMessage="Vpiši naslov" Display="Dynamic"></asp:RequiredFieldValidator>--%>
                        </td>
                    </tr>


                    <tr>
                        <%--<td class="td_text td_veralign"><span>Moj predlog*:</span></td>--%>
                        <td class="td_input td_nopadd">
                            <%--<textarea title="komentar" rows="4" class="val CoFoText wm"   name="CoFoText"></textarea>--%>
                            <asp:TextBox  ID="tbBody" runat="server" TextMode="MultiLine" Rows="4" CssClass=""></asp:TextBox>
                        </td>
                        <td class="td_error"> <asp:RequiredFieldValidator ValidationGroup="vgComm" id="valBody" ControlToValidate="tbBody" runat="server" ErrorMessage="<%$ resources:Modules, CommentsAddError %>" Display="Dynamic"></asp:RequiredFieldValidator></td>
                    </tr>
                </tbody>
            </table>

        
        <div class="contact_form_btnW">
           <%-- <a class="btn_send" href="#" ><span class="font_bevan">OBJAVI</span></a>--%>
          
             <asp:LinkButton CssClass="btn_send" ID="btSend" runat="server" OnClick="btSend_Click" ValidationGroup="vgComm" ><span class="font_bevan"><%= GetGlobalResourceObject("Modules", "CommentsButton") %></span></asp:LinkButton>
              
        </div>
        <div class="clearing"></div>


        <%} else {%>
         <div class="contact_form_btnW">
            <a class="btn_send "  href="<%= SM.BLL.Common.ResolveUrl(SM.EM.Rewrite.EmenikCustomerLogin(SM.BLL.Custom.MainUserID, ParentPage.GetReturnUrl()))%>"><span class="font_bevan"><%= GetGlobalResourceObject("Modules", "LoginTitle").ToString().ToUpper() %></span></a>
         </div>
         <div class="clearing"></div>
        <%} %>

</div>



<%--
    <table class="TABLEcontactform">


        <tr>
            <td class="td_first" style="vertical-align:top;">Komentar:</td>
            <td>
                <asp:TextBox  ID="tbBody" runat="server" TextMode="MultiLine" Rows="6" CssClass="TBXcontactform"></asp:TextBox><br />
            </td>
        </tr>
        <tr class="tr_validation">
            <td></td>
            <td>
                <asp:RequiredFieldValidator ValidationGroup="vgComm" id="valBody" ControlToValidate="tbBody" runat="server" ErrorMessage="Vpiši komentar" Display="Dynamic"></asp:RequiredFieldValidator>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td></td>
            <td>
                <asp:LinkButton ID="btSend" runat="server" OnClick="btSend_Click" ValidationGroup="vgComm" >Pošlji komentar</asp:LinkButton>
            </td>
        </tr>
    </table--%>
</asp:PlaceHolder>    