using System;
using System.Collections;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace SM.EM.UI
{
    public class BaseWebPart : BaseControl_EMENIK, IWebPart  
    {


        public bool AllowDelete = false;
        public bool AllowMinimize = false;

        private string _catalogIconImageUrl = "";
        public string CatalogIconImageUrl
        {
            get { return _catalogIconImageUrl; }
            set { _catalogIconImageUrl = value; }
        }

        private string _description = "";
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        protected string _subTitle = "";
        public string Subtitle
        {
            get { return _subTitle; }
            set { _subTitle = value; }
        }

        protected string _title = "";
        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }

        private string _titleIconImageUrl = "";
        public string TitleIconImageUrl
        {
            get { return _titleIconImageUrl; }
            set { _titleIconImageUrl = value; }
        }

        private string _titleUrl = "";
        public string TitleUrl
        {
            get { return _titleUrl; }
            set { _titleUrl = value; }
        }

        public bool IsTitleVisible
        {
            get;
            set;
        }

        protected int _dbID = -1;
        [Personalizable(PersonalizationScope.Shared), WebBrowsable, WebDisplayName("User control DB ID"), WebDescription("User control DB ID")]
        public int DbID
        {
            get { return _dbID; }
            set { _dbID = value; }
        }

        protected int _modID = -1;
        [Personalizable(PersonalizationScope.Shared), WebBrowsable, WebDisplayName("User module ID"), WebDescription("User module ID")]
        public int ModID
        {
            get { return _modID; }
            set { _modID = value; }
        }

        public virtual  void BindData() { 
        
        
        }



    }
}
