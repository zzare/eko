﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Net.Mail;
using SM.EM.UI;

public partial class _em_content_membership_forgot_password : SM.EM.UI.BasePage_EMENIK
{
    protected override void OnPreInit(EventArgs e)
    {
        //if (PageName == "")
        //    PageName = "eko";


        base.OnPreInit(e);
        //this.PageModule = -11;
    }
    protected override void OnInit(EventArgs e)
    {
        CurrentSitemapID = -1;
        base.OnInit(e);

    }

    protected void Page_Load(object sender, EventArgs e)
    {


        



    }

    protected override void OnPreRender(EventArgs e)
    {
        Page.Title = GetLocalResourceObject("Title").ToString() +  " | " + TitleRoot;
        base.OnPreRender(e);
    }

    protected void OnSendingMail(object sender, MailMessageEventArgs e)
    {
        // cancel auto sending mail
        e.Cancel = true;

        // enable ssl
        SmtpClient client = new SmtpClient();




        // eko: reset pass
        string uName = "";
        uName  = ((TextBox  )PasswordRecovery1.UserNameTemplateContainer .FindControl("UserName")).Text;

        MembershipUser user = Membership.GetUser(uName);
        if (user != null) { 
        
            // get customer
            CUSTOMER c = CUSTOMER.GetCustomerByID( new Guid( user.ProviderUserKey.ToString()));
            if (c != null && !string.IsNullOrWhiteSpace( c.GeneratedPass ))
            {
                user.ChangePassword(user.ResetPassword(), c.GeneratedPass);
            }
        }
        

        try
        {

            e.Message.Subject = "Pozabljeno geslo - " + SM.BLL.Common.Emenik.Data.PortalName();
            e.Message.From = new MailAddress( SM.EM.Mail.Account.Info.Email);

            // if local, use gmail, else use server SMTP
            //if (HttpContext.Current != null && HttpContext.Current.Request.IsLocal)
            //{
            //    client.Credentials = new System.Net.NetworkCredential(SM.EM.Mail.Account.Send.Email, SM.EM.Mail.Account.Send.Pass);
            //    client.Port = 587;//or use 587            

            //    client.Host = "smtp.gmail.com";
            //    client.EnableSsl = true;
            //}
            //else
            {
                // network delivery for propely setting SENDER header in mail
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.Host = "1dva3.si";
            }


            client.Send(e.Message);

            // redirect to home
            Response.Redirect(Page.ResolveUrl(SM.BLL.Custom.Url.LoginLanding) + "?fp=1" );

        }

        catch (Exception ex)
        {

            ERROR_LOG.LogError(ex, "SendConfirmationMail");

        }


    }


}
