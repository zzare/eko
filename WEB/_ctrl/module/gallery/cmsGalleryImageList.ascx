﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="cmsGalleryImageList.ascx.cs" Inherits="SM.EM.UI.Controls.cmsGalleryImageList" %>
<%@ Register Namespace="SM.UI.Controls" TagPrefix="SM"  %>


<div id="divGalleryList" class="contGal" runat="server" style="padding-top:5px;" >

        <asp:ListView ID="lvList" runat="server" InsertItemPosition="None" onitemcommand="lvList_ItemCommand"  >
            <LayoutTemplate>
                <div class="galImgList" >
                    <div id="itemPlaceholder" runat="server"></div>
                    
                   
                </div>
                    
            </LayoutTemplate>
            
            <ItemTemplate>
                <div class="divGalImgThumb" style="width:108px; height:88px;float:left; margin:0px 4px 8px 4px;">
                    <asp:PlaceHolder ID="phPreview" Visible='<%# !IsEditMode %>' runat="server">
                        <a href="javascript:void(0)" class="divGalImgThumb_thumb" title='<%# Eval("IMG_TITLE")  %>' onclick='<%# GALLERY.Common.RenderOpenGalleryPopup(CwpID, Container.DataItemIndex ) %>' ><img alt='<%# Eval("IMG_TITLE") + " | " + Eval("IMG_DESC") %>' id="Img2" runat="server" src='<%# Eval("IMG_URL") %>'  /></a>
                    </asp:PlaceHolder>

                    <asp:PlaceHolder ID="phEdit" Visible='<%# IsEditMode %>' runat="server" >
                            <asp:LinkButton style="position:absolute;" CssClass="divGalImgThumb_btnEdit" ToolTip="Uredi" ID="btEdit" Enabled='<%# IsEditMode %>' CommandName="editImage" CommandArgument='<%# Eval("IMG_ID") %>' runat="server"></asp:LinkButton>
                            <img class="imgMove" alt='<%# Eval("IMG_TITLE") + " | " + Eval("IMG_DESC") %>'  id='<%# Eval("IMG_ID") %>'  src='<%# Page.ResolveUrl( Eval("IMG_URL").ToString()) %>'  />
                            
                    </asp:PlaceHolder>                    
                </div>
            </ItemTemplate>
            
            <EmptyDataTemplate>
                
                <div class="galEmpty">
                    Vstavi slike.
            
                </div>
                
            </EmptyDataTemplate>
            
        
        </asp:ListView>

    <br />
    <div class="clearing"></div>
    <a  href="#" onclick='<%= RenderOpenGalleryPopup() %>' ><%= GetGlobalResourceObject("Modules", "OpenGallery") %></a>
    <%--<a  href="javascript:<%= RenderOpenGalleryPopup() %>"  ><%= GetGlobalResourceObject("Modules", "OpenGallery") %></a>--%>
        
        
</div>