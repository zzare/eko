﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewProductEdit.ascx.cs" Inherits="_ctrl_module_product_edit_viewProductEdit" %>

<script language = "javascript" type="text/javascript">
    function editProductSave(id) {
        var t = $("#cEditProduct").find("#tbTitle").val();
        var c = $("#cEditProduct").find("#tbCode").val();
        var loc = new locale(',', '.', 2);
        var p = parseLcNum($("#cEditProduct").find("#tbPrice").val(), loc);
        var d = parseLcNum($("#cEditProduct").find("#tbDiscount").val(), loc);
        var st = $("#cEditProduct").find("#tbStock").val();
        var m = $("#cEditProduct").find("#hfProdManufacturer").val();
        var s = $("#cEditProduct").find("#hfProdSmap").val();
        var a = $("#cEditProduct").find("#cbActive").attr('checked');
        var act = $("#cEditProduct").find("#cbAction").attr('checked');
        var sal = $("#cEditProduct").find("#cbSale").attr('checked');
        var imp = $("#cEditProduct").find("#tbImportance").val();
        var pt = $("#cEditProduct").find("#cbProductType").is(':checked');
        var _tax = parseLcNum($("#cEditProduct").find("#ddlTax").val(), loc);
        var _mImg = $("#cEditProduct").find("#tbProdMainImg").val();
        
        var _items = new Array();
        $("#prodGroupAttr input:checkbox:checked").each(function(i, val) {
            Array.add(_items, $(this).val());
            //alert(_lan + ' , ' +_desc);
        });
        var _itemsAdd = new Array();
        $("#prodSmapList input:checkbox:checked").each(function(i, val) {
            Array.add(_itemsAdd, $(this).val());
        });

        var pType = <%= PRODUCT.ProductType.NORMAL %>;
        if (pt)
            pType = <%= PRODUCT.ProductType.NO_PRICE %>;

        openModalLoading('Shranjujem...');
        WebServiceCWP.SaveProductEdit(id, t, c, p, d, st, a, _items, m, s, _itemsAdd, act, sal,  em_g_lang, imp, pType, _tax,_mImg, onProductEditSuccessSave, onProductSaveError, id);

    }

    function deleteProduct(p, del) {
        var ok = confirm('Ali res želite zbrisati izbran izdelek? Izdelek bo zbrisan iz baze');
        if (!ok)
            return;

        openModalLoading('Brišem...');
        WebServiceCWP.DeleteProduct(p, del,em_g_lang, onProductEditSuccessSave, onSaveError, p);

    }
   

       
</script>


<div id="cEditProduct" >
    <div class="modPopup_header modPopup_headerEdit">
        <h4>UREDI ARTIKEL</h4>
        <p>Uredi artikel</p>
    </div>
    <div class="modPopup_main">
        <div style="float:left;">
            <span class="modPopup_mainTitle">Naziv</span> 
            Naziv: <input class="modPopup_TBXsettings" style="width:400px; margin-right:20px;" id="tbTitle" maxlength="256" type="text"  value='<%= Prod.NAME %>' />
        </div>
        <div style="float:left;">
            <span class="modPopup_mainTitle">Šifra</span>
            Šifra: <input class="modPopup_TBXsettings" style="width:90px; text-align:right; margin-right:20px;" id="tbCode" maxlength="256"  type="text" value='<%= Prod.CODE %>' />
        </div>
        <div style="float:left;">
            <span class="modPopup_mainTitle">Aktivnost</span>
            <div style="padding-top:5px;">
            Aktivnost: <label  for="cbActive"> <input  id="cbActive" name ="cbActive" type="checkbox" <%= (Prod.ACTIVE ? "checked" : "") %> />&nbsp;&nbsp;Aktiven</label>
            </div>
        </div>
        <div class="clearing"></div>
        
      
        
        <div style="float:left;">
            <span class="modPopup_mainTitle">Cena</span> 
            Cena: <input class="modPopup_TBXsettings" style="width:80px; margin-right:3px; text-align:right;" id="tbPrice" maxlength="256"  type="text" value='<%= SM.EM.Helpers.FormatPriceEdit(Prod.PRICE)%>' />€
            &nbsp;&nbsp;&nbsp;&nbsp;
        </div>

        <div style="float:left;">
            <span class="modPopup_mainTitle">DDV</span> 
            <select id="ddlTax">
              <option <%= RenderCheckedDDV(PRODUCT.TaxRate .OTHER) %> value='<%= PRODUCT.TaxRate .OTHER %>'>20%</option>
              <option <%= RenderCheckedDDV( PRODUCT.TaxRate .FOOD) %>  value='<%= PRODUCT.TaxRate .FOOD %>'>8,5%</option>
            </select>
            &nbsp;&nbsp;&nbsp;&nbsp;
        </div>

        <div style="float:left; display:none;">
            <span class="modPopup_mainTitle">Popust [%]</span> 
            Popust: <input class="modPopup_TBXsettings" style="width:42px; margin-right:3px; text-align:right;" id="tbDiscount" maxlength="256"  type="text" value='<%= SM.EM.Helpers.FormatPriceEdit(Prod.DISCOUNT_PERCENT)%>' />%
            &nbsp;&nbsp;&nbsp;&nbsp;
        </div>
        <div style="float:left;">
            <span class="modPopup_mainTitle">Zaloga</span> 
            Zaloga: <input class="modPopup_TBXsettings" style="width:48px; text-align:right;" id="tbStock" maxlength="256"  type="text" value='<%= Prod.UNITS_IN_STOCK%>' />
        </div>
        <div style="float:left;<%= (PRODUCT.Common.EditShowSort == false) ? "display:none;" : "" %>">
            <span class="modPopup_mainTitle">Pomembnost</span> 
            <input class="modPopup_TBXsettings" style="width:80px; margin-right:3px; text-align:right;" id="tbImportance" maxlength="256"  type="text" value='<%= Prod.IMPORTANCE%>' />
            &nbsp;&nbsp;&nbsp;&nbsp;
        </div>  
        <div style="float:left;<%= (ShowMainImgID == false) ? "display:none;" : "" %>">
            <span class="modPopup_mainTitle">Vsili glavno sliko (št.)</span> 
            <input class="modPopup_TBXsettings" style="width:80px; margin-right:3px; text-align:right;" id="tbProdMainImg" maxlength="256"  type="text" value='<%= Prod.MainImgID %>' />
            &nbsp;&nbsp;&nbsp;&nbsp;
        </div>  





        <div style="float:left;<%= (PRODUCT.Common.EditShowManufacturer == false) ? "display:none;" : "" %>">
            <span class="modPopup_mainTitle">Skupina sorodnih</span> 
             <span id="prodManufacturerTitle" ><%= Prod.ManufacturerTitle  %></span> 
            <input id="hfProdManufacturer" type="hidden" value='<%=  Prod.ManufacturerID ?? Guid.Empty  %>' /> 
            <a onclick="openLookup('prodManufacturerTitle', 'hfProdManufacturer', 6, null); return false" class="Alookup" href="#"></a>
        </div>

        <div class="clearing"></div>
        <div style="display:none;">
        <span class="modPopup_mainTitle">Tip izdelka:</span> 
        <div style="padding-top:5px;">
            <label  for="cbProductType"> <input  id="cbProductType" name ="cbProductType" type="checkbox" <%= (Prod.PRODUCT_TYPE == PRODUCT.ProductType.NO_PRICE ? "checked" : "") %> />&nbsp;&nbsp;Nima cene (povpraševanje)</label>
        </div>
        </div>
        
        <div style="display:none;">
        <span class="modPopup_mainTitle">Izpostavljenost:</span> 
        <div style="padding-top:5px;">
            <label  for="cbAction"> <input  id="cbAction" name ="cbAction" type="checkbox" <%= (Prod.CAT_ACTION ? "checked" : "") %> />&nbsp;&nbsp;Akcija</label>
        </div>
        <div style="padding-top:5px;">
            <label  for="cbSale"> <input  id="cbSale" name ="cbSale" type="checkbox" <%= (Prod.CAT_SALE ? "checked" : "") %> />&nbsp;&nbsp;Dodatna izpostavljenost</label>
        </div>
        </div>

        <div style="display:block;">
        <span class="modPopup_mainTitle">Dodatni podatki:</span>
        <asp:ListView ID="lvListAttr" runat="server">
            <LayoutTemplate>
                <div id="prodGroupAttr">
                    <asp:PlaceHolder ID="itemPlaceholder" runat="server"/>
                </div>
                
            </LayoutTemplate>

            <ItemTemplate>
                <label>
                    <input <%# RenderChecked(Container.DataItem) %> type="checkbox" value='<%# Eval("GroupAttID") %>' />
                    &nbsp;&nbsp;<%# Eval("Title")%>
                </label>
                <br />
            
            </ItemTemplate>
        </asp:ListView>        
        </div>




        <span class="modPopup_mainTitle">Primarni oddelek:</span> 
        <span id="prodSmapTitle" ><%= SitemapLang.SML_TITLE %></span> 
        <input id="hfProdSmap" type="hidden" value='<%= ( SitemapLang != null) ? SitemapLang.SMAP_ID : -1  %>' /> 
        <a onclick="openLookup('prodSmapTitle', 'hfProdSmap', 2); return false" title="prebrskaj" class="Alookup" href="#"></a>
        

        <div style="<%= (PRODUCT.Common.EditShowAdditionalList == false) ? "display:none;" : "" %>">
        <span class="modPopup_mainTitle">Dodatno izpostavljeno v oddelkih:</span> 
        <asp:ListView ID="lvProductSmap" runat="server">
            <LayoutTemplate>
                <div id="prodSmapList">
                    <asp:PlaceHolder ID="itemPlaceholder" runat="server"/>
                </div>
                
            </LayoutTemplate>

            <ItemTemplate>
                <label>
                    <input <%# RenderCheckedProdSmap(Container.DataItem) %> type="checkbox" value='<%# Eval("SMAP_ID") %>' />
                    &nbsp;<%# Eval("SML_TITLE")%>
                </label>
            
            </ItemTemplate>
        </asp:ListView>        
        </div>


    </div>   

    <div class="modPopup_footer"> 
        <div class="buttonwrapperModal">
            <a class="lbutton lbuttonConfirm" href="#" onclick="editProductSave('<%= Prod.PRODUCT_ID %>'); return false"><span>Shrani</span></a>
            <a class="lbutton lbuttonEdit" href="#" onclick="closeModalLoader(); return false" ><span>Prekliči</span></a>
            <a class="lbutton lbuttonDelete" href="#" onclick="deleteProduct('<%= Prod.PRODUCT_ID %>', <%= (!Prod.DELETED).ToString().ToLower() %>); return false" ><span>ZBRIŠI ARTIKEL</span></a>            
        </div>
    </div>
</div>