﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewImageAddNew.ascx.cs" Inherits="SM.EM.UI.Controls._viewImageAddNew" %>


<script type="text/javascript" >
    



    function init_fu_image(bt, c) {
        new Ajax_upload('#' + bt, {
            action: '<%= SM.BLL.Common.ResolveUrl("~/_inc/ImageUpload.ashx")  %>' + '?c=' + c + '<%= ForceGallery %>',
            onSubmit: function(file, ext) {

                if (!(ext && /^(doc|docx|txt|jpg|png|jpeg|gif|zip)$/.test(ext))) {
                    // extension is not allowed
                    alert('Napaka: neveljavna datoteka');
                    // cancel upload
                    return false;
                }

                this.setData({
                    active: $('#<%= cbActive.ClientID %>').attr('checked'),
                    author: ''
                });

                this.disable();
                $('#' + bt).parent().find('span.fuLoading').css('visibility', 'visible').show();
            },

            responseType: 'json',
            onComplete: function(file, res) {
                if (res.Code == 0) {
                    setModalLoaderErr(res.Message, false);
                }
                //                else if (!res.Data || res.Data == null) {
                //                    setModalLoaderErr("Napaka pri prenosu datoteke. Prosim, poskusi ponovno.", false);
                //                }
                else {
                    var newItem = res.Data;
                    var cont = $("#<%= ViewID %>").find('div.itemFileList').show();
                    var fil = cont.find('span').eq(0);
                    cont.append(newItem);
                    //cont.find('input').eq(0).val(fil.html());

                    var conf = $("#<%= ViewID %>").find("span.upload-conf");
                    conf.show();
                    setTimeout(function() { conf.fadeOut(1000) }, 2000);
                    refrImgList = true;


                }
                $('#<%= ViewID %>').find('span.fuLoading').css('visibility', 'hidden').hide();
                this.enable();
            }
        });
    }



</script>  


<div id='<%= ViewID %>'>



    <div style="display:none;">
    <span class="modPopup_mainTitle" >Prikazovanje</span>

    <label  for="<%= cbActive.ClientID %>"> <input checked="checked"  id="cbActive" runat="server" name ="cbActive" type="checkbox" />&nbsp;&nbsp;Aktivna (prikazana obiskovalcem na spletni strani)</label>
   
    <br />
    <br />
    </div>
    <span class="modPopup_mainTitle" >Dodaj sliko (.jpg, .png, .gif)</span>
    <div class="buttonwrapperModal">
        <a id="fu_img_upload" href="javascript:void(0)" class="lbutton lbuttonConfirm" ><span>Izberi datoteko</span></a>
    
        <span class="fuLoading" style="display:none; width: 64px">
            <img src='<%= SM.BLL.Common.ResolveUrl("~/_inc/images/animation/loading_animation_liferay.gif") %>' alt="loading..." />
        </span>

        <span style="display:none; color:#666;" class="upload-conf">Slika je dodana</span>

    </div>   
    <div  class="itemFileList" style="display:none" >
        Dodane slike:<br />
    
    </div>  
    
</div>