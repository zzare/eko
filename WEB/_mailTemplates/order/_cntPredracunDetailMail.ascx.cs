﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;


namespace SM.UI.Controls
{
    public partial class c_user_ctrl_cntPredracunDetailMail : BaseControl 
    {

        public object Data;
        public object[] Parameters;

        //public Guid OrderID;
        //public Guid UserId { get; set; }
        public SHOPPING_ORDER Order { get; set; }
        public CUSTOMER Customer { get; set; }
        public EM_USER EmPage { get; set; }

        
        protected void Page_Load(object sender, EventArgs e)
        {

            BindData();
        }



        public void BindData()
        {

            if (Data == null)
                return;
            Order = (SHOPPING_ORDER)Data;

            if (Parameters != null)
            {
                if (Parameters.Length > 0 && Parameters[0] != null)
                    EmPage = (EM_USER)Parameters[0];
                if (Parameters.Length > 1 && Parameters[1] != null)
                    Customer = (CUSTOMER)Parameters[1];

            }


            if (Order == null)
                return;

            // init
            objPredracunDetail.UserId = Order.UserId;
            objPredracunDetail.OrderID = Order.OrderID;
            objPredracunDetail.Order = Order;
            objPredracunDetail.BindData();
             
        }






    }

}