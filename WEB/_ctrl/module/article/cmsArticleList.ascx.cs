﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SM.EM.BLL;
namespace SM.EM.UI.Controls
{
    public partial class cmsArticleList : BaseControl
    {
#region Property


        private string containerCssClass = "contArticles";
        public string ContainerCssClass
        {
            get { return this.containerCssClass; }
            set { this.containerCssClass = value; }
        }
        private string appendToTitle = "&nbsp;&raquo;";
        public string AppendToTitle
        {
            get { return this.appendToTitle; }
            set { this.appendToTitle = value; }
        }

        private bool showImage = false;
        public bool ShowImage
        {
            get { return this.showImage; }
            set { this.showImage = value; }
        }
        private bool showDesc = true;
        public bool ShowDesc
        {
            get { return this.showDesc; }
            set { this.showDesc = value; }
        }
        private int titleLength = -1;
        public int TitleLength
        {
            get { return this.titleLength; }
            set { this.titleLength = value; }
        }
        private int descLength = -1;
        public int DescLength
        {
            get { return this.descLength; }
            set { this.descLength = value; }
        }
        private int displayCount = 10;
        public int DisplayCount
        {
            get { return this.displayCount; }
            set { this.displayCount = value; }
        }
        private string articleDetailsPath = "~/c/ArticleDetails.aspx";
        public string ArticleDetailsPath
        {
            get { return this.articleDetailsPath; }
            set { this.articleDetailsPath = value; }
        }
        private bool isEditMode = false;
        public bool IsEditMode
        {
            get { return this.isEditMode; }
            set { this.isEditMode = value; }
        }
        private bool autoLoad = true;
        public bool AutoLoad
        {
            get { return this.autoLoad; }
            set { this.autoLoad = value; }
        }
        public Guid  CwpID{get;set;}

        public string PageName {get;set;}

        public bool ShowFooter { get; set; }
        public int? ImgTypeID { get; set; }

        public int PageNum
        {
            get { int ret =  (int)(ParentPage.GetViewStateValue(this, "PageNum") ?? 1);
            if (ret < 1) ret = 1;
            return ret;
            }
            set { ParentPage.SetViewStateValue (this, "PageNum", value); }
        }
        public int PageSize
        {
            get { int ret =  (int)(ParentPage.GetViewStateValue(this, "PageSize") ?? 10000 );
            if (ret < 1) ret = 10000;
            return ret;

            }
            set { ParentPage.SetViewStateValue (this, "PageSize", value); }
        }


#endregion

        public delegate void OnEditEventHandler(object sender,SM.BLL.Common.ClickIDEventArgs  e);
        public event OnEditEventHandler OnEditArticleClick;


        protected void InitData() {

            divArticles.Attributes["class"] = ContainerCssClass;
            dpArt.PageSize = PageSize;
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
                if (AutoLoad) BindData();

        }
        // bind data
        public void BindData()
        {
            //if (!IsPostBack)
                InitData();

                ArticleRep arep = new ArticleRep();
            
            // if group id is set, load articles for group only
                if (CwpID != Guid.Empty)
                    listArticles.DataSource = arep.GetArticlesByCwp(CwpID, IsEditMode).Skip(PageNum - 1).Take(PageSize);
                else// load all articles for current sitemap
                    //listArticles.DataSource = ARTICLE.GetArticles(CurrentSitemapID, IsEditMode);
                    return;

            listArticles.DataBind();

            if (listArticles.Items.Count < 1) { this.Visible = false; return; }
            else this.Visible = true;

            // show/hide prev/next buttons



        }


        protected string RenderArtHref(object item)
        {
            Data.ArticleData art = (Data.ArticleData)item;
            if (!String.IsNullOrEmpty(PageName))
                return Page.ResolveUrl(SM.EM.Rewrite.EmenikUserArticleDetailsUrl(PageName, ParentPage.CurrentSitemapID , SM.BLL.Common.PageModule.ArticleDetails, art.ART_ID, art.ART_TITLE));
            else
                return Page.ResolveUrl(ArticleDetailsPath + "?art=" + art.ART_ID.ToString() + "&smp=" + art.SMAP_ID.ToString());

        }
        protected string RenderArtTitle(object item)
        {
            Data.ArticleData art = (Data.ArticleData)item;
            return Helpers.CutString(art.ART_TITLE, TitleLength - AppendToTitle.Length) + AppendToTitle;

        }
        protected string RenderArtDesc(object item)
        {
            if (!ShowDesc) return "";

            Data.ArticleData art = (Data.ArticleData)item;
            string desc = art.ART_ABSTRACT;
            if (string.IsNullOrEmpty(desc))
                desc = art.ART_ABSTRACT;
            return Helpers.CutString(desc, DescLength ) ;

        }

        protected string RenderAuthor(object item)
        {
            Data.ArticleData art = (Data.ArticleData)item;
            string author = art.ADDED_BY;
            if (!string.IsNullOrEmpty(author))
                author = "napisal: " + author;
            return author;
        }
        protected string RenderComments(object item)
        {
            Data.ArticleData a = (Data.ArticleData)item;
            int count =  COMMENT.GetCommentsCount(a.ART_ID);

            return string.Format("komentarji: <a href=\"{0}\">{1}</a>", RenderArtHref(item) + "#comments" + a.ART_ID.ToString() , count.ToString());
        }
        protected string RenderDatePublished(object item)
        {
            Data.ArticleData a = (Data.ArticleData)item;

            DateTime date  = a.DATE_ADDED;
            if (a.RELEASE_DATE != null && a.RELEASE_DATE .Value.Date > a.DATE_ADDED.Date  )
                date = a.RELEASE_DATE .Value;

            if (date == date.Date ) // if time is not set, show only date part
               return  string.Format("{0:dddd, dd.MM.yyyy} ", date);
            else
                return string.Format("{0:dddd, dd.MM.yyyy ob HH:mm} ", date );
        }

        protected bool HasImage(object o)
        {
            Data.ArticleData art = (Data.ArticleData)o;
            if (art.IMG_ID == null)
                return false;
            return true;


        }

        void objArticleEdit_OnArticleListChanged(object sender, EventArgs e)
        {
            this.BindData();
        }


        protected void listArticles_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            if (e.CommandName == "editArticle") {
                OnEditArticleClick(this, new SM.BLL.Common.ClickIDEventArgs { ID = Int32.Parse(e.CommandArgument.ToString()) });            
            }
        }


        protected void PagerCommand(object sender, DataPagerCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Next":
                    PageNum = PageNum  + 1;
                    BindData();
                    break;
                case "Previous":
                    PageNum = PageNum - 1;
                    BindData();
                    break;
                default:
                    break;
            }
        }




    }
}
