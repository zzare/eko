﻿<%@ Page Language="C#" MasterPageFile="~/CMS/CMS.master" AutoEventWireup="true" CodeFile="ManageModule.aspx.cs" Inherits="SM.UI.CMS.EM.CMS_ManageModule" Title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Namespace="SM.UI.Controls" TagPrefix="SM" %>
<%@ Register Assembly="FredCK.FCKeditorV2" Namespace="FredCK.FCKeditorV2" TagPrefix="FCKeditorV2" %>

<asp:Content ID="Content3" ContentPlaceHolderID="cph" Runat="Server">

<%--    <asp:UpdatePanel ID="upList" runat="server" ChildrenAsTriggers="true">
        <ContentTemplate>--%>

            <asp:LinqDataSource ID="ldsImageType" runat="server" ContextTypeName="eMenikDataContext" TableName="IMAGE_TYPEs">
            </asp:LinqDataSource>

            <asp:LinqDataSource ID="ldsList" runat="server" 
                ContextTypeName="eMenikDataContext" EnableDelete="True" EnableInsert="True" 
                EnableUpdate="True" TableName="MODULEs" 
                OnSelecting="ldsList_OnSelecting" oninserting="ldsList_Inserting" 
                onupdating="ldsList_Updating">
            </asp:LinqDataSource>
            
            <SM:smListView ID="lvList" runat="server" DataSourceID="ldsList" 
                DataKeyNames="MOD_ID" InsertItemPosition="LastItem" 
                onitemcanceling="lvList_ItemCanceling" onitemediting="lvList_ItemEditing" 
                onitemupdated="lvList_ItemUpdated" onitemdatabound="lvList_ItemDataBound" >
                <LayoutTemplate>
                    <table  class="tblData">
                            <thead>
                                <tr id="headerSort" runat="server">
                                    <th width="50px"><asp:LinkButton ID="LinkButton1" CommandName="sort" CommandArgument="MOD_ID" runat="server">ID</asp:LinkButton></th>
                                    <th ><asp:LinkButton ID="LinkButton3" CommandName="sort" CommandArgument="MOD_TITLE" runat="server">Module</asp:LinkButton></th>
                                    <th ><asp:LinkButton ID="LinkButton4" CommandName="sort" CommandArgument="MOD_DESC" runat="server">Desc</asp:LinkButton></th>
                                    <th ><asp:LinkButton ID="LinkButton2" CommandName="sort" CommandArgument="CMS_ROLES" runat="server">CMS_ROLES</asp:LinkButton></th>
                                    <th ><asp:LinkButton ID="LinkButton8" CommandName="sort" CommandArgument="WWW_ROLES" runat="server">WWW_ROLES</asp:LinkButton></th>
                                    <th ><asp:LinkButton ID="LinkButton9" CommandName="sort" CommandArgument="MOD_SRC" runat="server">MOD_SRC</asp:LinkButton></th>
                                    <th ><asp:LinkButton ID="LinkButton10" CommandName="sort" CommandArgument="MOD_OBJ_ID" runat="server">MOD_OBJ_ID</asp:LinkButton></th>
                                    <th ><asp:LinkButton ID="LinkButton11" CommandName="sort" CommandArgument="EM_TITLE" runat="server">EM_TITLE</asp:LinkButton></th>
                                    <th ><asp:LinkButton ID="LinkButton5" CommandName="sort" CommandArgument="EM_NAME" runat="server">EM_NAME</asp:LinkButton></th>

                                    <th width="50px"><asp:LinkButton ID="LinkButton15" CommandName="sort" CommandArgument="EM_ACTIVE" runat="server">EM Active</asp:LinkButton></th>
                                    <th width="50px"><asp:LinkButton ID="LinkButton16" CommandName="sort" CommandArgument="EM_FREE" runat="server">EM Free</asp:LinkButton></th>
                                    <th width="50px"><asp:LinkButton ID="LinkButton17" CommandName="sort" CommandArgument="EM_PRICE" runat="server">EM Price</asp:LinkButton></th>
                                    <th width="50px"><asp:LinkButton ID="LinkButton18" CommandName="sort" CommandArgument="EM_BUY_TYPE" runat="server">EM_BUY_TYPE</asp:LinkButton></th>
                                    <th width="50px"><asp:LinkButton ID="LinkButton19" CommandName="sort" CommandArgument="EM_BUY_QUANTITY" runat="server">EM_BUY_QUANTITY</asp:LinkButton></th>
                                    <th width="50px"><asp:LinkButton ID="LinkButton6" CommandName="sort" CommandArgument="MOD_ORDER" runat="server">Order</asp:LinkButton></th>
                                    <th colspan="2">Manage</th>
                                </tr>
                            </thead>
                            <tbody id="itemPlaceholder" runat="server"></tbody>
                            <tfoot>
                                <tr>
                                    <th style="text-align:right" colspan="18">
                                        <asp:DataPager runat="server" ID="DataPager" PageSize="20">
                                            <Fields>
                                                <asp:NextPreviousPagerField  />
                                                <asp:NumericPagerField ButtonCount="5" />
                                            </Fields>
                                        </asp:DataPager>
                                    </th>
                                </tr>
                            </tfoot>
                        </table>
                </LayoutTemplate>
                
                <ItemTemplate>
                    <tr  class='<%#  Container.DataItemIndex % 2 == 0 ? " " : " odd " %>'  onmouseover ="this.className='<%#  Container.DataItemIndex % 2 == 0 ? "hovRow" : "hovRowodd" %>'" onmouseout ="this.className = GetCssClass(<%#  Container.DataItemIndex %>)">
                        <td>
                            <%# Eval("MOD_ID")%>
                        </td>
                        <td>
                            <%# Eval("MOD_TITLE")%>
                        </td>
                        <td>
                            <%# Eval("MOD_DESC") == null ? "" : SM.EM.Helpers.CutString(Eval("MOD_DESC").ToString(), 50, " ...")%>
                        </td>
                        <td>
                            <%# Eval("CMS_ROLES") == null ? "" : SM.EM.Helpers.CutString(Eval("CMS_ROLES").ToString(), 50, " ...")%>
                        </td>
                        <td>
                            <%# Eval("WWW_ROLES") == null ? "" : SM.EM.Helpers.CutString(Eval("WWW_ROLES").ToString(), 50, " ...")%>
                        </td>
                        <td>
                            <%# Eval("MOD_SRC") == null ? "" : SM.EM.Helpers.CutString(Eval("MOD_SRC").ToString(), 50, " ...")%>
                        </td>
                        <td>
                            <%# Eval("MOD_OBJ_ID") == null ? "" : SM.EM.Helpers.CutString(Eval("MOD_OBJ_ID").ToString(), 50, " ...")%>
                        </td>
                        <td>
                            <%# Eval("EM_TITLE")%>
                        </td>                        
                        <td>
                            <%# Eval("EM_NAME")%>
                        </td>                        
                        <td>
                            <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Eval("EM_ACTIVE") %>' Enabled="false" />
                        </td>
                        <td>
                            <asp:CheckBox ID="CheckBox2" runat="server" Checked='<%# Eval("EM_FREE") %>' Enabled="false" />
                        </td>
                        <td>
                            <%# SM.EM.Helpers.FormatPrice( Eval("EM_PRICE"))%>
                        </td>                         
                        <td>
                            <%# Eval("EM_BUY_TYPE")%>
                        </td>
                        <td>
                            <%# Eval("EM_BUY_QUANTITY")%>
                        </td>

                        <td>
                            <%# Eval("MOD_ORDER")%>
                        </td>
                        <td >
                            <asp:LinkButton ID="lbEdit" CommandName="Edit" runat="server">edit</asp:LinkButton>
                        </td>
                        <td>
                            <asp:LinkButton ID="lbDelete" OnClientClick="javascript: return confirm('Are you sure you want to delete this item?');" CommandName="Delete" runat="server">delete</asp:LinkButton>
                        </td>                
                    </tr>        
                
                </ItemTemplate>
                
                <InsertItemTemplate>
                    <tr class="insertRow">
                        <td colspan="2">
                            <asp:TextBox MaxLength="256" Width="99%" ID="tbTitle" runat="server" Text='<%# Bind("MOD_TITLE") %>'></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="256" Width="99%" ID="tbDesc" runat="server" Text='<%# Bind("MOD_DESC") %>'></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="512" Width="99%" ID="tbRolesCMS" runat="server" Text='<%# Bind("CMS_ROLES") %>'></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="512" Width="99%" ID="tbRolesWWW" runat="server" Text='<%# Bind("WWW_ROLES") %>'></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="512" Width="99%" ID="TextBox2" runat="server" Text='<%# Bind("MOD_SRC") %>'></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="512" Width="99%" ID="TextBox3" runat="server" Text='<%# Bind("MOD_OBJ_ID") %>'></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="512" Width="99%" ID="TextBox4" runat="server" Text='<%# Bind("EM_TITLE") %>'></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="512" Width="99%" ID="TextBox1" runat="server" Text='<%# Bind("EM_NAME") %>'></asp:TextBox>
                        </td>
                        
                        <td>
                            <asp:CheckBox ID="CheckBox4" runat="server" Checked='<%# Bind("EM_ACTIVE") %>'  />
                        </td>

                        <td>
                            <asp:CheckBox ID="CheckBox5" runat="server" Checked='<%# Bind("EM_FREE") %>'  />
                        </td>   
                        <td>
                            <asp:TextBox MaxLength="512" Width="99%" ID="tbPrice" runat="server" Text='<%# Bind("EM_PRICE") %>'></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="512" Width="99%" ID="tbBuyType" runat="server" Text='<%# Bind("EM_BUY_TYPE") %>'></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="512" Width="99%" ID="tbBuyQuantity" runat="server" Text='<%# Bind("EM_BUY_QUANTITY") %>'></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="9" Width="99%" ID="tbOrder" runat="server" Text='<%# Bind("MOD_ORDER") %>'></asp:TextBox>
                            <cc1:FilteredTextBoxExtender TargetControlID="tbOrder" ID="ftbeOrder" runat="server" FilterType="Numbers"></cc1:FilteredTextBoxExtender>
                        </td>
                        <td colspan="2">
                            <asp:Button ID="btSave" CommandName="Insert" runat="server" Text="Add new" ValidationGroup="insert" ></asp:Button>
                        </td>
                    </tr>     
                    <tr class="errorRow">
                        <td colspan="2">
                            <asp:RequiredFieldValidator ValidationGroup="insert" ID="reqState" ControlToValidate="tbTitle" Display="Dynamic"  runat="server" ErrorMessage="* Enter title"></asp:RequiredFieldValidator>
                        </td>
                        <td colspan="2">
                            <asp:RequiredFieldValidator ValidationGroup="insert" ID="RequiredFieldValidator2" ControlToValidate="tbTitle" Display="Dynamic"  runat="server" ErrorMessage="* Enter title"></asp:RequiredFieldValidator>
                        </td>
                        <td colspan="1">
                            <asp:RequiredFieldValidator ValidationGroup="insert" ID="rfvRisk" ControlToValidate="tbPrice" runat="server" ErrorMessage="* Enter price" Display="Dynamic" ></asp:RequiredFieldValidator>
                        </td>
                    </tr>   
                       
                </InsertItemTemplate>
                
                <EditItemTemplate>
                
                    <tr class="insertRow">
                        <td colspan="2">
                            <asp:TextBox MaxLength="256" Width="99%" ID="tbTitle" runat="server" Text='<%# Bind("MOD_TITLE") %>'></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="256" Width="99%" ID="tbDesc" runat="server" Text='<%# Bind("MOD_DESC") %>'></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="512" Width="99%" ID="tbRolesCMS" runat="server" Text='<%# Bind("CMS_ROLES") %>'></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="512" Width="99%" ID="tbRolesWWW" runat="server" Text='<%# Bind("WWW_ROLES") %>'></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="512" Width="99%" ID="TextBox2" runat="server" Text='<%# Bind("MOD_SRC") %>'></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="512" Width="99%" ID="TextBox3" runat="server" Text='<%# Bind("MOD_OBJ_ID") %>'></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="512" Width="99%" ID="TextBox4" runat="server" Text='<%# Bind("EM_TITLE") %>'></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="512" Width="99%" ID="TextBox5" runat="server" Text='<%# Bind("EM_NAME") %>'></asp:TextBox>
                        </td>
                        
                        <td>
                            <asp:CheckBox ID="CheckBox4" runat="server" Checked='<%# Bind("EM_ACTIVE") %>'  />
                        </td>

                        <td>
                            <asp:CheckBox ID="CheckBox5" runat="server" Checked='<%# Bind("EM_FREE") %>'  />
                        </td>   
                        <td>
                            <asp:TextBox MaxLength="512" Width="99%" ID="tbPrice" runat="server" Text='<%# Bind("EM_PRICE") %>'></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="512" Width="99%" ID="tbBuyType" runat="server" Text='<%# Bind("EM_BUY_TYPE") %>'></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="512" Width="99%" ID="tbBuyQuantity" runat="server" Text='<%# Bind("EM_BUY_QUANTITY") %>'></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="9" Width="99%" ID="tbOrder" runat="server" Text='<%# Bind("MOD_ORDER") %>'></asp:TextBox>
                            <cc1:FilteredTextBoxExtender TargetControlID="tbOrder" ID="ftbeOrder" runat="server" FilterType="Numbers"></cc1:FilteredTextBoxExtender>
                        </td>
                        <td >
                            <asp:Button ID="btSave" CommandName="Update" runat="server" Text="Save" ValidationGroup="edit" ></asp:Button>
                        </td>
                        <td >
                            <asp:Button ID="btCancel" CommandName="Cancel" runat="server" Text="Cancel" ></asp:Button>
                        </td>  
                    </tr>     
                    <tr class="errorRow">
                        <td colspan="2">
                            <asp:RequiredFieldValidator ValidationGroup="edit" ID="reqState" ControlToValidate="tbTitle" Display="Dynamic"  runat="server" ErrorMessage="* Enter title"></asp:RequiredFieldValidator>
                        </td>
                        <td colspan="2">
                            <asp:RequiredFieldValidator ValidationGroup="edit" ID="RequiredFieldValidator2" ControlToValidate="tbTitle" Display="Dynamic"  runat="server" ErrorMessage="* Enter title"></asp:RequiredFieldValidator>
                        </td>
                        <td colspan="1">
                            <asp:RequiredFieldValidator ValidationGroup="edit" ID="rfvRisk" ControlToValidate="tbPrice" runat="server" ErrorMessage="* Enter price" Display="Dynamic" ></asp:RequiredFieldValidator>
                        </td>
                    </tr>   
                    <tr class="insertRow">
                        <td colspan="18">
                            <FCKeditorV2:FCKeditor Width="100%" ID="fckBody"  runat="server" Height="300px" Value='<%# Bind("EM_DESC") %>'  ></FCKeditorV2:FCKeditor>
                        </td>
                    </tr>   
                </EditItemTemplate>
            
            </SM:smListView>

<%--        </ContentTemplate>
    </asp:UpdatePanel>
--%>

</asp:Content>


