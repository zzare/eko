﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;


namespace SM.UI.Controls
{
    public partial class _cmsNewsletterList : BaseControl 
    {
        public delegate void OnEditEventHandler(object sender, SM.BLL.Common.Args .IDguidEventArgs e);
        public event OnEditEventHandler OnEdit;
        public event OnEditEventHandler OnDelete;
        public string OnEditTarget = "_self";
        public int PageSize = 20;

        public string SearchText { get; set; }
        public bool? Active { get; set; }
        public short? Status {get; set;}
        public bool IsAdmin { get; set; }
        public short? NewsletterType { get; set; }

        
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        protected void ldsList_OnSelecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            eMenikDataContext db = new eMenikDataContext();

               var  list = from l in db.NEWSLETTERs   select l;

                // apply filters
               if (Status != null)
                   list = list.Where(w => w.Status == Status);

               if (NewsletterType != null)
                   list = list.Where(w => w.NewsletterType == NewsletterType);


            if (!String.IsNullOrEmpty(SearchText))
                list = list.Where(w => (w.Title.ToString().Contains(SearchText) || (w.Body ).Contains(SearchText) ));



            // result
            e.Result = list.OrderByDescending(o => o.DateSent  );
        }


        public void BindData()
        {
            lvList.DataBind();


        }




    }


}