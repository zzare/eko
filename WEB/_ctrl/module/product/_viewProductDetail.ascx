﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewProductDetail.ascx.cs" Inherits="_ctrl_module_product_viewProductDetail" %>
<%@ Register TagPrefix="SM" TagName="ProductAttribute" Src="~/_ctrl/module/product/attribute/_viewProductAttributeList.ascx"   %>
<%@ Register TagPrefix="SM" TagName="ProductBuy" Src="~/_ctrl/module/product/_viewProductDetailBuy.ascx"   %>
<%@ Register TagPrefix="SM" TagName="ProductContact" Src="~/_ctrl/module/product/_viewProductDetailContact.ascx"   %>
<%--<%@ Register TagPrefix="SM" TagName="ProductRating" Src="~/_ctrl/module/product/_viewProductDetailRating.ascx"   %>--%>

<%@ Register TagPrefix="SM" TagName="ProductFiles" Src="~/_ctrl/module/product/_viewProductDetailFiles.ascx"   %>


<%@ Register TagPrefix="SM" TagName="GalleryList" Src="~/_ctrl/module/gallery/_viewGalleryImageListSimple.ascx"    %>


<script type="text/javascript">

<% if (HasImage) {%>

    $(document).ready(function() {


        $('div.prod_detailLI').cycle({
            timeout: 0,
            speed: 2000,
            cleartypeNoBg: true,
            fx: 'fade',
            speedIn: 1000,
            speedOut: 900,
            delay: 0
        });


        $("a.fancy").fancybox({
            'titleShow': true,
            'transitionIn': 'elastic',
            'transitionOut': 'elastic',
            'easingIn': 'easeOutBack',
            'easingOut': 'easeInBack',
            'overlayOpacity': 0.4,
            'titlePosition': 'inside'
            ,
            'titleFormat': function(title, currentArray, currentIndex, currentOpts) {
            return title + '&nbsp<span class="fancybox-title-inside-index">(' + (currentIndex + 1) + '/' + currentArray.length + ')</span>' ;
            }
        });
    });
    <%} %>

</script>

<div id="pd<%= Prod.PRODUCT_ID.ToString() %>"  class='<%= RenderClass() %>'>

    <div class="prod_detailW">
        <%--<a id="A1" class="Aprint"  href="javascript:print()" ><%= GetGlobalResourceObject("Modules", "print")%></a>--%>
        
        <div class="prod_detail clearfix">
            <div class="prod_detailL">
                
                <div class="prod_detailLI">
<% if (HasImage) {%>
                    <div>
<% if (IsModeCMS && HasImage){%>
                        <a style="position:absolute; z-index:1000;" class="divGalImgThumb_btnEdit" href="#" onclick="editCustomCrop('<%= Prod.IMG_ID %>', '<%= Prod.TYPE_ID %>'); return false" ></a>
<%} %>
                        <a class="fancy Apd_bigimg" rel="prod" style="outline:none; position:relative;" href='<%= SM.BLL.Common.ResolveUrl ( Prod.IMG_URL_BIG )  %>' title='<%= RenderImageTitle()  %>' >
                            <%--<span class="Spd_mask"></span>--%>
                            <img class="prodImgBig" src='<%= SM.BLL.Common.ResolveUrl ( Prod.IMG_URL )  %>' alt='<%= RenderImageTitle()  %>'  />
                            <%--<div class="plImgTape_nw"></div>
                            <div class="plImgTape_se"></div>--%>
                        </a>
                    </div>
                        <%--<div style="display:none">--%>
                            <SM:GalleryList id="objGalleryList" runat="server"></SM:GalleryList>
                        
                        <%--</div>--%>
<%} %>                        

                        <%--<div style="float:left">
                            <SM:GalleryList id="objGalleryItem2" runat="server"></SM:GalleryList>
                        </div>--%>

                </div> 
                  
            </div>
            <div class="prod_detailR">
                <div class="prod_detailRI">
                    <h1 class="Hname"><%= Prod.NAME %></h1>
                    <div class="pd_descMI">
<%--<% if (IsModeCMS){ %>
            <a class="btEditContwthText" href="#" onclick="editProductDescFCKshort('<%= Prod.PRODUCT_ID %>'); return false" ><span>uredi opis (tudi slike)</span></a>
<%} %>--%>
                        <%= Prod.DESCRIPTION_SHORT  %>
                        <div class="clearing"></div>

                    <% if (IsActiveWWW){ %>
                        <% if (Prod.PRODUCT_TYPE == PRODUCT.ProductType.NORMAL  ) {%>        
                        <div class="prod_detailRMLI">
                            <SM:ProductBuy ID="objProductBuy" runat="server" />
                        
                        </div>
                        <%} %>
                        <% else if (Prod.PRODUCT_TYPE == PRODUCT.ProductType.NO_PRICE  ) {%>        
                        <div class="prod_detailRMLI">
                            <SM:ProductContact ID="objProductContact" runat="server" />
                        
                        </div>
                        <%} %>
                        <% else if (Prod.PRODUCT_TYPE == PRODUCT.ProductType.PRICE_DEPENDS_ON_ATRIBUTE  ) {%>        
                        <div class="prod_detailRMLI">
                            <%--<div class="cProductPriceNormal">--%>
                                <SM:ProductBuy ID="objProductBuyPrice" runat="server" />
                            <%--</div>--%>
                            <div class="cProductPriceContact" style="display:none;">
                                <SM:ProductContact ID="objProductContactPrice" runat="server" />
                            </div>
                            
                        </div>
                        <%} %>
                        

                        

                    <%}else
                           { %>
                              <span class="pd_pricefull">
                                <%= GetGlobalResourceObject("Product", "msgInactiveProduct")%>
                                      
                              </span> 
                            <div class="clearing"></div>

                    <% } %>
                    <div class="clearing"></div>


                    </div>
                </div>
            </div>


            
        </div>
        
                    
        <div class="clearing"></div>
        <div class="pd_files" style="display:none;">
            <% if (IsModeCMS){ %>
                <a class="btEditContwthText" href="#" onclick="editProductFile('<%= Prod.PRODUCT_ID %>'); return false" ><span>uredi datoteke</span></a>
            <%} %>
            <SM:ProductFiles ID="objProductFiles" runat="server" />
        </div>


        <div class="pd_desclong">
<% if (IsModeCMS){ %>
            <a class="btEditContwthText" href="#" onclick="editProductDescFCK('<%= Prod.PRODUCT_ID %>'); return false" ><span>uredi dodaten opis</span></a>
<%} %>
            <%= Prod.DESCRIPTION_LONG  %>
        </div>
            


    </div>

</div>
<div class="clearing"></div>