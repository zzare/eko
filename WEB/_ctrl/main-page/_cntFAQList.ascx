﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_cntFAQList.ascx.cs" Inherits="_ctrl_main_page_cntFAQList" %>


<asp:ListView ID="lvList" runat="server">

    <LayoutTemplate>
        <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
    </LayoutTemplate>

    <ItemTemplate>
        <a href='<%#  RenderArtHref(Container.DataItem) %>'><%#  Eval("ART_TITLE") %></a>
    </ItemTemplate>
    
    <ItemSeparatorTemplate>
        <br />
    </ItemSeparatorTemplate>

</asp:ListView>