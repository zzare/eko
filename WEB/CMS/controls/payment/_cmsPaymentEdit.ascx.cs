﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

namespace SM.UI.Controls
{
    public partial class _cmsPaymentEdit : BaseControl 
    {
        public event EventHandler OnChanged;
        public event EventHandler OnCancel;

        public int PaymentID {
            get
            {
                return (int) (ViewState["PaymentID"] ?? -1);
            }
            set{
                ViewState["PaymentID"] = value;
            }
        }
        public Guid UserID { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {

            }

        }

        // default values
        protected void LoadDefaultValues() {
            PaymentID = -1;
            tbDatePayed.Text  = DateTime.Now.Date.ToShortDateString();

            tbNotes.Text = "";
            tbPrice.Text = "";

        }



        // bind data
        public void BindData() {
            this.Visible = true;


            if (PaymentID  == -1)
            {
                LoadDefaultValues();
            }
            else {
                LoadData();
            
            }
            LoadTypes();

        }

        // LOAD Question
        protected void LoadData() {


            PAYMENT r = PAYMENT.GetPaymentByID(PaymentID );
            tbDatePayed.Text = r.DatePayed.ToShortDateString() ;
            tbNotes.Text  = r.Notes ;
            tbPrice.Text = SM.EM.Helpers.FormatPrice( r.Price);

            rblType.SelectedValue = r.Type.ToString();
        }

        protected void LoadTypes() {

            rblType.Items.Clear();
                rblType.Items.Add(new ListItem("Payment" , PAYMENT.Common.PaymentType.Payment.ToString()));
                rblType.Items.Add(new ListItem("Bonus", PAYMENT.Common.PaymentType.Bonus.ToString()));
                rblType.Items.Add(new ListItem("Promotion", PAYMENT.Common.PaymentType.Promotion.ToString()));

        }

        public void Pay()
        {
            
            // validation
            Page.Validate("payment");
            if (!Page.IsValid) return;

            
            eMenikDataContext db = new eMenikDataContext();

            decimal price;            
            if (!decimal.TryParse(tbPrice.Text, out  price)) return;

            string addedBy =  Page.User.Identity.Name ;
            DateTime  date = DateTime.Parse( tbDatePayed.Text) ;

            // add new payment
            if (PaymentID  == -1)
            {
                EM_USER.UpdateUserPayment(UserID, price, date, int.Parse(rblType.SelectedValue), tbNotes.Text, addedBy, Guid.Empty );
            }
            else
            {
            }

            // raise changed event
            OnChanged (this, new EventArgs());

        }

        // PAYMENT
        protected void btPay_Click(object sender, EventArgs e)
        {
            Pay();
        }

    }
}