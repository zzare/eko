﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

/// <summary>
/// Summary description for  Module
/// </summary>
public partial class MODULE
{

    public static IEnumerable<MODULE > GetModulesEmenik()
    {
        eMenikDataContext db = new eMenikDataContext();
        IEnumerable<MODULE> list;

        list = from m in db.MODULEs where m.EM_ACTIVE == true orderby m.EM_FREE descending, m.MOD_ORDER ascending  select m;
        return list;
    }
    public static IEnumerable<MODULE> GetModulesAll()
    {
        eMenikDataContext db = new eMenikDataContext();
        IEnumerable<MODULE> list;

        list = from m in db.MODULEs select m;
        return list;
    }

    public static MODULE GetModuleByID(int modID)
    {
        eMenikDataContext db = new eMenikDataContext();
        MODULE list;

        list = (from m in db.MODULEs where m.MOD_ID == modID  select m).FirstOrDefault();
     
        return list;
    }

    public static IEnumerable<MODULE> GetModulesByUser(Guid userID)
    {
        eMenikDataContext db = new eMenikDataContext();
        IEnumerable<MODULE> list;

        list = from m in db.MODULEs from um in db.USER_MODULEs  where um.UserId == userID && m.MOD_ID == um.MOD_ID  select m;
        return list;
    }

    public static List<MODULE> GetModulesByUserByZone(Guid userID, string zone, int pmodID)
    {
        eMenikDataContext db = new eMenikDataContext();
        List<MODULE> list;

        list = (from m in db.MODULEs from um in db.USER_MODULEs from zm in db.ZONE_MODULEs  where um.UserId == userID && m.MOD_ID == um.MOD_ID & zm.MOD_ID == m.MOD_ID && zm.PMOD_ID== pmodID && zm.ZONE.ToLower() == zone.ToLower()   select m).ToList();
        return list;
    }
    public static void InsertDefaultModulesForUser(Guid userID)
    {
        eMenikDataContext db = new eMenikDataContext();
        List<MODULE> list = GetModulesEmenik().ToList();

        foreach (MODULE m in list) {
            db.USER_MODULEs.InsertOnSubmit(new USER_MODULE { UserId=userID, MOD_ID = m.MOD_ID, EM_PRICE = m.EM_PRICE, QUANTITY = 1 }); 
        }
        db.SubmitChanges();
    }
    


    //public static bool IsCwpAllowedInZone(Guid wp, string zone, string  userName) {
    //    eMenikDataContext db = new eMenikDataContext();
    //    bool exists = ((from w in db.CMS_WEB_PARTs
    //                    join s in db.SITEMAPs on w.SMAP_ID equals s.SMAP_ID
    //                    join zm in db.ZONE_MODULEs on s.PMOD_ID equals zm.PMOD_ID
    //                    join m in db.MENUs on s.MENU_ID equals m.MENU_ID
    //                    join em in db.EM_USERs on m.USERNAME equals em.USERNAME
    //                    join um in db.USER_MODULEs on zm.MOD_ID equals um.MOD_ID
    //                    where w.CWP_ID == wp && zm.ZONE == zone && em.USERNAME == userName 
    //                    select 1).Count() > 0);

    //    return exists;
    //}

    public class Common {
        public const int ContentModID = 1;
        public const int ArticleModID = 2;
        public const int GalleryModID = 3;
        public const int ContactFormModID = 6;
        public const int MapModID = 7;
        public const int PollModID = 11;
    
    }

}
