﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_pmod_comb_3_1_3_1_.ascx.cs" Inherits="_em_content__ctrl_pmod_comb_3_1_3_1_" %>
<%@ Register Namespace="SM.EM.UI.Controls" TagPrefix="EM"  %>
<%@ Register TagPrefix="EM" TagName="ModuleNew" Src="~/_ctrl/emenik/emModuleNew.ascx"  %>


    <div class="cont_wideW">
        <div class="cont_wideT"></div>
        <div class="cont_wideM">
            <div class="cont_wideMI">

    <div class="clearfix contThreeColsWrapper_comb_3_1_3_1" > 
		<div class="clearfix contThreeColsTwoCols_comb_3_1_3_1"> 
			<div class="contThreeColsMain_comb_3_1_3_1" >            
                <EM:ModuleNew ID="mn_1_Main" runat="server" />

                <EM:CwpZone ID="wpz_2" runat="server">
                </EM:CwpZone>

            </div>

			<div class="contThreeColsRight_comb_3_1_3_1" >

                <EM:ModuleNew ID="mn_1_Right" runat="server" />

			    <EM:CwpZone ID="wpz_3" runat="server">
                </EM:CwpZone>
			</div>
		</div> 
		<div class="contThreeColsLeft_comb_3_1_3_1" >

                <EM:ModuleNew ID="mn_1_Left" runat="server" />

				<EM:CwpZone ID="wpz_1" runat="server">
                </EM:CwpZone>
		</div>
    </div>
    <div class="contOneCol_comb_3_1_3_1">

            <EM:ModuleNew ID="mn_2_Main" runat="server" />

			<EM:CwpZone ID="wpz_4" runat="server">
            </EM:CwpZone>
    </div>
    
    <div class="clearfix contThreeColsWrapper_comb_3_1_3_1" > 
		<div class="clearfix contThreeColsTwoCols_comb_3_1_3_1"> 
			<div class="contThreeColsMain_comb_3_1_3_1" >            
                <EM:ModuleNew ID="mn_3_Main" runat="server" />

                <EM:CwpZone ID="wpz_6" runat="server">
                </EM:CwpZone>

            </div>

			<div class="contThreeColsRight_comb_3_1_3_1" >

                <EM:ModuleNew ID="mn_3_Right" runat="server" />

			    <EM:CwpZone ID="wpz_7" runat="server">
                </EM:CwpZone>
			</div>
		</div> 
		<div class="contThreeColsLeft_comb_3_1_3_1" >

                <EM:ModuleNew ID="mn_3_Left" runat="server" />

				<EM:CwpZone ID="wpz_5" runat="server">
                </EM:CwpZone>
		</div>
    </div>
    <div class="contOneCol_comb_3_1_3_1">

            <EM:ModuleNew ID="mn_4_Main" runat="server" />

			<EM:CwpZone ID="wpz_8" runat="server">
            </EM:CwpZone>
    </div>

            </div>    
        </div>
        <div class="cont_wideB"></div>
    </div>
