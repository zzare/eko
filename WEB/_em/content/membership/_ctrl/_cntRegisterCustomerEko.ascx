﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_cntRegisterCustomerEko.ascx.cs" Inherits="_ctrl_content_membership_ctrl_cntRegisterCustomerShortEko" %>
<%@ Register Namespace="SM.UI.Controls" TagPrefix="SM"  %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Panel ID="panLogin" runat="server" DefaultButton="btRegister">
        <table class="TABLE_quickregister" cellpadding="0">

            <tr>
                <td class="TD_right"><asp:Label runat="server" ID="Label1" AssociatedControlID="tbName" Text="*Ime:" /></td>
                <td>
                    <asp:TextBox class="TBXlogin" runat="server" ID="tbName" /><br />
                </td>
            </tr>
            <tr class="tr_validation">
                <td></td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="tbName" SetFocusOnError="true" Display="Dynamic"
                        ErrorMessage="* Vpiši ime." ToolTip="* Vpiši ime." ValidationGroup="regShort"></asp:RequiredFieldValidator>
                </td>
            </tr> 

            <tr>
                <td class="TD_right"><asp:Label runat="server" ID="Label2" AssociatedControlID="tbLastName" Text="*Priimek:" /></td>
                <td>
                    <asp:TextBox class="TBXlogin" runat="server" ID="tbLastName" /><br />
                </td>
            </tr>
            <tr class="tr_validation">
                <td></td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="tbLastName" SetFocusOnError="true" Display="Dynamic"
                        ErrorMessage="* Vpiši priimek." ToolTip="* Vpiši priimek." ValidationGroup="regShort"></asp:RequiredFieldValidator>
                </td>
            </tr> 

            <tr>
                <td class="TD_right"><asp:Label runat="server" ID="lblEmail" AssociatedControlID="Email" Text="*E-pošta:" /></td>
                <td>
                    <asp:TextBox class="TBXlogin" runat="server" ID="Email" /><br />
                </td>
            </tr>
            <tr class="tr_validation">
                <td></td>
                <td>
                    <asp:RequiredFieldValidator ID="valRequireEmail" runat="server" ControlToValidate="Email" SetFocusOnError="true" Display="Dynamic"
                        ErrorMessage="* Vpiši email." ToolTip="* Vpiši email." ValidationGroup="regShort"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator runat="server" ID="valEmailPattern"  Display="Dynamic" SetFocusOnError="true" ValidationGroup="regShort"
                        ControlToValidate="Email" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ErrorMessage="* E-poštni naslov ni pravilne oblike."></asp:RegularExpressionValidator>
                    <asp:CustomValidator ValidationGroup="regShort" ID="cvEmail" runat="server" Display="Dynamic" ErrorMessage="* E-poštni naslov ni veljaven." OnServerValidate="ValidateEmail"></asp:CustomValidator>
                    <asp:CustomValidator ValidationGroup="regShort" ID="cvUsername" runat="server" Display="Dynamic" ErrorMessage="* Uporabniško ime je že zasedeno." OnServerValidate="ValidateDuplicate"></asp:CustomValidator>
                                        
                    &nbsp;
                </td>
            </tr> 

            <tr>
                <td class="TD_right"><asp:Label runat="server" ID="lblPassword" AssociatedControlID="Password" Text="Geslo (opcijsko):" /></td>
                <td>
                    <asp:TextBox class="TBXlogin" runat="server" ID="Password" TextMode="Password" /><br />
                </td>
            </tr>
            <tr class="tr_validation">
                <td></td>
                <td>
                   <%-- <asp:RequiredFieldValidator ID="valRequirePassword" runat="server" ControlToValidate="Password" SetFocusOnError="true" Display="Dynamic"
                        ErrorMessage="* Vpiši želeno geslo." ToolTip="* Vpiši želeno geslo." ValidationGroup="regShort"></asp:RequiredFieldValidator>--%>
                    <asp:RegularExpressionValidator ID="valPasswordLength" runat="server" ControlToValidate="Password" SetFocusOnError="true" Display="Dynamic"
                    ValidationExpression=".{5,}" ErrorMessage="* Geslo mora biti dolgo vsaj pet (5) znakov." ToolTip="* Geslo mora biti dolgo vsaj pet (5) znakov."
                    ValidationGroup="regShort"></asp:RegularExpressionValidator>&nbsp;
                </td>
            </tr>

           
        </table>
    <div style="padding:10px 0px 0px 0px;">
        <SM:LinkButtonDefault ID="btRegister" CssClass="Astepnext" runat="server"   ValidationGroup="regShort"    onclick="btRegister_Click"><span>REGISTRIRAJ</span></SM:LinkButtonDefault>
    
    </div>
    <div class="clearing"></div>
</asp:Panel>

