﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

/// <summary>
/// Summary description for VIDEO
/// </summary>
public partial class VIDEO
{

    //public bool IsValid
    //{
    //    get { return (GetRuleViolations().Count() == 0); }
    //}

    //public IEnumerable<RuleViolation> GetRuleViolations()
    //{
    //    //if (this.ART_ID == null || this.ART_ID < 1)
    //    //{
    //    //    // new article

    //    //}
    //    //else
    //    //{

    //    //}

    //    // common
    //    if (CWP_ID == null || CWP_ID == Guid.Empty)
    //        yield return new RuleViolation("Identifikator ni določen", "CWP_ID");
    //    if (string.IsNullOrEmpty( this.Title  ))
    //        yield return new RuleViolation("Naslov ni določen", "Title");


    //    yield break;
    //}

    //partial void OnValidate(System.Data.Linq.ChangeAction action)
    //{
    //    if (!IsValid)
    //        throw new ApplicationException("Podatki niso pravilno vneseni.");
    //}


    public static string GetVideoIDFromUrl (string url) {
        

            Match youtubeMatch = YoutubeVideoRegex.Match(url);
            Match vimeoMatch = VimeoVideoRegex.Match(url);

            string id = string.Empty;

            if (youtubeMatch.Success)
                id = youtubeMatch.Groups[4].Value;

            if (vimeoMatch.Success)
                id = vimeoMatch.Groups[1].Value;


            return id;
         }



    public static readonly Regex VimeoVideoRegex = new Regex(@"vimeo\.com/(?:.*#|.*/videos/)?([0-9]+)", RegexOptions.IgnoreCase | RegexOptions.Multiline);
    public static readonly Regex YoutubeVideoRegex = new Regex(@"youtu(?:\.be|be\.com)/(?:(.*)v(/|=)|(.*/)?)([a-zA-Z0-9-_]+)", RegexOptions.IgnoreCase);

    //public static VIDEO GetVideoByID(Guid  id)
    //{
    //    eMenikDataContext db = new eMenikDataContext();

    //    VIDEO art;

    //    //art = db.em_GetSitemapArticleByID(artID).FirstOrDefault();  
    //    art = db.VIDEOs.SingleOrDefault(w => w.VideoID  == id);

    //    return art;
    //}
    //public static VIDEO GetVideoByCwpID(Guid cwpID)
    //{
    //    eMenikDataContext db = new eMenikDataContext();

    //    VIDEO cont = (from c in db.VIDEOs where c.CWP_ID == cwpID select c).SingleOrDefault();

    //    return cont;
    //}
    //public static VIDEO CreateVideo(string user, Guid cwp, string title, string desc, int sort, bool active, short type)
    //{

    //    eMenikDataContext db = new eMenikDataContext();

    //    VIDEO art = new VIDEO();

    //    art.VideoID = Guid.NewGuid();
    //    art.CWP_ID= cwp;
    //    art.DateAdded = DateTime.Now; 
    //    art.Title= title;
    //    art. Desc= desc ;
    //    art. SortOrder = sort ;
    //    art.VideoType  = type ;
    //    art.Active = active ;

    //    db.VIDEOs.InsertOnSubmit(art);

    //    db.SubmitChanges();

    //    return art;
    
    //}

    //public static void DeleteVideoBySitemapID(int smpID)
    //{
    //    eMenikDataContext db = new eMenikDataContext();

    //    IQueryable<VIDEO> cont = (from c in db.VIDEOs 
    //                              from cwp in db.CMS_WEB_PARTs
    //                              from smp in db.SITEMAPs
    //                              where smp.SMAP_ID == smpID &&  cwp.REF_ID == smp.SMAP_GUID &&
    //                               cwp.CWP_ID == c.CWP_ID 
    //                              select c);
    //    db.VIDEOs.DeleteAllOnSubmit(cont);


    //    IQueryable<VIDEO> vidCwp = (from c in db.VIDEOs
    //                             from cwp in db.CMS_WEB_PARTs
    //                             from smp in db.SITEMAPs
    //                             where smp.SMAP_ID == smpID && cwp.REF_ID == smp.SMAP_GUID && c.CWP_ID == cwp.CWP_ID
    //                             select c);
    //    db.VIDEOs.DeleteAllOnSubmit(vidCwp);

    //    db.SubmitChanges();
    //}

    //public static void DeleteVideoByID(Guid id)
    //{
    //    eMenikDataContext db = new eMenikDataContext();

    //    VIDEO art = (from c in db.VIDEOs where c.VideoID == id select c).SingleOrDefault();
    //    if (art == null)
    //        return;
    //    db.VIDEOs.DeleteOnSubmit(art);
    //    db.SubmitChanges();
    //}



    public static string GetVideoPlayHref(string href, bool autoplay) {
        string ret = href;


        if (href.Contains("youtube")) {
            ret = href.Replace(@"watch?v=", "v/");
            if (autoplay)
                ret += "&autoplay=1";        
        }
        else if (href.Contains("vimeo")) { 
            ret = href.Replace(@"vimeo.com/", "vimeo.com/moogaloop.swf?clip_id=");

            if (autoplay)
                ret += "&autoplay=1";        
        
        }

        return ret;
    }


    //public static void UpdateOrder(Guid id, int order)
    //{
    //    eMenikDataContext db = new eMenikDataContext();

    //    VIDEO ig = db.VIDEOs.SingleOrDefault(w => w.VideoID == id );
    //    ig.SortOrder = order < 0 ? 0 : order;
    //    db.SubmitChanges();
    //}


    public class Common
    {
        //public const string MAIN_GALLERY_ZONE = "vid_main_gal";
        public const int DEFAULT_VIDEO_IMAGE_ID = 49;


        public class VideoType {
            public const short YOUTUBE = 1;
        
        }

        public class DisplayType
        {
            public const short NORMAL = 1;

        }

        public static string GetUserVideoFolder(Guid pageID) {

            return IMAGE.Common.GetUserImageFolder(pageID) + "vid/";
        }

    }

    //public class Move
    //{


    //    public static bool MoveToPosition(Guid cwpID, Guid  id, int pos, string user)
    //    {
    //        CMS_WEB_PART cwp = CMS_WEB_PART.GetWebpartByID(cwpID, user);

    //        if (cwp == null) return false;

    //        MoveToPosition(cwpID, id, pos);

    //        return true;

    //    }

    //    public static void MoveToPosition(Guid cwpID, Guid id, int pos)
    //    {

    //        int step = 5;
    //        int newOrder = step;// order for first item in list

    //        eMenikDataContext db = new eMenikDataContext();

    //        // get all videos in zone
    //        var list = (from i in db.VIDEOs 
    //                    where i.CWP_ID == cwpID  
    //                    orderby i.SortOrder select new { i.VideoID, i.SortOrder  }).ToList();
    //        // ...without current one
    //        list = list.Where(w => w.VideoID != id).ToList();

    //        // item is only one
    //        if (list == null)
    //        {
    //            UpdateOrder(id, newOrder);
    //            return;
    //        }

    //        // item is last one
    //        if (list.Count <= pos)
    //        {
    //            newOrder = list[list.Count - 1].SortOrder + step;
    //            UpdateOrder(id, newOrder);
    //            return;
    //        }

    //        int ord = step;
    //        // reorder entire list
    //        for (int i = 0; i < list.Count(); i++)
    //        {

    //            if (i == pos || (pos < 0 && i == 0))
    //            {
    //                newOrder = ord;
    //                ord += step;
    //            }
    //            UpdateOrder(list[i].VideoID, ord);

    //            ord += step;
    //        }

    //        UpdateOrder(id, newOrder);
    //    }




    //}



    public class Render
    {


        //public static string RenderCategoryHref(object item, string cssClass, string pname)
        //{
        //    string ret = "";


        //    Data.ArticleData a = (Data.ArticleData)item;

        //    if (string.IsNullOrEmpty(a.CAT_SML_TITLE) || a.CAT_SMAP_ID < 1)
        //        return ret;
        //    string css = "";
        //    if (!string.IsNullOrEmpty(cssClass))
        //        css = "class=\""+ cssClass  +"\"";

        //    return string.Format("<a href=\"{0}\" {2} >{1}</a>", SM.BLL.Common.ResolveUrl( SM.EM.Rewrite.EmenikUserUrl(pname, a.CAT_SMAP_ID, a.CAT_SML_TITLE )), a.CAT_SML_TITLE, css );
        //    //else
        //    //  return string.Format("{0:dddd, dd.MM.yyyy ob HH:mm} ", date);
        //}


    }

}
