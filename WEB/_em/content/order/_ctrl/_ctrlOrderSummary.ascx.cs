﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SM.UI.Controls;
using SM.EM.UI.Controls;


public partial class _em_content_order__ctrl__ctrlOrderSummary : System.Web.UI.UserControl  
{

    public SHOPPING_ORDER Order;
    public Guid PageId;


    protected void Page_Load(object sender, EventArgs e)
    { 




    }


    public void LoadData() {
        ProductRep prep = new ProductRep();
        Guid? orderid = Order.OrderID ;
        if (Order.OrderStatus > SHOPPING_ORDER.Common.Status.NEW && Order.OrderStatus < SHOPPING_ORDER.Common.Status.STEP_CONFIRMED){
            phStatus.Visible = false;
            orderid = null;
        }

        objOrderDetail.Order = Order;
        objOrderDetail.CartItems = prep.GetCartItemList(Order.UserId , LANGUAGE.GetCurrentLang(), PageId, orderid );
        objOrderDetail.LoadData();

    
    }


    public string RenderPayment() {
        string ret = "";

        if (Order == null)
            return ret;

        ret = SHOPPING_ORDER.Common.RenderSHOPPING_ORDERPayment(Order) + "<br/>";
        return ret;
    }

    public string RenderShipping()
    {
        string ret = "";

        if (Order == null)
            return ret;

        ret = SHOPPING_ORDER.Common.RenderShoppingOrderShipping(Order.ShippingType) ;

        if (Order.ShippingType == SHOPPING_ORDER.Common.ShippingType.SHIPPING)
        {


            ret += "";
            ret += "Ime in priimek: " + Order.GetShippingName() + "<br/>";
            ret += "Naslov: " + Order.GetShippingAddress() + "<br/>";
            ret += "Kraj: " + Order.GetShippingPostalAndCity() + "<br/>";
            //ret += "Tel: " + Order.GetShippingPhone() + "<br/>";
        }
        ret += "<br/>";

        return ret;
    }


}
