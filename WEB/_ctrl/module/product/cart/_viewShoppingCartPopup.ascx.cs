﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _ctrl_module_product_cart_viewShoppingCartPopup : BaseControlView
{

    public object Data;
    public object[] Parameters;
    public List<ProductRep.Data.ShoppingCartItem> CartItems;
    protected decimal _totalSum = -1;
    public decimal GetTotalSum {
        get {
            if (_totalSum > -1)
                return _totalSum;
            _totalSum = 0;
            if (CartItems == null || !CartItems.Any())
                return _totalSum;
            _totalSum = (from c in CartItems select c.PriceTotal).Sum();
            return _totalSum;
        
        }
    }
    public Guid PageID;


    protected void Page_Load(object sender, EventArgs e)
    {

        if (CartItems == null)
            LoadData();
    }



    public void LoadData()
    {

        if (Data == null)
        {
            //this.Visible = false;
            return;
        }
        CartItems = (List<ProductRep.Data.ShoppingCartItem >)Data;
        if (CartItems == null)
            return;

        if (Parameters != null)
        {
            if (Parameters.Length > 0 && Parameters[0] != null)
                PageID = new Guid(Parameters[0].ToString());

        }


        // bind data
        objShoppingCart.CartItems = CartItems;
        objShoppingCart.Parameters = Parameters;
        objShoppingCart.LoadData();



    }

    public string RenderLoadingVisible() {
        string ret = "";
        if (CartItems == null)
            ret = "style=\"display:block;\"";
        else
            ret = "style=\"display:none;\"";
        return ret;
    }

    public string RenderContainerVisible()
    {
        string ret = "";
//        if (CartItems != null)
            ret = "style=\"display:block;\"";
  //      else
    //        ret = "style=\"display:none;\"";
        return ret;
    }

    public string RenderProductAttributes(object o)
    {
        string ret = "";
        string separator = " | ";
        ProductRep.Data.ShoppingCartItem cartItem = (ProductRep.Data.ShoppingCartItem)o;

        foreach (ProductRep.Data.ItemGroupAttribute attr in cartItem.AttrList) {
            if (!string.IsNullOrEmpty(ret))
                ret+=separator ;
            //ret += attr.GroupTitle + "=" + attr.Title; 
            ret += attr.Title;   
        }

        return ret;
    }



}
