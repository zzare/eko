﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

/// <summary>
/// Summary description for MARKETING_CAMPAIGN
/// </summary>
public partial class MARKETING_CAMPAIGN
{
    public static MARKETING_CAMPAIGN  GetCampaignByID(Guid  id)
    {
        eMenikDataContext db = new eMenikDataContext();

        return db.MARKETING_CAMPAIGNs.Where(w => w.CampaignID == id).SingleOrDefault();
    }
    public static MARKETING_CAMPAIGN GetValidCampaignByCODE(string code)
    {
        eMenikDataContext db = new eMenikDataContext();

        MARKETING_CAMPAIGN ret = db.MARKETING_CAMPAIGNs.Where(w => w.CampaignPromoCode.ToUpper() == code.ToUpper() && w.Active ).SingleOrDefault();

        if (ret == null)
            return null;

        // campaign is in future
        if (ret.DateValidFrom != null && ret.DateValidFrom.Value > DateTime.Now)
            return null;

        // campaign is finished
        if (ret.DateValidTo != null && ret.DateValidTo.Value.Date  < DateTime.Now.Date )
            return null;

        return ret;
    }

    public static List<MARKETING_CAMPAIGN> GetCampaignsByUser(Guid userID)
    {
        eMenikDataContext db = new eMenikDataContext();

        return db.MARKETING_CAMPAIGNs.Where(w => w.MarketingUserId  == userID || w.AffilliateUserId == userID  ).ToList();
    }

    public class Common {
        public const int MAX_MARKETING_PERCENT = 30;
        public const int MAX_AFFILIATE_PERCENT = 25;


        public class ProvisionType
        {
            public const int Percent = 0;
            public const int Fixed = 1;

        }

        public static string GetCampaignDetailsCMSHref(Guid o)
        {
            string ret = "~/cms/emenik/ManageCampaign.aspx?c=" + o.ToString();

            return ret;
        }

        public static string GetPromoLink(Guid o)
        {
            
            string ret =  HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "?cid=" + o.ToString();

            return ret;
        }

    }

    //partial void OnValidate(System.Data.Linq.ChangeAction action)
    //{
    //}

}
