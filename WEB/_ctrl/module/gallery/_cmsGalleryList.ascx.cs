﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using SM.EM.BLL;
namespace SM.EM.UI.Controls
{
    public partial class cmsGalleryList : BaseControl
    {
        public int ExcludeID { get; set; }
        public Guid UserID { get; set; }
        public int TypeID { get; set; }
        public bool IsEmpty { get { return (lvList.Items.Count == 0); } }


        public delegate void OnEditEventHandler(object sender,SM.BLL.Common.ClickIDEventArgs  e);
        public event OnEditEventHandler OnEditClick;


        protected void InitData() {
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }



        // bind data
        public void BindData()
        {
            eMenikDataContext db = new eMenikDataContext ();

            List<GALLERY > list = (from g in db.GALLERies from c in db.CMS_WEB_PARTs where g.IMAGE_GALLERies.Count >0 && g.GAL_ID == c.ID_INT && c.UserId == UserID   select g).ToList();

            // exclude
            if (ExcludeID > 0)
                list = list.Where(w => w.GAL_ID != ExcludeID).ToList();


            lvList.DataSource = list.Distinct(); 
            lvList.DataBind();

        }


        protected void lvList_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            if (e.CommandName == "selectGal")
            {
                OnEditClick(this, new SM.BLL.Common.ClickIDEventArgs { ID = Int32.Parse(e.CommandArgument.ToString()) });
            }
        }

        protected string RenderMainImage( object gal, object img) { 
            // if main image is set, return url
            if (img != null) {
                IMAGE i = IMAGE.GetImageByID(int.Parse(img.ToString()), TypeID );
                return i.IMG_URL;            
            }

            // get first image
            vw_Image  im = GALLERY.GetImagesByGallery(int.Parse(gal.ToString()), TypeID).FirstOrDefault();
            if (im != null) return im.IMG_URL;

            return "";       
        
        }
        protected string RenderGalleryName(object gal)
        {
            List<vw_GalleryDesc > list =  GALLERY.GetGalleryDescsByID(int.Parse(gal.ToString()));
            vw_GalleryDesc desc;
            desc = list.Where(w => w.LANG_ID == LANGUAGE.GetCurrentLang()).SingleOrDefault();
            if (desc == null) desc = list.FirstOrDefault();

            if (desc == null) return "";

            return desc.GAL_TITLE;

        }



    }
}
