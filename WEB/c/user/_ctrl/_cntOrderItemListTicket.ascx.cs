﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;


namespace SM.UI.Controls
{
    public partial class c_user_ctrl_cntOrderItemListTicket : BaseControl 
    {
        public Guid OrderID;
        public Guid UserId { get; set; }

        
        protected void Page_Load(object sender, EventArgs e)
        {

        }



        public void BindData()
        {
            eMenikDataContext db = new eMenikDataContext();

            //lvList.DataSource = ORDER.GetOrderItemsByOrderActive(OrderID, UserId);
            //lvList.DataBind();

        }


        protected string RenderTicketLink(object o) { 
            if (o == null)
                return "";

            string ret = string.Format("<a target=\"_blank\" href=\"{0}\"  title=\"edit ticket\" >podrobnosti (ticket)</a>", Page.ResolveUrl( TICKET.Common.GetTicketDetailsCMSHref(new Guid(o.ToString()))));
            return ret;
        }




    }

}