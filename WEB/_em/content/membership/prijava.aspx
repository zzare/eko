﻿<%@ Page Language="C#" ValidateRequest="false" MasterPageFile="~/_em/templates/fall/fall.master" AutoEventWireup="true" CodeFile="prijava.aspx.cs" Inherits="_em_content_membership_prijava" Title="Untitled Page" %>
<%@ Register TagPrefix="SM" TagName="Login" Src="~/_em/content/membership/_ctrl/_cntLogin.ascx"   %>
<%@ Register TagPrefix="SM" TagName="EditModulePublic" Src="~/_ctrl/module/_edit/editModulePublic.ascx"   %>



<asp:Content ID="Content2" ContentPlaceHolderID="cph" Runat="Server">


<script type="text/javascript">

<!--
    var flashvars = {};
    var params = {};
    params.quality = "high";
    params.bgcolor = "#ffffff";
    params.allowscriptaccess = "sameDomain";
    params.allowfullscreen = "true";
    params.base = ".";
    params.wmode = "transparent";
    var attributes = {};
    attributes.id = "pano";
    attributes.name = "pano";
    attributes.align = "middle";

    swfobject.embedSWF(
    			"<%= Page.ResolveUrl("~/") %>_em/templates/_custom/EKO/_inc/flash/KoradaPanorama01_out.swf", "flashContent",
				"970", "250",
				"9.0.0", "expressInstall.swf",
				flashvars, params, attributes);

//-->



</script>


 <SM:EditModulePublic ID="objEditPublic" runat="server" />


    
    <div class="loginContW">

        <div  class="separator_main"  ></div>

        <div class="flashContentW">
	        <div id="flashContent">
		        <img alt="Terra-Gourmet" src="<%= Page.ResolveUrl("~/_em/templates/_custom/EKO/_inc/images/panorama/pan3_970.jpg") %>"  />
	        </div>
        </div>
        <p style="padding:5px 0px 10px 0px; font-size:12px;">360&deg; panorama</p>

        <%--<img alt="Terra-Gourmet" src="<%= Page.ResolveUrl("~/_em/templates/_custom/EKO/_inc/images/panorama/pan3_970.jpg") %>"  />--%>
        <div  class="separator_main"  ></div>
        
        <%--<h1>Vpišite se</h1>--%>
        <%--<p class="p_login_text">Predstavljamo vam Terra-Gourmet, deželo gurmanov. Skupaj smo ustvarili spletni klub za sladokusce, kjer si lahko izmenjujete mnenja, recepte in misli, predvsem pa naročite izbrane vrhunske izdelke. Med njimi boste našli okusno meso goveda Limousine, ki je prvovrstna govedina brez konkurence. Skrbno smo izbirali in ji dodali serijo dopolnjujočih izdelkov, kot so razni siri, jabolčni sok, vino, olivno olje in vinski kis, da boste lahko svojo pojedino zaokrožili v popolno harmonijo okusov.</p>--%>
        <p class="p_login_text">Predstavljamo vam Terra-Gourmet, deželo gurmanov. Skupaj smo ustvarili spletni klub za sladokusce, kjer si lahko izmenjujete mnenja, recepte in misli, predvsem pa naročite izbrane vrhunske izdelke. Med njimi boste našli okusno EKO meso goveda Limousine, ki je prvovrstna EKO govedina brez konkurence. Skrbno smo izbirali in ji dodali serijo dopolnjujočih izdelkov, kot so razni siri, jabolčni sok, vino, olivno olje in vinski kis, da boste lahko svojo pojedino zaokrožili v popolno harmonijo okusov.</p>
        <p class="p_login_text_c">Vse naše izdelke vam bomo z veseljem dostavili na dom.</p>
        <div class="ph_h2"><span class="ph_h2_text ph_h2_text_sepa_width"><span class="ph_h2_picL"></span><span class="ph_h2_picR"></span>&nbsp;</span></div>
        <p class="p_login_text_c">V Terra-Gourmet si lahko vrhunske izdelke priskrbite le z nekaj kliki. <br />Če pa vam je spletno naročanje nepraktično, lahko nakup opravite po telefonu na +386 (0) 40 602 222.</p>
        <%--<SM:Login ID="objLogin" runat="server" />
        <br />--%>

        <div class="loginboxinner">
        <a class="Astepnext" href="<%= SM.BLL.Common.ResolveUrl(SM.EM.Rewrite.EmenikUserUrl("eko", 1471, "V besedi")) %>"><span>VSTOPI</span></a>
            <%--<asp:LinkButton ID="LoginButton" class="Astepnext" runat="server" OnClick="btEnter_Click" ><span>VSTOPI</span></asp:LinkButton>--%>
        </div>
                    
        <%-- Še nimate računa?
        <br />
                    
        <a class="Astepnext" style="margin-top:7px;" href='<%=  PreserveReturnUrl( Page.ResolveUrl(SM.EM.Rewrite.EmenikCustomerRegister (em_user.UserId) )) %>'><span>REGISTRIRAJTE SE</span></a>--%>
        <div class="clearing"></div>
    </div>

    
    
    
</asp:Content>
