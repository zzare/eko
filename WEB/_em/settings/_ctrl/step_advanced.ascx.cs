﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SM.EM.UI.Controls;

public partial class _em_settings__ctrl_step_advanced : BaseWizardStep {

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        //Description = "Tukaj lahko nastavljate dodatne nastavitve.";

        Description = "Določi ime spletne strani in vnesi poddomeno v okviru sistema 1dva3.si oz. domeno. V primeru, da lastne domene nimaš, preveri razpoložljivost prostih domen in si naroči lastno domeno. Ko končaš, pritisni gumb \"<u>Končaj</u>\" oz. \"<u>Shrani spremembe</u>\". ";

        //Description = "<ul><li>";
        //Description += "To je sedmi, zadnji korak našega čarovnika za nastavitev spletne strani. V tem koraku boste določili ime vaše spletne strani in vnesli domeno, če jo imate (če domene nimate, se lahko obrnete na nas in poslali vam bomo ponudbo<<link do kontakta>>). Ko boste končali pritisnite gumb \"Shrani spremembe\" in začnite z urejanjem vsebine vaše spletne strani. ";
        //Description += "</li></ul><br/><br/><br/>";

        ShortDesc = "Dodatne nastavitve";

        StepID = 7;
        AllwaysShowSaveButton = false; 

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    public override void LoadData()
    {
        tbPageName.Text = ParentPageEmenik.em_user.PAGE_NAME;
        tbPageTitle.Text = ParentPageEmenik.em_user.PAGE_TITLE;
        tbDomain.Text = ParentPageEmenik.em_user.CUSTOM_DOMAIN;
        //tbBuyDomain.Text = ParentPageEmenik.em_user.ORDERED_DOMAIN;

        cbActivated.Visible = !string.IsNullOrEmpty(tbDomain.Text);
        cbActivated.Checked = ParentPageEmenik.em_user.CUSTOM_DOMAIN_ACTIVATED;





        //// domain is not ordered yet
        //if (ParentPageEmenik.em_user.ORDERED_DOMAIN_STATUS == ORDER.Common.DomainOrderStatus.NOT_ORDERED)
        //{
        //    phBuyDomain.Visible = true;
        //    if (ParentPageEmenik.em_user.PAYED)
        //        phOrderDomain.Visible = true;
        //    else
        //        phOrderDomain.Visible = false;

        //    tbBuyDomain.Text = ParentPageEmenik.em_user.ORDERED_DOMAIN;

        //}// domain has already beend ordered
        //else
        {
            phBuyDomain.Visible = false;
        }

        // pre set domain radio buttons

        // custom domain
        if (ParentPageEmenik.em_user.CUSTOM_DOMAIN != null && !string.IsNullOrEmpty(ParentPageEmenik.em_user.CUSTOM_DOMAIN))
        {
            rbCustomDomain.Checked = true;
        }
        // orderd domain
        //else if (ParentPageEmenik.em_user.CUSTOM_DOMAIN == null && ParentPageEmenik.em_user.ORDERED_DOMAIN != null && phBuyDomain.Visible)
        //{
        //    rbBuyDomain.Checked = true;

        //} // domain inside 1dva3
        else
        {
            rbPageName.Checked = true;

        }


    }

    public override bool SaveData(int step)
    {

        // update page title
        eMenikDataContext db = new eMenikDataContext();
        EM_USER user;
        user = db.EM_USERs.Where(u => u.UserId == ParentPageEmenik.em_user.UserId).SingleOrDefault();

        string pName = "";
        string oldPageName = ParentPageEmenik.em_user.PAGE_NAME;
        string oldDomain = ParentPageEmenik.em_user.CUSTOM_DOMAIN;
        bool oldDomainActive = ParentPageEmenik.em_user.CUSTOM_DOMAIN_ACTIVATED;
        bool isUserActive = ParentPageEmenik.em_user.ACTIVE_USER;
            


        if (rbPageName.Checked)
            pName = SM.EM.Rewrite.ConvertToFriendlyURL(tbPageName.Text, ".");
        else
            pName = user.PAGE_NAME;

        if (tbPageTitle.Text == "")
            user.PAGE_TITLE = pName;
        else
            user.PAGE_TITLE = tbPageTitle.Text;


        // update domain
        if (rbPageName.Checked)
        {
            user.PAGE_NAME = pName;
            user.CUSTOM_DOMAIN = null;
            //user.ORDERED_DOMAIN = null;
            EM_USER.SetPageName(pName);
        }
        else if (rbCustomDomain.Checked)
        {
            // format domain
            string d = SM.EM.Helpers.FormatDomain(tbDomain.Text);

            // update domain        
            if (string.IsNullOrEmpty(d))
            {
                user.CUSTOM_DOMAIN_ACTIVATED = false;
                user.CUSTOM_DOMAIN = null;
            }
            else
            {
                // if domain is set, check if exists
                user.CUSTOM_DOMAIN = d;
                user.CUSTOM_DOMAIN_ACTIVATED = SM.EM.Helpers.IsDomainActive(d);
            }

            //db.SubmitChanges();
            //user.AddToCache();

            // remove menu cache 
            SM.EM.Caching.RemoveEmenikUserMenuCacheKey(user.PAGE_NAME);
        }
        //else if (rbBuyDomain.Checked)
        //{
        //    user.ORDERED_DOMAIN = SM.EM.Helpers.FormatDomain(tbBuyDomain.Text);
        //}

        // save data
        user.RemoveFromCache();
        db.SubmitChanges();
        user.AddToCache();

        // all ok
        UpdateUserStep(step);


        // if page name was changed
        bool pageNameChanged = false;
        if (oldPageName != ParentPageEmenik.em_user.PAGE_NAME)
            pageNameChanged = true;

        // if domain was changed


        //int stepToRedirect = 7;
        //if (ParentPageEmenik.em_user.STEPS_DONE < 8 || !ParentPageEmenik.em_user.ACTIVE_USER)
        //{
        //    EM_USER.UpdateUserStep(ParentPageEmenik.em_user.UserId, 7);
        //    stepToRedirect = 8;
        //}
        int stepToRedirect = 7;
        if (!isUserActive)
        {
            stepToRedirect = 8;
            EM_USER.UpdateActiveUser(ParentPageEmenik.em_user.UserId, true);
        }

        // 





        // if entered is new domain and active (different from old one) -> redirect to new one
        if (ParentPageEmenik.em_user.CUSTOM_DOMAIN != null && !String.IsNullOrEmpty(ParentPageEmenik.em_user.CUSTOM_DOMAIN) && ParentPageEmenik.em_user.CUSTOM_DOMAIN_ACTIVATED && ParentPageEmenik.em_user.CUSTOM_DOMAIN != oldDomain)
        {
            // log off from this domain
            FormsAuthentication.SignOut();

            // log in and redirect
            //                SM.EM.Security.Login.LogIn(ParentPage.em_user.USERNAME, false, "");
            SM.EM.Security.Login.LogIn(ParentPageEmenik.em_user.USERNAME, false, SM.EM.Rewrite.EmenikUserSettingsUrlByStep(ParentPageEmenik.em_user.PAGE_NAME, stepToRedirect));

            //Response.Redirect("http://" + ParentPage.em_user.CUSTOM_DOMAIN);
        }
        // or now is no domain and before there was one and active                
        else if ((ParentPageEmenik.em_user.CUSTOM_DOMAIN == null || String.IsNullOrEmpty(ParentPageEmenik.em_user.CUSTOM_DOMAIN)) && !string.IsNullOrEmpty(oldDomain) && oldDomainActive)
        {
            // log off from this domain
            FormsAuthentication.SignOut();

            // log in and redirect
            //                SM.EM.Security.Login.LogIn(ParentPage.em_user.USERNAME, false, "");
            SM.EM.Security.Login.LogIn(ParentPageEmenik.em_user.USERNAME, false, SM.EM.Rewrite.EmenikUserSettingsUrlByStep(ParentPageEmenik.em_user.PAGE_NAME, stepToRedirect));


            //// redirect to new domain
            //Response.Redirect(SM.EM.Rewrite.EmenikUserSettingsUrlByStep(ParentPage.em_user.PAGE_NAME, 7));


        }// page name was changed - subdomain - log on subdomain
        else if (SM.BLL.Common.Emenik.Data.EnableSubDomainUsers() && pageNameChanged)
        {
            // log off from this domain
            FormsAuthentication.SignOut();

            // log in and redirect

            SM.EM.Security.Login.LogIn(ParentPageEmenik.em_user.USERNAME, false, SM.EM.Rewrite.EmenikUserSettingsUrlByStep(ParentPageEmenik.em_user.PAGE_NAME, stepToRedirect));
            //                SM.EM.Security.Login.LogIn(ParentPage.em_user.USERNAME, false, "");

        }


        Response.Redirect(SM.EM.Rewrite.EmenikUserSettingsUrlByStep(ParentPageEmenik.PageName, step));

        return true;
        
    }

    public override bool Validate()
    {
        rfvPageName.Enabled = rbPageName.Checked;
        cvPageName.Enabled = rbPageName.Checked;
        cvPageNameValid.Enabled = rbPageName.Checked;

        cvDomain.Enabled = rbCustomDomain.Checked;
        rfvCustomDomain.Enabled = rbCustomDomain.Checked;

        rfvBuyDomain.Enabled = rbBuyDomain.Checked;
        cvBuyDomainExists.Enabled = rbBuyDomain.Checked;


        Page.Validate("AdvSet");
        if (!Page.IsValid)
        {
            ErrorText = "Shranjevanje ni uspelo. Prosim preveri vnešene podatke.";
            return false;
        }

        return true;

    }


    protected void ValidatePageName(object o, ServerValidateEventArgs args)
    {
        args.IsValid = true;

        // check for duplicate page name
        if (tbPageName.Text.Contains(SM.BLL.Common.Emenik.Data.PortalHostDomain()))
        {

            args.IsValid = false;
            CustomValidator val = o as CustomValidator;
            val.ErrorMessage = "Poddomena ne sme vsebovati domene " + SM.BLL.Common.Emenik.Data.PortalHostDomain();
        }
    }

    protected void ValidateDuplicate(object source, ServerValidateEventArgs args)
    {
        if (EM_USER.ExistDuplicatePageName(ParentPageEmenik.em_user.UserId, SM.EM.Rewrite.ConvertToFriendlyURL(tbPageName.Text, ".")))
        {
            args.IsValid = false;
            return;
        }

        args.IsValid = true;
    }

    protected void ValidateDuplicateDomain(object source, ServerValidateEventArgs args)
    {
        if (string.IsNullOrEmpty(tbDomain.Text.Trim()))
            return;
        if (EM_USER.ExistDuplicateDomain(ParentPageEmenik.em_user.UserId, SM.EM.Helpers.FormatDomain(tbDomain.Text)))
        {
            args.IsValid = false;
            CustomValidator val = (CustomValidator)source;
            val.ErrorMessage = "* Domena je v sistemu " + SM.BLL.Common.Emenik.Data.PortalName() + " že zasedena. Če je prišlo do konflikta, nas kontaktiraj na " + SM.EM.Mail.Account.Support.Email;

            return;
        }
        else if (tbDomain.Text.Contains(SM.BLL.Common.Emenik.Data.PortalName()))
        {
            args.IsValid = false;
            CustomValidator val = (CustomValidator)source;
            val.ErrorMessage = "* Lastna domena ni pravilna";

            return;
        
        }

        args.IsValid = true;
    }

    protected void ValidateDomain(object o, ServerValidateEventArgs e)
    {
        // check domain if is free
        string domain = tbBuyDomain.Text.Trim().Replace("www.", "");

        try
        {
            if (SM.EM.Helpers.IsDomainTaken(domain))
            {
                e.IsValid = false;
                CustomValidator val = o as CustomValidator;
                val.ErrorMessage = "* Domena " + domain + " je že zasedena. Prosim izberi drugo domeno.";
            }

        }
        catch
        {
        }

    }


    protected void btOrderDomain_Click(object o, EventArgs e) {
        
        //// validate
        //rfvBuyDomain.Validate();
        //cvBuyDomainExists.Validate();

        //if (!(rfvBuyDomain.IsValid && cvBuyDomainExists.IsValid))
        //    return;


        //// create ticket
        //TICKET.CreateTicket(Guid.NewGuid(), ParentPageEmenik.em_user.UserId, "Naročilo domene (zapoznelo) " + ParentPageEmenik. em_user.USERNAME, tbBuyDomain.Text , TICKET.Common.Type.ORDER_DOMAIN, STATUS.Common.Status.TicketOrderStart, STATUS.Common.Type.TicketOrder, Guid.Empty, TICKET.Common.Priority.ORDER_ITEM, true);

        //// update domain is ordered
        //EM_USER.UpdateOrderDomain(ParentPageEmenik.em_user.UserId, tbBuyDomain.Text, ORDER.Common.DomainOrderStatus.ORDERED);
    
    }


}
