﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_cmsUpdateProgress.ascx.cs" Inherits="SM.EM.UI.CMS.Controls.CMS_cmsUpdateProgress" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
    
    <div class="uppBcg"></div>

    <asp:Panel  ID="panProgress" runat="server" >
    
        <div class="uppProgress">
            <asp:Image  ID="ajaxLoadNotificationImage" runat="server" ImageUrl="~/cms/_res/images/icon/ajax-loader.gif" AlternateText="[image]" />
            <br />
            Loading... 
        </div>
    
    </asp:Panel>
    
    <cc1:AlwaysVisibleControlExtender ID="avceProgress" runat="server" TargetControlID="panProgress" 
                                                HorizontalSide="Center" VerticalSide="Middle" VerticalOffset="0" >
    </cc1:AlwaysVisibleControlExtender>
            
