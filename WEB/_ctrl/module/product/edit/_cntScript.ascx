﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_cntScript.ascx.cs" Inherits="_ctrl_module_product_edit_cntScript" %>
<script type="text/javascript">
    var refrFileList = false;



    function newProductLoad(smp) {
        setModalLoaderLoading();
        openModalLoading('Nalagam');
        var _items = new Array();
        Array.add(_items, smp);
        Array.add(_items, em_g_lang);
        WebServiceCWP.RenderViewAut(_items, 1, onPopupSuccessLoad, onError);
    }

    function onProductNewSuccessSaveRedirect(ret) {
        if (ret.Code == 0) {
            setModalLoaderErrValidation('', ret.Message);
            return;
        }
        else if (ret.Code != 1) {
            setModalLoaderErr(ret.Message, true);
            return;
        }
        closeModalLoader();


        if (ret.Data != null && ret.Data.length > 0)
            document.location.href = ret.Data;
        else        // todo: porpavi na ajax
            document.location.reload(false);
            

        //$("#divModalLoader" + mpl).init_mpl().dialog('open').dialog('option', 'width', 810).dialog('option', 'position', 'center').parent().find("div.ui-dialog-titlebar").hide();
    }
    function onProductSaveError(err) {
        logError(err);
        setModalLoaderErr("Prišlo je do napake. Prosim poskusite ponovno.", false);
    }



    function editProduct(id) {
        setModalLoaderLoading();
        openModalLoading('Nalagam');
        var _items = new Array();
        Array.add(_items, id);
        Array.add(_items, em_g_lang);
        WebServiceCWP.RenderViewAut(_items, 3, onPopupSuccessLoad, onError);
    }


    function editProductDesc(id) {
        setModalLoaderLoading();
        openModalLoading('Nalagam');
        var _items = new Array();
        Array.add(_items, id);
        WebServiceCWP.RenderViewAut(_items, 4, onProductDescSuccessLoad, onError);
    }
    function onProductDescSuccessLoad(ret) {
        if (ret.Code == 1) {
            setModalLoader(ret.Data);
            closeModalLoading();
        }
        else if (ret.Code == 0) {
            setModalLoaderErr(ret.Message, true);
            return;
        }
        $("#divModalLoader" + mpl).init_mpl().dialog('option', 'open', function () { $("#prodDescTabs textarea").ckeditor(function () { $("#prodDescTabs").tabs(); }, { baseFloatZIndex: 200000 }); })
        .dialog('open').dialog('option', 'width', 810).dialog('option', 'position', 'center').parent().find("div.ui-dialog-titlebar").hide();
    }

    function editProductDescFCK(id) {
        setModalLoaderLoading();
        openModalLoading('Nalagam');
        var _items = new Array();
        Array.add(_items, id);
        WebServiceCWP.RenderViewAut(_items, 14, onProductDescSuccessLoadFCK, onError);
    }
    function editProductDescFCKshort(id) {
        setModalLoaderLoading();
        openModalLoading('Nalagam');
        var _items = new Array();
        Array.add(_items, id);
        WebServiceCWP.RenderViewAut(_items, 20, onProductDescSuccessLoadFCK, onError);
    }
    function onProductDescSuccessLoadFCK(ret) {
        if (ret.Code == 1) {
            setModalLoader(ret.Data);
            closeModalLoading();
        }
        else if (ret.Code == 0) {
            setModalLoaderErr(ret.Message, true);
            return;
        }
        $("#divModalLoader" + mpl).init_mpl().dialog('option', 'open', function () {
            $.fck.config = { floatingPanelsZIndex: 200000 };
            $("#prodDescTabs textarea").fck({ path: em_g_fckbp, toolbar: 'CMS_Content', FloatingPanelsZIndex: 200000 });
  //          $("#prodDescTabs textarea").fck();
        }).dialog('open').dialog('option', 'width', 810).dialog('option', 'position', 'center').parent().find("div.ui-dialog-titlebar").hide();
        $("#prodDescTabs").tabs();
    }

    function editProductDescJH(id) {
        setModalLoaderLoading();
        openModalLoading('Nalagam');
        var _items = new Array();
        Array.add(_items, id);
        WebServiceCWP.RenderViewAut(_items, 4, onProductDescSuccessLoad, onError);
    }
    function onProductDescSuccessLoadJH(ret) {
        if (ret.Code == 1) {
            setModalLoader(ret.Data);
            closeModalLoading();
        }
        else if (ret.Code == 0) {
            setModalLoaderErr(ret.Message, true);
            return;
        }
        $("#divModalLoader" + mpl).init_mpl().dialog('open').dialog('option', 'width', 810).dialog('option', 'position', 'center').parent().find("div.ui-dialog-titlebar").hide();
        $("#prodDescTabs textarea").htmlarea({
            toolbar: ["html", "|", "bold", "italic", "underline", "|", "orderedlist", "unorderedlist", "|", "indent", "outdent", "|", "h1", "h2", "h3", "h4", "|", "link", "unlink"]
        });
        $("#prodDescTabs").tabs();
    } 

    function onProductEditSuccessSave(ret, id) {
        if (ret.Code == 0) {
            setModalLoaderErrValidation('', ret.Message);
            return;
        }
        else if (ret.Code != 1) {
            setModalLoaderErr(ret.Message, true);
            return;
        }
        closeModalLoading();
        closeModalLoader();

        $('#pd' + id).html(ret.Data).sm_animate_change();
    } 


    function editProductAttribute(pid, gid) {
        setModalLoaderLoading();
        openModalLoading('Nalagam');
        var _items = new Array();
        Array.add(_items, pid);
        Array.add(_items, gid);
        WebServiceCWP.RenderViewAut(_items, 5, onProductDescSuccessLoad, onError);
    }

    function editProductFile(id) {
        refrFileList = false;
        setModalLoaderLoading();
        openModalLoading('Nalagam');
        var _items = new Array();
        Array.add(_items, id);
        WebServiceCWP.RenderViewAut(_items, 15, onProductFileSuccessLoad, onError);
    }
    function onProductFileSuccessLoad(ret) {
        if (ret.Code == 1) {
            setModalLoader(ret.Data);
            closeModalLoading();
        }
        else if (ret.Code == 0) {
            setModalLoaderErr(ret.Message, true);
            return;
        }
        $("#divModalLoader" + mpl).init_mpl().dialog('open').dialog('option', 'width', 810).dialog('option', 'position', 'center').parent().find("div.ui-dialog-titlebar").hide();
        $("#prodFileTabs").tabs({ select: function (event, ui) { window.location.hash = ui.tab.hash; } });

        $("#prodFileTabs .viewFileLangList").each(function () {
            init_fu_file(this.id);
        });

    }

    function reloadProductDetail(_pid) {
        var _items = new Array();
        Array.add(_items, _pid);
        Array.add(_items, em_g_lang);
        WebServiceCWP.RenderViewAut(_items, 12, onProductEditSuccessSave, onError, _pid);

    }
    
    
    
       

</script>