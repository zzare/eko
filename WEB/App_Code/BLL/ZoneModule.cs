﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

/// <summary>
/// Summary description for  ZONE_MODULE
/// </summary>
public partial class ZONE_MODULE
{


    public static ZONE_MODULE GetZoneModuleByID(int id)
    {
        eMenikDataContext db = new eMenikDataContext();
        ZONE_MODULE list;

        list = (from m in db.ZONE_MODULEs where m.ZMP_ID == id select m).SingleOrDefault();

        return list;
    }
    public static ZONE_MODULE GetZoneModule(int pmodID, int modID, string zone)
    {
        eMenikDataContext db = new eMenikDataContext();
        ZONE_MODULE list;

        list = (from m in db.ZONE_MODULEs where m.PMOD_ID == pmodID && m.MOD_ID == modID && m.ZONE == zone  select m).SingleOrDefault ();

        return list;
    }
}
