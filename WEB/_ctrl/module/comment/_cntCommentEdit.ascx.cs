﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SM.EM.BLL;
using System.Net.Mail;

namespace SM.EM.UI.Controls
{
    public partial class _cntCommentEdit : BaseControl 
    {
        public delegate void OnEditEventHandler(object sender, SM.BLL.Common.Args.IDguidEventArgs e );
        public event OnEditEventHandler OnListChanged;

        protected EM_USER _usr;
        public EM_USER user
        {
            get
            {
                if (_usr == null)
                {
                    _usr = EM_USER.GetUserByUserName(Page.User.Identity.Name);
                }
                return _usr;
            }

            set { _usr = value; }
        }
        public int CommentRefID
        {
            get
            {
                return (int)(ViewState["CommentRefID"] ?? -1);        
        
        }
            set { ViewState["CommentRefID"] = value; }
        }
        public int RefInt { get; set; }
        public Guid UserID { get; set; }
        public bool IsAuthor { get; set; }
        public bool IsAdmin { get; set; }
        public bool ShowEmail { get; set; }
        public bool ShowWWW { get; set; }
        public bool ShowName { get; set; }
        public bool ShowTitle { get; set; }
        public string ForceTitle { get; set; }
        public bool EnableNonAuthenticatedComments { get; set; }


        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            // init data
            InitData();

        }
        public virtual void InitData()
        {



            // init user id data
            if (Page.User.Identity.IsAuthenticated)
            {

                UserID = SM.EM.BLL.Membership.GetCurrentUserID();


            }


            // init
            if (!EnableNonAuthenticatedComments && !Page.User.Identity.IsAuthenticated)
            {
                phComment.Visible = false;
                phLogin.Visible = true;
            }
            else
            {
                phComment.Visible = true;
                phLogin.Visible = false;

            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {



            if (!IsPostBack) {
                LoadData();

            }


        }

        public  void LoadData()
        {



            InitFields();


            if (CommentRefID > 0)
                LoadDataOld();
            else
                LoadDataNew();
        
        }

        protected void InitFields() {
            trEmail.Visible = ShowEmail;
            trTitle.Visible = ShowTitle;
            trWWW.Visible = ShowWWW;
            trName .Visible = ShowName;
            
            // hide for logged users
            if (UserID != Guid.Empty)
            {
                trWWW.Visible = false;
                trName.Visible = false;

            }

            // if user is EDITING, show all
            if (CommentRefID > 0)
            {
                trEmail.Visible = true;
                trTitle.Visible = true;
                trWWW.Visible = true;
                trName.Visible = true;

            
            }
        
        }



        protected void LoadDataNew(){
            tbName.Text  = "";
            tbEmail.Text = "";
            tbSubject.Text = "";
            tbBody.Text = "";
            tbWWW.Text = "";

            // pre set
            if (user != null) {
                tbName.Text = user.PAGE_TITLE;
//                tbWWW.Text = Page.ResolveUrl(SM.EM.Rewrite.EmenikUserUrl(user ));           
            
            }
        
        }

        protected void LoadDataOld(){
            COMMENT c = COMMENT.GetCommentByID(CommentRefID);

            tbName.Text = c.UserName ;
            tbEmail.Text = c.Email ;
            tbSubject.Text = c.Title ;
            tbBody.Text = c.Comment ;
            tbWWW.Text = c.WWW ;

        
        
        }


        // bind data
        public void BindData()
        {

            
        }

        //protected void btCancelEdit_Click(object sender, EventArgs e)
        //{
        //    mpeEdit.Hide();

        //}
        //protected void btSaveEdit_Click(object sender, EventArgs  e)
        //{
           

        //}

        public void SaveData()
        {


            Page.Validate("vgComm");
            if (!Page.IsValid)
            {
                return;
            }
            eMenikDataContext db = new eMenikDataContext();
            COMMENT comm = new COMMENT();
            if (CommentRefID < 1)// insert new COMMENT
            {
         
                //PreSave();
                string title = tbSubject.Text;
                if (!string.IsNullOrEmpty(ForceTitle ))
                    title = ForceTitle;

                string www = tbWWW.Text.Trim();
                if (!www.StartsWith("http://"))
                    www = "http://" + www;



                var cust = CUSTOMER.GetCurrentCustomer();
                string name = tbName.Text;
                string email = tbEmail.Text ;
                if (cust != null){
                    name = cust.GetName;
                    email = cust.EMAIL;
                }

            
                comm = COMMENT.AddComment(RefInt, UserID, name, www, email, title, tbBody.Text, IsAuthor);

                var art = ARTICLE.GetArticleByID(RefInt);
                if (art != null)
                {

                    // send email to admin
                    string mailTo = SM.EM.Mail.Account.Info.Email;// "info@terra-gourmet.com";
                    if (SM.BLL.Common.Emenik.Data.IsTestMode() && Context.Request.IsLocal)
                        mailTo = "zzzare@gmail.com";
                    // send mail
                    MailMessage message = new MailMessage();
                    message.From = new MailAddress(SM.EM.Mail.Account.Info.Email);
                    message.To.Add(new MailAddress(mailTo));

                    if (user == null)
                        user = EM_USER.GetUserByID(SM.BLL.Custom.MainUserID);

                    message.Subject = "Nov komentar dodan" + ":" + art.ART_TITLE;
                    message.Body = "URL: " + SM.BLL.Common.ResolveUrlFull(SM.EM.Rewrite.EmenikUserArticleDetailsUrl(user.PAGE_NAME, art.ART_ID, art.ART_TITLE));
                    message.Body += "<br/>" + comm.Comment ;
                    message.IsBodyHtml = true;

                    SmtpClient client = new SmtpClient();


                    try
                    {

                        if (HttpContext.Current != null && HttpContext.Current.Request.IsLocal)
                        {
                            client.Credentials = new System.Net.NetworkCredential(SM.EM.Mail.Account.SendGmail.Email, SM.EM.Mail.Account.SendGmail.Pass);
                            client.Port = 587;//or use 587            
                            client.Host = "smtp.gmail.com";
                            client.EnableSsl = true;
                        }
                        else
                        {
                            //                client.EnableSsl = true;
                            client.DeliveryMethod = SmtpDeliveryMethod.Network;
                            client.Host = "1dva3.si";

                        }
                        client.Send(message);
                    }
                    catch (Exception ex)
                    {
                        ERROR_LOG.LogError(ex, "SaveBlogEdit send email");
                    }

                }
            }
            else
            { // update old comment
                comm = db.COMMENTs.Where(w => w.CommentRefID == CommentRefID).SingleOrDefault();
                if (comm == null)
                    return;

                comm.Title = tbSubject.Text ;
                comm.Comment = tbBody.Text ;
                comm.WWW = tbWWW .Text ;
                comm.Email = tbEmail.Text ;
                comm.UserName = tbName.Text;

                db.SubmitChanges();                
            }

            // clear form
            LoadDataNew();
            phConfirmation.Visible = true;

            // redirect to same page, scroll to comment

            OnListChanged(this, new SM.BLL.Common.Args.IDguidEventArgs { ID=comm.CommentID });

            //Response.Redirect(Request.Url.AbsoluteUri + "#comm" + comm.CommentID.ToString());

        }

    
    





        protected void btSend_Click(object sender, EventArgs e)
        {
            SaveData();
        }
}
}
