﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SM.UI.Controls;
using SM.EM.UI.Controls;
using System.Collections.Generic;

/// <summary>
/// Summary description for BasePage
/// </summary>
namespace SM.EM.UI
{

    public class BasePage_CmsWebPart : BasePage_EMENIK 
    {
        public  Dictionary<string,  CwpZone> Zones;
        public List<CMS_WEB_PART> CwpList;
        public CwpEdit EditArticle;
        public CwpEdit EditContent;
        public CwpEdit EditGallery;
        public CwpEdit EditImageNew;
        public CwpEdit EditImage;
        public CwpEdit GallerySettings;
        public CwpEdit ArticleSettings;
        public CwpEdit ContentSettings;
        public CwpEdit ContactFormSettings;
        public CwpEdit MapSettings;

        public bool HasGallery = false;
        public bool HasMap = false;



        protected Guid? _CwpRefID = Guid.Empty;
        public virtual Guid? CwpRefID
        {
            get
            {
                // if set, ok
                if (_CwpRefID != Guid.Empty )
                    return _CwpRefID;


                if (CurrentSitemapID > 0)
                {
                    // get ref id from sitemap
                    SITEMAP smp = SITEMAP.GetSiteMapByID(CurrentSitemapID);
                    if (smp != null)
                        _CwpRefID = smp.SMAP_GUID;
                }
                return _CwpRefID;
            }
            set{_CwpRefID = value;}
        }




        protected override void OnPreInit(EventArgs e)
        {
            Trace.Write("OnPreInit - start");

            // init zones dictionary
            Zones = new Dictionary<string, CwpZone>();
            
            base.OnPreInit(e);


            Trace.Write("OnPreInit - end");
            
        }

        protected override void OnInit(EventArgs e)
        {
            Trace.Write("OnInit - start");




            base.OnInit(e);

            if (IsEditMode && IsContentPage)
            {
                // default is CMS mode 
                if (SelectedStatusMenu != SM.BLL.Common.EditModeStatus.View)
                {
                    SelectedStatusMenu = SM.BLL.Common.EditModeStatus.CMS;
                    Trace.Warn("2- SelectedStatusMenu = " + SM.BLL.Common.EditModeStatus.CMS.ToString());

                }
            }


            // load web parts
            Trace.Write("LoadWebParts - start");
            LoadWebParts();
            Trace.Write("LoadWebParts - end");


            // disable partial rendering on emenik content pages
            //if (DisablePartialRendering)
                //ScriptManager.GetCurrent(this).EnablePartialRendering = false;

            //// add dummy zones if doesn' exist
            //DynamicWebPartManager wpManager = (DynamicWebPartManager)WebPartManager.GetCurrentWebPartManager(this);
            //foreach (string zoneID in SM.BLL.Common.Zones.Items())
            //{

            //    if (wpManager.Zones[zoneID] == null)
            //    {
            //        SM.EM.UI.Controls.EmenikWebPartZone zone = new SM.EM.UI.Controls.EmenikWebPartZone();
            //        zone.ID = zoneID;
            //        zone.Visible = false;
            //        ContentPlaceHolder cph = (ContentPlaceHolder)Master.FindControl("cph");
            //        cph.Controls.Add(zone);

            //    }
            //}

            Trace.Write("OnInit - end");

        }


        protected override void OnInitComplete(EventArgs e)
        {
            Trace.Write("OnInitComplete - start");

            base.OnInitComplete(e);

            Trace.Write("OnInitComplete - end");

        }

        protected override void LoadViewState(object savedState)
        {
            Trace.Write("LoadViewState - start");

            base.LoadViewState(savedState);


            // rebind webparts (for load properly after viewstate)
            LoadWebPartData();

            Trace.Write("LoadViewState - end");
        }

        protected override void OnLoad(EventArgs e)
        {
            Trace.Write("OnLoad - start");

            //// load web parts
            //LoadWebParts();

            
            base.OnLoad(e);

            Trace.Write("OnLoad - end");

        }

        public virtual void  LoadCwpList(){

            bool? active = true;
            if (IsModeCMS) active = null; // if edit mode, show all

            // get all webparts on current page by language ( not deleted ones)
            CwpList = CMS_WEB_PART.GetWebpartsBySitemapLangUserModules(CurrentSitemapID, LANGUAGE.GetCurrentLang(), active, false, em_user.UserId);
            
        
        }

        public void LoadWebParts()
        { 
        



            // get all webparts on current page by language ( not deleted ones)
            //CwpList = CMS_WEB_PART.GetWebpartsBySitemapLangUserModules(CurrentSitemapID, LANGUAGE.GetCurrentLang(), active, false, em_user.UserId );
            LoadCwpList();
            // get all zones
            foreach (KeyValuePair <string, CwpZone> zone in Zones) {

                // load zone
                zone.Value.ContentPlaceholder.Clear();
                LoadZone(zone.Value );
            }
        }

        public void ReLoadZone(CwpZone zone, bool reloadFromDB) {
            if (reloadFromDB)
            {
                bool? active = true;
                if (IsModeCMS) active = null; // if edit mode, show all
                LoadCwpList();
                //CwpList = CMS_WEB_PART.GetWebpartsBySitemapLangUserModules(CurrentSitemapID, LANGUAGE.GetCurrentLang(), active, false, em_user.UserId);
            }

            zone.ContentPlaceholder.Clear();
            LoadZone(zone);
        }
        
        public  void LoadZone(CwpZone zone ) {
            // get all webparts in zone
            List<CMS_WEB_PART> cwpZone = (from w in CwpList where w.ZONE_ID == zone.ID && w.DELETED == false orderby w.ORDER, w.DATE_ADDED descending    select w).ToList();

            foreach (CMS_WEB_PART wp in cwpZone) {
                LoadCwpToZone(wp, zone);
            } 
        
        }

        protected void LoadCwpToZone(CMS_WEB_PART wp, CwpZone zone) {
            if (zone == null) return;
            if (wp == null) return;

            Trace.Write("LoadCwpToZone - start" );
            
            // load webpart control
            BaseControl_CmsWebPart uc = (BaseControl_CmsWebPart)this.LoadControl(@wp.MODULE.MOD_SRC);
            if (uc == null) return;

            uc.ID = "obj" + wp.MODULE.MOD_TITLE + "_" + wp.CWP_ID.ToString();
            
            // add webpart to zone
            //zone.Controls.Add(uc);
            zone.ContentPlaceholder.AddAt(zone.ContentPlaceholder.Count, uc);

            uc.cwp = wp;
            uc.ParentPage = this;

            // load control data (!!! after control is added, for events to function properly !!!!)
            //if (!IsPostBack )
                uc.LoadData();

                switch (wp.MOD_ID)
                {
                    case MODULE.Common.GalleryModID:
                        HasGallery = true;
                        break;
                    case MODULE.Common.MapModID:
                        HasMap  = true;
                        break;
                    default:
                        break;
                }

                Trace.Write("LoadCwpToZone - end");

        }


        // calls load data function for all CWP controls
        public void LoadWebPartData()
        {
            // get all zones
            foreach (KeyValuePair<string, CwpZone> zone in Zones)
            {
                LoadZoneCwpData(zone.Value);
            }
        }
        public void LoadZoneCwpData(CwpZone zone)
        {
            foreach (Control  wp in zone.ContentPlaceholder  )
            {
                if (wp is BaseControl_CmsWebPart)
                    (wp as BaseControl_CmsWebPart).LoadData();
            }
        }

            








        public void AddEmptyCwpToZone(Guid? refID,  int modID, string zoneName, int pageModuleId, int pos, bool reloadZone)
        {

            CMS_WEB_PART.CreateNewCwp(em_user.UserId, refID, LANGUAGE.GetCurrentLang(), modID, zoneName, pageModuleId, pos);

            if (reloadZone)
                ReLoadZone(Zones[zoneName], true);
            //// load webpart
            //if (wp != null)
            //{
            //    LoadCwpToZone(wp, Zones[zoneName]);
            //}
 
        }


        protected override void OnPreRender(EventArgs e)
        {
            Trace.Write("OnPreRender - start");

            RegisterDocumentReadyScript += "em_init();";


            // if map, register script and CSS
            //if (HasMap)
            //    Helpers.RegisterScriptIncludeAJAX("http://maps.google.com/maps?file=api&amp;v=2&amp;sensor=true_or_false&amp;key=" + SM.BLL.Common.Emenik.Data.APIkey(), this, "google.maps.api");



            if (IsModeCMS && IsContentPage)
            {
                // if map, register script and CSS
                if (HasMap)
                {
                    //Helpers.AddLinkedStyleSheet(this, "http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.1/themes/base/jquery-ui.css");
                    //RegisterDocumentReadyScriptStart += " map_init();";

                    string mapEditInit = " mapEditSrc = '" + Page.ResolveUrl(CWP_MAP.Common.RenderMapEditHref(Page.Theme)) + "'; ";
                    RegisterDocumentReadyScriptStart += mapEditInit;
                }



                RegisterDocumentReadyScript += " cwp_init(); gal_init();" ;


                // init cwp

                // init fck
//                string fck = " for ( var i = 0; i < parent.frames.length; ++i ) if ( parent.frames[i].FCK ) parent.frames[i].FCK.UpdateLinkedField(); ";
//                string fck = " $('frames:not(iframe)') if ( parent.frames[i].FCK ) parent.frames[i].FCK.UpdateLinkedField(); ";
  
//                this.Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "AjaxFCKHack", fck );
            }


            // jCarousel
            SM.EM.Helpers.RegisterScriptIncludeAJAX(SM.BLL.Common.Emenik.ResolveMainDomainUrl("~/Javascript/jCarousel/lib/jquery.jcarousel.pack.js"), Page, "jCarousel");
            SM.EM.Helpers.AddLinkedStyleSheet(Page, "~/Javascript/jCarousel/lib/jquery.jcarousel.css");
            SM.EM.Helpers.AddLinkedStyleSheet(Page, "~/Javascript/jCarousel/skins/tango/skin.css");

            // load fancybox js
            SM.EM.Helpers.RegisterScriptIncludeAJAX(SM.BLL.Common.Emenik.ResolveMainDomainUrl("~/JavaScript/fancybox/jquery.fancybox-1.3.0.pack.js"), Page, "fancybox");
            SM.EM.Helpers.RegisterScriptIncludeAJAX(SM.BLL.Common.Emenik.ResolveMainDomainUrl("~/JavaScript/fancybox/jquery.easing-1.3.pack.js"), Page, "easing");
            // load css
            SM.EM.Helpers.AddLinkedStyleSheet(Page, SM.BLL.Common.Emenik.ResolveMainDomainUrl("~/javascript/fancybox/jquery.fancybox-1.3.0.css"), false);




            // custom CSS
            AddCustomCss();

            base.OnPreRender(e);

            Trace.Write("OnPreRender - end");

        }

        public override void OutputCaching()
        {
            //Response.Write("TIME:" + DateTime.Now);
            //return;
            if (IsEditMode)
            {
                // if previewing, also remove from cache (temporary solution)
                if (IsModePreview)
                    RemovePageFromCache();

                return;
            }

            if (this.IsPostBack) return;

            if (OutputCacheDuration <= 0)
                return;

            // cache page if not edit mode - only preview
            TimeSpan freshness = new TimeSpan(0, 0, 0, OutputCacheDuration);
            Response.Cache.SetCacheability(HttpCacheability.Server);
            Response.Cache.SetExpires(DateTime.Now.Add( freshness));
            //Response.Cache.SetMaxAge(new TimeSpan(0, 0, 0, 30));
            Response.Cache.SetValidUntilExpires(true);
//            Response.Cache.SetLastModified(this.Context.Timestamp);
            Response.Cache.VaryByParams.IgnoreParams = false;
            Response.Cache.VaryByParams["*"] = true;
            Response.Cache.SetVaryByCustom ("cms_user;ie6;req_login"); // eko: req_login 
 // add dependencies for invalidation
            string cacheKey = SM.EM.Caching.Dependency.Key.PageRefID(CwpRefID ?? Guid.Empty );
            Cache[cacheKey] = new object();
            Response.AddCacheItemDependency(cacheKey);
            



            //test
            //Response.Charset = "UTF-8";
            if (OutputCacheSliding )
            {
                Response.Cache.SetSlidingExpiration(true);
                Response.Cache.SetETagFromFileDependencies(); //important to work with sliding expiration
            }
            //Response.Cache .SetAllowResponseInBrowserHistory(true);


            //Response.Cache.VaryByHeaders["ACCEPT-CHARSET"] = true;
            //Response.Cache.VaryByHeaders["ACCEPT-ENCODING"] = true;
            //Response.Charset = "utf-8";
            //Response.Cache.VaryByHeaders["Accept-Language"] = true;

        }

        public override void RemovePageFromCache()
        {
            string cacheKey = SM.EM.Caching.Dependency.Key.PageRefID(CwpRefID ?? Guid.Empty);
            Cache.Remove(cacheKey);

            //base.RemovePageFromCache();
        }


        public void AddCustomCss() {

            if (CurrentSitemapIDMenu != 1444)
                return;

            // add custom CSS scripts
            HtmlGenericControl css = new HtmlGenericControl();
            css.TagName = "style";
            css.Attributes["type"] = "text/css";
            string absoluteurl = Request.Url.GetLeftPart(UriPartial.Authority ) + Page.ResolveUrl("~/_em/templates/_custom/zavetisceljubljana/_inc/images/design/background_blue.jpg");

            css.InnerHtml = "body{background:#62C4FF url(" + absoluteurl + ") repeat-x center top;}";
            Page.Header.Controls.Add(css);
        
        
        
        }
 
   }
}