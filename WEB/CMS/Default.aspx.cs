﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace SM.EM.UI.CMS
{

    public partial class CMS_Default : BasePage_CMS
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            CheckPermissions = false;


        }


        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}