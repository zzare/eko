﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using SM.EM.BLL;
namespace SM.EM.UI.Controls
{
    public partial class _popup_viewGalleryShowTMP : BaseControlView
    {

        public object Data;
        public object[] Parameters;
        
        protected IEnumerable <WebServiceP.GalImage> ImageLangList;
        public int CurrIndex = 0;




        protected void Page_Load(object sender, EventArgs e)
        {

            BindData();
        }
        // bind data
        public void BindData()
        {

            if (Data == null && ImageLangList == null)
                return;

            // data
            if (ImageLangList == null)
                ImageLangList = ((IQueryable<WebServiceP.GalImage>)Data).ToList();
            if (ImageLangList == null)
                return;
            if (Parameters != null)
            {
                if (Parameters.Length > 0 && Parameters[0] != null)
                    IsModeCMS = bool.Parse(Parameters[0].ToString());

                if (Parameters.Length > 1 && Parameters[1] != null)
                    CurrIndex  = int.Parse(Parameters[1].ToString());
            }

            // resolve URl
            foreach (WebServiceP.GalImage img in ImageLangList)
            {
                img.UrlBig = SM.BLL.Common.ResolveUrl(img.UrlBig);
            }

            lvList.DataSource = ImageLangList;
            lvList.DataBind();



            //if (lvList.Items.Count < 1 && !IsModeCMS) { this.Visible = false; return; }
            //else
            //    this.Visible = true;

//            Newtonsoft.Json.JsonConvert.SerializeObject(ImageLangList);

        }



        protected string GetJsonData()
        {
            string ret = "";



            ret = Newtonsoft.Json.JsonConvert.SerializeObject(ImageLangList);

            return ret;
        }

        protected string RenderMainImageSrc() {

            string ret = "";


            if (ImageLangList == null)
                return ret;

            var img = ImageLangList.ElementAt(CurrIndex);
            if (img == null)
                return ret;

            ret = img.UrlBig;
            return ret;
        
        }

    }
}
