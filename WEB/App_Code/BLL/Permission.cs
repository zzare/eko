﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Security.Principal;
using System.Web.Caching;
using System.Net.Mail;
using System.Collections.Generic;
using System.Threading;
using System.Linq.Expressions;


/// <summary>
/// Summary description for PERMISSION
/// </summary>

public partial class PERMISSION
{



    // PREDICATES
    //public static Expression<Func<PRODUCT, bool>> IsActiveP()
    //{
    //    return p => (p.ACTIVE == true) && (p.DELETED == false);
    //}
    //public static Expression<Func<vw_Product, bool>> IsActiveVW()
    //{
    //    return p => (p.ACTIVE == true) && (p.DELETED == false);
    //}



    public bool IsValid
    {
        get { return (GetRuleViolations().Count() == 0); }
    }

    public IEnumerable<RuleViolation> GetRuleViolations()
    {
        //if (this.ID_INT == null || this.ID_INT < 1)
        //{
        //    // new product


        //}
        //else {

        //    if (string.IsNullOrEmpty(CODE.Trim()))
        //        yield return new RuleViolation("Šifra ni določena", "CODE");
        //    if (PRICE <= 0)
        //        yield return new RuleViolation("Cena ni določena", "PRICE");
        
        //    }

        //// common
        //if (SMAP_ID < 1)
        //    yield return new RuleViolation("Podkategorija izdelka ni izbrana", "SMAP_ID");
        //if (PageId  == Guid.Empty )
        //    yield return new RuleViolation("Lastništvo izdelka ni določeno", "PageId");


        yield break;
    }

    partial void OnValidate(System.Data.Linq.ChangeAction action)
    {
        if (!IsValid)
            throw new ApplicationException("Podatki niso pravilno vnešeni.");
    }





    /* ********** STATIC ******************/


    /// <summary>
    /// Gets TYPE of permission in DB
    /// </summary>
    public static PERMISSION.Common.Type GetPermissionType(Guid user, Guid scope, Guid obj, PERMISSION.Common.Action action)
    {
        eMenikDataContext db = new eMenikDataContext();

        PERMISSION.Common.Type ret = PERMISSION.Common.Type.Not_Set;

        var p = db.PERMISSIONs.Where(w => w.UserId == user && w.Scope == scope && w.Object == obj && w.Action == (short)action).Select(s => s.Type).SingleOrDefault();
        if (p != null)
            ret = (PERMISSION.Common.Type)p;

        return ret;
    }


    /**************************************************** PERMISSION TYPE */

    /// <summary>
    /// Gets PERMISSION for CWP
    /// </summary>
    public static PERMISSION.Common.Type GetPermissionTypeCWP(Guid user, Guid page, Guid? smap, Guid? cwp, Guid obj, PERMISSION.Common.Action action)
    {
        //PERMISSION.Common.Type ret = PERMISSION.Common.Type.Not_Set;

        if (cwp != null)
        {
            var p = GetPermissionType(user, cwp.Value , obj, action);
            if (p != PERMISSION.Common.Type.Not_Set)
                return p;
        }

        return GetPermissionTypeSMAP(user, page, smap, obj, action); ;
    }

    /// <summary>
    /// Gets PERMISSION for SMAP
    /// </summary>
    public static PERMISSION.Common.Type GetPermissionTypeSMAP(Guid user, Guid page, Guid? smap, Guid obj, PERMISSION.Common.Action action)
    {
        //PERMISSION.Common.Type ret = PERMISSION.Common.Type.Not_Set;

        if (smap != null)
        {
            var p = GetPermissionType(user, smap.Value , obj, action);
            if (p != PERMISSION.Common.Type.Not_Set)
                return p;
        }

        return GetPermissionTypePAGE(user, page, obj, action); ;
    }

    /// <summary>
    /// Gets PERMISSION for PAGE
    /// </summary>
    public static PERMISSION.Common.Type GetPermissionTypePAGE(Guid user, Guid page, Guid obj, PERMISSION.Common.Action action)
    {
        PERMISSION.Common.Type ret = PERMISSION.Common.Type.Not_Set;

        var p = GetPermissionType(user, page, obj, action);
        if (p != PERMISSION.Common.Type.Not_Set)
            return p;


        // try all pages
        p = GetPermissionType(user, Common.Scope.ALL, obj, action);
        if (p != PERMISSION.Common.Type.Not_Set)
            return p;

        return ret;
    }





    /******************************************* HAS PERMISSION */
    /// <summary>
    /// Check if user has permission for action for ELEMENT 
    /// </summary>
    public static bool HasPermissionTypeMODULE(Guid user, Guid page, Guid? smap, Guid? cwp, Guid obj, PERMISSION.Common.Action action, PERMISSION.Common.Type typ)
    {
        // todo: cache!

        bool ret = false;

        // check for permission for this ACTION
        PERMISSION.Common.Type p = GetPermissionTypeCWP(user, page, smap, cwp, obj, action);
        if (p != PERMISSION.Common.Type.Not_Set)
            return (p == typ);


        // check for permissions for action: ALL
        p = GetPermissionTypeCWP(user, page, smap, cwp, obj, PERMISSION.Common.Action.ALL);
        if (p != PERMISSION.Common.Type.Not_Set)
            return (p == typ);


        // check for permissions for object MOD_ALL (watch for recursion)
        if (obj != PERMISSION.Common.Obj.MOD_ALL)
            return HasPermissionTypeMODULE(user, page, smap, cwp, PERMISSION.Common.Obj.MOD_ALL, action, typ);


        // if note set otherwise, allow to owner
        if (user == page)
            return true;

        return ret;
    }

    /// <summary>
    /// Check if user has permission for action for ELEMENT 
    /// </summary>
    public static bool HasPermissionTypePAGE(Guid user, Guid page,  Guid obj, PERMISSION.Common.Action action, PERMISSION.Common.Type typ)
    {
        // todo: cache!

        bool ret = false;

        // check for permission for this ACTION
        PERMISSION.Common.Type p = GetPermissionTypePAGE(user, page, obj, action);
        if (p != PERMISSION.Common.Type.Not_Set)
            return (p == typ);


        // check for permissions for action: ALL
        p = GetPermissionTypePAGE(user, page, obj, PERMISSION.Common.Action.ALL);
        if (p != PERMISSION.Common.Type.Not_Set)
            return (p == typ);

        // if note set otherwise, allow to owner
        if (user == page)
            return true;

        return ret;
    }





    public class User
    {
        public static bool HasEditModePermission()
        {
            bool ret = false;

            // todo: optimize

            if (SM.EM.Security.Permissions.IsAdmin())
                return true;
            if (SM.EM.Security.Permissions.IsPortalAdmin())
                return true;

            if (HttpContext.Current == null)
                return ret;
            else if (!HttpContext.Current.User.Identity.IsAuthenticated)
                return ret;
            else if (HttpContext.Current.User.IsInRole("is_editor_all"))
            {
                return true;
            }





            // check if owner
            EM_USER page = EM_USER.Current.GetCurrentPage();
            if (page == null)
                return false;
            if (HttpContext.Current.User.Identity.Name.Trim().ToLower() == page.USERNAME.ToLower())
                return true;

            // todo: add page_level editor perm

            return ret;
        }

        // users permission for MODULEs
        public static bool HasPermissionTypeMODULE(Guid page, Guid obj, PERMISSION.Common.Action action, PERMISSION.Common.Type typ)
        {
            return HasPermissionTypeMODULE(page, null, null, obj, action, typ);
        }
        public static bool HasPermissionTypeMODULE(Guid page, Guid smap, Guid obj, PERMISSION.Common.Action action, PERMISSION.Common.Type typ)
        {
            return HasPermissionTypeMODULE(page, smap, null, obj, action, typ);
        }

        public static bool HasPermissionTypeMODULE(Guid page, Guid? smap, Guid? cwp, Guid obj, PERMISSION.Common.Action action, PERMISSION.Common.Type typ)
        {
            bool ret = false;
            if (!HasEditModePermission())
                return ret;

            if (SM.EM.Security.Permissions.IsAdmin())
                return true;

            return PERMISSION.HasPermissionTypeMODULE(SM.EM.BLL.Membership.GetCurrentUserID(), page, smap, cwp, obj, action, typ);
        }

        public static bool HasPermissionTypePAGE(Guid page, Guid obj, PERMISSION.Common.Action action, PERMISSION.Common.Type typ)
        {
            bool ret = false;
            if (!HasEditModePermission())
                return ret;

            if (SM.EM.Security.Permissions.IsAdmin())
                return true;

            return PERMISSION.HasPermissionTypePAGE(SM.EM.BLL.Membership.GetCurrentUserID(), page, obj, action, typ);
        }

        public static bool HasPermissionTypeANIMAL(Guid page, Guid obj, PERMISSION.Common.Action action, PERMISSION.Common.Type typ)
        {
            bool ret = false;
            if (!HasEditModePermission())
                return ret;

            if (SM.EM.Security.Permissions.IsAdmin())
                return true;

            if (SM.EM.Security.Permissions.IsAnimalAdmin())
                return true;

            return PERMISSION.HasPermissionTypePAGE(SM.EM.BLL.Membership.GetCurrentUserID(), page, obj, action, typ);
        }






        public class Common
        {

            public static bool CanEditPageSettings(Guid pageid)
            {

                return PERMISSION.User.HasPermissionTypePAGE(pageid, PERMISSION.Common.Obj.PAGE, PERMISSION.Common.Action.Settings, PERMISSION.Common.Type.Allow);

            }
            public static bool CanAddNewModule(Guid pageid)
            {
                if (SM.EM.Security.Permissions.IsCMSeditor() || SM.EM.Security.Permissions.IsPortalAdmin() || SM.EM.Security.Permissions.IsAdmin())
                    return true;

                return false;

                //return PERMISSION.User.HasPermissionTypePAGE(pageid, PERMISSION.Common.Obj.MOD_ALL , PERMISSION.Common.Action.New , PERMISSION.Common.Type.Allow);

            }

            public static bool CanEditModuleSettings()
            {
                if (SM.EM.Security.Permissions.IsAdmin())
                    return true;

                if (SM.EM.Security.Permissions.IsPortalAdmin())
                    return true;

                return false;
            }
            public static bool CanDeleteModule()
            {
                if (SM.EM.Security.Permissions.IsAdmin())
                    return true;
                if (SM.EM.Security.Permissions.IsPortalAdmin())
                    return true;
                return false;
            }
            public static bool CanShowHideModule()
            {
                if (SM.EM.Security.Permissions.IsAdmin())
                    return true;
                if (SM.EM.Security.Permissions.IsPortalAdmin())
                    return true;
                return false;
            }



        }
    }



    public class Common
    {







        public enum Type : short
        {
            Allow = 1,
            Deny = 2,
            Not_Set = 0
        }


        public enum Action : short
        {
            ALL = 0,
            New = 1,
            Edit = 2,
            Delete = 3,
            Settings = 4,
            Move = 5,
            Hide = 6
        }



        public static class Scope
        {
            public static readonly Guid ALL = new Guid("2D180075-111B-414C-AB58-F6DC7AB07E2A");
        }

        public static class Obj
        {
            public static readonly Guid MOD_ALL = new Guid("F3BEE25D-DE64-49E3-A40C-7445BA827D12");

            public static readonly Guid MOD_CONTENT = new Guid("4CDF261E-814F-43F2-8786-A3D10016232B");
            public static readonly Guid MOD_ARTICLE = new Guid("0E9EE8BC-ABB1-42D6-989A-78237AC62CD2");
            public static readonly Guid MOD_GALLERY = new Guid("A0CEB56B-6129-40E4-BACA-87E58098B585");
            public static readonly Guid MOD_POLL = new Guid("3D05ECBA-101E-45B4-A76B-1B9F87CED330");

            public static readonly Guid MENU = new Guid("60E1FEA5-9033-49FA-B069-8C7832DA228B");

            public static readonly Guid PRODUCT = new Guid("05520795-CFCA-4C18-9B68-67D3BEDEF970");

            public static readonly Guid PAGE = new Guid("3BF9E7FD-C898-421F-89DC-E81F03596D8A");

            public static readonly Guid ANIMAL = new Guid("CB447DFF-D9FF-49E8-B29D-01B0792BAEF2");



            /* declare after item initialisation  */
            public static Dictionary<string, Guid> Items = new Dictionary<string, Guid>
            {
                {"MOD_ALL", MOD_ALL },
                {"MOD_ARTICLE", MOD_ARTICLE},
                {"MOD_GALLERY", MOD_GALLERY},
                {"MOD_CONTENT", MOD_CONTENT},
                {"MOD_POLL", MOD_POLL},
                {"PRODUCT", PRODUCT},
                {"PAGE", PAGE},
                {"ANIMAL", ANIMAL},
                {"MENU", MENU}
            };



           


        }






    }


}

