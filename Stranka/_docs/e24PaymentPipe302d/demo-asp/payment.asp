<%Dim MyObj
Set MyObj = Server.CreateObject("e24PaymentPipe.e24PaymentPipe.1")
MyObj.WebAddress = "test4.constriv.com"
MyObj.PortStr = "80"
MyObj.Context = "/cg301"
MyObj.ID = "89025555"
MyObj.Password = "test"
MyObj.Action = Request.Form("action")
MyObj.Amt = Request.Form("amount")
MyObj.Currency = "978"
MyObj.Language = "USA"
MyObj.ResponseURL = "http://www.my.server/Receipt.asp"
MyObj.ErrorURL = "http://www.my.server/Error.asp"
MyObj.PaymentId = Request.Form("paymentid")
MyObj.TrackId = Request.Form("trackid")
MyObj.TransId = Request.Form("transid")
MyObj.Udf1 = "AA" 
MyObj.Udf2 = "BB" 
MyObj.Udf3 = "CC" 
MyObj.Udf4 = "DD" 
MyObj.Udf5 = "EE" 

'perform the transaction
Dim TransVal,varTrackid, varResult,varErrorMsg,varRawResponse, varAuth, varRef, varAVR, varDate, varTransid, varudf1, _
 varudf2, varudf3, varudf4, varudf5

TransVal = MyObj.PerformTransaction 'returns 0 for succes -1 for failure

varRawResponse = MyObj.RawResponse
varResult = MyObj.Result
varAuth = MyObj.Auth
varRef = MyObj.Ref
varAVR = MyObj.AVR
varDate = MyObj.Date
varTransid = MyObj.Transid
varTrackid = MyObj.Trackid
varudf1 = MyObj.udf1
varudf2 = MyObj.udf2
varudf3 = MyObj.udf3
varudf4 = MyObj.udf4
varudf5 = MyObj.udf5
varErrorMsg = MyObj.ErrorMsg

'varRedirectURL = varPaymentPage & "?PaymentID=" & varPaymentId
'Response.Redirect varRedirectURL

'Response.Write("<P>Transval  = " &Transval)
'Response.Write("<P>varPaymentId  = " & varPaymentId )
'Response.Write("<P>varPaymentPage  = " & varPaymentPage )
'Response.Write("<P>varErrorMsg  = " & varErrorMsg)%> 

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<HTML>
<HEAD>
<TITLE>Colors of Success</TITLE>
</HEAD>

<BODY>
<!--#INCLUDE FILE = "header.inc"-->
<CENTER>
  <HR>
  <table width="50%" border="1" cellpadding="2">
    <tr> 
      <td width="23%">Transval</td>
      <td width="77%"><%=Transval%></td>
    </tr>
    <tr> 
      <td>Error</td>
      <td><%=varErrorMsg%></td>
    </tr>
    <tr> 
      <td>Result</td>
      <td><%=varResult%></td>
    </tr>
    <tr> 
      <td>Auth</td>
      <td><%=varAuth%></td>
    </tr>
    <tr> 
      <td>Ref</td>
      <td><%=varRef%></td>
    </tr>
    <tr> 
      <td>AVR</td>
      <td><%=varAvr%></td>
    </tr>
    <tr> 
      <td>Date</td>
      <td><%=varDate%></td>
    </tr>
    <tr> 
      <td>TransID</td>
      <td><%=varTransid%></td>
    </tr>
    <tr> 
      <td>TrackID</td>
      <td><%=varTrackid%></td>
    </tr>
    <tr> 
      <td>udf1</td>
      <td><%=varudf1%></td>
    </tr>
    <tr> 
      <td>udf2</td>
      <td><%=varudf2%></td>
    </tr>
    <tr> 
      <td>udf3</td>
      <td><%=varudf3%></td>
    </tr>
    <tr> 
      <td>udf4</td>
      <td><%=varudf4%></td>
    </tr>
    <tr> 
      <td>udf5</td>
      <td><%=varudf5%></td>
    </tr>
	<tr>
      <td>Raw Response</td>
      <td><%=varRawResponse%></td>
    </tr>
  </table>
  


  </CENTER>
<!--#INCLUDE FILE = "footer.inc"-->

</BODY>
</HTML>
