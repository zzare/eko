﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="cwpContactFormSettings.ascx.cs" Inherits="cwpContentSettings" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="FredCK.FCKeditorV2" Namespace="FredCK.FCKeditorV2" TagPrefix="FCKeditorV2" %>




<span class="modPopup_mainTitle" >Naziv kontaktnega obrazca</span>
<div style="float:left;">
    <asp:TextBox ID="tbTitle" class="modPopup_TBXsettings" runat="server"></asp:TextBox>
</div>
<div style="float:left; padding-top:3px;">

    <asp:CheckBox ID="cbShowTitle" CssClass="modPopup_CBXsettings" runat="server" Text = "&nbsp;&nbsp;Prikaži naziv na spletni strani" />
    <cc1:ToggleButtonExtender ID="ToggleEx" runat="server"
        TargetControlID="cbShowTitle" 
        ImageWidth="19" 
        ImageHeight="19"
        CheckedImageAlternateText="Odkljukaj"
        UncheckedImageAlternateText="Obkljukaj"
        UncheckedImageUrl="~/_inc/images/design/checkbox_off.gif" 
        CheckedImageUrl="~/_inc/images/design/checkbox_on.gif"
        DisabledCheckedImageUrl="~/_inc/images/design/checkbox_disabled.gif"
        DisabledUncheckedImageUrl="~/_inc/images/design/checkbox_disabled.gif" />
</div>
<div class="clearing"></div>
<br />

<span class="modPopup_mainTitle" >Vaš e-mail naslov, kamor naj se pošlje e-mail s poizvedbo</span>
<asp:TextBox ValidationGroup="settCF" ID="tbEmail" runat="server" CssClass="modPopup_TBXsettings"></asp:TextBox><br />
<asp:RequiredFieldValidator ValidationGroup="settCF" id="valEmail" ControlToValidate="tbEmail" runat="server" ErrorMessage="Vpiši email" Display="Dynamic"></asp:RequiredFieldValidator>
<asp:RegularExpressionValidator ValidationGroup="settCF" ID="valEmailR" ControlToValidate="tbEmail" runat="server" ErrorMessage="Vpiši veljaven email naslov" Display="Dynamic" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
<br />

<span class="modPopup_mainTitle" >Predmet spročila (viden v emailu, ki se pošlje na vaš naslov)</span>
<asp:TextBox ValidationGroup="settCF" ID="tbSubject" runat="server" CssClass="modPopup_TBXsettings"></asp:TextBox><br />
<asp:RequiredFieldValidator ValidationGroup="settCF" id="RequiredFieldValidator1" ControlToValidate="tbSubject" runat="server" ErrorMessage="Vpiši predmet" Display="Dynamic"></asp:RequiredFieldValidator>
<br />

