﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
namespace SM.UI.CMS
{
    public partial class EmailSend : SM.EM.UI.CMS.BasePage_CMS 
    {

        protected Guid NewsletterID
        {
            get
            {
                if (Request.QueryString["nl"] == null)
                    return Guid.Empty;
                if (!SM.EM.Helpers.IsGUID(Request.QueryString["nl"]))
                    return Guid.Empty;

                return new Guid(Request.QueryString["nl"]);
            }
        }

        protected bool IsAdmin { get {

            return true; // todo: remove
            if (User.Identity.IsAuthenticated)
                if (User.IsInRole("admin"))
                    return true;
            return false;
            }
        }

        protected override void OnInit(EventArgs e)
        {
            CurrentSitemapID = 1181;

            base.OnInit(e);

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            // event handlers
            objEdit.OnChanged += new EventHandler(objUserEdit_OnUserChanged);
            objEdit.OnCancel += new EventHandler(objUserEdit_OnCancel);
            objEdit.OnRefresh += new EventHandler(objEdit_OnRefresh);
            objFilter.OnFilterChanged += new EventHandler(objFilter_OnFilterChanged);
            objList.OnDelete += new SM.UI.Controls._cmsNewsletterList .OnEditEventHandler(objUserList_OnDelete);
            objList.OnEdit += new SM.UI.Controls._cmsNewsletterList.OnEditEventHandler(objUserList_OnEdit);
            

            ApplyFilters();

            if (!IsPostBack)
            {
                // if user is set as QS, show edit user
                if (NewsletterID   != Guid.Empty)
                {
                    LoadData();

                }
 
                    else // normal load
                {
                    HideTabs();
                    cpeQList.Collapsed = false;
                    objList .BindData();
                }

            }
        }

        void objEdit_OnRefresh(object sender, EventArgs e)
        {
            
        }


        protected void LoadData() {

            objEdit.NewsletterID = NewsletterID;
            objEdit.BindData();
            cpeQList.Collapsed = true;
            cpeQList.ClientState = "true";
            objList.BindData();


            ShowTabs();
        }

        void objUserEdit_OnCancel(object sender, EventArgs e)
        {
            HideTabs();

            cpeQList.Collapsed = false;
            cpeQList.ClientState = "false";
            
        }


        protected void ApplyFilters() {
            // list
            objList.SearchText = objFilter.SearchText;
            objList.Status = objFilter.Status;
            objList.NewsletterType = NEWSLETTER.Common.NewsletterType.EMAIL;

            objEdit.NewsletterType = NEWSLETTER.Common.NewsletterType.EMAIL;



            // item
            //objProjectEdit.CategoryID = objProjectFilter.CategoryID;
        
        }
        // filter
        void objFilter_OnFilterChanged(object sender, EventArgs e)
        {
            // filters
            ApplyFilters();

            // refresh question lists
            objList.BindData();

            
        }
        //edit
        void objUserEdit_OnUserChanged(object sender, EventArgs e)
        {
            // refresh user lists
            ApplyFilters();

            // refresh user lists
            objList.BindData();

            // refresh edit
            objEdit.BindData();


        }

        //list
        void objUserList_OnEdit(object sender, SM.BLL.Common.Args.IDguidEventArgs e)
        {
            // show tabs
            ShowTabs();


            // apply filters
            ApplyFilters();


            // load selected question
            objEdit.NewsletterID = e.ID;
            objEdit.BindData();



        }
        void objUserList_OnDelete(object sender, SM.BLL.Common.Args.IDguidEventArgs e)
        {
            ApplyFilters();

            // refresh group lists
            objList.BindData();

        }

        protected void btNew_Click(object o, EventArgs e) {
            objEdit.NewsletterID = Guid.Empty;
            objEdit.BindData();
            ShowTabs();
        }



        protected void ShowTabs()
        {
            //tabCampaign.Visible = true;
            //tabCampaign.ActiveTabIndex = 0;

            objEdit.Visible = true;
            

        }
        protected void HideTabs()
        {
            objEdit.Visible = false;

            //tabCampaign.Visible = false;

        }


    }
}