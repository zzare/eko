﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

namespace SM.UI.Controls
{
    public partial class _cmsNewsletterEdit : BaseControl 
    {
        public event EventHandler OnChanged;
        public event EventHandler OnCancel;
        public event EventHandler OnRefresh;

        public Guid NewsletterID {
            get
            {
                Guid res = Guid.Empty;
                if (ViewState["NewsletterID"] == null) return res;
                if (!SM.EM.Helpers.IsGUID(ViewState["NewsletterID"].ToString())) return res;

                return new Guid(ViewState["NewsletterID"].ToString());
            }
            set{
                ViewState["NewsletterID"] = value;
            }
        }
        public short? NewsletterType { get; set; }


        protected void Page_Load(object sender, EventArgs e)
        {
            objEmailList.OnDelete += new EventHandler(objEmailList_OnDelete);


            if (!IsPostBack)
            {

            }

        }

        void objEmailList_OnDelete(object sender, EventArgs e)
        {
            objEmailList.NewsletterID = NewsletterID;
            objEmailList.BindData();
        }





        // bind data
        public void BindData() {
            this.Visible = true;

            // load ddl
            if (ddlFrom.Items.Count == 1) {
                ddlFrom.Items.Add(new ListItem(SM.EM.Mail.Account.SendGmail.Email, SM.EM.Mail.Account.SendGmail.Email + ";" + SM.EM.Mail.Account.SendGmail.Pass));
                ddlFrom.Items.Add(new ListItem(SM.EM.Mail.Account.Info.Email, SM.EM.Mail.Account.Info.Email + ";" + SM.EM.Mail.Account.Info.Pass));
                
            }


            if (NewsletterID == Guid.Empty)
            {
                LoadDefaultValues();
            }
            else {
                LoadData();
            
            }


        }

        protected void LoadDefaultValues()
        {
            tbDesc.Text = "";
            tbSubject .Text = "";
            tbTitle.Text = "";
            fckBody.Value = "";
            tbFile.Text = "";


            btSend.Visible = false;
            btCopyNew.Visible = false;
            phAdd.Visible = false;

            tbAddEmails.Text = "";

            litStatus.Text  = "V pripravi";

            tbDesc.ReadOnly = false;
            tbSubject.ReadOnly = false;
            tbTitle.ReadOnly = false;
            ddlFrom.Enabled = true;
            btSave.Visible = true;
        }



        // LOAD Question
        protected void LoadData() {

            // load data
            NEWSLETTER r = NEWSLETTER.GetNewsletterByID(NewsletterID);
            tbDesc.Text = r.Desc;
            tbSubject.Text = r.Subject;
            tbTitle.Text = r.Title;
            fckBody.Value = r.Body;
            tbFile.Text = r.AttachmentFileName;

            ddlFrom.SelectedValue  = r.Email+";" +r.Password;

            litStatus.Text = NEWSLETTER.Common.RenderStatus(r.Status);


            // enable/disable editing
            if (r.Status == NEWSLETTER.Common.Status.NOT_SENT) {
                btSend.Visible = true;
                btCopyNew.Visible = false;
                tbDesc.ReadOnly = false;
                tbSubject.ReadOnly = false;
                tbTitle.ReadOnly = false;
                ddlFrom.Enabled = true;
                btSave.Visible = true;
                btAddEmails.Visible = true;

            
            }
            else if (r.Status == NEWSLETTER.Common.Status.SENT) {
                btSend.Visible = false;
                btCopyNew.Visible = true;

                btAddEmails.Visible = false;

                tbDesc.ReadOnly = true;
                tbSubject.ReadOnly = true;
                tbTitle.ReadOnly = true;
                ddlFrom.Enabled = false;

                btSave.Visible = false;
            }

            // email list
            phAdd.Visible = true;
            objEmailList.NewsletterID = NewsletterID;
            objEmailList.BindData();

        }

        public void Save() {
            
            // validation
            Page.Validate("nledit");
            if (!Page.IsValid) return;

            
            eMenikDataContext db = new eMenikDataContext();

            NEWSLETTER r;


            if (NewsletterID == Guid.Empty) {
                r = new NEWSLETTER();
                r.NewsletterID = Guid.NewGuid();
                NewsletterID = r.NewsletterID;
                r.CreatedBy = new Guid( Membership.GetUser().ProviderUserKey.ToString() );
                r.DateCreated = DateTime.Now;

                r.Status = NEWSLETTER.Common.Status.NOT_SENT;
                r.NewsletterType = NewsletterType ?? NEWSLETTER.Common.NewsletterType.PORTAL_NEWSLETTER ;
               
                db.NEWSLETTERs.InsertOnSubmit(r);


            
            }
            else
                r = db.NEWSLETTERs .SingleOrDefault(w => w.NewsletterID  == NewsletterID );

            r.Title = tbTitle.Text;
            r.EmailName = SM.BLL.Common.Emenik.Data.PortalName();
            r.Desc = tbDesc.Text;
            r.Subject = tbSubject.Text;
            r.Body = fckBody.Value;
            r.Email = ddlFrom.SelectedItem.Text ;
            r.Password = ddlFrom.SelectedItem.Value.Substring(ddlFrom.SelectedItem.Value.IndexOf(";") + 1);
            r.AttachmentFileName = tbFile.Text;


            // save
            db.SubmitChanges();

            


            // raise changed event
            OnChanged (this, new EventArgs());

        }

        // SAVE
        protected void btSave_Click(object sender, EventArgs e)
        {
            Save();
        }
        protected void btCancel_Click(object sender, EventArgs e)
        {
            //this.Visible = false;
            OnCancel(this, new EventArgs());
            // remove status text
            ParentPage.mp.StatusText = "";

            
        }

        protected void btSend_Click(object sender, EventArgs e)
        {
            SendEmails();
        }

        protected void btSendTest_Click(object sender, EventArgs e)
        {
            SendTestEmails();
        }


        protected void btCopyNew_Click(object sender, EventArgs e)
        {
            Guid userid = new Guid ( Membership.GetUser().ProviderUserKey.ToString());

            NEWSLETTER n = NEWSLETTER.CopyAsNew(NewsletterID, userid);

            if (n != null)
                Response.Redirect(Page.ResolveUrl(NEWSLETTER.Common.GetNewsletterDetailsCMSHref(n.NewsletterID)));

        }
        protected void btAddEmails_Click(object sender, EventArgs e)
        {
            string list = NEWSLETTER.ImportUsersForNewsletter(NewsletterID, tbAddEmails.Text, (short)SM.BLL.Common.Emenik.Data.PortalID())
                ;

            tbAddEmails.Text = list;

            // refresh list
            objEmailList.NewsletterID = NewsletterID;
            objEmailList.BindData();
             
        }

        protected void SendEmails() { 
        
            // check if emails exist
            NEWSLETTER.SendNewsletter(NewsletterID, "", null);
        
            // set status = SENT
            NEWSLETTER.UpdateStatus(NewsletterID, NEWSLETTER.Common.Status.SENT);

            // raise changed event
            OnChanged(this, new EventArgs());
        }

        protected void SendTestEmails()
        {

//            var usr = CUSTOMER.GetCustomerByID(new Guid("30183739-EAB3-4EAE-970C-6125198B9443"));
            var usr = CUSTOMER.GetCustomerByID(new Guid("B3FC48B0-D4A0-49E6-AC5F-D024CE961C68"));

            
            // check if emails exist
            NEWSLETTER.SendNewsletter(NewsletterID, tbTestEmail.Text, usr.CustomerID );

            // set status = SENT
            //NEWSLETTER.UpdateStatus(NewsletterID, NEWSLETTER.Common.Status.SENT);

            // raise changed event
            OnChanged(this, new EventArgs());
        }



    }
}