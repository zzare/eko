﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

public partial class c_a_Login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // try get id
        if (Request.QueryString["s"] == null) ErrorRedirect();
        if (!SM.EM.Helpers.IsGUID( Request.QueryString["s"] )) ErrorRedirect();
        Guid id = new Guid(Request.QueryString["s"]);

        // get username
        if (Cache[id + "username"] == null) ErrorRedirect();
        string username = Cache[id + "username"].ToString();

        // remember me?
        bool permanent = false;
        if (Cache[id + "permanent"] != null)
            permanent = Convert.ToBoolean(Cache[id + "permanent"]);

        // check ip
        string ip = "";
        if (Cache[id + "ip"] != null)
            ip = Cache[id + "ip"].ToString();
        


        
        // if ip match, login
        if (ip == Request.UserHostAddress)
        {
            // username exist, set auth cookie
            FormsAuthentication.SetAuthCookie(username, permanent);


            // check session
            string session = "";
            if (Cache[id + "session"] != null)
            {
                session = Cache[id + "session"].ToString();
                if (session == "is_admin") {
                    this.Session["is_admin"] = true;                
                }
            }

        }

        // create manual cookie
        //FormsAuthenticationTicket fat = new FormsAuthenticationTicket(1, username, DateTime.Now, DateTime.Now.AddMinutes(60), permanent, "");
        //HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName);
        //cookie.Value = FormsAuthentication.Encrypt(fat);
        //cookie.Expires = fat.Expiration;
        //cookie.Domain = Request.Url.Host;
        //Response.Cookies.Add(cookie);

        // get return url
        string returnUrl = "~/";

        if (Request.QueryString["r"] != null && !String.IsNullOrEmpty(Request.QueryString["r"]))
        {
            returnUrl = Server.UrlDecode(Request.QueryString["r"]);
        }
        // redirect
        Response.Redirect(Page.ResolveUrl(returnUrl));

    }

    protected void ErrorRedirect() {
        Response.Redirect("~/_err/Error.aspx?e=44");    
    }
}
