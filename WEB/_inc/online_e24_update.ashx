﻿<%@ WebHandler Language="C#" Class="online_e24_update" %>

using System;
using System.Web;

public class online_e24_update : IHttpHandler {

    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "text/plain";
        
        
        
        string PaymentID = context.Request.Form["paymentid"];
        string TranID = context.Request.Form["tranid"];
        string Result = context.Request.Form["result"];
        string Auth = context.Request.Form["auth"];
        string PostDate = context.Request.Form["postdate"];
        string TrackID = context.Request.Form["trackid"];
        string UD1 = context.Request.Form["udf1"];
        string UD2 = context.Request.Form["udf2"];
        string UD3 = context.Request.Form["udf3"];
        string UD4 = context.Request.Form["udf4"];
        string UD5 = context.Request.Form["udf5"];
        string Ref = context.Request.Form["ref"] ?? "";
        
        string Error = context.Request.Form["Error"];
        string ErrorText = context.Request.Form["ErrorText"];


        int transID = -1;
        int.TryParse(UD1, out transID);

        int orderRefID = -1;
        int.TryParse(TrackID, out orderRefID);


        // check for IP
        //if (HttpContext.Current.Request.UserHostAddress != "213.143.80.150" || HttpContext.Current.Request.UserHostAddress != "213.143.80.151")
        {
            //return;
        }

        OnlineE24PipeRep olr = new OnlineE24PipeRep();
        // get trans
        E24_TRAN trans = olr.GetTransByID(transID);

        if (trans == null)
        {
            // try by payment id
            trans = olr.GetTransByPaymentID(PaymentID);
            if (trans == null)
            {
                return;
            }
        }
        
        // check if transaction finished
        if (trans.Result == OnlineE24Payment.Common.Status.APPROVED || trans.Result == OnlineE24Payment.Common.Status.CAPTURED || trans.Result == OnlineE24Payment.Common.Status.REVERSED || trans.Result == OnlineE24Payment.Common.Status.VOIDED)
            return;


        //log 
        string postVals = "";
        foreach (string x in context.Request.Form)
        {
            postVals += (x + ": " + context.Request.Form[x] + "<br />");
        }

        if (!olr.Save())
        {

            ERROR_LOG.LogError(new System.Exception("trans update failed"), "online_e24_update: trans update failed; " + postVals);
        }
        
        // get ORDER
        OrderRep orep = new OrderRep();
        SHOPPING_ORDER Order = orep.GetOrderByID(trans.RefID);
        if (Order == null)
        {
            // try by ref
            Order = orep.GetOrderByRefID(orderRefID);
            if (Order == null)
            {
                ERROR_LOG.LogError(new System.Exception("Order == null"), "online_e24_update: Order == null; " + postVals);
                return;
            }
        }

        // error
        if (string.IsNullOrWhiteSpace(trans.TranID) && !string.IsNullOrWhiteSpace(Error)) {            
            trans.RetMsg = ErrorText;
            trans.RetCode = Error ;

            // REDIRECT url
            string errUrl = SM.BLL.Common.ResolveUrlFull(OnlineE24Payment.Common.RenderErrorHref(Order.OrderID));
            context.Response.Write( "REDIRECT=" + errUrl + "&PaymentID=" + PaymentID + "&errorText=" + ErrorText + "&error=" + Error);

            olr.Save();
            return;
        }
        
        
        // all ok

        // update TRANS
        trans.TranID = TranID;
        trans.Result = Result;
        trans.Auth = Auth;
        trans.PostDate = PostDate;
        trans.Udf1 = UD1;
        trans.Udf2 = UD2;
        trans.Udf3 = UD3;
        trans.Ref = Ref;

        trans.DateUpdate = DateTime.Now;






        
        // check if order not proccessed: todo?
        //if (Order.OrderStatus == SHOPPING_ORDER.Common.Status.ONLINE_PAYMENT_RESERVED || Order.OrderStatus == SHOPPING_ORDER.Common.Status.pay)


        // update ORDER
        if (trans.Result == OnlineE24Payment.Common.Status.APPROVED)
        {
            Order.OrderStatus = SHOPPING_ORDER.Common.Status.ONLINE_PAYMENT_RESERVED;
            Order.DateStart = DateTime.Now;
            orep.SubmitOrderOnlinePaymentEnd(Order);
        }
        else if (trans.Result == OnlineE24Payment.Common.Status.CAPTURED)
        {
            Order.DatePayed = DateTime.Now;
            Order.OrderStatus = SHOPPING_ORDER.Common.Status.PAYED;
        }
        else if (trans.Result == OnlineE24Payment.Common.Status.NOT_CAPTURED || trans.Result == OnlineE24Payment.Common.Status.DENIED_BY_RISK)
        {
            Order.OrderStatus = SHOPPING_ORDER.Common.Status.STEP_4;

            // move articels back to basket
            ProductRep prep = new ProductRep();
            prep.UpdateCartItemOrder(Order.UserId, Order.PageId, Order.OrderID, null);
        }
        else if (trans.Result == OnlineE24Payment.Common.Status.VOIDED || trans.Result == OnlineE24Payment.Common.Status.REVERSED)
        {
            Order.OrderStatus = SHOPPING_ORDER.Common.Status.CANCELED;
        }
        else if (trans.Result == OnlineE24Payment.Common.Status.HOST_TIMEOUT)
        {
            ERROR_LOG.LogError(new System.Exception("trans.Result == OnlineE24Payment.Common.Status.HOST_TIMEOUT"), "online_e24_update: trans.Result == OnlineE24Payment.Common.Status.HOST_TIMEOUT; " + postVals);
        }


        // save
        orep.Save();

        // V spodnji URL vnesete točen naslov vašega strežnika, npr.:

        // REDIRECT url
        string statusUrl = SM.BLL.Common.ResolveUrlFull(OnlineE24Payment.Common.RenderStatusHref(Order.OrderID));
        string ReceiptURL = "REDIRECT=" + statusUrl + "&PaymentID=" + PaymentID + "&TransID=" + TranID + "&TrackID=" + TrackID + "&postdate=" + PostDate + "&resultcode=" + Result + "&auth=" + Auth;
        trans.StateResult = ReceiptURL;
        olr.Save();
        
        // write RESPONSE
        
        context.Response.Write(ReceiptURL);
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}