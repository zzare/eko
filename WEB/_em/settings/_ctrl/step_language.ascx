﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="step_language.ascx.cs" Inherits="_em_settings__ctrl_step_language" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Namespace="SM.UI.Controls" TagPrefix="SM" %>

<h1>Izberi jezike, v katerih boš imel svojo spletno stran
    <a href="javascript:void(0)" class="btsettStatusHelp"><img src='<%=Page.ResolveUrl("~/_inc/images/icon/help.png") %>' /></a>
    <div class="settStatusWrapp" >
    <div class="settStatus">
        Izberi jezike, v katerih boš imel svojo spletno stran. <%= SM.BLL.Common.Emenik.Data.PortalName()%> ti ponuja več jezikovnih različic. <i>Vendar pozor, za prevode boš moral poskrbeti sam. </i> Če ne želiš imeti spletne strani v večih jezikih enostavno preskoči ta korak.    </div>
    </div>
</h1>
<br />
<asp:CheckBox ID="cbMultilingual" runat="server"  Text="&nbsp;&nbsp;&nbsp;Omogoči večjezičnost" AutoPostBack="true" oncheckedchanged="cbMultilingual_CheckedChanged"  CssClass="modPopup_CBXsettings" />
<cc1:ToggleButtonExtender ID="ToggleActive" runat="server"
        TargetControlID="cbMultilingual" 
        ImageWidth="19" 
        ImageHeight="19"
        CheckedImageAlternateText="Odkljukaj"
        UncheckedImageAlternateText="Obkljukaj"
        UncheckedImageUrl="~/_inc/images/design/checkbox_off.gif" 
        CheckedImageUrl="~/_inc/images/design/checkbox_on.gif"
        DisabledCheckedImageUrl="~/_inc/images/design/checkbox_disabled.gif"
        DisabledUncheckedImageUrl="~/_inc/images/design/checkbox_off.gif" />
        
<a href="javascript:void(0)" class="btsettStatusHelp"><img src='<%=Page.ResolveUrl("~/_inc/images/icon/help.png") %>' /></a>
<div class="settStatusWrapp" >
    <div class="settStatus">
        Če želiš dodati nov jezik potem obkljukaj "Omogoči večjezičnost". To ti bo odprlo možnost dodajanja novega jezika. Potem enostavno izberi jezik ki ga želiš dodati in klikni na "Dodaj jezik".
    </div>
</div>

<br />
<br />
<h2>Trenutno omogočeni jeziki:</h2>
<SM:smListView ID="lvList" runat="server" DataKeyNames="LCID" InsertItemPosition="None" OnItemCommand="lvList_OnItemCommand" >
    <LayoutTemplate>
        <table class="wizzLang_TB">
                <thead>
                    <tr id="headerSort" runat="server">
                        <th class="wizzLang_TB_th1">Jezik</th>
                        <th >
                            Privzeti jezik
<%--                        </th><th>    --%>
                            <a href="javascript:void(0)" class="btsettStatusHelp"><img src='<%=Page.ResolveUrl("~/_inc/images/icon/help.png") %>' /></a>
                            <div class="settStatusWrapp" >
                                <div class="settStatus">
                                    Privzeti jezik (jezik v katerem se odpre tvoja spletna stran) je slovenščina. To lahko poljubno spremeniš z obkljukanjem možnosti pri jeziku za katerega želiš da bo privzet.
                                </div>
                            </div>
                        </th>
                        <th ></th>
                    </tr>
                </thead>
                <tbody id="itemPlaceholder" runat="server"></tbody>
            </table>
    </LayoutTemplate>
    
    <ItemTemplate>
        <tr  class='<%#  Container.DataItemIndex % 2 == 0 ? " " : " odd " %>'  onmouseover ="this.className='<%#  Container.DataItemIndex % 2 == 0 ? "hovRow" : "hovRowodd" %>'" onmouseout ="this.className = GetCssClass(<%#  Container.DataItemIndex %>)">
            <td>
                <%#  Eval("DESC")%>
            </td>

            <td>
                <SM:CheckBoxID ID="cbActive" runat="server" Checked='<%# Eval("DEFAULT") %>' Enabled="true" IDint='<%# Eval("LCID")%>' AutoPostBack="true" OnCheckedChanged="cbActive_CheckedChanged"   CssClass="wizzFootCbx"/>
                <cc1:ToggleButtonExtender ID="ToggleActive" runat="server"
                        TargetControlID="cbActive" 
                        ImageWidth="19" 
                        ImageHeight="19"
                        CheckedImageAlternateText="Odkljukaj"
                        UncheckedImageAlternateText="Obkljukaj"
                        UncheckedImageUrl="~/_inc/images/design/checkbox_off.gif" 
                        CheckedImageUrl="~/_inc/images/design/checkbox_on.gif"
                        DisabledCheckedImageUrl="~/_inc/images/design/checkbox_disabled.gif"
                        DisabledUncheckedImageUrl="~/_inc/images/design/checkbox_off.gif" />
            </td>
            <%--<td >
                <asp:LinkButton ID="btDefault" Visible='<%# !bool.Parse(Eval("DEFAULT").ToString()) %>' CommandArgument='<%# Eval("LCID")%>' CommandName="default" runat="server">nastavi privzet</asp:LinkButton>
            </td>--%>
            <td>
                <asp:LinkButton ID="lbDelete" CommandArgument='<%# Eval("LCID")%>' OnClientClick="javascript: return confirm('Ali res želite zbrisati to jezikovno verzijo? Vsi morebitni vnosi in nastavitve bodo izgubljeni za ta jezik');" CommandName="del" runat="server">zbriši</asp:LinkButton>
            </td>                
        </tr>        
    
    </ItemTemplate>

</SM:smListView>


<br />

<asp:PlaceHolder ID="phAdd" runat="server">
    <h2>Dodaj nov jezik:
        <a href="javascript:void(0)" class="btsettStatusHelp"><img src='<%=Page.ResolveUrl("~/_inc/images/icon/help.png") %>' /></a>
        <div class="settStatusWrapp" >
            <div class="settStatus">
                Izberi jezik ki ga želiš dodati in klikni na gumb "Dodaj jezik".
            </div>
        </div>
    </h2>
    <asp:DropDownList ID="ddlCulture" runat="server" AppendDataBoundItems="true">
        <asp:ListItem Text=" - Izberi jezik - " Value="" ></asp:ListItem>
    </asp:DropDownList>
    <div class="buttonwrapper">    
        <asp:LinkButton ID="btAdd" runat="server" CssClass="lbutton lbuttonAdd" OnClick="btAdd_OnClick"><span>Dodaj jezik</span></asp:LinkButton>
    </div>
</asp:PlaceHolder>
<br />