﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SM.UI.Controls;
using System.Linq.Dynamic;
using LinqKit;
using SM.EM.UI.Controls;

public partial class _em_content_product__ctrl__pmodProductList : BaseControl_EMENIK 
{
    public ProductRep Prep;
    public CustomPager Pager { get; set; }
    public bool EnableFilters = true;

    public string GetCurrentFilterQS {
        get { return ""; }
    
    }

    //public override string RenderCurrentFilterQSWithout(string fid)
    //{
    //    string ret = "";

    //    if (fid != objFiltManufact.filtQS)
    //        ret += objFiltManufact.GetCurrentFilterQS;

    //    return ret;
    //}
    
    

    //public override string GetOrderColumnByID(int id) {
    //    string ret = "NAME";

    //    if (id == 1)
    //        ret = "NAME";
    //    else if (id == 2)
    //        ret = "PRICE";
    //    else if (id == 3)
    //        ret = "AvgRating";
    //    else if (id == 4)
    //        ret = "DISCOUNT_PERCENT";
    //    return ret;
    //}


    //public override  bool GetDefaultDirectionByID(int id)
    //{
    //    // true = ascending
    //    bool ret = true;

    //    if (id == 1)
    //        ret = true;
    //    else if (id == 2)
    //        ret = true;
    //    else if (id == 3)
    //        ret = false;
    //    else if (id == 4)
    //        ret = false;

    //    return ret;
    //}

    public bool EnableSorting = true;
    public bool EnableNew = true;

    protected IQueryable<vw_Product> _productList;
    public IQueryable<vw_Product> ProductList {
        get {

            if (_productList != null)
                return _productList;

//            ProductRep prep = new ProductRep();
            bool? active = true;
            if (ParentPage.IsModeCMS)
                active = null;

            // tmp
            if (ParentPage.PageModule == PAGE_MODULE.Common.PRODUCT_ADDITIONAL )
                _productList = Prep.GetProductDescAdditionalList(CurrentSitemapID, LANGUAGE.GetCurrentLang(), active, ParentPage.em_user.UserId);
            else
                _productList = Prep.GetProductDescList(CurrentSitemapID, LANGUAGE.GetCurrentLang(), active, ParentPage.em_user.UserId);
            return _productList;
        
        }

        set { _productList = value; }
    }



    protected override void OnInit(EventArgs e)
    {
        //PageSize = 
        Prep = new ProductRep();

        Pager = new CustomPager();
        Pager.OrderColumns = "NAME,PRICE";
        Pager.OrderColumns = "IMPORTANCE,NAME,PRICE";
        Pager.OrderDirections = "desc,asc,desc";
        Pager.PageSize = PRODUCT.Common.PageSizeDefault();
        Pager.SelectedClass = "pagesel";

        base.OnInit(e);

    }


    protected void Page_Load(object sender, EventArgs e)
    {

        mnMain.CurrentWebPartZone = "wpz_1";


        //ApplyFilters();

        // set pager TOTAL COUNT

        Pager.TotalCount = 0;
        if (ProductList != null)
            Pager.TotalCount = ProductList.Count();

        // load list
        objProductList.Data = ProductList.OrderBy(Pager.RenderCurrentSortSQL()).Skip( Pager.PageSize*(Pager.GetCurrentPage() -1)  ).Take(Pager.PageSize );
        objProductList.LoadList();


        //LoadFilters();

        // set relcanonical (if default SORT and DIRECTION and not first page)
        if (Pager.GetCurrentOrder() == 1 && Pager.GetCurrentDirection() == Pager.GetDefaultDirectionByID(1) && Pager.GetCurrentPage() > 1)
        {
            ParentPage.RelCanonical = Pager.RenderSortPageFullUrl(Pager.GetCurrentOrder(), Pager.GetCurrentPage(), false, "");
        }


    }

    protected override void OnPreRender(EventArgs e)
    {
     //   divSorting.Visible = EnableSorting;

        base.OnPreRender(e);
    }

    //protected void LoadFilters()
    //{

    //    if (!EnableFilters)
    //    {
    //        objFiltManufact.Visible = false;
    //        return;
    //    }


    //    // init filters
    //    objFiltManufact.CurrentUrlWithout = Pager.RenderSortPageFullUrl(Pager.GetCurrentOrder(), Pager.GetCurrentPage(), false, objFiltManufact.filtQS);
    //    objFiltManufact.PageId = ParentPage.em_user.UserId;
    //    objFiltManufact.SitemapID = ParentPage.CurrentSitemapID;

    //    // data for filters
    //    List<ProductRep.MANUFACTURER_COUNT> manuList = new List<ProductRep.MANUFACTURER_COUNT>();
    //    if (ParentPage.PageModule == PAGE_MODULE.Common.PRODUCT_ADDITIONAL )
    //        manuList = Prep.GetManufacturersByParentSmapAdditional(ParentPage.CurrentSitemapID, ParentPage.em_user.UserId).ToList();
    //    else
    //        manuList= Prep.GetManufacturersByParentSmap(ParentPage.CurrentSitemapID, ParentPage.em_user.UserId).ToList();

    //    objFiltManufact.ManuList = manuList;

    //    // load filters
    //    objFiltManufact.LoadData();

    //}

    //protected void ApplyFilters() {
    //    if (!EnableFilters)
    //        return;


    //    // manufacturer
    //    if (objFiltManufact.IsFilterSet)
    //        ProductList = ProductList.Where(objFiltManufact.GetFilterPredicate);


    
    //}

}
