﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewProductListRelated.ascx.cs" Inherits="_ctrl_module_product_viewProductListRelated" %>
<%@ Register TagPrefix="SM" TagName="ProductList" Src="~/_ctrl/module/product/_viewProductList.ascx"  %>

<div class="prodlist_relatedW">
    <h2 class="ph_h2"><span class="ph_h2_text"><span class="ph_h2_picL"></span><span class="ph_h2_picR"></span>PODOBNI IZDELKI</span></h2>

    <SM:ProductList ID="objProductList" runat="server" />

</div>
