﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewGalleryShowTMP.ascx.cs" Inherits="SM.EM.UI.Controls._popup_viewGalleryShowTMP" %>
<%@ Register Namespace="SM.UI.Controls" TagPrefix="SM"  %>

<%--<style type="text/css">

.jcarousel-list li,
.jcarousel-item {
    /* We set the width/height explicitly. No width/height causes infinite loops. */
    width: 83px;
    height: 63px;
}
</style>
--%>


<script type="text/javascript">
    var currIndex = <%= CurrIndex %>;
    var data = <%= GetJsonData()  %>;
    var hl = 1;

    function galSelectImage(index) {
        currIndex = index;
        galLoadMainImage();

        galScrollThumbs(currIndex - hl);
    }
    function galLoadImage(step) {
        currIndex += step;
        if (currIndex > data.length - 1)
            currIndex = data.length - 1;
        if (currIndex < 0)
            currIndex = 0;

        galScrollThumbs(currIndex - hl);

        galLoadMainImage();

        return false;
    }
    
    function galLoadMainImage() {
        
        var imgB = $("#imgGalBig")[0];
        if (data[currIndex]) {
            var pr = $("#cProgress");
            var cont = $("#cImgContent");

            pr.height(cont.height());
//            cont.hide().css('display', 'none');
//            pr.show().css('display', 'block');


            imgB.alt = data[currIndex].Title;
            $("#txtTitle").html(data[currIndex].Title);
            $("#txtDesc").html(data[currIndex].Desc);
            imgB.src = data[currIndex].UrlBig;
            if (imgB.complete)
                galOnImageLoaded(imgB);
            else
                $(imgB).load(function() { galOnImageLoaded(imgB); });
            
            
            if (currIndex == 0) $("a.aPrev").hide();
            else $("a.aPrev").show();

            if (currIndex < data.length - 1) $("a.aNext").show();
            else $("a.aNext").hide();

        }
    }    
    
    function galScrollThumbs(pos) {
        var scrlIndex  = pos;
        if (scrlIndex < 0) scrlIndex = 0;
        $("#" + scrlIndex).click();
        //alert(scrlIndex);
        //carousel.scroll(jQuery.jcarousel.intval(scrlIndex ));

        $("#cGalThumb img.galImgThumb").removeClass('selThumb').eq(currIndex).addClass('selThumb');
    }    



    function galOnImageLoaded(img) {
//alert('tesize');
        if (img) {
//            $("#cProgress").hide().css('display','none');
            $("#cImgContent").show().css('display', 'block');

            var hov = $('#galHovNav')[0];
            hov.style.height = img.height + 'px';
            hov.style.top = img.offsetTop + 'px';
            hov.style.left = '26px';
            hov.style.width = '768px';   
            
                     
        //alert('img:yes');
        }
//        alert('resize - galOnImageLoaded');

        $.fancybox.resize();
    }
    
    
    
    function galLoadCarousel() {


        var strt = currIndex - hl;
        if (strt < 0) strt = 0;

        $("#cPopupGal").show();
        $("#sProgress").hide();

        carousel = $('#cGalThumb').jcarousel({ start: strt, visible: 5, scroll: 4, animation: 400, /*auto:15 ,*/
//            buttonNextHTML: null,
//            buttonPrevHTML: null,
            initCallback: galC_initCallback

        });


        $("#cGalThumb img.galImgThumb").removeClass('selThumb').eq(currIndex).addClass('selThumb');

        // reposition hover - first time load
        galOnImageLoaded($("#imgGalBig")[0]);

    }

    function galC_initCallback(carousel) {
        $("#cPrev").click(function() { carousel.prev(); return false; });
        $("#cNext").click(function() { carousel.next(); return false; });


        for (var i = 0; i < data.length; i++) {
            //            btgo[i] = "#" + i;
            var a = $("<a ></a>").attr({ id: i + '' }).hide();
            $("#cGalImgBig").append(a);
            a.bind('click', function() {
                carousel.scroll(jQuery.jcarousel.intval(this.id));
                return false;
            });
        }
        //alert('resize - init');
        $.fancybox.resize();

    } 


</script>

<div style="width:820px;" >




    <div id="cPopupGal" class="cPopupGal">    
        <div class="galData">
            <span id="galTitle"></span>    <br />
            <span id="galDesc"></span>
        </div>
        
        <div class="jCarousel">
            <%--<div class="jCarouselLeft"><a id="cPrev" href="#" class="jCarouselPrevious"></a></div>--%>
            <div class="jCarouselCenter">
                <div  class="jCarouselGal"> 
                    <ul id="cGalThumb" class="jcarousel-skin-tango jcarousel-skin-galpopup" >
                        <%-- tukej se not nafilajo thumbnail slike  --%> 
                        <asp:ListView ID="lvList" runat="server" InsertItemPosition="None"  >
                            <LayoutTemplate>
                                <div id="itemPlaceholder" runat="server"></div>
                                    
                            </LayoutTemplate>
                            
                            <ItemTemplate>
                                <li >
                                    <a id="aC<%# Container.DataItemIndex.ToString()  %>" href="javascript:galSelectImage(<%# Container.DataItemIndex.ToString()  %>);">
                                        <img class="galImgThumb" alt='<%# Eval("Title") %>' src='<%# SM.BLL.Common.ResolveUrl( Eval("UrlThumb").ToString()) %>' />
                                    </a>
                                </li>

                            </ItemTemplate>
                            
                        
                        </asp:ListView>                    
                    </ul>
                </div> 
            </div>
            <%--<div class="jCarouselRight"><a id="cNext" href="#" class="jCarouselNext" ></a></div>--%>
            <div class="clearing"></div>
        </div>
        <div class="clearing"></div>
<%--        <div class="imagePN">
            <div class="imagePNleft">
                <a class="pPrev aPrev" onclick="LoadImage(-1); return false" href="#" ><%= GetGlobalResourceObject("Modules", "PreviousPhoto")%></a>
            </div>
            <div class="imagePNright">
                 <a class="pNext aNext" onclick="LoadImage(1); return false" href="#" ><%= GetGlobalResourceObject("Modules", "NextPhoto")%></a>
            </div>
            <div class="clearing"></div>
        </div>--%>
        
        <div id="cImgContent">
            <%-- big image  --%> 
            <div id="cGalImgBig">
                <div class="cGalImgBigImg" >
                    <img id="imgGalBig" src='<%= RenderMainImageSrc() %>' />
                </div>
            </div>
            <%-- image hover buttons  --%> 
            
            <div id="galHovNav" >
<%--                <a id="hovPrev" class="aPrev" title="&laquo; <%= GetGlobalResourceObject("Modules", "PreviousPhoto") %>" onclick="galLoadImage(-1); return false" href="#"  ><span>&nbsp;</span></a>
                <a id="hovNext" class="aNext" title="<%= GetGlobalResourceObject("Modules", "NextPhoto") %> &raquo;"  onclick="galLoadImage(1); return false" href="#" ><span>&nbsp;</span></a>
--%>            </div>
            
            <div class="txtData">
                <span id="txtTitle"></span><br />
                <span id="txtDesc"></span>
            </div>                            
            
<%--            <div class="btnwrappModalGallery">
                <a class="btClose" href="#" onclick="return false;"></a>
            </div>--%>
        </div>
        
        <div id="cProgress" style="display:none">
            <img id="imgProgress" style="margin-top:200px" src='<%= SM.BLL.Common.ResolveUrl("~/cms/_res/images/icon/ajax-loader.gif") %>' />
            
        </div>
        
    </div>
</div>