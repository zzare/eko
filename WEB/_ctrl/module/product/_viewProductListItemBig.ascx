﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewProductListItemBig.ascx.cs" Inherits="_ctrl_module_product_viewProductListItemBig" %>

<%--
<div class="prod_render <%= RenderClass(Prod) %>">
    <div class="prod_renderM">--%>
    <div class="prod_renderBig">
        <div class="prod_renderBigM">
            <div class="plImg">
                <div class="plImgMask">
                    <a class="plImgMaskA" href='<%= SM.BLL.Common.ResolveUrl(SM.EM.Rewrite.EmenikUserProductDetailsUrl(   Prod.PageId, Prod.PRODUCT_ID, Prod.NAME ))  %>'  title='<%= SM.EM.Helpers.StripHTML(Prod.NAME) %>' >
                        <span class="actionS"><%= GetGlobalResourceObject("Product", "actionOffer")%></span>
                    </a>
                </div>
                <% if (IsModeCMS) {%>
                <a style="position:absolute;" class="divGalImgThumb_btnEdit" href="#" onclick="editCustomCrop('<%= Prod.IMG_ID %>', '<%= Prod.TYPE_ID %>'); return false" ></a>
                <%} %>
                <img class="plImage" src='<%= SM.BLL.Common.ResolveUrl(Prod.IMG_URL)  %>' alt='<%= SM.EM.Helpers.StripHTML(Prod.NAME) %>' />
            </div>


            <div class="plText">
                <a class="plName" href='<%= SM.BLL.Common.ResolveUrl(SM.EM.Rewrite.EmenikUserProductDetailsUrl(   Prod.PageId, Prod.PRODUCT_ID, Prod.NAME ))  %>' >
                <span class="plNameS"><%= Prod.NAME %></span>
                <span class="plPrice"><%= GetGlobalResourceObject("Product", "priceRegular")%>: <%= SM.EM.Helpers.FormatPrice(Prod.PRICE)%></span>
                <span class="plDiscount"><%= GetGlobalResourceObject("Product", "discount")%>: <%= SM.EM.Helpers.FormatDiscount(Prod.DISCOUNT_PERCENT)%></span>
                <span class="plPriceSave"><%= GetGlobalResourceObject("Product", "priceYour")%>: <%= SM.EM.Helpers.FormatPriceFinal(Prod.PRICE, Prod.DISCOUNT_PERCENT)%></span>
                </a>
            </div>
            <div class="clearing"></div>
        </div>
        <div class="prod_renderBigB"></div>

    </div>

<%--    </div>
    <div class="prod_renderB"></div>
</div>--%>
