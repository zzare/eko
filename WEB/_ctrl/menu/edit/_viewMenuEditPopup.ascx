﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewMenuEditPopup.ascx.cs" Inherits="_ctrl_menu_edit_viewMenuEditPopup" %>
<%@ Register TagPrefix="SM" TagName="MenuEdit" Src="~/_ctrl/menu/edit/_viewMenuEdit.ascx"   %>
<%@ Register TagPrefix="SM" TagName="MenuPageModuleEdit" Src="~/_ctrl/menu/edit/_viewMenuPageModuleEdit.ascx"   %>

<script language = "javascript" type="text/javascript">
//    $(function(){
//        $('.pmodEdit h1').click(function(){
//            $('#cEditMenuPmod').slideToggle();
//        
//        
//        });
//        
//    });


    function editMenuAllSave(act) {
    
        var smp = <%= Smap.SMAP_ID %>;
        var _items = new Array();
        $("#cEditMenu").find("#tbEditMenu tbody tr.top").each(function(i, val) {
            var _lan = '' + $(this).find('input:hidden').val();
            var _title = '' + $(this).find('input:text[name="tbTitle"]').val();
            var _act = $(this).find('input:checkbox[name="cbActive"]').attr('checked');
            var _desc =  $(this).find('input:text[name="tbDesc"]').val();
            var _meta =  $(this).next().find('input:text[name="tbMetaDesc"]').val();
            var _kw =  $(this).next().next().find('input:text[name="tbKeywords"]').val();
            var _item = new Object();
            _item.Lang = _lan;
            _item.Title = _title;
            if (_desc )
                _item.Desc = _desc;
                else
                _item.Desc = '';
            if (_meta ) _item.MetaDesc = _meta; else _item.MetaDesc = '';
            if (_kw ) _item.Keywords = _kw; else _item.Keywords = '';
            _item.Active = _act;
            Array.add(_items, _item);
        });
        var pm = $("#cEditMenuPmod :input:radio:checked ").val();
        if (pm == null)
            pm = -1;
        var cc = $("#cEditMenu").find("#tbMenuCustomClass").val();
        if (cc == null)
            cc = '';

        openModalLoading('Shranjujem...');
        WebServiceEMENIK.SaveMenuEditAll(smp, em_g_lang, _items, cc,  pm, act, onMenuSuccessSaveRedirect, onSaveError, act );
    }
    
    function deleteMenu(smp){
        var ok = confirm('Ali res želite zbrisati izbrano podstran? Vsi morebitni vnosi bodo izgubljeni za vse jezike');
        if (!ok)
            return;
        
        openModalLoading('Brišem...');
        WebServiceEMENIK.DeleteMenu(smp, onMenuSuccessSaveRedirect, onSaveError);
    
    }
    
    function onMenuSuccessSaveRedirect(ret, c) {
        if (ret.Code == 0) {
            setModalLoaderErrValidation('', ret.Message);
            return;
        }
        else if (ret.Code != 1) {
            setModalLoaderErr(ret.Message, true);
            return;
        }
        closeModalLoader();
        closeModalLoading();
        
        // soft refresh
        if (c){
          if (c == 'soft_r'){
            $('#pme' + ret.ID).replaceWith(ret.Data).sm_animate_change();
            return;
        }}
        
        
        openModalLoading('Osvežujem...');


        if (ret.Data != null && ret.Data.length > 0)
            document.location.href = ret.Data;
        else {       // todo: porpavi na ajax
            //document.location.reload(false);
            window.location.href = window.location.href;
             
            }
    }    

       
</script>



<div class="modPopup_header modPopup_headerEdit">
    <h4>UREDI PODSTRAN</h4>
    <p>Uredi podstran</p>
</div>
<div class="modPopup_main">

    <h1>Urejanje menija:</h1>
    <SM:MenuEdit ID="objMenuEdit" runat="server" />   
    
    <div class="pmodEdit">
    <SM:MenuPageModuleEdit ID="objMenuPageModuleEdit" runat="server" />   
    </div>
    

</div>   

<div class="modPopup_footer"> 
    <div class="buttonwrapperModal">
        <a class="lbutton lbuttonConfirm" href="#" onclick="editMenuAllSave(''); return false"><span>Shrani</span></a>
        <% if (SM.EM.Security.Permissions.IsAdvancedCms()){ %>
            <a class="lbutton lbuttonConfirm" href="#" onclick="editMenuAllSave('soft_r'); return false"><span>Shrani hitro</span></a>
        <%} %>
        <%--<a class="lbutton lbuttonConfirm" href="#" onclick="addProductSave('edit'); return false"><span>Dodaj nov izdelek in ga UREDI</span></a>--%>
        <a class="lbutton lbuttonEdit" href="#" onclick="closeModalLoader(); return false" ><span>Prekliči</span></a>
        <a class="lbutton lbuttonDelete" href="#" onclick="deleteMenu(<%= Smap.SMAP_ID %>); return false" ><span>ZBRIŠI PODSTRAN</span></a>
    </div>
</div>
