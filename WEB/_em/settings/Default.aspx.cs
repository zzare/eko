﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SM.EM.UI;
using SM.EM.UI.Controls ;

public partial class _em_settings_Default : BasePage_EMENIK 
{
    protected int MaxStep = 7;

    protected bool IsWizardMode { 
        get {
            return !em_user.ACTIVE_USER;
        }
    }


    protected BaseWizardStep CurrentStep
    {
        get
        {
            switch (CurrentStepID)
            {
                case 1:
                    return StepDesign as BaseWizardStep;
                case 2:
                    return StepLanguage as BaseWizardStep;
                case 3:
                    return StepMenu as BaseWizardStep;
                case 4:
                    return StepHeader as BaseWizardStep;
                case 5:
                    return StepModuli as BaseWizardStep;
                case 6:
                    return StepFooter as BaseWizardStep;
                case 7:
                    return StepAdvanced as BaseWizardStep;
                case 8:
                    return StepFinish as BaseWizardStep;
                default:
                    return StepDesign as BaseWizardStep;

            }
        }
    }

    protected int CurrentStepID {
        get {
            int step = -1;

            // get step form viewstate
            if (step == -1 && ViewState["CurrentStepID"] != null){
                Int32.TryParse(ViewState["CurrentStepID"].ToString(), out step);
            }

            // if querystring is set, use it
            if (step == -1 && Request.QueryString["st"] != null){
                Int32.TryParse(Request.QueryString["st"], out step);
            }

            // if in wizard mode and step is not set, go to steps done
            if (step == -1 && !this.em_user.ACTIVE_USER)
                step = em_user.STEPS_DONE + 1;

            // if NOT wizard mode and step is not set, go to DEFAULT step
            if (step == -1 && this.em_user.ACTIVE_USER)
                step = 3;            

            // if step is not allowed, go to max step
            if (step > em_user.STEPS_DONE + 1) step = em_user.STEPS_DONE + 1;
            if (step < 1) step = 1;
            if (step > MaxStep) step = MaxStep+1;


            return step;
        }

        set {
            ViewState["CurrentStepID"] = value;
        }
    }



    protected override void OnPreInit(EventArgs e)
    {
        AutoRedirect = false;
        IsContentPage = false;
        DisablePartialRendering = false;

        base.OnPreInit(e);


        //if not editor then set error
        if (!IsEditMode) this.RequestLogin();

        // disable viewstate on all steps
        //StepDesign.EnableViewState = false;
        //StepMenu.EnableViewState = false;
        //StepFooter.EnableViewState = false;
        //StepHeader.EnableViewState = false;
        //StepModuli.EnableViewState = false;
        //StepLanguage.EnableViewState = false;
        //StepAdvanced.EnableViewState = false;
        //StepFinish.EnableViewState = false;
        //CurrentStep.EnableViewState = true;

    
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        // tmp, todo:odstrani
        //SM.EM.Caching.RemoveEmenikUserMenuCacheKey(em_user.USERNAME);


        // set status menu seelected
        SelectedStatusMenu = SM.BLL.Common.EditModeStatus.Settings;

        
        if (!IsPostBack) {




            // init navigation
            LoadStep();    
        }


    

    }
    protected override void OnPreRender(EventArgs e)
    {
        if (!String.IsNullOrEmpty( CurrentStep.ErrorText ))
            SetError(CurrentStep.ErrorText);
        base.OnPreRender(e);
    }


    protected void NavLoadStep(object sender, EventArgs e) {
        LinkButton bt = (LinkButton)sender;

        CurrentStepID = Int32.Parse(bt.CommandArgument );
        LoadStep();
    }

    protected void LoadStep() {


        // init wizard
        if (IsWizardMode)
        {
            panNormalNav.Visible = CurrentStep.AllwaysShowSaveButton;
            panWizardNav.Visible = true;
            divStatMainWiz.Visible = true;
            divStatMainNorm.Visible = false;
            panNormalNavTop.Visible = CurrentStep.ShowTopNavigation && CurrentStep.AllwaysShowSaveButton;
            panWizardNavTop.Visible = CurrentStep.ShowTopNavigation;

        
        }
        else
        {
            panNormalNav.Visible = !CurrentStep.AllwaysHideSaveButton;
            panWizardNav.Visible = false;
            divStatMainWiz.Visible = false ;
            divStatMainNorm.Visible = true;
            panNormalNavTop.Visible = CurrentStep.ShowTopNavigation && !CurrentStep.AllwaysHideSaveButton;
            panWizardNavTop.Visible = false;
        }

        divStatus.InnerHtml = CurrentStep.Description;

        // hide all steps
        StepDesign.Visible = false;
        StepMenu.Visible = false;
        StepFooter.Visible = false;
        StepHeader.Visible = false;
        StepModuli.Visible = false;
        StepLanguage.Visible = false;
        StepAdvanced.Visible = false;
        StepFinish.Visible = false;
        // disable viewstate on all steps
        //StepDesign.EnableViewState  = false;
        //StepMenu.EnableViewState = false;
        //StepFooter.EnableViewState = false;
        //StepHeader.EnableViewState = false;
        //StepModuli.EnableViewState = false;
        //StepLanguage.EnableViewState = false;
        //StepAdvanced.EnableViewState = false;
        //StepFinish.EnableViewState = false;


        // show current step
        CurrentStep.Visible = true;
        CurrentStep.EnableViewState  = true;
        CurrentStep.LoadData();

        // show/hide buttons
        btPrev.Visible = true;
        btNext.Visible = true;
        btFinish.Visible = false;
        btPrevTop.Visible = true;
        btNextTop.Visible = true;
        

        if (CurrentStepID < 2) {
            btPrev.Visible = false;
            btPrevTop.Visible = false;

        }
        if (CurrentStepID == MaxStep) {
            btNext.Visible = false;
            btFinish.Visible = true;
            btNextTop.Visible = false;
        }
        else if (CurrentStepID > MaxStep) {
            btNext.Visible = false;
            btFinish.Visible = false;
            btPrev .Visible = false;
            btNextTop.Visible = false;
            btPrevTop.Visible = false;
        }


        // init navigation buttons
        //InitNavigationButtons();
    
    
    }
    protected void btPrev_Click(object sender, EventArgs e)
    {
        CurrentStepID--;

        // redirect to new step
        Response.Redirect(Page.ResolveUrl(SM.EM.Rewrite.EmenikUserSettingsUrlByStep(em_user.PAGE_NAME, CurrentStepID)));

        //LoadStep();

    }
    protected void btNext_Click(object sender, EventArgs e)
    {
        // if not valid, then don't proceed
        if (!CurrentStep.Validate()){
            SetError();
            return;
        }

        // save step data
        CurrentStep.SaveData(CurrentStepID);

        CurrentStepID++;
        // redirect to new step
        Response.Redirect( Page.ResolveUrl(SM.EM.Rewrite.EmenikUserSettingsUrlByStep(em_user.PAGE_NAME, CurrentStepID )));

        //LoadStep();

    }
    protected void btFinish_Click(object sender, EventArgs e)
    {

        // if not valid, then don't proceed
        if (!CurrentStep.Validate())
        {
            SetError();
            return;
        }


        // set user active (finished wizard) before save - if redirect on save
        //EM_USER.UpdateActiveUser(em_user.UserId, true);


        // save step data
        CurrentStep.SaveData(CurrentStepID+1);


        // go to finish step
        CurrentStepID++;
        LoadStep();

    }
    protected void btSave_Click(object sender, EventArgs e)
    {
        // if is demo, can't save
        //if (!CanSave)
        //{
        //    SetError("Nimate pravic za spreminjanje podatkov");
        //    return;
        //}


        // if not valid, then don't proceed
        if (!CurrentStep.Validate())
        {
            SetError();
            return;
        }

        // save step data
        CurrentStep.SaveData(CurrentStepID);
    }

    protected void SetError() {
        SetError(CurrentStep.ErrorText);    
    
    }
    public void SetError(string msg)
    {
        if (string.IsNullOrEmpty(msg))
            msg = "Napaka pri shranjevanju. Prosim vnesite pravilne podatke. ";

        divError.Visible = true;
        litError.Text = msg;

    }


    protected string RenderClass( int i) {

        string ret = "";

        // set class
        if (em_user.STEPS_DONE + 1 < i)
            ret = "btNavDis" + i.ToString();
        else if (i == CurrentStepID)
            ret = "btNavOn" + i.ToString();
        else
            ret = "btNav" + i.ToString();
        
        return ret;
    }

    protected string RenderHref(int i)
    {
        string ret = "";

        // disable buttons that are not allowed yet
        if (em_user.STEPS_DONE + 1 < i)
            ret = "";
        else 
            ret = "href=\"" + Page.ResolveUrl( SM.EM.Rewrite.EmenikUserSettingsUrlByStep(em_user.PAGE_NAME,i)) + "\"";

        return ret;
    }
    protected string RenderClick(int i)
    {
        string ret = "";

        // disable buttons that are not allowed yet
        if (em_user.STEPS_DONE + 1 < i)
            ret = "";
        else
            switch (i)
            {
                case 1:
                    ret = "highlight('#main')";
                    break;
                case 2:
                    ret = "highlight('#cLang a')";
                    break;
                case 3:
                    ret = "highlight('#cMenu a')";
                    break;
                case 4:
                    ret = "highlight('#cHeader img, #cLogo img')";
                    break;
                case 5:
                    ret = "highlight('#main')";
                    break;
                case 6:
                    ret = "highlight('#footer')";
                    break;
            }

        return ret;
    }







    //protected void InitNavigationButtons() {
    //    for (int i = 1; i <= MaxStep; i++) {
    //        LinkButton btNav = (LinkButton)divNavCenter.FindControl("btNav" + i.ToString());

    //        if (btNav != null) {
    //            // disable buttons that are not allowed yet
    //            if (em_user.STEPS_DONE  + 1 < i)
    //            //    if (this.Profile.StepsDone + 1 < i)
    //                {
    //                btNav.Enabled = false;
    //                btNav.CssClass = "btNavDis" + i.ToString();
    //            }
    //            else
    //            {
    //                btNav.Enabled = true;
    //                // select current step
    //                if (i == CurrentStepID )
    //                    btNav.CssClass = "btNavOn" + i.ToString();
    //                else
    //                    btNav.CssClass = "btNav" + i.ToString();
    //            }
    //        }
    //    }    
    //}

}
