﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;


namespace SM.UI.Controls
{
    public partial class c_user_ctrl_cntPredracunDetail : BaseControl_EMENIK  
    {
        public Guid OrderID;
        public Guid UserId { get; set; }

        public OrderRep Orep;

        protected SHOPPING_ORDER _order;
        public SHOPPING_ORDER Order
        {
            get
            {
                if (_order != null)
                    return _order;

                if (SM.EM.Security.Permissions.IsCMSeditor ())
                    _order = SHOPPING_ORDER.GetSHOPPING_ORDERByID (OrderID);
                else
                    _order = SHOPPING_ORDER.GetShoppingOrderByIDByUser(OrderID, UserId );
                return _order ;
        }

            set
            {
                _order = value;
            
            }
        }
        public CUSTOMER  customer;
        public ADDRESS address;
        public COMPANY company;
        
        protected void Page_Load(object sender, EventArgs e)
        {


        }



        public void BindData()
        {
            if (Order == null && ParentPage != null)
                ParentPage.ErrorRedirect(74);



            // order summary
            objOrderSummaryFormatted.PageId = Order.PageId.Value  ;
            objOrderSummaryFormatted.Order = Order;
            objOrderSummaryFormatted.IsPredracun = true;
            objOrderSummaryFormatted.LoadData();





            //objOrderDetailStatus.Order = Order;

            // init list
            ProductRep prep = new ProductRep();

            objOrderDetail.Order = Order;
            
            objOrderDetail.CartItems = prep.GetCartItemList(Order.UserId , LANGUAGE.GetCurrentLang(), Order.PageId, Order.OrderID);
            objOrderDetail.LoadData();

            //// init data
            CustomerRep crep = new CustomerRep();
            customer = crep.GetCustomerByID(Order.UserId);


            if (Order.IsCompany)
            {
                company = crep.GetCompanyByID(Order.COMPANY_ID.Value);
                address = crep.GetAddressByID(company.ADDR_ID);
            }
            else
            {
                address = new ADDRESS { STREET = Order.ShippingAddress , POSTAL_CODE = Order.ShippingPostal , CITY = Order.ShippingCity  };
            }

        }




        protected string RenderUserContactName() { 
            string ret = "";

            //if (Order.IsCompany)
            //    ret = company.NAME;
            //else
            //    ret = Order.ShippingFirstName + " " + Order.ShippingLastName;

            if (string.IsNullOrEmpty(Order.OrderCompany))
            {
                ret = Order.ShippingFirstName + " " + Order.ShippingLastName;
            }
            else
            {
                string user  = Order.ShippingFirstName + " " + Order.ShippingLastName;
                ret = Order.OrderCompany;
                if (!string.IsNullOrEmpty(user.Trim()))
                    ret += "(" + user + ")";
            }


            return ret;
        }
        protected string RenderUserContactTAX()
        {
            string ret = "";
            //if (Order.IsCompany)

            if (!string.IsNullOrEmpty(Order.OrderTax))
                ret = "ID št. za DDV: " + Order.OrderTax ;

            return ret;
        }




    }

}