﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SM.UI.Controls;
using SM.EM.UI.Controls;


public partial class _em_content_order__ctrl__ctrlNavigation : BaseControl_EMENIK 
{

    public int CurrentStepID { get; set; }
    public int StepsDone { get; set; }
    public Guid OrderID { get;set; }

    protected void Page_Load(object sender, EventArgs e)
    {


    }





    protected string RenderClass(int i)
    {

        string ret = "";

        // set class
        if (StepsDone   < i)
            ret = "disabled" ;
        else if (i == CurrentStepID)
            ret = "selected";
        else
        {
            ret = "enabled";

        }

        if (i + 1 <= StepsDone)
            ret += " done";

        return ret;
    }

    protected string RenderHref(int i)
    {
        string ret = "";

        // disable buttons that are not allowed yet
        if (StepsDone   < i)
            ret = "";
        else if (StepsDone >= SHOPPING_ORDER.Common.Status.STEP_CONFIRMED)
            ret = "";
        else
            ret = "href=\"" + Page.ResolveUrl(SM.EM.Rewrite.OrderProductUrl(ParentPage.em_user.UserId, OrderID, i)) + "\"";

        return ret;
    }


}
