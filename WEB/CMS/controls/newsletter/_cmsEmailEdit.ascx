﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_cmsEmailEdit.ascx.cs" Inherits="SM.UI.Controls._cmsEmailEdit" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="FredCK.FCKeditorV2" Namespace="FredCK.FCKeditorV2" TagPrefix="FCKeditorV2" %>
<%@ Register TagPrefix="SM" TagName="EmailList" Src="~/CMS/controls/newsletter/_cmsNewsletterEmailList.ascx"   %>

<div class = "qContEdit clearfix">
        
    <asp:Button ValidationGroup="ordredit" ID="btSave" runat="server" Text="Save" onclick="btSave_Click" CssClass="btnOrange " />

    <asp:Button ID="btCancel" runat="server" Text="Cancel" onclick="btCancel_Click" CssClass="btnRed"/>


    <strong>Status: <asp:Literal ID="litStatus" runat="server"></asp:Literal></strong>
    <asp:Button ID="btSend" runat="server" Text="Send" onclick="btSend_Click" CssClass="btnOrange " />
    <asp:Button ID="btCopyNew" runat="server" Text="Copy as new" onclick="btCopyNew_Click" CssClass="btnOrange " />
    
    <br />
    <br />
    Email iz katerega se pošlje
    <asp:DropDownList ID="ddlFrom" runat="server">
        <asp:ListItem Text="- Izberi email -" Value="" ></asp:ListItem>
    </asp:DropDownList> 
    <asp:RequiredFieldValidator ID="rfvFrom" ControlToValidate="ddlFrom" runat="server" ErrorMessage="* Choose email" ValidationGroup="nledit"></asp:RequiredFieldValidator>
    <br />   
    Title:
    <asp:RequiredFieldValidator ID="rfvTitle" ControlToValidate="tbTitle" runat="server" ErrorMessage="* Enter title" ValidationGroup="nledit"></asp:RequiredFieldValidator>
    <br />
    <asp:TextBox ID="tbTitle" Width="50%" runat="server"></asp:TextBox>
    <br />
    Desc:
    <br />
    <asp:TextBox ID="tbDesc" Width="50%" runat="server"></asp:TextBox>
    <br />
    Subject:
    <asp:RequiredFieldValidator ID="rfvSubject" ControlToValidate="tbSubject" runat="server" ErrorMessage="* Enter subject" ValidationGroup="nledit"></asp:RequiredFieldValidator>
    <br />
    <asp:TextBox ID="tbSubject" Width="50%" runat="server"></asp:TextBox>
    <br />
    Body:
    <asp:RequiredFieldValidator ID="rfvBody" ControlToValidate="fckBody" runat="server" ErrorMessage="* Enter email body" ValidationGroup="nledit"></asp:RequiredFieldValidator>
    <br />
    <FCKeditorV2:FCKeditor Width="100%" ID="fckBody"  runat="server" Height="300px" ></FCKeditorV2:FCKeditor>
    <br />
    File :<asp:TextBox ID="tbFile" Width="50%" runat="server"></asp:TextBox>
    
<%--
<asp:Button ID="btTest" runat="server" Text="TEST emails " onclick="btTest_Click" CssClass="btnOrange " />

<asp:Button ID="btTest2" runat="server" Text="TEST emails 2" onclick="btTest2_Click" CssClass="btnOrange " />--%>

<asp:Literal ID="litTest" runat="server"></asp:Literal>
    
    <asp:PlaceHolder ID="phAdd" runat="server">
    
        <br />
        <asp:TextBox ID="tbAddEmails" Width="50%" TextMode = "MultiLine" runat="server"></asp:TextBox>
        
        <asp:Button ID="btAddEmails" runat="server" Text="Add emails to list" onclick="btAddEmails_Click" CssClass="btnOrange " />
        <br />
        <strong>Seznam emailov</strong>
        <SM:EmailList id="objEmailList" runat="server"></SM:EmailList>
    </asp:PlaceHolder>
    
    
</div>

