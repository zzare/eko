﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class cwpArticleSettings : SM.EM.UI.Controls.CwpEdit  
{


    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public override void LoadData()
    {
        // load data
        tbTitle.Text  = cwp.TITLE;
        tbPageSize.Text = cwp.PAGE_SIZE.ToString();
        cbShowTitle.Checked = cwp.SHOW_TITLE;

        // default values
        cbShowAbstract.Checked = true;
        cbShowArchive.Checked = true;
        cbShowImage.Checked = false;

        // load values
        CWP_ARTICLE cwpA = CMS_WEB_PART.GetCwpArticleByID(CwpID);
        if (cwpA != null) {
            cbShowAbstract.Checked = cwpA.LIST_SHOW_ABSTRACT ;
            cbShowArchive.Checked = cwpA.LIST_SHOW_ARCHIVE;
            cbShowImage.Checked = cwpA.SHOW_IMAGE;            
            
        }


    }

    public bool SaveData() {


        if (!ParentPage.CanSave) return false;

        eMenikDataContext db = new eMenikDataContext();
        CMS_WEB_PART c = db.CMS_WEB_PARTs.SingleOrDefault(w => w.CWP_ID == CwpID);

        // save CWP
//        c.TITLE = Server.HtmlEncode(tbTitle.Text);
        c.TITLE = tbTitle.Text;
        int page = 1;
        if (int.TryParse(tbPageSize.Text, out page))
        {
            if (page < 1) page = 1;
            c.PAGE_SIZE = page;
        }
        c.SHOW_TITLE = cbShowTitle.Checked;

        // save CWP_ARTICLE
        CWP_ARTICLE cwpA = db.CWP_ARTICLEs.SingleOrDefault(w => w.CWP_ID == CwpID);
        if (cwpA == null) {
            cwpA = new CWP_ARTICLE { CWP_ID = CwpID  };
            db.CWP_ARTICLEs.InsertOnSubmit(cwpA);
        }
        cwpA.LIST_SHOW_ARCHIVE = cbShowArchive.Checked;
        cwpA.LIST_SHOW_ABSTRACT = cbShowAbstract.Checked;

        cwpA.SHOW_IMAGE = cbShowImage.Checked;
        
        
        db.SubmitChanges();


        return true;
    }

    protected void btSaveEdit_Click(object sender, EventArgs e)
    {
        SaveData();
    }
}
