﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="cwpArticle.ascx.cs" Inherits="SM.EM.UI.Controls.cwpArticleList" %>
<%--<%@ Register TagPrefix="SM" TagName="ArticleList" Src="~/_ctrl/module/article/cmsArticleList.ascx"   %>--%>
<%@ Register TagPrefix="SM" TagName="ArticleList" Src="~/_ctrl/module/article/_viewArticleList.ascx"   %>
<%@ Register TagPrefix="SM" TagName="ArticleList2" Src="~/_ctrl/module/article/_viewArticleList2Paged.ascx"   %>

<div id='<%= cwp.CWP_ID.ToString() %>' class='<%= "cwp" + (ParentPage.IsModeCMS ? " cwpEdit " : " ") + CMS_WEB_PART.Common.RenderHiddenClass(cwp) %>'>
    <div id="divEdit" runat="server" class="cwpHeader cwpMove" visible="false" >
          <%--  <img class="cwpMove" src='<%= Page.ResolveUrl("~/_inc/images/icon/move.png") %>'  alt="move" title="move"  />
            --%>
        <div class="cwpHeaderRight"> &nbsp;</div>

        <div style="float:right; ">
<% if (cwp.DISPLAY_TYPE != CMS_WEB_PART.Common.DisplayType.ART_WITH_SUBCATS)  {%> 
            <a id="btNew" onclick ="editArticleLoad('<%= cwp.CWP_ID.ToString() %>', -1); return false" title="dodaj" href="#" class="btNew"></a>
<%} %>
<% if (PERMISSION.User.Common.CanShowHideModule()){%>            
            <a id="btShow"  title="pokaži" runat="server" class="btShow"></a>
            <a id="btHide" title="skrij" runat="server" class="btHide"></a>
<%} %>
<% if (PERMISSION.User.Common.CanDeleteModule()){%>
            <a id="btDelete" href="#" title="izbriši" class="btDelete" onclick="deleteEL('<%= cwp.CWP_ID.ToString() %>'); return false" ></a>
<%} %>
<% if (PERMISSION.User.Common.CanEditModuleSettings ()){%>
            <a id="btSettings" href="#" onclick="settingsLoad('<%= cwp.CWP_ID.ToString() %>'); return false" title="nastavitve" class="btSettings"  ></a>
<%} %>
        </div>

        <div class="cwpHeaderLeft"> &nbsp;</div>

        <a id="btMove" class="btMove"></a>

        <div class="cwpHeaderTitle">
            Dinamične vsebine
            <a href="javascript:void(0)" class="btsettStatusHelp btcwpHeadHelp"><img src='<%=Page.ResolveUrl("~/_inc/images/icon/help.png") %>' /></a>
            <div class="settStatusWrapp cwpHeadHelpWrapp" >
                <div class="settStatus cwpHeadHelp">
                    Urejevalnik dinamičnih spletnih vsebin
                    <br />
                    Element omogoča urejanje dinamičnih vsebin tvoje spletne predstavitve. Je osnovni modul, s katerim vnašaš dinamične vsebine kot so obvestila, novice, ponudba, storitve, .... Omogoča določitev začetka prikazovanja obvestil in konec prikazovanja, ter vpisovanje obvestil za v naprej.
                    <br /><br />
                    Z elementom za urejanje dinamičnih vsebin lahko urejaš:
                    <br />
                    - novice, obvestila, blog, ponudbo<br />
                    - datum začetka/konca objave <br />
                    - vpisovanje vsebin v naprej, ki se prikaže v želenem časovnem obdobju                
                </div>
            </div>
        </div>
    </div>
    <div  class="clearing"></div>

    <div class='<%=  "cArt " + ( ParentPage.IsModeCMS ? "cwpContainer" : "") %>'>

        <div id="divTitleArticle"  class="cwpH1" runat="server">
            <h1><asp:Literal ID="litTitle" runat="server" ></asp:Literal></h1>
        </div>
        <div class="cArtList">
            <SM:ArticleList ID="objArticleList" runat="server"   />
            <SM:ArticleList2 ID="objArticleList2" runat="server"   />


        </div>
        <div id="divArchive" class="divArchive" runat="server">
            <a class="btFwdwthText" href='<%= RenderArhivHREF() %>' >Vse novice<%--<%= GetGlobalResourceObject("Modules", "AllArticles") %>--%></a>
        </div>   
    </div>
</div>