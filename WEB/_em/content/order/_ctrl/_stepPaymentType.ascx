﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_stepPaymentType.ascx.cs" Inherits="_em_content_order__ctrl__stepPaymentType" %>
<%@ Register Namespace="SM.EM.UI.Controls" TagPrefix="EM"  %>
<%@ Register TagPrefix="SM" TagName="EditModulePublic" Src="~/_ctrl/module/_edit/editModulePublic.ascx"   %>

<SM:EditModulePublic ID="objEditPublic" runat="server" />

<div class="stepHeaderW">
<h1><%= GetGlobalResourceObject("Product", "step1Title1")%></h1>
</div>

<div class="stepShoppingCart" >
    <div class="contentRightW">
        <span class="contentRightHeadT_Text"><%= GetGlobalResourceObject("Product", "step1Title2") %></span>
            <div class="contentRightMI">
            
            
                <asp:RadioButton class="RBTorder" GroupName="rbgPayment" ID="rbDelivery" runat="server" Text="&nbsp;PO POVZETJU" />
                <br />
<% if (LANGUAGE.GetCurrentLang() == "si"){%>
                Plačilo po povzetju je možno z gotovino ali z naslednjimi kreditnimi karticami:  aktiva, mastercard, maestro, visa, visa elektron.
<%}else if (LANGUAGE.GetCurrentLang() == "en"){ %>  
<%}else if (LANGUAGE.GetCurrentLang() == "it"){ %>  
<%} %>
                
                
                <br /><br />

                <asp:RadioButton class="RBTorder" GroupName="rbgPayment" ID="rbPredracun" runat="server" Text="&nbsp;PO PREDRAČUNU" />
                <br />
<% if (LANGUAGE.GetCurrentLang() == "si"){%>
                Na e-mail boste prejeli predračun s podatki o plačilu.
<%}else if (LANGUAGE.GetCurrentLang() == "en"){ %>  
<%}else if (LANGUAGE.GetCurrentLang() == "it"){ %>  
<%} %>
                
                
                <%--<br /><br />
                * Plačilo z kreditno kartico bo možno v kratkem.--%>
                <br /><br />
                
                <asp:PlaceHolder ID="phOnline" runat="server" Visible="true">
                    <asp:RadioButton class="RBTorder" GroupName="rbgPayment" ID="rbOnline" runat="server" Text="&nbsp;ON-LINE PLAČILO S KARTICO" />
                    <br />
<% if (LANGUAGE.GetCurrentLang() == "si"){%>
                    Online plačilo s kartico (Activa, Maestro, MasterCard)
<%}else if (LANGUAGE.GetCurrentLang() == "en"){ %>  
<%}else if (LANGUAGE.GetCurrentLang() == "it"){ %>  
<%} %>                    
                    <br />
                    <div class="stepSCL_paycardslogos_2">
                        <img alt="" src='<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/design/card_activa.jpg") %>' />
                        <img alt="" src='<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/design/card_maestrocard.jpg") %>' />
                        <img alt="" src='<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/design/card_mastercard.jpg") %>' />
                    </div>

                </asp:PlaceHolder>
                
                
                <asp:CustomValidator ID="cvPayment" runat="server" OnServerValidate="ValidatePayment" ValidationGroup="valPayment" ErrorMessage="* Izberite način plačila"></asp:CustomValidator>
                <div class="stepnextbuttonW">
                <a class="Astepnext" runat="server" onserverclick="btNext_Click" ><span><%= GetGlobalResourceObject("Product", "step1Next")%> &raquo;</span></a>
                </div>
                <div class="clearing"></div>
                <div class="stepSCL_paycardslogos stepSCL_paycardslogos_st2">
                    <img alt="" src='<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/design/card_visa_verified.jpg") %>' />
                    <img alt="" src='<%= Page.ResolveUrl("~/_em/templates/_custom/eko/_inc/images/design/card_mastercard_securecode.jpg") %>' />
                </div>

            </div>
    </div>



</div>



