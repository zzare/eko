﻿<%@ Page Language="C#" MasterPageFile="~/CMS/CMS.master" AutoEventWireup="true" CodeFile="ManageGalleryType.aspx.cs" Inherits="SM.UI.CMS.EM.CMS_ManageGalleryType" Title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Namespace="SM.UI.Controls" TagPrefix="SM" %>

<asp:Content ID="Content3" ContentPlaceHolderID="cph" Runat="Server">

    <asp:UpdatePanel ID="upList" runat="server" ChildrenAsTriggers="true">
        <ContentTemplate>

            <asp:LinqDataSource ID="ldsList" runat="server" 
                ContextTypeName="eMenikDataContext" EnableDelete="True" EnableInsert="True" 
                EnableUpdate="True" TableName="GALLERY_TYPEs" 
                OnSelecting="ldsList_OnSelecting" oninserting="ldsList_Inserting" OnInserted="ldsList_Inserted" >
            </asp:LinqDataSource>
            
           <asp:LinqDataSource ID="ldsImageType" runat="server" 
                ContextTypeName="eMenikDataContext" 
                TableName="IMAGE_TYPEs" >
            </asp:LinqDataSource>  
            
            <SM:smListView ID="lvList" runat="server" DataSourceID="ldsList" 
                DataKeyNames="GAL_TYPE" InsertItemPosition="LastItem" 
                onitemcanceling="lvList_ItemCanceling" onitemediting="lvList_ItemEditing" 
                onitemupdated="lvList_ItemUpdated"  OnItemDataBound ="lvList_ItemDataBound"
                 >
                <LayoutTemplate>
                    <table  class="tblData">
                            <thead>
                                <tr id="headerSort" runat="server">
                                    <th width="50px"><asp:LinkButton ID="LinkButton1" CommandName="sort" CommandArgument="GAL_TYPE" runat="server">ID</asp:LinkButton></th>
                                    <th ><asp:LinkButton ID="LinkButton3" CommandName="sort" CommandArgument="GAL_TITLE" runat="server">Title</asp:LinkButton></th>
                                    <th ><asp:LinkButton ID="LinkButton2" CommandName="sort" CommandArgument="GAL_DESC" runat="server">Desc</asp:LinkButton></th>
                                    <th width="50px"><asp:LinkButton ID="LinkButton5" CommandName="sort" CommandArgument="IS_PUBLIC" runat="server">Public</asp:LinkButton></th>
                                    <th  colspan="2">Manage</th>
                                </tr>
                            </thead>
                            <tbody id="itemPlaceholder" runat="server"></tbody>
                            <tfoot>
                                <tr>
                                    <th style="text-align:right" colspan="6">
                                        <asp:DataPager runat="server" ID="DataPager" PageSize="40">
                                            <Fields>
                                                <asp:NextPreviousPagerField  />
                                                <asp:NumericPagerField ButtonCount="5" />
                                            </Fields>
                                        </asp:DataPager>
                                    </th>
                                </tr>
                            </tfoot>
                        </table>
                </LayoutTemplate>
                
                <ItemTemplate>
                    <tr  class='<%#  Container.DataItemIndex % 2 == 0 ? " " : " odd " %>'  onmouseover ="this.className='<%#  Container.DataItemIndex % 2 == 0 ? "hovRow" : "hovRowodd" %>'" onmouseout ="this.className = GetCssClass(<%#  Container.DataItemIndex %>)">
                        <td>
                            <%# Eval("GAL_TYPE")%>
                        </td>
                        <td>
                            <%# Eval("GAL_TITLE")%>
                        </td>
                        <td>
                            <%# Eval("GAL_DESC") == null ? "" : SM.EM.Helpers.CutString(Eval("GAL_DESC").ToString(), 50, " ...")%>
                        </td>
                        <td>
                            <asp:CheckBox ID="cbPublic" runat="server" Enabled="false" Checked='<%# Eval("IS_PUBLIC") %>' />
                        </td>
                        <td >
                            <asp:LinkButton ID="lbEdit" CommandName="Edit" runat="server">edit</asp:LinkButton>
                        </td>
                        <td>
                            <asp:LinkButton ID="lbDelete" OnClientClick="javascript: return confirm('Are you sure you want to delete this item?');" CommandName="Delete" runat="server">delete</asp:LinkButton>
                        </td>                
                    </tr>        
                
                </ItemTemplate>
                
                <InsertItemTemplate>
                    <tr class="insertRow">
                        <td colspan="2">
                            <asp:TextBox MaxLength="256" Width="99%" ID="tbTitle" runat="server" Text='<%# Bind("GAL_TITLE") %>'></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="512" Width="99%" ID="tbDesc" runat="server" Text='<%# Bind("GAL_DESC") %>'></asp:TextBox>
                        </td>
                        <td>
                            <asp:CheckBox ID="cbPublic" runat="server" Text = "Public" />
                        </td>
                        <td colspan="2">
                            <asp:Button ID="btSave" CommandName="Insert" runat="server" Text="Add new" ValidationGroup="insert" ></asp:Button>
                        </td>
                    </tr>
                    <tr class="errorRow">
                        <td colspan="2">
                            <asp:RequiredFieldValidator ValidationGroup="insert" ID="reqState" ControlToValidate="tbTitle" Display="Dynamic"  runat="server" ErrorMessage="* Enter title"></asp:RequiredFieldValidator>
                            <asp:CustomValidator ValidationGroup="insert" ID="cvState" ControlToValidate="tbTitle" Display="Dynamic" runat="server" ErrorMessage="* Title already exists" OnServerValidate="ValidateDuplicates"></asp:CustomValidator>
                        </td>
                        <td colspan="4">
                            <asp:RequiredFieldValidator ValidationGroup="insert" ID="RequiredFieldValidator2" ControlToValidate="tbDesc" Display="Dynamic"  runat="server" ErrorMessage="* Enter desc"></asp:RequiredFieldValidator>
                        </td>
                    </tr>   
                    <tr class="insertRow">
                        <td colspan="6">
                            <asp:CheckBoxList ID="cblTypes"  RepeatLayout="Flow" RepeatDirection="Horizontal"  runat="server" DataSourceID="ldsImageType" DataValueField="TYPE_ID" DataTextField="TYPE_TITLE">
                            </asp:CheckBoxList>
                        </td>
                    </tr>                          
                    
                </InsertItemTemplate>
                
                <EditItemTemplate>
                
                    <tr class="insertRow">
                        <td colspan="2">
                            <asp:TextBox MaxLength="256" Width="99%" ID="tbTitle" runat="server" Text='<%# Bind("GAL_TITLE") %>'></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox MaxLength="512" Width="99%" ID="tbDesc" runat="server" Text='<%# Bind("GAL_DESC") %>'></asp:TextBox>
                        </td>
                        <td>
                            <asp:CheckBox ID="cbPublic" runat="server" Checked='<%# Bind("IS_PUBLIC") %>' Text = "Public" />
                        </td>
                        <td >
                            <asp:Button ID="btSave" CommandName="Update" runat="server" Text="Save" ValidationGroup="edit" ></asp:Button>
                        </td>
                        <td >
                            <asp:Button ID="btCancel" CommandName="Cancel" runat="server" Text="Cancel" ></asp:Button>
                        </td> 
                    </tr>
                    <tr class="errorRow">
                        <td colspan="2">
                            <asp:RequiredFieldValidator ValidationGroup="edit" ID="reqState" ControlToValidate="tbTitle" Display="Dynamic"  runat="server" ErrorMessage="* Enter title"></asp:RequiredFieldValidator>
                            <asp:CustomValidator ValidationGroup="edit" ID="cvState" ControlToValidate="tbTitle" Display="Dynamic" runat="server" ErrorMessage="* Title already exists" OnServerValidate="ValidateDuplicates"></asp:CustomValidator>
                        </td>
                        <td colspan="4">
                            <asp:RequiredFieldValidator ValidationGroup="edit" ID="RequiredFieldValidator2" ControlToValidate="tbDesc" Display="Dynamic"  runat="server" ErrorMessage="* Enter desc"></asp:RequiredFieldValidator>
                        </td>
                    </tr>   
                    <tr class="insertRow">
                        <td colspan="6">
                            <asp:CheckBoxList ID="cblTypes"  RepeatLayout="Flow" RepeatDirection="Horizontal"  runat="server" DataSourceID="ldsImageType" DataValueField="TYPE_ID" DataTextField="TYPE_TITLE">
                            </asp:CheckBoxList>
                        </td>
                    </tr>                  
                

                </EditItemTemplate>
            
            </SM:smListView>

        </ContentTemplate>
    </asp:UpdatePanel>


</asp:Content>


