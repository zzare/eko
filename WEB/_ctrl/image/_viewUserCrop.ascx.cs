﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
namespace SM.UI.Controls
{
    public partial class popupUserCrop : BaseControlView
    {
        public object Data;
        public object[] Parameters;

        public string ImgTargetSrc { get; set; }


        public string ImgTargetClientID { get; set; }
        public string TargetVirtualURLClientID { get; set; }
        public string CroppedImageUrl { get { return hfImageUrlVirt.Value; } set { hfImageUrlVirt.Value = value; } }
        public bool AutoSaveImage { get; set; }
        protected int WidthForce;
        protected int HeightForce;


        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            AutoSaveImage = true;

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            LoadData();
        }



        public void LoadData() {
            if (Data == null )
                return;

            IMAGE_ORIG imgOrig = (IMAGE_ORIG )Data;

            if (imgOrig == null) return;

            IMAGE_TYPE imageType = null;
            if (Parameters != null)
            {
                if (Parameters.Length > 0 && Parameters[0] != null)
                    imageType = (IMAGE_TYPE)(Parameters[0]);
                if (Parameters.Length > 1 && Parameters[1] != null)
                    ImgTargetSrc = Parameters[1].ToString();
            }
            if (imageType == null)
                return;

            // load big image
            imgBig.Src = SM.BLL.Common.ResolveUrl( imgOrig.IMG_URL);

            // load init data
            hfImg.Value = imgOrig.IMG_ID .ToString();
            hfType.Value = imageType.TYPE_ID .ToString();

            // get image type data            
            if (imageType.WIDTH > 0) 
                WidthForce = imageType.WIDTH;
            else
                WidthForce = imageType.W_MAX;
            if (imageType.HEIGHT  > 0) 
                HeightForce = imageType.HEIGHT;
            else
                HeightForce = imageType.H_MAX;


            }


    }
}