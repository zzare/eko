﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewFileEditProduct.ascx.cs" Inherits="_ctrl_module_file_edit_viewFileEditProduct" %>


<script language = "javascript" type="text/javascript">
    

    function closeFileAdd(_prod) {
        closeModalLoader();
        

        if (refrFileList) {
//            if (typeof imgAddPopClbck == "function") {
//                imgAddPopClbck();
//                imgAddPopClbck = null;
//            }
            reloadProductDetail(_prod);

        }
    }


function init_fu_file(v) {
        var _r = $('#' + v).find('input.hfRefID').val();
        var _f = $('#' + v).find('input.hfFType').val();
        var _l = $('#' + v).find('input.hfLangID').val();


        new Ajax_upload($('#' + v + ' .fu_file_upload'), {
            action: '<%= SM.BLL.Common.ResolveUrl("~/_inc/FileUpload.ashx")  %>' ,
            onSubmit: function (file, ext) {

                this.setData({
                    RefID: _r,
                    fType: _f,
                    LangID: _l
                });

                this.disable();
                $('#' + v).find('span.fuLoading').css('visibility', 'visible').show();
            },

            responseType: 'json',
            onComplete: function (file, res) {
                if (res.Code == 0) {
                    setModalLoaderErr(res.Message, false);
                }
                //                else if (!res.Data || res.Data == null) {
                //                    setModalLoaderErr("Napaka pri prenosu datoteke. Prosim, poskusi ponovno.", false);
                //                }
                else {
                    var newItem = res.Data;
                    var cont = $("#" + v).find('div.itemFileList');
                    //var fil = cont.find('span').eq(0);
                    cont.append(newItem);
                    //cont.find('input').eq(0).val(fil.html());

                    var conf = $("#" + v).find("span.upload-conf");
                    conf.show();
                    setTimeout(function () { conf.fadeOut(1000) }, 2000);
                    refrFileList = true;

                }
                $('#' + v).find('span.fuLoading').css('visibility', 'hidden').hide();
                this.enable();
            }
        });
    }

 
</script>


<div id="cEditProductFile" >
    <div class="modPopup_header modPopup_headerEdit">
        <h4>UREDI DATOTEKE</h4>
        <p>Uredi datoteke izdelka za različne jezikovne različice</p>
    </div>
    <div class="modPopup_main">
    
        <div id="prodFileTabs">
            <ul>
                <asp:ListView ID="lvListHeader" runat="server"  InsertItemPosition="None" >
                    <LayoutTemplate>
                        <asp:PlaceHolder id="itemPlaceholder" runat="server"></asp:PlaceHolder>
                    </LayoutTemplate>
                    
                    <ItemTemplate>
                         <li><a href="#tabs-<%# Container.DataItemIndex %>"><%# Eval("DESC")  %></a></li>
                    </ItemTemplate>
                </asp:ListView>	 	        
	        </ul>  
    	    
    	    
                <asp:ListView ID="lvListDetail" runat="server"  InsertItemPosition="None" >
                    <LayoutTemplate>
                        <asp:PlaceHolder id="itemPlaceholder" runat="server"></asp:PlaceHolder>
                    </LayoutTemplate>
                    
                    <ItemTemplate>
                        <div id="tabs-<%# Container.DataItemIndex %>" class="item" >
                            <input class="hfLang"  type="hidden" value= '<%#  Eval("LANG_ID")%>' />
                               
                            <%# RenderItem(Container.DataItem) %>
                        </div>
                    </ItemTemplate>
                </asp:ListView>	    


        </div>



    </div>   

    <div class="modPopup_footer"> 
        <div class="buttonwrapperModal">
            <a class="lbutton lbuttonEdit" href="#" onclick="closeFileAdd('<%= ProductID %>'); return false"><span>Zapri</span></a>
        </div>
    </div>
</div>