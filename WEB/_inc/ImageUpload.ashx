﻿<%@ WebHandler Language="C#" Class="ImageUpload" %>

using System;
using System.Web;
using System.IO;
using System.Text;
using System.Web.Script.Serialization;
using System.Text.RegularExpressions;
using System.Linq;
using System.Xml.Linq;





public class ImageUpload : IHttpHandler
{

    public class FileUploadResponse {
        public int Result { get; set; }
        public string Response { get; set; }
        public string Data { get; set; }
    
    }

    public int MAX_FILE_SIZE = 10485760; // 10 MB

    public int GalleryID = -1;

    public bool ForceDummy { get {

        if (Context.Request.Params["force"] != null && Context.Request.Params["force"] == "1")
            return true;
        return false;
    
    } }
    public HttpContext Context;

    public void ProcessRequest(HttpContext context)
    {
        if (context.Request.Files.Count == 0)
            return;

        Context = context;
            
        // get file
        string strFileName = Path.GetFileName(context.Request.Files[0].FileName);
        string strExtension = Path.GetExtension(context.Request.Files[0].FileName).ToLower();


        SM.EM.Ajax.ReturnData ret = ProcessImage(context, strFileName, strExtension);

        // set response        
        JavaScriptSerializer serializer = new JavaScriptSerializer();

        //context.Response.ContentType = "application/json";
        context.Response.ContentType = "text/html";
        context.Response.Charset = "utf-8";
        context.Response.Write(serializer.Serialize(ret));

        
    }

    protected SM.EM.Ajax.ReturnData ProcessImage(HttpContext context, string fileName, string fileExt)
    {

        SM.EM.Ajax.ReturnData ret = new SM.EM.Ajax.ReturnData { Code = SM.EM.Ajax.ReturnCode.Failure, Message = " no go! " };


        // check user
        if (!context.User.Identity.IsAuthenticated)
            return ret ;

        if (context.Request.QueryString["c"] == null)
            return ret;

        if (!SM.EM.Helpers.IsGUID( context.Request.QueryString["c"]))
            return ret;
                
        Guid cwpID = new Guid( context.Request.QueryString["c"]) ;


        
        // active
        bool active = true;
        if (context.Request.Params["active"] != null)
        {
            if (context.Request.Params["active"] == "false")
                active = false;
        }
        // author
        string author = "";
        if (context.Request.Params["author"] != null)
        {
            author = context.Request.Params["author"];
        }
        
        // get page
        //EM_USER usr = EM_USER.GetUserByUserName(context.User.Identity.Name);
        EM_USER usr = EM_USER.Current.GetCurrentPage();
        if (usr == null)
            return ret;

        // check permissions
        if (!ForceDummy &&  !PERMISSION.User.HasPermissionTypeMODULE(usr.UserId, null, null, PERMISSION.Common.Obj.MOD_GALLERY , PERMISSION.Common.Action.New, PERMISSION.Common.Type.Allow))
        {
            ret.Message = "Nimaš pravic. Kontaktiraj administratorja.";
            return ret;
        }

        // get CWP
        CMS_WEB_PART cwp = CMS_WEB_PART.GetWebpartByID(cwpID);
        if (cwp == null)
        {
            if (context.Request.Params["force"] != null && context.Request.Params["force"] == "1")
            {
                // create dummy gallery
                // insert CWP for gallery
                cwp = CMS_WEB_PART.CreateNewCwpGallery(SM.BLL.Common.GalleryType.DefaultArticle, cwpID, null, MODULE.Common.GalleryModID, ARTICLE.Common.MAIN_GALLERY_ZONE, usr.UserId, "Galerija", "", true, false, -1, false, false, 1, SM.BLL.Common.ImageType.ArticleDetailsThumbDefault, SM.BLL.Common.ImageType.EmenikBigDefault);
        
            }
            else
            {
                throw new System.Exception("Napaka pri galeriji");
            }
        }

        if (cwp.ID_INT == null)
            throw new System.Exception("Galerija ne obstaja");

        GalleryID = cwp.ID_INT.Value ;
        
        
        // upload folder

        string folder = IMAGE.Common.GetUserImageFolder(usr.UserId );




        //try
        //{
            // validate file size
            if (context.Request.Files[0].ContentLength > MAX_FILE_SIZE)
            {
                ret.Message = "Datoteka je prevelika. Maksimalna velikost datoteke je " + (MAX_FILE_SIZE / (1024 * 1024)).ToString() + "MB";
                return ret;
            }

            // validate file type
            if (!Regex.IsMatch(fileExt, @"^.*\.(png|PNG|jpg|JPG|gif|GIF|zip|ZIP)$"))
            {
                ret.Message = "Datoteka ni veljavnega formata (.png, .jpg, .gif) ";
                return ret;
                
            }

            string retData = "";                        

            // if zip, extract it first
            if (fileExt == ".zip")
            {
                string zipFile = context. Server.MapPath(SM.BLL.Common.Emenik.DEFAULT_ZIP_FOLDER + (Guid.NewGuid()).ToString() + ".zip");
                // create folder of not exist
                if (!System.IO.Directory.Exists(context.Server.MapPath(SM.BLL.Common.Emenik.DEFAULT_ZIP_FOLDER)))
                    System.IO.Directory.CreateDirectory(context.Server.MapPath(SM.BLL.Common.Emenik.DEFAULT_ZIP_FOLDER));

                // save zip to zip folder
                context.Request.Files[0].SaveAs(zipFile);

                // extract zip and process files in stream
                ICSharpCode.SharpZipLib.Zip.ZipFile zip = new ICSharpCode.SharpZipLib.Zip.ZipFile(zipFile);
                foreach (ICSharpCode.SharpZipLib.Zip.ZipEntry ze in zip)
                {
                    // process zipped file
                    string target = System.IO.Path.Combine(context.Server.MapPath(IMAGE.Common.GetUserImageFolder()), ze.Name);
                    if (ze.IsFile)
                    {

                        // get file type
                        string fType = ze.Name.Substring(ze.Name.LastIndexOf("."));


                        // check valid format
                        if (IMAGE.Common.IsFormatValid(fType))
                        {
                            // format is valid, add image
                            System.IO.Stream inputStream = zip.GetInputStream(ze);
                            retData += AddImage(inputStream, fType, active, author, folder, usr);
                        }
                    }
                }
                zip.Close();

                // delete zip file
                System.IO.File.Delete(zipFile);


            }
            else // one image is uploaded
            {
                // process image
                retData += AddImage(context.Request.Files[0].InputStream, fileExt, active, author, folder, usr);
            }

        //}
        //catch (Exception ex)
        //{
        //    throw;
        //}
        
 
        
        ret.Message = "Datoteka je dodana";
        ret.Code = SM.EM.Ajax.ReturnCode.OK;
        ret.Data = retData ;

        return ret;
    }


    protected string AddImage(System.IO.Stream stream, string format, bool active, string author, string folder, EM_USER page)
    {
        string ret = "";
        
        // if image type not set, get image type from gallery
        int imageTypeID = GALLERY.GetImageTypeFromGalleryDefault(GalleryID);



        // save and manipulate image
        IMAGE_ORIG imgOrig = new IMAGE_ORIG { IMG_ACTIVE = active, IMG_AUTHOR = author, USERNAME = page.USERNAME  };
        IMAGE img = IMAGE.Func.AddNewImage(System.Drawing.Image.FromStream(stream), IMAGE_TYPE.GetImageTypeByID(imageTypeID), imgOrig, false, format, folder );

        // for each image type in gallery add new image types
        if (GalleryID > 0)
        {
            eMenikDataContext db = new eMenikDataContext();
            var types = from gt in db.GALLERY_IMAGE_TYPEs where gt.GAL_ID == GalleryID select gt.TYPE_ID;

            foreach (int imgType in types)
            {
                IMAGE imgt = IMAGE.Func.AutoCreateImageForType(img.IMG_ID, imgType, true);
                if (imgt.TYPE_ID == SM.BLL.Common.ImageType.EmenikGalleryThumbDefault)
                    ret = imgt.IMG_URL;
                
            }

            // add image to gallery
            GALLERY.AddImageToGallery(GalleryID, img.IMG_ID);

        }

        if (!string.IsNullOrEmpty(ret))
            ret = FILE.Common.Render.ImageHtml( ret, "");
        return ret;
    }    
    
    

 
    public bool IsReusable {
        get {
            return false;
        }
    }

}