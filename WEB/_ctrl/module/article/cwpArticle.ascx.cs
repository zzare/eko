﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SM.EM.BLL;

namespace SM.EM.UI.Controls
{
    public partial class cwpArticleList : BaseControl_CmsWebPart  
    {

        public DateTime FiltDate
        {
            get
            {
                DateTime ret = DateTime.MinValue;
                if (Request.QueryString["dt"] != null)
                    DateTime.TryParseExact(HttpUtility.UrlDecode(Request.QueryString["dt"]), "dd.MM.yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out ret);

                if (Request.QueryString["n"] != null)
                    //return DateTime.Today.AddDays(-10);
                return DateTime.Today.AddDays(SM.BLL.Custom.Settings.NovostiDaysMargin); 


                int year = DateTime.Now.Year;
                // get from year if set
                if (ret == DateTime.MinValue && Request.QueryString["y"] != null)
                {
                    if (int.TryParse(Request.QueryString["y"], out year))
                    {
                        if (year < (DateTime.Now.Year - 15) || year > (DateTime.Now.Year + 15))
                            year = DateTime.Now.Year;
                    }
                }


                // get from month if set
                if (ret == DateTime.MinValue && Request.QueryString["m"] != null)
                {
                    int m = -1;
                    if (int.TryParse(Request.QueryString["m"], out m))
                    {
                        if (m > 0 && m <= 12)
                            ret = new DateTime(year, m, 1);
                    }
                }



                return ret;
            }
        }

        public DateTime FiltDateEnd
        {
            get
            {
                DateTime ret = DateTime.MinValue;
                if (Request.QueryString["dte"] != null)
                    DateTime.TryParseExact(HttpUtility.UrlDecode(Request.QueryString["dte"]), "dd.MM.yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out ret);

                // get from month if set
                if (ret == DateTime.MinValue && Request.QueryString["m"] != null)
                {
                    int m = -1;
                    if (int.TryParse(Request.QueryString["m"], out m))
                    {
                        if (m > 0 && m <= 12)
                            ret = new DateTime(DateTime.Now.Year, m, 1).AddMonths(1).AddDays(-1);
                    }
                }



                return ret;
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

        }
        protected void Page_Load(object sender, EventArgs e)
        {

                // edit mode
            if (ParentPage.IsModeCMS)
            {
//                objArticleList.OnEditArticleClick += new cmsArticleList.OnEditEventHandler(objArticleList_OnEditArticleClick);

            }
        }

        public override void LoadData()
        {
            InitEditMode();
            LoadArticles();
        }

        protected void InitEditMode()
        {


            // edit mode
            if (ParentPage.IsModeCMS)
            {
                divEdit.Visible = true;

                // button actions
                //btEdit.HRef = "javascript:onEdit('" + mpeEdit.BehaviorID + "');";
                btShow.HRef = "javascript:onToggleCwpActive('" + btShow.ClientID + "','" + btHide.ClientID + "', '" + cwp.CWP_ID.ToString() + "' ,true);";
                btHide.HRef = "javascript:onToggleCwpActive('" + btShow.ClientID + "','" + btHide.ClientID + "','" + cwp.CWP_ID.ToString() + "', false);";

                // initial hiding of buttons
                if (cwp.ACTIVE)
                {
                    btShow.Attributes["style"] = "display:none";
                    btHide.Attributes["style"] = "display:";
                    //divEdit.Attributes["class"] = divEdit.Attributes["class"].Replace("cwpHidden", "");
                }
                else
                {
                    btShow.Attributes["style"] = "display:";
                    btHide.Attributes["style"] = "display:none";
                    //divEdit.Attributes["class"] += " cwpHidden";
                }

            }
            else
            {
                divEdit.Visible = false;
            }

            // title
            litTitle.Text = cwp.TITLE;
            divTitleArticle .Visible = cwp.SHOW_TITLE;


        }

        protected void LoadArticles() {

            
            

            // init cwp_article
            CWP_ARTICLE cwpA = CMS_WEB_PART.GetCwpArticleByID(cwp.CWP_ID);
            if (cwpA != null) {
                divArchive.Visible = cwpA.LIST_SHOW_ARCHIVE;

            }

            // article list
            ArticleRep arep = new ArticleRep();

            // bind to control            
             if (cwp.DISPLAY_TYPE == CMS_WEB_PART.Common.DisplayType.ART_2) {
                objArticleList2.CurrentSitemapID = ParentPage.CurrentSitemapID;
                objArticleList2.IsModeCMS = ParentPage.IsModeCMS;
                objArticleList2.em_user = ParentPage.em_user;
                objArticleList2.CwpID = cwp.CWP_ID;
                objArticleList2.PageSize = cwp.PAGE_SIZE;
                objArticleList2.EnablePager = cwp.PAGE_ENABLE;

                objArticleList2.ShowDesc = cwpA.LIST_SHOW_ABSTRACT;
                objArticleList2.ShowImage = cwpA.SHOW_IMAGE;
                objArticleList2.ImgTypeID = cwpA.IMG_TYPE_THUMB;


                objArticleList2.CwpArticle = cwpA;
                objArticleList2.Data = arep.GetArticlesByCwp(cwp.CWP_ID, ParentPage.IsModeCMS);
                objArticleList2.BindData();            
            }
             else if (cwp.DISPLAY_TYPE == CMS_WEB_PART.Common.DisplayType.ART_WITH_SUBCATS)
             {
                 // init articles
                 objArticleList.CurrentSitemapID = ParentPage.CurrentSitemapID;
                 objArticleList.IsModeCMS = ParentPage.IsModeCMS;
                 objArticleList.em_user = ParentPage.em_user;
                 objArticleList.CwpID = cwp.CWP_ID;
                 objArticleList.PageSize = cwp.PAGE_SIZE;
                 objArticleList.EnablePager = cwp.PAGE_ENABLE;

                 objArticleList.ShowDate = true;
                 objArticleList.ShowDesc = cwpA.LIST_SHOW_ABSTRACT;
                 objArticleList.ShowCategory = true;
                 objArticleList.ShowImage = cwpA.SHOW_IMAGE;
                 objArticleList.ImgTypeID = cwpA.IMG_TYPE_THUMB;
                 objArticleList.ShowAddNewBlog = false;


                 var baseL = arep.db.ARTICLEs.Where(w => w.ACTIVE == true  );
                 if (!ParentPage.IsModeCMS )
                     baseL = baseL.Where(w => w.APPROVED == true);
                 
                 if (FiltDate != DateTime.MinValue )
                 {
                     DateTime dt = FiltDate.Date;
                     baseL = baseL.Where(w => w.RELEASE_DATE != null && w.RELEASE_DATE.Value >= dt );
                 } // from / to
                 if (FiltDateEnd != DateTime.MinValue)
                 {
                     DateTime dte = FiltDateEnd;
                     baseL = baseL.Where(w => w.RELEASE_DATE.Value < dte.AddDays(1));
                 }
                 baseL = baseL.OrderByDescending(o=>o.RELEASE_DATE ).ThenByDescending(o=>o.DATE_MODIFY ).ThenByDescending(o=>o.DATE_ADDED );
                 
                 objArticleList.Data = arep.GetArticleListHierarchyWithImage(ParentPage.CurrentSitemapID, LANGUAGE.GetCurrentLang(), true, SM.BLL.Custom.MainUserID, baseL,  cwpA.IMG_TYPE_THUMB ?? SM.BLL.Common.ImageType.ArticleThumbDefault   );//.Skip(0).Take(cwp.PAGE_SIZE);
                 objArticleList.BindData();
             }
             else //if (cwp.DISPLAY_TYPE == CMS_WEB_PART.Common.DisplayType.ART_NORMAL)
             { // blog
                 // init articles
                 objArticleList.CurrentSitemapID = ParentPage.CurrentSitemapID;
                 objArticleList.IsModeCMS = ParentPage.IsModeCMS;
                 objArticleList.em_user = ParentPage.em_user;
                 objArticleList.CwpID = cwp.CWP_ID;
                 objArticleList.PageSize = cwp.PAGE_SIZE;
                 objArticleList.EnablePager  = cwp.PAGE_ENABLE;

                 objArticleList.ShowDate = true;
                 objArticleList.ShowCategory = true;
                 objArticleList.ShowDesc = cwpA.LIST_SHOW_ABSTRACT;
                 objArticleList.ShowImage = cwpA.SHOW_IMAGE;
                 objArticleList.ImgTypeID = cwpA.IMG_TYPE_THUMB;
                 objArticleList.ShowAddNewBlog = true;


                 // get normal by CWP
                 //var artList = arep.GetArticlesByCwp(cwp.CWP_ID, ParentPage.IsModeCMS);//.Skip(0).Take(cwp.PAGE_SIZE);
                 //// add additional articles
                 //var artAddList = arep.GetArticlesByAdditionalSmap (ParentPage.IsModeCMS, ParentPage.CurrentSitemapID, cwpA.IMG_TYPE_THUMB );//.Skip(0).Take(cwp.PAGE_SIZE);
                 //artList = artList .Union(artAddList);

                 var list = arep.GetArticlesByCwpAndAdditionalSmap(cwp.CWP_ID, ParentPage.IsModeCMS, ParentPage.CurrentSitemapID, cwpA.IMG_TYPE_THUMB, LANGUAGE.GetCurrentLang ());


//                 objArticleList.Data = arep.GetArticlesByCwp(cwp.CWP_ID, ParentPage.IsModeCMS);//.Skip(0).Take(cwp.PAGE_SIZE);
                 objArticleList.Data = list;
                 objArticleList.BindData();
             }

        
        }

//        void objArticleList_OnEditArticleClick(object sender, SM.BLL.Common.ClickIDEventArgs e)
//        {
//            // find edit
////            CwpEdit edit = SM.HelperClasses.FindControl2<CwpEdit>(Page, "objEditArticle");
//            CwpEdit edit = ParentPage.EditArticle;
//            if (edit == null)
//                return;
//            edit.IDref = e.ID;
//            edit.CwpID = cwp.CWP_ID;
//            edit.cwp = cwp;
//            edit.LoadData();
//        }
        //public void OnNewArticleClick(object sender, EventArgs e) {
        //    // find edit
        //    //CwpEdit edit = SM.HelperClasses.FindControl2<UserControl>(Page, "objEditArticle") as CwpEdit;
        //    CwpEdit edit = ParentPage.EditArticle ;
        //    if (edit == null)
        //        return;
        //    edit.ID = -1;
        //    edit.CwpID = cwp.CWP_ID;
        //    edit.cwp = cwp;
        //    edit.LoadData();

        //}

        //protected void btSettings_Click(object sender, EventArgs e)
        //{
        //    CwpEdit edit = ParentPage.ArticleSettings;
        //    if (edit == null) return;

        //    edit.cwp = cwp;
        //    edit.CwpID = cwp.CWP_ID;
        //    edit.LoadData();
        //    edit.Zone = cwp.ZONE_ID;
        //}

        public string RenderArhivHREF() {

            return Page.ResolveUrl( SM.EM.Rewrite.EmenikUserArticleArchiveUrl(ParentPage.em_user.PAGE_NAME, ParentPage.CurrentSitemapID, cwp.CWP_ID));
        }
        
    }
}