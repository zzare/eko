﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SM.UI.Controls;
using SM.EM.UI.Controls;


public partial class _em_content_order__ctrl__stepShipping : BaseWizardStepOrder 
{
    public event EventHandler OnSaveOK;

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        ShortDesc = "Naslov za dostavo";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    


    }


    public override void LoadData()
    {
        if (Order == null)
            return;
        
        // load order
        if (Order.ShippingType == SHOPPING_ORDER.Common.ShippingType.SHIPPING)
        {
            rbShipping.Checked = true;
            tbName.Text = Order.ShippingFirstName;
            tbAddress.Text = Order.ShippingAddress;
            tbCity.Text = Order.ShippingCity ;
            tbPhone.Text = Order.ShippingPhone ;


        }
        else if (Order.ShippingType == SHOPPING_ORDER.Common.ShippingType.PERSONAL)
            rbPersonal.Checked = true;

        cbCompany.Checked = Order.IsCompany;
        if (Order.COMPANY_ID != null) {
            tbCTititle.Text = Order.COMPANY.NAME;
            tbCTax.Text = Order.COMPANY.TAX_NUMBER;
            tbCAddress.Text = Order.COMPANY.ADDRESS.STREET;
            tbCCity.Text = Order.COMPANY.ADDRESS.CITY;
        
        }

        var shippList = Orep.GetShippingAddressesByUser(Order.UserId);
        lvListCopy.DataSource = shippList;
        lvListCopy.DataBind();
        if (lvListCopy.Items.Count <= 0)
            phCopyAddress.Visible = false;

        var compList = Orep.GetCompanyDetailsByUser(Order.UserId);
        lvListCAddr.DataSource = compList;
        lvListCAddr.DataBind();
        if (lvListCAddr.Items.Count <= 0)
            phCopyC.Visible = false;


    }

    protected void ValidateShipping(object o, ServerValidateEventArgs args)
    {
        args.IsValid = true;

        // check for duplicate page name
        if (!rbShipping.Checked && !rbPersonal.Checked)
        {
            args.IsValid = false;
            CustomValidator cv = (CustomValidator)o;
            cv.ErrorMessage = "* Izberite način dostave";
        }
    }


    protected void btNext_Click(object o, EventArgs e)
    {
        bool isValid = true;
        Page.Validate("valShipping");
        if (!Page.IsValid)
            isValid = false;
        // validate company
        if (cbCompany.Checked)
        {
            Page.Validate("vgCompany");
            if (!Page.IsValid)
                isValid = false;

        }
        // validate shiupping address
        if (rbShipping.Checked)
        {
            Page.Validate("vgShipping");
            if (!Page.IsValid)
                isValid = false;
        }

        // there was an error
        if (!isValid)
            return;


        // error
        if (Order == null)
            return;


        short shippingType = SHOPPING_ORDER.Common.ShippingType.NOT_SET;
        if (rbShipping.Checked)
        {
            //Page.Validate("vgShipping");
            //if (!Page.IsValid)
            //    return;
            shippingType = SHOPPING_ORDER.Common.ShippingType.SHIPPING;
            Order.ShippingFirstName = tbName.Text;
            Order.ShippingAddress = tbAddress.Text;
            Order.ShippingCity = tbCity.Text;
            Order.ShippingPhone = tbPhone.Text;


        }
        else if (rbPersonal.Checked)
            shippingType = SHOPPING_ORDER.Common.ShippingType.PERSONAL;
        Order.ShippingType  = shippingType;


        if (cbCompany.Checked)
        {
            ADDRESS address = new ADDRESS();
            COMPANY company = new COMPANY ();
            CustomerRep crep = new CustomerRep();

            if (Order.COMPANY_ID != null){
                company = crep.GetCompanyByID(Order.COMPANY_ID.Value );
                address = crep.GetAddressByID(company.ADDR_ID);
            }

            // save new company/address
            address.STREET = tbCAddress.Text;
            address.CITY = tbCCity.Text;
            address.POSTAL_CODE = tbCCity.Text.Trim().Split(" ".ToCharArray())[0] ?? "";

            company.TAX_NUMBER = tbCTax.Text;
            company.NAME = tbCTititle.Text;
            company.ZAVEZANEC = cbZavezanec.Checked;

            if (Order.COMPANY_ID == null)
            {
                // insert new company (and save)
                crep.AddCompany(company, address);

                // get reference to company
                Order.COMPANY_ID = company.COMPANY_ID;
            }

 
            // save
            crep.Save();
        
        }

        Order.IsCompany = cbCompany.Checked;


        // save
        Orep.Save();  

        // if all ok
        OnSaveOK(this, new EventArgs());
    
    }


}
