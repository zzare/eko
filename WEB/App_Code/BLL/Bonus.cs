﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

/// <summary>
/// Summary description for BONUS
/// </summary>
/// 
namespace SM.EM
{

    public partial class Bonus
    {
        public static string GetBonusUserHrefRegister(Guid userID)
        {
            return "~/Register.aspx?bid=" + HttpContext.Current.Server.UrlEncode(SM.EM.Security.Encryption.Hex(userID.ToString()));

        }

        public static int GetBonusPercent() {
            return 20;       
        }

        public static decimal GetBonusPrice(decimal fullPrice)
        {
            return ((decimal)((decimal)GetBonusPercent() / 100) * fullPrice);
        }


    }
}