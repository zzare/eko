﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_viewLoggedIn.ascx.cs" Inherits="_ctrl_login_viewLoggedIn" %>

 <script type="text/javascript">

     $(document).ready(function () {

         $('.login_ddl_W span.userNameS_droppanel').hide();

         $('.login_ddl_W ').hover(function () {
             $('.login_ddl_W span.userNameS_droppanel').stop(true, true).slideDown('fast');
         }, function () {
             $('.login_ddl_W span.userNameS_droppanel').stop(true, true).slideUp('slow');
         });

     });


</script>

<%--<span class="userNameS"><%= Page.User.Identity.Name  %></span>--%>
<span class="login_ddl_W">
    <a class="userNameS" href='<%= SM.BLL.Common.ResolveUrl( SM.EM.Rewrite.EmenikUserUrl("eko", 1478, GetGlobalResourceObject("Modules", "LoginMyAccount").ToString() ) )%>'><%= Page.User.Identity.Name  %></a>

    <% if (LANGUAGE.GetCurrentLang() == "si"){%>
     <span class="userNameS_droppanel">
            <a href='<%= SM.BLL.Common.ResolveUrl( SM.EM.Rewrite.EmenikUserUrl("eko", 1478, GetGlobalResourceObject("Modules", "LoginMyAccount").ToString()) )%>'><%= GetGlobalResourceObject("Modules", "LoginMyAccount")%></a>
            <a href='<%= SM.BLL.Common.ResolveUrl( SM.EM.Rewrite.EmenikUserUrl("eko", 1479, GetGlobalResourceObject("Modules", "LoginMyOrders").ToString()) )%>'><%= GetGlobalResourceObject("Modules", "LoginMyOrders")%></a>
        </span>

<%} %>   

</span>
<a class="logoutS" href="#" onclick="logOut()" ><%= GetGlobalResourceObject("Modules", "signout")%></a>


<div class="clearing"></div>