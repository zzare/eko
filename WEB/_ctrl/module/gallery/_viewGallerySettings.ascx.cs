﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _ctrl_module_poll_cntGallerySettings : BaseControlView
{

    public object Data;
    public object[] Parameters;

    protected CMS_WEB_PART  Cwp;
    public CWP_GALLERY CwpGallery;
    protected string  LangID;
    protected IEnumerable<vw_GalleryDesc> GalleryDescList;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        LoadData();
    }



    protected void LoadData() {

        if (Data == null)
            return;

        // data
        Cwp = (CMS_WEB_PART )Data;


        if (Parameters != null)
        {
            if (Parameters.Length > 0 && Parameters[0] != null)
                IsModeCMS = bool.Parse(Parameters[0].ToString());
            if (Parameters.Length > 1 && Parameters[1] != null)
                CwpGallery = (CWP_GALLERY)Parameters[1];
            if (Parameters.Length > 2 && Parameters[2] != null)
                GalleryDescList = (IEnumerable<vw_GalleryDesc >) Parameters[2];
        }

        // load poll data
        tbTitle.Value = Cwp.TITLE ;
        cbActive.Checked = Cwp.SHOW_TITLE;
        tbPageSize.Value = Cwp.PAGE_SIZE.ToString();


        if (CwpGallery == null)
            CwpGallery = CMS_WEB_PART.GetCwpGalleryByID(Cwp .CWP_ID);

        // bind types
        if (Cwp .ID_INT != null)
        {

            lvType.DataSource = GALLERY.GetImageTypesFromGalleryByType(Cwp .ID_INT.Value, IMAGE_TYPE.Common.Type.THUMB_PUBLIC).OrderBy(w=>w.DESC );
            lvType.DataBind();


            // load gallery desc
            if (GalleryDescList != null)
            {
                List<vw_GalleryDesc> list = GalleryDescList.ToList() ;
                lvList.DataSource = list;
                lvList.DataBind();
            }
        }

    }


    protected string RenderChecked(object d)
    {
        string ret = "";
        if (Cwp  == null)
            return ret;
        if (CwpGallery == null)
            return ret;

        IMAGE_TYPE item = (IMAGE_TYPE)d;

        if (item.TYPE_ID == CwpGallery.THUMB_TYPE_ID)
            return "checked";

        return ret;

    }


}
